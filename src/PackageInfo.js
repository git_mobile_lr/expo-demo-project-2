import Constants from 'expo-constants';
import * as Device from 'expo-device'
import _ from 'lodash'
import * as Application from 'expo-application';
import * as SecureStore from 'expo-secure-store'

const PACKAGE_INFO = require('../app.json')
// const PACKAGE_INFO = require('../env/rc/app.json')
// const PACKAGE_INFO = require('../env/production/app.json')


getVersionCode = () => {
  return PACKAGE_INFO.fieldbuddy.version_code + (__DEV__
    ? "DEV"
    : "default")
}
getDisplayChannel = () => {
  let rc = (__DEV__
    ? "DEV"
    : "default")

  return rc || "default"
}

getDisplayVersion = () => {
  let vc = getVersionCode() || "default"
  return `${PACKAGE_INFO.version} channel: ${vc}`
}

manifestId = () => {
  try {
    
    let _ssid = "@" + Constants.expoConfig.owner + "/" + Constants.expoConfig.slug;
    console.warn("manifestId " + _ssid)
    return _ssid
    return "@" + Constants.manifest.owner + "/" + Constants.manifest.slug;

    
  } catch (error) {
    return "@fieldbuddy/fieldbuddyswift-test"
    // return "@fieldbuddy/fieldbuddyswift-rc"
    // return "@fieldbuddy/fieldbuddyswift"
    // debugger
    // let c = Constants
    // debugger
    // let m = Constants.manifest
    // debugger
    // return Constants.manifest.originalFullName

  }
}

const GUID = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  let _guid = s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();

  return _guid
}

const getNewDeviceId = async () => {

  let deviceId = await SecureStore.getItemAsync('newDeviceId');
  if (!deviceId) {

    deviceId = "sdk44+" + GUID()
    await SecureStore.setItemAsync('newDeviceId', deviceId);
  }
  return deviceId;
}

getNewSessionUUID = async () => {
  
  let _deviceId = await getNewDeviceId()
  if (__DEV__) {
    return "DEV-" + _deviceId + "-" + manifestId()
  }

  return _deviceId + "-" + manifestId()
}


export default {
  MANIFEST_ID: manifestId(),
  NUUID: getNewSessionUUID,
  CLIENT_ID: PACKAGE_INFO.fieldbuddy.connected_app_consumer_key,
  CLIENT_SECRET: PACKAGE_INFO.fieldbuddy.connected_app_consumer_secret,
  REDIRECT_URL: PACKAGE_INFO.fieldbuddy.redirect_url,
  AUTH_URL: PACKAGE_INFO.fieldbuddy.auth_url,
  STORAGE_ENGINE: PACKAGE_INFO.fieldbuddy.storage,
  STORE_VERSION: PACKAGE_INFO.expo.version,
  EXPO_SDK: PACKAGE_INFO.expo.sdkVersion,
  EXPO_VERSION: PACKAGE_INFO.expo.version,
  RELEASE_CHANNEL_DISPLAY: getDisplayChannel(),
  VERSION_DISPLAY: PACKAGE_INFO.fieldbuddy.version_display,
  VERSION: PACKAGE_INFO.fieldbuddy.version,
  VERSION_CODE: getVersionCode(),
  GOOGLE_API_ANDROID: PACKAGE_INFO.expo.android.config.googleMaps.apiKey,
  GOOGLE_API_IOS: PACKAGE_INFO.fieldbuddy.google_api_ios,
  LOGZ_API: PACKAGE_INFO.fieldbuddy.logz_api,
  LOGZ_URL: PACKAGE_INFO.fieldbuddy.logz_url,
  SENTRY: PACKAGE_INFO.fieldbuddy.sentry,
  EXVERSION: PACKAGE_INFO.version,
  RELEASE_NOTES: PACKAGE_INFO.fieldbuddy.version_display,
  FORCE_UI_CONFIG_PATH: PACKAGE_INFO.fieldbuddy.force_ui_config_path,
  SALESFORCE_PRODUCTION: `login.salesforce.com`,
  SALESFORCE_SANDBOX: `test.salesforce.com`
}