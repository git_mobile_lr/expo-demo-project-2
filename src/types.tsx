
export type ReduxAction = {
  type: ReduxActions.SET_DATA |
  ReduxActions.SET_DELTA |
  ReduxActions.SET_APP_SETTINGS |
  ReduxActions.SET_ACTIVE_WORK_ORDER |
  ReduxActions.SET_NAVIGATION |
  ReduxActions.OPEN_WORK_ORDER |
  ReduxActions.ADD_CHANGE |
  ReduxActions.SET_CHANGES_HISTORY |
  ReduxActions.CLEAR_CHANGES_HISTORY |
  ReduxActions.FAKE_CHANGES_HISTORY |
  ReduxActions.SYNC |
  ReduxActions.SYNC_RESULT |
  ReduxActions.SET_PLATFORM_STATUS |
  ReduxActions.FIRST_AID_STOP_STOPWATCH | 
  ReduxActions.FIRST_AID_REFRESH_APP |
  ReduxActions.FIRST_AID_SEND_BACKUP |
  ReduxActions.STOPWATCH_STARTED |
  ReduxActions.STOPWATCH_STOPPED | 
  ReduxActions.STOPWATCH_REFERENCE_UPDATE | 
  ReduxActions.SESSION_UUID_REFRESH_APP  |
  ReduxActions.CLOSURE_STATUS_RESET  
  payload: any
}


export enum ReduxActions {
  ADD_CHANGE = "ADD_CHANGE",
  SET_DATA = "SET_DATA",
  SET_DELTA = "SET_DELTA",
  SET_APP_SETTINGS = "SET_APP_SETTINGS",
  SET_NAVIGATION = "SET_NAVIGATION",
  OPEN_WORK_ORDER = "OPEN_WORK_ORDER",
  SET_ACTIVE_WORK_ORDER = "SET_ACTIVE_WORK_ORDER",
  SET_CHANGES_HISTORY = "SET_CHANGES_HISTORY",
  CLEAR_CHANGES_HISTORY = "CLEAR_CHANGES_HISTORY",
  FAKE_CHANGES_HISTORY = "FAKE_CHANGES_HISTORY",
  SYNC = "SYNC",
  SYNC_RESULT = "SYNC_RESULT",
  SET_PLATFORM_STATUS = "SET_PLATFORM_STATUS",
  FIRST_AID_STOP_STOPWATCH = "FIRST_AID_STOP_STOPWATCH",
  FIRST_AID_REFRESH_APP = "FIRST_AID_REFRESH_APP",
  FIRST_AID_SEND_BACKUP = "FIRST_AID_SEND_BACKUP",

  STOPWATCH_STARTED = "STOPWATCH_STARTED",
  STOPWATCH_STOPPED = "STOPWATCH_STOPPED",
  STOPWATCH_REFERENCE_UPDATE = "STOPWATCH_REFERENCE_UPDATE",

  STOPWATCH_ENDTIME_ON_HARD_RESET = "STOPWATCH_ENDTIME_ON_HARD_RESET",
  STOPWATCH_REACTIVATION_ON_HARD_RESET = "STOPWATCH_REACTIVATION_ON_HARD_RESET",
  SESSION_UUID_REFRESH_APP = "SESSION_UUID_REFRESH_APP",

  CLOSURE_STATUS_RESET = "CLOSURE_STATUS_RESET",
}

export enum Icons {
  DATABASE = "database",
  WORK = "work",
  DOT = "dot",
  PLUS = "plus",
  PLUSCIRCLE = "plusCircle",
  MINUS = "minus",
  MINUSCIRCLE = "minusCircle",
  HOME = "home",
  COMMENTS = "comments",
  WARNING = "warning",
  CHECK = "check",
  WRONG = "wrong",
  COMMENTSDOTS = "commentsdots",
  FILE = "file",
  BLOCK = "block"

}

export enum PdfMode {
  DOWNLOAD = "download",
  PREVIEW="preview"
}

export type icon =
  Icons.WORK |
  Icons.BLOCK |
  Icons.DOT |
  Icons.PLUS |
  Icons.PLUSCIRCLE |
  Icons.MINUS |
  Icons.MINUSCIRCLE |
  Icons.HOME |
  Icons.COMMENTS |
  Icons.WARNING |
  Icons.CHECK |
  Icons.WRONG |
  Icons.COMMENTSDOTS |
  Icons.FILE |
  Icons.DATABASE

export enum RecordTypes {
  STRING = "STRING",
  DATE = "DATE",
  DATETIME = "DATE/TIME",
  DATETIME2 = "DATETIME",
  FORMULA = "FORMULA",
  REFERENCE = "REFERENCE",
  CHECKBOX = "CHECKBOX",
  BOOLEAN = "BOOLEAN",
  CURRENCY = "CURRENCY",
  EMAIL = "EMAIL",
  NUMBER = "NUMBER",
  PHONE = "PHONE",
  PICKLIST = "PICKLIST",
  TEXT = "TEXT",
  TEXTAREA = "TEXTAREA",
  TIME = "TIME",
  DOUBLE = "DOUBLE",
  RECORDTYPE = "RECORDTYPE",
  MULTISELECTPICKLIST = "MULTISELECTPICKLIST",
  ICONPICKLIST = "ICONPICKLIST",
  LABELPICKLIST = "LABELPICKLIST",
  LOCATION = "LOCATION",
  PICTURE = "PICTURE"
}

export type RecordType =
  RecordTypes.STRING |
  RecordTypes.DATE |
  RecordTypes.FORMULA |
  RecordTypes.REFERENCE |
  RecordTypes.CHECKBOX |
  RecordTypes.BOOLEAN |
  RecordTypes.CURRENCY |
  RecordTypes.EMAIL |
  RecordTypes.NUMBER |
  RecordTypes.PHONE |
  RecordTypes.PICKLIST |
  RecordTypes.TEXT |
  RecordTypes.TEXTAREA |
  RecordTypes.TIME |
  RecordTypes.DOUBLE |
  RecordTypes.RECORDTYPE |
  RecordTypes.DATETIME |
  RecordTypes.DATETIME2 |
  RecordTypes.MULTISELECTPICKLIST |
  RecordTypes.ICONPICKLIST |
  RecordTypes.LABELPICKLIST |
  RecordTypes.LOCATION |
  RecordTypes.PICTURE

export enum upsertScreenModes {
  INSERT = "INSERT",
  UPDATE = "UPDATE",
  READONLY = "READONLY"
}

export enum editScreenModes {
  UPSERT = "UPSERT",
  READONLY = "READONLY"
}

export enum installedProductModes {
  DEFAULT = "DEFAULT",
  READONLY = "READONLY"
}

export enum DateMode {
  DATE = "date",
  DATETIME = "datetime"
}

export enum Locale {
  nlNL = "nl-NL",
  enNL = "en-NL",
  enUS = "en-US"
}

export type FilterCondition = {
  field: string //
  operator: "=" | "!=" | ">=" | "<=" | ">" | "<" | "$exists" | "contains"
  value: any
  label: string
  defaultText?: string
  active: boolean
  dynamic?: boolean
  table?: string
  id?: number
  hidden?: boolean
  unique?: boolean
}




export type upsertScreenMode = upsertScreenModes.INSERT | upsertScreenModes.UPDATE | upsertScreenModes.READONLY

export type MetaData = {
  accesible: boolean
  controllerName: string | null
  label: string
  length: number
  name: string
  picklistvalues: Array<object>
  precision: number
  referenceTo: Array<string>
  referenceToRelationshipNames: object
  scale: number
  type: RecordType
  updateable: boolean
}
export type DataText = Array<string>

export type DataBinding = Array<Record>

export type Record = {
  table: string
  field: string
  var: string
  format?: string
  label?: string
}

export type Events = Array<Event>

export type Event = {
  dataBinding: DataBinding
  dataText: DataText
  eventName: string
  table: string
  goBack: string
  target: string
  triggeredEvent: string
}

export type condition_v1 = [
  {
    table: string
    field: string
    filter: string
    value: string
    default: boolean
  }
]

export type singleCondition = {
  formula?: string,
  source?: source,
  alias?: alias
}

export type multipleCondition = {
  operator: operator,
  conditions: Array<singleCondition | multipleCondition>
}

export enum Sources {
  DATA = "DATA",
  MEDIA = "MEDIA_LIBRARY",
  CONTEXT = "CONTEXT"
}

export type InputValidationResponse = {
  [key: string]: Array<string>
} | undefined

export type source = Sources.DATA | Sources.MEDIA

export enum Constants {
  CURRENT_WORK_ORDER = "CURRENT_WORK_ORDER",
  CURRENT_ID = "CURRENT_ID",
  CURRENT_IP_ID = "CURRENT_IP_ID"
}

export type constant = Constants.CURRENT_WORK_ORDER | Constants.CURRENT_ID | Constants.CURRENT_IP_ID

export enum Operators {
  OR = "OR",
  AND = "AND"
}

export type operator = Operators.OR | Operators.AND

export enum ConditionAliases {
  ACTIVE_WORK_ORDER_HAS_ATTACHMENTS = "ACTIVE_WORK_ORDER_HAS_ATTACHMENTS",
  ACTIVE_WORK_ORDER_HAS_ACTIVITIES = "ACTIVE_WORK_ORDER_HAS_ACTIVITIES",
  ACTIVE_WORK_ORDER_HAS_PARTS = "ACTIVE_WORK_ORDER_HAS_PARTS",
  ACTIVE_WORK_ORDER_HAS_OTHER_EXPENSES = "ACTIVE_WORK_ORDER_HAS_OTHER_EXPENSES",
  ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS = "ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS"
}

export type alias =
  ConditionAliases.ACTIVE_WORK_ORDER_HAS_ACTIVITIES |
  ConditionAliases.ACTIVE_WORK_ORDER_HAS_ATTACHMENTS |
  ConditionAliases.ACTIVE_WORK_ORDER_HAS_OTHER_EXPENSES |
  ConditionAliases.ACTIVE_WORK_ORDER_HAS_PARTS |
  ConditionAliases.ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS

export interface WorkOrderLineItem {
  Name: string
  Owner: string
}

export interface InstalledProduct {
  Id: String
  Name: string
  Owner: string
}

export interface InstalledProductWithStatus extends InstalledProduct {
  isOpen: boolean
  openWolis: number
  closedWolis: number
}



export type appState = AppStates.ACTIVE | AppStates.INACTIVE | AppStates.BACKGROUND

export enum AppComponents {
  CARD = "Card",
  BOX = "Box"
}

/*
.active - The app is running in the foreground
.background - The app is running in the background. The user is either:
in another app
on the home screen
[Android] on another Activity (even if it was launched by your app)

[iOS] inactive - This is a state that occurs when transitioning between foreground & background, 
and during periods of inactivity such as entering the Multitasking view or in the event of an incoming call
*/
export enum AppStates {
  ACTIVE = "active",
  INACTIVE = "inactive",
  BACKGROUND = "background"
}

export type BackupInfo = {
  timestamp: string
  status: string
}

export interface SalesForcePlatformState {
  limitValue: number
  limitName: string
  actualValue: number
}

export interface AppComponent {
  type: AppComponents.CARD | AppComponents.BOX,
  config: any
}
