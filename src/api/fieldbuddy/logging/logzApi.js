import {AsyncStorage, Platform} from 'react-native'
import {PackageInfo} from '../../../helpers'
import * as FileSystem from 'expo-file-system'

import _ from 'lodash'
import moment from 'moment'
import 'moment/locale/nl'

const STORAGE_KEY = "LOGZ_1"
// / https://app.logz.io/#/dashboard/data-sources/JSON e.g.
// https://listener.logz.io:8071/?token=PkyVcBBIivtQuqFejYwounEyTCoDKRRn&type=FB2
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export const AddLog = async(logJson) => {

  const format = "YYYY-MM-DD HH:mm:ss"
  let timeStamp = moment(new Date()).format(format)
  const dateFormat = "YYYY-MM-DD"
  let date = moment(new Date()).format(dateFormat)

  let logs = []

  let currentLog = await GetLog()

  if (currentLog.type === 'success') {
    logs = [].concat(logs, currentLog.data)
  }

  let newEntry = {
    guid: guid(),
    date: date,
    timeStamp: timeStamp,
    log: {
      ...logJson
    }

  }
  logs.push(newEntry)

  let _key = `${PackageInfo.VERSION_CODE}_${STORAGE_KEY}`

  try {
    const _fileUri = FileSystem.documentDirectory + _key
    // const raw = await FileSystem.readAsStringAsync(_fileUri)

    await FileSystem.writeAsStringAsync(_fileUri, JSON.stringify(logs || []))

  } catch (e) {
     
    return {type: 'error', context: 'Logging AddLog', message: `Caught error. SetKeyValue: ${STORAGE_KEY}. Error: ${e}`}
  }

  return {type: 'success'}
}

export const GetInfo = async() => {
  try {

    let _key = `${PackageInfo.VERSION_CODE}_${STORAGE_KEY}`

    // let raw = await AsyncStorage.getItem(_key)
    const _fileUri = FileSystem.documentDirectory + _key
    return await FileSystem.getInfoAsync(_fileUri)
  } catch (e) {
    return {type: 'error', message: `Caught error. GetInfo: ${STORAGE_KEY}. Error: ${e}`}
  }

  return {type: 'error', message: `Uncaught error. GetInfo: ${STORAGE_KEY}.`}
}

export const GetLog = async() => {
  try {
    let _key = `${PackageInfo.VERSION_CODE}_${STORAGE_KEY}`
    const _fileUri = FileSystem.documentDirectory + _key
    const _info = await FileSystem.getInfoAsync(_fileUri)

    if (_info.exists) {

      const raw = await FileSystem.readAsStringAsync(_fileUri)
      if (!_.isEmpty(raw)) {
        try {
          let jsonContent = JSON.parse(raw)
          return {type: 'success', data: jsonContent}
        } catch (e) {

          return {type: 'error', message: `Caught error. GetLog: ${STORAGE_KEY}. Error: ${e}`}
        }
      }
    }
  } catch (e) {

    return {type: 'error', message: `Caught error. GetLog: ${STORAGE_KEY}. Error: ${e}`}
  }
  return {type: 'error', message: `Uncaught error. GetLog: ${STORAGE_KEY}.`}
}

export const DeleteLog = async() => {
  try {
    let _key = `${PackageInfo.VERSION_CODE}_${STORAGE_KEY}`
    const _fileUri = FileSystem.documentDirectory + _key
    const _info = await FileSystem.getInfoAsync(_fileUri)

    if (_info.exists) {
      await FileSystem.deleteAsync(_fileUri, {idempotent: true})
    }
    return true
  } catch (e) {
    return false
  }
}