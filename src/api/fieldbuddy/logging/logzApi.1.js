import {AsyncStorage, Platform} from 'react-native'
import {PackageInfo} from '../../../helpers'
import {SQLite} from 'expo'
import * as FileSystem from 'expo-file-system'

import _ from 'lodash'
import moment from 'moment'
import 'moment/locale/nl'

const DB = SQLite.openDatabase('logs.db');
const FORMAT = "YYYY-MM-DD HH:mm:ss"

const STORAGE_KEY = "LOGZ_1"
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export const InitLog = async() => {
  DB.transaction(tx => {
    tx.executeSql("create table if not exists LOGS (id INTEGER PRIMARY KEY AUTOINCREMENT, timeStamp text, content blob);");
  });

  return true

}
export const AddLog = async(logJson) => {
  
  let timeStamp = moment(new Date()).format(FORMAT)  
  const logTable = await InitLog()  
  DB.transaction(tx => {
    tx.executeSql('insert into LOGS (timeStamp, content) values (?, ?)', [timeStamp, logJson]);
    tx.executeSql('select * from LOGS', [], (_, { rows })=> {
       //(JSON.stringify(rows));
    });    
  })
  return {type: 'success'}
}


export const GetLog = async() => {

    let __array = []

     DB.transaction(tx => {
      
      tx.executeSql('select * from LOGS', [], (_, { rows }) => {
        
         //(JSON.stringify(rows));
        __array = rows._array;
        return {type: 'success', data: rows}
        
      }, (err)=>{
         
      });
    })

    
    return {type: 'success', data: {}}

   
}



export const DeleteLog = async() => {
  try {
    let _key = `${PackageInfo.VERSION_CODE}_${STORAGE_KEY}`
    const _fileUri = FileSystem.documentDirectory + _key
    const _info = await FileSystem.getInfoAsync(_fileUri)

    if (_info.exists) {
      await FileSystem.deleteAsync(_fileUri, {idempotent: true})
    }
    return true
  } catch (e) {
     
    return false
  }
}



export const GetInfo = async() => {
  try {

    let _key = `${PackageInfo.VERSION_CODE}_${STORAGE_KEY}`

    // let raw = await AsyncStorage.getItem(_key)
    const _fileUri = FileSystem.documentDirectory + _key
    return await FileSystem.getInfoAsync(_fileUri)
  } catch (e) {
     
    return {type: 'error', message: `Caught error. GetInfo: ${STORAGE_KEY}. Error: ${e}`}
  }

  return {type: 'error', message: `Uncaught error. GetInfo: ${STORAGE_KEY}.`}
}