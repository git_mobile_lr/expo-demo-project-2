import jwtDecoder from 'jwt-decode'
import qs from 'qs'
import {PackageInfo} from '../../../helpers'
import moment from 'moment'
import 'moment/locale/nl'
import {Platform} from 'react-native'

import Constants from 'expo-constants'
import * as Device from 'expo-device';

import {GetKeyValue, GetSessionInfo, GetUserInfoAsync, GetLog} from '../../../api/fieldbuddy'
import _ from 'lodash'
const ToQueryString = (params) => {
  return '?' + Object
    .entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

// /example:
// 'https://listener.logz.io:8071/?token=PkyVcBBIivtQuqFejYwounEyTCoDKRRn&type=F
// B 2';
const LOGZ_URL = PackageInfo.LOGZ_URL

// / https://app.logz.io/#/dashboard/data-sources/JSON e.g.
// https://listener.logz.io:8071/?token=PkyVcBBIivtQuqFejYwounEyTCoDKRRn&type=FB2
getPackage = () => {
  let pkg = Object.assign({}, {
    ...PackageInfo
  })
  delete pkg.GOOGLE_API
  delete pkg.LOGZ_API
  delete pkg.LOGZ_URL

  return pkg
}
export const PushSystemReport = async(systemReport, userBasic) => {

  if (!systemReport) {
    return {type: 'success', message: `No systemReport to send.`}
  }

  const format = "YYYY-MM-DD HH:mm:ss"
  let timeStamp = moment(new Date()).format(format)

  //  let bDoc = bson().serialize(systemReport)

  let names = Object.getOwnPropertyNames(systemReport)
  let _ssid = "@" + Constants.expoConfig.owner + "/" + Constants.expoConfig.slug;

  let _logs_ = names.map(_name => {

    let newEntry = {

      timeStamp: timeStamp,

      ...systemReport[_name],

      userBasic: {
        ...userBasic
      },
      device: {
        deviceId: Constants.deviceId,
        
        deviceName: Device.deviceName,
        deviceYearClass: Device.deviceYearClass,
        isDevice: Constants.isDevice
      },
      appManifest: {
        name: _ssid,
        
        
        sdkVersion: Constants.expoConfig.sdkVersion,
        slug: Constants.expoConfig.slug

      },
      expo: {
        expoRuntimeVersion: Constants.expoRuntimeVersion,
        expoVersion: Constants.expoVersion
      },

      platform: {
        os: Platform.OS,
        version: Platform.Version,
        ...Constants.platform
      },

      pkg: getPackage()
    }

    newEntry.message = `${newEntry
      .timeStamp} system_report ${newEntry
      .userBasic
      .organization_id} ${newEntry
      .userBasic
      .user_id} ${newEntry
      .userBasic
      .preferred_username} ${newEntry
      .platform
      .os} ${newEntry
      .platform
      .version} ${names
      .toString()}`

    return newEntry
  })

  //

  let pushAllResult = await this.pushAll(_logs_)

  return pushAllResult

}

const getFullLogEntry = async(_log, _userBasic) => {
  let _ssid = "@" + Constants.expoConfig.owner + "/" + Constants.expoConfig.slug;

  let newEntry = {
    ..._log,

    userBasic: {
      ..._userBasic
    },
    device: {
      deviceId: Constants.deviceId,
      
      deviceName: Device.deviceName,
      deviceYearClass: Device.deviceYearClass,
      isDevice: Constants.isDevice
    },
    appManifest: {
      name: _ssid,
      
      
      sdkVersion: Constants.expoConfig.sdkVersion,
      slug: Constants.expoConfig.slug

    },
    expo: {
      expoRuntimeVersion: Constants.expoRuntimeVersion,
      expoVersion: Constants.expoVersion
    },

    platform: {
      os: Platform.OS,
      version: Platform.Version,
      ...Constants.platform
    },

    pkg: getPackage()
  }
  newEntry.type = newEntry.log.type
  newEntry.message = `${newEntry.timeStamp} ${newEntry.log.type || "info"} ${newEntry.userBasic.organization_id} ${newEntry.userBasic.user_id} ${newEntry.userBasic.preferred_username} ${newEntry.platform.os} ${newEntry.platform.version} ${newEntry.log.context} ${newEntry.log.message}`

  return newEntry
}

const getHeaderLogEntry = async(_userBasic) => {
  let _ssid = "@" + Constants.expoConfig.owner + "/" + Constants.expoConfig.slug;
  let newEntry = {
    userBasic: {
      ..._userBasic
    },
    device: {
      deviceId: Constants.deviceId,
      
      deviceName: Device.deviceName,
      deviceYearClass: Device.deviceYearClass,
      isDevice: Constants.isDevice
    },
    appManifest: {
      name: _ssid,
      
      
      sdkVersion: Constants.expoConfig.sdkVersion,
      slug: Constants.expoConfig.slug

    },
    expo: {
      expoRuntimeVersion: Constants.expoRuntimeVersion,
      expoVersion: Constants.expoVersion
    },

    platform: {
      os: Platform.OS,
      version: Platform.Version,
      ...Constants.platform
    },

    pkg: getPackage()
  }

  newEntry.username = _userBasic.preferred_username
  newEntry.organization_id = _userBasic.organization_id
  newEntry.user_id = _userBasic.user_id
  newEntry.platform = Platform.OS
  newEntry.platform_version = Platform.Version
  newEntry.type = "HEADER"

  return newEntry
}
const getSimpleLogEntry = async(_log, _userBasic, _advanced = false) => {

  let newEntry = {}

  newEntry.timeStamp = _log.timeStamp
  newEntry.date = _log.date
  newEntry.username = _userBasic.preferred_username
  newEntry.organization_id = _userBasic.organization_id
  newEntry.user_id = _userBasic.user_id
  newEntry.platform = Platform.OS
  newEntry.platform_version = Platform.Version
  newEntry.expo_version = PackageInfo.VERSION_DISPLAY
  newEntry.activeRing = _userBasic.activeRing

  ///THIS MUST CLEANED UP!
  newEntry.type = "INFO"
  if (_log.type) {
    newEntry.type = (_log.type +"").toUpperCase()
  } else if (_log.log && _log.log.type) {
    newEntry.type = (_log.log.type +"").toUpperCase()
    
  }
  if(newEntry.type === "TRUE"){
    newEntry.type = "INFO"
  }
  
  
  newEntry.message = _log.log.message
  let _ssid = "@" + Constants.expoConfig.owner + "/" + Constants.expoConfig.slug;

  if (_advanced) {
    newEntry.advanced = {
      userBasic: {
        ..._userBasic
      },
      device: {
        deviceId: Constants.deviceId,
        
        deviceName: Device.deviceName,
        deviceYearClass: Device.deviceYearClass,
        isDevice: Constants.isDevice
      },
      appManifest: {
        name: _ssid,
        
        
        sdkVersion: Constants.expoConfig.sdkVersion,
        slug: Constants.expoConfig.slug

      },
      expo: {
        expoRuntimeVersion: Constants.expoRuntimeVersion,
        expoVersion: Constants.expoVersion
      },

      platform: {
        os: Platform.OS,
        version: Platform.Version,
        ...Constants.platform
      },

      pkg: getPackage()
    }
  }

  return newEntry
}

export const PushLogs = async(allLogs, userBasic, addHeader = true) => {
  
  if (!allLogs) {
    return {type: 'success', message: `No logs to send.`}
  }

  let _logs_ = []
  let _addHeader = addHeader
  for (let log of allLogs) {
    if (_addHeader) {
      _addHeader = false
      let headerLog = await getSimpleLogEntry(log, userBasic, true)
      _logs_.push(headerLog)
    } else {
      let fullLog = await getSimpleLogEntry(log, userBasic, false)
      _logs_.push(fullLog)
    }
  }

  // if (_.isEmpty(_logs_)) {   return {type: 'success', message: `No logs to
  // send.`} }

  let pushAllResult = await this.pushAll(_logs_)

  return pushAllResult
}

pushAll = async(_logs_) => {
  let that = this
  var results = await Promise.all(_logs_.map(async(item) => {
    return await that.push(item);

  }));

  if ((results.filter(x => x.type != 'success') || []).length === 0) {
    return {type: 'success'}
  } else {
    return {type: 'error'}
  }
}

push = async(log) => {

  let token = PackageInfo.LOGZ_API

  if (!token) {
    return {type: 'error', message: `No LOGZ_API specified.`};
  }

  let _url = LOGZ_URL

  this.pushInsightOps(log)

  return fetch(_url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    },
    body: (JSON.stringify(log) || "").substr(0, 4000)
  }).then((response) => {

    if (response.ok) {
      return {type: 'success', result: response.statusText}
    }

    return {
      type: 'error',
      errorCode: response.status,
      message: response._bodyText || ''
    }
  }).catch((error) => {
     
    return {type: 'error', message: error}
  })

}

pushInsightOps = async(log) => {

 

  let _url = ""

  

  return fetch(_url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    },
    body: (JSON.stringify(log) || "").substr(0, 4000)
  }).then((response) => {

    if (response.ok) {
      return {type: 'success', result: response.statusText}
    }

    return {
      type: 'error',
      errorCode: response.status,
      message: response._bodyText || ''
    }
  }).catch((error) => {
     
    return {type: 'error', message: error}
  })

}
