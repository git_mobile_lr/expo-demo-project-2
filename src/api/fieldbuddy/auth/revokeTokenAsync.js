// r o_understanding_refresh_token_oauth.htm
import jwtDecoder from 'jwt-decode';
import qs from 'qs';
import _ from 'lodash'
import { AddLog } from '../logging/logzApi'
import * as Sentry from 'sentry-expo';
// import * as Sentry from 'sentry-expo';

const ToQueryString = (params) => {
  return '?' + Object
    .entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

export default (revokeTokenAsync = async (instance_url = `login.salesforce.com`, token) => {

  if (!token) {
    let _error = {
      type: 'error',
      message: `Missing at least one parameter (token).`
    }
    // AddLog(_error)
    // Sentry.captureException(new Error(_error.message), {logger: 'revokeTokenAsync'})
    // Sentry.Native.captureException(new Error(_error.message), {logger: 'revokeTokenAsync'})
    return _error
  }

  let _url = `` + ToQueryString({ token: token })



  // AddLog({type: 'info', message: 'AuthSession retrieving access token', params: _url})

  return fetch(_url, { method: 'GET' }).then((response) => {

    return response.ok
      ? {
        type: 'success'
      }
      : {
        type: 'error'
      }
  }).catch((e) => {

    let _error = {
      type: 'error',
      message: e.message
    }
    // AddLog(_error)

    // Sentry.captureException(new Error(_error.message), {logger: 'revokeTokenAsync'})
    // Sentry.Native.captureException(new Error(_error.message), {logger: 'revokeTokenAsync'})
    return _error
  })

})