import * as AuthSession from 'expo-auth-session';
import jwtDecoder from 'jwt-decode';
import qs from 'qs';
import * as Sentry from 'sentry-expo';
import { AddLog } from '../logging/logzApi'
import { PackageInfo } from '../../../helpers'
import _ from 'lodash'
const ToQueryString = (params) => {
  return '?' + Object
    .entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

export default (signInWithSalesforceAsync = async (auth_url = `login.salesforce.com`, client_id) => {

  if (!client_id) {
    let _error = {
      type: 'error',
      message: `No client_id specified.`
    }
    // AddLog(_error)
    return _error
  }

  try {
    // . com
    let _projectNameForProxy = PackageInfo.MANIFEST_ID

    const redirectUri = AuthSession.makeRedirectUri({
      useProxy: true,
      scheme: 'myapp',
      projectNameForProxy: _projectNameForProxy
    })
    // let redirectUri = AuthSession.getRedirectUrl();

    let redirect_uri = redirectUri



    let url = ``;
    url += ToQueryString({ response_type: 'code', client_id: client_id, display: 'touch', prompt: 'login consent', scope: 'full api id web refresh_token' });
    url += `&redirect_uri=${redirect_uri}&prompt=${redirect_uri}`;
    
    AddLog({ type: 'info', message: `AuthSession about to start:  ${JSON.stringify(url)}`, params: url })

    let result = await AuthSession.startAsync({
      authUrl: url,
      projectNameForProxy: _projectNameForProxy
    })
    
    if (result.type === 'success') {
      let _log = {
        type: 'info',
        message: 'AuthSession succeeded'
      }
      
      _log.params = result.params
      AddLog(_log)

      result.params.redirect_uri = redirect_uri

      return result
    } else if (result.type === 'error') {
      let _error = {
        type: 'error',
        message: `AuthSession error: ${result.errorCode}`,
        params: params
      };
      // AddLog(_error)
      // Sentry.captureException(new Error(_error.message), {logger: 'signInWithSalesforceAsync'});
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'signInWithSalesforceAsync'});
      return _error

    } else {
      let _cancel = {
        type: 'cancelled',
        message: 'AuthSession cancelled'
      };
      // AddLog(_cancel)
      return _cancel
    }
  } catch (e) {
    
    console.log("e " + e.message)
    let _error = {
      type: 'error',
      message: e.message
    }
    AddLog(_error)
    // Sentry.captureException(new Error(_error.message), {logger: 'signInWithSalesforceAsync'});
    // Sentry.Native.captureException(new Error(_error.message), {logger: 'signInWithSalesforceAsync'});
    return _error
  }

});
