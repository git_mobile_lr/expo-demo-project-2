// r o_understanding_refresh_token_oauth.htm
import jwtDecoder from 'jwt-decode';
import qs from 'qs';
import _ from 'lodash'
import { AddLog } from '../logging/logzApi'
import * as Sentry from 'sentry-expo';
// import * as Sentry from 'sentry-expo';

const ToQueryString = (params) => {
  return '?' + Object
    .entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

export default (refreshAccessTokenAsync = async (sessionID, instance_url = `login.salesforce.com`, access_token, client_id, client_secret, refresh_token) => {

  if (!client_id || !client_secret || !refresh_token) {
    let _error = {
      type: 'error',
      message: `Missing at least one parameter (client_id, client_secret, refresh_token).`
    }
    // AddLog(_error)
    // Sentry.captureException(new Error(_error.message), {logger: 'refreshAccessTokenAsync'})
    // Sentry.Native.captureException(new Error(_error.message), {logger: 'refreshAccessTokenAsync'})
    return _error
  }

  let _url = `${instance_url}/services/oauth2/token`

  let jsonParams = {
    grant_type: 'refresh_token',
    refresh_token: `${refresh_token}`,
    client_id: `${client_id}`,
    client_secret: `${client_secret}`
  }

  // AddLog({type: 'info', message: 'AuthSession refreshing token', params: jsonParams})

  return fetch(_url, {
    method: 'POST',
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    },
    body: qs.stringify(jsonParams)
  }).then((response) => {

    return response.json()
  }).then((responseJson) => {
    if (responseJson.error) {
      let _error = {
        type: 'error',
        message: responseJson.error
      }
      // AddLog(_error)
      // Sentry.captureException(new Error(_error.message), {logger: 'refreshAccessTokenAsync'})
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'refreshAccessTokenAsync'})
      return _error
    }
    // AddLog({type: 'info', message: 'AuthSession token refreshed', params: responseJson})

    return { type: 'success', result: responseJson }
  }).catch((e) => {

    let _error = {
      type: 'error',
      message: e.message
    }
    // AddLog(_error)

    // Sentry.captureException(new Error(_error.message), {logger: 'refreshAccessTokenAsync'})
    // Sentry.Native.captureException(new Error(_error.message), {logger: 'refreshAccessTokenAsync'})
    return _error
  })
})