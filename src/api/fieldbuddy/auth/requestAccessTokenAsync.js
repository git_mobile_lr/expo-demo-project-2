// o _understanding_web_server_oauth_flow.htm
import jwtDecoder from 'jwt-decode';
import qs from 'qs';
import _ from 'lodash'
import { AddLog } from '../logging/logzApi'
import * as Sentry from 'sentry-expo';
// import * as Sentry from 'sentry-expo';

const ToQueryString = (params) => {
  return '?' + Object
    .entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

export default (requestAccessTokenAsync = async (instance_url = `login.salesforce.com`, client_id, client_secret, code, redirect_uri) => {
  
  
  
  if (!client_id || !client_secret || !redirect_uri || !code) {
    let _error = {
      type: 'error',
      message: `Missing at least one parameter (client_id, client_secret, redirect_uri, code).`
    }
    // AddLog(_error)
    // Sentry.captureException(new Error(_error.message + " - Params = " +instance_url), {logger: 'requestAccessTokenAsync'})
    // Sentry.Native.captureException(new Error(_error.message + " - Params = " +instance_url), {logger: 'requestAccessTokenAsync'})
    return _error
  }

  
  // await SLEEP(2000)

  let _url = `` + ToQueryString({ grant_type: 'authorization_code', client_id: `${client_id}`, client_secret: `${client_secret}`, redirect_uri: `${redirect_uri}`, code: `${code}` })
  // AddLog({type: 'info', message: 'AuthSession retrieving access token', params: _url})
  
  // await SLEEP(3000)

  return fetch(_url, { method: 'GET', headers: {    
    'Cache-Control': 'no-cache'    
  } })
  // .then(checkStatus)
  .then((response) =>{
    
    // JS(response, response.json())
    
    
    if (!response.ok && response.status === 400) {
      console.log("RequestAccessTokenAsync 3 X ");  
     // return { type: 'error', code: 400, errorCode: 'BAD_REQUEST_ERROR', message: 'Bad request error' }
      
    }
    

    return  response.json()
    
  }).then((responseJson) => {
    // console.log("RequestAccessTokenAsync 4")
    if (responseJson.error) {
      let _error = {
        type: 'error',
        message: responseJson.error
      }
      // AddLog(_error)
      // Sentry.captureException(new Error(_error.message + " - Params = " +_url), {logger: 'requestAccessTokenAsync'})
      // Sentry.Native.captureException(new Error(_error.message + " - Params = " +_url), {logger: 'requestAccessTokenAsync'})
      return _error
    }

    if (responseJson.type === 'error') {
      console.log("RequestAccessTokenAsync 5")
      return responseJson
    }
    // AddLog({type: 'info', message: 'AuthSession access token retrieved', params: responseJson})
    return { type: 'success', result: responseJson }
  }).catch((e) => {
    console.log("RequestAccessTokenAsync e ")
    let _error = {
      type: 'error',
      message: e.message
    }
    // AddLog(_error)

    // Sentry.captureException(new Error(_error.message + " - Params = " +_url), {logger: 'requestAccessTokenAsync'})
    // Sentry.Native.captureException(new Error(_error.message + " - Params = " +_url), {logger: 'requestAccessTokenAsync'})
    return _error
  })
})



const SLEEP = async (msec = 1000) => {
  console.warn("Gonna sleep for " + msec);
  return new Promise(resolve => setTimeout(resolve, msec));
}


const JS = async (pure, json) => {
  console.warn("Let's see " );
  console.log("p", pure)
  console.log("j", JSON.stringify(json))
}


function checkStatus(res) {
  // console.log("checkStatus ")
  
  if (res.status >= 200 && res.status < 300) {
    return res
  } else {
    if (res.status === 401) {
      return { type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID', message: 'Session expired or invalid' }
    }
    if (res.status === 413) {
      return { type: 'error', code: 413, errorCode: 'PayloadTooLargeError', message: 'PayloadTooLargeError: request entity too large' }
    }
    let err = new Error(res.statusText)
    err.response = res
    throw err
  }
}