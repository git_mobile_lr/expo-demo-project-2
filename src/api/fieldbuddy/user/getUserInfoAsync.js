
import _ from 'lodash'
export default(getUserInfoAsync = async(instance_url, access_token) => {

  if (!instance_url || !access_token) {
    return {type: 'error', message: `Missing parameters: instance_url or/and access_token`}
  }

  return fetch(`${instance_url}/services/oauth2/userinfo`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`,
      'Content-Type': 'application/json'
    }
  })
  .then((response) => response.json())
  .then((responseJson) => {  
    // Sentry.captureMessage("SignIn", JSON.stringify(responseJson));
    return {type: 'success', result: responseJson}
  }).catch((e) => {
    return {type: 'error', message: e.message}
  })
})