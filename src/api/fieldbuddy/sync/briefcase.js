import { PackageInfo } from '../../../helpers'
import { AddLog } from '../logging/logzApi'
import * as Sentry from 'sentry-expo';
import moment from 'moment'
import { Platform } from 'react-native';
import _ from 'lodash'
import * as FileSystem from 'expo-file-system'
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING`
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING-RC`
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING-LIVE`
const rings = require('../../../../rings.json')
const ACTIVE_RING = rings.activeRing

const GetSessionUUID = async ()=>{
  return await PackageInfo.NUUID()
}


export const measure = (__START_DATE__, info) => {
  const _period = moment().diff(__START_DATE__, "ms")
  // if (_period > 1000) {
    // console.warn(info, _period)
  // }

}
const briefcasePost = async (instance_url, access_token, jsonParams, instance_url_endpoint = '/services/apexrest/FIELDBUDDY/RemoteService', skipParams = false, useCaughtErrorNetworkRequest = false) => {

  let _jsonParams = Object.assign({}, {
    ...jsonParams
  })

  if (!skipParams) {
    _jsonParams.SessionId = await GetSessionUUID()

    _jsonParams = Object.assign({}, {
      ..._jsonParams
    }, {
      StoreVersion: PackageInfo.STORE_VERSION,
      ExpoVersion: PackageInfo.VERSION_DISPLAY,
      ExpoSDK: PackageInfo.EXPO_SDK,
      Platform: Platform.OS,
      PlatformVersion: Platform.Version
    })

  }
  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }



  AddLog({
    type: 'info',
    context: 'Syncing',
    message: 'Syncing ' + JSON.stringify({ instance_url: instance_url, access_token: access_token, jsonParams: _jsonParams }),
    params: {
      instance_url: instance_url,
      access_token: access_token,
      jsonParams: _jsonParams
    }
  })
  if (!instance_url || !access_token) {
    let _error = {
      type: 'error',
      context: 'Syncing',
      message: `Missing parameters: instance_url or/and access_token`
    }

    Sentry.Native.withScope(function (scope) {
      scope.setLevel(Sentry.Native.Severity.Warning);
      Sentry.Native.captureException(new Error(_error));
    });

    // AddLog(_error)
    // Sentry.Native.captureException(new Error(_error.message), { logger: 'sync' });

    return _error
  }
  var __START_DATE__ = moment();

  const _endpoint_ = `${instance_url}${instance_url_endpoint}`
  const _request = JSON.stringify(_jsonParams)
  const TYPE = _jsonParams.Type


  
  measure(__START_DATE__, "before post");
  
  // return _fetch()
  
  function _fetch() {
    return fetch(_endpoint_, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${access_token}`,
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Swift-token': guid()
      },
      body: _request
    }).then((response) => {
      
      measure(__START_DATE__, "part post");
      if (useCaughtErrorNetworkRequest) {
        let _errorMsg = "Done in Tester or Dev Mode: Fake 'TypeError: Network request failed'!";
        console.warn(_errorMsg);
        // alert(_errorMsg)
        throw _errorMsg;
      }

      // TEST-CASE: return {type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID',
      // message: 'Session expired or invalid'}
      // console.warn("RESPONSE STATUS: "+response.status )
      if (!response.ok) {

        if (response.status === 401) {
          const _error401 = {
            type: 'error',
            code: 401,
            errorCode: 'INVALID_SESSION_ID',
            message: '401 Unauthorized'
          };

          // Sentry.Native.captureException(new Error(_error401.message), { logger: 'sync' });
          return _error401;
        }
        if (response.status === 404) {

          Sentry.Native.withScope(function (scope) {
            scope.setLevel(Sentry.Native.Severity.Error);
            Sentry.Native.captureException(new Error("404: " + _request));
          });

          try {
            return {
              type: 'error',
              code: 404,
              errorCode: 'NOT_FOUND',
              extra: JSON.stringify(response)
            };
          } catch (error) {
            return {
              type: 'error',
              code: 404,
              errorCode: 'NOT_FOUND',
              extra: "response is not json"
            };
          }
        }

        if (response.status === 500) {

          Sentry.Native.withScope(function (scope) {
            scope.setLevel(Sentry.Native.Severity.Error);

            Sentry.Native.captureException(new Error("500: " + response + " " + _request));
          });



          response['request'] = _request;

          return response.json();




        }
      }

      if (response.status >= 200 && response.status < 300) {
        return response.json();
      } else {
        if (response.status) {
          Sentry.Native.withScope(function (scope) {
            scope.setLevel(Sentry.Native.Severity.Warning);
            Sentry.Native.captureException(new Error(response.status + ": " + _request));
          });

        } else {

          Sentry.Native.withScope(function (scope) {
            scope.setLevel(Sentry.Native.Severity.Error);
            Sentry.Native.captureException(new Error("Unknown: " + _request));
          });

        }


        return {
          type: 'error',
          code: response.status,
          errorCode: 'OTHER_ERROR',
          extra: JSON.stringify(response)
        };
      }
    }).then((responseJson) => {

      measure(__START_DATE__, "post");

      if (responseJson && _.isArray(responseJson) && !_.isEmpty(responseJson.map(x => x.errorCode))) {

        let msg = _.trim(_.join(responseJson.filter(x => x.errorCode).map(x => x.message)));


        const _error = {
          request: _request,
          type: 'error',
          code: 500,
          errorCode: 500,
          message: msg,
          isHard: true
        };



        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Error);
          Sentry.Native.captureException(new Error(_error.message));
        });



        return _error;
      }

      if (responseJson.type === 'error' && responseJson.statusCode === 500) {

        if (!(responseJson.errorMessage || "").startsWith("Inconsistent sequence")) {
          const _msgPrevented500 = `Response with errorMessage: ${responseJson.errorMessage}`;
          const _isSoft = !responseJson.ConfigurationName && !responseJson.mobileSyncLogId;

          const _error = {
            request: _request,
            type: 'error',
            code: 500,
            errorCode: 500,
            message: _msgPrevented500,
            isHard: !_isSoft
          };

          AddLog(_error);

          // Sentry.Native.captureException(new Error(_error.message), { logger: 'sync' });
          Sentry.Native.withScope(function (scope) {
            scope.setLevel(Sentry.Native.Severity.Error);
            Sentry.Native.captureException(new Error(_error.message));
          });

          return _error;
        }
      }

      if (responseJson.type === 'error' && responseJson.errorMessage) {

        const isAlreadyPushed = responseJson
          .errorMessage
          .indexOf("already pushed") >= 0;

        if (isAlreadyPushed) {
          //DEPRECATED
          const _prevSyncId = responseJson.prevSyncId || 0;
          return {
            type: 'warning',
            context: 'Syncing',
            message: 'SyncId was already pushed',
            nextSyncId: (_prevSyncId + 1)
          };
        }

      }

      if (responseJson.type === 'error') {
        const isInconsistentSequence = (responseJson.errorMessage || "").startsWith("Inconsistent sequence");
        if (isInconsistentSequence) {
          let _error = {
            type: 'error',
            context: 'Syncing',
            message: 'Response with type error',
            isInconsistentSequence: true,
            params: responseJson
          };

          AddLog(_error);
          let _responseJson = Object.assign({}, responseJson, { result: responseJson }, { isInconsistentSequence: true });
          return _responseJson;
        }
        let _error = {
          type: 'error',
          context: 'Syncing',
          message: 'Response with type error',
          params: responseJson
        };

        AddLog(_error);
        let _responseJson = Object.assign({}, responseJson, { result: responseJson });
        return _responseJson;
      }

      if (responseJson.errorMessage) {

        let _error = {
          type: 'error',
          context: 'Syncing',
          message: `Response with errorMessage: ${responseJson.errorMessage}`,
          params: responseJson
        };

        AddLog(_error);
        return _error;
      }

      if (([] || responseJson.errors).length > 0) {

        let _errorsReduced = ([] || responseJson.errors).reduce((x, y) => x + y);

        let _error = {
          type: 'error',
          context: 'Syncing',
          message: `Response with errors: ${_errorsReduced}`
        };

        AddLog(_error);
        return _error;
      }

      if (responseJson.previouslyReturnedResponse) {
        // console.warn("YOU SHALL PUSH CHANGES AGAIN!")
        return { type: 'duplicate', result: responseJson, prevSyncId: responseJson.prevSyncId };

      }

      if (TYPE === 'DATA') {
        if (responseJson.type === 'success') {
          if (jsonParams.processAsync) {
            return { type: 'success', result: responseJson.asyncJobIds, mobileSyncLogId: responseJson.mobileSyncLogId };
          }
          return { type: 'success', result: responseJson };
        } else {

          return { type: 'error', result: responseJson };
        }
      }


      return { type: 'success', result: responseJson };
    }).catch((e) => {
      measure(__START_DATE__, "catch");
      
      console.error("Error when fetching data: " + e.message);
      let _error = {
        type: 'error',
        context: 'Syncing',
        message: `Caught error (briefcasePost): ${e}`
      };

      AddLog(_error);
      return _error;

    });
  }
  // return _fetch()
  function fetchWithTimeout(timeout) {
    // Create a timeout promise that will reject after the specified time
    const timeoutPromise = new Promise((_, reject) => {
      setTimeout(() => {
        
        // if (TYPE === 'DATA') {
        //   console.warn('Request timed out ' + timeout + " s")
        //   alert("No i dupa!")
        // }
        reject(new Error('408 Request Timeout'));
        // reject(()=>{
        //   new Error('Request timed out ' + timeout + ' s');
        // });
      }, timeout);
    });
  
    // Use Promise.race() to race between the fetch request and the timeout
    let _race =  Promise.race([
      _fetch(),
      timeoutPromise,
    ]);


    return _race
  }

 let r = 
  fetchWithTimeout(60 * 1000)

  // r.then((response) => {
  //   // if (!response.ok) {
  //   //   throw new Error(`HTTP error! Status: ${response.status}`);
  //   // }
    
  
  // })
  //   .catch((error) => {
  //     // Handle errors, including timeouts
  //     // alert(error.message)
  //     console.error(error.message);
  //   });

  return r;
  // .then((response) => {
  //   if (!response.ok) {
  //     throw new Error(`HTTP error! Status: ${response.status}`);
  //   }
  //   return response.json();
  // })
  // .then((data) => {
  //   // Handle the fetched data
  //   console.log(data);
  // })
  // .catch((error) => {
  //   // Handle errors, including timeouts
  //   console.error(error.message);
  // });

}

const briefcaseGenerateLocationBasedWO = async (instance_url, access_token, jsonParams, instance_url_endpoint = '/services/apexrest/FieldBuddyTestData', skipParams = false) => {

  const _endpoint_ = `${instance_url}${instance_url_endpoint}`
  return await fetch(_endpoint_, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(jsonParams)
  }).then((response) => {
    if (!response.ok) {
      if (response.status === 401) {
        const _error401 = {
          type: 'error',
          code: 401,
          errorCode: 'INVALID_SESSION_ID',
          message: '401 Unauthorized'
        }

        return _error401
      }
    }
    else {
            
      return { type: 'success', result:  JSON.stringify(jsonParams)}
    }
  })
}


const briefcasePost500 = async (instance_url, access_token, jsonParams, instance_url_endpoint = '/services/apexrest/FieldBuddyAdmin/RemoteService', skipParams = false) => {

  let _jsonParams = Object.assign({}, {
    ...jsonParams
  })

  if (!skipParams) {
    _jsonParams.SessionId = await GetSessionUUID()

    _jsonParams = Object.assign({}, {
      ..._jsonParams
    }, {
      StoreVersion: PackageInfo.STORE_VERSION,
      ExpoVersion: PackageInfo.VERSION_DISPLAY,
      ExpoSDK: PackageInfo.EXPO_SDK,
      Platform: Platform.OS,
      PlatformVersion: Platform.Version
    })

  }
  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  AddLog({
    type: 'info',
    context: 'Syncing',
    message: 'Syncing ' + JSON.stringify({ instance_url: instance_url, access_token: access_token, jsonParams: _jsonParams }),
    params: {
      instance_url: instance_url,
      access_token: access_token,
      jsonParams: _jsonParams
    }
  })
  if (!instance_url || !access_token) {
    let _error = {
      type: 'error',
      context: 'Syncing',
      message: `Missing parameters: instance_url or/and access_token`
    }

    Sentry.Native.withScope(function (scope) {
      scope.setLevel(Sentry.Native.Severity.Warning);
      Sentry.Native.captureException(new Error(_error));
    });

    // AddLog(_error)
    // Sentry.Native.captureException(new Error(_error.message), { logger: 'sync' });

    return _error
  }

  const _endpoint_ = `${instance_url}${instance_url_endpoint}`
  const _request = JSON.stringify(_jsonParams)
  const TYPE = _jsonParams.Type
  return fetch(_endpoint_, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`,
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Swift-token': guid()
    },
    body: _request
  }).then((response) => {

    // TEST-CASE: return {type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID',
    // message: 'Session expired or invalid'}
    // console.warn("RESPONSE STATUS: "+response.status )

    if (!response.ok) {

      if (response.status === 401) {
        const _error401 = {
          type: 'error',
          code: 401,
          errorCode: 'INVALID_SESSION_ID',
          message: '401 Unauthorized'
        }

        // Sentry.Native.captureException(new Error(_error401.message), { logger: 'sync' });
        return _error401
      }
      if (response.status === 404) {

        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Fatal);
          Sentry.Native.captureException(new Error("404: " + _request));
        });

        // Sentry.Native.captureException(new Error("404: " + _request), { logger: 'sync' });

        return {
          type: 'error',
          code: 404,
          errorCode: 'NOT_FOUND',
          extra: JSON.stringify(response)
        }
      }

      if (response.status === 500) {

        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Error);
          Sentry.Native.captureException(new Error("500: " + _request));
        });

        // Sentry.Native.captureException(new Error("500: " + _request), { logger: 'sync' });

        response['request'] = _request

        return response.json()
      }
    }

    if (response.status >= 200 && response.status < 300) {
      return response.json()
    } else {
      if (response.status) {
        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Warning);
          Sentry.Native.captureException(new Error(response.status + ": " + _request));
        });
        // Sentry.Native.captureException(new Error(response.status + ": " + _request), { logger: 'sync' });
      } else {

        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Error);
          Sentry.Native.captureException(new Error("Unknown: " + _request));
        });
        // Sentry.Native.captureException(new Error("Unknown: " + _request), { logger: 'sync' });
      }


      return {
        type: 'error',
        code: response.status,
        errorCode: 'OTHER_ERROR',
        extra: JSON.stringify(response)
      }
    }
  }).then((responseJson) => {


    if (responseJson && _.isArray(responseJson) && !_.isEmpty(responseJson.map(x => x.errorCode))) {

      let msg = _.trim(_.join(responseJson.filter(x => x.errorCode).map(x => x.message)))

      const _error = {
        request: _request,
        type: 'error',
        code: 500,
        errorCode: 500,
        message: msg
      }

      // AddLog(_error)

      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(new Error(_error.message));
      });

      // Sentry.Native.captureException(new Error(_error.message), { logger: 'sync' });

      return _error
    }

    if (responseJson.type === 'error' && responseJson.statusCode === 500) {

      if (!(responseJson.errorMessage || "").startsWith("Inconsistent sequence")) {
        const _msgPrevented500 = `Response with errorMessage: ${responseJson.errorMessage}`
        const _isSoft = !responseJson.ConfigurationName && !responseJson.mobileSyncLogId

        const _error = {
          request: _request,
          type: 'error',
          code: 500,
          errorCode: 500,
          message: _msgPrevented500,
          isSoft: _isSoft
        }

        AddLog(_error)

        // Sentry.Native.captureException(new Error(_error.message), { logger: 'sync' });

        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Error);
          Sentry.Native.captureException(new Error(_error.message));
        });

        return _error
      }
    }

    if (responseJson.type === 'error' && responseJson.errorMessage) {

      const isAlreadyPushed = responseJson
        .errorMessage
        .indexOf("already pushed") >= 0

      if (isAlreadyPushed) {
        //DEPRECATED

        const _prevSyncId = responseJson.prevSyncId || 0
        return {
          type: 'warning',
          context: 'Syncing',
          message: 'SyncId was already pushed',
          nextSyncId: (_prevSyncId + 1)
        }
      }

    }

    if (responseJson.type === 'error') {
      const isInconsistentSequence = (responseJson.errorMessage || "").startsWith("Inconsistent sequence")
      if (isInconsistentSequence) {
        let _error = {
          type: 'error',
          context: 'Syncing',
          message: 'Response with type error',
          isInconsistentSequence: true,
          params: responseJson
        }

        AddLog(_error)
        let _responseJson = Object.assign({}, responseJson, { result: responseJson }, { isInconsistentSequence: true })
        return _responseJson
      }
      let _error = {
        type: 'error',
        context: 'Syncing',
        message: 'Response with type error',
        params: responseJson
      }

      AddLog(_error)
      let _responseJson = Object.assign({}, responseJson, { result: responseJson })
      return _responseJson
    }

    if (responseJson.errorMessage) {

      let _error = {
        type: 'error',
        context: 'Syncing',
        message: `Response with errorMessage: ${responseJson.errorMessage}`,
        params: responseJson
      }

      AddLog(_error)
      return _error
    }

    if (([] || responseJson.errors).length > 0) {

      let _errorsReduced = ([] || responseJson.errors).reduce((x, y) => x + y)

      let _error = {
        type: 'error',
        context: 'Syncing',
        message: `Response with errors: ${_errorsReduced}`
      }

      AddLog(_error)
      return _error
    }

    if (responseJson.previouslyReturnedResponse) {
      // console.warn("YOU SHALL PUSH CHANGES AGAIN!")
      return { type: 'duplicate', result: responseJson, prevSyncId: responseJson.prevSyncId }

    }

    if (TYPE === 'DATA') {
      if (responseJson.type === 'success') {
        return { type: 'success', result: responseJson }
      } else {

        return { type: 'error', result: responseJson }
      }
    }


    return { type: 'success', result: responseJson }
  }).catch((e) => {
    let _error = {
      type: 'error',
      context: 'Syncing',
      message: `Caught error (briefcasePost500): ${e}`
    }

    AddLog(_error)
    return _error

  })
}

const briefcasePostUpload = async (instance_url, access_token, jsonParams, instance_url_endpoint = '/services/apexrest/FIELDBUDDY/RemoteService', MediaObject) => {
  try {

    let _jsonParams = Object.assign({}, {
      ...jsonParams
    })

    let _jsonParamsLogging = Object.assign({}, {
      ...jsonParams
    })

    if(MediaObject){

      _.unset(_jsonParamsLogging, "VersionData")
    }

    

    AddLog({
      type: 'info',
      context: 'Syncing',
      message: 'Syncing ' + JSON.stringify({ instance_url: instance_url, access_token: access_token, jsonParams: _jsonParamsLogging }),
      params: {
        instance_url: instance_url,
        access_token: access_token
      }
    })

    if (!instance_url || !access_token) {
      let _error = {
        type: 'error',
        context: 'Syncing',
        message: `Missing parameters: instance_url or/and access_token`
      }
      //No Setry    
      return _error
    }

    const _endpoint_ = `${instance_url}${instance_url_endpoint}`
    

    if(MediaObject){
      MediaObject.uploading()
    }
    
    // console.warn("P " + JSON.stringify(_jsonParams))

    // console.log(`Bearer ${access_token}`)
    

    return fetch(_endpoint_, {
      method: 'POST',
      timeout: 60 * 1000,
      headers: {
        'Authorization': `Bearer ${access_token}`,
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
      },
      body: JSON.stringify(_jsonParams)
    })
      .then(checkStatusUpload)
      .then((response) => {
        // console.warn("RAW " + JSON.stringify(response))
        // TEST-CASE: return {type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID',
        // message: 'Session expired or invalid'}

        if (!response.ok && response.status === 401) {
          return { type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID', message: 'Session expired or invalid' }
        }
        if (!response.ok && response.status === 404) {
          return {
            type: 'error',
            code: 404,
            errorCode: 'NOT_FOUND',
            extra: JSON.stringify(response)
          }
        }

        if (!response.ok ){
          console.warn("RAW " + JSON.stringify(response))
          if (response.hasOwnProperty("status")) {
            return {
              type: 'error', code: response.status, errorCode: 'Error ' + response.status,
              message: 'Error ' + response.status
            }
          }
          return {
            type: 'error', code: response.message, errorCode: 'Error ' + response.message,
            message: 'Error ' + response.message
          }
        }

        
        return response.json()
      })
      .then((responseJson) => {
        //console.warn("OK " + JSON.stringify(responseJson))
        
               
        if (MediaObject) {
          if (responseJson.success) {
            MediaObject.succeeded(responseJson.id)
          } else {
            Sentry.Native.captureException(new Error("responseJson"), { logger: 'briefcasePostUpload' });
            // console.warn("Failed " + JSON.stringify(responseJson))
            // if (responseJson.code !== "400 Bad Request") {
              MediaObject.failed()
            // }
          }
        }
           
        
      
        return { type: responseJson.success ? 'success' : 'error', result: responseJson }
      })
      .catch((e) => {
        let _error = {
          type: 'error',
          context: 'briefcasePostUpload',
          message: `Caught error (briefcasePostUpload): ${e} `
        }

        if(MediaObject){
         
          _error.message += " " + JSON.stringify(_jsonParamsLogging)
        }
        Sentry.Native.captureException(new Error(_error.message), { logger: 'briefcasePostUpload' });
        
        if(MediaObject){  
          MediaObject.failed()
        }
        // console.warn("Y " + JSON.stringify(_error))
        AddLog(_error)
        return _error

      })

  } catch (e) {
    let _error = {
      type: 'error',
      context: 'briefcasePostUpload',
      message: `Globally caught error (briefcasePostUpload): ${e}`
    }
    if(MediaObject){
      _error.message += " " + JSON.stringify(_jsonParamsLogging)
    }
    Sentry.Native.captureException(new Error(_error.message), { logger: 'briefcasePostUpload' });
    // console.warn("E" + _error)
    if(MediaObject){  
      MediaObject.failed()
    }
    AddLog(_error)
    return _error
  }
}


export const Cause500 = async (sesssionID, instance_url, access_token) => {
  let jsonParams = {

    "hard500Enabled": true,
    "soft500Enabled": false,
    "resetAllSettings": false,

    "SessionId": sesssionID
  }



  return await briefcasePost500(instance_url, access_token, jsonParams)
}

export const Solve500 = async (sesssionID, instance_url, access_token) => {
  let jsonParams = {

    "hard500Enabled": false,
    "soft500Enabled": false,
    "resetAllSettings": false,

    "SessionId": sesssionID
  }



  return await briefcasePost500(instance_url, access_token, jsonParams)
}

export const GenerateLocationBasedWO = async (sesssionID, instance_url, access_token, ownerId, baseWorkOrderId) => {
  let startDate = new Date()
  var endDate = new Date();

  let startHour = startDate.getHours();
  let endHour = startHour + 1
  endDate.setHours(endHour)

  let startDateISO = startDate.toISOString()
  let endDateISO = endDate.toISOString()

  jsonParams = {
    "recordId": baseWorkOrderId, 
    "ownerId": ownerId, 
    "startDate": startDateISO, 
    "endDate": endDateISO
  }

  return await briefcaseGenerateLocationBasedWO(instance_url, access_token, jsonParams)
}

export const GetMeta = async (sesssionID, instance_url, access_token, chunk_id = 0, configuration_name = '') => {
  let jsonParams = {
    "Type": "METADATA",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },
    "ChunkId": chunk_id,
    "SessionId": sesssionID
  }

  if (configuration_name) {
    jsonParams["ConfigurationName"] = configuration_name
  }

  return await briefcasePost(instance_url, access_token, jsonParams)
}


const getActiveRing = async () => {
  try {
    const value = await AsyncStorage.getItem(ACTIVE_RING);
    if (value !== null) {
      return value === "current" ? "current" : "slow"
      // value previously stored
    }
  } catch (e) {
    // error reading value
  }

  return null
};

export const GetData = async (sesssionID, instance_url, access_token, sync_id = 0, changes = [], backendActions = [], configuration_name = '', processAsync = false, useCaughtErrorNetworkRequest = false) => {
  var __START_DATE__ = moment();
  let ring = await getActiveRing()

  let jsonParams = {
    "Type": "DATA",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },

    "SyncId": sync_id,
    "Changes": changes,
    "Actions": backendActions,
    "SessionId": sesssionID,
    "processAsync": processAsync,
    "Ring": ring,
    "SwitfSyncToken": guid()
  }

  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  console.log("SwitfSyncToken", jsonParams.SwitfSyncToken);

  if (configuration_name) {
    jsonParams["ConfigurationName"] = configuration_name
  }

  if (useCaughtErrorNetworkRequest) {
    return await briefcasePost(instance_url, access_token, jsonParams, '/services/apexrest/FIELDBUDDY/RemoteService', false, useCaughtErrorNetworkRequest)
  }

  measure(__START_DATE__, "before briefcasePost");
  let r = await briefcasePost(instance_url, access_token, jsonParams)
  measure(__START_DATE__, "after briefcasePost");
  return r;
}


export const UpdateRecords = async (sesssionID, instance_url, access_token, sync_id = 0, changes = []) => {

  console.warn("ur")
  let ring = await getActiveRing()
// changes =[
//   {
//       "changeId": "FIELDBUDDY__Work_Order__c:uniqueId1",
//       "target": "FIELDBUDDY__Work_Order__c",
//       "salesforceId": "a0jDK000007gAbPYAU",
//       "localId": "a0jDK000007gAbPYAU",
//       "changeType": "UPDATE",
//       "changeData": {
//           "FIELDBUDDY__STATUS__c": "OPEN"
//       }
//   }
// ]
  let _jsonParams = {
    "Type": "UPDATE_RECORDS",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },

    "SyncId": sync_id,
    "Changes": changes,
    "SessionId": sesssionID,

    "Ring": ring,
    "SwitfSyncToken": guid()
  }

  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  const TIMEOUT = 60 * 1000;

  try {

    
    
    

    if (!instance_url || !access_token) {
      let _error = {
        type: 'error',
        context: 'Syncing',
        message: `Missing parameters: instance_url or/and access_token`
      }
  
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Warning);
        Sentry.Native.captureException(new Error(_error));
      });
  
      // AddLog(_error)
      // Sentry.Native.captureException(new Error(_error.message), { logger: 'sync' });
  
      return _error
    }
    
    let instance_url_endpoint = '/services/apexrest/FIELDBUDDY/RemoteService'
    const _endpoint_ = `${instance_url}${instance_url_endpoint}`
   

    const headers = {
      'Authorization': `Bearer ${access_token}`,
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Swift-token': guid()
    }

    

    

    const response = await axios.post(_endpoint_, _jsonParams, {
      headers,
      timeout: TIMEOUT,

    });

    console.error('updateRecords:' + (response.data.status));

    return { type: response.data.type, result: response.data }


  } catch (error) {
    
    
    // console.error("D " + error.response.data);  
    // console.error("S " + error.response.status);  
    // console.error("H " + error.response.headers); 

    console.error('Error while updateRecords:', error.message);
    const _err =  `timeout of ${TIMEOUT}ms exceeded`
    // console.error("err " + _err)
    // e.g. timeout of 5000ms exceeded
    if(_.includes(error.message, _err)){
      
      return { type: 'error', message: error.message, code: 408  }
    }
  

    
    if(error.response && error.response.status){
    
      return { type: 'error', message: error.message, code: error.response.status  }
    }
    return { type: 'error', message: error.message, code: 404  }

  }

  return { type: 'error' }
  
}

export const GetUserConfig = async (sesssionID, instance_url, access_token) => {

  let jsonParams = {
    "Type": "USER_CONFIG",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },
    "SessionId": sesssionID
  }

  return await briefcasePost(instance_url, access_token, jsonParams)
}

export const GetLabels = async (sesssionID, instance_url, access_token) => {
  let jsonParams = {
    "Type": "LABELS",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },
    "SessionId": sesssionID
  }

  return await briefcasePost(instance_url, access_token, jsonParams)
}


export const GetAsyncJobsStatus = async (sesssionID, instance_url, access_token, jobIds = [], mobileSyncLogId = "") => {
  let jsonParams = {
    "Type": "ASYNC_STATUS",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },
    "SessionId": sesssionID,
    "jobIds": jobIds,
    "mobileSyncLogId": mobileSyncLogId
  }

  return await briefcasePost(instance_url, access_token, jsonParams)
}

export const GetAsset = async (sesssionID, instance_url, access_token, assetName, assetType = 'Document') => {
  let jsonParams = {
    "Type": "ASSET",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },
    "assetName": assetName,
    "assetType": assetType,
    "SessionId": sesssionID
  }

  return await briefcasePost(instance_url, access_token, jsonParams)
}

const _underscoreReady = (condition) => {

  if (condition) {
    if (condition.conditions) {
      for (let cond of condition.conditions) {
        cond = _underscoreReady(cond);
      }
    }
    if (condition.searchValue) {

      if (condition.searchValue.indexOf('_') >= 0) {
        condition.searchValue = _.replace(condition.searchValue, '_', "\\\\_")
      }
    }
  }
  return condition
}

export const SearchForCorrectId = async ( condition, instance_url, access_token) => {
  let jsonParams = {
    "Type": "SEARCH",  
    "Version": {
      "Major": 2, 
      "Minor": 0, 
      "Patch": 0 
    }, 
    "SyncId": 0,
    "ObjectName": "ContentVersion",
    "fieldsToRequest": ["Id","ContentDocumentId","Title"],
    "condition": condition
  }

  const result = await briefcasePost(instance_url, access_token, jsonParams)
  return result
}

export const SearchTable = async (sesssionID, instance_url, access_token, objectName, valueToSearch, fieldsToFilter, fieldsToRequest, limitOfRecords, condition, skipRecordsFromBriefcase = false) => {

  if (!_.isEmpty(condition)) {
    condition = _underscoreReady(condition)

  } else if (valueToSearch) {
    if (valueToSearch.indexOf('_') >= 0) {
      valueToSearch = _.replace(valueToSearch, '_', "\\\\_")
    }
  }

  let jsonParams = {
    "Type": "SEARCH",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },
    "objectName": objectName,
    "valueToSearch": valueToSearch,
    "fieldsToFilter": fieldsToFilter,
    "fieldsToRequest": fieldsToRequest,
    "limitOfRecords": limitOfRecords,
    "condition": condition,
    "skipRecordsFromBriefcase": skipRecordsFromBriefcase
  }

  //  //(JSON.stringify(jsonParams)) If there is a condition then we must
  // remove fields to filter. Otherwise the call doesn't return the correct data
  // from the BE
  if (!_.isEmpty(condition)) {


    _.unset(jsonParams, "fieldsToFilter")
  }
  
  result = await briefcasePost(instance_url, access_token, jsonParams)
  //console.warn("SEARCH " + JSON.stringify(jsonParams))
  //console.warn("RS: " + JSON.stringify(result))
  
  return result
}

export const RequestRelatedRecords = async (sesssionID, instance_url, access_token, objectName, relatedRecords, commonParentsPath) => {
  let jsonParams = {
    "Type": "RELATED_RECORDS",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    },
    "sObjectName": objectName,
    "RelatedRecords": relatedRecords

  }



  let params = { ...jsonParams, ...commonParentsPath }



  try {
    result = await briefcasePost(instance_url, access_token, params)
    return result
  } catch (err) {
    Sentry.Native.withScope(function (scope) {
      scope.setLevel(Sentry.Native.Severity.Warning);
      Sentry.Native.captureException(err);
    });
    // Sentry.Native.captureException(new Error("Error retrieving related records"), { logger: 'sync' });
  }

  return result
}

/* Returns aggregation, e.g.
 {
    "Type": "SEARCH",
    "Version": {"Major": 2, "Minor": 0, "Patch": 0 },
    "fieldsToRequest":["count(Id) c","Type__c"],
    "objectName": "Work_Order_Item__c",
    "skipRecordsFromBriefcase": false,
    "groupBy":["Type__c"]
  }
*/
export const Aggregate = async (sesssionID, instance_url, access_token, params) => {

  let jsonParams = Object.assign({}, {
    "Type": "SEARCH",
    "Version": {
      "Major": 2,
      "Minor": 0,
      "Patch": 0
    }
  }, {
    ...params
  })

  result = await briefcasePost(instance_url, access_token, jsonParams)

  return result
}

// {   "Title": "SpecialForGreg2.txt",   "PathOnClient": "SpecialForGreg2.txt",
// "VersionData":
// "RGV2ZWxvcGVycyBkZXZlbG9wZXJzIGRldmVsb3BlcnMgZGV2ZWxvcGVycw==",
// "FirstPublishLocationId": "a0lb0000000UovAAAS" }
export const Upload = async (sesssionID, instance_url, access_token, Title, PathOnClient, VersionData, FirstPublishLocationId, MediaObject) => {
  let jsonParams = {
    "Title": Title,
    "PathOnClient": PathOnClient,
    "VersionData": VersionData,
    "FirstPublishLocationId": FirstPublishLocationId
  }
  const instance_url_endpoint = '/services/data/v50.0/sobjects/ContentVersion'

  return await briefcasePostUpload(instance_url, access_token, jsonParams, instance_url_endpoint, MediaObject)
}

export const UploadUserPhoto = async (sesssionID, instance_url, access_token, fileName, userId, fileId) => {

  try {
    const instance_url_endpoint = `/services/data/v50.0/connect/user-profiles/${userId}/photo?fileId=${fileId}`
    const _endpoint_ = `${instance_url}${instance_url_endpoint}`
    await FileSystem.uploadAsync(
      _endpoint_,
      fileName,
      {
        "headers": {
          "Authorization": `Bearer ${access_token}`,
          "Cache-Control": "no-cache",
          'domain': instance_url
        }
      }
    )
  }
  catch (exx) {
    console.warn('exx' + exx)
  }
}

export const Download = async (sesssionID, instance_url, access_token, instance_url_endpoint) => {

  return await briefcaseGet(instance_url, access_token, instance_url_endpoint)
}

export const GetPictureByUrl = async (sesssionID, instance_url, access_token, url) => {
  try {
    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '' + s4() + '' + s4() + '' + s4() + '' + s4() + s4() + s4();
    }

    const { uri } = await FileSystem.downloadAsync(
      url,
      FileSystem.documentDirectory + guid(),
      {
        headers: {
          'Authorization': `Bearer ${access_token}`,
          'Cache-Control': 'no-cache',
          'domain': instance_url
        }
      }
    )

    return { type: 'success', result: uri }
  } catch (error) {
    return { type: 'error', result: null }
  }
}

/*
Native Salesforce API:
*/
export const GetRecentlyUploadedDocumentsByUserId = async (sesssionID, instance_url, access_token, userId, limitOfRecords = 100) => {
  try {
    const query = `services/data/v52.0/query?q=SELECT+Id,Title,LatestPublishedVersionId++from+ContentDocument+where+CreatedById=+'${userId}'+ORDER+BY+CreatedDate+DESC+LIMIT+${limitOfRecords}`
    const instance_url_endpoint = `${instance_url}/${query}`
    
    return await briefcaseGet(instance_url, access_token, instance_url_endpoint)
  } catch (error) {
    return { type: 'error', result: null }
  }
}

function checkStatus(res) {
  if (res.status >= 200 && res.status < 300) {
    return res
  } else {
    if (res.status === 401) {
      return { type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID', message: 'Session expired or invalid' }
    }
    if (res.status === 413) {
      return { type: 'error', code: 413, errorCode: 'PayloadTooLargeError', message: 'PayloadTooLargeError: request entity too large' }
    }
    let err = new Error(res.statusText)
    err.response = res
    throw err
  }
}
function checkStatusUpload(res) {
  // console.warn("RES " + JSON.stringify(res))
  if (res.status >= 200 && res.status < 300) {
    return res
  } else {
    
    Sentry.Native.captureException(new Error(res.status), { logger: 'briefcasePostUpload.checkStatusUpload' });

    if (res.status === 401) {
      return { type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID', message: 'Session expired or invalid' }
    }
    if (res.status === 413) {
      return { type: 'error', code: 413, errorCode: 'PayloadTooLargeError', message: 'PayloadTooLargeError: request entity too large' }
    }
    if (res.status === 400) {
      return { type: 'error', code: 400, errorCode: '400 Bad Request', message: '400 Bad Request' }
    }

    let err = new Error(res.statusText)
    err.response = res    
    throw err
  }
}
const briefcaseGet = async (instance_url, access_token, instance_url_endpoint) => {

  AddLog({
    type: 'info',
    context: 'Syncing',
    message: 'Syncing ' + JSON.stringify({ instance_url: instance_url, access_token: access_token }),
    params: {
      instance_url: instance_url,
      access_token: access_token
    }
  })
  if (!instance_url || !access_token) {
    let _error = {
      type: 'error',
      context: 'Syncing',
      message: `Missing parameters: instance_url or/and access_token`
    }

    // AddLog(_error)
    // Sentry.Native.captureException(new Error(_error.message), { logger: 'sync' });
    return _error
  }

  return fetch(instance_url_endpoint, {
    method: 'GET',

    headers: {
      'Authorization': `Bearer ${access_token}`,
      'Cache-Control': 'no-cache',
      'domain': instance_url
    }
  })
    .then(checkStatus)
    .then((response) => {

      //  //('Download response', response)
      if (!response.ok && response.status === 401) {
        return { type: 'error', code: 401, errorCode: 'INVALID_SESSION_ID', message: 'Session expired or invalid' }
      }

      if (response.status === 413) {
        return { type: 'error', code: 413, errorCode: 'PayloadTooLargeError', message: 'PayloadTooLargeError: request entity too large' }
      }


      return response.json()
    })
    .then((arrayOfJsons) => {
      
      if (arrayOfJsons.type === 'error') {

        let _error = {
          type: 'error',
          context: 'Fetching',
          message: 'Response with type error',
          params: arrayOfJsons
        }

        // AddLog(_error)
        let _arrayOfJsons = Object.assign({}, arrayOfJsons, { result: arrayOfJsons })
        return _arrayOfJsons
      }

/*
{
    "totalSize": 100,
    "done": true,
    "records": [
        {
            "attributes": {
                "type": "ContentDocument",
                "url": "/services/data/v52.0/sobjects/ContentDocument/0693I000000VeUHQA0"
            },
            "Id": "0693I000000VeUHQA0",
            "Title": "FieldBuddy Admin (salesforce+tso5@intigris.nl) Developer ios at 2021-12-01 20:40_backup_data.json",
            "LatestPublishedVersionId": "0683I000000VyNFQA0"
        },
        ...
    ]
}
*/
      if (arrayOfJsons) {

        if (arrayOfJsons.length > 0) {

          return { type: 'success', result: arrayOfJsons[0].data }
        }
        if (arrayOfJsons.hasOwnProperty("done") && arrayOfJsons.done) {
          if (arrayOfJsons.hasOwnProperty("records")) {
            return { type: 'success', result: arrayOfJsons.records || [] }
          }

        }

      }

      return { type: 'success', result: null }
    })
    .catch((e) => {
      
      let _error = {
        type: 'error',
        context: 'Syncing',
        message: `Caught error (briefcaseGet): ${e}`
      }

      AddLog(_error)
      
      return _error
    })
}