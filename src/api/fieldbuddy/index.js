import GetUserInfoAsync from './user/getUserInfoAsync';

import RevokeTokenAsync from './auth/revokeTokenAsync';
import RefreshAccessTokenAsync from './auth/refreshAccessTokenAsync';
import RequestAccessTokenAsync from './auth/requestAccessTokenAsync';
import SignInWithSalesforceAsync from './auth/signInWithSalesforceAsync';

import { DeleteKeyValue, GetKeyValue, MergeKeyValue, SetKeyValue, GetKeyValueStorageSize } from './store/applicationStore';
import { DeleteSessionInfo, GetSessionInfo, RefreshSessionInfo, SetSessionInfo, DeleteSessionId } from './store/sessionStore';


import {
  Cause500,
  Solve500,
  GetMeta,
  GetData,
  UpdateRecords,
  GetLabels,
  GetUserConfig,
  GetAsyncJobsStatus,
  GetAsset,
  Aggregate,
  SearchTable,
  SearchForCorrectId,
  Upload,
  UploadUserPhoto,
  Download,
  GetPictureByUrl,
  GetRecentlyUploadedDocumentsByUserId,
  RequestRelatedRecords,
  GenerateLocationBasedWO
} from './sync/briefcase'

import GoogleGeocodeApi from './google/googleGeocodeApi'
import KeenCloudApi from './keen/keenCloudApi'
import { AddLog, GetLog, DeleteLog, GetInfo } from './logging/logzApi'
import { PushLogs, PushSystemReport } from './logging/logzRemoteApi'

import {CheckSlowFastRingAsync} from './slowfast/rings'

//import {Model} from './model/Model'

module.exports = {
  GetUserInfoAsync,
  RequestRelatedRecords,
  RefreshAccessTokenAsync,
  RevokeTokenAsync,
  RequestAccessTokenAsync,
  SignInWithSalesforceAsync,
  DeleteSessionInfo,
  DeleteSessionId,
  GetSessionInfo,
  RefreshSessionInfo,
  SetSessionInfo,
  DeleteKeyValue,
  GetKeyValue,
  GetKeyValueStorageSize,
  MergeKeyValue,
  SetKeyValue,
  Cause500,
  Solve500,
  GenerateLocationBasedWO,
  GetData,
  UpdateRecords,
  GetLabels,
  GetMeta,
  GetUserConfig,
  GetAsyncJobsStatus,
  GetAsset,
  Aggregate,
  SearchTable,
  SearchForCorrectId,
  Upload,
  UploadUserPhoto,
  Download,
  GetPictureByUrl,
  GetRecentlyUploadedDocumentsByUserId,
  GoogleGeocodeApi,
  KeenCloudApi,
  AddLog,
  GetLog,
  GetInfo,
  DeleteLog,
  PushLogs,
  PushSystemReport,
  CheckSlowFastRingAsync
};