import * as FileSystem from 'expo-file-system'
import * as SecureStore from 'expo-secure-store'
import {PackageInfo} from '../../../helpers'
import {AddLog} from '../logging/logzApi'
import * as Sentry from 'sentry-expo';
import _ from 'lodash'
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function memorySizeOf(obj) {
  var bytes = 0;

  function sizeOf(obj) {
      if(obj !== null && obj !== undefined) {
          switch(typeof obj) {
          case 'number':
              bytes += 8;
              break;
          case 'string':
              bytes += obj.length * 2;
              break;
          case 'boolean':
              bytes += 4;
              break;
          case 'object':
              var objClass = Object.prototype.toString.call(obj).slice(8, -1);
              if(objClass === 'Object' || objClass === 'Array') {
                  for(var key in obj) {
                      if(!obj.hasOwnProperty(key)) continue;
                      sizeOf(obj[key]);
                  }
              } else bytes += obj.toString().length * 2;
              break;
          }
      }
      return bytes;
  };

  function formatByteSize(bytes) {
      if(bytes < 1024) return bytes + " bytes";
      else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KiB";
      else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MiB";
      else return(bytes / 1073741824).toFixed(3) + " GiB";
  };

  return formatByteSize(sizeOf(obj));
};

export const SetSessionInfo = async(obj) => {
  // / Example response: {   "access_token":
  // "00D5E0000004d2Z!ARYAQBNqxhJGnkuQYg8EFhlu9IgDx5dNcWa92AtrY_QZ4IMufwdqlIuHs0MFC
  // F6JKpir9sr8jjSJG8YRhDvDIASFY5e3xJqC",   "refresh_token":
  // "5Aep8611MDY00mymq415t1sNCt9WhNfabbA1H6.Fy_KCu9gsWAhJV0v7ZjoT7H7pKaNQHc3UHKfDo
  // RctBvHlAA5",   "signature": "fmwC7EvubxBzKV5hozUjP1RaOb6yyNI+Islf0SxqROk=",
  // "scope": "refresh_token id api full",   "id_token":
  // "eyJraWQiOiIyMTAiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiNXdrWm5p
  // ZGdaSXp2dHZPdkNReFZWZyIsInN1YiI6Imh0dHBzOi8vdGVzdC5zYWxlc2ZvcmNlLmNvbS9pZC8wME
  // Q1RTAwMDAwMDRkMlpVQVEvMDA1YjAwMDAwMDVoMFc5QUFJIiwiem9uZWluZm8iOiJFdXJvcGUvQW1z
  // dGVyZGFtIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImFkZHJlc3MiOiJ7c3RyZWV0X2FkZHJlc3M9bn
  // VsbCwgbG9jYWxpdHk9bnVsbCwgcmVnaW9uPW51bGwsIGNvdW50cnk9bnVsbCwgcG9zdGFsX2NvZGU9
  // bnVsbH0iLCJwcm9maWxlIjoiaHR0cHM6Ly9jczg0LnNhbGVzZm9yY2UuY29tLzAwNWIwMDAwMDA1aD
  // BXOUFBSSIsImlzcyI6Imh0dHBzOi8vdGVzdC5zYWxlc2ZvcmNlLmNvbSIsInByZWZlcnJlZF91c2Vy
  // bmFtZSI6ImdyemVnb3J6am9zemN6QGZpZWxkYnVkZHkuY29tLnRlc3QzLnNhbmRib3g1IiwiZ2l2ZW
  // 5fbmFtZSI6IkdyemVnb3J6IiwibG9jYWxlIjoibmxfTkwiLCJwaWN0dXJlIjoiaHR0cHM6Ly9jLmNz
  // ODQuY29udGVudC5mb3JjZS5jb20vcHJvZmlsZXBob3RvLzAwNS9GIiwiYXVkIjoiM01WRzk2bUdYZX
  // V1d1RaaDMyUUtuOFRuaF80OU1FR1dabnd4QVRmaW9LQWdVZHlHMDhlSkNsejlpTUFqWXNLVExGc3Rr
  // UHZxUmxLVG1CVmRZdGU2TSIsInVwZGF0ZWRfYXQiOiJXZWQgU2VwIDI3IDE0OjAzOjE5IEdNVCAyMD
  // E3Iiwibmlja25hbWUiOiJncnplZ29yempvc3pjeiIsIm5hbWUiOiJHcnplZ29yeiBKb3N6Y3oiLCJw
  // aG9uZV9udW1iZXIiOm51bGwsImV4cCI6MTUwNjY3MzE3NywiaWF0IjoxNTA2NjczMDU3LCJmYW1pbH
  // lfbmFtZSI6Ikpvc3pjeiIsImVtYWlsIjoiZ3J6ZWdvcnpqb3N6Y3pAZmllbGRidWRkeS5jb20ifQ.U
  // EIIaQyQ8Ov1xUvfeoVBPttMp4wHnzReWYJOa8ly3iwTezrEnvfSRsr1Tw2eFt3COT-JdUPqoSiO1Ai
  // jbsAMRMJfIXe5nG4fc2n35iCQ2YFi2W3wGtKBGjSld9IVErm9MNVeJ1uC1dghMGTsMVfWiZVrMCGnX
  // G5QzvSI82_xR6Aa-HIlH5Pp4Kr9BPeOjHLY9Sk548ytv3Sfusg9Zs_R2VHP9UXrfJW0045ZC2BkWdZ
  // pCf4MI6LGFp1AlX6HtpaClzFzz7NBoOweQopFeOnSYDQQSMsTx9fme7vrKXe-dZ7UWY7kfFluBOwjc
  // GLsf2cVmoFaDzYGZn-TAttNWTvF-NSI-_Vags0AfjalNc5m5YEPVd5Hr-cycIFf4D2Lmj6FsgSUlov
  // xy85VG4VV1TDIZmLqoqhCeeV6I16Ro_Iyh8tpYRMEKSSGJQKtQZBfnUQVtwq8OjoU9X62CjHzEZyx7
  // P_ezv99hEmg5ZQnUg4EBhG1EHBdZOqkb-FubvwQWGuVEu3x0lL0ew2aat2V2GEE2029zNb9VdHWdDx
  // fXuoyKflXVl_QuxGLIJQChWVBz6w7HN97g-6PimzRY3ksHCi5K3Pc4uGWDh6lz_vHSNABMI1AYPM7e
  // oAxA53WBemNQetx0b2CQ_IBGJntwta9g4NC7SlQ7VYqx5SKK8OA6tbEI84",
  // "instance_url": "",   "id":
  // "",
  // "token_type": "Bearer",   "issued_at": "1506673057475" }
  if (!obj) {
    let _error = {
      type: 'error',
      context: 'Session Store',
      message: `No key specified.`
    }

    Sentry.Native.withScope(function(scope) {
      scope.setLevel(Sentry.Native.Severity.Fatal);
      Sentry.Native.captureException(new Error(_error));
    });
    // AddLog(_error)
    // Sentry.Native.captureException(new Error(_error.message), {
    //   logger: 'sessionStore'
    // });
    return _error
  }
  
  const _sessionObj = Object.assign({}, {...obj}, {sessionID: guid()})

  // Check the size of each key
  // Object.keys(_sessionObj).forEach((key)=>{
  //   size = memorySizeOf(_sessionObj[key])
  //    
  // })

  // Remove the biggest one of 3.3kb
  _.unset(_sessionObj,"id_token")

  // check size 
  //size = memorySizeOf(_sessionObj)


  await SecureStore
    .setItemAsync(`FIELDBUDDY_${PackageInfo.VERSION_CODE}_SESSION`, JSON.stringify(_sessionObj))
  return await GetSessionInfo()
}

export const RefreshSessionInfo = async(new_access_token) => {
  // / Example response: {     "access_token":
  // "00D5E0000004d2Z!ARYAQB1RqFdJk_TIL81L6XnVFwfudF8zutfTMuQ5IL83U2jf9GCUyFzLkzCV1
  // SND8yeqro_.ialLWSjRd4SUcs0rrMCj2ePe",     "signature":
  // "kArRpDmYyuVqOJpFQJ7wqVAqAfayGIiDaIcfjWzig6w=",     "scope": "refresh_token
  // id api full",     "id_token":
  // "eyJraWQiOiIyMTAiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiQndXZkdN
  // c0s3MHBLaFp3LU5CX08ydyIsInN1YiI6Imh0dHBzOi8vdGVzdC5zYWxlc2ZvcmNlLmNvbS9pZC8wME
  // Q1RTAwMDAwMDRkMlpVQVEvMDA1YjAwMDAwMDVoMFc5QUFJIiwiem9uZWluZm8iOiJFdXJvcGUvQW1z
  // dGVyZGFtIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImFkZHJlc3MiOiJ7c3RyZWV0X2FkZHJlc3M9bn
  // VsbCwgbG9jYWxpdHk9bnVsbCwgcmVnaW9uPW51bGwsIGNvdW50cnk9bnVsbCwgcG9zdGFsX2NvZGU9
  // bnVsbH0iLCJwcm9maWxlIjoiaHR0cHM6Ly9jczg0LnNhbGVzZm9yY2UuY29tLzAwNWIwMDAwMDA1aD
  // BXOUFBSSIsImlzcyI6Imh0dHBzOi8vdGVzdC5zYWxlc2ZvcmNlLmNvbSIsInByZWZlcnJlZF91c2Vy
  // bmFtZSI6ImdyemVnb3J6am9zemN6QGZpZWxkYnVkZHkuY29tLnRlc3QzLnNhbmRib3g1IiwiZ2l2ZW
  // 5fbmFtZSI6IkdyZWciLCJsb2NhbGUiOiJubF9OTCIsInBpY3R1cmUiOiJodHRwczovL2MuY3M4NC5j
  // b250ZW50LmZvcmNlLmNvbS9wcm9maWxlcGhvdG8vMDA1L0YiLCJhdWQiOiIzTVZHOTZtR1hldXV3VF
  // poMzJRS244VG5oXzQ5TUVHV1pud3hBVGZpb0tBZ1VkeUcwOGVKQ2x6OWlNQWpZc0tUTEZzdGtQdnFS
  // bEtUbUJWZFl0ZTZNIiwidXBkYXRlZF9hdCI6Ik1vbiBPY3QgMjMgMTM6MjI6MzggR01UIDIwMTciLC
  // JuaWNrbmFtZSI6ImdyemVnb3J6am9zemN6IiwibmFtZSI6IkdyZWcgSm9zemN6IiwicGhvbmVfbnVt
  // YmVyIjpudWxsLCJleHAiOjE1MDkzNTUyMjUsImlhdCI6MTUwOTM1NTEwNSwiZmFtaWx5X25hbWUiOi
  // JKb3N6Y3oiLCJlbWFpbCI6ImdyemVnb3J6am9zemN6QGZpZWxkYnVkZHkuY29tIn0.IgYt_mqvHszn
  // YxUcF_nPs7bfnoLsGcu-5Qw7B0goTrpSYFEFVfw19rErd6i8i1hAnGZshZrwpE215aRwGIsjsWxzV0
  // tYSykyh6N0NKuOIDKBuC4TJ9Zlo0uQAJq-g17Rb1yxJt3kDR-4dd_oduPxCz77wlZVc1w8oYreTXkM
  // 9nFJPpc7hAzcWHP1BEdnfBbUUBk69NG35ot-LElaKSYdhdycii58LcP7DTR7v4reprM0kvhbdLZ453
  // 2T7tXjllWvZghPOppDLXDxH5Ujw2mC1SEE-08KMof_9PVv_WS7YW2oJP8R-0lYkt4d-5vSjv1vRS5j
  // l3P70CEErpxiKvBE8uq-E6-lxCKqKGA49xD9wUuWxSmIfIjXV5NSW8ESQ8EZnJSmp7U1scVt4R3hT2
  // v7R9h7fAKCHtfOBl4X82fXOIAOnxg_j3Fld_UzNqGP-u9DUq6zsaSc2dKWCAl76-R59hb8DMduX7cD
  // NVp6q8qsKOZ9-dLnQJN5t6fT55F5roOyh8Yuuk7Dis5RaSdXRCR6Hzf-3HBTvX4nBMRGAkOlgP3-ih
  // K3MmQvBarXqQYdXNTvaLvMfsnNEzmtHrPiVRHFLp6goUgLjGwUC4j_iRYIgtWumpmPTTRSKnJNLAOE
  // DIEZJvMAkey74dIWmmtZYNvLNYF8dS1n5K2xHk_5stN9xrI",     "instance_url":
  // "",     "id":
  // "",
  // "token_type": "Bearer",     "issued_at": "1509355105090"   }

  if (!new_access_token) {
    let _error = {
        type: 'error',
        context: 'Session Store',
        message: `No key specified.`
    }
    AddLog(_error)
    return _error
  }

  let currentUserInfo = await GetSessionInfo()
  if (currentUserInfo.type === 'success') {
    currentUserInfo.access_token = new_access_token

    try {
      await SecureStore
        .setItemAsync(`FIELDBUDDY_${PackageInfo.VERSION_CODE}_SESSION`, JSON.stringify(currentUserInfo))

      return {'type': 'success'}
    } catch (e) {
      
      let _error = {
        type: 'error',
        context: 'Session Store',
        sessionCode: `FIELDBUDDY_${PackageInfo.VERSION_CODE}_SESSION`,
        message: e,
        params: currentUserInfo
      }
      AddLog(_error)
      return _error
    }
  } else {
    
    let _error = {
        type: 'error',
        context: 'Session Store',
        message: 'Could not retrieve current session data.'
      }
      AddLog(_error)
      return _error
  }
}

export const GetSessionInfo = async() => {
  let raw = await SecureStore
    .getItemAsync(`FIELDBUDDY_${PackageInfo.VERSION_CODE}_SESSION`)
  
  if (raw) {
    try {
      return Object.assign({
        'type': 'success'
      }, JSON.parse(raw))
    } catch (e) {
      
      let _error = {
        type: 'error',
        context: 'Session Store',
        message: e
      }
      AddLog(_error)
      return _error
    }
  }

 let _error = {
    type: 'error',
    context: 'Session Store',
    sessionCode: `FIELDBUDDY_${PackageInfo.VERSION_CODE}_SESSION`,
    message: `Uncaught error. GetSessionInfo. FIELDBUDDY_${PackageInfo.VERSION_CODE}_SESSION`
  }

  //AddLog(_error)

  return _error
}

export const DeleteSessionInfo = async() => {
  let k = await SecureStore
    .deleteItemAsync(`FIELDBUDDY_${PackageInfo.VERSION_CODE}_SESSION`)
  return !(k && k.type === 'success')
}


export const DeleteSessionId = async() => {
  
  let k = await SecureStore
    .deleteItemAsync("newDeviceId")
    

  
  return !(k && k.type === 'success')
}