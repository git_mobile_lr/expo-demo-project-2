import moment from 'moment'
import { AsyncStorage, Platform } from 'react-native'
import * as FileSystem from 'expo-file-system'

import _ from 'lodash'
import Constants from 'expo-constants'

import { PackageInfo } from '../../../helpers'
import { AddLog } from '../logging/logzApi'
import * as Sentry from 'sentry-expo';

export const UseAsyncStorage = () => {
  return (PackageInfo.STORAGE_ENGINE && (PackageInfo.STORAGE_ENGINE || "").toLowerCase() === "AsyncStorage".toLowerCase())
}

export const UseCompressStorage = () => {
  return (PackageInfo.STORAGE_ENGINE && (PackageInfo.STORAGE_ENGINE || "").toLowerCase() === "Compress".toLowerCase())
}

export const SetKeyValue = async (key, value) => {
  if (!key) {
    let _error = {
      type: 'error',
      context: 'Application Store',
      message: `No key specified.`
    }
    AddLog(_error)
    return _error
  }

  let _key = `${PackageInfo.VERSION_CODE}_${key}`

  try {

    var __START_DATE__ = moment();

    if (UseCompressStorage()) {
      const _fileUri = FileSystem.documentDirectory + _key
      var __START_DATE2__ = moment();
      const _doc = JSON.stringify(value || {})

      measure(__START_DATE2__, `${PackageInfo.STORAGE_ENGINE} JSON.stringify ${key}`)

      await FileSystem.writeAsStringAsync(_fileUri, _doc)

    } else if (UseAsyncStorage()) {
      const result = await AsyncStorage.setItem(_key, JSON.stringify(value || {}))
    } else {
      const _fileUri = FileSystem.documentDirectory + _key
      await FileSystem.writeAsStringAsync(_fileUri, JSON.stringify(value || {}))
    }

    measure(__START_DATE__, `${PackageInfo.STORAGE_ENGINE} SetKeyValue ${key}`)

  } catch (e) {
    let _error = {
      type: 'error',
      context: 'Application Store',
      message: `Caught error. SetKeyValue: ${key}:${value}. Error: ${e}`
    }
    // AddLog(_error)
    // Sentry.captureException(new Error(_error.message), {logger: 'store'});
    Sentry.Native.withScope(function (scope) {
      scope.setLevel(Sentry.Native.Severity.Fatal);
      Sentry.Native.captureException(e);
    });

    // Sentry.Native.captureException(new Error(_error.message), {logger: 'store'});
    return _error
  }

  return { type: 'success' }
}

export const ToJSON = (key, raw) => {
  if (raw) {
    try {
      var __START_DATE__ = moment();
      let jsonContent = JSON.parse(raw)
      measure(__START_DATE__, `${PackageInfo.STORAGE_ENGINE} ToJSON() ${key}`,)
      return { type: 'success', data: jsonContent }

    } catch (parseEx) {
      let _error = {
        type: 'error',
        context: 'Application Store',
        message: `Caught error when parsing to JSON. GetKeyValue: ${key}. Error: ${parseEx}`
      }

      // AddLog(_error)
      //Sentry.captureException(new Error(_error.message), { logger: 'store' });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'store'});

      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Fatal);
        Sentry.Native.captureException(parseEx);
      });

      return _error
    }
  }

  return { type: 'success', data: {} }
}

export const measure = (__START_DATE__, info) => {

  // //(info, moment().diff(__START_DATE__, "ms"))

}

export const GetKeyValue = async (key) => {

  try {
    let _key = `${PackageInfo.VERSION_CODE}_${key}`
    var __START_DATE__ = moment();
    if (UseCompressStorage()) {
      const _fileUri = FileSystem.documentDirectory + _key
      const raw = await FileSystem.readAsStringAsync(_fileUri)
      measure(__START_DATE__, `${PackageInfo.STORAGE_ENGINE} GetKeyValue ${key}`)
      return ToJSON(key, raw)

    } else if (UseAsyncStorage()) {
      let raw = await AsyncStorage.getItem(_key)
      measure(__START_DATE__, `${PackageInfo.STORAGE_ENGINE} GetKeyValue ${key}`)
      return ToJSON(key, raw)
    } else {
      const _fileUri = FileSystem.documentDirectory + _key
      const raw = await FileSystem.readAsStringAsync(_fileUri)
      measure(__START_DATE__, `${PackageInfo.STORAGE_ENGINE} GetKeyValue ${key}`)
      const _json = ToJSON(key, raw)
      measure(__START_DATE__, `${PackageInfo.STORAGE_ENGINE} GetKeyValue toJSON ${key}`)
      return _json
    }

    let _error = {
      type: 'error',
      context: 'Application Store',
      message: `Caught error when AsyncStorage.getItem. GetKeyValue: ${key}.`
    }
    AddLog(_error)
    Sentry.Native.captureException(new Error(_error.message), { logger: 'store' });
    return _error

  } catch (asyncStorageEx) {
    let _error = {
      type: 'error',
      context: 'Application Store',
      message: `Caught error. GetKeyValue: ${key}. Error: ${asyncStorageEx}`
    }

    // AddLog(_error)
    // Sentry.captureException(new Error(_error.message), {logger: 'store'});
    // Sentry.Native.withScope(function(scope) {
    //   scope.setLevel(Sentry.Native.Severity.Warning);
    //   Sentry.Native.captureException(_error);
    // });

    // AddLog(_error)
    // Sentry.Native.captureException(new Error(_error.message), {logger: 'store'});
    return _error
  }

  let _error = {
    type: 'error',
    context: 'Application Store',
    message: `Uncaught error. GetKeyValue: ${key}.`
  }
  Sentry.Native.captureException(new Error(_error.message), { logger: 'store' });
  //AddLog(_error)

  return _error
}

export const GetKeyValueStorageSize = async (keys) => {
  let stats = []
  try {

    // const _total = await FileSystem.getInfoAsync(FileSystem.documentDirectory, {size: true})
    // if (_total.exists) {
    //   stats.push({key: 'Total', size: _total.size})
    // }
    for (let key of keys) {
      let _key = `${PackageInfo.VERSION_CODE}_${key}`
      const _fileUri = FileSystem.documentDirectory + _key
      const _file = await FileSystem.getInfoAsync(_fileUri, { size: true })
      if (_file.exists) {
        stats.push({ key: key, size: _file.size })
      }
    }
  } catch (e) { }

  return stats
}

export const DeleteKeyValue = async (key) => {

  try {
    let _key = `${PackageInfo.VERSION_CODE}_${key}`

    const _fileUri = FileSystem.documentDirectory + _key

    await FileSystem.deleteAsync(_fileUri, { idempotent: true })

    return true
    // await AsyncStorage.removeItem(_key) let k = await GetKeyValue(key) return !(k
    // && k.type === 'success')

  } catch (e) {
    let _error = {
      type: 'error',
      context: 'Application Store',
      message: `Caught error. DeleteKeyValue: ${key}. Error: ${e}`
    }

    // AddLog(_error)
    // Sentry.captureException(new Error(_error.message), { logger: 'store' });
    Sentry.Native.withScope(function (scope) {
      scope.setLevel(Sentry.Native.Severity.Fatal);
      Sentry.Native.captureException(e);
    });

    // AddLog(_error)
    // Sentry.Native.captureException(new Error(_error.message), {logger: 'store'});
    return false
  }
}