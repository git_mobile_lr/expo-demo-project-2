import {DeleteKeyValue, GetKeyValue, SetKeyValue, GetKeyValueStorageSize} from './applicationStore'
import {DeleteSessionInfo, GetSessionInfo, RefreshSessionInfo, SetSessionInfo, 
  DeleteSessionId} from './sessionStore'


module.exports = {
  DeleteSessionInfo,
  GetSessionInfo,
  RefreshSessionInfo,
  SetSessionInfo,
  
  DeleteKeyValue,
  GetKeyValue,
  SetKeyValue,
  GetKeyValueStorageSize,

  DeleteSessionId
}