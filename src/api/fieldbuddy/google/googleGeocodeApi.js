import moment from 'moment'
import jwtDecoder from 'jwt-decode';
import qs from 'qs';
import * as Sentry from 'sentry-expo';
import _ from 'lodash'
const ToQueryString = (params) => {
  return '?' + Object
    .entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
}

const GOOGLEAPIS_GEOCODE = '';
// a n,133&key=AIzaSyDM2RMe0CuNDEvupdz0xCW72NW0js9APfg

function checkStatus(res) {

  if (res.status >= 200 && res.status < 300) {
    return res
  } else {

    let err = new Error(res.statusText)
    err.response = res
    throw err
  }
}

export const measure = (__START_DATE__, info) => {
  const _period = moment().diff(__START_DATE__, "ms")
  if (_period > 1000) {
    //(info, _period)
  }

}

export default (async function googleGeocodeApi(key, address) {

  if (!key) {
    return { type: 'error', message: `No key specified.` };
  }

  if (!address) {
    return { type: 'error', message: `No address specified.` };
  }
  var __START_DATE__ = moment();
  // address = "1944NX,Creutzberglaan,133" key =
  // "AIzaSyDM2RMe0CuNDEvupdz0xCW72NW0js9APfg"
  let _url = GOOGLEAPIS_GEOCODE + ToQueryString({ key: key, address: address })

  return fetch(_url, { method: 'GET' })
    .then(checkStatus)
    .then((response) => response.json())
    .then((responseJson) => {

      measure(__START_DATE__, `googleapi`)
      if (responseJson.error) {
        //('error: ' + responseJson.error)
        let _error = {
          type: 'error',
          context: 'googleGeocodeApi',
          message: `Retrieving coordinates`,
          params: responseJson
        }

        // AddLog(_error)
        // Sentry.captureException(new Error(responseJson.error), {logger: 'googleGeocodeApi'});
        // Sentry.Native.captureException(new Error(responseJson.error), {logger: 'googleGeocodeApi'});

        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Error);
          Sentry.Native.captureException(new Error(responseJson.error), { logger: 'googleGeocodeApi' });
        });


        return { type: 'error', message: responseJson.error }
      }

      if (responseJson.error_message) {

        let _error = {
          type: 'error',
          context: 'googleGeocodeApi',
          message: `Retrieving coordinates`,
          params: responseJson
        }

        Sentry.Native.withScope(function (scope) {
          scope.setLevel(Sentry.Native.Severity.Error);
          Sentry.Native.captureException(new Error(responseJson.error), { logger: 'googleGeocodeApi' });
        });

        // AddLog(_error) Sentry.Native.captureException(new Error(_error), {logger:
        // 'googleGeocodeApi'});  //("googleGeocodeApi", key,
        // responseJson.status + " " + responseJson.error_message + " " + _url)
        // Sentry.captureException(new Error(responseJson.status + " " + responseJson.error_message + " " + _url), {logger: 'googleGeocodeApi'});
        // Sentry.Native.captureException(new Error(responseJson.status + " " + responseJson.error_message + " " + _url), {logger: 'googleGeocodeApi'});

        return {
          type: 'error',
          message: JSON.stringify(responseJson)
        }
      }
      return { type: 'success', result: responseJson }
    })
    .catch((e) => {

      // Sentry.captureException(new Error(e.message + " " + _url), {logger: 'googleGeocodeApi'});
      Sentry.Native.captureException(new Error(e.message + " " + _url), { logger: 'googleGeocodeApi' });
      return { type: 'error', message: e.message }
    })
});
