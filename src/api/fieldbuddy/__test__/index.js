import RequestAccessTokenAsync from './requestAccessTokenAsync';
import SignInWithSalesforceAsync from './signInWithSalesforceAsync';

module.exports = {
  RequestAccessTokenAsync,
  SignInWithSalesforceAsync
};