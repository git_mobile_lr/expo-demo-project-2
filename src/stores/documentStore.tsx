import { action, computed, observable, trace } from 'mobx'
import _ from 'lodash'
import * as Sentry from 'sentry-expo';

import * as DocumentPicker from 'expo-document-picker';


export enum ResultType {
    "success",
    "error"
}

export interface Result {
    type: ResultType | string
    data: any
}



/**
 * MOBX STORE - Meta Data Handler
 */

export default class DocumentStore {

    constructor(rootStore: any) {
        this.rootStore = rootStore


    }

    /**
     * Mobx Stores
     */
    rootStore: any



    async getDocumentAsync() {

        /*
         "pdf", "xlsx", "docx", "pptx", "txt", "power_point_x", "excel_x", "word_x", "text",
                   "rtf", "ods", "csv", "odp", "epub", "odt", "unknown",
                   "xml",
                   "png", "jpg", "jpeg", "gif", "bmp", "tiff", "svg", "exif", "webp", "heif", "heic", "ico",
               "json", "zip"
        */


        const { supportedFilesTypes, maxUploadFileSize } = this.rootStore.settingsStore.rules.attachments
        
        let noFilter = (_.isEmpty(supportedFilesTypes) || JSON.stringify(supportedFilesTypes) === '["*/*"]')
        
        let _selectedFileResult = noFilter ? await DocumentPicker.getDocumentAsync() :  await DocumentPicker.getDocumentAsync({ type: this._getFilterBasedOnSupportedTypes()})
        
        if (_selectedFileResult.type === 'success') {

 /*
        {
            "size": 4686,
            "mimeType": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "type": "success",
            "uri": 
            "file:///Users/grzegorzjoszcz/Library/Developer/CoreSimulator/Devices/397BF783-D734-4A38-8BC7-9B2AC4D67E07/data/Containers/Data/Application/2D695EEB-C263-45CB-A738-9E71ED629927/Library/Caches/ExponentExperienceData/%2540fieldbuddy%252Ffieldbuddyswift-test/DocumentPicker/73331364-D549-4A7B-B42A-0644B3D67F56.xlsx",
            "name": "ExampleExcel 2.xlsx"
        }
        */
            const maxUploadFileSizeInBytes = 1024 * 1024 * maxUploadFileSize

            if (_selectedFileResult.size && _selectedFileResult.size > maxUploadFileSizeInBytes) {
                return { type: "error", message: "Selected file is too large" }

            }
        }

        return _selectedFileResult
         

    }

    ifFileAnImage(file : any) {
        /*
         "pdf", "xlsx", "docx", "pptx", "txt", "power_point_x", "excel_x", "word_x", "text",
                   "rtf", "ods", "csv", "odp", "epub", "odt", "unknown",
                   "xml",
                   "png", "jpg", "jpeg", "gif", "bmp", "tiff", "svg", "exif", "webp", "heif", "heic", "ico",
               "json", "zip"
        */
        try {
            // console.warn(JSON.stringify(file))
            
            let l = ["png", "jpg", "jpeg", "gif", "bmp", "heic", "ico",
            "image/png",
            "image/jpeg",
            "image/gif",
            "image/bmp",
            "image/heic",
            "image/heif",
            "image/x-icon",
            "image/vnd.microsoft.icon"
        
        ]
            return _.includes(l, file.FileExtension.toLowerCase())
            ||
            _.includes(l, file.FileType.toLowerCase())
        } catch (error) {
            return false
        }
    }

    _getFilterBasedOnSupportedTypes() {
        const { supportedFilesTypes } = this.rootStore.settingsStore.rules.attachments
        let filter = []
        if (_.includes(supportedFilesTypes, "pdf")) {
            filter.push("application/pdf");
        }

        if (_.includes(supportedFilesTypes, "xlsx") ||
            _.includes(supportedFilesTypes, "excel_x")) {
            filter.push("application/vnd.ms-excel");
            filter.push("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        if (_.includes(supportedFilesTypes, "docx") ||
            _.includes(supportedFilesTypes, "word_x")) {
            filter.push("application/msword");
            filter.push("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }

        if (_.includes(supportedFilesTypes, "pptx") ||
            _.includes(supportedFilesTypes, "power_point_x")) {
            filter.push("application/vnd.ms-powerpoint");
            filter.push("application/vnd.openxmlformats-officedocument.presentationml.presentation");
        }

        if (_.includes(supportedFilesTypes, "txt") ||
            _.includes(supportedFilesTypes, "text")) {
            filter.push("text/plain");
        }

        if (_.includes(supportedFilesTypes, "rtf")) {
            filter.push("application/rtf");
            filter.push("text/rtf");

        }

        if (_.includes(supportedFilesTypes, "xml")) {
            filter.push("application/xml");
            filter.push("text/xml");
        }

        if (_.includes(supportedFilesTypes, "epub")) {
            filter.push("application/epub+zip");
            filter.push("application/epub");
        }

        if (_.includes(supportedFilesTypes, "odt")) {
            filter.push("application/vnd.oasis.opendocument.text");
        }

        if (_.includes(supportedFilesTypes, "odp")) {
            filter.push("application/vnd.oasis.opendocument.presentation");
        }

        if (_.includes(supportedFilesTypes, "ods")) {
            filter.push("application/vnd.oasis.opendocument.spreadsheet");
        }

        if (_.includes(supportedFilesTypes, "json")) {
            filter.push("application/json");
        }

        if (_.includes(supportedFilesTypes, "csv")) {
            filter.push("text/csv");
            filter.push("text/comma-separated-values");
            filter.push("application/csv");
        }

        if (_.includes(supportedFilesTypes, "zip")) {
            filter.push("application/zip");
        }

        if (_.includes(supportedFilesTypes, "webp")) {
            filter.push("image/webp");
        }

        if (_.includes(supportedFilesTypes, "png")) {
            filter.push("image/png");
        }
        if (_.includes(supportedFilesTypes, "jpg") || _.includes(supportedFilesTypes, "jpeg")) {
            filter.push("image/jpeg");
        }
        if (_.includes(supportedFilesTypes, "gif")) {
            filter.push("image/gif");
        }
        if (_.includes(supportedFilesTypes, "tiff")) {
            filter.push("image/tiff");
        }
        if (_.includes(supportedFilesTypes, "bmp")) {
            filter.push("image/bmp");
            filter.push("image/x-bmp");
            filter.push("image/x-ms-bmp");
        }
        if (_.includes(supportedFilesTypes, "svg")) {
            filter.push("image/svg+xml");
        }
        if (_.includes(supportedFilesTypes, "heic")) {
            filter.push("image/heic");
        }
        if (_.includes(supportedFilesTypes, "heif")) {
            filter.push("image/heif");
        }
        if (_.includes(supportedFilesTypes, "ico")) {
            filter.push("image/x-icon");
            filter.push("image/vnd.microsoft.icon");
        }

        //anything out of this default list 
        let defaultList = [
          "pdf",
          "xlsx",
          "docx",
          "pptx",
          "txt",
          "power_point_x",
          "excel_x",
          "word_x",
          "text",
          "rtf",
          "ods",
          "csv",
          "odp",
          "epub",
          "odt",
          "unknown",
          "xml",
          "png",
          "jpg",
          "jpeg",
          "gif",
          "bmp",
          "tiff",
          "svg",          
          "webp",
          "heif",
          "heic",
          "ico",
          "json",
          "zip"
        ]

        

        for(let _elem of supportedFilesTypes){
            if(! _.includes(defaultList, _elem)){
                
                filter.push(_elem);
            }
        }

        
        
        return filter.filter(x => x !== "");
    }

    async isSupportedFile(){
        
    }

    async isFile(item : any) {
        try {
            const { settingsStore } = this.rootStore
            const { rules } = settingsStore
            const { attachments } = rules
            //    console.log(JSON.stringify(attachments));

            if (item.FileType) {


                let _isFile = _.includes(attachments.supportedFilesTypes, item.FileType.toLowerCase())
                
                return _isFile
            } else {
                let _isFile = _.includes(attachments.supportedFilesTypes, item.FileType.toLowerCase())
                
                return _isFile
            }
        } catch (error) {
            //pass
            return false
        }

    }


}
