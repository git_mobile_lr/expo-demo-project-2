import {
  CheckSlowFastRingAsync
} from '../../api/fieldbuddy'
import _ from 'lodash'
class RingsApi {

  checkSlowFastRingAsync = async(postData) => {
    
    // {
    //   "success": true,
    //   "message": "",
    //   "ring": "Slow",
    //   "allowedToSwitch": true
    // }

    let result = await CheckSlowFastRingAsync(postData)

    return result
  }

}

module.exports = {
  RingsApi
}