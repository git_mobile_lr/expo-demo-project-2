import {
  AddLog,
  DeleteLog,
  GetLog,
  GetInfo,
  PushLogs,
  PushSystemReport,
  GetKeyValue,
  GetSessionInfo,
  GetUserInfoAsync
} from '../../api/fieldbuddy'
import NetInfo from '@react-native-community/netinfo';
import _ from 'lodash'


import AsyncStorage from '@react-native-async-storage/async-storage';
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING`
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING-RC`
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING-LIVE`
const rings = require('../../../rings.json')
const ACTIVE_RING = rings.activeRing

const getActiveRing = async () => {
  
  try {
    const value = await AsyncStorage.getItem(ACTIVE_RING);
  
    if (value !== null) {
      return value
      // value previously stored
    }
  } catch (e) {
    // error reading value
  }

  return null
};

class LoggingApi {

  userInfo = null

  cachedUserInfo = async() => {
    if (this.userInfo) {

      return this.userInfo

    }

    let userInfoResult = await GetKeyValue("UserInfo")
    this.userInfo = userInfoResult
    return userInfoResult

  }


  log = async(logJson) => {
    

    const _pushImmidiately = await this.pushImmidiately(logJson)

    if (_pushImmidiately.type === 'success') {
      return {type: "success", message: 'Idle'}
    } else {
      return await AddLog(logJson)
    }
  }

  getLogs = async() => {
    // return true
    return await GetLog()
  }

  deleteLogs = async() => {
    return await DeleteLog()
  }

  deleteLogsHappy = async() => {
    return DeleteLog()
  }

  getInfo = async() => {
    return await GetInfo()
  }

  pushSystemReports = async(data) => {

    let sessionInfoResult = await GetSessionInfo()
    if (sessionInfoResult.type === 'success') {
      const {id, instance_url} = sessionInfoResult

      let userInfoResult = await this.cachedUserInfo()
      if (userInfoResult.type === 'success') {
        const {
          user_id,
          organization_id,
          preferred_username,
          name,
          email,
          zoneinfo,
          user_type,
          language,
          locale
        } = userInfoResult.data

        let userBasic = {
          user_id: user_id,
          organization_id: organization_id,
          preferred_username: preferred_username,
          name: name,

          email: email,
          zoneinfo: zoneinfo,
          user_type: user_type,
          language: language,
          locale: locale,
          salesforce_id: id,
          salesforce_instance_url: instance_url
        }

        return await PushSystemReport(data, userBasic)
      }
    }
  }

  pushImmidiately = async(log) => {

    let _log = {}
    _log.log = _.clone(log)

    let allLogs = []
    allLogs.push(_log)

    let sessionInfoResult = await GetSessionInfo()

    if (sessionInfoResult.type === 'success') {
      const {id, instance_url, sessionID} = sessionInfoResult

      let userInfoResult = await this.cachedUserInfo()
      // let userInfoResult = await GetKeyValue("UserInfo")
      let activeRing = await getActiveRing()

      if (userInfoResult.type === 'success') {
        const {
          user_id,
          organization_id,

          preferred_username,
          name,
          email,
          zoneinfo,
          user_type,
          language,
          locale
        } = userInfoResult.data

        let userBasic = {
          user_id: user_id,
          organization_id: organization_id,
          session_id: sessionID,
          preferred_username: preferred_username,
          name: name,

          email: email,
          zoneinfo: zoneinfo,
          user_type: user_type,
          language: language,
          locale: locale,
          salesforce_id: id,
          salesforce_instance_url: instance_url,
          activeRing: activeRing === "current" ? "current" : "slow"
        }


        const _result = await PushLogs(allLogs, userBasic, false)

        return _result
      }
    }

    return {type: 'success', message: 'Idle'}
  }

  pushLogs = async() => {
    

    let allLogsResult = await GetLog()

    if (allLogsResult.type === 'success') {

      let sessionInfoResult = await GetSessionInfo()

      if (sessionInfoResult.type === 'success') {
        const {id, instance_url} = sessionInfoResult

        let userInfoResult = await this.cachedUserInfo()
        let activeRing = await getActiveRing()

        if (userInfoResult.type === 'success') {
          const {
            user_id,
            organization_id,
            preferred_username,
            name,
            email,
            zoneinfo,
            user_type,
            language,
            locale
          } = userInfoResult.data

          let allLogs = allLogsResult.data
          let userBasic = {
            user_id: user_id,
            organization_id: organization_id,
            preferred_username: preferred_username,
            name: name,

            email: email,
            zoneinfo: zoneinfo,
            user_type: user_type,
            language: language,
            locale: locale,
            salesforce_id: id,
            salesforce_instance_url: instance_url,
            activeRing: activeRing === "current" ? "current" : "slow"
          }

          return await PushLogs(allLogs, userBasic)
        }
      }
    }

    return {type: 'success', message: 'Idle'}
  }
}

module.exports = {
  LoggingApi
}