import {Platform} from 'react-native'
import _ from 'lodash'
import {
  GoogleGeocodeApi
} from '../../api/fieldbuddy'

import {PackageInfo} from '../../helpers'

class GoogleApi {

  getGeocoordinates = async(address) => {
    
    const _GOOGLE_API_KEY = Platform.OS === 'ios' ? PackageInfo.GOOGLE_API_IOS : PackageInfo.GOOGLE_API_ANDROID
    let getDataResult = await GoogleGeocodeApi(_GOOGLE_API_KEY, address)
    
    if (getDataResult) {
      if (getDataResult.type === 'success') {

        return {type: getDataResult.type, data: getDataResult.result}


      } else if (getDataResult.type === 'error') {
        
      }
      return {type: 'error', message: 'Unsupported result for GoogleApi request.'}
    }

    return {type: 'error', message: 'GoogleApi request failed.'}
  }

}

module.exports = {
  GoogleApi
}