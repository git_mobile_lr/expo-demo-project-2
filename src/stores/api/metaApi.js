import * as Sentry from 'sentry-expo';
import { AddLog } from './loggingApi'
import _ from 'lodash'
class MetaApi {

  getMetaFullTable(META, tableName) {
    try {
      if (!META || !tableName) {
        return null
      }
      return META[tableName]

    } catch (e) {
      let _error = {
        type: 'error',
        context: 'Meta API',
        message: `Caught error. getMetaFullTable tableName: ${tableName} Error: ${e}`
      }
      AddLog(_error)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'meta_api'});
      return _error

    }
  }

  getMetaPrefix(META) {
    try {
      if (!META) {
        return ""
      }

      const _tableNames = Object.getOwnPropertyNames(META)
      const _sum = _tableNames.map(x => {
        return _.includes(x, "FIELDBUDDY__")
      })
      //  //(_sum)
      return _.sum(_sum) > 0
        ? "FIELDBUDDY__"
        : ""

    } catch (e) {
      let _error = {
        type: 'error',
        context: 'Meta API',
        message: `Caught error. getMetaFields tableName: ${tableName} Error: ${e}`
      }
      AddLog(_error)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'meta_api'});
      return ""

    }
  }

  getFormat(_m) {
    const { createable, accessible, updateable, deletable } = _m
    let _createable = createable
      ? 'true'
      : "*FALSE*"
    let _accessible = accessible
      ? 'true'
      : "*FALSE*"
    let _updateable = updateable
      ? 'true'
      : "*FALSE*"
    let _deletable = deletable
      ? 'true'
      : "*FALSE*"

    return `\tC: ${_createable}\tR: ${_accessible}\tU: ${_updateable}\tD: ${_deletable}\n`
  }
  getPermissions(META) {

    try {
      const _keys = _.keys(META)

      let _v = `\nPermissions:\n`
      for (let _key of _keys) {
        const _m = META[_key]

        let _p = `${_key}:\n`
        _p += this.getFormat(_m)
        _v += _p

      }
      _v += `\nC:\tcreateable\tR:\taccessible\tU:\tupdateable\tD:\tdeletable`
      return _v
    } catch (error) {
      return "Couldn't receive Permissions..."
    }
  }

  getMetaFields(META, tableName) {

    try {
      if (!META || !tableName) {
        return null
      }
      return META[tableName].fields

    } catch (e) {
      let _error = {
        type: 'error',
        context: 'Meta API',
        message: `Caught error. getMetaFields tableName: ${tableName} Error: ${e}`
      }
      AddLog(_error)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'meta_api'});
      return _error

    }
  }

  getMetaRecordTypes(META, tableName) {
    try {
      if (!META || !tableName) {
        return null
      }
      return META[tableName].recordtypes

    } catch (e) {
      let _error = {
        type: 'error',
        context: 'Meta API',
        message: `Caught error. getMetaRecordTypes tableName: ${tableName} Error: ${e}`
      }
      AddLog(_error)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'meta_api'});
      return _error

    }
  }

  getMetaPerField(META, tableName, fieldName) {
    try {

      let _fieldMeta = META[tableName]
        .fields
        .filter(x => x.name === fieldName)[0]

      return _fieldMeta
    } catch (e) {
      let _error = {
        type: 'error',
        context: 'Meta API',
        message: `Caught error. getMetaPerField tableName: ${tableName} fieldName: ${fieldName} Error: ${e}`
      }
      AddLog(_error)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'meta_api'});
      return _error

    }
  }

  getTables(META) {
    return Object
      .getOwnPropertyNames(META)
      .filter(t => {
        return !(t === "$mobx")
      })
    // .filter(t => {   return !(t === "FIELDBUDDY__Timesheet__c" || t ===
    // "FIELDBUDDY__Timesheet_Lineitem__c" || t === "$mobx") })
  }

  getTablesStringified(META) {
    return JSON.parse(JSON.stringify(this.getTables(META)))
  }

  cleanEmpty(META, tableName, originalRecord) {
    try {
      let that = this
      let record = originalRecord

      let _fields = META[tableName].fields

      let presentProps = Object
        .getOwnPropertyNames(record)
        .filter(x => x.name != "attributes")

      let expectedProps = Object
        .values(_fields)
        .map(x => x.name)

      expectedProps.forEach(x => {
        
        try {
          if (typeof (record[x]) !== 'number') {

            if (!record[x]) {
              record[x] = ""
            }
          }
        } catch (error) {
          //pass
          
        }

      })

      return record

    } catch (e) {

      let _error = {
        type: 'error',
        context: 'Meta API',
        message: `Caught error. cleanEmpty tableName: ${tableName} fieldName: ${fieldName} Error: ${e}`
      }
      AddLog(_error)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'meta_api'});

      return originalRecord

    }
  }
}

module.exports = {
  MetaApi
}