import {
  DeleteKeyValue,
  GetKeyValue,
  Cause500,
  Solve500,
  GenerateLocationBasedWO,
  GetData,
  UpdateRecords,
  GetLabels,
  GetMeta,
  GetUserConfig,
  GetAsyncJobsStatus,
  GetUserInfoAsync,
  GetAsset,
  Aggregate,
  SearchTable,
  Upload,
  Upload2,
  UploadUserPhoto,
  SearchForCorrectId,
  RequestRelatedRecords,
  GetSessionInfo,
  RefreshSessionInfo,

  RefreshAccessTokenAsync,
  RevokeTokenAsync,

  SignInWithSalesforceAsync,
  GetPictureByUrl,
  GetRecentlyUploadedDocumentsByUserId,
  SetKeyValue
} from '../../api/fieldbuddy'
import _ from 'lodash'
import { LoggingApi } from './loggingApi'
import { PackageInfo } from '../../helpers'
import { Download } from '../../api/fieldbuddy/sync/briefcase';
import { Platform } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import * as Sentry from 'sentry-expo';
import moment from 'moment'
// import * as Sentry from 'sentry-expo';
import { reduxStore } from '../../App'
import { ReduxActions } from '../../types';


class SyncApi {

  measure = (__START_DATE__, info) => {
    // console.warn(info, moment().diff(__START_DATE__, "ms"))
  }

  isOfflineEnough = async () => {



    NetInfo.fetch().then(state => {
      if (state.type == 'none') {
        return false
      }
    });

    //  //('Initial, type: ' + connectionInfo.type + ', effectiveType: ' +
    // connectionInfo.effectiveType)
    // if (connectionInfo.type === 'none') {
    //   //|| connectionInfo.effectiveType === '2g') {
    //   return false
    // }

    // if (Platform.OS === 'android' && (connectionInfo.type === 'bluetooth' ||
    // connectionInfo.effectiveType === '3g')) {   return false } const


    return true
  }

  cause500 = async () => {
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()
    await Cause500(sessionID, instance_url, access_token)
  }

  solve500 = async () => {
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()
    await Solve500(sessionID, instance_url, access_token)
  }

  generateLocationBasedWO = async (ownerId, basedWorkOrderId) => {
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()
    
    let getResult = await GenerateLocationBasedWO(sessionID, instance_url, access_token, ownerId, basedWorkOrderId)

    if (getResult) {
      if (getResult.type === 'success') {
        return { type: getResult.type, data: getResult.result }
      } else if (getResult.type === 'error') {
        if (getResult.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {
            return this.generateLocationBasedWO()
          }
        }
      }
      return { type: 'error', message: 'Unsupported result' }
    }

    return { type: 'error', message: 'Synchronization failed.' }
  }

  fetchMeta = async (chunk_id) => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let getDataResult = await GetMeta(sessionID, instance_url, access_token, chunk_id)

    if (getDataResult) {
      if (getDataResult.type === 'success') {
        return { type: getDataResult.type, data: getDataResult.result }
      } else if (getDataResult.type === 'error') {
        if (getDataResult.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {
            return this.fetchMeta()
          }
        }
      }
      return { type: 'error', message: 'Unsupported result for metadata synchronization.' }
    }

    return { type: 'error', message: 'Metadata synchronization failed.' }
  }

  fetchUserInfo = async () => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let _userInfo = await GetUserInfoAsync(instance_url, access_token)

    if (_userInfo) {

      if (_userInfo.type === 'success') {

        let keyValueResult = await SetKeyValue("UserInfo", _userInfo.result);
        if (keyValueResult.type === 'success') {
          return { type: 'success', data: _userInfo.result }
        }

      } else if (_userInfo.type === 'error') {

        if (_userInfo.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return await this.fetchUserInfo()
          }
        }

        return { type: 'error', message: 'Invalid user config defined.' }
      }

    }
    return { type: 'error', message: 'No user config defined.' }
  }

  fetchUserConfig = async () => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let _userConfig = await GetUserConfig(sessionID, instance_url, access_token)

    if (_userConfig) {

      if (_userConfig.type === 'success') {
        if (_userConfig.result && _userConfig.result.ScreenData && _userConfig.result.ScreenData.sample) {

          return { type: 'success', data: _userConfig.result.ScreenData.sample, configurationName: _userConfig.result.ConfigurationName }
        }
      } else if (_userConfig.type === 'error') {

        if (_userConfig.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return this.fetchUserConfig()
          }
        }

        return { type: 'error', message: 'Invalid user config defined.' }
      }

    }
    return { type: 'error', message: 'No user config defined.' }
  }

  fetchAsyncJobsStatus = async (jobIds = [], mobileSyncLogId= "") => {
    
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let _asyncJobsStatus = await GetAsyncJobsStatus(sessionID, instance_url, access_token, jobIds, mobileSyncLogId)

    if (_asyncJobsStatus) {

      if (_asyncJobsStatus.type === 'success') {
        
        /*
        {
    "type": "success",
    "result": {
        "timeStamp": 1618408310274,
        "statusCode": 200,
        "errorMessage": null,
        "jobStatus": {
            "7075p0000DEDLmDAQX": "Completed"
        }
    }
}
        */
        if (_asyncJobsStatus.result && _asyncJobsStatus.result.jobStatus) {
        

          return { type: 'success', data: _asyncJobsStatus.result.jobStatus }

        }
      } else if (_asyncJobsStatus.type === 'error') {

        if (_asyncJobsStatus.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return this.fetchAsyncJobsStatus(jobIds, mobileSyncLogId)
          }
        }

        return { type: 'error', message: 'Invalid async jobs called.' }
      }

    }
    return { type: 'error', message: 'No async jobs called.' }
  }

  fetchLabels = async () => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let _labels = await GetLabels(sessionID, instance_url, access_token)

    if (_labels) {

      if (_labels.type === 'success') {
        if (_labels.result && _labels.result.labels) {

          return { type: 'success', data: _labels.result.labels }
        }

      } else if (_labels.type === 'error') {
        if (_labels.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)
          if (refetchTokenResult.type === 'success') {
            return this.fetchLabels()
          }
        }
        return { type: 'error', message: `Couldn't retrieve LABELS.` }
      }
    }

    return { type: 'error', message: `Couldn't retrieve LABELS.` }
  }



  fetchAsset = async (assetName, assetType = 'Document') => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let asset = await GetAsset(sessionID, instance_url, access_token, assetName, assetType)

    if (asset) {
      if (asset.type === 'success') {

        return { type: 'success', data: asset.result }

      } else if (asset.type === 'error' || asset.errorMessage) {

        if (asset.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return this.fetchAsset(assetName, assetType)
          }
        }

        return { type: 'error', message: 'fetchAsset: Invalid asset defined.' }
      }

    }
    return { type: 'error', message: 'No asset retrieved.' }
  }

  fetchStatistics = async (params) => {
    try {
      const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()
      let aggregationResult = await Aggregate(sessionID, instance_url, access_token, params)
      if (aggregationResult.type === 'success') {
        return { type: 'success', data: aggregationResult.result.aggregatedData }
      }
      // [   {     "Type__c": "Activity",     "stats": 29   },   {     "Type__c":
      // "Part",     "stats": 16   },   {     "Type__c": "Other expense",     "stats":
      // 3   } ]
      return aggregationResult;
    } catch (error) {
      return { type: 'error', result: [] }
    }

  }

  requestRelatedRecords = async (objectName, relatedRecords, commonParentPaths) => {
    try {
      const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()
      if (!_.isEmpty(sessionID) && !_.isEmpty(instance_url) && !_.isEmpty(access_token) && !_.isEmpty(objectName) & !_.isEmpty(relatedRecords) & !_.isEmpty(commonParentPaths)) {
        let search = await RequestRelatedRecords(sessionID, instance_url, access_token, objectName, relatedRecords, commonParentPaths)

        if (search) {
          if (search.type === 'success') {

            return { type: 'success', data: search.result }

          } else if (search.type === 'error' || search.errorMessage) {

            if (search.code === 401) {
              let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

              if (refetchTokenResult.type === 'success') {

                return this.requestRelatedRecords(objectName, relatedRecords, commonParentPaths)
              }
            }

            return { type: 'error', message: 'requestRelatedRecords failed.' }
          }

        }
        return { type: 'error', message: 'requestRelatedRecords failed.' }
      } else {
        return { type: "error" }
      }
    } catch (err) {

    }
  }

  searchForCorrectId = async (condition, Id) => {
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let resultId = await SearchForCorrectId(condition, instance_url, access_token)

    if (resultId) {
      if (resultId.type === "success") {

        return { type: resultId.type, data: resultId["result"]["searchData"][Id]["ContentDocumentId"] }

      } else if (resultId.type === 'error' || search.errorMessage) {
        if (resultId.code === 401) {

          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return this.searchForCorrectId(condition, Id)

          }
        }
        return { type: result.type, message: 'searchForCorrectId failed.' }
      }
    }

    return { type: result.type, message: 'searchForCorrectId failed.' }
  }

  searchTable = async (objectName, valueToSearch, fieldsToFilter = ['Name'], fieldsToRequest = [
    'Id', 'Name', 'OwnerId'
  ], limitOfRecords = 10, condition, skipRecordsFromBriefcase = false) => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }

    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let search = await SearchTable(sessionID, instance_url, access_token, objectName, valueToSearch, fieldsToFilter, fieldsToRequest, limitOfRecords, condition, skipRecordsFromBriefcase)

    if (search) {
      if (search.type === 'success') {

        return { type: 'success', data: search.result }

      } else if (search.type === 'error' || search.errorMessage) {

        if (search.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return this.searchTable(objectName, valueToSearch, fieldsToFilter, fieldsToRequest, limitOfRecords)
          }
        }

        return { type: 'error', message: 'searchTable failed.' }
      }

    }
    return { type: 'error', message: 'searchTable failed.' }
  }

  upload = async (Title, PathOnClient, VersionData, FirstPublishLocationId, MediaObject) => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }

    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let uploadResult = await Upload(sessionID, instance_url, access_token, Title, PathOnClient, VersionData, FirstPublishLocationId, MediaObject)

    if (uploadResult) {
      if (uploadResult.type === 'success') {

        return { type: 'success', data: uploadResult.result }

      } else if (uploadResult.type === 'error' || uploadResult.errorMessage) {

        if (uploadResult.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return await this.upload(Title, PathOnClient, VersionData, FirstPublishLocationId, MediaObject)
          }
        }

        return { type: 'error', message: `Upload failed: ${uploadResult}` }

      }

    }

    return { type: 'error', message: `Upload failed: ${uploadResult}` }
  }

  upload2 = async (Title, PathOnClient, VersionData, FirstPublishLocationId) => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }

    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let uploadResult = await Upload2(sessionID, instance_url, access_token, Title, PathOnClient, VersionData, FirstPublishLocationId)

    if (uploadResult) {
      if (uploadResult.type === 'success') {

        return { type: 'success', data: uploadResult.result }

      } else if (uploadResult.type === 'error' || uploadResult.errorMessage) {

        if (uploadResult.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return this.upload2(Title, PathOnClient, VersionData, FirstPublishLocationId)
          }
        }

        return { type: 'error', message: `Upload failed: ${uploadResult}` }

      }

    }

    return { type: 'error', message: `Upload failed: ${uploadResult}` }
  }

  uploadUserPhoto = async (fileName, userId, fileId) => {
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }

    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let uploadResult = await UploadUserPhoto(sessionID, instance_url, access_token, fileName, userId, fileId)

    if (uploadResult) {
      if (uploadResult.type === 'success') {

        return { type: 'success', data: uploadResult.result }

      } else if (uploadResult.type === 'error' || uploadResult.errorMessage) {

        if (uploadResult.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)
          if (refetchTokenResult.type === 'success') {
            return this.uploadUserPhoto(fileName, userId, fileId)
          }
        }

        return { type: 'error', message: `Upload photp failed: ${uploadResult}` }
      }
    }
    
    return { type: 'error', message: `Upload failed: ${uploadResult}` }
  }
    

  download = async (instance_url_endpoint) => {



    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let downloadResult = await Download(sessionID, instance_url, access_token, instance_url_endpoint)

    if (downloadResult) {

      if (downloadResult.type === 'success') {

        return { type: 'success', data: downloadResult.result }

      } else if (downloadResult.type === 'error' || downloadResult.errorMessage) {

        if (downloadResult.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return await this.download(instance_url_endpoint)
          }
        }

        return { type: 'error', message: 'Invalid asset defined.' }
      }

    }
    return { type: 'error', message: 'No asset retrieved.' }
  }

  getPictureByUrl = async (url) => {

    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let downloadResult = await GetPictureByUrl(sessionID, instance_url, access_token, url)

    if (downloadResult) {

      if (downloadResult.type === 'success') {

        return { type: 'success', data: downloadResult.result }

      } else if (downloadResult.type === 'error' || downloadResult.errorMessage) {

        if (downloadResult.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return await this.getPictureByUrl(url)
          }
        }

        return { type: 'error', message: 'Invalid asset defined.' }
      }

    }
    return { type: 'error', message: 'No asset retrieved.' }
  }

  getRecentlyUploadedDocumentsByUserId = async (userId, limitOfRecords) => {
    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let recentlyUploadedResultSet = await GetRecentlyUploadedDocumentsByUserId(sessionID, instance_url, access_token, userId, limitOfRecords)
    
    if (recentlyUploadedResultSet) {

      if (recentlyUploadedResultSet.type === 'success') {

        return { type: 'success', data: recentlyUploadedResultSet.result }

      } else if (recentlyUploadedResultSet.type === 'error' || recentlyUploadedResultSet.errorMessage) {

        if (recentlyUploadedResultSet.code === 401) {
          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)

          if (refetchTokenResult.type === 'success') {

            return await this.getRecentlyUploadedDocumentsByUserId(userId, limitOfRecords)
          }
        }

        return { type: 'error', message: 'Invalid asset defined.' }
      }

    }
    return { type: 'error', message: 'No recently uploaded documents retrieved.' }
  }

  revokeToken = async () => {
    const { instance_url, access_token, refresh_token } = await GetSessionInfo()

    let revokeAccessTokenResult = await RevokeTokenAsync(instance_url, access_token)

    let revokeRefreshTokenResult = await RevokeTokenAsync(instance_url, refresh_token)

    if (revokeAccessTokenResult.type === 'success' && revokeRefreshTokenResult.type === 'success') {

      return { type: 'success' }

    }

    return { type: 'error', message: 'Revoking token failed.' }

  }

  refetchToken = async (sessionID, instance_url, access_token, refresh_token) => {
    let refreshTokenResponse = await RefreshAccessTokenAsync(sessionID, instance_url, access_token, PackageInfo.CLIENT_ID, PackageInfo.CLIENT_SECRET, refresh_token)

    if (refreshTokenResponse.type === 'success') {
      const { result } = refreshTokenResponse

      let storeNewAccessToken = await RefreshSessionInfo(result.access_token)
      //re-try

      if (storeNewAccessToken.type === 'success') {
        return { type: 'success' }
      }
      //ERROR

    }

    return { type: 'error', message: 'Refreshing token failed.' }
  }

  

  syncData = async (syncId = 0, changes = [], backendActions = [], configuration_name = "", processAsync = false, useCaughtErrorNetworkRequest = false) => {
    var __START_DATE__ = moment();
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }

    //('syncId:', syncId)
    try {
      const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()
      this.measure(__START_DATE__, 'syncData 1')
      let getDataResult = await GetData(sessionID, instance_url, access_token, syncId, changes, backendActions, configuration_name, processAsync, useCaughtErrorNetworkRequest)

      this.measure(__START_DATE__, 'syncData 2')
      if (getDataResult) {
        if (getDataResult.type === 'success') {
          try {

            // // Set Platform Status
            // reduxStore.dispatch({
            //   type: ReduxActions.SET_PLATFORM_STATUS,
            //   payload: getDataResult?.result?.platformStatus
            // })

            if (processAsync) {

              return { type: getDataResult.type, asyncJobIds: getDataResult.result, mobileSyncLogId: getDataResult.mobileSyncLogId }
            }
            let hiddenErrorCodes = getDataResult
              .result
              .map(x => {
                return { error: x.errorCode, message: x.message }
              })

            if (hiddenErrorCodes && hiddenErrorCodes.length > 0) {
              return { type: 'error', message: hiddenErrorCodes[0].message }
            }
          } catch (e) { }

          this.measure(__START_DATE__, 'syncData 3')
          return { type: getDataResult.type, data: getDataResult.result }
        } else if (getDataResult.type === 'error') {



          if (_.includes([401, 403, 404], getDataResult.code)) {
            // Sentry.Native.withScope(function (scope) {
            //   scope.setLevel(Sentry.Native.Severity.Warning);
            //   Sentry.Native.captureException(new Error("SyncData warning " + getDataResult.code));
            // });
          } else {
            Sentry.Native.withScope(function (scope) {
              scope.setLevel(Sentry.Native.Severity.Error);
              Sentry.Native.captureException(new Error("SyncData error " + getDataResult.code));
            });
          }




          if (getDataResult.code === 401) {

            let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)
            if (refetchTokenResult.type === 'success') {
              return await this.syncData(syncId, changes, backendActions, configuration_name, processAsync, useCaughtErrorNetworkRequest)
            } else {

              // refetchToken
              // message: "invalid_grant"
              return {
                type: 'error', message: 'Token failed.', code: 401,
                errorCode: "invalid_grant"
              }
            }
          }

        } else if (getDataResult.type === 'warning') {

          return getDataResult;
          // return this.syncData(getDataResult.nextSyncId, changes, backendActions)

        }

        // Sentry.Native.withScope(function (scope) {
        //   scope.setLevel(Sentry.Native.Severity.Warning);
        //   Sentry.Native.captureException(new Error("SyncData error or partialError"));
        // });

        if (!_.isEmpty(getDataResult.rejected)) {
          return { type: 'partialError', message: 'Some changes have been accepted.', accepted: getDataResult.accepted, data: getDataResult.result }

        }


        return getDataResult

      }

      return getDataResult
    } catch (err) {


      return { type: 'error', message: err.message }
    }
  }


  updateRecords = async (syncId = 0, changes = []) => {
        
    const isOfflineEnough = await this.isOfflineEnough()
    if (!isOfflineEnough) {
      return { type: 'offline' }
    }

    const { sessionID, instance_url, access_token, refresh_token } = await GetSessionInfo()

    let getDataResult = await UpdateRecords(sessionID, instance_url, access_token,
      syncId, changes)
      
    if (getDataResult) {
      if (getDataResult.type === 'success') {
        try {

          // // Set Platform Status
          // reduxStore.dispatch({
          //   type: ReduxActions.SET_PLATFORM_STATUS,
          //   payload: getDataResult?.result?.platformStatus
          // })

          if (processAsync) {

            return { type: getDataResult.type, asyncJobIds: getDataResult.result, mobileSyncLogId: getDataResult.mobileSyncLogId }
          }
          let hiddenErrorCodes = getDataResult
            .result
            .map(x => {
              return { error: x.errorCode, message: x.message }
            })

          if (hiddenErrorCodes && hiddenErrorCodes.length > 0) {
            return { type: 'error', message: hiddenErrorCodes[0].message }
          }
        } catch (e) { }


        return { type: getDataResult.type, data: getDataResult.result }
      } else if (getDataResult.type === 'error') {



        if (getDataResult.code === 401) {

          let refetchTokenResult = await this.refetchToken(sessionID, instance_url, access_token, refresh_token)
          if (refetchTokenResult.type === 'success') {
            return await this.updateRecords(syncId, changes)
          } else {

            // refetchToken
            // message: "invalid_grant"
            return {
              type: 'error', message: 'Token failed.', code: 401,
              errorCode: "invalid_grant"
            }
          }
        }

      }

      
      return getDataResult

    }
    
    
    return getDataResult

  }

}

module.exports = {
  SyncApi
}