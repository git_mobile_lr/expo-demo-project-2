import {
  DeleteKeyValue,
  GetKeyValue,
  SetKeyValue,
  DeleteSessionInfo,
  GetSessionInfo,
  GetKeyValueStorageSize,
  DeleteSessionId
} from '../../api/fieldbuddy'
import _ from 'lodash'
import {AddLog} from './loggingApi'
import {PackageInfo} from '../../helpers'
import * as FileSystem from 'expo-file-system'
import {Dimensions, Platform} from 'react-native'
import Constants from 'expo-constants'
import * as Sentry from 'sentry-expo';
const IsIOS = () => {
  return Platform.OS === 'ios'
}
class StorageApi {

  constructor() {
    this.USER_CONFIG = `@FIELDBUDDY-${PackageInfo.VERSION_CODE}:USER_CONFIG`
    this.LABELS = `@FIELDBUDDY-${PackageInfo.VERSION_CODE}:LABELS`
    this.META = `@FIELDBUDDY-${PackageInfo.VERSION_CODE}:META`
    this.RELATIONSHIPS = `@FIELDBUDDY-${PackageInfo.VERSION_CODE}:RELATIONSHIPS`
    this.DATA = `@FIELDBUDDY-${PackageInfo.VERSION_CODE}:DATA`
    this.LOGIN_STORAGE = `@FIELDBUDDY-${PackageInfo.VERSION_CODE}:LOGINSTORAGE`
    this.FIELDBUDDYSETTINGS = `@FIELDBUDDY-${PackageInfo.VERSION_CODE}:FIELDBUDDYSETTINGS`
  }

  async ERROR(e, c) {
    let _error = {
      type: 'error',
      context: 'storage',
      message: e
    }

    //  AddLog(_error) Sentry.Native.captureException(new Error(_error.message), {logger:
    // 'storage_api', context: c}); (`%c${JSON.stringify(_error)}`, "background:
    // '#F00'; color:'#FFF'")
    return _error
  }

  clearData = async() => {
    try {
      console.warn("clearing data...")
      const userConfigResult = this.deleteUserConfig()
      const labelsResult = this.deleteLabels()
      const metaResult = this.deleteMeta()
      const relationshipsResult = this.deleteRelationships()
      const dataResult = this.deleteDataStorage()
      const loginStorageResult = this.deleteLoginStorage()
      const fieldbuddySettingResult = this.deleteFieldbuddySettings();

      const userInfoResult = this.deleteUserInfo()
      const sessionInfo = this.deleteSessionInfo()
      
      const sessionId = this.deleteSessionId()


      const [_uc,
        _l,
        _m,
        _r,
        _d,
        _ls,
        _fd,

        _ui,
        _si, _sid] = await Promise.all([
        userConfigResult,
        labelsResult,
        metaResult,
        relationshipsResult,
        dataResult,
        loginStorageResult,
        fieldbuddySettingResult,

        userInfoResult,
        sessionInfo,
        sessionId
      ])

      return {
        type: 'success',
        result: _uc && _l && _m && _r && _d && _ls && _fd && _ui && _si && _sid
      }
    } catch (e) {
      return this.ERROR(e, "clearData")

    }

  }

 

  clearStorage = async() => {
    try {
      console.warn("clearing storage...")
      const userConfigResult = this.deleteUserConfig()
      const labelsResult = this.deleteLabels()
      const metaResult = this.deleteMeta()
      const relationshipsResult = this.deleteRelationships()
      const dataResult = this.deleteDataStorage()
      const loginStorageResult = this.deleteLoginStorage()
      const fieldbuddySettingResult = this.deleteFieldbuddySettings()

      const sessionInfoResult = this.deleteSessionInfo()
      const userInfoResult = this.deleteUserInfo()
      const sessionId = this.deleteSessionId()
      const [_uc,
        _l,
        _m,
        _r,
        _d,
        _ls,
        _fd,
        _si,
        _ui,
        _sid] = await Promise.all([
          userConfigResult,
          labelsResult,
          metaResult,
          relationshipsResult,
          dataResult,
          loginStorageResult,
          fieldbuddySettingResult,
          sessionInfoResult,
          userInfoResult,
          sessionId
        ])

      return {
        type: 'success',
        result: _uc && _l && _m && _r && _d && _ls && _fd && _si && _ui && _sid
      }
    } catch (e) {
      return this.ERROR(e, "clearStorage")

    }

  }

  moveDataFiles = async() => {
    const oldPaths2 = ["ExperienceData/%2540expoUsername%252FappName/", "ExponentExperienceData/%2540expoUsername%252FappName/"];

    const oldPaths = [
      "ExperienceData/%2540fieldbuddy%252F" + Constants.manifest.slug + "/",
      "ExponentExperienceData/%2540fieldbuddy%252F" + Constants.manifest.slug + "/"
    ]

    const newUri = FileSystem.documentDirectory;
    var info = await FileSystem.getInfoAsync(newUri, {});
    if (info.exists) {
      //("New location already exists.");
      return;
    }

    // try to move the SQLite directory from one of the old locations
    for (let index = 0; index < oldPaths.length; index++) {
      var path = oldPaths[index];
      var oldUri = FileSystem.documentDirectory + path + "SQLite";
      info = await FileSystem.getInfoAsync(oldUri, {});
      if (info.exists) {
        //("moving " + oldUri);
        await FileSystem.moveAsync({from: oldUri, to: newUri});
        //("moved " + oldUri);
        return;
      }
    }
  }

  migrateStorage = async() => {
    try {

      // if (FileSystem.documentDirectory.includes("ExperienceData")) { return false }

      try {

        // const _MID_PATH_ = IsIOS()   ? "ExponentExperienceData/%40fieldbuddy%2F" +
        // Constants.manifest.slug   : "ExperienceData/%40fieldbuddy%2F" +
        // Constants.manifest.slug
        const _MID_PATH_3 = IsIOS()
          ? `ExponentExperienceData/%40fieldbuddy%2F${Constants.manifest.slug}`
          : "ExperienceData/%40fieldbuddy%2F" + Constants.manifest.slug

        const _MID_PATH_ = IsIOS()
          ? `ExponentExperienceData/%2540fieldbuddy%252F${Constants.manifest.slug}`
          : "ExperienceData/%2540fieldbuddy%252F" + Constants.manifest.slug

        let _fromDirectory = `${FileSystem.documentDirectory}${_MID_PATH_}`
        // alert(_fromDirectory)
        alert('migrateStorage2')
        let content2 = await FileSystem.readDirectoryAsync(FileSystem.documentDirectory)
        let f = content2.filter(x => x.includes("Experience"))[0]

        let ff = `${FileSystem.documentDirectory}${f}`
        // alert(ff)
        let content = await FileSystem.readDirectoryAsync(ff)
        let f3 = content.filter(x => x.includes("fieldbuddy"))[0]
        let _MID_ = `${ff}/${f3}`

        for (let key of[this.USER_CONFIG,
          this.LABELS,
          this.META,
          this.RELATIONSHIPS,
          this.DATA,
          this.LOGIN_STORAGE]) {

          // let _key = `${PackageInfo.VERSION_CODE}_${key}`

          let _key = `${PackageInfo.VERSION_CODE}_${key}`;
          let _fileUri = FileSystem.documentDirectory + _MID_ + _key.replace(":", "/")
          const _fileUrijson = FileSystem.documentDirectory + _key.replace(":", "/")
          // const _fileUri = _MID_ + _key alert(_fileUri) const _fi = await
          // FileSystem.getInfoAsync(_fileUri, {})
          await FileSystem.moveAsync({from: _fileUri, to: _fileUrijson})
          // if (_fi.exists) {   alert(_fileUri+ ' exists') }   const _fileUrijson
          // =FileSystem.documentDirectory + "/" + _fileUri   await
          // FileSystem.copyAsync({from: _fileUri, to: _fileUrijson})
          // _uri.push(_fileUrijson)    //(_fi.uri, _fileUrijson) }
        }

        // let _filesToMove = await FileSystem.readDirectoryAsync(_MID_) //
        // alert(_filesToMove[0]) _filesToMove = _filesToMove.filter(x => //
        // x.includes("LIVE")) alert(_filesToMove) for (let file of _filesToMove) {   //
        //   // let _fi = await FileSystem.getInfoAsync(FileSystem.documentDirectory +
        // // file)   //  // let _exists = _fi.exists   let _options = {     "from":
        // _MID_ + "/" + file,     "to": FileSystem.documentDirectory + "/" + file   }
        // //   alert(_options)  // Sentry.Native.captureException(new Error("migrating... " +
        // // file), {logger: 'migrateStorage'});   // if (!_exists) {   await
        // FileSystem.copyAsync(_options) }
      } catch (err) {
        // Sentry.Native.captureException(new Error("migrating exc... " + file), {logger: 'migrateStorage'});

      }

    } catch (error) {}

    return true
  }

  backupStorage = async() => {
    let _uri = []
    for (let key of[this.USER_CONFIG,
      this.LABELS,
      this.META,
      this.RELATIONSHIPS,
      this.DATA,
      this.LOGIN_STORAGE]) {
      let _key = `${PackageInfo.VERSION_CODE}_${key}`
      const _fileUri = FileSystem.documentDirectory + _key

      const _fi = await FileSystem.getInfoAsync(_fileUri)
      if (_fi.exists) {
        const _fileUrijson = _fileUri + ".json"
        await FileSystem.copyAsync({from: _fileUri, to: _fileUrijson})
        _uri.push(_fileUrijson)
        //(_fi.uri, _fileUrijson)
      }
    }

    return _uri
  }

  backupStorage2Android = async () => {

    let _uri = []
    try {


      let _data = await this.getData();
      if (_data.type === 'success') {

        let _key = `${PackageInfo.VERSION_CODE}_${this.DATA}`
        const _fileUri = FileSystem.documentDirectory + _key
        _uri.push({ fileName: "backup_data_android.json", fileContent: JSON.stringify(_data.data), fileURI: _fileUri, fileInfo: null })
      }
    } catch (x) {
      console.warn("Android Backup Storage retrieval issue: " + x)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);              
        Sentry.Native.captureException("Android Backup Storage retrieval issue: " + x);
      });
    }
    return _uri
  }

  backupStorage2 = async() => {
    if (Platform.OS === 'android') {
      return await this.backupStorage2Android()
    }

    let _uri = []
    try {

      for (let key of [this.DATA]) {
        let _key = `${PackageInfo.VERSION_CODE}_${key}`
        const _fileUri = FileSystem.documentDirectory + _key
        
        const _fi = await FileSystem.getInfoAsync(_fileUri)

        if (_fi.exists) {
          const _fileUrijson = _fileUri + ".json"
          const _content = await FileSystem.readAsStringAsync(_fileUri)
          _uri.push({fileName: "backup_data.json", fileContent: _content, fileURI: _fileUri, fileInfo:_fi})
        }
      }
    } catch (x) {
      console.warn("iOS Backup Storage retrieval issue: " + x)
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);              
        Sentry.Native.captureException("iOS Backup Storage retrieval issue: " + x);
      });
    }
   
    return _uri
  }

  restoreUserConfig = async() => {
    return await GetKeyValue(this.USER_CONFIG)
  }
  storeUserConfig = async(userConfig) => {
    let result = await SetKeyValue(this.USER_CONFIG, userConfig)
    if (result.type === 'success') {
      return await GetKeyValue(this.USER_CONFIG)
    }
    return {type: 'error'}
  }
  deleteUserConfig = async() => {
    return await DeleteKeyValue(this.USER_CONFIG)
  }

  restoreLabels = async() => {
    return await GetKeyValue(this.LABELS)
  }
  storeLabels = async(labels) => {
    let result = await SetKeyValue(this.LABELS, labels)
    if (result.type === 'success') {
      return await GetKeyValue(this.LABELS)
    }
    return {type: 'error'}
  }
  deleteLabels = async() => {
    return await DeleteKeyValue(this.LABELS)
  }

  restoreMeta = async() => {
    return await GetKeyValue(this.META)
  }
  storeMeta = async(meta) => {
    return await SetKeyValue(this.META, meta)
    if (result.type === 'success') {
      return await GetKeyValue(this.META)
    }
    return {type: 'error'}
  }
  deleteMeta = async() => {
    return await DeleteKeyValue(this.META)
  }

  restoreRelationships = async() => {
    return await GetKeyValue(this.RELATIONSHIPS)
  }
  storeRelationships = async(relationships) => {
    return await SetKeyValue(this.RELATIONSHIPS, relationships)
  }
  deleteRelationships = async() => {
    return await DeleteKeyValue(this.RELATIONSHIPS)
  }

  getData = async() => {
    return await GetKeyValue(this.DATA)
  }
  restoreData = async() => {
    try {
      let d = await GetKeyValue(this.DATA)
      return d
    } catch (e) {}
  }
  getKeyValueStorageSize = async() => {
    const _keys = [
      this.USER_CONFIG,
      this.LABELS,
      this.META,
      this.RELATIONSHIPS,
      this.DATA,
      this.LOGIN_STORAGE
    ]
    return await GetKeyValueStorageSize(_keys)
  }
  storeData = async(data) => {
    let result = await SetKeyValue(this.DATA, data)

    if (result.type === 'success') {
      return await GetKeyValue(this.DATA)
    }
    return {type: 'error'}
  }

  mergeData = async(data) => {
    let result = await SetKeyValue(this.DATA, data)

    if (result.type === 'success') {
      return await GetKeyValue(this.DATA)
    }
    return {type: 'error'}
  }

  deleteDataStorage = async() => {
    return await DeleteKeyValue(this.DATA)
  }

  restoreLoginStorage = async() => {
    return await GetKeyValue(this.LOGIN_STORAGE)
  }
  storeLoginStorage = async(loginStorage) => {
    try {
      let result = await SetKeyValue(this.LOGIN_STORAGE, loginStorage)
      if (result.type === 'success') {
        return await GetKeyValue(this.LOGIN_STORAGE)
      }
    } catch (ex) {}
    return {type: 'error'}
  }
  deleteLoginStorage = async() => {
    return await DeleteKeyValue(this.LOGIN_STORAGE)
  }

  restoreFieldbuddySettings = async() => {
    return await GetKeyValue(this.FIELDBUDDYSETTINGS)
  }
  storeFieldbuddySettings = async(settings) => {
    try {
      let result = await SetKeyValue(this.FIELDBUDDYSETTINGS, settings)
      if (result.type === 'success') {
        return await GetKeyValue(this.FIELDBUDDYSETTINGS)
      }
    } catch (ex) {}
    return {type: 'error'}
  }

  deleteFieldbuddySettings = async() => {
    return await DeleteKeyValue(this.FIELDBUDDYSETTINGS)
  }

  deleteSessionInfo = async() => {
    return await DeleteSessionInfo()
  }

  deleteSessionId = async() => {  
    return await DeleteSessionId()
  }

  getSessionInfo = async() => {
    return await GetSessionInfo()
  }

  getUserInfo = async() => {
    return await GetKeyValue("UserInfo")
  }

  deleteUserInfo = async() => {
    return await DeleteKeyValue("UserInfo")
  }
}

module.exports = {
  StorageApi
}