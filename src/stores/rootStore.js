import LoginStore from "./loginStore"
import ChangesStore from './changesStore'
import DataManipulationStore from './dataManipulationStore'
import ValidationStore from './validationStore'
import SettingsStore from './appSettings/settingsStore'
import AppStore from './appsStore'
import LocalizeStore from './appSettings/localizeStore'
import DataStore from './dataStore'
import MetaStore from './metaStore'
import PermissionStore from './appSettings/permissionStore'
import DocumentStore from "./documentStore"
import FieldBuddySettings from './appSettings/fieldbuddySettings';

import { LoggingApi, MetaApi, StorageApi, SyncApi, RingsApi } from './api'
import _ from 'lodash'
import { spy } from 'mobx'
import AppsStore from "./appsStore"
const logginApi = new LoggingApi()
const metaApi = new MetaApi()
const storageApi = new StorageApi()
const syncApi = new SyncApi()
const ringsApi = new RingsApi()


export default class RootStore {
  constructor() {
    this.loginStore = new LoginStore(storageApi, syncApi, metaApi, logginApi, ringsApi, this)
    this.dataManipulationStore = new DataManipulationStore(this)
    this.validationStore = new ValidationStore(this)
    this.settingsStore = new SettingsStore(this)
    this.appsStore = new AppsStore(this)
    this.localizeStore = new LocalizeStore(this)
    this.dataStore = new DataStore(this)
    this.metaStore = new MetaStore(this)
    this.permissionStore = new PermissionStore(this)
    this.changesStore = new ChangesStore(this)
    this.documentStore = new DocumentStore(this)
    this.fieldbuddySettingsStore = new FieldBuddySettings(this)
  }
}

// spy((event) => {
//   if (event.type === 'action') {
//        //(`[SPY][${event.name}][${event.arguments}]`)
//   }
// })