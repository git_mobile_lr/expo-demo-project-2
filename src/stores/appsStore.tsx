import { action, observable } from 'mobx'
import _ from 'lodash'
import * as Sentry from 'sentry-expo';
import { Icons } from '../types'
const PREFIXED_TIME_REGISTRATION_CONFIG = require('../stores/appsDefaults/TimeRegistrationApp/prefixed/config.json');
const PREFIXED_TIME_REGISTRATION_BRIEFCASE_REQ = require('../stores/appsDefaults/TimeRegistrationApp/prefixed/briefcaseRequirements.json');
const TIME_REGISTRATION_CONFIG = require('../stores/appsDefaults/TimeRegistrationApp/nonPrefixed/config.json');
const TIME_REGISTRATION_BRIEFCASE_REQ = require('../stores/appsDefaults/TimeRegistrationApp/nonPrefixed/briefcaseRequirements.json');
const TIME_REGISTRATION_MESSED_UP__BRIEFCASE_REQ = require('../stores/appsDefaults/TimeRegistrationApp/messedUpBriefcaseRequirements.json');

/**
 * MOBX STORE - Default App Settings merged with the config received from the BE.
 */
export default class AppsStore {

    constructor(rootStore: any) {

        this.rootStore = rootStore
        this.initApps()


    }



    /**
     * Mobx Root Store
     */
    rootStore: any

    /**
    * App Rules
    */
    apps: any

    @observable
    activeAppName = "WorkOrders"

    @observable
    activeScreen = {
        appName: 'WorkOrders',
        screenName: 'WorkOrders'
    }

    @observable
    defaultApps = {
        WorkOrders: {
            pluggableByDefault: false
        },
        TimeRegistration: {
            pluggableByDefault: true,
            minPackageVersion: "1.135.0"
        },
        StockManagement: {
            pluggableByDefault: false
        }
    }

    initApps() {
        this.apps = [
            {
                name: 'WorkOrders',
                defaultScreen: "WorkOrders",
                title: '${Label.WorkOrdersApp}',
                defaultTitle: "Work Orders",
                icon: 'briefcase',
                default: true,
                screens: [
                    {
                        "config": {
                            "header": {
                                "title": "${FIELDBUDDY__Work_Order__c.plurallabel}"
                            },
                            "config": {
                                "groupBy": {
                                    "field": "T_Start_Time__c",
                                    "table": "FIELDBUDDY__Work_Order__c"
                                },
                                "groupByRange": {
                                    "fields": {
                                        "endDate": "T_End_Time__c",
                                        "startDate": "T_Start_Time__c"
                                    },
                                    "table": "FIELDBUDDY__Work_Order__c"
                                },
                                "item": {
                                    "config": {
                                        "cardLayout": [
                                            {
                                                "config": {
                                                    "active": false,
                                                    "columns": 1,
                                                    "items": [
                                                        {
                                                            "dataBinding": [
                                                                {
                                                                    "empty": "-",
                                                                    "field": "Name",
                                                                    "table": "Account",
                                                                    "var": "CUSTOMER"
                                                                },
                                                                {
                                                                    "empty": "-",
                                                                    "field": "FIELDBUDDY__Street__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "STREET"
                                                                },
                                                                {
                                                                    "empty": "-",
                                                                    "field": "FIELDBUDDY__House_Number__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "HOUSE"
                                                                },
                                                                {
                                                                    "empty": "-",
                                                                    "field": "FIELDBUDDY__House_Number_Suffix__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "SUFFIX"
                                                                },
                                                                {
                                                                    "empty": "-",
                                                                    "field": "FIELDBUDDY__Postal_Code__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "ZIP"
                                                                },
                                                                {
                                                                    "empty": "-",
                                                                    "field": "FIELDBUDDY__City__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "CITY"
                                                                },
                                                                {
                                                                    "empty": "-",
                                                                    "field": "Phone",
                                                                    "table": "Account",
                                                                    "var": "PHONE"
                                                                }
                                                            ],
                                                            "dataText": {
                                                                "LINE1": "CUSTOMER",
                                                                "LINE2": "STREET HOUSE SUFFIX",
                                                                "LINE3": "ZIP CITY",
                                                                "LINE4": "PHONE"
                                                            },
                                                            "staticBinding": [
                                                                {
                                                                    "name": "CARD_TITLE",
                                                                    "text": "${Account.label}",
                                                                    "type": "TEXT"
                                                                },
                                                                {
                                                                    "android": "md-person",
                                                                    "ios": "ios-person",
                                                                    "name": "CARD_ICON",
                                                                    "type": "ICON"
                                                                }
                                                            ],
                                                            "type": "FSingleCard"
                                                        }
                                                    ]
                                                },
                                                "type": "FCardLayout"
                                            },
                                            {
                                                "config": {
                                                    "active": false,
                                                    "columns": 1,
                                                    "items": [
                                                        {
                                                            "dataBinding": [
                                                                {
                                                                    "empty": "-",
                                                                    "field": "FIELDBUDDY__Type__c",
                                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                                    "var": "P"
                                                                }
                                                            ],
                                                            "dataText": {
                                                                "LINE1": "P"
                                                            },
                                                            "staticBinding": [
                                                                {
                                                                    "name": "CARD_TITLE",
                                                                    "text": "${FIELDBUDDY__Work_Order__c.FIELDBUDDY__Type__c.label}",
                                                                    "type": "TEXT"
                                                                },
                                                                {
                                                                    "android": "md-briefcase",
                                                                    "ios": "ios-briefcase",
                                                                    "name": "CARD_ICON",
                                                                    "type": "ICON"
                                                                }
                                                            ],
                                                            "type": "FSingleCard"
                                                        },
                                                        {
                                                            "condition": [
                                                                {
                                                                    "executor": "jsonQuery",
                                                                    "type": "filter",
                                                                    "source": "${DATA}",
                                                                    "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}]:isServiceRequestBased"
                                                                }
                                                            ],
                                                            "dataBinding": [
                                                                {
                                                                    "empty": "-",
                                                                    "field": "Name",
                                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                                    "var": "${IP}"
                                                                },
                                                                {
                                                                    "empty": "-",
                                                                    "field": "FIELDBUDDY__Description__c",
                                                                    "table": "FIELDBUDDY__Sub_location__c",
                                                                    "var": "${DESC}"
                                                                }
                                                            ],
                                                            "dataText": {
                                                                "LINE1": "${IP}",
                                                                "LINE2": "${DESC}"
                                                            },
                                                            "staticBinding": [
                                                                {
                                                                    "name": "CARD_TITLE",
                                                                    "text": "${Label.InstallationData}",
                                                                    "type": "TEXT"
                                                                },
                                                                {
                                                                    "android": "md-cog",
                                                                    "ios": "ios-cog",
                                                                    "name": "CARD_ICON",
                                                                    "type": "ICON"
                                                                }
                                                            ],
                                                            "type": "FSingleCard"
                                                        }
                                                    ]
                                                },
                                                "type": "FCardLayout"
                                            }
                                        ],
                                        "events": [
                                            {
                                                "dataBinding": [
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "ID"
                                                    }
                                                ],
                                                "eventName": "onPress",
                                                "target": "WorkOrderDetails",
                                                "triggeredEvent": "navigate"
                                            }
                                        ],
                                        "header": {
                                            "config": {
                                                "dataBinding": [
                                                    {
                                                        "field": "FIELDBUDDY__Status__c",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "STATUS"
                                                    },
                                                    {
                                                        "field": "T_Start_Time__c",
                                                        "format": "HH:mm",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "START_TIME"
                                                    },
                                                    {
                                                        "field": "T_End_Time__c",
                                                        "format": "HH:mm",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "END_TIME"
                                                    }
                                                ],
                                                "dataText": {
                                                    "LeftText": "START_TIME → END_TIME",
                                                    "RightText": "STATUS",
                                                    "Status": "STATUS"
                                                },
                                                "staticBinding": [
                                                    {
                                                        "android": "md-information-circle",
                                                        "ios": "ios-information-circle",
                                                        "name": "ICON_LEFT",
                                                        "type": "ICON"
                                                    },
                                                    {
                                                        "android": "md-play",
                                                        "ios": "ios-play",
                                                        "name": "ICON_RIGHT",
                                                        "type": "ICON"
                                                    },
                                                    {
                                                        "android": "md-radio-button-off",
                                                        "ios": "ios-radio-button-off",
                                                        "name": "ICON_STATUS_OPEN",
                                                        "type": "ICON"
                                                    },
                                                    {
                                                        "android": "md-radio-button-on",
                                                        "ios": "ios-radio-button-on",
                                                        "name": "ICON_STATUS_CLOSED",
                                                        "type": "ICON"
                                                    },
                                                    {
                                                        "android": "md-walk-outline",
                                                        "ios": "ios-walk-outline",
                                                        "name": "ICON_STATUS_PROGRESS_2",
                                                        "type": "ICON"
                                                    },
                                                    {
                                                        "animation": "in_progress_balls",
                                                        "name": "ICON_STATUS_PROGRESS",
                                                        "type": "ANIMATION"
                                                    }
                                                ]
                                            },
                                            "type": "FListItemHeader"
                                        },
                                        "headline": {
                                            "config": {
                                                "dataBinding": [
                                                    {
                                                        "field": "FIELDBUDDY__Type__c",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "A"
                                                    },
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "B"
                                                    }
                                                ],
                                                "dataText": "A (B)"
                                            },
                                            "type": "FListItemHeadline"
                                        },
                                        "templateHeadline": {
                                            "config": {
                                                "condition": [
                                                    {
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "field": "FIELDBUDDY__Template__c",
                                                        "filter": "in",
                                                        "value": [
                                                            "Location"
                                                        ]
                                                    }
                                                ],
                                                "dataBinding": [
                                                    {
                                                        "aggregationType": "$COUNT",
                                                        "groupBy": "FIELDBUDDY__Installed_Product__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "COUNT"
                                                    }
                                                ],
                                                "staticBinding": [
                                                    {
                                                        "name": "PRODUCTS",
                                                        "text": "${Label.InstalledProductsOnLocation}",
                                                        "type": "TEXT"
                                                    }
                                                ],
                                                "dataText": "COUNT PRODUCTS"
                                            },
                                            "type": "FListItemTemplateHeadline"
                                        }
                                    },
                                    "type": "FListItem"
                                },
                                "orderBy": {
                                    "field": "T_Start_Time__c",
                                    "table": "FIELDBUDDY__Work_Order__c"
                                },
                                "empty": {
                                    "line1": "${Label.NoWorkOrdersLine1}",
                                    "line2": "${Label.NoWorkOrdersLine2}"
                                }
                            },
                            "type": "FWeekToolbar"
                        },
                        "name": "WorkOrders"
                    }
                ]
            }
        ]

        // this.activeAppName = "WorkOrders"
    }

    @action
    reset() {
        this.activeAppName = "WorkOrders"
        this.activeScreen = {
            appName: 'WorkOrders',
            screenName: 'WorkOrders'
        }
        this.initApps()
    }


    async checkBriefcaseRequirementsAndLabels(app:string, appConfig:any, briefcaseReq:any) {
        const { loginStore, metaStore } = this.rootStore
        const userInfo = loginStore.getUserInfo
        const _language = userInfo.language ? userInfo.language.replace("_", "-") : '-'
        const missingFieldsOrTables = metaStore.getMissingFieldsAndTables(briefcaseReq)

        if (_.isEmpty(missingFieldsOrTables)) {
            // check if the required labels are present
            const requiredLabelNames = loginStore.extractLabelsFromConfig(appConfig, true)
            const missingLabels = requiredLabelNames.filter((label:any) => {
                return !loginStore.getLabelsArray().map((label:any) => label.key).includes(label)
            })
            if (!_.isEmpty(missingLabels)) {
                const _warning = `\`WARNING\` Checked: ${requiredLabelNames.length} required labels while plugging in ${app} app. Found ${missingLabels.length} unresolved unique label(s). Using ${loginStore.getConfigurationName} ${_language}.`
                await loginStore._slackLabels(_warning, missingLabels)
            }
            return appConfig
        }
        const _err = `Detected ${missingFieldsOrTables.length} missing fields/tables while plugging in ${app} app. Using ${loginStore.getConfigurationName} ${_language}.`
        await loginStore._slackAppPluginError(_err, missingFieldsOrTables)
        return null
    }

    checkIfPluggableAppsArePresent() {
        const { defaultApps } = this.rootStore.appsStore
        const array = Object.entries(defaultApps)
        let appsToPlugin = []
        let isEveryAppPresent = true
        try {
            const pluggableDefaultApps = Object.keys(Object.fromEntries(array.filter(([key, value]) => value.pluggableByDefault === true)))
            const presentApps = this.getApps().map(({ name }) => name)
            isEveryAppPresent = pluggableDefaultApps.every(app => presentApps.includes(app))
            if (!isEveryAppPresent) {
                appsToPlugin = pluggableDefaultApps.filter(app => !presentApps.includes(app))
            }
        } catch (error) {
            //pass
        }
        return {isEveryAppPresent, appsToPlugin}
    }

    async tryToPlugInApps() {
        const { loginStore, metaStore } = this.rootStore
        const { isEveryAppPresent, appsToPlugin } = this.checkIfPluggableAppsArePresent()
        const userInfo = loginStore.getUserInfo
        const _language = userInfo.language ? userInfo.language.replace("_", "-") : '-'
        const prefix = loginStore.getPrefix()
        
        if (isEveryAppPresent) {
            return null
        } else {
            // if they pass we plugin the app(s)
            // If you're on DEV4 you don't have a packageVersion
            if (!_.isEmpty(loginStore.packageVersion) || prefix === '') {
                const { major, minor, patch } = loginStore.packageVersion
                const formattedVersion = `${+major}.${+minor}.${+patch}`

                if (!_.isEmpty(appsToPlugin)) {
                    const readyApps = await Promise.all(appsToPlugin.map(async app => {
                        const minPackageVersion = prefix ? this.defaultApps[app].minPackageVersion : ''
                        switch (app) {
                            // case 'WorkOrders': We do not support a plugin WO app at this stage
                            //     break
                            case 'TimeRegistration':
                                let briefcaseReq = prefix
                                    ? PREFIXED_TIME_REGISTRATION_BRIEFCASE_REQ
                                    : TIME_REGISTRATION_BRIEFCASE_REQ
                                if (loginStore.useMessedUpBriefcaseRequirements) {
                                    briefcaseReq = TIME_REGISTRATION_MESSED_UP__BRIEFCASE_REQ
                                }
                                const config = prefix
                                    ? PREFIXED_TIME_REGISTRATION_CONFIG
                                    : TIME_REGISTRATION_CONFIG

                                // check if there is a minimum package required
                                if (!minPackageVersion) {
                                    // only check briefcase requirements + required labels
                                    return this.checkBriefcaseRequirementsAndLabels(app, config, briefcaseReq)
                                } else {
                                    // check the minimum package version
                                    const v = loginStore.compareSoftwareVersions(formattedVersion, minPackageVersion)
                                    if (v >= 0) {
                                        // checks if the briefcaseRequirements (tables & fields) are present in metaStore
                                        return this.checkBriefcaseRequirementsAndLabels(app, config, briefcaseReq)
                                    }
                                    // min. package version is below minimum ERROR (slack fb2-logs)
                                    const _err = `Detected that the package version: ${formattedVersion} is below the required: ${minPackageVersion} while plugging in ${app} app. Using ${loginStore.getConfigurationName} ${_language}.`
                                    loginStore._slackAppPluginError(_err)
                                    return null
                                }
                        }
                        return null
                    }))
                    return readyApps.filter((app:any) => app !== null)
                }
            }
        }
        return null
    }

    @action
    updateApps() {
        const { loginStore } = this.rootStore

        if (loginStore.userConfig && !_.isEmpty(loginStore.userConfig)) {

            try {
                const apps = loginStore.getApps()

                this.setApps(apps)

                this.tryToSetPluggableApps(apps)

            } catch (err) {

            }

        }

    }

    @observable
    pluggableApps = 'initial'

    @action
    setPluggableApps(apps : Array) {
        this.pluggableApps = apps
    }
    
    @action
    async tryToSetPluggableApps(apps: any) {
        let pluggableApps = await this.tryToPlugInApps();
        this.setPluggableApps(pluggableApps)
        if (pluggableApps) {
            if (!_.isEmpty(pluggableApps)) {
                for (let pluggableApp of pluggableApps) {
                    if (pluggableApp) {
                        pluggableApp.isVirtual = true
                    }
                }
            }
            this.setApps(_.concat(apps, pluggableApps));
        }
    }

    setApps(apps: any) {
        this.apps = apps
    }

    @action
    getApps() {
        return this.apps
        return [
            {
                name: 'WorkOrders',
                title: '${Label.WorkOrdersApp}',
                defaultTitle: "Work Orders",
                icon: 'briefcase',
                default: false
            },
            {
                name: 'TimeRegistration',
                title: '${Label.TimeRegistrationApp}',
                defaultTitle: "Time Registration",
                icon: 'clock-o',
                default: false
            }
        ]
    }

    @action
    getWOAppIcon() {
        const apps = this.getApps()
        const woApp = apps.filter(x => x.defaultTitle === 'Work Orders' ||  x.defaultTitle === 'WorkOrders')
        try {


            if (woApp) {
                return woApp[0].icon
            }
        } catch (error) {
            //pass
        }
        return 'briefcase'
    }

    @action
    async setActiveAppName(appName: string) {

        this.activeAppName = appName

        try {
            
            
            if (appName !== "WorkOrders" && this.getActiveScreen() !== "WorkOrders") {                    
                this.rootStore.dataStore.clearContextData();                
            }
           

        } catch (error) {
            //pass
        }

        await this.rootStore.loginStore.initRules()
        this.rootStore.settingsStore.updateAppSettings()
    }

    @action
    getActiveAppAppSettings() {
        return this.getActiveApp().appSettings
    }

    @action
    getActiveApp() {

        const activeAppIndex = _.findIndex(this.apps, a => a.name === this.activeAppName)
        if (activeAppIndex >= 0) {
            return this.apps[activeAppIndex]
        }

        if (_.isEmpty(this.pluggableApps)) {
            this.setActiveAppName(this.apps[0].name)
            return this.apps[0]
        }
        return null
    }

    @action
    getActiveScreen() {
        const activeApp = this.getActiveApp()
        if (activeApp) {
            return activeApp.defaultScreen
        }

        return null
    }

    @action
    getScreenConfig(screenName: string) {
        try {

            const screen = this.getActiveApp().screens
                .filter(screen => screen.name === screenName)
            if (_.isEmpty(screen)) {
                return { success: false, result: {} }
            } else {
                return { success: true, result: screen[0].config }
            }
        } catch (error) {
            return { success: false, result: {} }
        }

    }

    @action
    getValidationRuleById(id: string){
        try {
            /*
             "Id": "a0w3N000005gjuCQAQ",
                    "Name": "CO Meting hoog_v2",
            */
            const index = _.findIndex(this.getActiveApp().validationRules, x => x.Id === id || x.Name === id);
            if(index>=0){
                return this.getActiveApp().validationRules[index]
            }
        } catch (error) {
            //pass
        }
        
        return null   
    }

    @action
    getValidationRules(sourceComponent: string = "", sourceComponentType: string = "") {
        try {
            /*
            [
                "",
                "Edit Work Order Activities",
                "Edit Work Order Other Expenses",
                "Edit Work Order Parts",
                "Edit Work Order Returns",
                "Timesheet Lineitem",
                "Work Order Closure"
            ]
            */
            let mobilePage = ""
            let code = ""

            switch (sourceComponent.toLowerCase()) {
                case "BEFORECLOSURE".toLowerCase():
                    mobilePage = ""
                    code = "BEFORECLOSURE"
                    break;
                case "TSLI".toLowerCase():
                    mobilePage = "Timesheet Lineitem"
                    break;
                case "CLOSURE".toLowerCase():
                    mobilePage = "Work Order Closure"
                    break;
                case "WOLI".toLowerCase(): 
                    case "WOI".toLowerCase(): {
                    mobilePage = this.getValidationRulesForWoliType(sourceComponentType, mobilePage);
                }


                    break;

            }

            if (mobilePage) {

                if (this.getActiveApp().validationRules) {
                    /*
                    {
                        "Id": "a0w0J00000Aad83QAB",
                        "Name": "Arbeidsloon (Zon- en feestdagen) niet aanpasbaar",
                        "Code__c": null,
                        "Error_Message__c": "Het is niet toegestaan om het bedrag aan te passen. Het standaard tarief voor Arbeidsloon (Zon- en feestdagen) is €31,00",
                        "Formula__c": "=AND(data['FIELDBUDDY__Unit_Price_Including_Tax_input__c'] != '31',data['S_SMScode__c']=='(Zon- en feestdagen)')",
                        
                        "Mobile_Page__r": "Edit Work Order Other Expenses"
                    }
                    */

                    const f = this.getActiveApp().validationRules.filter(x => x.Mobile_Page__r === mobilePage)

                    return { success: true, validationRules: f }
                }

            } else if (code && !mobilePage) {
                    /*
                    {
                        "Id": "a0w0J00000D4XEDQA3",
                        "Name": "Onderdelen_tijd",
                        "Code__c": "BEFORECLOSURE",
                        "Error_Message__c": "De totaal gespendeerde tijd op alle Onderdelen is groter dan de geschreven tijd op Servicebezoek",
                        "Formula__c": "function output(data) {    var wo_id = data['Id'];    var ts_store = Ext.getStore('Timesheet_Lineitems');    var ts = ts_store.findRecordsAll(function(record){ return (record.get('FIELDBUDDY__Work_Order__c') == wo_id);});    var wop_store = Ext.getStore('Work_Order_Parts');    var wop = wop_store.findRecordsAll(function(record){ return (record.get('FIELDBUDDY__Work_Order__c') == wo_id);});    var total_time_parts = 0;    for(var i = 0; i < wop.length; i++) {        if (wop[i].get('T_Logged_Time__c') != null) {            total_time_parts += wop[i].get('T_Logged_Time__c');        }    }    var total_time_ts = 0;    for(var i = 0; i < ts.length; i++) {        if (   ts[i].get('FIELDBUDDY__Start_Time__c') != null             && ts[i].get('FIELDBUDDY__End_Time__c') != null            && ts[i].get('FIELDBUDDY__Type__c') == 'Servicebezoek') {            var minutes = (ts[i].get('FIELDBUDDY__End_Time__c') - ts[i].get('FIELDBUDDY__Start_Time__c'))/60000;            total_time_ts += minutes;        }    }    if (total_time_ts > 0 && total_time_ts < total_time_parts) {        return false;    } else {        return true;    }}output(data);",
                        "Mobile_Page__r": ""
                    }
                    */
                    const f = this.getActiveApp().validationRules.filter(x => x.Code__c === code && !x.Mobile_Page__r)

                    return { success: true, validationRules: f }
            }

        } catch (error) {
            
            //pass
        }
        
        return { success: false, validationRules: [] }
    }

    private getValidationRulesForWoliType(sourceComponentType: string, mobilePage: string) {
        switch (sourceComponentType.toLowerCase()) {
            case "Activity".toLowerCase():
                mobilePage = "Edit Work Order Activities";
                break;
            case "Part".toLowerCase():
                mobilePage = "Edit Work Order Parts";
                break;
            case "Other expense".toLowerCase():
                mobilePage = "Edit Work Order Other Expenses";
                break;
            case "Return".toLowerCase():
                mobilePage = "Edit Work Order Returns";
                break;
        }
        return mobilePage;
    }
}
