import {
    action,
    computed,
    observable,
} from 'mobx';
import R from 'ramda';
import _ from 'lodash';
import jsonQuery from 'json-query';
import * as Sentry from 'sentry-expo';
import moment from 'moment/min/moment-with-locales';
import { reduxStore } from '../App';
import { ReduxActions } from '../types';
import { onAddChange } from '../reducers/actions'

export default class ChangesStore {

    constructor(rootStore: any) {
        this.rootStore = rootStore
        this.syncStatus = "Waiting for next one"
        this.syncResponse = undefined
    }


    @observable
    changesPerSyncId: any = {}


    getAllChangesPerSyncId() {
        return this.changesPerSyncId
    }

    //triggered when restored from device
    setAllChangesPerSyncId(rawChanges: any) {
        try {
            let _json = JSON.parse(JSON.stringify(rawChanges))

            if (_json) {
                this.changesPerSyncId = _json
            } else {
                //if restored json is empty, recover default
                this.resetChanges()
            }
        } catch (e) {
            console.warn("setAllChangesPerSyncId: ", e)
            //recover default
            this.resetChanges()
        }


    }

    addChangeToHistory(change: any) {
        //console.warn("addChangeToHistory " + this.rootStore.settingsStore.rules.backup.enabled)
        if (this.rootStore.settingsStore.rules.backup.enabled) {
            let timestamp = moment().utc().format()

            let _change = _.first(change)


            _change['timestamp'] = timestamp

            let state = reduxStore.getState()
            reduxStore.dispatch({
                type: ReduxActions.ADD_CHANGE,
                state: state,
                payload: _change
            })
        }
    }

    @observable
    syncStatus: string

    @observable
    syncResponse: string | undefined

    setSyncStatus(status: string) {

        this.syncStatus = status
    }

    setSyncResponse(response: string) {

        this.syncResponse = response
    }

    ///all the changes (past, now, future)
    // are re-assigned to (current) syncId
    // that is going to be executed next
    async rearrangeChanges(syncId: number) {
        try {
            let changes = this.getAllChanges()
            this.resetChanges()
            this.setChanges(syncId, changes)

        } catch (e) {
            console.warn("rearrangeChanges: ", e)
        }
    }

    async regroupBySalesforceId(syncId: number) {
        try {
            let changes = this.getAllChanges()

            let _newChanges = []
            // {
            //     "salesforceId": "a1T3B000003ivR7UAI",
            //         "target": "FIELDBUDDY__Work_Order_Line_Item__c",
            //             "changeType": "UPDATE",
            //                 "changeId": "ab2053ef66e590db6a19e1193dc4edaa",
            //                     "changeData": {
            //         "FIELDBUDDY__Status__c": "Open"
            //     },
            //     "LastUpdate": "2020-10-16T06:22:59.579Z"
            // }
            const FORMAT = "YYYY-MM-DD HH:mm:ss"
            //sort changes from oldest to the most recent ones:
            let sortChanges = _.sortBy(changes, x => {
                if (x.LastUpdate) {
                    return moment(x.LastUpdate).format(FORMAT)
                }
                return ""
            })

            for (let aChange of sortChanges) {

                const alreadyIndex = _.findIndex(_newChanges, x => x.salesforceId && aChange.salesforceId && x.salesforceId === aChange.salesforceId && x.target === aChange.target)

                if (alreadyIndex >= 0) {

                    const _previousChange = _newChanges[alreadyIndex]
                    if (aChange.changeData && _previousChange.changeData) {
                        let _mergeChange = Object.assign({}, { ..._previousChange }, { ...aChange })
                        _newChanges[alreadyIndex] = _mergeChange

                        let _mergeChangeData = Object.assign({}, { ..._previousChange.changeData }, { ...aChange.changeData })
                        _newChanges[alreadyIndex].changeData = _mergeChangeData
                    }
                } else {
                    const alreadyIndexedLocalId = _.findIndex(_newChanges, x => x.localId && aChange.localId && x.localId === aChange.localId && x.target === aChange.target)

                    if (alreadyIndexedLocalId >= 0) {

                        const _previousChangeLocal = _newChanges[alreadyIndexedLocalId]
                        if (aChange.changeData && _previousChangeLocal.changeData) {

                            let _mergeChange = Object.assign({}, { ..._previousChangeLocal }, { ...aChange })
                            _newChanges[alreadyIndexedLocalId] = _mergeChange

                            let _mergeChangeData = Object.assign({}, { ..._previousChangeLocal.changeData }, { ...aChange.changeData })
                            _newChanges[alreadyIndexedLocalId].changeData = _mergeChangeData
                        }

                    } else {

                        _newChanges.push(aChange)
                    }
                }
            }
            _newChanges = _newChanges.sort((a, b) => moment(a.CreatedOn) - moment(b.CreatedOn))
            this.resetChanges()
            this.setChanges(syncId, _newChanges)
        } catch (e) {
            console.warn("regroupBySalesforceId: " + e)
        }
    }


    hasBeenDeleted(salesforceId: number) {
        try {
            let changes = this.getAllChanges()

            const _deleted = changes.filter(x => x.changeType === "DELETE" && x.salesforceId === salesforceId)
            return !_.isEmpty(_deleted)

        } catch (e) {
            console.warn("hasBeenDeleted: ", e)
        }

        return false
    }

    async cleanUpDeletedLocalChanges(syncId: number, tablesNames, timesheetRecordIdToDelete = null) {
        try {
            /*
             2) local Id
            {
              "salesforceId": null,
              "localId": "__FB__TEMPLATE_TYPE_FIELDBUDDY__Work_Order_Line_Item__c_a24b775b303fd64a9faf9c8ebead6d9e",
              "target": "FIELDBUDDY__Work_Order_Line_Item__c",
              "changeType": "DELETE",
              "changeId": "27858f8480253cac59c1f9edeb6546f1",
              "changeData": {},
              "LastUpdate": "2020-10-16T14:03:22.551Z"
            }
    
            {
            "salesforceId": "__FB__TEMPLATE_TYPE_FIELDBUDDY__Work_Order_Line_Item_Detail__c_145842b006130b845f7a5c6c44cb8a37",
            "localId": "__FB__TEMPLATE_TYPE_FIELDBUDDY__Work_Order_Line_Item_Detail__c_145842b006130b845f7a5c6c44cb8a37",
            "target": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
            "changeType": "INSERT",
            "changeId": "88947091c0d9841ed3a4d6494308e649",
            "changeData": {
                "Name": "Inspectie",
                "FIELDBUDDY__Field_Type__c": "Page",
                "FIELDBUDDY__Value__c": "",
                "FIELDBUDDY__Work_Order_Line_Item__c": "__FB__TEMPLATE_TYPE_FIELDBUDDY__Work_Order_Line_Item__c_a24b775b303fd64a9faf9c8ebead6d9e",
                "FIELDBUDDY__Order__c": 100,
                "FIELDBUDDY__Page__c": "1"
            },
            "parentReference": "FIELDBUDDY__Work_Order_Line_Item__c",
            "LastUpdate": "2020-10-16T14:03:07.873Z"
            }
            
    
            2) Salesforce Id
            {
              "salesforceId": "a1T3B000002pxpBUAQ",
              "target": "FIELDBUDDY__Work_Order_Line_Item__c",
              "changeType": "DELETE",
              "changeId": "2aff7041419b17c16702c04ed7c0e8fd",
              "changeData": {},
              "LastUpdate": "2020-10-16T14:13:29.653Z"
            }
    
            3) 
            {
            "salesforceId": null,
            "localId": "__FB__TEMPLATE_TYPE_FIELDBUDDY__Timesheet_Lineitem__c_a0j1K00000ERft9QAD_a944fdfdffca83f3d28301213c49aa21",
            "target": "FIELDBUDDY__Timesheet_Lineitem__c",
            "changeType": "DELETE",
            "changeId": "2be067fb16322e4113d50e0b3ec9cb6f",
            "changeData": {},
            "LastUpdate": "2020-10-16T19:40:32.269Z"
            }
            
            {
            "salesforceId": "__FB__TEMPLATE_TYPE_FIELDBUDDY__Timesheet__c_a0j1K00000ERft9QAD_a1c98ac9b11fbc592b02b110d5502a23",
            "localId": "__FB__TEMPLATE_TYPE_FIELDBUDDY__Timesheet__c_a0j1K00000ERft9QAD_a1c98ac9b11fbc592b02b110d5502a23",
            "target": "FIELDBUDDY__Timesheet__c",
            "changeType": "INSERT",
            "changeId": "68eab1ecca85a1c61bd3992022874b6d",
            "changeData": {
                "Name": "Swift 2020-10-16",
                "FIELDBUDDY__User__c": "00541000005hBRzAAM",
                "FIELDBUDDY__Date__c": "2020-10-16"
            },
            "LastUpdate": "2020-10-16T19:40:05.901Z"
            }
    
    
            */
            await this.regroupBySalesforceId(syncId)

            let changes = _.clone(this.getAllChanges())



            let TO_DELETE_WOLIs = []
            let TO_DELETE_TSLIs = []


            const { WOLI_TABLE_NAME, WOLID_TABLE_NAME, TSLI_TABLE_NAME, TS_TABLE_NAME } = tablesNames


            // 1. WOLI_TABLE_NAME
            for (let aChange of changes) {
                if (aChange.changeType === "DELETE" && aChange.target === WOLI_TABLE_NAME && aChange.localId) {
                    aChange.TO_DELETE = true
                    TO_DELETE_WOLIs.push(aChange.localId)
                }

            }

            // 1a. are there any other references to TO_DELETE_WOLIs? Clean them up as well
            if (!_.isEmpty(TO_DELETE_WOLIs)) {

                // for (let aChange of changes.filter(x => _.includes(TO_DELETE_WOLIs, x => x.localId))) {
                //     aChange.TO_DELETE = true
                // }
                for (let aChange of changes) {
                    if (_.includes(TO_DELETE_WOLIs, x => x.localId)) {
                        aChange.TO_DELETE = true
                    }
                }
            }

            // 2. WOLID_TABLE_NAME
            for (let aChange of changes) {

                if (aChange.target === WOLID_TABLE_NAME && aChange.localId) {
                    if (aChange.changeData && aChange.changeData.hasOwnProperty(WOLI_TABLE_NAME) &&
                        _.includes(TO_DELETE_WOLIs, aChange.changeData[WOLI_TABLE_NAME])
                    ) {

                        aChange.TO_DELETE = true
                    }
                }
            }



            // 3. TSLI_TABLE_NAME
            for (let aChange of changes) {
                if (aChange.changeType === "DELETE" && aChange.target === TSLI_TABLE_NAME && aChange.localId) {
                    aChange.TO_DELETE = true
                    TO_DELETE_TSLIs.push(aChange.localId)
                }

            }
            // 3a. are there any other references to TO_DELETE_TSLIs? Clean them up as well
            if (!_.isEmpty(TO_DELETE_TSLIs)) {
                // for (let aChange of changes.filter(x => _.includes(TO_DELETE_TSLIs, x => x.localId))) {
                //     aChange.TO_DELETE = true
                // }
                for (let aChange of changes) {
                    if (_.includes(TO_DELETE_TSLIs, x => x.localId)) {
                        aChange.TO_DELETE = true
                    }
                }
            }

            // 4. TS_TABLE_NAME
            // if (timesheetRecordIdToDelete) {
            //     for (let aChange of changes) {
            //         if (aChange.target === TS_TABLE_NAME && aChange.localId === timesheetRecordIdToDelete) {
            //             aChange.TO_DELETE = true
            //         }
            //     }
            // }

            console.warn("DELETED: ", (changes.filter(x => x.TO_DELETE) || []).length)


            changes = changes.filter(x => !x.TO_DELETE)

            this.resetChanges()
            this.setChanges(syncId, changes)
        } catch (error) {
            //pass
        }
    }

    getAllChanges() {

        
        let changes: [] = Object.values(this.changesPerSyncId).reduce((acc: Array<any>, value: Array<any>) => {
            return acc.concat(Object.values(value))
        }, [])

        try {
            return changes.sort((a, b) => moment(a.CreatedOn) - moment(b.CreatedOn))
        } catch (ex) {
            return changes
        }



    }


    setChanges(syncId: number, changes: []) {
        // console.warn('setChanges syncId', syncId)
        this.changesPerSyncId[syncId] = changes
    }

    getChangesPerSyncId(syncId: number) {
        const _changes = this.changesPerSyncId[syncId] || []
        return JSON.parse(JSON.stringify(_changes))
    }

    //Changes from the prevSyncId must be sent again
    //However, some of them might have been already applied
    //And those should be removed
    async cloneChangesAndAdapt(prevSyncId: number, syncId: number, accepted = []) {
        // console.warn('cloneChangesAndAdapt', prevSyncId, "->", syncId)
        if (!this.changesPerSyncId[syncId]) {
            this.changesPerSyncId[syncId] = []
        }
        
        //1. Collect changes from previous SyncIds
        if (prevSyncId < syncId) {

            //_.range(80, 80)
            //[]
            //_.range(79, 80)
            //[79]
            //_.range(74, 80)
            //[74, 75, 76, 77, 78, 79]]

            for (let _sId of _.range(prevSyncId, syncId)) {
                // console.warn('Collecting changes from #' + _sId)

                if (this.changesPerSyncId[_sId]) {

                    let _previousChanges = JSON.parse(
                        JSON.stringify(this.changesPerSyncId[_sId]))

                    this.changesPerSyncId[syncId] = [].concat(this.changesPerSyncId[syncId], _previousChanges)

                    this.clearChanges(_sId)
                }
            }

        }

        //2. Clear previously accepted changes
        
        this.removePreviouslyAccepted(syncId, accepted)
        
        this._displayAllChangesPerSync()
    }

    @action
    resetChanges() {
        this.changesPerSyncId = {}
        this.changesPerSyncId[0] = []
    }


    async removePreviouslyAccepted(syncId: number, accepted = []) {
        // console.warn('removePreviouslyAccepted', syncId)

        if (this.changesPerSyncId[syncId]) {
            let changes = this.changesPerSyncId[syncId]
            
            // _accepted = ["b365f4bd9b89e3042b2cf11c383501a7"]
            for (let _acc of accepted) {
                //@ts-ignore
                let _index = _.findIndex(changes, x => x.changeId === _acc)

                changes = changes.slice(0, _index).concat(changes.slice(_index + 1, changes.length))

            }
            
            if (_.isEmpty(changes)) {
                //if all the changes were accually accepted
                //then clear all the changes per sync
                this.clearChanges(syncId)
            } else {
                //otherwise, just update it:
                this.changesPerSyncId[syncId] = changes
            }
        }
    }

    _displayAllChangesPerSync() {

        const _allChanges = JSON.parse(JSON.stringify(this.changesPerSyncId))
        const _keys = _.keys(_allChanges)
        if (_.isEmpty(_keys)) {
            console.log('No changes registered')
        } else {
            console.log('Following syncIds changes', _keys)
        }
        if (__DEV__) {
            for (let _key of _keys) {
                console.log('syncId ' + _key + ' : ' + JSON.stringify(_allChanges[_key]))
            }
        }
    }

    @action
    getChanges(syncId: number) {
        //console.warn('getChanges');

        let _processedChanges = []

        let originalChanges = this.getChangesPerSyncId(syncId)

        _processedChanges = originalChanges.map((_change: any) => {
            if (_.startsWith(_change.salesforceId, "__FB__TEMPLATE_TYPE")) {
                let _newChange = JSON.parse(JSON.stringify(_change))
                _newChange.salesforceId = null
                return _newChange
            } else {
                return _change
            }
        })

        try {
            _processedChanges = _processedChanges.sort((a, b) => moment(a.CreatedOn) - moment(b.CreatedOn))
        } catch (error) {

        }


        return _processedChanges
    }


    async clearChanges(syncId: number) {
        if (this.changesPerSyncId[syncId]) {
            _.unset(this.changesPerSyncId, syncId)
        }
    }

    guid() {
        const _guid = this.GUID()
        return R.replace(/-/g, "", _guid)
    }

    GUID() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    @action
    pushChange(syncId: number, change: any) {
        //console.warn('pushChange')
        this.pushChangePerSyncId(syncId, change)
    }

    pushChangePerSyncId(syncId: number, change: any) {
        //console.warn('pushChangePerSyncId')
        if (!this.changesPerSyncId[syncId]) {
            this.changesPerSyncId[syncId] = []
        }
        const _now = moment()
        change.LastUpdate = _now
        change.CreatedOn = _now


        this.changesPerSyncId[syncId].push(change)
    }

    updateChangePerSyncId(syncId: number, changes: any) {
        //console.warn('updateChangePerSyncId')
        if (!this.changesPerSyncId[syncId]) {
            this.changesPerSyncId[syncId] = []
        }
        let _lastUpdate = moment()
        changes.map((x: any) => {
            x.LastUpdate = _lastUpdate;
            if (!x.CreatedOn) {
                //console.warn('CreatedOn added')
                x.CreatedOn = _lastUpdate;
            }
        })
        this.changesPerSyncId[syncId] = changes
    }

    updateChangePerSyncIdAndIndex(syncId: number, existingChange: number, newChange: any) {
        //console.warn('updateChangePerSyncIdAndIndex')

        if (this.changesPerSyncId[syncId] && this.changesPerSyncId[syncId][existingChange] && this.changesPerSyncId[syncId][existingChange].changeData) {

            let _existingChangeType = this.changesPerSyncId[syncId][existingChange].changeType
            let _newChangeType = newChange.changeType

            if (_newChangeType !== _existingChangeType && _newChangeType === 'DELETE') {
                newChange.LastUpdate = moment()
                this.changesPerSyncId[syncId][existingChange] = newChange

            } else {
                let _createdOn = this.changesPerSyncId[syncId][existingChange].CreatedOn


                const _newChangeData = Object.assign({}, this.changesPerSyncId[syncId][existingChange].changeData, newChange.changeData)
                this.changesPerSyncId[syncId][existingChange].LastUpdate = moment()
                this.changesPerSyncId[syncId][existingChange].CreatedOn = _createdOn
                this.changesPerSyncId[syncId][existingChange].changeData = _newChangeData
            }
        }

    }

    updateChangePerSyncIdAndIndexAndQty(syncId: number, existingChange: number, newChange: any, qtyFieldName: string, qty: number) {
        //console.warn('updateChangePerSyncIdAndIndexAndQty')
        if (this.changesPerSyncId[syncId] && this.changesPerSyncId[syncId][existingChange] && this.changesPerSyncId[syncId][existingChange].changeData) {
            let _createdOn = this.changesPerSyncId[syncId][existingChange].CreatedOn
            const _newChangeData = Object.assign({}, this.changesPerSyncId[syncId][existingChange].changeData, newChange.changeData)
            this.changesPerSyncId[syncId][existingChange].changeData = _newChangeData
            this.changesPerSyncId[syncId][existingChange].LastUpdate = moment()
            this.changesPerSyncId[syncId][existingChange].CreatedOn = _createdOn
            this.changesPerSyncId[syncId][existingChange].changeData[qtyFieldName] = qty
        }
    }

    popChange(syncId: number) {
        // console.warn('popChange')
        return this.popChangePerSyncId(syncId)
    }

    popChangePerSyncId(syncId: number) {
        // console.warn('popChangePerSyncId')
        if (this.changesPerSyncId[syncId]) {
            return this.changesPerSyncId[syncId].pop()
        }
        return null
    }

    @action
    async addChanges(syncId: number, newChanges: [], changeType = "UPDATE", isLocal = false, parentReference = null) {
        // console.warn('addChanges', syncId)

        try {
            const { loginStore } = this.rootStore
            let _formattedChanges = newChanges.map(_aChange => {
                let y = JSON.parse(JSON.stringify(_aChange))
                _.unset(y, "Id")
                _.unset(y, "target")

                let _changeData = Object.assign({}, {
                    ...y
                })

                if (!isLocal) {

                    isLocal = _aChange
                        //@ts-ignore
                        .Id
                        .indexOf("__FB__TEMPLATE_TYPE_") >= 0
                }

                let _changeType = changeType

                if (isLocal && _changeType !== "DELETE") {
                    _changeType = "INSERT"
                }

                //@addChanges
                let _change = isLocal
                    ? {
                        "salesforceId": null,
                        //@ts-ignore
                        "localId": _aChange.Id,
                        //@ts-ignore
                        "target": _aChange.target,
                        "changeType": _changeType,
                        "changeId": this.guid(),
                        "changeData": _changeData
                    }
                    : {
                        //@ts-ignore
                        "salesforceId": _aChange.Id,
                        //@ts-ignore
                        "target": _aChange.target,
                        "changeType": changeType,
                        "changeId": this.guid(),
                        "changeData": _changeData
                    }

                if (parentReference !== null) {
                    _change["parentReference"] = parentReference
                }

                return _change
            })

            this.addChangeToHistory(_formattedChanges)

            const hasChanges = this.getChangesPerSyncId(syncId).length > 0

            if (hasChanges) {
                for (let _newChange of _formattedChanges) {
                    const existingChange = _.findIndex(this.getChangesPerSyncId(syncId), x => isLocal
                        //@ts-ignore
                        ? x.localId == _newChange.localId
                        //@ts-ignore
                        : x.salesforceId === _newChange.salesforceId)

                    if (existingChange >= 0) {
                        // this.changes[existingChange].changeType = _newChange.changeType !!!
                        this.updateChangePerSyncIdAndIndex(syncId, existingChange, _newChange)

                    } else {
                        this.pushChangePerSyncId(syncId, _newChange)
                    }
                }
                loginStore.setHasCacheChanged = true

            } else {
                this.updateChangePerSyncId(syncId, _formattedChanges)
                loginStore.setHasCacheChanged = true
            }
            return { type: "success" }
        } catch (error) {
            
            // console.warn("addChanges error", syncId, error)
            return { type: "error" }
        }
    }

    @action
    async addItemChanges(syncId: number, newChanges: [], qtyFieldName: string, parentReference?: string) {
        // console.warn('addItemChanges', syncId)
        try {
            let _formattedChanges = newChanges.map((_aChange: Object) => {

                let y = JSON.parse(JSON.stringify(_aChange))

                const _localId = y.Id
                _.unset(y, "Id")
                _.unset(y, "target")
                _.unset(y, "__FB__TEMPLATE_TYPE")

                let _changeData = Object.assign({}, {
                    ...y
                })

                let _cleanChangeData = JSON.parse(JSON.stringify(_changeData))
                _.unset(_cleanChangeData, "localId")
                //@addItemChanges
                let json = {
                    //@ts-ignore
                    "salesforceId": _aChange.Id,
                    "localId": _localId,
                    //@ts-ignore
                    "target": _aChange.target,
                    "changeType": "INSERT",
                    "changeId": this.guid(),
                    "changeData": _cleanChangeData
                }

                if (parentReference) {
                    //@ts-ignore
                    json.parentReference = parentReference
                }

                return json
            })

            this.addChangeToHistory(_formattedChanges)

            const hasChanges = this.getChangesPerSyncId(syncId).length > 0

            if (hasChanges) {

                _formattedChanges.map((_newChange: Object) => {
                    //@ts-ignore
                    const existingChange = _.findIndex(this.getChangesPerSyncId(syncId), x => x.salesforceId === _newChange.salesforceId)

                    if (existingChange >= 0) {
                        //@ts-ignore
                        const qty = this.getChangesPerSyncId(syncId)[existingChange].changeData[qtyFieldName] + _newChange.changeData[qtyFieldName]
                        this.updateChangePerSyncIdAndIndexAndQty(syncId, existingChange, _newChange, qtyFieldName, qty)

                    } else {

                        this.pushChangePerSyncId(syncId, _newChange)
                    }

                })

            } else {

                this.updateChangePerSyncId(syncId, _formattedChanges)
            }

            return { type: "success" }
        } catch (error) {

            return { type: "error" }

        }
    }

    @action
    async addLineItemChanges(syncId: number, newChanges: []) {
        // console.warn('addLineItemChanges ', syncId)
        try {
            let _formattedChanges = newChanges.map((_aChange: Object) => {

                let y = JSON.parse(JSON.stringify(_aChange))

                const _localId = y.Id
                _.unset(y, "Id")
                _.unset(y, "target")

                _.unset(y, "__FB__TEMPLATE_TYPE")

                let _changeData = Object.assign({}, {
                    ...y
                })

                let _cleanChangeData = JSON.parse(JSON.stringify(_changeData))
                _.unset(_cleanChangeData, "localId")
                //@addLineItemChanges
                let json = {
                    //@ts-ignore
                    "salesforceId": _aChange.Id,
                    "localId": _localId,
                    //@ts-ignore
                    "parentReference": _aChange.parentReference,
                    //@ts-ignore
                    "target": _aChange.target,
                    "changeType": "INSERT",
                    "changeId": this.guid(),
                    "changeData": _cleanChangeData
                }

                return json
            })

            this.addChangeToHistory(_formattedChanges)

            const hasChanges = this.getChangesPerSyncId(syncId).length > 0

            if (hasChanges) {
                _formattedChanges.map(_newChange => {
                    //@ts-ignore
                    const existingChange = _.findIndex(this.getChangesPerSyncId(syncId), x => x.salesforceId === _newChange.salesforceId)
                    if (existingChange >= 0) {

                        this.updateChangePerSyncIdAndIndex(syncId, existingChange, _newChange)
                    } else {

                        this.pushChangePerSyncId(syncId, _newChange)
                    }

                })

            } else {
                this.updateChangePerSyncId(syncId, _formattedChanges)
            }

            return { type: "success" }
        } catch (error) {
            console.warn("addChanges error", syncId, error)
            return { type: "error" }

        }

    }

    @action
    async addLineItemParentRefChanges(syncId: number, newChanges: []) {
        // console.warn('addLineItemParentRefChanges', syncId)
        try {
            let _formattedChanges = newChanges.map((_aChange: any) => {

                let y = JSON.parse(JSON.stringify(_aChange))

                const _localId = y.Id
                _.unset(y, "Id")
                _.unset(y, "target")
                _.unset(y, "parentReference")
                _.unset(y, "__FB__TEMPLATE_TYPE")

                let _changeData = Object.assign({}, {
                    ...y
                })

                let _cleanChangeData = JSON.parse(JSON.stringify(_changeData))
                _.unset(_cleanChangeData, "localId")
                //@addLineItemParentRefChanges
                if (_cleanChangeData[_aChange.parentReference] && _cleanChangeData[_aChange.parentReference].indexOf("__FB__TEMPLATE_TYPE") < 0) {
                    let json2 = {
                        "salesforceId": _aChange.Id,
                        "localId": _localId,

                        "target": _aChange.target,
                        "changeType": "INSERT",
                        "changeId": this.guid(),
                        "changeData": _cleanChangeData
                    }

                    return json2
                }

                let json = {
                    "salesforceId": _aChange.Id,
                    "localId": _localId,
                    "parentReference": _aChange.parentReference,
                    "target": _aChange.target,
                    "changeType": "INSERT",
                    "changeId": this.guid(),
                    "changeData": _cleanChangeData
                }

                return json
            })

            this.addChangeToHistory(_formattedChanges)

            const hasChanges = this.getChangesPerSyncId(syncId).length > 0

            if (hasChanges) {
                _formattedChanges.map(_newChange => {
                    const existingChange = _.findIndex(this.getChangesPerSyncId(syncId), (x: any) => x.salesforceId === _newChange.salesforceId)
                    if (existingChange >= 0) {
                        this.updateChangePerSyncIdAndIndex(syncId, existingChange, _newChange)
                    } else {
                        this.pushChangePerSyncId(syncId, _newChange)
                    }
                })
            } else {
                this.updateChangePerSyncId(syncId, _formattedChanges)
            }
            return { type: 'success' }
        } catch (error) {
            console.warn("addLineItemParentRefChanges error", syncId, error)
            return { type: 'error' }
        }
    }
    /**
     * Mobx Root Store
     */
    rootStore: any

}
