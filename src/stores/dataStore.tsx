import {

    action,
    computed,
    observable,
} from 'mobx'
import _ from 'lodash'
import jsonQuery from 'json-query'

import { RecordTypes } from '../types'

import { WorkOrderLineItem, InstalledProduct, InstalledProductWithStatus, upsertScreenModes } from '../types'
import { BackupInfo } from '../types'
import moment from 'moment';
import { SalesForcePlatformState } from '../types'
type OnlineSource = 'ONLINE_DATA' | '';

export type FLocation = {
    IsActive: boolean, 

    Id : string,
    Name : string,
    FIELDBUDDY__Description__c : string,
    // FIELDBUDDY__Location__c : REFERENCE,
    // FIELDBUDDY__Account__c : REFERENCE,
    // RecordTypeId : REFERENCE,
    //FIELDBUDDY__Coordinates__c : LOCATION,
    FIELDBUDDY__Coordinates__Longitude__s? : number,
    FIELDBUDDY__Coordinates__Latitude__s? : number,
    Timestamp__c? : Date,
    Accuracy__c? : number,
    
    FIELDBUDDY__Street__c? : string,
    FIELDBUDDY__House_Number__c? : number,
    FIELDBUDDY__Postal_Code__c? : string,
    FIELDBUDDY__House_Number_Suffix__c? : string,
    FIELDBUDDY__City__c? : string,
    FIELDBUDDY__Country__c? : string,
    DeveloperName: string //Record Type developer name
}

export type FGeoMarker = {
    Pin?: FLocation, //if Pin is empty, you should display '+'
    Title: string,
    Subtitle: string
    //Add more as you wish, e.g. icon, color?
}


export default class DataStore {
    constructor(rootStore: any) {
        this.idsReceivedFromBE = []
        this.rootStore = rootStore
        this.activeItems = []
        this.contextData = {}
        this.onlineData = {}
        this.backups = []
        this.platformStatus = []
        this.classicValidationRulesDataContext = {}
        this.markers= []

        this.dashboardWorkOrders = null
    }

    /**
     * Mobx Root Store
     */
    rootStore: any

    /**
     * Used to link the navigation to the current actives objects Ex: Work Order -> Installed Product -> WOLI -> WOLID
     */
    activeItems: Array<any>

    /**
     * Used as context data for the active object. ( Data Tree )
     */

    contextData: any
    /**
         * Used as context meta data legacy field in Details as Checklist mode ( Data Tree )
         */
    metaFieldsLegacy: any
    

    @observable
    activeMarker: Number = 0


    @observable
    markers: Array <FGeoMarker> | null

    get getMarkers() : Array<FGeoMarker> | null {
        return this.markers
    }

    get getActiveMarkerIndex() : number {
        if((this.markers || []).length ===0){
            return -1;
        }
        const _activeIndex = _.findIndex(this.markers, x=> {
            if(x.hasOwnProperty("Pin")){
                return x.Pin.IsActive
            }
            return false
        });
        return _activeIndex;
    }

    @action
    setActiveMarkerIndex(ni?: number) {
        if ((this.markers || []).length === 0) {
            return;
        }
        if (ni === this.getActiveMarkerIndex) {
            return;
        }

        if (ni >= 0 && ni < (this.markers || []).length) {

            let ci = this.getActiveMarkerIndex
            
            //1. remove current index
            for (let i = 0; i < (this.markers || []).length; i++) {
                if (ci === i) {
                    let pin = this.markers[i].Pin;
                    pin.IsActive = false;
                    this.markers[i].Pin = pin;
                    
                }
                if (ni === i) {
                    let pin = this.markers[i].Pin;
                    pin.IsActive = true;
                    this.markers[i].Pin = pin;
                    
                }

            }
            this.activeMarker = ni

        }

    }

    @action
    async setMarkers(locations: Array<any>, template: string, installedProduct?: any) {
        if (template === "Service_Request") {
            await this.setMarkersForSRbWO(locations, installedProduct)
        } else if (template === "Location") {
            await this.setMarkersForLbWO(locations)
        } else if (template === "InstalledProduct") {
            await this.setMarkersForInstalledProduct(locations, installedProduct)
        }
    }

    @action
    async setMarkersForSRbWO(locations: Array<any>, installedProduct?: any ){
       
       let _markers : Array<FGeoMarker> = []
        
        const  L =(locations || []).length

        for(let loc of locations){
            let _l  : FLocation=
            {
                Id: loc.Id,

                Name: loc.Name,
                FIELDBUDDY__Description__c: loc.FIELDBUDDY__Description__c,

                FIELDBUDDY__Coordinates__Longitude__s: loc.FIELDBUDDY__Coordinates__Longitude__s,
                FIELDBUDDY__Coordinates__Latitude__s: loc.FIELDBUDDY__Coordinates__Latitude__s,
                Timestamp__c: loc.Timestamp__c,
                Accuracy__c: loc.Accuracy__c,

                FIELDBUDDY__Street__c: loc.FIELDBUDDY__Street__c,
                FIELDBUDDY__House_Number__c: loc.FIELDBUDDY__House_Number__c,
                FIELDBUDDY__Postal_Code__c: loc.FIELDBUDDY__Postal_Code__c,
                FIELDBUDDY__House_Number_Suffix__c: loc.FIELDBUDDY__House_Number_Suffix__c,
                FIELDBUDDY__City__c: loc.FIELDBUDDY__City__c,
                FIELDBUDDY__Country__c: loc.FIELDBUDDY__Country__c,
                
                IsActive: false
            } 

            let _title = ""
            if (loc.hasOwnProperty("RecordType")) {
                _l.DeveloperName = loc.RecordType.DeveloperName
                if (_l.DeveloperName.toLocaleLowerCase() === 'location') {
                    _l.IsActive = true
                }
                if (_l.DeveloperName.toLocaleLowerCase() === 'sub_location') {
                    _title = loc.title
                    // _l.IsActive = true
                }
            } else if (loc.hasOwnProperty("localId")) { //this is for new local sub locations
                _title = loc.title
                // _l.IsActive = true
            } 

            if(L === 1){
                _l.IsActive = true
            }

            
            let _g = {
                Pin: _l,
                Title: _title
            } 

            // if(typeof _g.Pin['FIELDBUDDY__Street__c'
            // ] === 'undefined') {
            //     return 
            // } else {
            //     _markers.push(_g)
            // }
            _markers.push(_g)
        }
        
        this.markers = _markers
        
        // let inx = this.getActiveMarkerIndex
        
        // this.setAcitveMarkerIndex(1)
        
        // inx = this.getActiveMarkerIndex        

    }

    @action
    async setMarkersForInstalledProduct(locations: Array<any>, installedProduct?: any ){
        
       let _markers : Array<FGeoMarker> = []
        
       const  L =(locations || []).length

        for(let loc of locations){
            let _l  : FLocation=
            {
                Id: loc.Id,

                Name: loc.Name,
                FIELDBUDDY__Description__c: loc.FIELDBUDDY__Description__c,

                FIELDBUDDY__Coordinates__Longitude__s: loc.FIELDBUDDY__Coordinates__Longitude__s,
                FIELDBUDDY__Coordinates__Latitude__s: loc.FIELDBUDDY__Coordinates__Latitude__s,
                Timestamp__c: loc.Timestamp__c,
                Accuracy__c: loc.Accuracy__c,

                FIELDBUDDY__Street__c: loc.FIELDBUDDY__Street__c,
                FIELDBUDDY__House_Number__c: loc.FIELDBUDDY__House_Number__c,
                FIELDBUDDY__Postal_Code__c: loc.FIELDBUDDY__Postal_Code__c,
                FIELDBUDDY__House_Number_Suffix__c: loc.FIELDBUDDY__House_Number_Suffix__c,
                FIELDBUDDY__City__c: loc.FIELDBUDDY__City__c,
                FIELDBUDDY__Country__c: loc.FIELDBUDDY__Country__c,
                
                IsActive: false
            } 

            let _title = ""
            if (loc.hasOwnProperty("RecordType")) {
                _l.DeveloperName = loc.RecordType.DeveloperName                
                if(_l.DeveloperName.toLocaleLowerCase() === 'sub_location'){
                    _title = installedProduct.Name
                    _l.IsActive = true
                }

            } else {
                _title = installedProduct.Name
                _l.IsActive = true
            }

            if(L === 1){
                _l.IsActive = true
            }

            
            let _g = {
                Pin: _l,
                Title: _title
            
            } 
            
            
            _markers.push(_g)
        }
        
        this.markers = _markers
        
        // let inx = this.getActiveMarkerIndex        
        // this.setAcitveMarkerIndex(1)        
        // inx = this.getActiveMarkerIndex        

    }

    @action
    async setMarkersForLbWO(locations: Array<any>, installedProduct?: any ){
        ///this.markers = ...
       let _markers : Array<FGeoMarker> = []
        
        for(let loc of locations){
            let _l : FLocation=
            {
                Id: loc.Id,

                Name: loc.Name,
                FIELDBUDDY__Description__c: loc.FIELDBUDDY__Description__c,

                FIELDBUDDY__Coordinates__Longitude__s: loc.FIELDBUDDY__Coordinates__Longitude__s,
                FIELDBUDDY__Coordinates__Latitude__s: loc.FIELDBUDDY__Coordinates__Latitude__s,
                Timestamp__c: loc.Timestamp__c,
                Accuracy__c: loc.Accuracy__c,

                FIELDBUDDY__Street__c: loc.FIELDBUDDY__Street__c,
                FIELDBUDDY__House_Number__c: loc.FIELDBUDDY__House_Number__c,
                FIELDBUDDY__Postal_Code__c: loc.FIELDBUDDY__Postal_Code__c,
                FIELDBUDDY__House_Number_Suffix__c: loc.FIELDBUDDY__House_Number_Suffix__c,
                FIELDBUDDY__City__c: loc.FIELDBUDDY__City__c,
                FIELDBUDDY__Country__c: loc.FIELDBUDDY__Country__c,
                
                IsActive: true
            } 

            let _title = ""
            if (loc.hasOwnProperty("RecordType")) {
                _l.DeveloperName = loc.RecordType.DeveloperName
                if(_l.DeveloperName.toLocaleLowerCase() === 'location'){
                    _l.IsActive = true
                }
                if(_l.DeveloperName.toLocaleLowerCase() === 'sub_location'){
                    _title = loc?.title
                }

            }

            
            let _g = {
                Pin: _l,
                Title: _title
            
            } 
            
            
            _markers.push(_g)
        }
        
        this.markers = _markers
        
        // let inx = this.getActiveMarkerIndex
        
        // this.setAcitveMarkerIndex(1)
        
        // inx = this.getActiveMarkerIndex        

    }


    /**
     * Used to ensure updated data in Dashboard
     */
    @observable
    dashboardWorkOrders: Array<any> | null

    // getDashboardWorkOrders() : Array<any> {

    //     if(this.dashboardWorkOrders === null){
    //         this.setDashboardWorkOrders()
    //     }

    //     return this.dashboardWorkOrders || []
    // }

    clearDashboardWorkOrders() {
        // console.warn("clearDashboardWorkOrders dataStore")        
        this.dashboardWorkOrders = null
    }

    
    setDashboardWorkOrders(dashboardWorkOrders: Array<any>) {
        // console.warn("setDashboardWorkOrders dataStore")        
        this.dashboardWorkOrders = dashboardWorkOrders
    }


    /**
     * Used to mark last update on formula results
     */
    @observable
    lastUpdate

    /**
     * Used as a container for online results. This data is lost after its usage.
     */
    onlineData: any

    backups: Array<BackupInfo>

    /**
     * SF ids received on the last payload
     */
    @observable
    idsReceivedFromBE: Array<string>

    @action
    setBackups(backups: Array<BackupInfo>) {
        this.backups = backups
    }

    @observable
    _woliStatus: string

    set status(status: string) {
        this._woliStatus = status
    }

    get status() {
        return this._woliStatus
    }

    /*
    classic validationRules
    */
    @observable
    classicValidationRulesDataContext: any

    set setClassicValidationRulesDataContext(dataContext: any) {
        
        this.classicValidationRulesDataContext = dataContext
    }
    /**
     * SF limits received in each response
     */
    @observable
    platformStatus: Array<SalesForcePlatformState>

    @action
    setPlatformStatus(newStatus: Array<SalesForcePlatformState>) {
        this.platformStatus = newStatus
    }

    @action
    newBackup(status: string) {
        try {
            let backup = {
                timestamp: `${moment().format()}`,
                status: status
            }

            if (this.backups) {

                if (this.backups.length >= 10) {
                    this.backups.shift()
                }

                this.backups.push(backup)

            } else {
                this.backups = [backup]
            }

        } catch (err) {
            // Sentry.Native.captureException(new Error("Failed Backup."), {
            //     logger: 'dataStore',
            //     context: 'backup'
            // });
        }
    }

    @action
    async backup() {
        const { settingsStore, loginStore } = this.rootStore
        const { autobackup, interval } = settingsStore.rules.backup
        let timeInMinutes = interval * 60

        if (_.isEmpty(this.backups) && autobackup) {
            try {
                loginStore._backupAction()
            } catch (err) {
                debugger
            }
        }

        if (autobackup && !_.isEmpty(this.backups)) {
            const { timestamp } = _.last(this.backups)
            let momentLastBackup = moment(timestamp)
            let timeDifference = moment().diff(momentLastBackup, 'minutes')
            if (timeDifference >= timeInMinutes) {
                try {
                    loginStore._backupAction()
                } catch (err) {
                    debugger
                }
            }
        }
    }

    @action
    setIdsReceivedFromBE(ids: Array<string>) {
        this.idsReceivedFromBE = ids
    }

    /**
     * 
     */
    @computed
    get dataDiff(): Array<string> {
        try {
            return _.difference(this.allIds, this.idsReceivedFromBE)
        } catch (err) {
            debugger
            return []

        }
    }

    @computed
    get integralIds(): boolean {
        try {
            const { changesStore } = this.rootStore

            if (_.isEmpty(this.idsReceivedFromBE)) return true
            let corruptedData = this.dataDiff.filter((id: string) => {
                let changes = changesStore.getAllChanges()
                if (!_.isEmpty(changes)) {
                    let changesIds = changes.map((change: any) => change.localId ? change.localId : null)
                    if (changesIds.includes(id)) {
                        return false
                    }
                }
                return true
            })

            const a = this.dataDiff
            const b = changesStore.getAllChanges().map((change: any) => change.localId ? change.localId : null)

            if (!_.isEmpty(corruptedData)) {
                debugger
            }
            return _.isEmpty(corruptedData)

        } catch (err) {
            return false
        }
    }

    @computed
    get integralMetaData(): boolean {
        try {
            const { metaStore, loginStore } = this.rootStore
            const { meta } = metaStore
            const { data } = loginStore

            const metaTables = Object.keys(meta)
            let results = metaTables.map((metaTable: string) => {
                let tableData = data[metaTable]
                if (!_.isEmpty(tableData)) {
                    let _results = tableData.map((table: object) => {
                        const metaFields = meta[metaTable].fields.map((field: object) => { return field.name ? field.name : "" })
                        let dataFields = Object.keys(table)
                        dataFields = dataFields.filter((field: string) => field != "attributes") // Default
                        dataFields = dataFields.filter((field: string) => field != "LastModifiedDate") // Default
                        dataFields = dataFields.filter((field: string) => field != "UserRecordAccess") // Default
                        dataFields = dataFields.filter((field: string) => field != "RecordType") // using a record with record types
                        dataFields = dataFields.filter((field: string) => field != "isOpen") // FBSWIFT-2675 -> on rendering an IP in a LOC based WO
                        dataFields = dataFields.filter((field: string) => field != "localId") // Adding an item with details
                        dataFields = dataFields.filter((field: string) => field != "__FB__TEMPLATE_TYPE") // Adding an item with details
                        dataFields = dataFields.filter((field: string) => field != "undefined") // FBSWIFT-2676 -> Adding a TSLI
                        dataFields = dataFields.filter((field: string) => field != "parentReference") // FBSWIFT-2677 -> Adding a TSLI
                        dataFields = dataFields.filter((field: string) => field != "target") // FBSWIFT-2677 -> Closing a WO
                        dataFields = dataFields.filter((field: string) => field != "Signature__FB") // Closing a WO

                        // Special Cases
                        const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
                        const WO = loginStore.getPrefixedFieldName("Work_Order__c")
                        if (table.attributes.type == IP) {
                            dataFields = dataFields.filter((field: string) => field != WO) // FBSWIFT-2679 -> Opening an IP on a LOC based WO
                        }
                        let diff = _.difference(dataFields, metaFields)



                        return _.isEmpty(diff)
                    })


                    return _results.reduce((acc: boolean, value: boolean) => { return acc && value }, true)
                }
                return true
            })

            return results.reduce((acc: boolean, value: boolean) => { return acc && value }, true)

        } catch (err) {
            return false
        }
    }

    @computed
    get integralData(): boolean {
        try {

            return this.integralIds && this.integralMetaData
        } catch (err) {
            return false
        }
    }

    /**
     * Assigns data to onlineData
     * @param {any} data 
     */
    @action
    addOnlineData(data = {}) {
        try {
            this.onlineData = data
        } catch (e) {
            this.onlineData = {}
            // Sentry.captureException(new Error("Not able to set online data."), {
            // Sentry.Native.captureException(new Error("Not able to set online data."), {
            //     logger: 'dataStore',
            //     context: 'addOnlineData'
            // });
        }
    }

    /**
     * Assign a context data
     * @param {any} item 
     */
    @action
    addContextItem(item = {}) {
        try {
            this.contextData = item

        } catch (e) {
            this.contextData = {}
            // Sentry.captureException(new Error("Not able to set context data."), {
            // this.contextData = {}
            // Sentry.Native.captureException(new Error("Not able to set context data."), {
            //     logger: 'dataStore',
            //     context: 'addContextItem'
            // });
        }
        finally {


        }
    }

    /**
     * Assign a context data
     * @param {any} item 
     */
    @action
    addContextItemInWoliUpsertMode(item = {}) {
        try {
            let _data = this.contextData.data
            let _wolids = this.contextData.wolids
            this.contextData = item

            
            
            if (_data) {
                if (this.contextData.data !== _data) {
                    this.contextData.data = _data
                }
            }
            if (_wolids) {
                if (this.contextData.wolids !== _wolids) {
                    
                    this.contextData.wolids = _wolids
                }
            }

        } catch (e) {
            this.contextData = {}
            // Sentry.captureException(new Error("Not able to set context data."), {
            // this.contextData = {}
            // Sentry.Native.captureException(new Error("Not able to set context data."), {
            //     logger: 'dataStore',
            //     context: 'addContextItem'
            // });
        }
        finally {


        }
    }


    /**
    * Assign a context metaFieldsLegacy
    * @param {any} item 
    */
    @action
    setMetaFieldsLegacy(item = {}) {
        try {
            this.metaFieldsLegacy = item

        } catch (e) {
            this.metaFieldsLegacy = {}

        }
    }

    @action
    callCalcFilterLogics(): any {
        try {
            if (this.contextData && this.contextData.hasOwnProperty("dataFieldsFromTemplate")) {
                const _allFilterLogicApiNames = (this.contextData.dataFieldsFromTemplate || []).filter(x => x.Filter_Logic__c).map(x => x.API_Name__c)
                if (!_.isEmpty(_allFilterLogicApiNames)) {
                    _allFilterLogicApiNames.map(apiName => {
                        this.calcFilterLogic(apiName);
                    })
                }
            }
        } catch (error) {
            //pass       
        }
    }

    @action
    calcFilterLogic(apiName: string): any {

        let __FORMULA__ = null
        const INVALID_FORMULA = this.rootStore.loginStore.getLabel("Check formula")
        try {

            // if (this.contextData.wolids) {


            const API_NAME = this.rootStore.loginStore.getPrefixedFieldNameByPackageVersion("API_Name__c")
            const FILTER_LOGIC = this.rootStore.loginStore.getPrefixedFieldNameByPackageVersion("Filter_Logic__c")

            const VALUE = this.rootStore.loginStore.getPrefixedFieldName("Value__c")
            if (!this.contextData.wolids) {
                this.contextData.wolids = []
            }

            const { dataFieldsFromTemplate, wolids } = this.contextData


            let _apiNamedFields = wolids.filter((x: any) => x[API_NAME] === apiName)

            let formulaField: any = _.first(_apiNamedFields)
            if (!formulaField) {

                formulaField = _.clone(_.first((dataFieldsFromTemplate || []).filter((x: any) => x[API_NAME] === apiName)))
                // return true
            }
            let otherWolids = wolids.filter((x: any) => x[API_NAME] !== apiName)
            if (_.isEmpty(otherWolids)) {

                otherWolids = _.clone((dataFieldsFromTemplate || []).filter((x: any) => x[API_NAME] !== apiName))
            }


            if (formulaField[API_NAME] && formulaField[FILTER_LOGIC]) {

                let p = {}
                const _apiNames = otherWolids.map((x: any) => x[API_NAME])

                //1. check what apiNames are needed for this formula
                /*
                (Sum1)=>Sum1!=10

                */
                __FORMULA__ = formulaField[FILTER_LOGIC]

                try {
                    /*
                    {
                    "executor": "default",
                    "type": "validation",
                    "alertType": "warning",
                    "alertMessage": "${Label.NoActivities}",
                    "alertDefaultText": "Sum can't be 10",
                    "formula": "(Sum1)=>Sum1===10"
                    }
                    */
                    const _objFormula = JSON.parse(formulaField[FILTER_LOGIC])

                    if (typeof _objFormula === 'object') {
                        __FORMULA__ = _objFormula.formula

                    }
                } catch (error) {

                }




                //2. is any apiName missing
                let allPresent = true

                const results = _.split(__FORMULA__, "=>")

                if (results.length > 0) {

                    const _neededApiNames = results[0].replace("(", "").replace(")", "").split(",").map(x => x.trim())

                    for (let _apiName of _neededApiNames) {
                        if (!_.includes(_apiNames, _apiName)) {
                            allPresent = false
                        }
                    }
                    // all params are needed otherwise 
                    if (allPresent) {

                        //3. get values by apiName
                        for (let _apiName of _neededApiNames) {
                            p[_apiName] = null
                            try {
                                const _apiNameIndex = _.findLastIndex(otherWolids, (x: any) => x[API_NAME] === _apiName)
                                if (_apiNameIndex >= 0) {
                                    p[_apiName] = otherWolids[_apiNameIndex][VALUE]
                                    // const rightWolids = wolids.filter((wolid: any) => wolid[API_NAME] === _apiName)
                                    // if (_.isEmpty(rightWolids)) {
                                    //     this.contextData.wolids.push(otherWolids[_apiNameIndex])
                                    // }
                                    if (p[_apiName] === undefined || p[_apiName] === INVALID_FORMULA) {
                                        p[_apiName] = 0
                                    }
                                }
                            } catch (error) {
                                //ignore
                            }

                        }
                        //4. call formula with the paramaters 
                        const _params = _.values(p).join()

                        if (_.values(p).length === _neededApiNames.length) {
                            // console.warn(_params);

                            if (_.includes(_params, INVALID_FORMULA)) {

                                return {
                                    result: true,
                                    rule: INVALID_FORMULA
                                }
                            }

                            const _formula = __FORMULA__
                            const _callableFunction = `f=${_formula};f(${_params});`
                            //5. call function and store values 



                            const _result = eval(_callableFunction)



                            let _r = {
                                result: !!_result,
                                rule: formulaField[FILTER_LOGIC]
                            }

                            return _r
                        }

                    }


                }
            }

            // }


        } catch (error) {

            debugger
        }



        return {
            result: true,
            rule: __FORMULA__
        }
    }

    @action
    calcFormula(apiName: string): any {
        const INVALID_FORMULA = this.rootStore.loginStore.getLabel("Check formula")
        try {
            let FORMULA = this.rootStore.loginStore.getPrefixedFieldNameByPackageVersion("Formula__c")
            const API_NAME = this.rootStore.loginStore.getPrefixedFieldNameByPackageVersion("API_Name__c")


            const VALUE = this.rootStore.loginStore.getPrefixedFieldName("Value__c")

            if (!this.contextData.wolids) {
                this.contextData.wolids = []
            }
            const { dataFieldsFromTemplate, wolids } = this.contextData

            let formulaField: any = _.first(wolids.filter((x: any) => x[API_NAME] === apiName))
            if (!formulaField) {
                formulaField = _.clone(_.first((dataFieldsFromTemplate || []).filter((x: any) => x[API_NAME] === apiName)))
            }

            let otherWolids = wolids.filter((x: any) => x[API_NAME] !== apiName)
            if (_.isEmpty(otherWolids)) {
                otherWolids = _.clone((dataFieldsFromTemplate || []).filter((x: any) => x[API_NAME] !== apiName))
            }

            if (formulaField[API_NAME] && formulaField[FORMULA]) {
                let p = {}
                const _apiNames = otherWolids.map((x: any) => x[API_NAME])

                //1. check what apiNames are needed for this formula
                //2. is any apiName missing
                let allPresent = true
                const results = _.split(formulaField[FORMULA], "=>")

                if (results.length > 0) {

                    const _neededApiNames = results[0].replace("(", "").replace(")", "").split(",").map(x => x.trim())
                    for (let _apiName of _neededApiNames) {
                        if (!_.includes(_apiNames, _apiName)) {
                            allPresent = false
                        }
                    }

                    if (allPresent) {

                        //3. get values by apiName
                        for (let _apiName of _neededApiNames) {
                            p[_apiName] = null
                            try {

                                const _apiNameIndex = _.findLastIndex(otherWolids, (x: any) => x[API_NAME] === _apiName)
                                if (_apiNameIndex >= 0) {
                                    if (otherWolids[_apiNameIndex][VALUE]) {
                                        p[_apiName] = otherWolids[_apiNameIndex][VALUE]

                                        const rightWolids = wolids.filter((wolid: any) => wolid[API_NAME] === _apiName)
                                        if (_.isEmpty(rightWolids)) {
                                            this.contextData.wolids.push(otherWolids[_apiNameIndex])
                                        }
                                    }
                                }
                            } catch (error) {
                                //ignore
                            }
                        }

                        //4. call formula with the paramaters
                        const _params = _.values(p).join()
                        const _formula = formulaField[FORMULA]
                        const _callableFunction = `f=${_formula};f(${_params});`

                        //5. call function and store values 
                        const _result = eval(_callableFunction)

                        if (isNaN(_result)) {
                            if (typeof (_result) === "string") {
                                formulaField[VALUE] = _result
                            } else {
                                formulaField[VALUE] = INVALID_FORMULA
                            }
                        } else {
                            formulaField[VALUE] = _result
                        }
                    }
                    else {
                        formulaField[VALUE] = INVALID_FORMULA
                    }


                    if (!this.contextData.wolids) {
                        this.contextData.wolids = []
                    }

                    const _wolidIndex = _.findIndex(this.contextData.wolids, (x: any) => x.Id === formulaField.Id)
                    if (_wolidIndex >= 0) {
                        this.contextData.wolids[_wolidIndex][VALUE] = formulaField[VALUE]
                    } else {
                        this.contextData.wolids.push(formulaField)
                    }

                    return formulaField[VALUE]
                }
            }

        } catch (error) {
            console.warn('e: ' + error);

        }

        return INVALID_FORMULA
    }

    @action
    renewLastUpdate() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
        }
        this.lastUpdate = s4()
    }

    /**
     * 
     * @param {string} Id Sales Force ID
     * @param {string} source one of -> "ONLINE_DATA"
     */
    @action
    getItemById(Id: string, source: OnlineSource) {
        const { loginStore } = this.rootStore
        if (source === "ONLINE_DATA") {
            let queryResult = jsonQuery(`[**][recordId=${Id}]`, { data: this.onlineData }).value
            if (!_.isEmpty(queryResult)) {
                return { data: queryResult.recordData, meta: loginStore.meta[queryResult.recordData.attributes.type] }
            }
            return {}
        }

    }


    /**
     * Add an item to the active items array
     * @param {any} obj 
     */
    @action
    addActiveObject(obj: any) {
        if (!obj) {
            return;
        }

        if (obj.hasOwnProperty("table") && obj.hasOwnProperty("currentRecord")) {

            let _obj = Object.assign({}, { ...obj.currentRecord })
            if (!_obj.hasOwnProperty("attributes")) {
                _obj.attributes = {}
                _obj.attributes.type = obj.table
            }

            this.addActiveObject(_obj)

        } else {
            const { activeItems } = this

            if (_.isEmpty(activeItems)) {
                this.activeItems.push(obj)
            }

            if (obj.hasOwnProperty("attributes")) {
                let recordTypeMatch = activeItems.find((item: any) => item.attributes.type == obj.attributes.type)

                if (_.isEmpty(recordTypeMatch)) {
                    activeItems.push(obj)
                } else {
                    this.activeItems = activeItems.map((item: any) => {
                        if (item.attributes.type == obj.attributes.type) {
                            return obj
                        } else {
                            return item
                        }
                    })
                }
            }

        }
    }

    restoreActiveItems(items: any) {
        this.activeItems = items
    }

    /**
     * Removes the last item in the active data
     */
    @action
    removeLastActiveObject() {
        this.activeItems.pop()
    }

    /**
     * Remove all the items from the active data
     */
    @action
    clearActiveObjects() {
        this.markers = []
        this.activeItems = []
        try {
            this.rootStore.loginStore.slackDev("dataStore.clearActiveObjects")
        } catch (error) {
            alert("clearActiveObjects " + error)
        }

    }

    @action
    clearContextData() {

        this.contextData = []
        try {
            this.rootStore.loginStore.slackDev("dataStore.clearContextData")
        } catch (error) {
            alert("clearContextData " + error)
        }
    }

    /**
     * Search for a relation value TABLE[FIELD] in the active data.
     */
    @action
    getValue(table: string, field: string) {
        try {

            let data = this.activeItems.filter(item => item.attributes.type === table)

            if (!this.isLocationBased) {

                if (table.includes("Installed_Product")) {
                    data = [this.currentInstalledProduct]
                }

                if (table.includes("Service_Request")) {
                    data = [this.currentServiceRequest]
                }

                if (table.includes("Location")) {
                    data = [this.currentLocation]
                }

            }
            if (!data) return "-"

            if (_.isEmpty(data)) return "-"

            if (_.first(data).hasOwnProperty(field)) {
                let x = _.first(data)[field]
                return x
            } else {
                return "-"
            }

        } catch (error) {

        }
        return "-"
    }

    /**
     * Returns the type of object of the last item in the active data. Ex: Work_Order__c or Installed_Product__c
     */
    @computed
    get lastActiveItemType() {
        try {
            return _.last(this.activeItems) ? null : _.last(this.activeItems).attributes.type
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the active data type."), {
            // Sentry.Native.captureException(new Error("Not able to get the active data type."), {
            //     logger: 'dataStore',
            //     context: 'get lastActiveItemType'
            // });
        }
    }


    /**
     * Returns the sales force id of the last item in the active data
     */
    @computed
    get currentId() {
        try {
            return _.isEmpty(this.activeItems) ? null : _.last(this.activeItems).Id
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current id."), {
            // Sentry.Native.captureException(new Error("Not able to get the current id."), {
            //     logger: 'dataStore',
            //     context: 'get currentId'
            // });
        }

    }

    /**
     * Returns the current Wolis in the active WO
     */
    @computed
    get currentWolis(): Array<WorkOrderLineItem> | [] {
        try {
            const { loginStore } = this.rootStore
            const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
            const WO = loginStore.getPrefixedFieldName("Work_Order__c")

            

            let _woId = this.workOrderId
            if(loginStore.isRelatedWorkOrder()){
                console.warn(loginStore.isRelatedWorkOrder())
                if(this.currentRelatedWorkOrder !== null && this.currentRelatedWorkOrder !== null){
                    console.warn(_woId + " <-> " + this.currentRelatedWorkOrder)
                    _woId = this.currentRelatedWorkOrder
                }

            }

            return _.isEmpty(_woId) ? [] : loginStore.data[WOLI].filter((WOLI: any) => WOLI[WO] == _woId)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current wolis."), {
            // Sentry.Native.captureException(new Error("Not able to get the current wolis."), {
            //     logger: 'dataStore',
            //     context: 'get currentWolis'
            // });
        }

        return []
    }

    @action
    __currentWolis(): Array<WorkOrderLineItem> | [] {
        try {
            console.warn("__currentWolis")
            const { loginStore } = this.rootStore
            const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
            const WO = loginStore.getPrefixedFieldName("Work_Order__c")

            

            
            if(loginStore.isRelatedWorkOrder()){
                console.warn(loginStore.isRelatedWorkOrder())
                if(this.currentRelatedWorkOrder !== null && this.currentRelatedWorkOrder !== null){
                    console.warn(this.workOrderId + " <-> " + this.currentRelatedWorkOrder)
                    
                    return _.isEmpty(this.currentRelatedWorkOrder) ? [] : loginStore.data[WOLI].filter((WOLI: any) => WOLI[WO] == this.currentRelatedWorkOrder)
                }

            }

            return _.isEmpty(this.workOrderId) ? [] : loginStore.data[WOLI].filter((WOLI: any) => WOLI[WO] == this.workOrderId)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current wolis."), {
            // Sentry.Native.captureException(new Error("Not able to get the current wolis."), {
            //     logger: 'dataStore',
            //     context: 'get currentWolis'
            // });
        }

        return []
    }

    /**
     * If there is an active Installed Product in the Active Items
     * then we can access the wolis that are linked to this IP
     */
    @computed
    get currentWolisFilterByActiveIP(): Array<WorkOrderLineItem> | [] {
        const { currentWolis, installedProductId } = this
        const { loginStore } = this.rootStore
        const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
        if (installedProductId) {
            return currentWolis.filter((woli: WorkOrderLineItem) => woli[IP] == installedProductId)
        }
        return []
    }



    @computed
    get installedProductsInLocationBasedWorkOrder(): Array<InstalledProductWithStatus> | [] {
        if (this.isLocationBased) {
            const { loginStore } = this.rootStore
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
            const STATUS = loginStore.getPrefixedFieldName("Status__c")
            if (!_.isEmpty(this.currentInstalledProducts) && _.isArray(this.currentInstalledProducts)) {

                try {
                    //@ts-ignore Typed checked in the if statement
                    let result = this.currentInstalledProducts.map((installedProduct: InstalledProduct) => {
                        let installedProductWithStatus = Object.assign(installedProduct)
                        let wolis = this.currentWolis.filter((woli: WorkOrderLineItem) => woli[IP] == installedProduct.Id)

                        let isOpen = wolis.reduce((acc, woli) => { return woli[STATUS] == "Open" || acc }, false)

                        installedProductWithStatus["isOpen"] = isOpen
                        return installedProductWithStatus
                    })
                    return result
                } catch (err) {

                    return []
                }


            }

        }
        return []
    }

     @action
    __installedProductsInLocationBasedWorkOrder(): Array<InstalledProductWithStatus> | [] {
        console.warn("__installedProductsInLocationBasedWorkOrder")
        if (this.isLocationBased) {
            const { loginStore } = this.rootStore
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
            const STATUS = loginStore.getPrefixedFieldName("Status__c")
            let _ips = this.__currentInstalledProducts()
            if (!_.isEmpty(_ips) && _.isArray(_ips)) {

                try {
                    //@ts-ignore Typed checked in the if statement
                    let result = _ips.map((installedProduct: InstalledProduct) => {
                        let installedProductWithStatus = Object.assign(installedProduct)
                        let wolis = this.currentWolis.filter((woli: WorkOrderLineItem) => woli[IP] == installedProduct.Id)

                        let isOpen = wolis.reduce((acc, woli) => { return woli[STATUS] == "Open" || acc }, false)

                        installedProductWithStatus["isOpen"] = isOpen
                        return installedProductWithStatus
                    })
                    return result
                } catch (err) {

                    return []
                }


            }

        }
        return []
    }

    /**
     * Returns an Array of Installed Products linked to the active WO
     */
    @computed
    get currentInstalledProducts(): Array<InstalledProduct> | [] {
        if (this.isLocationBased) {
            const { loginStore } = this.rootStore
            const { data } = loginStore
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
            const { currentWolis } = this
            const installedProductsIds = _.uniqBy(currentWolis, IP).map((woli: WorkOrderLineItem) => woli[IP])
            let installedProducts = installedProductsIds.map((id: string) => data[IP].find((ip: InstalledProduct) => ip.Id == id))
            return _.compact(installedProducts)
        }
        return _.compact([this.currentInstalledProduct])
    }

    @action
    __currentInstalledProducts(): Array<InstalledProduct> | [] {
        console.warn("__currentInstalledProducts")
        if (this.isLocationBased) {
            const { loginStore } = this.rootStore
            const { data } = loginStore
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
            let currentWolis = this.__currentWolis()
            const installedProductsIds = _.uniqBy(currentWolis, IP).map((woli: WorkOrderLineItem) => woli[IP])
            
            console.warn("__currentInstalledProducts " + (installedProductsIds.length))
            let installedProducts = installedProductsIds.map((id: string) => data[IP].find((ip: InstalledProduct) => ip.Id == id))
            return _.compact(installedProducts)
        }
        return _.compact([this.currentInstalledProduct])
    }

    /**
     * Returns an Array of Installed Products linked to the active WO
     */
    @computed
    get currentInstalledProductsWithStatus(): Array<InstalledProduct> | [] {
        if (this.isLocationBased) {
            const { currentInstalledProducts, currentWolis } = this
            const { loginStore } = this.rootStore
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
            const STATUS = loginStore.getPrefixedFieldName("Status__c")
            if (!_.isEmpty(currentInstalledProducts) && _.isArray(currentInstalledProducts)) {
                //@ts-ignore Type checked in the if statement
                let currentInstalledProductsWithStatus = currentInstalledProducts.map((ip: InstalledProduct) => {
                    let wolisInThisInstalledProduct = currentWolis.filter((woli: WorkOrderLineItem) => woli[IP] == ip.Id)

                    ip["isOpen"] = wolisInThisInstalledProduct.reduce((acc, woli) => { return woli[STATUS] == "Open" || acc }, false)
                    ip["badgeCount"] = wolisInThisInstalledProduct.length
                    return ip
                })
                return currentInstalledProductsWithStatus
            }
        }
        return [this.currentInstalledProduct]
    }

    /**
     * Returns the Work Order Id if there is a Work Order in the Active Data
     */
    @computed
    get workOrderId() {
        try {
            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === this.rootStore.loginStore.getPrefixedFieldName("Work_Order__c")) ? item.Id : acc }, null)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the work order id."), {
            // Sentry.Native.captureException(new Error("Not able to get the work order id."), {
            //     logger: 'dataStore',
            //     context: 'get workOrderId'
            // });
        }
    }

    /**
     * Returns the Work Order Name if there is a Work Order in the Active Data
     */
    @computed
    get workOrderName() {
        try {

            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === this.rootStore.loginStore.getPrefixedFieldName("Work_Order__c")) ? item.Name : acc }, null)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the work order name."), {
            //     logger: 'dataStore',
            //     context: 'get workOrderName'
            // });
            //     Sentry.Native.captureException(new Error("Not able to get the work order name."), {
            //         logger: 'dataStore',
            //         context: 'get workOrderName'
            //     });
        }
    }

    /**
     * Returns the Work Order Template if there is a Work Order in the Active Data
     */
    @computed
    get workOrderTemplate() {
        const { loginStore } = this.rootStore
        const WO = loginStore.getPrefixedFieldName("Work_Order__c")
        const TEMPLATE = loginStore.getPrefixedFieldName("Template__c")
        try {
            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === WO) ? item[TEMPLATE] : acc }, null)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the work order template."), {
            // Sentry.Native.captureException(new Error("Not able to get the work order template."), {
            //     logger: 'dataStore',
            //     context: 'get workOrderTemplate'
            // });
        }
    }


    /**
     * Returns the Work Order if there is one in the Active Data
     */
    @computed
    get currentWorkOrder() {
        try {
            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === this.rootStore.loginStore.getPrefixedFieldName("Work_Order__c")) ? item : acc }, null)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the work order."), {
            //     logger: 'dataStore',
            //     context: 'get currentWorkOrder'
            // });
        }
    }

    /**
     * Returns the Work Order if there is one in the Active Data
     */
    @action
    getCurrentWorkOrder() {
        try {

            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === this.rootStore.loginStore.getPrefixedFieldName("Work_Order__c")) ? item : acc }, null)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the work order."), {
            //     logger: 'dataStore',
            //     context: 'get currentWorkOrder'
            // });
        }
    }

    @observable
    currentProduct = null

    @action
    clearCurrentProduct() {
        this.currentProduct = null
    }

    @action
    getTimesheetLineItem() {
        try {

            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === this.rootStore.loginStore.getPrefixedFieldName("Timesheet_Lineitem__c")) ? item : acc }, null)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the work order."), {
            //     logger: 'dataStore',
            //     context: 'get currentWorkOrder'
            // });
        }
    }
    // getTimesheetLineItem2() {
    //     try {
    //         if (!_.isEmpty(this.activeItems)) {

    //             const { loginStore } = this.rootStore
    //             const _index = _.findIndex(this.activeItems, x => x.table === loginStore.getPrefixedFieldName("Timesheet_Lineitem__c"))
    //             debugger
    //             if (_index >= 0) {

    //                 return this.activeItems[_index].currentRecord || null
    //             }
    //         }
    //     } catch (err) {


    //     }

    //     return null
    // }

    @action
    removeActiveTimesheetLineItem() {
        if (!_.isEmpty(this.activeItems)) {
            let _tsli = this.getTimesheetLineItem()
            if (_tsli) {
                this.activeItems = this.activeItems.filter(x => x.Id !== _tsli.Id)
            }
        }
    }


    /**
     * Returns true if the current Work Order in the Active Data is a Location based work order
     */
    @computed
    get isLocationBased() {
        try {
            const { loginStore } = this.rootStore
            return this.currentWorkOrder[loginStore.getPrefixedFieldName("Template__c")] === "Location"
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current wo template."), {
            // Sentry.Native.captureException(new Error("Not able to get the current wo template."), {
            //     logger: 'dataStore',
            //     context: 'get isLocationBased'
            // });
        }
    }


    /**
     * Return the current Service Request if there is one in the Active Data
     */
    @computed
    get currentServiceRequest() {
        try {
            const { loginStore } = this.rootStore
            let serviceRequest = loginStore.getPrefixedFieldName("Service_Request__c")
            return jsonQuery(`${serviceRequest}[Id=${this.currentWorkOrder[serviceRequest]}]`, { data: loginStore.data }).value
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current service request."), {
            // Sentry.Native.captureException(new Error("Not able to get the current service request."), {
            //     logger: 'dataStore',
            //     context: 'get currentServiceRequest'
            // });
        }
    }

    /**
     * Returns the id of the Installed Product if there is one in the Active Data
     */
    @computed
    get currentInstalledProduct() {
        try {
            const { loginStore } = this.rootStore
            let installedProduct = loginStore.getPrefixedFieldName("Installed_Product__c")
            return jsonQuery(`${installedProduct}[Id=${this.currentServiceRequest[installedProduct]}]`, { data: loginStore.data }).value
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current service request."), {
            // Sentry.Native.captureException(new Error("Not able to get the current service request."), {
            //     logger: 'dataStore',
            //     context: 'get currentServiceRequest'
            // });
        }
    }

    @computed
    get currentLocation() {
        try {
            const { loginStore } = this.rootStore
            let location = loginStore.getPrefixedFieldName("Location__c")
            return jsonQuery(`${location}[Id=${this.currentInstalledProduct[location]}]`, { data: loginStore.data }).value
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current service request."), {
            // Sentry.Native.captureException(new Error("Not able to get the current service request."), {
            //     logger: 'dataStore',
            //     context: 'get currentServiceRequest'
            // });
        }
    }

    @computed
    get currentWorkOrderLocation() {
        try {
            if (this.isLocationBased) {
                const { loginStore } = this.rootStore
                let location = loginStore.getPrefixedFieldName("Location__c")
                return jsonQuery(`${location}[Id=${this.currentWorkOrder[location]}]`, { data: loginStore.data }).value
            }

            //valid response for a non-Location-based WOs
            return null
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current Work Orders's Location request."), {
            // Sentry.Native.captureException(new Error("Not able to get the current Work Orders's Location request."), {
            //     logger: 'dataStore',
            //     context: 'get currentWorkOrderLocation'
            // });
        }
    }

    @computed
    get installationsByCurrentLocation() {
        try {
            const { loginStore } = this.rootStore
            const LOC = loginStore.getPrefixedFieldName("Location__c")
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
            return jsonQuery(`${IP}[*${LOC}=${this.currentLocation.Id}]`, { data: loginStore.data }).value
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current service request."), {
            // Sentry.Native.captureException(new Error("Not able to get the current service request."), {
            //     logger: 'dataStore',
            //     context: 'get currentServiceRequest'
            // });
        }
    }

    @computed
    get installedProductId() {
        try {
            const { loginStore } = this.rootStore
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")

            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === IP) ? item.Id : acc }, null)
        } catch (err) {
            //pass
        }
    }
    //For now commented out but let's leave it.
    //     @computed
    //     get installedProductName() {
    //         try {
    //             const { loginStore } = this.rootStore
    //             const IP = loginStore.getPrefixedFieldName("Installed_Product__c")

    //             return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === IP) ? item.Name : acc }, null)
    //         } catch (err) {
    // //
    //         }
    //     }

    /**
     * Returns an array of all ids present in Swift
     */
    @computed
    get allIds() {
        try {
            const { loginStore } = this.rootStore

            const { data } = loginStore

            let dataIds = Object.values(data).reduce((acc: Array<any>, value: Array<any>) => {
                if (!_.isEmpty(value)) {
                    let ids = value.map(item => item.Id)
                    return acc.concat(ids)
                }
                return acc
            }, [])

            return dataIds

        } catch (err) {
            return []
        }
    }

    @action
    getInstalledProductId() {
        try {
            const { loginStore } = this.rootStore
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")

            return _.isEmpty(this.activeItems) ? null : this.activeItems.reduce((acc, item) => { return (item.attributes.type === IP) ? item.Id : acc }, null)
        } catch (err) {

            // Sentry.captureException(new Error("Not able to get the current installed product ID."), {
            //     logger: 'dataStore',
            //     context: 'get installedProductId'
            // });
        }
    }

    @action
    getRecordById(table: string, UID: string, data: any) {
        let _table = table
        const { loginStore } = this.rootStore
        if (_.isEmpty(data)) {
            data = this.rootStore.loginStore.data
        }
        if (table == "OwnerId") {
            _table = "User"
        }
        if (data.hasOwnProperty(_table)) {
            let record = data[_table].filter(record => record.Id == UID)
            if (!_.isEmpty(record)) {
                return _.first(record)
            }
            return null
        }
    }

    /**
     * Gets the corrent label-value from a given table-field
     * @param table : string
     * @param field : string
     * @param value : string
     */
    @action
    getLabelValue(table: string, field: string, value: any, online = false) {
        try {
            const { metaStore } = this.rootStore
            const { onlineData } = this
            const { data } = this.rootStore.loginStore

            let meta = metaStore.getFieldMeta(table, field)



            switch (meta.data.type) {
                case RecordTypes.PICKLIST: {
                    let valueMeta = meta.data.picklistvalues.filter((meta: any) => meta.value == value)
                    if (!_.isEmpty(valueMeta)) {
                        return _.first(valueMeta).label
                    }
                    return value
                }
                case "DATETIME": {
                    const FORMAT = 'YYYY-MM-DD'
                    let formattedDate = moment(value).format(FORMAT)
                    if (formattedDate == "Invalid date") return ""
                    return formattedDate
                }
                case "DATE": {
                    const FORMAT = 'YYYY-MM-DD'
                    let formattedDate = moment(value).format(FORMAT)

                    if (formattedDate == "Invalid date") return ""
                    return formattedDate
                }
                case "REFERENCE": {

                    let record = online
                        ? this.getRecordById(field, value, onlineData)
                        : this.getRecordById(field, value, data)
                    if (!_.isEmpty(record)) {

                        if (record.attributes.type == "User") {
                            let firstName = record.FirstName ? record.FirstName : ""
                            let lastName = record.LastName ? record.LastName : ""
                            return `${firstName} ${lastName}`
                        }
                        return record.Name ? record.Name : "-"
                    }
                    return "-"

                }

                default: return value
            }

        } catch (e) {
            // Sentry.captureException(new Error("Not able to get label-value."), {
            //     logger: 'MetaStore',
            //     context: 'getLabelValue'
            // });
        }
    }

    mergeRelatedWorkOrderData(onlineData: any) {
        try {
            const { data } = this.rootStore.loginStore
            const { loginStore } = this.rootStore


            if (!_.isEmpty(onlineData)) {
                let entries = Object.entries(onlineData)

                // Add is readOnly attribute
                let readOnlyEntries = entries.map((entry: Array<any>) => {
                    let entryData = entry[1].map((record: any) => {

                        record.recordData.attributes["readOnly"] = true
                        return record.recordData
                    })
                    return [entry[0], entryData]
                })

                // Merge New Entries to Local Storage
                for (let key in data) {
                    let onlineEntries = readOnlyEntries.filter((entry: Array<any>) => entry[0] == key)
                    if (!_.isEmpty(onlineEntries) && _.first(onlineEntries)[1].length > 0) {
                        let newEntries = _.first(onlineEntries)[1]
                        let localEntries = data[key]


                        newEntries.map((newRecord: any) => { loginStore.addRecordToLocalData(key, newRecord) })




                    }
                }


            }

        } catch (e) {

            // Sentry.captureException(new Error("Not able to merge online data."), {
            //     logger: 'MetaStore',
            //     context: 'mergeRelatedWorkOrderData'
            // });
        }


    }

    @observable
    currentRelatedWorkOrder = ""

    @action
    setCurrentRelatedWorkOrder(id: string) {
        // console.warn("setCurrentRelatedWorkOrder " + id)
        this.currentRelatedWorkOrder = id
    }

    @observable
    currentChecklistMode: upsertScreenModes = upsertScreenModes.READONLY

    setCurrentChecklistMode(m: upsertScreenModes) {
        this.currentChecklistMode = m
    }
}
