import { Platform } from 'react-native';
import { APP_SETTINGS, APP_SETTINGS_2, APP_SETTINGS_3 } from '../fakeData/AppSettings'
import  RootStore  from '../../stores/rootStore'

import { 
    TSO5NL_META_DATA
} from '../fakeData/index'

describe("SettingsStore",()=>{
    
    it("Updates the princing settings",()=>{
        const rootStore = new RootStore()
        const {loginStore,settingsStore} = rootStore
        loginStore.getUserConfig.appSettings = APP_SETTINGS
        settingsStore.updateAppSettings()
        expect(settingsStore.rules.pricing.showActivitiesPrices).toBe(false)
        expect(settingsStore.rules.pricing.showPartPrices).toBe(true)
        expect(settingsStore.rules.pricing.showOtherExpensesPrices).toBe(true)
    })

    it("Normalice a Work Order Item Type",()=>{
        const rootStore = new RootStore()
        const {settingsStore} = rootStore
        expect(settingsStore.normalizeString("Other Expense")).toBe("OTHER_EXPENSE")
        expect(settingsStore.normalizeString("Error_Code")).toBe("ERROR_CODE")
    })


    // TO IMPLEMENT

    // it("Updates the princing settings 2",()=>{
    //     const rootStore = new RootStore()
    //     const {loginStore,settingsStore} = rootStore
    //     loginStore.getUserConfig.appSettings = APP_SETTINGS_2
    //     settingsStore.updateAppSettings()
    //     expect(settingsStore.rules.pricing.showActivitiesPrices).toBe(false)
    //     expect(settingsStore.rules.pricing.showPartPrices).toBe(true)
    //     expect(settingsStore.rules.pricing.showOtherExpensesPrices).toBe(true)
    // })

    // it("Takes default pricing when settings is not there",()=>{
    //     const rootStore = new RootStore()
    //     const {loginStore,settingsStore} = rootStore
    //     loginStore.getUserConfig.appSettings = APP_SETTINGS_3
    //     settingsStore.updateAppSettings()
    //     expect(settingsStore.rules.pricing.showActivitiesPrices).toBe(true)
    //     expect(settingsStore.rules.pricing.showPartPrices).toBe(false)
    //     expect(settingsStore.rules.pricing.showOtherExpensesPrices).toBe(false)
    // })

    
})