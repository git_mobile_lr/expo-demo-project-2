import { Platform } from 'react-native';
import  RootStore  from '../../stores/rootStore'

import { 
    TSO5NL_META_DATA
} from '../fakeData/index'

describe("LoginStore",()=>{
    
    it("Returns the correct field prefixed",()=>{
        const rootStore = new RootStore()
        const {loginStore} = rootStore
        loginStore.meta = TSO5NL_META_DATA
        expect(loginStore.getPrefixedTableName("Work_Order__c")).toStrictEqual("FIELDBUDDY__Work_Order__c")

    })

    
})