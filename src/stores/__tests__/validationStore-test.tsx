import { Platform } from 'react-native';
import  RootStore  from '../rootStore'
import  LoginStore  from '../loginStore'
import { 
    SINGLE_CONDITION, 
    FRESH_MAINTENANCE_META_DATA, 
    FRESH_MAINTENANCE_DATA,
    LOCATION_CONDITION, 
    INVALID_LOCATION_CONDITION,
    ACTIVE_DATA,
    TSO5NL_META_DATA,
    TSO5NL_DATA,
    TSO5NL_CURRENT_WORK_ORDER,
    MEDIALIBRARY_FAILED,
    MEDIALIBRARY_UPLOADED
} from '../fakeData/index'

describe("validationStore",()=>{
    
    it("Validates a valid single Condition",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore,dataStore} = rootStore
        loginStore.data = TSO5NL_DATA
        let singleCondition = { formula : "FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA]"}
        expect(validationStore.validateSingleCondition(singleCondition)).toBe(true)
    })

    it("Doesn't validate an invalid single Condition",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore,dataStore} = rootStore
        loginStore.data = TSO5NL_DATA
        let singleCondition = { formula : "FIELDBUDDY__Location__c[Id=SAAS]"}
        expect(validationStore.validateSingleCondition(singleCondition)).toBe(false)
    })

    it("Doesn't validate an invalid-formatted single Condition",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore,dataStore} = rootStore
        loginStore.data = TSO5NL_DATA
        let singleCondition = { formulas : "FIELDBUDDY__Location__c[Id=SAAS]"}
        //@ts-ignore
        expect(validationStore.validateSingleCondition(singleCondition)).toBe(false)
    })

    it("Validates a valid condition",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore,dataStore} = rootStore
        loginStore.data = TSO5NL_DATA
        let condition = { 
            conditions :[
                {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA]"},
                {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA]"}
            ],
            operator : "AND"
        }
        //@ts-ignore
        expect(validationStore.validateCondition(condition)).toBe(true)
    })

    it("Doesn't validate an invalid condition",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore,dataStore} = rootStore
        loginStore.data = TSO5NL_DATA
        let condition = { 
            conditions :[
                {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUssAA]"},
                {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA]"}
            ],
            operator : "AND"
        }
        //@ts-ignore
        expect(validationStore.validateCondition(condition)).toBe(false)
    })

    it("Validates a valid nested condition with AND and OR",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore,dataStore} = rootStore
        loginStore.data = TSO5NL_DATA
        let condition = { 
            conditions :[
                {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA]"},
                { 
                    conditions :[
                        {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA222]"},
                        {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA]"}
                    ],
                    operator : "OR"
                }
            ],
            operator : "AND"
        }
        //@ts-ignore
        expect(validationStore.validateCondition(condition)).toBe(true)
    })

    it("Doesn't validates an invalid nested condition with AND and OR",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore,dataStore} = rootStore
        loginStore.data = TSO5NL_DATA
        let condition = { 
            conditions :[
                {formula:"FIELDBUDDY__Location__c[Id=a0D3I00s00006ieiUAA]"},
                { 
                    conditions :[
                        {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA222]"},
                        {formula:"FIELDBUDDY__Location__c[Id=a0D3I0000006ieiUAA]"}
                    ],
                    operator : "OR"
                }
            ],
            operator : "AND"
        }
        //@ts-ignore
        expect(validationStore.validateCondition(condition)).toBe(false)
    })

    it("Doesn't validate if some attachments are not uploaded",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore} = rootStore
        loginStore.data = TSO5NL_DATA,
        loginStore.currentWorkOrderId = "a0n3I000000OVHPQA4"
        loginStore.mediaLibrary = MEDIALIBRARY_FAILED
        const dialogs = loginStore.getLocalizedDialogs()
        const { ATTACHMENTS_VALIDATION } = dialogs
        let singleCondition = {
            "alias" : "ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS",
            "type": "validation",
            "alertType": "warning",
            "source": "${MEDIA_LIBRARY}",
            "alertMessage": ATTACHMENTS_VALIDATION.MESSAGE,
            "alertDefaultText": ATTACHMENTS_VALIDATION.MESSAGE
        }
        //@ts-ignore
        expect(validationStore.validateSingleCondition(singleCondition)).toBe(false)
    })

    it("validate if all attachments are uploaded",()=>{
        const rootStore = new RootStore()
        const {validationStore,loginStore} = rootStore
        loginStore.data = TSO5NL_DATA
        loginStore.currentWorkOrderId = "a0n3I000000OVHPQA4"
        loginStore.mediaLibrary = MEDIALIBRARY_UPLOADED
        const dialogs = loginStore.getLocalizedDialogs()
        const { ATTACHMENTS_VALIDATION } = dialogs
        let singleCondition = {
            "alias" : "ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS",
            "type": "validation",
            "alertType": "warning",
            "source": "${MEDIA_LIBRARY}",
            "alertMessage": ATTACHMENTS_VALIDATION.MESSAGE,
            "alertDefaultText": ATTACHMENTS_VALIDATION.MESSAGE
        }
        //@ts-ignore
        expect(validationStore.validateSingleCondition(singleCondition)).toBe(true)
    })
})