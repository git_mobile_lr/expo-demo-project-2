import { Platform } from 'react-native';
import  RootStore  from '../../stores/rootStore'
import  LoginStore  from '../../stores/loginStore'
import { 
    FRESH_MAINTENANCE_META_DATA, 
    FRESH_MAINTENANCE_DATA,
    TSO5NL_META_DATA,
    DEV4_META_DATA,
    RELATIONSHIPS
} from '../fakeData/index'

describe("MetaStore",()=>{
    

    it("Sets a correct meta data sObjects",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(TSO5NL_META_DATA)
        expect(metaStore.meta).toStrictEqual(TSO5NL_META_DATA)
    })

    it("Sets a correct meta data sObjects",()=>{
      const rootStore = new RootStore()
      const {metaStore} = rootStore
      metaStore.setMeta(DEV4_META_DATA)
      expect(metaStore.meta).toStrictEqual(DEV4_META_DATA)
  })

    // it("Sets an incorrect meta data sObjects",()=>{
    //     const rootStore = new RootStore()
    //     const {metaStore} = rootStore
    //     metaStore.setMeta("NOT CORRECT ARGUMENT")
    //     expect(metaStore.meta).toStrictEqual(null)
    // })

    it("Sets a null meta data sObjects",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(null)
        expect(metaStore.meta).toStrictEqual({})
    })


    // it("Sets an incorrect meta data relationships",()=>{
    //     const rootStore = new RootStore()
    //     const {metaStore} = rootStore
    //     metaStore.setRelationships("NOT CORRECT ARGUMENT")
    //     expect(metaStore.relationships).toStrictEqual(null)
    // })

    it("Sets a null meta data relationships",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setRelationships(null)
        expect(metaStore.relationships).toStrictEqual([])
    })

    it("Request of meta data per table and field (correct parameters)",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(TSO5NL_META_DATA)
        expect(metaStore.getFieldMeta("FIELDBUDDY__Product__c","Id")).toStrictEqual(
            {
                "type":"success",
                "data":{
                    "updateable": false,
                    "type": "ID",
                    "scale": 0,
                    "referenceToRelationshipNames": [],
                    "referenceTo": [],
                    "precision": 0,
                    "picklistvalues": [],
                    "name": "Id",
                    "length": 18,
                    "label": "Record ID",
                    "controllerName": null,
                    "accessible": true
                  }
            }
        )
    })

    it("Request of meta data per table and field (incorrect parameters)",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(TSO5NL_META_DATA)
        expect(metaStore.getFieldMeta("FIELDBUDDY__Product__c","Ids")).toStrictEqual(
            {
                type:"error",
                data: "No results"
            }
        )
    })

    it("Expect Prefix to be FIELDBUDDY__",()=>{
      const rootStore = new RootStore()
      const {metaStore} = rootStore
      metaStore.setMeta(TSO5NL_META_DATA)
      expect(metaStore.getMetaPrefix()).toStrictEqual("FIELDBUDDY__")
    })  

    it("Expect Prefix to be BLANK",()=>{
      const rootStore = new RootStore()
      const {metaStore} = rootStore
      metaStore.setMeta(DEV4_META_DATA)

      expect(metaStore.getMetaPrefix()).toStrictEqual("")
    })  

    it("Request of meta data per table and field (null parameters)",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(TSO5NL_META_DATA)
        expect(metaStore.getFieldMeta()).toStrictEqual(
            {
                type:"error",
                data: "No results"
            }
        )
    })

    it("Request of meta data per record (correct parameters)",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(TSO5NL_META_DATA)
        expect(metaStore.getObjectMeta("FIELDBUDDY__Product__c")).toStrictEqual(
            {
                "type":"success",
                "data":{
                    "updateable": true,
                    "relationships": [],
                    "recordtypes": {
                      "012000000000000AAA": {
                        "active": true,
                        "available": true,
                        "defaultRecordTypeMapping": true,
                        "developerName": "Master",
                        "master": true,
                        "name": "Master",
                        "recordTypeId": "012000000000000AAA",
                        "urls": {
                          "layout": "/services/data/v43.0/sobjects/FIELDBUDDY__Product__c/describe/layouts/012000000000000AAA"
                        }
                      }
                    },
                    "prefixid": "a0H",
                    "plurallabel": "Products",
                    "name": "FIELDBUDDY__Product__c",
                    "label": "Product",
                    "fields": [
                      {
                        "updateable": false,
                        "type": "ID",
                        "scale": 0,
                        "referenceToRelationshipNames": [],
                        "referenceTo": [],
                        "precision": 0,
                        "picklistvalues": [],
                        "name": "Id",
                        "length": 18,
                        "label": "Record ID",
                        "controllerName": null,
                        "accessible": true
                      },
                      {
                        "updateable": true,
                        "type": "STRING",
                        "scale": 0,
                        "referenceToRelationshipNames": [],
                        "referenceTo": [],
                        "precision": 0,
                        "picklistvalues": [],
                        "name": "Name",
                        "length": 80,
                        "label": "Product Name",
                        "controllerName": null,
                        "accessible": true
                      },
                      {
                        "updateable": false,
                        "type": "DATETIME",
                        "scale": 0,
                        "referenceToRelationshipNames": [],
                        "referenceTo": [],
                        "precision": 0,
                        "picklistvalues": [],
                        "name": "CreatedDate",
                        "length": 0,
                        "label": "Created Date",
                        "controllerName": null,
                        "accessible": true
                      },
                      {
                        "updateable": true,
                        "type": "PICKLIST",
                        "scale": 0,
                        "referenceToRelationshipNames": [],
                        "referenceTo": [],
                        "precision": 0,
                        "picklistvalues": [
                          {
                            "active": true,
                            "defaultValue": false,
                            "label": "Own manufacturing",
                            "validFor": null,
                            "value": "Own manufacturing"
                          },
                          {
                            "active": true,
                            "defaultValue": false,
                            "label": "Dealership",
                            "validFor": null,
                            "value": "Dealership"
                          },
                          {
                            "active": true,
                            "defaultValue": false,
                            "label": "Purchase",
                            "validFor": null,
                            "value": "Purchase"
                          },
                          {
                            "active": true,
                            "defaultValue": false,
                            "label": "Heating",
                            "validFor": null,
                            "value": "Heating"
                          },
                          {
                            "active": true,
                            "defaultValue": false,
                            "label": "Ventilation",
                            "validFor": null,
                            "value": "Ventilation"
                          },
                          {
                            "active": true,
                            "defaultValue": false,
                            "label": "Airconditioning",
                            "validFor": null,
                            "value": "Airconditioning"
                          }
                        ],
                        "name": "FIELDBUDDY__Product_Type__c",
                        "length": 255,
                        "label": "Product Type",
                        "controllerName": null,
                        "accessible": true
                      }
                    ],
                    "deletable": true,
                    "createable": true,
                    "childrelationships": [
                      {
                        "ParentToChildFieldName": "FIELDBUDDY__Installed_Products__r",
                        "ParentObject": "FIELDBUDDY__Product__c",
                        "ChildToParentFieldName": "FIELDBUDDY__Product__c",
                        "ChildObject": "FIELDBUDDY__Installed_Product__c"
                      }
                    ],
                    "accessible": true
                  }
            }
        )
    })

    it("Request of meta data per record (incorrect parameters)",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(TSO5NL_META_DATA)
        expect(metaStore.getObjectMeta("FIELDBUDDY__Product__cc" )).toStrictEqual(
            {
                type:"error",
                data: "No results"
            }
        )
    })

    it("Request of meta data per record (null parameters)",()=>{
        const rootStore = new RootStore()
        const {metaStore} = rootStore
        metaStore.setMeta(TSO5NL_META_DATA)
        expect(metaStore.getObjectMeta()).toStrictEqual(
            {
                type:"error",
                data: "No results"
            }
        )
    })

});