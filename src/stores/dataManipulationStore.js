
import { action, autorun, computed, observable } from 'mobx'
import moment from 'moment'
import 'moment/locale/nl'
import { EN, NL } from '../i18n/locales'
import { enDialogs, nlDialogs } from '../i18n/dialogs'
//import {enOnboarding, nlOnboarding} from '../i18n/onboarding'
import { enPermissions, nlPermissions } from '../i18n/permissions'
import { Dimensions, Platform, TouchableNativeFeedback } from 'react-native'
import _ from 'lodash'
const IsIOS = () => {
  return Platform.OS === 'ios'
}

const DUTCH = (x) => {
  const CURRENCY_FORMAT = {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2,
    style: 'currency',
    currency: 'EUR',
    currencyDisplay: 'symbol'
  }
  if (IsIOS()) {

    return Number(x).toLocaleString("nl-NL", CURRENCY_FORMAT)
  }

  try {

    return "€ " + Number(x)
      .toFixed(2)
      .toLocaleString("nl-NL", CURRENCY_FORMAT)
  } catch (ex) {
    return "€ " + Number(x).toLocaleString("nl-NL", CURRENCY_FORMAT)
  }
}
// / / pure and read-only data manipulation / preparing data based on dataText
// and dataBinding configs /
export default class DataManipulationStore {

  constructor(rootStore) {
    this.rootStore = rootStore

  }

  _getLabel(dataBinding, metaBinding, key) {
    var __START_DATE__ = moment();
    let _label = ""
    try {

      if (dataBinding.filter(x => x.var === key)) {
        _label = dataBinding.filter(x => x.var === key)[0].label || ""
      }

      if (!_label && metaBinding[key]) {
        _label = metaBinding[key].label
      }
      if (!_label && metaBinding[key] && metaBinding[key].meta) {
        _label = metaBinding[key].meta.label
      }
      return _label
    } catch (error) {
      return key
    }
  }

  _displayLocalizedDouble(raw) {
    try {
      return DUTCH(raw)

    } catch (e) {
      return ""
    }

  }

  @action
  getSectionList(initAt, sliceData, config) {
    var __START_DATE__ = moment();
    const _table = sliceData[config.orderBy.table]

    const _preItems = _table.map((item, index) => {

      let _record = {
        key: index,
        targetTable: config.orderBy.table,
        dataItem: []
      }
      _record.dataItem[config.orderBy.table] = item
      return _record
    })

    const _sorted = _.sortBy(_preItems, x => {
      return (x.dataItem[x.targetTable][config.orderBy.field] || "").toLowerCase()
    })

    for (let _s of _sorted) {
      //   const result = config.groupBy(_s)

      _s.isOpen = true
    }

    const _grouped = _.groupBy(_sorted, x => x.isOpen)
    const keys = _.keys(_grouped)

    let _sectionData = []
    for (let _key of keys) {

      _sectionData.push({
        title: _key === "true"
          ? "Todo"
          : "Done",
        index: _key === "true"
          ? 0
          : 1,
        data: _grouped[_key]
      })

    }

    return _.sortBy(_sectionData, x => x.index)
  }

  @action
  getForeignKeys(tables) {
    var __START_DATE__ = moment();
    let _common = {}
    const _ids = tables.filter(x => x.field === "Id")
    for (let _item of _ids) {
      _common[_item.table] = _item.value
    }

    return _common
  }
  ///retrieve data based on given dataText and dataBinding params
  @action
  prepareData(data, dataText, dataBinding) {
    var __START_DATE__ = moment();
    let _dataText = JSON.parse(JSON.stringify(dataText))
    var _keys = dataBinding.map(x => {
      return x.var
    })

    var _values = dataBinding.map(x => {
      return data[x.table][x.field]
    })

    var _meta = dataBinding.map(x => {
      let result = this
        .rootStore
        .metaStore
        .getFieldMeta(x.table, x.field)
      if (result.type === "success") {

        let alias = _.invert(_dataText)[x.var]

        return { alias: alias, table: x.table, field: x.field, label: x.label, meta: result.data }
      }
    })

    let _formats = dataBinding.filter(x => x.format)

    let _bindings = _.zipObject(_keys, _values)

    let _props = Object.getOwnPropertyNames(_dataText)

    for (let p of _props) {
      for (let x of _keys) {
        let _rawValue = _bindings[x]

        let _format = _formats.filter(f => f.var === x)

        if (_format && (_format || []).length > 0) {

          if (moment(_rawValue).isValid()) {
            let _formattedValue = moment(_rawValue).format(_format[0].format)

            _dataText[p] = _.replace(_dataText[p], x, _formattedValue)
          } else {

            _dataText[p] = _.replace(_dataText[p], x, _rawValue)
          }
        } else {

          _dataText[p] = _.replace(_dataText[p], x, _rawValue)

        }
      }
    }

    return { meta: _meta, data: _dataText }
  }

  @action
  prepareDataLineItemNew(data, dataText, dataBinding) {
    var __START_DATE__ = moment();
    let _dataText = JSON.parse(JSON.stringify(dataText))
    var _keys = dataBinding.map(x => {
      return x.var
    })

    var _values = dataBinding.map(x => {
      return data[x.table][x.field] || ""
    })

    var _meta = dataBinding.map(x => {
      let result = this
        .rootStore
        .metaStore
        .getFieldMeta(x.table, x.field)
      if (result.type === "success") {

        let alias = _.invert(_dataText)[x.var]

        return { alias: alias, table: x.table, field: x.field, label: x.label, meta: result.data }
      }
    })

    let _formats = dataBinding.filter(x => x.format)

    let _bindings = _.zipObject(_keys, _values)

    let _props = Object.getOwnPropertyNames(_dataText)

    for (let p of _props) {

      for (let x of _keys) {

        let _rawValue = _bindings[x]

        let _format = _formats.filter(f => f.var === x)

        if (_format && (_format || []).length > 0) {
          moment.locale(this._language())
          if (moment(_rawValue).isValid()) {
            let _formattedValue = moment(_rawValue).format(_format[0].format)

            _dataText[p] = _.replace(_dataText[p], x, _formattedValue)
          } else {

            _dataText[p] = _.replace(_dataText[p], x, _rawValue)
          }
        } else {

          _dataText[p] = _.replace(_dataText[p], x, _rawValue)
        }

      }
    }

    return { meta: _meta, dataText: _dataText }
  }

  @action
  prepareDataLineItem(data, dataText, dataBinding) {
    var __START_DATE__ = moment();
    let _dataText = JSON.parse(JSON.stringify(dataText))
    const _keys = dataBinding.map(x => {
      return x.var
    });

    const _values = dataBinding.map(x => {

      return data[x.table][x.field] || ""
    });

    const _meta = dataBinding.map(x => {
      let result = this
        .rootStore
        .metaStore
        .getFieldMeta(x.table, x.field)

      if (result.type === "success") {

        let alias = _.invert(_dataText)[x.var]

        return { alias: alias, table: x.table, field: x.field, label: x.label, meta: result.data }
      }
    });

    let _formats = dataBinding.filter(x => x.format);

    let _bindings = _.zipObject(_keys, _values);

    let _props = Object.getOwnPropertyNames(_dataText);

    for (let p of _props) {

      for (let x of _keys) {

        let _rawValue = _bindings[x]

        let _format = _formats.filter(f => f.var === x)

        if (_format && (_format || []).length > 0) {

          if (moment(_rawValue).isValid()) {
            let _formattedValue = moment(_rawValue).format(_format[0].format)
            _dataText[p] = _.replace(_dataText[p], x, _formattedValue)
          } else {
            _dataText[p] = _.replace(_dataText[p], x, _rawValue)
          }
        } else {

          const _fieldMeta = _.first(_meta.filter(x => x.alias === p))
          if (_fieldMeta) {
            if (_fieldMeta.meta && _fieldMeta.meta.type === 'PICKLIST' && _fieldMeta.meta.picklistvalues) {

              const _picklistItem = _.first(_fieldMeta.meta.picklistvalues.filter(x => x.value === _rawValue))
              if (_picklistItem) {
                _dataText[p] = _picklistItem.label
              }

            } else {
              _dataText[p] = _.replace(_dataText[p], x, _rawValue)
            }

          } else {
            _dataText[p] = _.replace(_dataText[p], x, _rawValue)
          }
        }
      }
    }

    return { meta: _meta, data: _dataText }
  }

  ///retrieve data based on given dataText and dataBinding params
  @action
  prepareData2(data, dataText, dataBinding) {
    const { loginStore } = this.rootStore
    var __START_DATE__ = moment();
    try {
      let that = this

      let _dataText = JSON.parse(JSON.stringify(dataText));

      var _keys = dataBinding.map(x => {
        return x.var;
      })

      var _values = dataBinding.map(x => {
        let result = this.rootStore.metaStore.getFieldMeta(x.table, x.field)
        if (result.type === "success") {
          let _meta = result.data
          return (_meta.type === "PICKLIST")
            ? _meta.picklistvalues.reduce((acc, option) => { return (option.value === data[x.table][x.field]) ? option.label : acc }, "")
            : data[x.table][x.field] || ""
        }

      })



      var _meta = dataBinding.map(x => {
        let result = this
          .rootStore
          .metaStore
          .getFieldMeta(x.table, x.field)

        if (result.type === "success") {

          let alias = _.invert(_dataText)[x.var];

          return { alias: alias, table: x.table, field: x.field, label: x.label, meta: result.data }
        }
      })

      let _formats = dataBinding.filter(x => x.format)

      let _bindings = _.zipObject(_keys, _values)
      let _bindingsMeta = _.zipObject(_keys, _meta)

      let _props = Object.getOwnPropertyNames(_dataText)




      for (let p of _props) {
        for (let x of _keys) {
          let _rawValue = _bindings[x]
          let _rawMeta = _bindingsMeta[x]
          let _format = _formats.filter(f => f.var === x)
          if (_format && (_format || []).length > 0) {
            if (moment(_rawValue).isValid()) {
              let _formattedValue = moment(_rawValue).format(_format[0].format)
              _dataText[p] = _.replace(_dataText[p], x, _formattedValue)
            } else {
              _dataText[p] = _.replace(_dataText[p], x, _rawValue)
            }
          } else {
            if (_rawMeta && _rawMeta.meta && _rawMeta.meta.type === "CURRENCY") {
              _dataText[p] = _.replace(_dataText[p], x, this._displayLocalizedDouble(_rawValue))
            } else {
              _dataText[p] = _.replace(_dataText[p], x, _rawValue)
            }
          }
        }
      }

      return { meta: _meta, data: _dataText }

    } catch (error) {

      return null
    }
  }

  _getData(data, table, field, path) {
    try {
      var __START_DATE__ = moment();
      if (path) {

        const _refId = _.get(data, path)

        if (_refId) {

          if (data[table]) {

            let _data = Array.isArray(JSON.parse(JSON.stringify(data[table])))
              ? data[table]
              : _.toArray([].concat(data[table]))

            let _found = _.first(_data.filter(x => x.Id === _refId))



            if (_found) {
              return _found[field] || ""
            }

          }



          const _foundRecord = this
            .rootStore
            .loginStore
            .getRecordById(table, _refId)

          if (_foundRecord) {
            return _foundRecord[field]
          }
        }

      }


      if (data[table] && data[table][field]) {
        return data[table][field] || ""
      }

      // const globaleActiveData = this   .rootStore   .loginStore
      // .getActiveScreenData() if (globaleActiveData && globaleActiveData[table] &&
      // globaleActiveData[table][field]) {   return globaleActiveData[table][field]
      // || "" }
    } catch (error) { }

    return ""
  }

  ///retrieve data based on given dataText and dataBinding params
  @action
  async prepareData3(data, dataText, dataBinding) {
    var __START_DATE__ = moment();
    let _dataText = JSON.parse(JSON.stringify(dataText));

    var _keys = dataBinding.map(x => {
      return x.var;
    })

    var _values = dataBinding.map(x => {

      return this._getData(data, x.table, x.field, x.path)
    })




    let _meta = dataBinding.map(x => {
      let result = this
        .rootStore
        .metaStore
        .getFieldMeta(x.table, x.field)
      if (result.type === "success") {

        let alias = _.invert(_dataText)[x.var];

        return { alias: alias, table: x.table, field: x.field, label: x.label, meta: result.data }
      }
    })

    let _formats = dataBinding.filter(x => x.format)

    let _bindings = _.zipObject(_keys, _values)
    let _bindingsMeta = _.zipObject(_keys, _meta)

    let _props = Object.getOwnPropertyNames(_dataText)

    for (let p of _props) {
      for (let x of _keys) {
        let _rawValue = _bindings[x]
        let _rawMeta = _bindingsMeta[x]
        let _format = _formats.filter(f => f.var === x)
        if (_format && (_format || []).length > 0) {
          if (moment(_rawValue).isValid()) {
            let _formattedValue = moment(_rawValue).format(_format[0].format)
            _dataText[p] = _.replace(_dataText[p], x, _formattedValue)
          } else {
            _dataText[p] = _.replace(_dataText[p], x, _rawValue)
          }
        } else {
          if (_rawMeta && _rawMeta.meta && _rawMeta.meta.type === "CURRENCY") {
            _dataText[p] = _.replace(_dataText[p], x, this._displayLocalizedDouble(_rawValue))
          } else {
            _dataText[p] = _.replace(_dataText[p], x, _rawValue)
          }
        }
      }
    }

    return _
      .toPairs(_dataText)
      .map(x => {

        let p = x[0];

        return {
          label: this._getLabel(dataBinding, _bindingsMeta, dataText[p]),
          value: x[1]
        }
      })
  }

  @action
  _prepareData4(data, dataText, dataBinding, defaultPath) {
    var __START_DATE__ = moment();
    let _dataText = JSON.parse(JSON.stringify(dataText))
    let _keys = dataBinding.map(x => {
      return x.var
    })

    let _values = dataBinding.map(x => {
      let _usedPath = null

      if (defaultPath && defaultPath[x.table]) {
        _usedPath = defaultPath[x.table]
      }

      if (x.path) {
        _usedPath = x.path
      }

      if (_usedPath) {

        const _refId = _.get(data, _usedPath)

        if (_refId && data[x.table]) {

          let _data = Array.isArray(JSON.parse(JSON.stringify(data[x.table])))
            ? data[x.table]
            : _.toArray([].concat(data[x.table]))

          let _found = _.first(_data.filter(x => x.Id === _refId))

          if (_found) {

            return _found[x.field] || ""
          }

        }
        return ""
      } else {
        // return data[x.table][x.field] || ""
        let _data = Array.isArray(JSON.parse(JSON.stringify(data[x.table])))
          ? data[x.table]
          : _.toArray([].concat(data[x.table]))

        let _found = _.first(_data)
        if (_found) {
          return _found[x.field] || ""
        }
        return ""
      }
    })

    // let _empties = dataBinding.map(x => {   const _empty =
    // this.props.rootStore.loginStore.getLabelOrEmpty(x);   return _empty; })
    let _formats = dataBinding.filter(x => x.format)

    let _bindings = _.zipObject(_keys, _values)

    let _props = Object.getOwnPropertyNames(_dataText)

    for (let p of _props) {

      for (let x of _keys) {
        let _rawValue = _bindings[x]

        let _format = _formats.filter(f => f.var === x)

        if (_format && (_format || []).length > 0) {

          if (moment(_rawValue).isValid()) {
            let _formattedValue = moment(_rawValue).format(_format[0].format)

            _dataText[p] = _.replace(_dataText[p], x, _formattedValue)
          } else {

            _dataText[p] = _.replace(_dataText[p], x, _rawValue)
          }
        } else {

          _dataText[p] = _.replace(_dataText[p], x, _rawValue)
        }
      }
    }
    this.measure(__START_DATE__, '_prepareData4 !!!')
    return _dataText
  }

  measure(__START_DATE__, info) {
    const _period = moment().diff(__START_DATE__, "ms")
    if (_period > 1000) {
      //(info, _period)
    }
    //  //(info, "time: " + moment().diff(__START_DATE__, "ms") + " ms")
  }

  @action
  prepareCardData(data, dataText, dataBinding, defaultPath) {
    var __START_DATE__ = moment();
    let _dataText = JSON.parse(JSON.stringify(dataText))

    let _emptyText = JSON.parse(JSON.stringify(_dataText))

    let _props = _.keys(dataText)

    let _keys = dataBinding.map(x => x.var)

    let _values = dataBinding.map(x => {

      let _usedPath = null
      if (defaultPath && defaultPath[x.table]) {

        _usedPath = defaultPath[x.table]
      }

      if (x.path) {
        _usedPath = x.path

      }

      if (_usedPath) {

        const _refId = _.get(data, _usedPath)

        if (_refId && data[x.table]) {

          let _data = Array.isArray(data[x.table])
            ? data[x.table]
            : _.toArray([].concat(data[x.table]))

          let _found = _.first(_data.filter(x => x.Id === _refId))

          if (_found) {
            const _fieldValue = _found[x.field]

            let result = this
              .rootStore
              .metaStore
              .getFieldMeta(x.table, x.field)

            if (result.type === 'success') {
              const { type } = result.data
              if (type.toLowerCase() === "picklist") {
                const { picklistvalues } = result.data
                const _index = _.findIndex(picklistvalues, x => x.value === _fieldValue)
                if (_index >= 0) {
                  const _label = picklistvalues[_index].label
                  if (_label) {
                    return _label
                  }
                }
              }
            }

            return _fieldValue || ""
          }

        }

        return ""
      } else {
        // return data[x.table][x.field] || ""

        let _data = Array.isArray(data[x.table])
          ? data[x.table]
          : _.toArray([].concat(data[x.table]))

        let _found = _.first(_data)
        if (_found) {
          const _fieldValue = _found[x.field]

          let result = this
            .rootStore
            .metaStore
            .getFieldMeta(x.table, x.field)

          if (result.type === 'success') {
            const { type } = result.data
            if (type.toLowerCase() === "picklist") {
              const { picklistvalues } = result.data
              const _index = _.findIndex(picklistvalues, x => x.value === _fieldValue)
              if (_index >= 0) {
                const _label = picklistvalues[_index].label
                if (_label) {
                  return _label
                }
              }
            }
          }

          return _fieldValue || ""
        }
        return ""

      }

    })

    let _empties = dataBinding.map(x => {

      if (x.empty) {
        const _empty = this
          .rootStore
          .loginStore
          .getLabelOrEmpty(x);

        return _empty;
      }
      return ""
    })

    let _refs = dataBinding.map(x => {

      if (x.ref_table && x.ref_field) {
        return { ref_table: x.ref_table, ref_field: x.ref_field };

      }
      return null
    })

    let _formats = dataBinding.filter(x => x.format)
    let _bindings = _.zipObject(_keys, _values)
    let _bindingsEmpties = _.zipObject(_keys, _empties)

    let result = {}

    result.EMPTY = _empties;
    result.REFS = _refs;
    //result.META = _meta

    for (let x of _keys) {

      for (let p of _.sortBy(_props)) {

        if (_dataText[p]) {

          let _rawValue = _bindings[x]

          let _rawEmptyValue = _bindingsEmpties[x]

          let _format = _formats.filter(f => f.var === x)

          if (_format && (_format || []).length > 0) {

            if (moment(_rawValue).isValid()) {
              let _formattedValue = moment(_rawValue).format(_format[0].format)

              _emptyText[p] = _.replace(_emptyText[p], x, _rawEmptyValue)
              _dataText[p] = _.replace(_dataText[p], x, _formattedValue)

              result.RAW_CONTENT = moment(_rawValue)

            } else {

              _emptyText[p] = _.replace(_emptyText[p], x, _rawEmptyValue)
              _dataText[p] = _.replace(_dataText[p], x, _rawValue)
            }
          } else {

            _emptyText[p] = _.trim(_.replace(_emptyText[p], x, _rawEmptyValue))

            _dataText[p] = _.trim(_.replace(_dataText[p], x, _rawValue))

          }
        }
      }
    }



    result.PICKLIST_LABEL = [] //_labels

    result.CARD_EMPTY = _emptyText

    result.CARD_CONTENT = _dataText

    return result
  }

  @action
  async prepareHeadlineData(data, dataText, dataBinding, globalActiveData, useGlobalData = true) {
    var __START_DATE__ = moment();
    let _headline = JSON.parse(JSON.stringify(dataText))

    let _keys = dataBinding.map(x => x.var)

    let _values = []
    for (let x of dataBinding) {
      let _value = ''
      if (data[x.table] && data[x.table][x.field]) {
        _value = data[x.table][x.field]
      } else if (useGlobalData) {

        if (globalActiveData) {
          if (globalActiveData[x.table] && globalActiveData[x.table][x.field]) {
            _value = globalActiveData[x.table][x.field]
          }
        }

      }

      let result = this
        .rootStore
        .metaStore
        .getFieldMeta(x.table, x.field)
      if (result.type === 'success') {
        const { type } = result.data
        if (type.toLowerCase() === "picklist") {
          const { picklistvalues } = result.data
          const _index = _.findIndex(picklistvalues, x => x.value === _value)

          if (_index >= 0) {
            const _label = picklistvalues[_index].label

            _values.push(_label)
          } else {
            _values.push('')
          }
        } else if (type.toLowerCase() === "currency") {
          _values.push(this._displayLocalizedDouble(_value))
        } else {

          _values.push(_value)
        }
      }
    }

    let _bindings = _.zipObject(_keys, _values)

    _keys.forEach(x => {

      _headline = _.replace(_headline, x, _bindings[x])
    })
    this.measure(__START_DATE__, 'prepareHeadlineData !!!')
    return _headline

  }

  aggregate(table, aggregationType, groupBy) {

    switch (aggregationType.toUpperCase()) {

      case "$COUNT":
        if (groupBy) {
          return _.size(_.keys(_.groupBy(_.filter(table, x => x[groupBy]), x => x[groupBy])))

        } else {
          return (table || []).length
        }

    }

    return 0
  }

  @action
  prepareTemplateHeadlineData(data, dataText, dataBinding, defaultPath) {
    try {


      let _headline = JSON.parse(JSON.stringify(dataText))

      let _keys = dataBinding.map(x => x.var)
      
      let _values = []
      for (let _dataBind1 of dataBinding) {
        
        if (_dataBind1.aggregationType) {
          _values.push(this.aggregate(data[_dataBind1.table], _dataBind1.aggregationType, _dataBind1.groupBy))

        } else if (_dataBind1.field) {
          let _value = data[_dataBind1.table][_dataBind1.field]
          let result = this
            .rootStore
            .metaStore
            .getFieldMeta(_dataBind1.table, _dataBind1.field)
          if (result.type === 'success') {
            const { type } = result.data
            if (type.toLowerCase() === "picklist") {
              const { picklistvalues } = result.data
              const _index = _.findIndex(picklistvalues, x => x.value === _value)

              if (_index >= 0) {
                const _label = picklistvalues[_index].label

                _values.push(_label)
              } else {
                _values.push('')
              }

            } else {

              _values.push(_value)
            }
          }
        }
      }
      
      let _bindings = _.zipObject(_keys, _values)

      _keys.forEach(_dataBind2 => {
        
        _headline = _.replace(_headline, _dataBind2, _bindings[_dataBind2])
      })


      return _headline

    } catch (error) {
      console.warn("p: " + error)
    }

    return null

  }

}