import { action, observable } from 'mobx'
import _ from 'lodash'
import * as Sentry from 'sentry-expo';
import { Icons } from '../../types'
/**
 * MOBX STORE - Default App Settings merged with the config received from the BE.
 */
export default class SettingsStore {

    constructor(rootStore: any) {
        this.prefix = ""
        this.rootStore = rootStore
        this.initRules()
        this.initTestAutomation()
        this.swiftVersion = '3.413.0'
    }

    /**
     * DataBase Prefix
     */
    prefix: string

    /**
     * Mobx Root Store
     */
    rootStore: any

    /**
     * App Rules
     */
    rules: any

    testAutomation : any

    /**
    * Current Swift Version
    */
    swiftVersion: string


    /**
     * Initiates the store with the defaults values of the app.
     */
    initRules() {

        this.prefix = this.rootStore.loginStore.getPrefix()
        this.rules = {
            backup: {
                enabled: false,
                autobackup: false,
                interval: 24
            },
            logs: {
                changesHistory: 500
            },
            sync: {
                intervalBasedSyncEnabled: false,
                quickStatusSync: true,
                syncIdLimit: 5000,
                interval: 10,
                custom: ["ON_SAVE_CHECKLIST", "ON_DUPLICATE"],
                performSpeedTest: false,
                performSpeedTestTimeout: 1000,
                minUploadSpeedInKbps: 300,
                minDownloadSpeedInKbps: 300

            },
            
            warnings: {
                configuration: true,
                offline: {
                    period: 240
                }
            },
            status: {
                hidden: [
                    "CLOSED",
                    "CANCELLED"
                ]
            },
            type: {
                colors: {
                    "INSTALLATION" : "#6d9137",
                    "INSPECTION" : "#00ff77",
                    "INCIDENT" : "red",
                    "REMOVAL" : "darkred",
                    "OVERHAUL" : "lightsteelblue",
                    "MAINTENANCE" : "turquoise",
                    "PREVENTIVE_MAINTENANCE" : "turquoise",
                    "CORRECTIVE_MAINTENANCE" : "orange",
                    "PREVENTIVE MAINTENANCE" : "turquoise",
                    "CORRECTIVE MAINTENANCE" : "orange",
                    "DEFAULT": "#00AAFF"
                }
            },
            timesheets: {
                readonlyExceptions :{
                    onEdit: false,
                    onDelete: false,
                    onAdd: false
                },
                useOdoMeterPrompt: false,
                odoMeterPrompt: {
                    start: ["ON_THE_GO"],
                    end: ["ARRIVED"]
                },
                dayplanner : false,
                stopwatch: false,
                stopwatchRules: {
                    start: [
                        {
                            status: "ON_THE_GO",
                            type: "Reizen"
                        },
                        {
                            status: "IN_PROGRESS",
                            type: "Servicebezoek"
                        }
                    ],
                    stop: [
                        "ARRIVED",
                        "CLOSED"
                    ]
                },
                filterOut: [                   
                    "Lunch",
                    "Vakantie",
                    "Training",
                    "Travel home",
                    "Vergadering",
                    "Training",
                    "Consignatie Nacht",
                    "Consignatie Dag",
                    "Consignatie Avond"
                ],
                implicit: true,
                manual: false,
                weekOverviewDisplayMode: "default",
                table: `${this.prefix}Timesheet__c`,
                groupBy: {
                  table: `${this.prefix}Timesheet_Lineitem__c`,
                  field: `${this.prefix}Type__c`,
                  icons: {
                    "Service visit": "wrench",
                    "Travel": "car",
                    "Internal meeting": "users",
                    "Lunch": "hourglass"
                  }
                }
            },
            pricing: {
                showAggregationsSubtotal: false,
                showAggregationsVat: false,
                showAggregationsTotal: false,
                showAggregationsSubtotalLabel: "${Label.Subtotal}",
                showAggregationsVatLabel: "${Label.VAT}",
                showAggregationsTotalLabel: "${Label.Total}",
                showActivitiesPrices: false,
                showPartPrices: true,
                showOtherExpensesPrices: true,
                pricingIncludeVAT: true,
                pricingPrecision: 2
            },
            items: {
                readonlyExceptions :{
                    onEdit: false,
                    onDelete: false,
                    onAdd: false
                },
                checklistShowActions:{
                    comments : true,
                    attachments : true
                },
                attachments: false,
                showWolidsForTypeDetailsOnSummary: false,
                useDetailsAsChecklist: false,
                showApplicableTypesAsHashtags: true,
                useApplicableProductTypes : true,
                useApplicableWorkOrderTypes : true,
                cloneWoiToWoli: false,
                supportedTypes: { "PART": "Part", "OTHER_EXPENSE": "Other expense", "ACTIVITY": "Activity" },
                statistics: {
                    "fieldName": "C",
                    "fieldsToRequest": [
                        "count(Id) C",
                        `${this.prefix}Type__c`
                    ],
                    "objectName": `${this.prefix}Work_Order_Item__c`,
                    "groupBy": [
                        `${this.prefix}Type__c`
                    ],
                    "skipRecordsFromBriefcase": false
                },
                limits: {
                    "maxItemsToDisplay": 25,
                    "maxResultToDisplay": 20
                },
                enableSorting: [],
                checklist: {
                    icons: [{ "Ok": Icons.CHECK }, { "Warning": Icons.WARNING }, { "Wrong": Icons.WRONG }]
                },

            },
            installedProducts: {
                enableSorting: [
					"Name",
                    "FIELDBUDDY__Installed_Product_Number__c"
				]
            },
            otherDefaults: {
                barcodeEnabled: false,
                showReleaseNotes: false,
                supportValidationRules: false,
                slackValidationRules: false,
                pagination: {
                    installedProducts: 5
                }
            },
            firstAid: {
                showRefreshApp: true,
                showForceStopStopwatch: true,
                showSendBackup: true,
            },
            scopeNames: {
                relatedWorkOrdersForInstalledProduct: {
                    initAt: `${this.prefix}Work_Order__c`,
                    scope: [
                        {
                            up: `${this.prefix}Installed_Product__c`,
                            down: `${this.prefix}Work_Order__c`,
                            relatedsObjectName:`${this.prefix}Work_Order_Line_Item__c`,
                            relatedObjectIsJunction:true
                        },
                        {
                            down: `${this.prefix}Service_Request__r.Installed_Product__c`,
                            up: "Id",
                            relatedsObjectName:`${this.prefix}Installed_Product__c`,
                            relatedObjectIsJunction:false
                        }
                    ]
                },
                relatedWorkOrdersForLocation: {
                    initAt: "FIELDBUDDY__Work_Order__c",
                    scope: [
                        {
                            up: "Id",
                            down: `${this.prefix}Location__c`,
                            relatedsObjectName: `${this.prefix}Location__c`,
                            relatedObjectIsJunction: false
                        },
                        {
                            up: "Id",
                            down: `${this.prefix}Service_Request__r.${this.prefix}Installed_Product__r.${this.prefix}Location__c`,
                            relatedsObjectName: `${this.prefix}Location__c`,
                            relatedObjectIsJunction: false
                        }
                    ]
                },
                relatedWorkOrdersForServiceRequest: {
                    initAt: `${this.prefix}Work_Order__c`,
                    scope: [
                        {
                            up: `${this.prefix}Service_Request__c`,
                            down: `${this.prefix}Service_Request__c`
                        },
                    ]
                }
            },
            attachments: {
                readonlyExceptions :{
                    onEdit: false,
                    onDelete: false,
                    onAdd: false
                },
                pdfMode: "download",
                cropOption: true,
                compression: 1.0,
                heicCompression: 0.5,
                maxPictures: 50,
                
                allowFilesUpload: true,
                allowPhotosUpload: true,

                maxUploadFileSize: 50,
                maxDownloadFileSize: 10,
                supportedFilesTypes: ["pdf", "xlsx", "docx", "pptx", "txt", "power_point_x", "excel_x", "word_x", "text",
                    "rtf", "ods", "csv", "odp", "epub", "odt", "unknown",
                    "xml",
                    "png", "jpg", "jpeg", "gif", "bmp", "tiff", "svg", "webp", "heif", "heic", "ico",
                    "json", "zip"]
                    
                
            },


        }
    }

    initTestAutomation() {
        this.testAutomation = {
            ownerId: "005G00000069mtTIAQ",
            baseWorkOrderId: "a0j3I000001MmD5QAK",
            displayTestAutomationElements : false
        }
    }

    /**
     * Formats a string to Swift Standars
     * ex: Other Expense -> OTHER_EXPENSE
     * @param unformatedString 
     */
    normalizeString(unformatedString: string): string {
        return unformatedString.split(" ").join("_").toUpperCase()
    }

    /**
     * Overwrites the default appSettings with the config sent from the BE
     */
    @action
    updateAppSettings() {
        const { loginStore } = this.rootStore
        
        if (loginStore.userConfig && !_.isEmpty(loginStore.userConfig)) {

            try {

                const { userConfig } = loginStore

                let settings = loginStore.getAppSettings()

                // SET APP RULES
                if (settings && settings.rules) {
                    this.setRules(settings)
                }


            } catch (err) {
                // Sentry.Native.captureException(new Error("Not able to get App Settings from Config."), {
                //     logger: 'settingsStore',
                //     context: 'getAppSettingsFromConfig'
                // });
            }

        }

    }

    /**
     * Overwrites the Pricing rules and Items rules with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setRules(settings: any) {
        try {
            
            this.setBackupsRules(settings)
            this.setLogsRules(settings)
            this.setSyncRules(settings)
            this.setTimesheetRules(settings)
            this.setPricingRules(settings)
            this.setItemsRules(settings)
            this.setWarnings(settings)
            
            this.setOtherDefaultsRules(settings)
            this.setFirstAidRules(settings)
            this.setScopeNames(settings)
            this.setAttachmentSettings(settings)
            this.setTestAutomation(settings)
            this.setStatusRules(settings)
            this.setTypeRules(settings)

            this.setInstalledProductsRules(settings)
        } catch (error) {
            //left on purpose
            console.warn('setRules', error)
            
        }

    }

    /**
     * Overwrites the backups rule with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setBackupsRules(settings: any) {
        if (settings.rules.hasOwnProperty("backup")) {
            //this.rules.backup.enabled = settings.rules.backup.hasOwnProperty("enabled") ? settings.rules.backup.enabled : false

            if (settings.rules.backup.hasOwnProperty("enabled")) {
                this.rules.backup.enabled = settings.rules.backup.enabled
            }

            let autobackup = settings.rules.backup?.autobackup
            this.rules.backup.autobackup = autobackup ? autobackup : false

            let interval = settings.rules.backup?.interval
            this.rules.backup.interval = interval ? interval : 24
        }

    }

    /**
     * Overwrites the logs rule with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setLogsRules(settings: any) {
        try {
            const { loginStore } = this.rootStore

            if (settings.rules.hasOwnProperty("logs")
                && settings.rules.logs.hasOwnProperty("changesHistory")) {
                this.rules.logs.changesHistory = (settings.rules.hasOwnProperty("logs")
                    && settings.rules.logs.hasOwnProperty("changesHistory"))
                    ? settings.rules.logs.changesHistory : 500
            }
        } catch (error) {
            //pass
        }
    }


    /**
     * Overwrites the sync rule with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setSyncRules(settings: any) {
        try {

            const { loginStore } = this.rootStore
            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("interval")) {
                this.rules.sync.interval = (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("interval")) ? settings.rules.sync.interval : 10
                loginStore.setSyncInterval = Math.max(this.rules.sync.interval, 0)
            }
            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("syncIdLimit")) {
                this.rules.sync.syncIdLimit = (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("syncIdLimit")) ? settings.rules.sync.syncIdLimit : 5000
                
            }
            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("custom")) {
                this.rules.sync.custom = (settings.rules.hasOwnProperty("sync") 
                && settings.rules.sync.hasOwnProperty("custom")) ? settings.rules.sync.custom : ["ON_SAVE_CHECKLIST", "ON_DUPLICATE"]
            }

            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("intervalBasedSyncEnabled")) {
                this.rules.sync.intervalBasedSyncEnabled = (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("intervalBasedSyncEnabled")) ? settings.rules.sync.intervalBasedSyncEnabled : false                
            }

            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("quickStatusSync")) {
                this.rules.sync.quickStatusSync = settings.rules.sync.quickStatusSync
            }

            /*
            performSpeedTest: true,
            performSpeedTestTimeout: 1000,
                minUploadSpeedInKbps: 300,
                minDownloadSpeedInKbps: 300
            */
            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("performSpeedTest")) {
                this.rules.sync.performSpeedTest = settings.rules.sync.performSpeedTest
            }
            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("performSpeedTestTimeout")) {
                this.rules.sync.performSpeedTestTimeout = settings.rules.sync.performSpeedTestTimeout
            }
            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("minUploadSpeedInKbps")) {
                this.rules.sync.minUploadSpeedInKbps = settings.rules.sync.minUploadSpeedInKbps              
            }
            if (settings.rules.hasOwnProperty("sync") && settings.rules.sync.hasOwnProperty("minDownloadSpeedInKbps")) {
                this.rules.sync.minDownloadSpeedInKbps =  settings.rules.sync.minDownloadSpeedInKbps
            }

            
        } catch (error) {
           //pass
        }
    }

    /**
     * Overwrites the warning rule with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setWarnings(settings: any) {
        if (settings.rules.hasOwnProperty("warnings") && settings.rules.warnings.hasOwnProperty("configuration")) {
            this.rules.warnings.configuration = (settings.rules.hasOwnProperty("warnings") && settings.rules.warnings.hasOwnProperty("configuration")) ? settings.rules.warnings.configuration : true
        }
        try {


            if (settings.rules.hasOwnProperty("warnings") && settings.rules.warnings.hasOwnProperty("offline")) {
                this.rules.warnings.offline = (settings.rules.hasOwnProperty("offline") && settings.rules.warnings.hasOwnProperty("offline")) ? settings.rules.warnings.offline : {}

            }
            if (settings.rules.hasOwnProperty("warnings") && settings.rules.warnings.hasOwnProperty("offline")
                && settings.rules.warnings.offline.hasOwnProperty("period")) {

                this.rules.warnings.offline["period"] = settings.rules.warnings.offline.period
            }
        } catch (error) {

        }
    }

    /**
     * Overwrites the Scope Names with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setScopeNames(settings: any) {

        if (settings && settings.rules && settings.rules.scopeNames) {
            this.rules.scopeNames = { ...settings.rules.scopeNames }
        }
    }

    @action
    setStatusRules(settings: any) {
        if (settings.rules.hasOwnProperty("status")) {
            if (settings.rules.status.hasOwnProperty("hidden")) this.rules.status.hidden = settings.rules.status.hidden
        }
    }

    @action
    setTypeRules(settings: any) {
        if (settings.rules.hasOwnProperty("type")) {
            if(settings.rules.type.hasOwnProperty("colors")) this.rules.type.colors = settings.rules.type.colors
        }
    }

    @action
    setAttachmentSettings(settings: any) {

        if (settings && settings.rules && settings.rules.attachments) {
            if (settings.rules.attachments.hasOwnProperty("pdfMode")) this.rules.attachments.pdfMode = settings.rules.attachments.pdfMode
            if (settings.rules.attachments.hasOwnProperty("cropOption")) this.rules.attachments.cropOption = settings.rules.attachments.cropOption
            if (settings.rules.attachments.hasOwnProperty("compression")) this.rules.attachments.compression = settings.rules.attachments.compression
            if (settings.rules.attachments.hasOwnProperty("heicCompression")) this.rules.attachments.heicCompression = settings.rules.attachments.heicCompression
            if (settings.rules.attachments.hasOwnProperty("maxPictures")) this.rules.attachments.maxPictures = settings.rules.attachments.maxPictures

            if (settings.rules.attachments.hasOwnProperty("allowFilesUpload")) this.rules.attachments.allowFilesUpload = settings.rules.attachments.allowFilesUpload
            if (settings.rules.attachments.hasOwnProperty("allowPhotosUpload")) this.rules.attachments.allowPhotosUpload = settings.rules.attachments.allowPhotosUpload

            if (settings.rules.attachments.hasOwnProperty("maxUploadFileSize")) this.rules.attachments.maxUploadFileSize = settings.rules.attachments.maxUploadFileSize
            if (settings.rules.attachments.hasOwnProperty("maxDownloadFileSize")) this.rules.attachments.maxDownloadFileSize = settings.rules.attachments.maxDownloadFileSize            
            if (settings.rules.attachments.hasOwnProperty("supportedFilesTypes")) this.rules.attachments.supportedFilesTypes = settings.rules.attachments.supportedFilesTypes
            if (settings.rules.attachments.readonlyExceptions) {
                const { readonlyExceptions } = settings.rules.attachments
                if (readonlyExceptions.hasOwnProperty("onAdd")) this.rules.attachments.readonlyExceptions.onAdd = readonlyExceptions.onAdd
                if (readonlyExceptions.hasOwnProperty("onEdit")) this.rules.attachments.readonlyExceptions.onEdit = readonlyExceptions.onEdit
                if (readonlyExceptions.hasOwnProperty("onDelete")) this.rules.attachments.readonlyExceptions.onDelete = readonlyExceptions.onDelete
            }
        }
    }

    @action
    setTimesheetRules(settings: any) {
        
        if (settings && settings.rules && settings.rules.timesheets) {
            if (settings.rules.timesheets.hasOwnProperty("dayplanner")) this.rules.timesheets.dayplanner = settings.rules.timesheets.hasOwnProperty("dayplanner") ? settings.rules.timesheets.dayplanner : false
            if (settings.rules.timesheets.hasOwnProperty("stopwatch")) this.rules.timesheets.stopwatch = settings.rules.timesheets.stopwatch
            if (settings.rules.timesheets.hasOwnProperty("stopwatchRules")) this.rules.timesheets.stopwatchRules = settings.rules.timesheets.stopwatchRules
            if (settings.rules.timesheets.hasOwnProperty("implicit")) this.rules.timesheets.implicit = settings.rules.timesheets.implicit
            if (settings.rules.timesheets.hasOwnProperty("manual")) this.rules.timesheets.manual = settings.rules.timesheets.manual
            if (settings.rules.timesheets.hasOwnProperty("weekOverviewDisplayMode")) this.rules.timesheets.weekOverviewDisplayMode = settings.rules.timesheets.weekOverviewDisplayMode
            if (settings.rules.timesheets.hasOwnProperty("table")) this.rules.timesheets.table = settings.rules.timesheets.table
            if (settings.rules.timesheets.hasOwnProperty("filterOut")) this.rules.timesheets.filterOut = settings.rules.timesheets.filterOut
            
            if (settings.rules.timesheets.hasOwnProperty("useOdoMeterPrompt")) this.rules.timesheets.useOdoMeterPrompt = settings.rules.timesheets.useOdoMeterPrompt
            if (settings.rules.timesheets.hasOwnProperty("odoMeterPrompt")) this.rules.timesheets.odoMeterPrompt = settings.rules.timesheets.odoMeterPrompt
            
            if (settings.rules.timesheets.groupBy) {
                const { groupBy } = settings.rules.timesheets
                
                // this.rules.timesheets.groupBy = {}
                
                if (groupBy.hasOwnProperty("table")) this.rules.timesheets.groupBy.table = groupBy.table
                if (groupBy.hasOwnProperty("field")) this.rules.timesheets.groupBy.field = groupBy.field
                if (settings.rules.timesheets.groupBy.icons) {
                    
                    this.rules.timesheets.groupBy.icons = _.clone(settings.rules.timesheets.groupBy.icons)
                    
                }
            }

            if (settings.rules.timesheets.readonlyExceptions) {
                const { readonlyExceptions } = settings.rules.timesheets
                if (readonlyExceptions.hasOwnProperty("onAdd")) this.rules.timesheets.readonlyExceptions.onAdd = readonlyExceptions.onAdd
                if (readonlyExceptions.hasOwnProperty("onEdit")) this.rules.timesheets.readonlyExceptions.onEdit = readonlyExceptions.onEdit
                if (readonlyExceptions.hasOwnProperty("onDelete")) this.rules.timesheets.readonlyExceptions.onDelete = readonlyExceptions.onDelete
            }
        }
    }

    /**
     * Overwrites the Pricing rules with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setPricingRules(settings: any) {
        if (settings && settings.rules && settings.rules.pricing) {
            if (settings.rules.pricing.hasOwnProperty("includeVAT")) this.rules.pricing.pricingIncludeVAT = settings.rules.pricing.includeVAT
            if (settings.rules.pricing.hasOwnProperty("precision")) this.rules.pricing.pricingPrecision = settings.rules.pricing.precision
            if (settings.rules.pricing.showPrices) {
                const { showPrices } = settings.rules.pricing
                if (showPrices.hasOwnProperty("activities")) this.rules.pricing.showActivitiesPrices = showPrices.activities
                if (showPrices.hasOwnProperty("parts")) this.rules.pricing.showPartPrices = showPrices.parts
                if (showPrices.hasOwnProperty("otherExpenses")) this.rules.pricing.showOtherExpensesPrices = showPrices.otherExpenses
            }
            if (settings.rules.pricing.showAggregations) {
                const { showAggregations } = settings.rules.pricing
                if (showAggregations.hasOwnProperty("subtotal")) this.rules.pricing.showAggregationsSubtotal = showAggregations.subtotal
                if (showAggregations.hasOwnProperty("vat")) this.rules.pricing.showAggregationsVat = showAggregations.vat
                if (showAggregations.hasOwnProperty("total")) this.rules.pricing.showAggregationsTotal = showAggregations.total

                if (showAggregations.hasOwnProperty("subtotalLabel")) this.rules.pricing.showAggregationsSubtotalLabel = showAggregations.subtotalLabel
                if (showAggregations.hasOwnProperty("vatLabel")) this.rules.pricing.showAggregationsVatLabel = showAggregations.vatLabel
                if (showAggregations.hasOwnProperty("totalLabel")) this.rules.pricing.showAggregationsTotalLabel = showAggregations.totalLabel
            }
        }
    }

    /**
     * Overwrites the items rules with the config received from the BE
     * @param settings -> config received from the BE
     */
    @action
    setItemsRules(settings: any) {
        if (!_.isEmpty(settings) && settings.hasOwnProperty("rules") && settings.rules.hasOwnProperty("items")) {
            this.rules.items.enableSorting = settings.rules.items.hasOwnProperty("enableSorting") ? settings.rules.items.enableSorting : []
            this.rules.items.supportedTypes = settings.rules.items.hasOwnProperty("supportedTypes") ? settings.rules.items.supportedTypes : { "PART": "Part", "OTHER_EXPENSE": "Other expense", "ACTIVITY": "Activity" }
            this.rules.items.attachments = settings.rules.items.hasOwnProperty("attachments") ? settings.rules.items.attachments : false
           
            
            if (settings.rules.items.hasOwnProperty("useDetailsAsChecklist")) {
                this.rules.items.useDetailsAsChecklist = settings.rules.items.useDetailsAsChecklist
            }

            if (settings.rules.items.hasOwnProperty("cloneWoiToWoli")) {
                this.rules.items.cloneWoiToWoli = settings.rules.items.cloneWoiToWoli
            }

            this.rules.items.useApplicableProductTypes = settings.rules.items.hasOwnProperty("useApplicableProductTypes") ? settings.rules.items.useApplicableProductTypes : true
            this.rules.items.useApplicableWorkOrderTypes = settings.rules.items.hasOwnProperty("useApplicableWorkOrderTypes") ? settings.rules.items.useApplicableWorkOrderTypes : true
            this.rules.items.showApplicableTypesAsHashtags = settings.rules.items.hasOwnProperty("showApplicableTypesAsHashtags") ? settings.rules.items.showApplicableTypesAsHashtags : true
            this.rules.items.showWolidsForTypeDetailsOnSummary = settings.rules.items.hasOwnProperty("showWolidsForTypeDetailsOnSummary") ? 
            settings.rules.items.showWolidsForTypeDetailsOnSummary : false
            if (settings.rules.items.readonlyExceptions) {
                const { readonlyExceptions } = settings.rules.items
                if (readonlyExceptions.hasOwnProperty("onAdd")) {
                    this.rules.items.readonlyExceptions.onAdd = readonlyExceptions.onAdd
                }
                if (readonlyExceptions.hasOwnProperty("onEdit")) {
                    this.rules.items.readonlyExceptions.onEdit = readonlyExceptions.onEdit
                }
                if (readonlyExceptions.hasOwnProperty("onDelete")) {
                    this.rules.items.readonlyExceptions.onDelete = readonlyExceptions.onDelete
                }
            }

            if (settings.rules.items.checklistShowActions) {
                const { checklistShowActions } = settings.rules.items
                if (checklistShowActions.hasOwnProperty("comments")) this.rules.items.checklistShowActions.comments = checklistShowActions.comments
                if (checklistShowActions.hasOwnProperty("attachments")) this.rules.items.checklistShowActions.attachments = checklistShowActions.attachments
            }
        }
    }

    /*
     "otherDefaults": {
                "barcodeEnabled": true,
                "pagination": {
                    "installedProducts": 5
                }
            },
    */
    @action
    setOtherDefaultsRules(settings: any) {
        if (!_.isEmpty(settings) && settings.hasOwnProperty("rules") && settings.rules.hasOwnProperty("otherDefaults")) {
            if(settings.rules.otherDefaults.hasOwnProperty("barcodeEnabled")){
                this.rules.otherDefaults.barcodeEnabled = settings.rules.otherDefaults.barcodeEnabled
            }
            // if(settings.rules.otherDefaults.hasOwnProperty("showReleaseNotes")){
            //     this.rules.otherDefaults.showReleaseNotes =  settings.rules.otherDefaults.showReleaseNotes 
            // }
                
            if (settings.rules.otherDefaults.hasOwnProperty("pagination")) {
                this.rules.otherDefaults.pagination = settings.rules.otherDefaults.pagination
            }

            if (settings.rules.otherDefaults.hasOwnProperty("supportValidationRules")) {
                this.rules.otherDefaults.supportValidationRules = settings.rules.otherDefaults.supportValidationRules
            }

            if (settings.rules.otherDefaults.hasOwnProperty("slackValidationRules")) {
                this.rules.otherDefaults.slackValidationRules = settings.rules.otherDefaults.slackValidationRules
            }
        }
    }

    /*
        "installedProducts": {
            "enableSorting": [
                "Name",
                "FIELDBUDDY__Installed_Product_Number__c"
            ]
        },
    */
    @action
    setInstalledProductsRules(settings: any) {
        try {
            if (!_.isEmpty(settings) && settings.hasOwnProperty("rules") && settings.rules.hasOwnProperty("installedProducts")) {
                if (settings.rules.installedProducts.hasOwnProperty("enableSorting")) {
                    this.rules.installedProducts.enableSorting = settings.rules.installedProducts.enableSorting
                }
            }
        } catch (error) {
            //pass       
        }
    }


    /*
        firstAid: {
            showRefreshApp: false,
            showForceStopStopwatch: false,
            showSendBackup: false,
        },
    */

    @action
    setFirstAidRules(settings: any) {
        if (!_.isEmpty(settings) && settings.hasOwnProperty("rules") && settings.rules.hasOwnProperty("firstAid")) {
            if (settings.rules.firstAid.hasOwnProperty("showRefreshApp")) {
                this.rules.firstAid.showRefreshApp = settings.rules.firstAid.showRefreshApp
            }
            if (settings.rules.firstAid.hasOwnProperty("showForceStopStopwatch")) {
                this.rules.firstAid.showForceStopStopwatch = settings.rules.firstAid.showForceStopStopwatch
            }
            if (settings.rules.firstAid.hasOwnProperty("showSendBackup")) {
                this.rules.firstAid.showSendBackup = settings.rules.firstAid.showSendBackup
            }
        }
    }

    @action
    getScope(scopeName: string) {
        try {

            const { scopeNames } = this.rules

            if (scopeNames.hasOwnProperty(scopeName)) {
                return scopeNames[scopeName]
            }
        } catch (error) {
            //pass
        }
        return null
    }

    @action
    setTestAutomation(settings: any) {
        try {
            const { loginStore } = this.rootStore

            let ownerId = settings.testAutomation?.ownerId
            this.testAutomation.ownerId = ownerId ? ownerId : "005G00000069mtTIAQ"

            let baseWorkOrderId = settings.testAutomation?.baseWorkOrderId
            this.testAutomation.baseWorkOrderId = baseWorkOrderId ? baseWorkOrderId : "a0j3I000001MmD5QAK"

            let displayTestAutomationElements = settings.testAutomation?.displayTestAutomationElements
            this.testAutomation.displayTestAutomationElements = displayTestAutomationElements ? displayTestAutomationElements : false
            }
        catch(e) {

        }
    }
       
}
