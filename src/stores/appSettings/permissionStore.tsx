
import _ from 'lodash';
import { observable } from 'mobx';
import * as Permissions from 'expo-permissions';
import { PermissionType } from 'expo-permissions';
import { Camera } from 'expo-camera';
import * as MediaLibrary from 'expo-media-library';
import * as Location from 'expo-location';
import { Alert, Platform, TouchableHighlightBase, Linking } from 'react-native';
import * as IntentLauncher from 'expo-intent-launcher';

export const GRANTED = "granted";
export const WHEN_IN_USE = "whenInUse";
export const ALWAYS = "always";
export const DENIED = "denied";
export const UNDETERMINED = "undetermined";

export enum PermissionTypes {
    CAMERA = "camera",
    MEDIA_LIBRARY = "mediaLibrary",
    AUDIO_RECORDING = "audioRecording",
    
    LOCATION_BACKGROUND = "location_background",
    LOCATION_FOREGROUND = "location_foreground",
    USER_FACING_NOTIFICATIONS = "userFacingNotifications",
    NOTIFICATIONS = "notifications",
    CONTACTS = "contacts",
    CALENDAR = "calendar",
    REMINDERS = "reminders"
}

/**
 * MobX Permissions Store
 */
export default class PermissionStore {

    constructor(rootStore: any) {
        this.rootStore = rootStore,
            this.isAskingBackgroundLocation = false,
            this.isAskingPhotos = false,
            this.notifications = DENIED,
            this.userFacingNotifications = DENIED,
            
            this.location_background = DENIED
            this.location_foreground = DENIED

            this.camera = DENIED,
            this.audioRecording = DENIED,
            this.contacts = DENIED,
            this.cameraRoll = DENIED,
            this.calendar = DENIED,
            this.reminders = DENIED,

            this.allChecked = this.checkAll()
    }

    rootStore: any

    @observable
    isAskingBackgroundLocation: boolean

    @observable
    isAskingPhotos: boolean

    @observable
    notifications: string

    @observable
    userFacingNotifications: string

    

    @observable
    location_background: string

    @observable
    location_foreground: string

    @observable
    camera: string

    @observable
    audioRecording: string

    @observable
    contacts: string

    @observable
    cameraRoll: string

    @observable
    calendar: string

    @observable
    reminders: string

    allChecked: Promise<boolean>


    /**
     * Checks ALL the permissions and stores the results in the Store State
     */
    async checkAll() {
        if(__DEV__){
            return;
        }
        
        this.notifications = await this.checkPermission(Permissions.NOTIFICATIONS)
        this.userFacingNotifications = await this.checkPermission(Permissions.USER_FACING_NOTIFICATIONS)
        
        this.audioRecording = await this.checkPermission(Permissions.AUDIO_RECORDING)
        this.contacts = await this.checkPermission(Permissions.CONTACTS)
        this.calendar = await this.checkPermission(Permissions.CALENDAR)
        this.reminders = await this.checkPermission(Permissions.REMINDERS)
        
        await this.checkCameraAndCameraRoll()
        
       
        await this._permissionsForLocation();
        
        
        return true
    }

    private async _permissionsForLocation() {
        if (Platform.OS === 'ios') {
            if (this.isAskingPhotos) return;
        }
        
        try {

            this.location_foreground = await this.checkPermissionLocationForeground()
            // this.location_background = await this.checkPermissionLocationBackground2()

            if (this.GRANTED_OR_ALWAYS_OR_WHEN_IN_USE(this.location_foreground)) {
                await this._backgroundLocationAccess()
            } else {
                
                this.location_background = this.location_foreground
            }



        } catch (error) {
            
        }


    }

    private async _backgroundLocationAccess() {
        try {
            
            if (!this.GRANTED_OR_ALWAYS_OR_WHEN_IN_USE_OR_UNDETERMINED(this.location_background)) {                
                await this._prominentBackgroundLocationDialog()
                
            }
        } catch (error) {

        }


    }
    
    async checkCameraAndCameraRoll() {
        this.camera = await this._checkPermissionCamera()
        if (Platform.OS === 'android') {
            this.cameraRoll = await this._checkPermissionMediaLibrary()
        } else if (Platform.OS === 'ios') {
            await this._checkPermissionMediaLibraryIOS()
        }
    }



    GRANTED_OR_ALWAYS_OR_WHEN_IN_USE(status: string) {
        return status === GRANTED || status === ALWAYS || status === WHEN_IN_USE
    }

    GRANTED_OR_ALWAYS_OR_WHEN_IN_USE_OR_UNDETERMINED(status: string) {
        return status === GRANTED || status === ALWAYS || status === WHEN_IN_USE || status === UNDETERMINED
    }

    

    /**
     * Checks if the permission passed as an argument has been granted
     * @param perm 
     */
    async checkPermission(perm: PermissionType) {
        try {


            if (this.isPermissionType(perm)) {

                const { status } = await Permissions.getAsync(perm)
                return status
            } else {
                return DENIED
            }
        } catch (error) {
            console.warn("E" + perm + " :" + error)
        }
        return DENIED
    }

    private async _checkPermissionCamera() {
        
        try {
            let { status } = await Camera.getCameraPermissionsAsync();
            
            if (status !== GRANTED) {
                const { status } = await Camera.requestCameraPermissionsAsync();

                return status
            } else {
                return status
            }
        } catch (error) {
            //pass  
        }
        return DENIED
    }



    private async _checkPermissionMediaLibrary() {

        try {

            let { accessPrivileges } = await MediaLibrary.getPermissionsAsync();

            if (accessPrivileges === 'all' || accessPrivileges === 'limited') {
                return GRANTED
            } else {
                const { status } = await MediaLibrary.requestPermissionsAsync();

                return (status === 'granted') ? GRANTED : DENIED

            }

        } catch (error) {
            //pass
        }
        return DENIED
    }


    private async _checkPermissionMediaLibraryIOS() {

        try {
            if (this.isAskingPhotos) return;

            this.isAskingPhotos = true

            let { accessPrivileges } = await MediaLibrary.getPermissionsAsync();

            if (accessPrivileges === 'all' || accessPrivileges === 'limited') {
                this.cameraRoll = GRANTED
                this.isAskingPhotos = false
                await this._permissionsForLocation();
            } else {

                

                const PERMISSIONS = this.rootStore.loginStore
                    .getPermissions()

                const { MEDIA_LIBRARY } = PERMISSIONS
                Alert.alert(MEDIA_LIBRARY.DIALOG_TITLE, MEDIA_LIBRARY.ASK_FOR_PERMISSION, [
                    {
                        text: 'OK',
                        onPress: async () => {
                            const { status } = await MediaLibrary.requestPermissionsAsync();
                            this.cameraRoll = (status === 'granted') ? GRANTED : DENIED
                            this.isAskingPhotos = false
                            await this._permissionsForLocation();
                        }
                    },
                    {
                        text: MEDIA_LIBRARY.ASK_ME_LATER_BUTTON,
                        onPress: async () => {
                            this.isAskingPhotos = false
                            await this._permissionsForLocation();
                        }
                    }
                ], { cancelable: false })
            }
        } catch (error) {
            //pass
            this.isAskingPhotos = false
        }
    }


    async _prominentBackgroundLocationDialog() {
        try {
            if (this.isAskingBackgroundLocation) return;

            this.isAskingBackgroundLocation = true
            let that = this


            let { status: ss } = await Location.getBackgroundPermissionsAsync()

            this.location_background = ss

            if (this.GRANTED_OR_ALWAYS_OR_WHEN_IN_USE(this.location_background)) return;

            const PERMISSIONS = this.rootStore.loginStore
                .getPermissions()

            const { BACKGROUND_LOCATION } = PERMISSIONS


            Alert.alert(BACKGROUND_LOCATION.DIALOG_TITLE, BACKGROUND_LOCATION.ASK_FOR_PERMISSION, [
                {
                    text: 'OK',
                    onPress: async () => {


                        if (!this.GRANTED_OR_ALWAYS_OR_WHEN_IN_USE(this.location_background)) {

                            if (Platform.OS === 'ios') {
                                let bb = await this.checkPermissionLocationBackground2()
                                that.location_background = bb


                            } else {
                                let { status } = await Location.requestBackgroundPermissionsAsync();

                                this.location_background = status;
                            }

                        }

                        // this.isAskingBackgroundLocation = false;

                    }
                }
            ], { cancelable: false })
        } catch (error) {
            this.isAskingBackgroundLocation = false
        }
    }

    async checkPermissionLocationForeground() {
        let { status } = await Location.requestForegroundPermissionsAsync();
        return status
    }

    async checkPermissionLocationBackground2() {
        let { status } = await Location.requestBackgroundPermissionsAsync();
        return status
    }

    /**
     * Checks if the arg type is PermissionType
     * @param arg 
     */
    isPermissionType(arg: any): arg is PermissionType {
        return (
            arg && Object.values(PermissionTypes).includes(arg)
        )
    }

    /**
     * Checks if the permission is already granted. If is not, then it ask for the permission to the user.
     * After that ir saves the result to the store state
     * @param this 
     * @param perm 
     */
    async askPermission(this: any, perm: PermissionType) {
        if (this.isPermissionType(perm) && this[perm] !== GRANTED) {
            const { status } = await Permissions.askAsync(perm);
            this[perm] = status === GRANTED ? GRANTED : DENIED
        }
    }

    async askPermissions(this: any, perms: Array<PermissionType>) {
        let ok: boolean = true
        for (let perm of perms) {
            await this.askPermission(perm)
            ok = ok && (this[perm] === GRANTED)
        }

        return ok
        // return await Promise.all(perms.map(perm => {
        //     {
        //          ;
        //         return this.askPermission(perm);
        //     }
        // }))
    }


    /**
     * Opens the device settings
     */
    openSettings = () => {
        if (Platform.OS == 'ios') {
            Linking
                .canOpenURL('app-settings:')
                .then((supported: any) => {
                    if (!supported) {
                    } else {
                        return Linking.openURL('app-settings:');
                    }
                })
                .catch((err: any) => {
                })
        }
        else {
            IntentLauncher.startActivityAsync(IntentLauncher.ActivityAction.APPLICATION_SETTINGS);
            // IntentLauncher.startActivityAsync(IntentLauncher.ACTION_MANAGE_ALL_APPLICATIONS_SETTINGS);
        }
    }

}
