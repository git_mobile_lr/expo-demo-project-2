import React from 'react'
import {
    action,
    reaction,
    autorun,
    computed,
    observable
} from 'mobx'
import _ from 'lodash'
import moment from 'moment-timezone'
import jsonQuery from 'json-query'
import {enDialogs, nlDialogs} from '../../i18n/dialogs'




//import remotedev from 'mobx-remotedev/lib/dev';

//@remotedev({ remote:false })
export default class LocalizeStore {

    constructor(rootStore) {
        this.rootStore = rootStore
        
        this.activeDate = moment()
        
        this.activeLocale = 'nl-NL'
        this.activeLanguage = 'NL'
        this.activeZone = "BE"
        this.activeCurrency = "EUR"
        this.currency = "€"
        this.supportedZones = ['NL','BE','PL']
        this.supportedCurrencies = ['EUR']
        this.supportedLanguages = ['EN','NL','DE','DA','FR']
    }

   

    getActiveLocale(){
        try {


            return this.activeLocale
        //    const Localization from 'expo-localization'
        } catch (error) {
              
        }
        
       return this.activeLocale
    }

    @observable
    activeLocale

    @observable
    activeCurrency

    @observable
    activeLanguage

    @observable
    activeZone

    @observable
    utcOffset

    // @computed
    // get deviceLocale(){
    //     try {
    //           
    //         return 'nl'
    //         // return Localization.locale
    //     } catch (error) {
    //           
    //         return 'nl'
    //     }
        
    // }

     
    @computed get
    activeDialogs() {
        const _NL_ = nlDialogs
        const _EN_ = enDialogs
        switch (this.activeLanguage) {
            case "NL":  return _NL_
            default: return _EN_
        }

    }

    @action
    setUtcOffset(value){
        this.utcOffset = value
    }

    @action
    setActiveZone(value){
        if (this.supportedZones.includes(value.toUpperCase())){
            this.activeZone = value.toUpperCase()
            return true
        }
        return false
    }

    @action
    setActiveLanguage(value){
        if (this.supportedLanguages.includes(value.toUpperCase())){
            this.activeLanguage = value.toUpperCase()
            return true
        }
        return false
        
    }

    @action
    setActiveCurrency(){
        if (this.supportedCurrencies.includes(value.toUpperCase())){
            this.activeCurrency = value.toUpperCase()
            return true
        }
        return false
    }



}
