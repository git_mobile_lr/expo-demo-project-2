import { action, observable } from "mobx";

/**
 * MOBX Store for FieldBuddy Settings
 * @ It will be updated when user try to hard sync
 * @ Members
 * * payloadLimit: string
 * * enterprise: boolean
 * * enchancedMap: boolean 
 *   ** this is for "Enable Enhanced Map in Fieldbuddy Swift" Option in Fieldbuddy Swift Setup on saleforce backend configuration panel, Settings tab
 */

/**
 * Store settings to device.
 * 
 */

export default class FieldBuddySettings {
  constructor(rootStore: any) {
    this.prefix = "";
    this.rootStore = rootStore;
    this.payloadLimit = "6100";
    this.enterprise = false; 
    this.enhancedMap = false;
  }

  /**
   * DataBase Prefix
   */
  prefix: string;

  /**
   * Mobx Root Store
   */
  rootStore: any;
  
  /**
   * payloadLimit: string
   */
  @observable
  payloadLimit: string;

  /**
   * enterprice: boolean
  */
  @observable
  enterprise: boolean;

  /**
   * enhancedMap: boolean;
   */
  @observable
  enhancedMap: boolean;
  
  @action
  hasEnhancedMap(){
    //for a quick testing purpose:
    //return false
    return this.enhancedMap;
  }


  /**
   * Set setting values
   */
  @action
  updateSetting(settings: any){
    if (!settings) return;
    
    if (settings.hasOwnProperty('payloadLimit')) {
      this.payloadLimit = settings.payloadLimit;
    }

    if (settings.hasOwnProperty('enterprise')) {
      this.enterprise = settings.enterprise;
    }

    if (settings.hasOwnProperty('enhancedMap')){
      this.enhancedMap = settings.enhancedMap;
    }
  }

  @action
  getSettings() {
    return {
      payloadLimit: this.payloadLimit,
      enterprise: this.enterprise,
      enhancedMap: this.enhancedMap
    }
  }

}
