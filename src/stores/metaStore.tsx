import { action, computed, observable, trace } from 'mobx'
import _ from 'lodash'
import * as Sentry from 'sentry-expo';
import { computedFn } from "mobx-utils"


/**
 * INTERFACES
 */

export interface TableMetaData {
    updateable: Boolean
    relationships?: Array<Relationship>
    recordtypes?: Object
    prefixid: String
    plurallabel: String
    name: String
    label: String
    fields: Array<any>
    deletable: Boolean
    createable: Boolean
    childrelationships?: Array<any>
    accesible: Boolean
}

export interface Relationship {
    ParentToChildFieldName: String
    ParentObject: String
    ChildToParentFieldName: String
    ChildObject: String
}

export interface MetaData {
    [key: string]: TableMetaData
}

export enum ResultType {
    "success",
    "error"
}

export interface Result {
    type: ResultType | string
    data: any
}



/**
 * MOBX STORE - Meta Data Handler
 */

export default class MetaStore {

    constructor(rootStore: any) {
        this.rootStore = rootStore
        this.meta = {}
        this.relationships = []
    }

    /**
     * Mobx Stores
     */
    rootStore: any

    /**
     * sObjects present in the Meta Data. It could be either retrieved from the BE or from the device local storage.
     */
    @observable
    meta: MetaData

    /**
     * Relationships present in the Meta Data. It could be either retrieved from the BE or from the device local storage.
     */
    @observable
    relationships: Array<Relationship> | []

    /**
     * Sets only the sObjects from METADATA
     * @param newMeta : MetaData
     */
    @action
    setMeta(newMeta: MetaData) {
        try {
            if (!_.isEmpty(newMeta)) {
                this.meta = newMeta
            } else {
                this.meta = {}
            }
        } catch (e) {
            // Sentry.Native.captureException(new Error("Not able to set meta data."), {
            //     logger: 'MetaStore',
            //     context: 'setMeta'
            // });
        }
    }

    /**
     * Sets only the relationships from METADATA
     * @param newRelationships : Array<Relationship>
     */
    @action
    setRelationships(newRelationships: Array<Relationship>) {
        try {
            if (!_.isEmpty(newRelationships)) {
                this.relationships = newRelationships
            } else {
                this.relationships = []
            }
        } catch (e) {
            // Sentry.Native.captureException(new Error("Not able to set relationships."), {
            //     logger: 'MetaStore',
            //     context: 'setRelationships'
            // });
        }
    }

    /**
     * Sets to null meta and relationships
     */
    @action
    resetMeta = () => {
        try {
            const { loginStore } = this.rootStore
            this.meta = {}
            this.relationships = []
            loginStore.meta = null
        } catch (e) {
            // Sentry.Native.captureException(new Error("Not able to reset meta data."), {
            //     logger: 'MetaStore',
            //     context: 'resetMeta'
            // });
        }
    }

    @action
    getMissingFieldsAndTables(requirements : Object) {
        const missing = []
        for (const [tableName, required] of Object.entries(requirements)) {
            if (this.meta.hasOwnProperty(tableName)) {
                const presentfields = (this.meta[tableName].fields || []).map(field => field.name)
                required.fields.forEach((field:string) => {
                    if (!presentfields.includes(field)) {
                        missing.push(`Table: ${tableName} -> Field: ${field}`)
                    }
                })
            } else {
                missing.push(`Table: ${tableName}`)
                required.fields.forEach((field:string) => missing.push(`Table: ${tableName} -> Field: ${field}`))
            }
        }
        return missing
    }

    /**
     * Returns Meta Data for a specific field
     * @param tableName :string
     * @param fieldName :string
     */
    @action
    getFieldMeta(tableName: string, fieldName: string): Result {
        try {
            let result = null

            if (this.meta.hasOwnProperty(tableName)) {

                result = (this.meta[tableName].fields || []).filter((item: any) => item.name === fieldName)
            }

            if (!_.isEmpty(result)) {
                return {
                    type: "success",
                    data: _.first(result)
                }
            }

            return (
                {
                    type: "error",
                    data: "No results"
                }
            )

        } catch (e) {
            // Sentry.Native.captureException(new Error("Not able get meta."), {
            //     logger: 'MetaStore',
            //     context: 'getFieldMeta'
            // });
        }

        return (
            {
                type: "error",
                data: "No results"
            }
        )
    }

    @action
    getLabelForPicklistValue(tableName: any, fieldName: any, value: any) {
        try {
        const result = this.getFieldMeta(tableName, fieldName)

        if (result.type === "success") {
            if (!_.isEmpty(result.data.picklistvalues)) {
                const index = _.findIndex(result.data.picklistvalues, (x:any) => x.value === value)
                if (index >= 0) {
                    return result.data.picklistvalues[index].label
                }
            }
        }
        } catch (e) { }

        return value
    }

    getObjectUpdatableFieldsMeta(tableName: string): Result {
        let allMetaFields = this.getObjectFieldsMeta(tableName)
        if (allMetaFields.type === "success") {
            allMetaFields.data = allMetaFields.data.filter((field : any) => field.updateable === true)
        }

        return allMetaFields
    }

    getObjectFieldsMeta(tableName: string): Result {
        try {
            let result = null

            if (this.meta.hasOwnProperty(tableName)) {
                result = this.meta[tableName].fields || []
            }

            if (!_.isEmpty(result)) {
                
                return {
                    type: "success",
                    data: result
                }
            }

            return (
                {
                    type: "error",
                    data: "No results"
                }
            )
        } catch (error) {
            // Sentry.Native.captureException(new Error("Not able get meta."), {
            //     logger: 'MetaStore',
            //     context: 'getObjectFieldsMeta'
            // });
        }

        return (
            {
                type: "error",
                data: "No results"
            }
        )
    }

    /**
     * Returns Meta Data for a specific object
     * @param tableName :string
     */
    @action
    getObjectMeta(tableName: string): Result {
        try {
            let result = null

            if (this.meta.hasOwnProperty(tableName)) {

                result = this.meta[tableName]
            }

            if (!_.isEmpty(result)) {
                return {
                    type: "success",
                    data: result
                }
            }

            return (
                {
                    type: "error",
                    data: "No results"
                }
            )

        } catch (e) {
            // Sentry.Native.captureException(new Error("Not able get meta."), {
            //     logger: 'MetaStore',
            //     context: 'getObjectMeta'
            // });
        }

        return (
            {
                type: "error",
                data: "No results"
            }
        )
    }

    @action
    getMetaPrefix(): string {
        try {

            const _tableNames = Object.getOwnPropertyNames(this.meta)
            const _sum = _tableNames.map(x => {
                return _.includes(x, "FIELDBUDDY__")
            })

            return _.sum(_sum) > 0
                ? "FIELDBUDDY__"
                : ""


        } catch (e) {
            // Sentry.Native.captureException(new Error("Not able get meta prefix."), {
            //     logger: 'MetaStore',
            //     context: 'getMetaPrefix'
            // });
        }

        return ""
    }

    async getSimplifiedMetadata(withTypes = false) {
        let _s = {}
        try {

            const _tableNames = Object.getOwnPropertyNames(this.meta)
            for (let tableName of _tableNames) {
                let _fr = this.getObjectFieldsMeta(tableName)
                _s[tableName] = []
                if (_fr.type === 'success') {
                    if (withTypes) {
                        _s[tableName] = _fr.data.map(f => { return f.name + " (" + f.type + ")" })
                    } else {
                        _s[tableName] = _fr.data.map(f => { return f.name; })
                    }


                }
            }
        } catch (error) {
            //pass
        }

        return _s;
    }

    async getSimplifiedMetadataSlackFormatted(withTypes = false) {

        const smd = await this.getSimplifiedMetadata(withTypes)

        let _l = ""
        try {

            for (let t of _.keys(smd)) {
                _l += `${t}:\t `
                _l += `[${smd[t]}]\n`
            }
            _l = _l.trim()

        } catch (error) {
            //pass     
        }
        return _l
    }

}
