export const SINGLE_CONDITION = {
    "executor": "jsonQuery",
    "type": "validation",
    "alertType": "warning",
    "source": "${DATA}",
    "alertMessage": "No Activities given at current work order!",
    "formula": "Work_Order_Line_Item__c[*Work_Order__c=${CURRENT_WORK_ORDER}&Type__c=Activity]:isNotEmpty"
}

export const LOCATION_CONDITION = {
    "executor": "jsonQuery",
    "type": "filter",
    "source": "${DATA}",
    "formula": "FIELDBUDDY__Location__c[Id=a0D1U000000oad2UAA]:isNotEmpty"
}

export const INVALID_LOCATION_CONDITION = {
    "executor": "jsonQuery",
    "type": "filter",
    "source": "${DATA}",
    "formula": "FIELDBUDDY__Location__c[Id=a0D1U000000oad2UAAsAs]:isNotEmpty"
}

export const ACTIVE_DATA = 
    {
        "attributes": {
        "type": "FIELDBUDDY__Work_Order__c",
        "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j5B000002RxPqQAK"
        },
        "Id": "a0j5B000002RxPqQAK",
        "Name": "WO-001415",
        "CreatedDate": "2019-10-23T11:29:46.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T5B000001HnnwUAC",
        "FIELDBUDDY__Status__c": "CLOSED",
        "T_Start_Time__c": "2019-10-23T09:00:00.000+0000",
        "T_End_Time__c": "2019-10-23T10:00:00.000+0000",
        "FIELDBUDDY__Comments_from_Technician__c": "Well and closed",
        "FIELDBUDDY__Closure_Status__c": "SOLVED",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "Approved_by__c": "Abc",
        "UserRecordAccess": {
        "attributes": {
            "type": "UserRecordAccess"
        },
        "HasDeleteAccess": true,
        "HasEditAccess": true,
        "HasTransferAccess": true,
        "MaxAccessLevel": "All"
        }


      }

export const RELATIONSHIPS = [ {
    "ParentToChildFieldName" : "Contacts",
    "ParentObject" : "Account",
    "ChildToParentFieldName" : "AccountId",
    "ChildObject" : "Contact"
  }, {
    "ParentToChildFieldName" : "Installed_Products__r",
    "ParentObject" : "Account",
    "ChildToParentFieldName" : "Account__c",
    "ChildObject" : "Installed_Product__c"
  }, {
    "ParentToChildFieldName" : "Locations__r",
    "ParentObject" : "Account",
    "ChildToParentFieldName" : "Account__c",
    "ChildObject" : "Location__c"
  }, {
    "ParentToChildFieldName" : "Service_Agreements__r",
    "ParentObject" : "Account",
    "ChildToParentFieldName" : "Account__c",
    "ChildObject" : "Service_Agreement__c"
  }, {
    "ParentToChildFieldName" : "Service_Requests__r",
    "ParentObject" : "Account",
    "ChildToParentFieldName" : "Account__c",
    "ChildObject" : "Service_Request__c"
  }, {
    "ParentToChildFieldName" : "Users",
    "ParentObject" : "Account",
    "ChildToParentFieldName" : "AccountId",
    "ChildObject" : "User"
  }, {
    "ParentToChildFieldName" : "Installed_Products__r",
    "ParentObject" : "Location__c",
    "ChildToParentFieldName" : "Location__c",
    "ChildObject" : "Installed_Product__c"
  }, {
    "ParentToChildFieldName" : "Installed_Products1__r",
    "ParentObject" : "Location__c",
    "ChildToParentFieldName" : "Sub_location__c",
    "ChildObject" : "Installed_Product__c"
  }, {
    "ParentToChildFieldName" : "Sub_Locations__r",
    "ParentObject" : "Location__c",
    "ChildToParentFieldName" : "Location__c",
    "ChildObject" : "Location__c"
  }, {
    "ParentToChildFieldName" : "Work_Orders__r",
    "ParentObject" : "Location__c",
    "ChildToParentFieldName" : "Location__c",
    "ChildObject" : "Work_Order__c"
  }, {
    "ParentToChildFieldName" : "Installed_Products__r",
    "ParentObject" : "Product__c",
    "ChildToParentFieldName" : "Product__c",
    "ChildObject" : "Installed_Product__c"
  }, {
    "ParentToChildFieldName" : "Installed_Products__r",
    "ParentObject" : "Service_Agreement__c",
    "ChildToParentFieldName" : "Service_Agreement__c",
    "ChildObject" : "Installed_Product__c"
  }, {
    "ParentToChildFieldName" : "Service_Requests__r",
    "ParentObject" : "Contact",
    "ChildToParentFieldName" : "Contact__c",
    "ChildObject" : "Service_Request__c"
  }, {
    "ParentToChildFieldName" : "Work_Orders__r",
    "ParentObject" : "Contact",
    "ChildToParentFieldName" : "Contact__c",
    "ChildObject" : "Work_Order__c"
  }, {
    "ParentToChildFieldName" : "Service_Requests__r",
    "ParentObject" : "Installed_Product__c",
    "ChildToParentFieldName" : "Installed_Product__c",
    "ChildObject" : "Service_Request__c"
  }, {
    "ParentToChildFieldName" : "Work_Order_Line_Items__r",
    "ParentObject" : "Installed_Product__c",
    "ChildToParentFieldName" : "Installed_Product__c",
    "ChildObject" : "Work_Order_Line_Item__c"
  }, {
    "ParentToChildFieldName" : "Work_Order_Line_Item_Details__r",
    "ParentObject" : "Work_Order_Line_Item__c",
    "ChildToParentFieldName" : "Work_Order_Line_Item__c",
    "ChildObject" : "Work_Order_Line_Item_Detail__c"
  }, {
    "ParentToChildFieldName" : "Timesheet_Lineitems__r",
    "ParentObject" : "Work_Order__c",
    "ChildToParentFieldName" : "Work_Order__c",
    "ChildObject" : "Timesheet_Lineitem__c"
  }, {
    "ParentToChildFieldName" : "Work_Order_Line_Items__r",
    "ParentObject" : "Work_Order__c",
    "ChildToParentFieldName" : "Work_Order__c",
    "ChildObject" : "Work_Order_Line_Item__c"
  }, {
    "ParentToChildFieldName" : "Work_Order_Item_Details__r",
    "ParentObject" : "Work_Order_Item__c",
    "ChildToParentFieldName" : "Work_Order_Item__c",
    "ChildObject" : "Work_Order_Item_Detail__c"
  }, {
    "ParentToChildFieldName" : "Work_Order_Line_Items__r",
    "ParentObject" : "Work_Order_Item__c",
    "ChildToParentFieldName" : "Work_Order_Item__c",
    "ChildObject" : "Work_Order_Line_Item__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Account"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Contact"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "ContentVersion"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Installed_Product__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Location__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Product__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Service_Agreement__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Service_Request__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Timesheet__c"
  }, {
    "ParentToChildFieldName" : "Timesheets__r",
    "ParentObject" : "User",
    "ChildToParentFieldName" : "User__c",
    "ChildObject" : "Timesheet__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Work_Order_Item__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Work_Order_Line_Item__c"
  }, {
    "ParentToChildFieldName" : null,
    "ParentObject" : "User",
    "ChildToParentFieldName" : "OwnerId",
    "ChildObject" : "Work_Order__c"
  }, {
    "ParentToChildFieldName" : "Timesheet_Lineitems__r",
    "ParentObject" : "Timesheet__c",
    "ChildToParentFieldName" : "Timesheet__c",
    "ChildObject" : "Timesheet_Lineitem__c"
  }, {
    "ParentToChildFieldName" : "Work_Orders__r",
    "ParentObject" : "Service_Request__c",
    "ChildToParentFieldName" : "Service_Request__c",
    "ChildObject" : "Work_Order__c"
  }
]

