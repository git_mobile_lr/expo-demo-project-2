export const APP_SETTINGS = {
        "applyOnlyUserChanges": true,
        "assets": [
            {
                "assetName": "FB2Logo",
                "assetType": "StaticResource"
            },
            {
                "assetName": "confetti",
                "assetType": "StaticResource"
            },
            {
                "assetName": "DefaultPDFLogo",
                "assetType": "StaticResource"
            }
        ],
        "initAt": "Work_Order__c",
        "proxyForContent": "https://rocky-escarpment-81550.herokuapp.com",
        "rules": {
            "privileges": {
                "checkPrivileges": false,
                "showPrivilegesAlerts": true,
                "hidePrivilegesUI": false,
                "onRead": "accessible",
                "onEdit": "updateable",
                "onDelete": "deletable",
                "onCreate": "createable"
            },
            "requiredRelationships": [
                "Service_Request__c",
                "Installed_Product__c",
                "Location__c",
                "Account"
            ],
            "items": {
                "table": "Work_Order_Item__c",
                "useDisplayType": true,
                "hideStatus": false,
                "chooseDefaultStatus": {
                    "targetField": "Status__c",
                    "targetValue": {
                        "Open": {
                            "executor": "jsonQuery",
                            "type": "filter",
                            "source": "${DATA}",
                            "formula": "Work_Order__c[Id=${CURRENT_WORK_ORDER}&Type__c=INSPECTION]"
                        },
                        "Closed": {
                            "executor": "jsonQuery",
                            "type": "filter",
                            "source": "${DATA}",
                            "formula": "Work_Order__c[Id=${CURRENT_WORK_ORDER}&Type__c!=INSPECTION]"
                        }
                    }
                },
                "icons": {
                    "Activity": "wrench",
                    "Part": "cube",
                    "Other Expense": "credit-card",
                    "Delete": "trash",
                    "Add": "plus",
                    "search-plus": "search-plus"
                }
            },
            "onboarding": {
                "active": {
                    "field": "Show_Mobile_Onboarding__c",
                    "table": "User"
                }
            },
            "readonly": [
                {
                    "condition": {
                        "rule": "==",
                        "value": "CLOSED"
                    },
                    "field": "Status__c",
                    "table": "Work_Order__c"
                },
                {
                    "condition": {
                        "rule": "isIncomplete"
                    }
                }
            ],
            "actions": {
                "active": true
            },
            "renderEngine": "2.0",
            "defaultPaths": {
                "Service_Request": {
                    "Service_Request__c": "Service_Request__c",
                    "Installed_Product__c": "Service_Request__c.Installed_Product__c",
                    "Location__c": "Service_Request__c.Installed_Product__c.Location__c",
                    "Sub_location__c": "Service_Request__c.Installed_Product__c.Sub_location__c",
                    "Account": "Service_Request__c.Account__c",
                    "Installed_Product__cAccount": "Service_Request__c.Installed_Product__c.Location__c.Account__c",
                    "Service_Agreement__cAccount": "Service_Request__c.Installed_Product__c.Service_Agreement__c.Account__c",
                    "Service_Agreement__c": "Service_Request__c.Installed_Product__c.Service_Agreement__c",
                    "Product__c": "Service_Request__c.Installed_Product__c.Product__c",
                    "Contact": "Service_Request__c.Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]"
                },
                "Location": {
                    "Service_Request__c": "Service_Request__c",
                    "Location__c": "Location__c",
                    "Account": "Location__c.Account__c",
                    "Account2": "Service_Request__c.Account__c",
                    "Contact": "Service_Request__c.Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Installed_Product__c": "Work_Order_Line_Items__r[].Installed_Product__c",
                    "Product__c": "Product__c",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]",
                    "Service_Agreement__c": "Service_Agreement__c",
                    "LocationsUnderAccount": "Location__c.Account__c.Locations__r",
                    "ContactsUnderAccount": "Location__c.Account__c.Contacts",
                    "Service_Agreement__c2": "Service_Request__c.Installed_Product__c.Service_Agreement__c",
                    "SubLocations": "Work_Order_Line_Items__r.Installed_Product__c.Sub_location__c",
                    "SubLocation": "Work_Order_Line_Items__r[Id].Installed_Product__c.Sub_location__c",
                    "SubLocationsFromLocation": "Location__c.Sub_Locations__r",
                    "ContactOfCertainSubLocation": "Location__c.Sub_Locations__r[Id].Contact__c",
                    "ContactsOfAllSubLocations": "Location__c.Sub_Locations__r.Contact__c",
                    "p1": "Work_Order_Line_Items__r[Work_Order_Line_Item__c.Id]",
                    "p2": "Work_Order_Line_Items__r",
                    "a": "Assets__r.Assets__r",
                    "a2": "Assets__r.Assets__r.Assets__r",
                    "a3": "Assets__r.Assets__r.Assets__r.Assets__r",
                    "Weirdo": "Work_Order_Line_Items__r[Work_Order_Line_Item__c].Installed_Product__c.Service_Agreement__c.Service_Agreement_Lines__r.WorkOrderItem__c",
                    "IPS_FOR_LB_WO": "Work_Order_Line_Items__r.Installed_Product__c",
                    "IP_FOR_LB_WO": "Work_Order_Line_Items__r[Id].Installed_Product__c",
                    "WOLIDS": "Work_Order_Line_Items__r.Work_Order_Line_Item_Details__r",
                    "HistoricalWO": "Location__c.Work_Orders__r",
                    "HistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r",
                    "AllWoDetailsPerHistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r[Id].Work_Order_Line_Items__r."
                }
            },
            "timesheets": {
                "implicit": true,
                "manual": false,
                "table": "Timesheet__c",
                "groupBy": {
                    "table": "Timesheet_Lineitem__c",
                    "field": "Type__c",
                    "icons": {
                        "Service visit": "wrench",
                        "Travel": "car",
                        "Internal meeting": "users",
                        "Lunch": "hourglass"
                    }
                }
            },
            "pricing": {
                "showPrices":{
                    "activities":false,
                    "parts":true,
                    "otherExpenses":true
                },
                "includeVAT": true,
                "precision":2
            }
        },
        "status": {
            "filters": [
                {
                    "acceptedValues": [
                        "OPEN",
                        "ON_THE_GO",
                        "ARRIVED",
                        "IN_PROGRESS",
                        "CLOSED"
                    ],
                    "field": "Status__c"
                }
            ],
            "order": {
                "field": "Status__c",
                "values": [
                    "OPEN",
                    "ON_THE_GO",
                    "ARRIVED",
                    "IN_PROGRESS",
                    "CLOSED"
                ]
            }
        },
        "workOrderCreation": {
            "isEnabled": true,
            "header": {
                "title": "${Label.CreateWorkOrder}"
            },
            "event": {
                "privileges": true,
                "eventType": "createable",
                "targets": [
                    "Work_Order__c"
                ]
            },
            "params": [
                {
                    "field": "Id",
                    "returnProperty": "$selectedAccount"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedInstalledProduct"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedLocation"
                }
            ],
            "steps": [
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToFilter": [
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "label": "${Label.SelectAccount}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedAccount",
                    "selectedDisplay": "${Label.SelectedAccount}",
                    "table": "Account",
                    "tableNameDisplay": "${Label.SearchAccount}"
                },
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "Street__c",
                        "House_Number__c",
                        "House_Number_Suffix__c",
                        "Postal_Code__c",
                        "City__c"
                    ],
                    "fieldsToFilter": [
                        "Account__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "Street__c",
                        "House_Number__c",
                        "House_Number_Suffix__c",
                        "Postal_Code__c",
                        "City__c"
                    ],
                    "label": "${Label.SelectLocation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedLocation",
                    "selectedDisplay": "${Label.SelectedLocation}",
                    "table": "Location__c",
                    "tableNameDisplay": "${Label.SearchLocation}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedAccount"
                    }
                },
                {
                    "allowNewRecord": true,
                    "supportRecordTypes": false,
                    "creationFields": [
                        {
                            "recordType": "Default",
                            "fields": [
                                "Name",
                                "Construction_Year__c",
                                "Status__c",
                                "Date_Installed__c"
                            ]
                        }
                    ],
                    "fieldsToDisplay": [
                        "Name",
                        "Installed_Product_Number__c",
                        "Construction_Year__c"
                    ],
                    "fieldsToFilter": [
                        "Location__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "Construction_Year__c",
                        "Installed_Product_Number__c"
                    ],
                    "label": "${Label.SelectInstallation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedInstalledProduct",
                    "selectedDisplay": "${Label.SelectedInstallation}",
                    "table": "Installed_Product__c",
                    "tableNameDisplay": "${Label.SearchInstallation}",
                    "newTableNameDisplay": "${Label.AddInstalledProduct}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedLocation"
                    }
                }
            ]
        }
    }



    export const APP_SETTINGS_2 = {
        "applyOnlyUserChanges": true,
        "assets": [
            {
                "assetName": "FB2Logo",
                "assetType": "StaticResource"
            },
            {
                "assetName": "confetti",
                "assetType": "StaticResource"
            },
            {
                "assetName": "DefaultPDFLogo",
                "assetType": "StaticResource"
            }
        ],
        "initAt": "Work_Order__c",
        "proxyForContent": "https://rocky-escarpment-81550.herokuapp.com",
        "rules": {
            "privileges": {
                "checkPrivileges": false,
                "showPrivilegesAlerts": true,
                "hidePrivilegesUI": false,
                "onRead": "accessible",
                "onEdit": "updateable",
                "onDelete": "deletable",
                "onCreate": "createable"
            },
            "requiredRelationships": [
                "Service_Request__c",
                "Installed_Product__c",
                "Location__c",
                "Account"
            ],
            "items": {
                "table": "Work_Order_Item__c",
                "useDisplayType": true,
                "hideStatus": false,
                "chooseDefaultStatus": {
                    "targetField": "Status__c",
                    "targetValue": {
                        "Open": {
                            "executor": "jsonQuery",
                            "type": "filter",
                            "source": "${DATA}",
                            "formula": "Work_Order__c[Id=${CURRENT_WORK_ORDER}&Type__c=INSPECTION]"
                        },
                        "Closed": {
                            "executor": "jsonQuery",
                            "type": "filter",
                            "source": "${DATA}",
                            "formula": "Work_Order__c[Id=${CURRENT_WORK_ORDER}&Type__c!=INSPECTION]"
                        }
                    }
                },
                "icons": {
                    "Activity": "wrench",
                    "Part": "cube",
                    "Other Expense": "credit-card",
                    "Delete": "trash",
                    "Add": "plus",
                    "search-plus": "search-plus"
                }
            },
            "onboarding": {
                "active": {
                    "field": "Show_Mobile_Onboarding__c",
                    "table": "User"
                }
            },
            "readonly": [
                {
                    "condition": {
                        "rule": "==",
                        "value": "CLOSED"
                    },
                    "field": "Status__c",
                    "table": "Work_Order__c"
                },
                {
                    "condition": {
                        "rule": "isIncomplete"
                    }
                }
            ],
            "actions": {
                "active": true
            },
            "renderEngine": "2.0",
            "defaultPaths": {
                "Service_Request": {
                    "Service_Request__c": "Service_Request__c",
                    "Installed_Product__c": "Service_Request__c.Installed_Product__c",
                    "Location__c": "Service_Request__c.Installed_Product__c.Location__c",
                    "Sub_location__c": "Service_Request__c.Installed_Product__c.Sub_location__c",
                    "Account": "Service_Request__c.Account__c",
                    "Installed_Product__cAccount": "Service_Request__c.Installed_Product__c.Location__c.Account__c",
                    "Service_Agreement__cAccount": "Service_Request__c.Installed_Product__c.Service_Agreement__c.Account__c",
                    "Service_Agreement__c": "Service_Request__c.Installed_Product__c.Service_Agreement__c",
                    "Product__c": "Service_Request__c.Installed_Product__c.Product__c",
                    "Contact": "Service_Request__c.Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]"
                },
                "Location": {
                    "Service_Request__c": "Service_Request__c",
                    "Location__c": "Location__c",
                    "Account": "Location__c.Account__c",
                    "Account2": "Service_Request__c.Account__c",
                    "Contact": "Service_Request__c.Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Installed_Product__c": "Work_Order_Line_Items__r[].Installed_Product__c",
                    "Product__c": "Product__c",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]",
                    "Service_Agreement__c": "Service_Agreement__c",
                    "LocationsUnderAccount": "Location__c.Account__c.Locations__r",
                    "ContactsUnderAccount": "Location__c.Account__c.Contacts",
                    "Service_Agreement__c2": "Service_Request__c.Installed_Product__c.Service_Agreement__c",
                    "SubLocations": "Work_Order_Line_Items__r.Installed_Product__c.Sub_location__c",
                    "SubLocation": "Work_Order_Line_Items__r[Id].Installed_Product__c.Sub_location__c",
                    "SubLocationsFromLocation": "Location__c.Sub_Locations__r",
                    "ContactOfCertainSubLocation": "Location__c.Sub_Locations__r[Id].Contact__c",
                    "ContactsOfAllSubLocations": "Location__c.Sub_Locations__r.Contact__c",
                    "p1": "Work_Order_Line_Items__r[Work_Order_Line_Item__c.Id]",
                    "p2": "Work_Order_Line_Items__r",
                    "a": "Assets__r.Assets__r",
                    "a2": "Assets__r.Assets__r.Assets__r",
                    "a3": "Assets__r.Assets__r.Assets__r.Assets__r",
                    "Weirdo": "Work_Order_Line_Items__r[Work_Order_Line_Item__c].Installed_Product__c.Service_Agreement__c.Service_Agreement_Lines__r.WorkOrderItem__c",
                    "IPS_FOR_LB_WO": "Work_Order_Line_Items__r.Installed_Product__c",
                    "IP_FOR_LB_WO": "Work_Order_Line_Items__r[Id].Installed_Product__c",
                    "WOLIDS": "Work_Order_Line_Items__r.Work_Order_Line_Item_Details__r",
                    "HistoricalWO": "Location__c.Work_Orders__r",
                    "HistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r",
                    "AllWoDetailsPerHistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r[Id].Work_Order_Line_Items__r."
                }
            },
            "timesheets": {
                "implicit": true,
                "manual": false,
                "table": "Timesheet__c",
                "groupBy": {
                    "table": "Timesheet_Lineitem__c",
                    "field": "Type__c",
                    "icons": {
                        "Service visit": "wrench",
                        "Travel": "car",
                        "Internal meeting": "users",
                        "Lunch": "hourglass"
                    }
                }
            },
            "pricing": {
                "showPrices":{
                    "activities":true,
                    "parts":false,
                    "otherExpenses":false
                },
                "includeVAT": true,
                "precision":2
            }
        },
        "status": {
            "filters": [
                {
                    "acceptedValues": [
                        "OPEN",
                        "ON_THE_GO",
                        "ARRIVED",
                        "IN_PROGRESS",
                        "CLOSED"
                    ],
                    "field": "Status__c"
                }
            ],
            "order": {
                "field": "Status__c",
                "values": [
                    "OPEN",
                    "ON_THE_GO",
                    "ARRIVED",
                    "IN_PROGRESS",
                    "CLOSED"
                ]
            }
        },
        "workOrderCreation": {
            "isEnabled": true,
            "header": {
                "title": "${Label.CreateWorkOrder}"
            },
            "event": {
                "privileges": true,
                "eventType": "createable",
                "targets": [
                    "Work_Order__c"
                ]
            },
            "params": [
                {
                    "field": "Id",
                    "returnProperty": "$selectedAccount"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedInstalledProduct"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedLocation"
                }
            ],
            "steps": [
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToFilter": [
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "label": "${Label.SelectAccount}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedAccount",
                    "selectedDisplay": "${Label.SelectedAccount}",
                    "table": "Account",
                    "tableNameDisplay": "${Label.SearchAccount}"
                },
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "Street__c",
                        "House_Number__c",
                        "House_Number_Suffix__c",
                        "Postal_Code__c",
                        "City__c"
                    ],
                    "fieldsToFilter": [
                        "Account__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "Street__c",
                        "House_Number__c",
                        "House_Number_Suffix__c",
                        "Postal_Code__c",
                        "City__c"
                    ],
                    "label": "${Label.SelectLocation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedLocation",
                    "selectedDisplay": "${Label.SelectedLocation}",
                    "table": "Location__c",
                    "tableNameDisplay": "${Label.SearchLocation}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedAccount"
                    }
                },
                {
                    "allowNewRecord": true,
                    "supportRecordTypes": false,
                    "creationFields": [
                        {
                            "recordType": "Default",
                            "fields": [
                                "Name",
                                "Construction_Year__c",
                                "Status__c",
                                "Date_Installed__c"
                            ]
                        }
                    ],
                    "fieldsToDisplay": [
                        "Name",
                        "Installed_Product_Number__c",
                        "Construction_Year__c"
                    ],
                    "fieldsToFilter": [
                        "Location__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "Construction_Year__c",
                        "Installed_Product_Number__c"
                    ],
                    "label": "${Label.SelectInstallation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedInstalledProduct",
                    "selectedDisplay": "${Label.SelectedInstallation}",
                    "table": "Installed_Product__c",
                    "tableNameDisplay": "${Label.SearchInstallation}",
                    "newTableNameDisplay": "${Label.AddInstalledProduct}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedLocation"
                    }
                }
            ]
        }
    }

    export const APP_SETTINGS_3 = {
        "applyOnlyUserChanges": true,
        "assets": [
            {
                "assetName": "FB2Logo",
                "assetType": "StaticResource"
            },
            {
                "assetName": "confetti",
                "assetType": "StaticResource"
            },
            {
                "assetName": "DefaultPDFLogo",
                "assetType": "StaticResource"
            }
        ],
        "initAt": "Work_Order__c",
        "proxyForContent": "https://rocky-escarpment-81550.herokuapp.com",
        "rules": {
            "privileges": {
                "checkPrivileges": false,
                "showPrivilegesAlerts": true,
                "hidePrivilegesUI": false,
                "onRead": "accessible",
                "onEdit": "updateable",
                "onDelete": "deletable",
                "onCreate": "createable"
            },
            "requiredRelationships": [
                "Service_Request__c",
                "Installed_Product__c",
                "Location__c",
                "Account"
            ],
            "items": {
                "table": "Work_Order_Item__c",
                "useDisplayType": true,
                "hideStatus": false,
                "chooseDefaultStatus": {
                    "targetField": "Status__c",
                    "targetValue": {
                        "Open": {
                            "executor": "jsonQuery",
                            "type": "filter",
                            "source": "${DATA}",
                            "formula": "Work_Order__c[Id=${CURRENT_WORK_ORDER}&Type__c=INSPECTION]"
                        },
                        "Closed": {
                            "executor": "jsonQuery",
                            "type": "filter",
                            "source": "${DATA}",
                            "formula": "Work_Order__c[Id=${CURRENT_WORK_ORDER}&Type__c!=INSPECTION]"
                        }
                    }
                },
                "icons": {
                    "Activity": "wrench",
                    "Part": "cube",
                    "Other Expense": "credit-card",
                    "Delete": "trash",
                    "Add": "plus",
                    "search-plus": "search-plus"
                }
            },
            "onboarding": {
                "active": {
                    "field": "Show_Mobile_Onboarding__c",
                    "table": "User"
                }
            },
            "readonly": [
                {
                    "condition": {
                        "rule": "==",
                        "value": "CLOSED"
                    },
                    "field": "Status__c",
                    "table": "Work_Order__c"
                },
                {
                    "condition": {
                        "rule": "isIncomplete"
                    }
                }
            ],
            "actions": {
                "active": true
            },
            "renderEngine": "2.0",
            "defaultPaths": {
                "Service_Request": {
                    "Service_Request__c": "Service_Request__c",
                    "Installed_Product__c": "Service_Request__c.Installed_Product__c",
                    "Location__c": "Service_Request__c.Installed_Product__c.Location__c",
                    "Sub_location__c": "Service_Request__c.Installed_Product__c.Sub_location__c",
                    "Account": "Service_Request__c.Account__c",
                    "Installed_Product__cAccount": "Service_Request__c.Installed_Product__c.Location__c.Account__c",
                    "Service_Agreement__cAccount": "Service_Request__c.Installed_Product__c.Service_Agreement__c.Account__c",
                    "Service_Agreement__c": "Service_Request__c.Installed_Product__c.Service_Agreement__c",
                    "Product__c": "Service_Request__c.Installed_Product__c.Product__c",
                    "Contact": "Service_Request__c.Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]"
                },
                "Location": {
                    "Service_Request__c": "Service_Request__c",
                    "Location__c": "Location__c",
                    "Account": "Location__c.Account__c",
                    "Account2": "Service_Request__c.Account__c",
                    "Contact": "Service_Request__c.Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Installed_Product__c": "Work_Order_Line_Items__r[].Installed_Product__c",
                    "Product__c": "Product__c",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]",
                    "Service_Agreement__c": "Service_Agreement__c",
                    "LocationsUnderAccount": "Location__c.Account__c.Locations__r",
                    "ContactsUnderAccount": "Location__c.Account__c.Contacts",
                    "Service_Agreement__c2": "Service_Request__c.Installed_Product__c.Service_Agreement__c",
                    "SubLocations": "Work_Order_Line_Items__r.Installed_Product__c.Sub_location__c",
                    "SubLocation": "Work_Order_Line_Items__r[Id].Installed_Product__c.Sub_location__c",
                    "SubLocationsFromLocation": "Location__c.Sub_Locations__r",
                    "ContactOfCertainSubLocation": "Location__c.Sub_Locations__r[Id].Contact__c",
                    "ContactsOfAllSubLocations": "Location__c.Sub_Locations__r.Contact__c",
                    "p1": "Work_Order_Line_Items__r[Work_Order_Line_Item__c.Id]",
                    "p2": "Work_Order_Line_Items__r",
                    "a": "Assets__r.Assets__r",
                    "a2": "Assets__r.Assets__r.Assets__r",
                    "a3": "Assets__r.Assets__r.Assets__r.Assets__r",
                    "Weirdo": "Work_Order_Line_Items__r[Work_Order_Line_Item__c].Installed_Product__c.Service_Agreement__c.Service_Agreement_Lines__r.WorkOrderItem__c",
                    "IPS_FOR_LB_WO": "Work_Order_Line_Items__r.Installed_Product__c",
                    "IP_FOR_LB_WO": "Work_Order_Line_Items__r[Id].Installed_Product__c",
                    "WOLIDS": "Work_Order_Line_Items__r.Work_Order_Line_Item_Details__r",
                    "HistoricalWO": "Location__c.Work_Orders__r",
                    "HistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r",
                    "AllWoDetailsPerHistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r[Id].Work_Order_Line_Items__r."
                }
            },
            "timesheets": {
                "implicit": true,
                "manual": false,
                "table": "Timesheet__c",
                "groupBy": {
                    "table": "Timesheet_Lineitem__c",
                    "field": "Type__c",
                    "icons": {
                        "Service visit": "wrench",
                        "Travel": "car",
                        "Internal meeting": "users",
                        "Lunch": "hourglass"
                    }
                }
            },
            "pricing": {
                "includeVAT": true,
                "precision":2
            }
        },
        "status": {
            "filters": [
                {
                    "acceptedValues": [
                        "OPEN",
                        "ON_THE_GO",
                        "ARRIVED",
                        "IN_PROGRESS",
                        "CLOSED"
                    ],
                    "field": "Status__c"
                }
            ],
            "order": {
                "field": "Status__c",
                "values": [
                    "OPEN",
                    "ON_THE_GO",
                    "ARRIVED",
                    "IN_PROGRESS",
                    "CLOSED"
                ]
            }
        },
        "workOrderCreation": {
            "isEnabled": true,
            "header": {
                "title": "${Label.CreateWorkOrder}"
            },
            "event": {
                "privileges": true,
                "eventType": "createable",
                "targets": [
                    "Work_Order__c"
                ]
            },
            "params": [
                {
                    "field": "Id",
                    "returnProperty": "$selectedAccount"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedInstalledProduct"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedLocation"
                }
            ],
            "steps": [
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToFilter": [
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "label": "${Label.SelectAccount}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedAccount",
                    "selectedDisplay": "${Label.SelectedAccount}",
                    "table": "Account",
                    "tableNameDisplay": "${Label.SearchAccount}"
                },
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "Street__c",
                        "House_Number__c",
                        "House_Number_Suffix__c",
                        "Postal_Code__c",
                        "City__c"
                    ],
                    "fieldsToFilter": [
                        "Account__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "Street__c",
                        "House_Number__c",
                        "House_Number_Suffix__c",
                        "Postal_Code__c",
                        "City__c"
                    ],
                    "label": "${Label.SelectLocation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedLocation",
                    "selectedDisplay": "${Label.SelectedLocation}",
                    "table": "Location__c",
                    "tableNameDisplay": "${Label.SearchLocation}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedAccount"
                    }
                },
                {
                    "allowNewRecord": true,
                    "supportRecordTypes": false,
                    "creationFields": [
                        {
                            "recordType": "Default",
                            "fields": [
                                "Name",
                                "Construction_Year__c",
                                "Status__c",
                                "Date_Installed__c"
                            ]
                        }
                    ],
                    "fieldsToDisplay": [
                        "Name",
                        "Installed_Product_Number__c",
                        "Construction_Year__c"
                    ],
                    "fieldsToFilter": [
                        "Location__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "Construction_Year__c",
                        "Installed_Product_Number__c"
                    ],
                    "label": "${Label.SelectInstallation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedInstalledProduct",
                    "selectedDisplay": "${Label.SelectedInstallation}",
                    "table": "Installed_Product__c",
                    "tableNameDisplay": "${Label.SearchInstallation}",
                    "newTableNameDisplay": "${Label.AddInstalledProduct}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedLocation"
                    }
                }
            ]
        }
    }