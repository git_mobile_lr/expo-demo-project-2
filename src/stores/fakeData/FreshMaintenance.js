export const FRESH_MAINTENANCE_DATA = {
    "Account": [
      {
        "attributes": {
          "type": "Account",
          "url": "/services/data/v43.0/sobjects/Account/0011U000003jjLhQAI"
        },
        "Id": "0011U000003jjLhQAI",
        "Name": "DLR Adviesgroep",
        "CreatedDate": "2018-11-28T13:05:42.000+0000",
        "Phone": "69956985",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "Account",
          "url": "/services/data/v43.0/sobjects/Account/0011U000003jjNdQAI"
        },
        "Id": "0011U000003jjNdQAI",
        "Name": "Skylift BV",
        "CreatedDate": "2018-11-28T13:51:00.000+0000",
        "Phone": "088 - 505 15 00",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "FIELDBUDDY__Location__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Location__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Location__c/a0D1U000000oad2UAA"
        },
        "Id": "a0D1U000000oad2UAA",
        "Name": "LOC-000275",
        "CreatedDate": "2019-01-15T09:16:30.000+0000",
        "FIELDBUDDY__Account__c": "0011U000007d2nQQAQ",
        "FIELDBUDDY__Street__c": "Kraanspoor",
        "FIELDBUDDY__House_Number__c": 42,
        "FIELDBUDDY__Postal_Code__c": "1033SE",
        "FIELDBUDDY__City__c": "Amsterdam",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Location__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Location__c/a0D1U000000ohalUAA"
        },
        "Id": "a0D1U000000ohalUAA",
        "Name": "LOC-000277",
        "CreatedDate": "2019-01-18T17:42:29.000+0000",
        "FIELDBUDDY__Account__c": "0011U000003jjOMQAY",
        "FIELDBUDDY__Street__c": "Paranadreef",
        "FIELDBUDDY__House_Number__c": 131,
        "FIELDBUDDY__Postal_Code__c": "3563 WG",
        "FIELDBUDDY__House_Number_Suffix__c": "Aaaasss",
        "FIELDBUDDY__City__c": "Utrecht",
        "Extra_informatie__c": "Paranadreef 131-253",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Location__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Location__c/a0D1U000000pRwBUAU"
        },
        "Id": "a0D1U000000pRwBUAU",
        "Name": "LOC-000288",
        "CreatedDate": "2019-02-06T11:17:33.000+0000",
        "FIELDBUDDY__Account__c": "0011U000003jjLhQAI",
        "FIELDBUDDY__Street__c": "Coolsingel 139",
        "FIELDBUDDY__House_Number__c": 139,
        "FIELDBUDDY__Postal_Code__c": "3012 AG",
        "FIELDBUDDY__City__c": "Rotterdam",
        "Extra_informatie__c": "Toegang via trappenhuis 12de etage, daarna dakluik naar installatie.",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "FIELDBUDDY__Installed_Product__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Installed_Product__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Installed_Product__c/a091U0000004DZpQAM"
        },
        "Id": "a091U0000004DZpQAM",
        "Name": "Marathon Hilversum ladder borga",
        "CreatedDate": "2019-02-06T12:02:25.000+0000",
        "FIELDBUDDY__Location__c": "a0D1U000000pS30UAE",
        "FIELDBUDDY__Account__c": "0011U000003jjLhQAI",
        "FIELDBUDDY__Construction_Year__c": 2003,
        "FIELDBUDDY__Status__c": "In bedrijf",
        "Installatie_nummer__c": "FM# 201902-128",
        "Installatie_referentie_account__c": "gvl mth02 01",
        "Type_Installatie__c": "Gevellift",
        "installatie_type_2__c": "Ladder",
        "Fabrikant__c": "Borga Bijstede",
        "Installatie_nummer_fabrikant__c": "98239-01",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Installed_Product__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Installed_Product__c/a091U0000004DZzQAM"
        },
        "Id": "a091U0000004DZzQAM",
        "Name": "Dakwagen 4e verdieping Max Euwelaan Rotterdam",
        "CreatedDate": "2019-02-06T12:04:44.000+0000",
        "FIELDBUDDY__Location__c": "a0D1U000000pS35UAE",
        "FIELDBUDDY__Account__c": "0011U000003jjLhQAI",
        "FIELDBUDDY__Construction_Year__c": 1990,
        "FIELDBUDDY__Status__c": "In bedrijf",
        "Installatie_nummer__c": "FM# 201902-129",
        "Installatie_referentie_account__c": "gvl mxe60 01",
        "Type_Installatie__c": "Gevellift",
        "installatie_type_2__c": "Dakwagen",
        "Fabrikant__c": "Mannesmann",
        "Installatie_nummer_fabrikant__c": "6.6602",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Installed_Product__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Installed_Product__c/a091U0000004DaJQAU"
        },
        "Id": "a091U0000004DaJQAU",
        "Name": "Dakwagen De Bavinck Amstelveen",
        "CreatedDate": "2019-02-06T12:11:58.000+0000",
        "FIELDBUDDY__Location__c": "a0D1U000000pS3KUAU",
        "FIELDBUDDY__Account__c": "0011U000003jjLhQAI",
        "FIELDBUDDY__Construction_Year__c": 2001,
        "FIELDBUDDY__Status__c": "In bedrijf",
        "Installatie_nummer__c": "FM# 201902-133",
        "Installatie_referentie_account__c": "gvl pfsbl07 01",
        "Type_Installatie__c": "Gevellift",
        "installatie_type_2__c": "Dakwagen + ZHG",
        "Fabrikant__c": "Kranenburg",
        "Installatie_nummer_fabrikant__c": "Rivo K 98-3-3030",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "Contact": [
      {
        "attributes": {
          "type": "Contact",
          "url": "/services/data/v43.0/sobjects/Contact/0031U000002k1syQAA"
        },
        "Id": "0031U000002k1syQAA",
        "FirstName": "Jeffrey",
        "LastName": "Westerhout",
        "Name": "Jeffrey Westerhout",
        "CreatedDate": "2018-11-28T13:10:56.000+0000",
        "MobilePhone": "06111144",
        "Phone": "0155666",
        "Email": "jwesterhout@dlr.nl",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "Contact",
          "url": "/services/data/v43.0/sobjects/Contact/0031U000005E29PQAS"
        },
        "Id": "0031U000005E29PQAS",
        "FirstName": "Fermina",
        "LastName": "Heeren",
        "Name": "Fermina Heeren",
        "CreatedDate": "2019-01-10T07:38:32.000+0000",
        "Phone": "015 256 6666",
        "Email": "fheeren@dlr.nl",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "FIELDBUDDY__Service_Request__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Service_Request__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Service_Request__c/a0T1U0000003NurUAE"
        },
        "Id": "a0T1U0000003NurUAE",
        "Name": "SR-000000997",
        "CreatedDate": "2019-01-09T16:26:46.000+0000",
        "FIELDBUDDY__Subject__c": "Hoofdschakelaar checken",
        "FIELDBUDDY__Description__c": "Glasbewassing is geweest en heeft geconstateerd dat van de bak aan de kant\r\nvan de buszijde de hoofschakelaar verweerd is. Deze durft men niet om te\r\nschakelen, omdat ze bang zijn dat hij dan helemaal defect gaat.\r\nAan de achterzijde is nog een bak. Deze staat qua bediening in de\r\nautomatische functie en dat is volgens hen niet de bedoeling.graag nazien en\r\nverhelpen bvd.",
        "FIELDBUDDY__Contact__c": "0031U000002k1syQAA",
        "FIELDBUDDY__Origin__c": "Phone",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Service_Request__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Service_Request__c/a0T1U000001AIjZUAW"
        },
        "Id": "a0T1U000001AIjZUAW",
        "Name": "SR-000001191",
        "CreatedDate": "2019-03-28T07:25:22.000+0000",
        "FIELDBUDDY__Installed_Product__c": "a091U000001fUysQAE",
        "FIELDBUDDY__Subject__c": "Preventief onderhoud en inventarisatie",
        "FIELDBUDDY__Contact__c": "0031U000005E29PQAS",
        "FIELDBUDDY__Origin__c": "Phone",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Service_Request__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Service_Request__c/a0T1U000001AqIZUA0"
        },
        "Id": "a0T1U000001AqIZUA0",
        "Name": "SR-000001279",
        "CreatedDate": "2019-06-06T09:21:03.000+0000",
        "FIELDBUDDY__Installed_Product__c": "a091U00000055WHQAY",
        "FIELDBUDDY__Subject__c": "Preventief onderhoud ( te inventariseren)",
        "FIELDBUDDY__Contact__c": "0031U00000B60vhQAB",
        "FIELDBUDDY__Origin__c": "Phone",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Service_Request__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Service_Request__c/a0T5B0000018mTcUAI"
        },
        "Id": "a0T5B0000018mTcUAI",
        "Name": "SR-000001286",
        "CreatedDate": "2019-08-08T14:13:03.000+0000",
        "FIELDBUDDY__Installed_Product__c": "a091U000004s9OzQAI",
        "FIELDBUDDY__Origin__c": "Phone",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "FIELDBUDDY__Work_Order_Logged_Activity_Detail__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity_Detail__c/a0c5B000003y4GGQAY"
        },
        "Id": "a0c5B000003y4GGQAY",
        "Name": "Aandrijving handmatige lier",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Work_Order_Logged_Activity__c": "a0d5B000002XDpUQAW",
        "FIELDBUDDY__Order__c": 81,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity_Detail__c/a0c5B000003y4GHQAY"
        },
        "Id": "a0c5B000003y4GHQAY",
        "Name": "Electrische aandrijving",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Work_Order_Logged_Activity__c": "a0d5B000002XDpUQAW",
        "FIELDBUDDY__Order__c": 82,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity_Detail__c/a0c5B000003y4GIQAY"
        },
        "Id": "a0c5B000003y4GIQAY",
        "Name": "Automatische rem",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Work_Order_Logged_Activity__c": "a0d5B000002XDpUQAW",
        "FIELDBUDDY__Order__c": 83,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity_Detail__c/a0c5B000003y4GJQAY"
        },
        "Id": "a0c5B000003y4GJQAY",
        "Name": "Veiligheidsschakelaars",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Work_Order_Logged_Activity__c": "a0d5B000002XDpUQAW",
        "FIELDBUDDY__Order__c": 84,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },

    ],
    "FIELDBUDDY__Work_Order_Logged_Activity__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity__c/a0d5B000002XDpSQAW"
        },
        "Id": "a0d5B000002XDpSQAW",
        "Name": "fudge fudger activity",
        "CreatedDate": "2019-10-31T07:47:56.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002SfLdQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tgwnQAA",
        "FIELDBUDDY__Item_Code__c": "1617/0",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity__c/a0d5B000002XDpTQAW"
        },
        "Id": "a0d5B000002XDpTQAW",
        "Name": "Gepland correctief onderhoud",
        "CreatedDate": "2019-10-31T07:47:56.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002SfLdQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvBQAS",
        "FIELDBUDDY__Item_Code__c": "FM2",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 12,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity__c/a0d5B000002XDpUQAW"
        },
        "Id": "a0d5B000002XDpUQAW",
        "Name": "Conditiemeting",
        "CreatedDate": "2019-10-31T07:47:56.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002SfLdQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "FIELDBUDDY__Item_Code__c": "FM2767",
        "FIELDBUDDY__Quantity__c": 1,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity__c/a0d5B000002XBiLQAW"
        },
        "Id": "a0d5B000002XBiLQAW",
        "Name": "fudge fudger activity",
        "CreatedDate": "2019-10-28T09:46:44.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002OIFMQA4",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tgwnQAA",
        "FIELDBUDDY__Item_Code__c": "1617/0",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Logged_Activity__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Logged_Activity__c/a0d5B000002XBiMQAW"
        },
        "Id": "a0d5B000002XBiMQAW",
        "Name": "Gepland correctief onderhoud",
        "CreatedDate": "2019-10-28T09:46:44.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002OIFMQA4",
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvBQAS",
        "FIELDBUDDY__Item_Code__c": "FM2",
        "FIELDBUDDY__Quantity__c": 1,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      }
    ],
    "FIELDBUDDY__Work_Order_Part_Detail__c": [],
    "FIELDBUDDY__Work_Order_Part__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t4wSQAQ"
        },
        "Id": "a0h5B000004t4wSQAQ",
        "Name": "falling hammer",
        "CreatedDate": "2019-10-28T09:46:45.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002OIFMQA4",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tcV0QAI",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t4wTQAQ"
        },
        "Id": "a0h5B000004t4wTQAQ",
        "Name": "fudge fudger thing",
        "CreatedDate": "2019-10-28T09:46:45.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002OIFMQA4",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tgwiQAA",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t3VkQAI"
        },
        "Id": "a0h5B000004t3VkQAI",
        "Name": "fudge fudger thing",
        "CreatedDate": "2019-10-21T13:08:36.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002RmQtQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tgwiQAA",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t3VlQAI"
        },
        "Id": "a0h5B000004t3VlQAI",
        "Name": "falling hammer",
        "CreatedDate": "2019-10-21T13:08:36.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002RmQtQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tcV0QAI",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t3VfQAI"
        },
        "Id": "a0h5B000004t3VfQAI",
        "Name": "falling hammer",
        "CreatedDate": "2019-10-21T12:51:22.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002RmQtQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tcV0QAI",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t3VgQAI"
        },
        "Id": "a0h5B000004t3VgQAI",
        "Name": "fudge fudger thing",
        "CreatedDate": "2019-10-21T12:51:22.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002RmQtQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tgwiQAA",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t5i8QAA"
        },
        "Id": "a0h5B000004t5i8QAA",
        "Name": "fudge fudger thing",
        "CreatedDate": "2019-11-01T09:06:25.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002Sl6oQAC",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tgwiQAA",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Part__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Part__c/a0h5B000004t5i9QAA"
        },
        "Id": "a0h5B000004t5i9QAA",
        "Name": "falling hammer",
        "CreatedDate": "2019-11-01T09:06:25.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002Sl6oQAC",
        "FIELDBUDDY__Work_Order_Item__c": "a0b5B000002tcV0QAI",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 121,
        "FIELDBUDDY__Tax_Rate__c": 21,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      }
    ],
    "FIELDBUDDY__Work_Order_Other_Expense_Detail__c": [],
    "FIELDBUDDY__Work_Order_Other_Expense__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Other_Expense__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Other_Expense__c/a0f5B000006Qkv6QAC"
        },
        "Id": "a0f5B000006Qkv6QAC",
        "Name": "Parkeerkosten",
        "CreatedDate": "2019-10-28T09:46:45.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002OIFMQA4",
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvPQAS",
        "FIELDBUDDY__Item_Code__c": "FM5",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 25,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Other_Expense__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Other_Expense__c/a0f5B000006Qk25QAC"
        },
        "Id": "a0f5B000006Qk25QAC",
        "Name": "Parkeerkosten",
        "CreatedDate": "2019-10-21T12:51:22.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002RmQtQAK",
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvPQAS",
        "FIELDBUDDY__Item_Code__c": "FM5",
        "FIELDBUDDY__Quantity__c": 1,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Other_Expense__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Other_Expense__c/a0f5B000006QlCTQA0"
        },
        "Id": "a0f5B000006QlCTQA0",
        "Name": "Parkeerkosten",
        "CreatedDate": "2019-11-01T09:06:26.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002Sl6oQAC",
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvPQAS",
        "FIELDBUDDY__Item_Code__c": "FM5",
        "FIELDBUDDY__Quantity__c": 1,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Other_Expense__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Other_Expense__c/a0f5B000006QlCYQA0"
        },
        "Id": "a0f5B000006QlCYQA0",
        "Name": "Parkeerkosten",
        "CreatedDate": "2019-11-01T10:27:06.000+0000",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002SlZHQA0",
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvPQAS",
        "FIELDBUDDY__Item_Code__c": "FM5",
        "FIELDBUDDY__Quantity__c": 1,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      }
    ],
    "FIELDBUDDY__Timesheet__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet__c/a0W5B000001OtVCUA0"
        },
        "CreatedDate": "2019-10-17T08:25:17.000+0000",
        "Id": "a0W5B000001OtVCUA0",
        "Name": "Swift 2019-10-17",
        "FIELDBUDDY__Date__c": "2019-10-17",
        "FIELDBUDDY__User__c": "0051U000000G6dNQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet__c/a0W5B000001OtpHUAS"
        },
        "CreatedDate": "2019-10-21T12:48:17.000+0000",
        "Id": "a0W5B000001OtpHUAS",
        "Name": "Swift 2019-10-21",
        "FIELDBUDDY__Date__c": "2019-10-21",
        "FIELDBUDDY__User__c": "0051U000000G6dNQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet__c/a0W5B000001OuRuUAK"
        },
        "CreatedDate": "2019-10-23T10:21:40.000+0000",
        "Id": "a0W5B000001OuRuUAK",
        "Name": "Swift 2019-10-23",
        "FIELDBUDDY__Date__c": "2019-10-23",
        "FIELDBUDDY__User__c": "0051U000000G6dNQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet__c/a0W5B000001OucyUAC"
        },
        "CreatedDate": "2019-10-24T08:47:49.000+0000",
        "Id": "a0W5B000001OucyUAC",
        "Name": "Swift 2019-10-24",
        "FIELDBUDDY__Date__c": "2019-10-24",
        "FIELDBUDDY__User__c": "0051U000000G6dNQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "FIELDBUDDY__Timesheet_Lineitem__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet_Lineitem__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet_Lineitem__c/a0V5B000008q4qEUAQ"
        },
        "Id": "a0V5B000008q4qEUAQ",
        "Name": "TSLI-000450",
        "FIELDBUDDY__Comments__c": "Actief -> Gesloten",
        "FIELDBUDDY__Start_Time__c": "2019-10-31T07:46:22.000+0000",
        "FIELDBUDDY__Type__c": "Servicebezoek",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002SfLdQAK",
        "FIELDBUDDY__Total_Time_Minutes__c": 1,
        "FIELDBUDDY__End_Time__c": "2019-10-31T07:47:07.000+0000",
        "FIELDBUDDY__Timesheet__c": "a0W5B000001OvPGUA0",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet_Lineitem__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet_Lineitem__c/a0V5B000008q4cgUAA"
        },
        "Id": "a0V5B000008q4cgUAA",
        "Name": "TSLI-000441",
        "FIELDBUDDY__Comments__c": "Actief -> Gearriveerd",
        "FIELDBUDDY__Start_Time__c": "2019-10-29T12:58:49.000+0000",
        "FIELDBUDDY__Type__c": "Servicebezoek",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002OIF7QAO",
        "FIELDBUDDY__Total_Time_Minutes__c": 2,
        "FIELDBUDDY__End_Time__c": "2019-10-29T13:00:28.000+0000",
        "FIELDBUDDY__Timesheet__c": "a0W5B000001OvFdUAK",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet_Lineitem__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet_Lineitem__c/a0V5B000008q4clUAA"
        },
        "Id": "a0V5B000008q4clUAA",
        "Name": "TSLI-000442",
        "FIELDBUDDY__Comments__c": "Onderweg -> Gearriveerd",
        "FIELDBUDDY__Start_Time__c": "2019-10-29T13:06:42.000+0000",
        "FIELDBUDDY__Type__c": "Lunch",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002OIF7QAO",
        "FIELDBUDDY__Total_Time_Minutes__c": 1,
        "FIELDBUDDY__End_Time__c": "2019-10-29T13:07:53.000+0000",
        "FIELDBUDDY__Timesheet__c": "a0W5B000001OvFdUAK",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet_Lineitem__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet_Lineitem__c/a0V5B000008q4cqUAA"
        },
        "Id": "a0V5B000008q4cqUAA",
        "Name": "TSLI-000443",
        "FIELDBUDDY__Comments__c": "Onderweg -> Gearriveerd",
        "FIELDBUDDY__Start_Time__c": "2019-10-29T13:07:59.000+0000",
        "FIELDBUDDY__Type__c": "Reizen",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002SVK6QAO",
        "FIELDBUDDY__Total_Time_Minutes__c": 0,
        "FIELDBUDDY__End_Time__c": "2019-10-29T13:08:10.000+0000",
        "FIELDBUDDY__Timesheet__c": "a0W5B000001OvFdUAK",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Timesheet_Lineitem__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Timesheet_Lineitem__c/a0V5B000008q4rlUAA"
        },
        "Id": "a0V5B000008q4rlUAA",
        "Name": "TSLI-000458",
        "FIELDBUDDY__Comments__c": "Onderweg -> Gearriveerd",
        "FIELDBUDDY__Start_Time__c": "2019-10-31T10:23:03.000+0000",
        "FIELDBUDDY__Type__c": "Reizen",
        "FIELDBUDDY__Work_Order__c": "a0j5B000002SfyuQAC",
        "FIELDBUDDY__Total_Time_Minutes__c": 61,
        "FIELDBUDDY__End_Time__c": "2019-10-31T11:24:00.000+0000",
        "FIELDBUDDY__Timesheet__c": "a0W5B000001OvPGUA0",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      }
    ],
    "FIELDBUDDY__Work_Order__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j5B000002RxPqQAK"
        },
        "Id": "a0j5B000002RxPqQAK",
        "Name": "WO-001415",
        "CreatedDate": "2019-10-23T11:29:46.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T5B000001HnnwUAC",
        "FIELDBUDDY__Status__c": "CLOSED",
        "T_Start_Time__c": "2019-10-23T09:00:00.000+0000",
        "T_End_Time__c": "2019-10-23T10:00:00.000+0000",
        "FIELDBUDDY__Comments_from_Technician__c": "Well and closed",
        "FIELDBUDDY__Closure_Status__c": "SOLVED",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "Approved_by__c": "Abc",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j1U000001617BQAQ"
        },
        "Id": "a0j1U000001617BQAQ",
        "Name": "WO-001285",
        "CreatedDate": "2019-03-28T07:25:36.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T1U000001AIjZUAW",
        "FIELDBUDDY__Status__c": "CLOSED",
        "T_Start_Time__c": "2019-08-06T11:30:00.000+0000",
        "T_End_Time__c": "2019-08-09T09:30:00.000+0000",
        "FIELDBUDDY__Type__c": "Preventief",
        "FIELDBUDDY__Comments_from_Backoffice__c": "Preventief onderhoud en inventarisatie",
        "FIELDBUDDY__Comments_from_Technician__c": "Bcaaaa",
        "FIELDBUDDY__Closure_Status__c": "SOLVED",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "Approved_by__c": "Abc",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j1U0000016SA2QAM"
        },
        "Id": "a0j1U0000016SA2QAM",
        "Name": "WO-001390",
        "CreatedDate": "2019-06-06T09:21:30.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T1U000001AqIZUA0",
        "FIELDBUDDY__Status__c": "CLOSED",
        "T_Start_Time__c": "2019-08-08T06:00:00.000+0000",
        "T_End_Time__c": "2019-09-05T09:18:00.000+0000",
        "FIELDBUDDY__Type__c": "Preventief",
        "FIELDBUDDY__Comments_from_Backoffice__c": "Preventief onderhoud ( te inventariseren) Nieuwe installatie via SKYLIFT",
        "FIELDBUDDY__Comments_from_Technician__c": "We",
        "FIELDBUDDY__Closure_Status__c": "FOLLOW_UP",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "Approved_by__c": "Aa",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j5B000002RmdNQAS"
        },
        "Id": "a0j5B000002RmdNQAS",
        "Name": "WO-001410",
        "CreatedDate": "2019-10-21T13:35:24.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T5B000001HnNZUA0",
        "FIELDBUDDY__Status__c": "ARRIVED",
        "T_Start_Time__c": "2019-10-22T09:00:00.000+0000",
        "T_End_Time__c": "2019-10-22T10:00:00.000+0000",
        "FIELDBUDDY__Closure_Status__c": "SOLVED",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j5B000002OIFMQA4"
        },
        "Id": "a0j5B000002OIFMQA4",
        "Name": "WO-001404",
        "CreatedDate": "2019-09-24T07:18:59.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T5B000001DdioUAC",
        "FIELDBUDDY__Status__c": "CLOSED",
        "T_Start_Time__c": "2019-11-01T09:00:00.000+0000",
        "T_End_Time__c": "2019-11-01T10:00:00.000+0000",
        "FIELDBUDDY__Type__c": "Installatie",
        "FIELDBUDDY__Closure_Status__c": "SOLVED",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "Approved_by__c": "Abc",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j5B000002RmdhQAC"
        },
        "Id": "a0j5B000002RmdhQAC",
        "Name": "WO-001412",
        "CreatedDate": "2019-10-21T13:36:19.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T5B000001HnNjUAK",
        "FIELDBUDDY__Status__c": "ON_THE_GO",
        "T_Start_Time__c": "2019-10-23T11:30:00.000+0000",
        "T_End_Time__c": "2019-10-23T13:30:00.000+0000",
        "FIELDBUDDY__Closure_Status__c": "SOLVED",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j5B000002RmQtQAK"
        },
        "Id": "a0j5B000002RmQtQAK",
        "Name": "WO-001409",
        "CreatedDate": "2019-10-21T12:45:13.000+0000",
        "FIELDBUDDY__Service_Request__c": "a0T5B000001HnNUUA0",
        "FIELDBUDDY__Status__c": "CLOSED",
        "T_Start_Time__c": "2019-10-21T09:00:00.000+0000",
        "T_End_Time__c": "2019-10-21T10:00:00.000+0000",
        "FIELDBUDDY__Type__c": "Installatie",
        "FIELDBUDDY__Comments_from_Backoffice__c": "Hdheh",
        "FIELDBUDDY__Closure_Status__c": "SOLVED",
        "Generate_WO_PDF_Email_Attachment_UN__c": false,
        "Send_WO_PDF_Email_To_Customer_UN__c": false,
        "Approved_by__c": "Abc",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "FIELDBUDDY__Work_Order_Item_Detail__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item_Detail__c/a0a1U000000FJ5sQAG"
        },
        "Id": "a0a1U000000FJ5sQAG",
        "Name": "Algemene indruk",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Order__c": 1,
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item_Detail__c/a0a1U000000FJ5tQAG"
        },
        "Id": "a0a1U000000FJ5tQAG",
        "Name": "Eindoordeel",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Order__c": 2,
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item_Detail__c/a0a1U000000FJ5uQAG"
        },
        "Id": "a0a1U000000FJ5uQAG",
        "Name": "Opschriften installatie",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Order__c": 4,
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item_Detail__c/a0a1U000000FJ5vQAG"
        },
        "Id": "a0a1U000000FJ5vQAG",
        "Name": "Valbeveiligingsvoorzieningen installatie",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Order__c": 5,
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item_Detail__c/a0a1U000000FJ5wQAG"
        },
        "Id": "a0a1U000000FJ5wQAG",
        "Name": "Valbeveiligingsvoorzieningen dak",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Order__c": 6,
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item_Detail__c/a0a1U000000FJ5xQAG"
        },
        "Id": "a0a1U000000FJ5xQAG",
        "Name": "Toegang tot installatie",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Order__c": 7,
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item_Detail__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item_Detail__c/a0a1U000000FJ5yQAG"
        },
        "Id": "a0a1U000000FJ5yQAG",
        "Name": "Logboek",
        "FIELDBUDDY__Field_Type__c": "Text Area",
        "FIELDBUDDY__Order__c": 8,
        "FIELDBUDDY__Work_Order_Item__c": "a0b1U000000DvvFQAS",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": false,
          "MaxAccessLevel": "Delete"
        }
      }
    ],
    "FIELDBUDDY__Work_Order_Item__c": [
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b1U000000DvvAQAS"
        },
        "CreatedDate": "2019-01-15T13:17:45.000+0000",
        "FIELDBUDDY__Item_Code__c": "FM1",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Type__c": "Activity",
        "FIELDBUDDY__Unit__c": "hour",
        "Id": "a0b1U000000DvvAQAS",
        "Name": "Preventief onderhoud",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b1U000000DvvBQAS"
        },
        "CreatedDate": "2019-01-15T13:19:20.000+0000",
        "FIELDBUDDY__Item_Code__c": "FM2",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Type__c": "Activity",
        "FIELDBUDDY__Unit__c": "hour",
        "Id": "a0b1U000000DvvBQAS",
        "Name": "Gepland correctief onderhoud",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b1U000000DvvFQAS"
        },
        "CreatedDate": "2019-01-15T13:18:24.000+0000",
        "FIELDBUDDY__Item_Code__c": "FM2767",
        "FIELDBUDDY__Quantity__c": 1,
        "FIELDBUDDY__Type__c": "Activity",
        "FIELDBUDDY__Unit__c": "hour",
        "Id": "a0b1U000000DvvFQAS",
        "Name": "Conditiemeting",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b1U000000DvvKQAS"
        },
        "CreatedDate": "2019-01-15T13:20:21.000+0000",
        "FIELDBUDDY__Item_Code__c": "FM3",
        "FIELDBUDDY__Type__c": "Activity",
        "FIELDBUDDY__Unit__c": "hour",
        "Id": "a0b1U000000DvvKQAS",
        "Name": "Kleine herstelwerkzaamheden",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b1U000000DvvPQAS"
        },
        "CreatedDate": "2019-01-15T13:21:34.000+0000",
        "FIELDBUDDY__Item_Code__c": "FM5",
        "FIELDBUDDY__Type__c": "Other expense",
        "Id": "a0b1U000000DvvPQAS",
        "Name": "Parkeerkosten",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b1U000000DvvUQAS"
        },
        "CreatedDate": "2019-01-15T13:26:29.000+0000",
        "FIELDBUDDY__Item_Code__c": "FM9",
        "FIELDBUDDY__Type__c": "Activity",
        "FIELDBUDDY__Unit__c": "hour",
        "Id": "a0b1U000000DvvUQAS",
        "Name": "Storing werkzaamheden",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      },
      {
        "attributes": {
          "type": "FIELDBUDDY__Work_Order_Item__c",
          "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b1U000000DvvZQAS"
        },
        "CreatedDate": "2019-01-15T13:29:12.000+0000",
        "FIELDBUDDY__Item_Code__c": "FM6",
        "FIELDBUDDY__Type__c": "Activity",
        "FIELDBUDDY__Unit__c": "hour",
        "Id": "a0b1U000000DvvZQAS",
        "Name": "Opname correctieve werkzaamheden",
        "UserRecordAccess": {
          "attributes": {
            "type": "UserRecordAccess"
          },
          "HasDeleteAccess": true,
          "HasEditAccess": true,
          "HasTransferAccess": true,
          "MaxAccessLevel": "All"
        }
      }
    ],
    "User": [
      {
        "attributes": {
          "type": "User",
          "url": "/services/data/v43.0/sobjects/User/0051U000000G6dNQAS"
        },
        "Id": "0051U000000G6dNQAS",
        "FirstName": "Koen",
        "LastName": "Albers",
        "Email": "slack+freshmaintenance@fieldbuddy.com"
      }
    ]
}

export const FRESH_MAINTENANCE_META_DATA = {}