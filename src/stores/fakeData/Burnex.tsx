export const SETTINGS14 = {
    "version": {
        "org": "BURNEX",
        "based": "CFG-000012",
        "version": 1,
        "date": "Dec-18-2019",
        "configuration": "CFG-000014"
    },
    "appSettings": {
        "applyOnlyUserChanges": true,
        "assets": [
            {
                "assetName": "confetti",
                "assetType": "StaticResource"
            },
            {
                "assetName": "FieldBuddyLogo",
                "assetType": "StaticResource"
            }
        ],
        "initAt": "FIELDBUDDY__Work_Order__c",
        "proxyForContent": "",
        "rules": {
            "privileges": {
                "checkPrivileges": false,
                "showPrivilegesAlerts": true,
                "hidePrivilegesUI": false,
                "onRead": "accessible",
                "onEdit": "updateable",
                "onDelete": "deletable",
                "onCreate": "createable"
            },
            "requiredRelationships": [
                "FIELDBUDDY__Service_Request__c",
                "FIELDBUDDY__Installed_Product__c",
                "FIELDBUDDY__Location__c",
                "Account"
            ],
            "items": {
                "table": "FIELDBUDDY__Work_Order_Item__c",
                "useDisplayType": true,
                "hideStatus": false,
                "chooseDefaultStatus": {
                    "targetField": "FIELDBUDDY__Status__c",
                    "targetValue": {
                        "Closed": {
                            "executor": "jsonQuery",
                            "type": "filter",
                            "source": "${DATA}",
                            "formula": "FIELDBUDDY__Work_Order__c[*]:isNotEmpty"
                        }
                    }
                },
                "icons": {
                    "Activity": "wrench",
                    "Part": "cube",
                    "Other Expense": "credit-card",
                    "Delete": "trash",
                    "Add": "plus",
                    "search-plus": "search-plus"
                },
                "limits": {
                    "maxItemsToDisplay": 25,
                    "maxResultToDisplay": 20
                }
            },
            "readonly": [
                {
                    "condition": {
                        "rule": "==",
                        "value": "CLOSED"
                    },
                    "field": "FIELDBUDDY__Status__c",
                    "table": "FIELDBUDDY__Work_Order__c"
                },
                {
                    "condition": {
                        "rule": "isIncomplete"
                    }
                }
            ],
            "actions": {
                "active": true
            },
            "renderEngine": "2.0",
            "defaultPaths": {
                "Service_Request": {
                    "Service_Request__c": "FIELDBUDDY__Service_Request__c",
                    "Installed_Product__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c",
                    "Location__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Location__c",
                    "Sub_location__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Sub_location__c",
                    "Account": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Account__c",
                    "Installed_Product__cAccount": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Location__c.FIELDBUDDY__Account__c",
                    "Service_Agreement__cAccount": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c.FIELDBUDDY__Account__c",
                    "Service_Agreement__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c",
                    "Product__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Product__c",
                    "Contact": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]",
                    "FIELDBUDDY__Installed_Product__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c",
                    "FIELDBUDDY__Installed_Product__cAccount": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Location__c.FIELDBUDDY__Account__c",
                    "FIELDBUDDY__Location__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Location__c",
                    "FIELDBUDDY__Service_Agreement__c": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c",
                    "FIELDBUDDY__Service_Agreement__cAccount": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c.FIELDBUDDY__Account__c",
                    "FIELDBUDDY__Service_Request__c": "FIELDBUDDY__Service_Request__c",
                    "FIELDBUDDY__Timesheet_Lineitem__c": "FIELDBUDDY__Timesheet_Lineitems__r[]",
                    "FIELDBUDDY__Work_Order_Line_Item__c": "FIELDBUDDY__Work_Order_Line_Items__r[]"
                },
                "Location": {
                    "Service_Request__c": "FIELDBUDDY__Service_Request__c",
                    "Location__c": "FIELDBUDDY__Location__c",
                    "Account": "FIELDBUDDY__Location__c.FIELDBUDDY__Account__c",
                    "Account2": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Account__c",
                    "Contact": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Contact__c",
                    "Timesheet_Lineitem__c": "Timesheet_Lineitems__r[]",
                    "Installed_Product__c": "Work_Order_Line_Items__r[].FIELDBUDDY__Installed_Product__c",
                    "Product__c": "FIELDBUDDY__Product__c",
                    "Work_Order_Line_Item__c": "Work_Order_Line_Items__r[]",
                    "Service_Agreement__c": "FIELDBUDDY__Service_Agreement__c",
                    "LocationsUnderAccount": "FIELDBUDDY__Location__c.FIELDBUDDY__Account__c.FIELDBUDDY__Locations__r",
                    "ContactsUnderAccount": "FIELDBUDDY__Location__c.FIELDBUDDY__Account__c.Contacts",
                    "Service_Agreement__c2": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c",
                    "SubLocations": "Work_Order_Line_Items__r.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Sub_location__c",
                    "SubLocation": "Work_Order_Line_Items__r[Id].FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Sub_location__c",
                    "SubLocationsFromLocation": "FIELDBUDDY__Location__c.FIELDBUDDY__Sub_Locations__r",
                    "ContactOfCertainSubLocation": "FIELDBUDDY__Location__c.FIELDBUDDY__Sub_Locations__r[Id].FIELDBUDDY__Contact__c",
                    "ContactsOfAllSubLocations": "FIELDBUDDY__Location__c.FIELDBUDDY__Sub_Locations__r.FIELDBUDDY__Contact__c",
                    "p1": "Work_Order_Line_Items__r[FIELDBUDDY__Work_Order_Line_Item__c.Id]",
                    "p2": "Work_Order_Line_Items__r",
                    "a": "Assets__r.Assets__r",
                    "a2": "Assets__r.Assets__r.Assets__r",
                    "a3": "Assets__r.Assets__r.Assets__r.Assets__r",
                    "Weirdo": "Work_Order_Line_Items__r[FIELDBUDDY__Work_Order_Line_Item__c].FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c.Service_Agreement_Lines__r.FIELDBUDDY__WorkOrderItem__c",
                    "IPS_FOR_LB_WO": "Work_Order_Line_Items__r.FIELDBUDDY__Installed_Product__c",
                    "IP_FOR_LB_WO": "Work_Order_Line_Items__r[Id].FIELDBUDDY__Installed_Product__c",
                    "WOLIDS": "Work_Order_Line_Items__r.Work_Order_Line_Item_Details__r",
                    "HistoricalWO": "FIELDBUDDY__Location__c.Work_Orders__r",
                    "HistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].FIELDBUDDY__Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r",
                    "AllWoDetailsPerHistoricalWOPerIP[]": "Work_Order_Line_Items__r[Id].FIELDBUDDY__Installed_Product__c.Work_Order_Line_Items__r.Work_Orders__r[Id].Work_Order_Line_Items__r.",
                    "FIELDBUDDY__Installed_Product__c": "FIELDBUDDY__Work_Order_Line_Items__r[].FIELDBUDDY__Installed_Product__c",
                    "FIELDBUDDY__Location__c": "FIELDBUDDY__Location__c",
                    "FIELDBUDDY__Service_Agreement__c": "FIELDBUDDY__Work_Order_Line_Items__r[].FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c",
                    "FIELDBUDDY__Service_Agreement__c2": "FIELDBUDDY__Service_Request__c.FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Service_Agreement__c",
                    "FIELDBUDDY__Service_Request__c": "FIELDBUDDY__Service_Request__c",
                    "FIELDBUDDY__Timesheet_Lineitem__c": "FIELDBUDDY__Timesheet_Lineitems__r[]",
                    "FIELDBUDDY__Work_Order_Line_Item__c": "FIELDBUDDY__Work_Order_Line_Items__r[]"
                }
            },
            "timesheets": {
                "implicit": true,
                "manual": false,
                "table": "FIELDBUDDY__Timesheet__c",
                "groupBy": {
                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                    "field": "FIELDBUDDY__Type__c",
                    "icons": {
                        "Service visit": "wrench",
                        "Travel": "car",
                        "Internal meeting": "users",
                        "Lunch": "hourglass"
                    }
                }
            },
            "pricing": {
                "showPrices": {
                    "activities": false,
                    "parts": false,
                    "otherExpenses": false
                },
                "includeVAT": true,
                "precision": 2
            }
        },
        "status": {
            "filters": [
                {
                    "acceptedValues": [
                        "OPEN",
                        "ON_THE_GO",
                        "ARRIVED",
                        "IN_PROGRESS",
                        "CLOSED"
                    ],
                    "field": "FIELDBUDDY__Status__c"
                }
            ],
            "order": {
                "field": "FIELDBUDDY__Status__c",
                "values": [
                    "OPEN",
                    "ON_THE_GO",
                    "ARRIVED",
                    "IN_PROGRESS",
                    "CLOSED"
                ]
            }
        },
        "workOrderCreation": {
            "isEnabled": true,
            "header": {
                "title": "${Label.CreateWorkOrder}"
            },
            "event": {
                "privileges": true,
                "eventType": "createable",
                "targets": [
                    "FIELDBUDDY__Work_Order__c"
                ]
            },
            "params": [
                {
                    "field": "Id",
                    "returnProperty": "$selectedAccount"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedInstalledProduct"
                },
                {
                    "field": "Id",
                    "returnProperty": "$selectedLocation"
                }
            ],
            "steps": [
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToFilter": [
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "BillingStreet",
                        "BillingPostalCode",
                        "BillingCity"
                    ],
                    "label": "${Label.SelectAccount}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedAccount",
                    "selectedDisplay": "${Label.SelectedAccount}",
                    "table": "Account",
                    "tableNameDisplay": "${Label.SearchAccount}"
                },
                {
                    "allowNewRecord": false,
                    "fieldsToDisplay": [
                        "FIELDBUDDY__Street__c",
                        "FIELDBUDDY__House_Number__c",
                        "FIELDBUDDY__House_Number_Suffix__c",
                        "FIELDBUDDY__Postal_Code__c",
                        "FIELDBUDDY__City__c"
                    ],
                    "fieldsToFilter": [
                        "FIELDBUDDY__Account__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "FIELDBUDDY__Street__c",
                        "FIELDBUDDY__House_Number__c",
                        "FIELDBUDDY__House_Number_Suffix__c",
                        "FIELDBUDDY__Postal_Code__c",
                        "FIELDBUDDY__City__c"
                    ],
                    "label": "${Label.SelectLocation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedLocation",
                    "selectedDisplay": "${Label.SelectedLocation}",
                    "table": "FIELDBUDDY__Location__c",
                    "tableNameDisplay": "${Label.SearchLocation}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedAccount"
                    }
                },
                {
                    "allowNewRecord": true,
                    "supportRecordTypes": false,
                    "creationFields": [
                        {
                            "recordType": "Default",
                            "fields": [
                                "Name",
                                "FIELDBUDDY__Construction_Year__c",
                                "FIELDBUDDY__Status__c",
                                "FIELDBUDDY__Date_Installed__c"
                            ]
                        }
                    ],
                    "fieldsToDisplay": [
                        "Name",
                        "FIELDBUDDY__Installed_Product_Number__c",
                        "FIELDBUDDY__Construction_Year__c"
                    ],
                    "fieldsToFilter": [
                        "FIELDBUDDY__Location__c"
                    ],
                    "fieldsToRequest": [
                        "Id",
                        "Name",
                        "FIELDBUDDY__Construction_Year__c",
                        "FIELDBUDDY__Installed_Product_Number__c"
                    ],
                    "label": "${Label.SelectInstallation}",
                    "limitOfRecords": 10,
                    "returnProperty": "$selectedInstalledProduct",
                    "selectedDisplay": "${Label.SelectedInstallation}",
                    "table": "FIELDBUDDY__Installed_Product__c",
                    "tableNameDisplay": "${Label.SearchInstallation}",
                    "newTableNameDisplay": "${Label.AddInstalledProduct}",
                    "valueToSearch": {
                        "field": "Id",
                        "returnProperty": "$selectedLocation"
                    }
                }
            ]
        }
    },
    "screens": [
        {
            "config": {
                "header": {
                    "title": "${FIELDBUDDY__Work_Order__c.plurallabel}"
                },
                "config": {
                    "groupBy": {
                        "field": "T_Start_Time__c",
                        "table": "FIELDBUDDY__Work_Order__c"
                    },
                    "groupByRange": {
                        "fields": {
                            "endDate": "T_End_Time__c",
                            "startDate": "T_Start_Time__c"
                        },
                        "table": "FIELDBUDDY__Work_Order__c"
                    },
                    "item": {
                        "config": {
                            "cardLayout": [
                                {
                                    "config": {
                                        "active": false,
                                        "columns": 1,
                                        "items": [
                                            {
                                                "dataBinding": [
                                                    {
                                                        "empty": "-",
                                                        "field": "Name",
                                                        "table": "Account",
                                                        "var": "CUSTOMER"
                                                    },
                                                    {
                                                        "empty": "-",
                                                        "field": "FIELDBUDDY__Street__c",
                                                        "table": "FIELDBUDDY__Location__c",
                                                        "var": "STREET"
                                                    },
                                                    {
                                                        "empty": "-",
                                                        "field": "FIELDBUDDY__House_Number__c",
                                                        "table": "FIELDBUDDY__Location__c",
                                                        "var": "HOUSE"
                                                    },
                                                    {
                                                        "empty": "-",
                                                        "field": "FIELDBUDDY__House_Number_Suffix__c",
                                                        "table": "FIELDBUDDY__Location__c",
                                                        "var": "SUFFIX"
                                                    },
                                                    {
                                                        "empty": "-",
                                                        "field": "FIELDBUDDY__Postal_Code__c",
                                                        "table": "FIELDBUDDY__Location__c",
                                                        "var": "ZIP"
                                                    },
                                                    {
                                                        "empty": "-",
                                                        "field": "FIELDBUDDY__City__c",
                                                        "table": "FIELDBUDDY__Location__c",
                                                        "var": "CITY"
                                                    },
                                                    {
                                                        "empty": "-",
                                                        "field": "FIELDBUDDY__Description__c",
                                                        "table": "FIELDBUDDY__Sub_location__c",
                                                        "var": "DESC"
                                                    }
                                                ],
                                                "dataText": {
                                                    "LINE1": "CUSTOMER",
                                                    "LINE2": "STREET HOUSE SUFFIX",
                                                    "LINE3": "ZIP CITY",
                                                    "LINE5": "DESC"
                                                },
                                                "staticBinding": [
                                                    {
                                                        "name": "CARD_TITLE",
                                                        "text": "${Account.label}",
                                                        "type": "TEXT"
                                                    },
                                                    {
                                                        "android": "md-person",
                                                        "ios": "ios-person",
                                                        "name": "CARD_ICON",
                                                        "type": "ICON"
                                                    }
                                                ],
                                                "type": "FSingleCard"
                                            }
                                        ]
                                    },
                                    "type": "FCardLayout"
                                },
                                {
                                    "config": {
                                        "active": false,
                                        "columns": 1,
                                        "items": [

                                            {
                                                "dataBinding": [
                                                    {
                                                        "empty": "-",
                                                        "field": "Sluitingsdagen__c",
                                                        "table": "Account",
                                                        "var": "Sluitingsdagen"
                                                    }
                                                ],
                                                "dataText": {
                                                    "LINE1": "Sluitingsdagen"
                                                },
                                                "staticBinding": [
                                                    {
                                                        "name": "CARD_TITLE",
                                                        "text": "${Account.Sluitingsdagen__c.label}",
                                                        "type": "TEXT"
                                                    },
                                                    {
                                                        "android": "md-briefcase",
                                                        "ios": "ios-briefcase",
                                                        "name": "CARD_ICON",
                                                        "type": "ICON"
                                                    }
                                                ],
                                                "type": "FSingleCard"
                                            },
                                            {
                                                "dataBinding": [
                                                    {
                                                        "empty": "-",
                                                        "field": "FIELDBUDDY__Type__c",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "P"
                                                    }
                                                ],
                                                "dataText": {
                                                    "LINE1": "P"
                                                },
                                                "staticBinding": [
                                                    {
                                                        "name": "CARD_TITLE",
                                                        "text": "${FIELDBUDDY__Work_Order__c.FIELDBUDDY__Type__c.label}",
                                                        "type": "TEXT"
                                                    },
                                                    {
                                                        "android": "md-briefcase",
                                                        "ios": "ios-briefcase",
                                                        "name": "CARD_ICON",
                                                        "type": "ICON"
                                                    }
                                                ],
                                                "type": "FSingleCard"
                                            },
                                            {
                                                "condition": [
                                                    {
                                                        "executor": "jsonQuery",
                                                        "type": "filter",
                                                        "source": "${DATA}",
                                                        "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}]:isServiceRequestBased"
                                                    }
                                                ],
                                                "dataBinding": [
                                                    {
                                                        "empty": "-",
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Installed_Product__c",
                                                        "var": "_Installed_Product__c_Naam_"
                                                    }
                                                ],
                                                "dataText": {
                                                    "LINE1": "_Installed_Product__c_Naam_"
                                                },
                                                "staticBinding": [
                                                    {
                                                        "name": "CARD_TITLE",
                                                        "text": "${Label.InstallationData}",
                                                        "type": "TEXT"
                                                    },
                                                    {
                                                        "android": "md-cog",
                                                        "ios": "ios-cog",
                                                        "name": "CARD_ICON",
                                                        "type": "ICON"
                                                    }
                                                ],
                                                "type": "FSingleCard"
                                            }
                                        ]
                                    },
                                    "type": "FCardLayout"
                                }
                            ],
                            "events": [
                                {
                                    "dataBinding": [
                                        {
                                            "field": "Name",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "NAME"
                                        },
                                        {
                                            "field": "Id",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "ID"
                                        }
                                    ],
                                    "eventName": "onPress",
                                    "target": "WorkOrderDetails",
                                    "triggeredEvent": "navigate"
                                }
                            ],
                            "header": {
                                "config": {
                                    "dataBinding": [
                                        {
                                            "field": "FIELDBUDDY__Status__c",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "STATUS"
                                        },
                                        {
                                            "field": "T_Start_Time__c",
                                            "format": "HH:mm",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "START_TIME"
                                        },
                                        {
                                            "field": "T_End_Time__c",
                                            "format": "HH:mm",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "END_TIME"
                                        }
                                    ],
                                    "dataText": {
                                        "LeftText": "START_TIME → END_TIME",
                                        "RightText": "STATUS",
                                        "Status": "STATUS"
                                    },
                                    "staticBinding": [
                                        {
                                            "android": "md-information-circle",
                                            "ios": "ios-information-circle",
                                            "name": "ICON_LEFT",
                                            "type": "ICON"
                                        },
                                        {
                                            "android": "md-play",
                                            "ios": "ios-play",
                                            "name": "ICON_RIGHT",
                                            "type": "ICON"
                                        },
                                        {
                                            "android": "md-radio-button-off",
                                            "ios": "ios-radio-button-off",
                                            "name": "ICON_STATUS_OPEN",
                                            "type": "ICON"
                                        },
                                        {
                                            "android": "md-radio-button-on",
                                            "ios": "ios-radio-button-on",
                                            "name": "ICON_STATUS_CLOSED",
                                            "type": "ICON"
                                        },
                                        {
                                            "android": "md-walk-outline",
                                            "ios": "ios-walk-outline",
                                            "name": "ICON_STATUS_PROGRESS_2",
                                            "type": "ICON"
                                        },
                                        {
                                            "animation": "in_progress_balls",
                                            "name": "ICON_STATUS_PROGRESS",
                                            "type": "ANIMATION"
                                        }
                                    ]
                                },
                                "type": "FListItemHeader"
                            },
                            "headline": {
                                "config": {
                                    "dataBinding": [
                                        {
                                            "field": "FIELDBUDDY__Type__c",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "A"
                                        },
                                        {
                                            "field": "Name",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "B"
                                        }
                                    ],
                                    "dataText": "A (B)"
                                },
                                "type": "FListItemHeadline"
                            },
                            "templateHeadline": {
                                "config": {
                                    "condition": [
                                        {
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "field": "FIELDBUDDY__Template__c",
                                            "filter": "in",
                                            "value": [
                                                "Location"
                                            ]
                                        }
                                    ],
                                    "dataBinding": [
                                        {
                                            "aggregationType": "$COUNT",
                                            "groupBy": "FIELDBUDDY__Installed_Product__c",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "var": "COUNT"
                                        }
                                    ],
                                    "staticBinding": [
                                        {
                                            "name": "PRODUCTS",
                                            "text": "${Label.InstalledProductsOnLocation}",
                                            "type": "TEXT"
                                        }
                                    ],
                                    "dataText": "COUNT PRODUCTS"
                                },
                                "type": "FListItemTemplateHeadline"
                            }
                        },
                        "type": "FListItem"
                    },
                    "orderBy": {
                        "field": "T_Start_Time__c",
                        "table": "FIELDBUDDY__Work_Order__c"
                    },
                    "empty": {
                        "line1": "${Label.NoWorkOrdersLine1}",
                        "line2": "${Label.NoWorkOrdersLine2}"
                    }
                },
                "type": "FWeekToolbar"
            },
            "name": "WorkOrders"
        },
        {
            "config": {
                "footerLayout": {
                    "config": {
                        "dataBinding": [
                            {
                                "field": "FIELDBUDDY__Status__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "STATUS"
                            }
                        ],
                        "dataText": {
                            "Status": "STATUS"
                        },
                        "events": [
                            {
                                "validation": [
                                    {
                                        "executor": "jsonQuery",
                                        "type": "validation",
                                        "alertType": "warning",
                                        "source": "${DATA}",
                                        "alertMessage": "${Label.StillOpenActivities}",
                                        "formula": "FIELDBUDDY__Work_Order_Line_Item__c[*FIELDBUDDY__Work_Order__c=${CURRENT_WORK_ORDER}&FIELDBUDDY__Type__c=Activity&FIELDBUDDY__Status__c=Open]:isEmpty"
                                    }
                                ],
                                "condition": [
                                    {
                                        "field": "Status",
                                        "filter": "==",
                                        "value": "CLOSED"
                                    }
                                ],
                                "dataBinding": [
                                    {
                                        "field": "Name",
                                        "table": "FIELDBUDDY__Work_Order__c",
                                        "var": "NAME"
                                    },
                                    {
                                        "field": "Id",
                                        "table": "FIELDBUDDY__Work_Order__c",
                                        "var": "ID"
                                    }
                                ],
                                "eventName": "onValueChanged",
                                "target": "WorkOrderClosurePlus",
                                "triggeredEvent": "navigate"
                            }
                        ]
                    },
                    "type": "FStatusFooter"
                },
                "tabsLayout": {
                    "config": [
                        {
                            "body": {
                                "config": {
                                    "itemLayout": [
                                        {
                                            "config": {
                                                "active": false,
                                                "columns": 1,
                                                "items": [
                                                    {
                                                        "dataBinding": [
                                                            {
                                                                "field": "Name",
                                                                "table": "Account",
                                                                "var": "CUSTOMER"
                                                            },
                                                            {
                                                                "field": "FIELDBUDDY__Street__c",
                                                                "table": "FIELDBUDDY__Location__c",
                                                                "var": "STREET"
                                                            },
                                                            {
                                                                "field": "FIELDBUDDY__House_Number__c",
                                                                "table": "FIELDBUDDY__Location__c",
                                                                "var": "HOUSE"
                                                            },
                                                            {
                                                                "field": "FIELDBUDDY__House_Number_Suffix__c",
                                                                "table": "FIELDBUDDY__Location__c",
                                                                "var": "SUFFIX"
                                                            },
                                                            {
                                                                "field": "FIELDBUDDY__Postal_Code__c",
                                                                "table": "FIELDBUDDY__Location__c",
                                                                "var": "ZIP"
                                                            },
                                                            {
                                                                "field": "FIELDBUDDY__City__c",
                                                                "table": "FIELDBUDDY__Location__c",
                                                                "var": "CITY"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "CUSTOMER",
                                                            "LINE2": "STREET HOUSE SUFFIX",
                                                            "LINE3": "ZIP CITY"
                                                        },
                                                        "staticBinding": [
                                                            {
                                                                "name": "CARD_TITLE",
                                                                "text": "${Account.label}",
                                                                "type": "TEXT"
                                                            },
                                                            {
                                                                "android": "md-person",
                                                                "ios": "ios-person",
                                                                "name": "CARD_ICON",
                                                                "type": "ICON"
                                                            }
                                                        ],
                                                        "type": "FSingleCard"
                                                    },
                                                    {
                                                        "dataBinding": [
                                                            {
                                                                "field": "Phone",
                                                                "table": "Account",
                                                                "var": "PHONE"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "PHONE"
                                                        },
                                                        "type": "FPhoneCard"
                                                    },
                                                    {
                                                        "dataBinding": [
                                                            {
                                                                "empty": "",
                                                                "field": "FirstName",
                                                                "table": "Contact",
                                                                "var": "FN"
                                                            },
                                                            {
                                                                "empty": "",
                                                                "field": "LastName",
                                                                "table": "Contact",
                                                                "var": "LN"
                                                            },
                                                            {
                                                                "empty": "",
                                                                "field": "Email",
                                                                "table": "Contact",
                                                                "var": "EMAIL"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "FN LN",
                                                            "LINE4": "EMAIL"
                                                        },
                                                        "staticBinding": [
                                                            {
                                                                "name": "CARD_TITLE",
                                                                "text": "${Contact.label}",
                                                                "type": "TEXT"
                                                            },
                                                            {
                                                                "android": "md-person",
                                                                "ios": "ios-person",
                                                                "name": "CARD_ICON",
                                                                "type": "ICON"
                                                            }
                                                        ],
                                                        "type": "FSingleCard"
                                                    },
                                                    {
                                                        "dataBinding": [
                                                            {
                                                                "field": "MobilePhone",
                                                                "table": "Contact",
                                                                "var": "MOBILEPHONE"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "MOBILEPHONE"
                                                        },
                                                        "type": "FPhoneCard"
                                                    },
                                                    {
                                                        "dataBinding": [
                                                            {
                                                                "field": "Phone",
                                                                "table": "Contact",
                                                                "var": "PHONE"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "PHONE"
                                                        },
                                                        "type": "FPhoneCard"
                                                    }
                                                ]
                                            },
                                            "type": "FCardLayout"
                                        },
                                        {
                                            "config": {
                                                "active": false,
                                                "columns": 1,
                                                "items": [
                                                    {
                                                        "dataBinding": [
                                                            {
                                                                "empty": "-",
                                                                "field": "Sluitingsdagen__c",
                                                                "table": "Account",
                                                                "var": "Sluitingsdagen"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "Sluitingsdagen"
                                                        },
                                                        "staticBinding": [
                                                            {
                                                                "name": "CARD_TITLE",
                                                                "text": "${Account.Sluitingsdagen__c.label}",
                                                                "type": "TEXT"
                                                            },
                                                            {
                                                                "android": "md-briefcase",
                                                                "ios": "ios-briefcase",
                                                                "name": "CARD_ICON",
                                                                "type": "ICON"
                                                            }
                                                        ],
                                                        "type": "FSingleCard"
                                                    },
                                                    {
                                                        "dataBinding": [
                                                            {
                                                                "empty": "",
                                                                "field": "FIELDBUDDY__Type__c",
                                                                "table": "FIELDBUDDY__Work_Order__c",
                                                                "var": "P"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "P"
                                                        },
                                                        "staticBinding": [
                                                            {
                                                                "name": "CARD_TITLE",
                                                                "text": "${FIELDBUDDY__Work_Order__c.FIELDBUDDY__Type__c.label}",
                                                                "type": "TEXT"
                                                            },
                                                            {
                                                                "android": "md-briefcase",
                                                                "ios": "ios-briefcase",
                                                                "name": "CARD_ICON",
                                                                "type": "ICON"
                                                            }
                                                        ],
                                                        "type": "FSingleCard"
                                                    },
                                                    {
                                                        "condition": [
                                                            {
                                                                "executor": "jsonQuery",
                                                                "type": "filter",
                                                                "source": "${DATA}",
                                                                "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}]:isServiceRequestBased"
                                                            }
                                                        ],
                                                        "dataBinding": [
                                                            {
                                                                "field": "Name",
                                                                "table": "FIELDBUDDY__Installed_Product__c",
                                                                "var": "_Installed_Product__c_Naam_"
                                                            },
                                                            {
                                                                "field": "FIELDBUDDY__Construction_Year__c",
                                                                "table": "FIELDBUDDY__Installed_Product__c",
                                                                "var": "_Installed_Product__c_Construction_Year__c_"
                                                            }
                                                        ],
                                                        "dataText": {
                                                            "LINE1": "_Installed_Product__c_Naam_",
                                                            "LINE2": "_Installed_Product__c_Construction_Year__c_"
                                                        },
                                                        "staticBinding": [
                                                            {
                                                                "name": "CARD_TITLE",
                                                                "text": "${Label.InstallationData}",
                                                                "type": "TEXT"
                                                            },
                                                            {
                                                                "android": "md-cog",
                                                                "ios": "ios-cog",
                                                                "name": "CARD_ICON",
                                                                "type": "ICON"
                                                            }
                                                        ],
                                                        "type": "FSingleCard"
                                                    }
                                                ]
                                            },
                                            "type": "FCardLayout"
                                        }
                                    ],
                                    "mapLayout": {
                                        "config": {
                                            "addressCard": {
                                                "config": {
                                                    "active": true,
                                                    "columns": 2,
                                                    "items": [
                                                        {
                                                            "dataBinding": [
                                                                {
                                                                    "field": "Name",
                                                                    "table": "Account",
                                                                    "var": "CUSTOMER"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__Street__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "STREET"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__House_Number__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "HOUSE"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__House_Number_Suffix__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "SUFFIX"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__Postal_Code__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "ZIP"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__City__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "CITY"
                                                                }
                                                            ],
                                                            "dataText": {
                                                                "LINE1": "STREET HOUSE SUFFIX",
                                                                "LINE2": "ZIP CITY"
                                                            },
                                                            "staticBinding": [
                                                                {
                                                                    "name": "CARD_TITLE",
                                                                    "text": "${FIELDBUDDY__Location__c.label}",
                                                                    "type": "TEXT"
                                                                },
                                                                {
                                                                    "android": "md-pin",
                                                                    "ios": "ios-pin",
                                                                    "name": "CARD_ICON",
                                                                    "type": "ICON"
                                                                }
                                                            ],
                                                            "type": "FSingleCard"
                                                        },
                                                        {
                                                            "dataBinding": [
                                                                {
                                                                    "field": "FIELDBUDDY__Postal_Code__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "P1"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__Street__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "P2"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__House_Number__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "P3"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__City__c",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "P4"
                                                                },
                                                                {
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "field": "FIELDBUDDY__Coordinates__Latitude__s",
                                                                    "var": "Latitude"
                                                                },
                                                                {
                                                                    "field": "FIELDBUDDY__Coordinates__Longitude__s",
                                                                    "table": "FIELDBUDDY__Location__c",
                                                                    "var": "Longitude"
                                                                }
                                                            ],
                                                            "dataText": {
                                                                "RAW_ADDRESS": "P1,P2,P3,P4"
                                                            },
                                                            "type": "FActionCard"
                                                        }
                                                    ]
                                                },
                                                "type": "FCardLayout"
                                            },
                                            "dataBinding": [
                                                {
                                                    "field": "FIELDBUDDY__Postal_Code__c",
                                                    "table": "FIELDBUDDY__Location__c",
                                                    "var": "P1"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Street__c",
                                                    "table": "FIELDBUDDY__Location__c",
                                                    "var": "P2"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__House_Number__c",
                                                    "table": "FIELDBUDDY__Location__c",
                                                    "var": "P3"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__City__c",
                                                    "table": "FIELDBUDDY__Location__c",
                                                    "var": "P4"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Coordinates__Latitude__s",
                                                    "table": "FIELDBUDDY__Location__c",
                                                    "var": "Latitude"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Coordinates__Longitude__s",
                                                    "table": "FIELDBUDDY__Location__c",
                                                    "var": "Longitude"
                                                }
                                            ],
                                            "dataText": {
                                                "DISPLAY_ADDRESS": "P1, P2 P3, P4",
                                                "Latitude": "Latitude",
                                                "Longitude": "Longitude",
                                                "RAW_ADDRESS": "P1,P2,P3,P4"
                                            }
                                        },
                                        "type": "FMapViewEx"
                                    },
                                    "propertiesLayout": {
                                        "config": {
                                            "sections": [
                                                {
                                                    "iconName": "md-people",
                                                    "iosIconName": "ios-people",
                                                    "mapping": "Account",
                                                    "title": "${Account.label}",
                                                    "objects": [
                                                        {
                                                            "name": "Name"
                                                        },
                                                        {
                                                            "name": "Phone"
                                                        },
                                                        {
                                                            "name": "Sluitingsdagen__c"
                                                        }
                                                    ],
                                                    "table": "Account"
                                                },
                                                {
                                                    "table": "Contact",
                                                    "iconName": "md-person",
                                                    "iosIconName": "ios-person",
                                                    "title": "${Contact.label}",
                                                    "mapping": "Contact",
                                                    "objects": [
                                                        {
                                                            "name": "Name"
                                                        },
                                                        {
                                                            "name": "Email"
                                                        },
                                                        {
                                                            "name": "Phone"
                                                        },
                                                        {
                                                            "name": "MobilePhone"
                                                        }
                                                    ]
                                                },
                                                {
                                                    "iconName": "md-pin",
                                                    "iosIconName": "ios-pin",
                                                    "mapping": "FIELDBUDDY__Location__c",
                                                    "title": "${FIELDBUDDY__Location__c.label}",
                                                    "objects": [
                                                        {
                                                            "name": "FIELDBUDDY__Street__c"
                                                        },
                                                        {
                                                            "name": "FIELDBUDDY__House_Number__c"
                                                        },
                                                        {
                                                            "name": "FIELDBUDDY__House_Number_Suffix__c"
                                                        },
                                                        {
                                                            "name": "FIELDBUDDY__Postal_Code__c"
                                                        },
                                                        {
                                                            "name": "FIELDBUDDY__City__c"
                                                        }
                                                    ],
                                                    "table": "FIELDBUDDY__Location__c"
                                                },
                                                {
                                                    "condition": [
                                                        {
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "field": "Product_Type__c",
                                                            "filter": "in",
                                                            "value": [
                                                                "Oven techniek"
                                                            ]
                                                        }
                                                    ],
                                                    "iconName": "md-cog",
                                                    "iosIconName": "ios-cog",
                                                    "mapping": "FIELDBUDDY__Installed_Product__c",
                                                    "objects": [
                                                        {
                                                            "name": "Name"
                                                        },
                                                        {
                                                            "name": "LD1G__c"
                                                        },
                                                        {
                                                            "name": "LD2L__c"
                                                        },
                                                        {
                                                            "name": "HD_G__c"
                                                        },
                                                        {
                                                            "name": "m3__c"
                                                        },
                                                        {
                                                            "name": "Ion__c"
                                                        },
                                                        {
                                                            "name": "Pa__c"
                                                        },
                                                        {
                                                            "name": "Bar__c"
                                                        },
                                                        {
                                                            "name": "TEMP__c"
                                                        }
                                                    ],
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "title": "${FIELDBUDDY__Installed_Product__c.label}"
                                                },
                                                {
                                                    "condition": [
                                                        {
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "field": "Product_Type__c",
                                                            "filter": "in",
                                                            "value": [
                                                                "Koeltechniek"
                                                            ]
                                                        }
                                                    ],
                                                    "iconName": "md-cog",
                                                    "iosIconName": "ios-cog",
                                                    "mapping": "FIELDBUDDY__Installed_Product__c",
                                                    "objects": [
                                                        {
                                                            "name": "Name"
                                                        },
                                                        {
                                                            "name": "X1_Koelmiddel__c"
                                                        },
                                                        {
                                                            "name": "X1_Code__c"
                                                        },
                                                        {
                                                            "name": "X1_KG__c"
                                                        },
                                                        {
                                                            "name": "X1_Flesnummer__c"
                                                        },
                                                        {
                                                            "name": "X2_Koelmiddel_R__c"
                                                        },
                                                        {
                                                            "name": "X2_Flesnummer__c"
                                                        },
                                                        {
                                                            "name": "Bedrijfsstroom_V__c"
                                                        },
                                                        {
                                                            "name": "Bedrijfsstroom_A__c"
                                                        },
                                                        {
                                                            "name": "Compressor_Type__c"
                                                        },
                                                        {
                                                            "name": "Olieniveau__c"
                                                        },
                                                        {
                                                            "name": "HD_pressostaat__c"
                                                        },
                                                        {
                                                            "name": "LD_pressostaat__c"
                                                        },
                                                        {
                                                            "name": "Installatie_controle_Persdruk__c"
                                                        },
                                                        {
                                                            "name": "Zuigdruk__c"
                                                        },
                                                        {
                                                            "name": "Condensatietemperatuur__c"
                                                        },
                                                        {
                                                            "name": "Verdampingstemperatuur__c"
                                                        },
                                                        {
                                                            "name": "Omgevingstemperatuur__c"
                                                        },
                                                        {
                                                            "name": "Condensor__c"
                                                        },
                                                        {
                                                            "name": "Verdamper__c"
                                                        },
                                                        {
                                                            "name": "Ventilatoren__c"
                                                        },
                                                        {
                                                            "name": "Bevochtiging_Div__c"
                                                        },
                                                        {
                                                            "name": "Elektra__c"
                                                        },
                                                        {
                                                            "name": "Verwarming__c"
                                                        },
                                                        {
                                                            "name": "Lekdichtheidscontrole__c"
                                                        },
                                                        {
                                                            "name": "Lektestertype__c"
                                                        },
                                                        {
                                                            "name": "Lektesternummer__c"
                                                        }
                                                    ],
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "title": "${FIELDBUDDY__Installed_Product__c.label}"
                                                },
                                                {
                                                    "iconName": "md-megaphone",
                                                    "iosIconName": "ios-megaphone",
                                                    "mapping": "FIELDBUDDY__Service_Request__c",
                                                    "objects": [
                                                        {
                                                            "name": "FIELDBUDDY__Subject__c"
                                                        },
                                                        {
                                                            "name": "FIELDBUDDY__Description__c"
                                                        }
                                                    ],
                                                    "table": "FIELDBUDDY__Service_Request__c",
                                                    "title": "${FIELDBUDDY__Service_Request__c.label}"
                                                },
                                                {
                                                    "iconName": "md-briefcase",
                                                    "iosIconName": "ios-briefcase",
                                                    "mapping": "FIELDBUDDY__Work_Order__c",
                                                    "title": "${FIELDBUDDY__Work_Order__c.label}",
                                                    "objects": [
                                                        {
                                                            "name": "FIELDBUDDY__Type__c"
                                                        },
                                                        {
                                                            "name": "FIELDBUDDY__Comments_from_Backoffice__c"
                                                        },
                                                        {
                                                            "name": "FIELDBUDDY__Comments_from_Technician__c"
                                                        }
                                                    ],
                                                    "table": "FIELDBUDDY__Work_Order__c"
                                                }
                                            ]
                                        },
                                        "type": "FProperties"
                                    }
                                },
                                "type": "FItem"
                            },
                            "header": {
                                "title": "${Label.WorkOrderTabHeader}"
                            }
                        },
                        {
                            "condition": [
                                {
                                    "executor": "jsonQuery",
                                    "type": "filter",
                                    "source": "${DATA}",
                                    "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}]:isLocationBased"
                                }
                            ],
                            "body": {
                                "config": {
                                    "events": [
                                        {
                                            "dataBinding": [
                                                {
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "var": "NAME"
                                                },
                                                {
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "var": "IP"
                                                }
                                            ],
                                            "eventName": "onGoDetails",
                                            "target": "MultiItems",
                                            "triggeredEvent": "navigate",
                                            "screenTitle": "${Label.Unknown}"
                                        }
                                    ],
                                    "item": {
                                        "config": {
                                            "events": [
                                                {
                                                    "dataBinding": [
                                                        {
                                                            "field": "Name",
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "var": "NAME"
                                                        },
                                                        {
                                                            "field": "Id",
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "var": "IP"
                                                        }
                                                    ],
                                                    "eventName": "onGoDetails",
                                                    "target": "MultiItems",
                                                    "triggeredEvent": "navigate",
                                                    "screenTitle": "${Label.Unknown}"
                                                }
                                            ],
                                            "dataBinding": [
                                                {
                                                    "label": "${Label.Number}",
                                                    "field": "FIELDBUDDY__Installed_Product_Number__c",
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "var": "_IP_"
                                                },
                                                {
                                                    "label": "Name",
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "var": "_NAME_"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Status__c",
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "var": "_STATUS_"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Installed_Product_Number__c",
                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                    "var": "_BARCODE_"
                                                },
                                                {
                                                    "path": "FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Sub_location__c",
                                                    "label": "${Label.Location}",
                                                    "field": "FIELDBUDDY__Description__c",
                                                    "table": "FIELDBUDDY__Location__c",
                                                    "var": "_DESC_"
                                                },
                                                {
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Work_Order__c"
                                                },
                                                {
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Work_Order__c"
                                                }
                                            ],
                                            "dataText": [
                                                "_NAME_",
                                                "_IP_",
                                                "_STATUS_",
                                                "_DESC_",
                                                "_BARCODE_"
                                            ],
                                            "searchText": [
                                                "_IP_",
                                                "_NAME_",
                                                "_BARCODE_"
                                            ]
                                        },
                                        "type": "FInstalledProduct"
                                    },
                                    "onlineSearchResults": {
                                        "config": {
                                            "recipe": {
                                                "tableToSearch": "FIELDBUDDY__Installed_Product__c",
                                                "fieldsToFilter": [
                                                    "Name",
                                                    "FIELDBUDDY__Installed_Product_Number__c"
                                                ],
                                                "restrictedBy": "FIELDBUDDY__Location__c",
                                                "fieldsToRequest": [
                                                    "Id",
                                                    "Name",
                                                    "FIELDBUDDY__Status__c",
                                                    "FIELDBUDDY__Location__c",
                                                    "FIELDBUDDY__Installed_Product_Number__c"
                                                ],
                                                "limitOfRecords": 30
                                            },
                                            "item": {
                                                "config": {
                                                    "events": [
                                                        {
                                                            "dataBinding": [
                                                                {
                                                                    "field": "Name",
                                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                                    "var": "NAME"
                                                                },
                                                                {
                                                                    "field": "Id",
                                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                                    "var": "ID"
                                                                },
                                                                {
                                                                    "field": "Name",
                                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                                    "var": "IP_NAME"
                                                                },
                                                                {
                                                                    "field": "Id",
                                                                    "table": "FIELDBUDDY__Installed_Product__c",
                                                                    "var": "IP_ID"
                                                                }
                                                            ],
                                                            "staticBinding": [
                                                                {
                                                                    "value": "Activity",
                                                                    "var": "TEMPLATE_TYPE"
                                                                },
                                                                {
                                                                    "value": "Part",
                                                                    "var": "TEMPLATE_TYPE"
                                                                },
                                                                {
                                                                    "value": "Other expense",
                                                                    "var": "TEMPLATE_TYPE"
                                                                },
                                                                {
                                                                    "value": "Location",
                                                                    "var": "TEMPLATE"
                                                                }
                                                            ],
                                                            "eventName": "onGoRelate",
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "goBack": "WorkOrderDetails",
                                                            "target": "WorkOrderItems",
                                                            "triggeredEvent": "navigate",
                                                            "screenTitle": "${Label.Unknown}"
                                                        }
                                                    ],
                                                    "dataBinding": [
                                                        {
                                                            "field": "FIELDBUDDY__Installed_Product_Number__c",
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "var": "_IP_"
                                                        },
                                                        {
                                                            "label": "Name",
                                                            "field": "Name",
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "var": "_NAME_"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Status__c",
                                                            "table": "FIELDBUDDY__Installed_Product__c",
                                                            "var": "_STATUS_"
                                                        }
                                                    ],
                                                    "dataText": [
                                                        "_NAME_",
                                                        "_IP_",
                                                        "_STATUS_"
                                                    ],
                                                    "searchText": [
                                                        "_IP_",
                                                        "_NAME_"
                                                    ]
                                                },
                                                "type": "FInstalledProduct"
                                            }
                                        },
                                        "type": "FOnlineSearchResults"
                                    },
                                    "params": {
                                        "parentTableName": "FIELDBUDDY__Installed_Product__c",
                                        "title": "${Label.InstalledProducts}"
                                    }
                                },
                                "type": "FInstalledProducts"
                            },
                            "actions": "${AddInstalledProducts}",
                            "header": {
                                "title": "${Label.InstalledProducts}"
                            }
                        },
                        {
                            "body": {
                                "config": {
                                    "events": [
                                        {
                                            "dataBinding": [
                                                {
                                                    "alias_field": "FIELDBUDDY__Work_Order__c",
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                    "var": "ID"
                                                }
                                            ],
                                            "eventName": "onPress",
                                            "staticBinding": [],
                                            "target": "LineItemInsert",
                                            "triggeredEvent": "navigate"
                                        }
                                    ],
                                    "item": {
                                        "config": {
                                            "dataBinding": [
                                                {
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "ID"
                                                },
                                                {
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "NAME"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Comments__c",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "COMMENTS"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Type__c",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "TYPE"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Start_Time__c",
                                                    "format": "dddd Do HH:mm",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "START_TIME"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__End_Time__c",
                                                    "format": "HH:mm",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "END_TIME"
                                                },
                                                {
                                                    "field": "localId",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "LOCAL_ID"
                                                },
                                                {
                                                    "field": "__FB__TEMPLATE_TYPE",
                                                    "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                    "var": "__FB__TEMPLATE_TYPE"
                                                }
                                            ],
                                            "dataText": {
                                                "Body": "COMMENTS",
                                                "Footer": "TYPE",
                                                "Id": "ID",
                                                "Title": "START_TIME - END_TIME"
                                            },
                                            "events": [
                                                {
                                                    "dataBinding": [
                                                        {
                                                            "alias_field": "Id",
                                                            "alias_table": "FIELDBUDDY__Work_Order__c",
                                                            "field": "FIELDBUDDY__Work_Order__c",
                                                            "table": "FIELDBUDDY__Timesheet_Lineitem__c"
                                                        },
                                                        {
                                                            "alias_field": "Id",
                                                            "alias_table": "FIELDBUDDY__Timesheet_Lineitem__c",
                                                            "field": "Id",
                                                            "nested": true,
                                                            "table": "FIELDBUDDY__Timesheet_Lineitem__c"
                                                        }
                                                    ],
                                                    "eventName": "onGoDetails",
                                                    "target": "LineItemUpsert",
                                                    "triggeredEvent": "navigate"
                                                }
                                            ]
                                        },
                                        "type": "FLineItem"
                                    },
                                    "params": {
                                        "parentTableName": "FIELDBUDDY__Timesheet_Lineitem__c",
                                        "title": "${FIELDBUDDY__Timesheet_Lineitem__c.label}"
                                    }
                                },
                                "type": "FTimesheetLineItems"
                            },
                            "actions": "${AddTimeSheet}",
                            "header": {
                                "title": "${Label.TimeRegistration}"
                            }
                        },
                        {
                            "condition": [
                                {
                                    "executor": "jsonQuery",
                                    "type": "filter",
                                    "source": "${DATA}",
                                    "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}]:isServiceRequestBased"
                                }
                            ],
                            "body": {
                                "config": {
                                    "events": [
                                        {
                                            "dataBinding": [
                                                {
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                    "var": "NAME"
                                                },
                                                {
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                    "var": "ID"
                                                }
                                            ],
                                            "eventName": "onPress",
                                            "staticBinding": [
                                                {
                                                    "value": "Activity",
                                                    "var": "TEMPLATE_TYPE"
                                                },
                                                {
                                                    "value": "Location",
                                                    "var": "TEMPLATE"
                                                }
                                            ],
                                            "goBack": "WorkOrderDetails",
                                            "target": "WorkOrderItems",
                                            "triggeredEvent": "navigate"
                                        },
                                        {
                                            "dataBinding": [
                                                {
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                    "var": "NAME"
                                                },
                                                {
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                    "var": "ID"
                                                }
                                            ],
                                            "eventName": "onPress",
                                            "staticBinding": [
                                                {
                                                    "value": "Part",
                                                    "var": "TEMPLATE_TYPE"
                                                },
                                                {
                                                    "value": "Location",
                                                    "var": "TEMPLATE"
                                                }
                                            ],
                                            "goBack": "WorkOrderDetails",
                                            "target": "WorkOrderItems",
                                            "triggeredEvent": "navigate"
                                        },
                                        {
                                            "dataBinding": [
                                                {
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                    "var": "NAME"
                                                },
                                                {
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Work_Order__c",
                                                    "var": "ID"
                                                }
                                            ],
                                            "eventName": "onPress",
                                            "staticBinding": [
                                                {
                                                    "value": "Other expense",
                                                    "var": "TEMPLATE_TYPE"
                                                },
                                                {
                                                    "value": "Location",
                                                    "var": "TEMPLATE"
                                                }
                                            ],
                                            "goBack": "WorkOrderDetails",
                                            "target": "WorkOrderItems",
                                            "triggeredEvent": "navigate"
                                        }
                                    ],
                                    "templateItem": {
                                        "Activity": {
                                            "config": {
                                                "dataBinding": [
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Type__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "TYPE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Item_Code__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "CODE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Quantity__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "QTY"
                                                    },
                                                    {
                                                        "field": "localId",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "LOCAL_ID"
                                                    },
                                                    {
                                                        "field": "__FB__TEMPLATE_TYPE",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "__FB__TEMPLATE_TYPE"
                                                    }
                                                ],
                                                "dataText": {
                                                    "Id": "ID",
                                                    "Title": "NAME",
                                                    "Body": "CODE",
                                                    "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                                },
                                                "increment": {
                                                    "referenceTable": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "referenceField": "FIELDBUDDY__Work_Order_Item__c",
                                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                                    "field": "FIELDBUDDY__Increment__c"
                                                },
                                                "events": [
                                                    {
                                                        "eventName": "onGoEdit",
                                                        "triggeredEvent": "navigate",
                                                        "target": "Edit",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditActivity}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onUpdateRecord",
                                                        "privileges": true,
                                                        "eventType": "updateable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onDeleteRecord",
                                                        "privileges": true,
                                                        "eventType": "deletable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "eventName": "onGoEditPlus",
                                                        "triggeredEvent": "navigate",
                                                        "target": "EditPlus",
                                                        "templateType": "Activity",
                                                        "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditActivity}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                            
                                                        ]
                                                    }
                                                ],
                                                "searchText": [
                                                    "NAME"
                                                ]
                                            },
                                            "type": "FPartWithStatus"
                                        },
                                        "Part": {
                                            "config": {
                                                "dataBinding": [
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Type__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "TYPE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Item_Code__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "CODE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Tax_Rate__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "VAT"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Quantity__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "QTY"
                                                    },
                                                    {
                                                        "field": "localId",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "LOCAL_ID"
                                                    },
                                                    {
                                                        "field": "__FB__TEMPLATE_TYPE",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "__FB__TEMPLATE_TYPE"
                                                    }
                                                ],
                                                "dataText": {
                                                    "Id": "ID",
                                                    "Title": "NAME",
                                                    "Body": "CODE",
                                                    "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                                },
                                                "increment": {
                                                    "referenceTable": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "referenceField": "FIELDBUDDY__Work_Order_Item__c",
                                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                                    "field": "FIELDBUDDY__Increment__c"
                                                },
                                                "events": [
                                                    {
                                                        "eventName": "onGoEdit",
                                                        "triggeredEvent": "navigate",
                                                        "target": "Edit",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditPart}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onUpdateRecord",
                                                        "privileges": true,
                                                        "eventType": "updateable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onDeleteRecord",
                                                        "privileges": true,
                                                        "eventType": "deletable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "eventName": "onGoEditPlus",
                                                        "triggeredEvent": "navigate",
                                                        "target": "EditPlus",
                                                        "templateType": "Part",
                                                        "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditPart}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                        ]
                                                    }
                                                ],
                                                "searchText": [
                                                    "NAME"
                                                ]
                                            },
                                            "type": "FPartWithStatus"
                                        },
                                        "Other expense": {
                                            "config": {
                                                "dataBinding": [
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Type__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "TYPE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Item_Code__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "CODE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Quantity__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "QTY"
                                                    },
                                                    {
                                                        "field": "localId",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "LOCAL_ID"
                                                    },
                                                    {
                                                        "field": "__FB__TEMPLATE_TYPE",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "__FB__TEMPLATE_TYPE"
                                                    }
                                                ],
                                                "dataText": {
                                                    "Body": "CODE",
                                                    "Id": "ID",
                                                    "Title": "NAME",
                                                    "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                                },
                                                "increment": {
                                                    "referenceTable": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "referenceField": "FIELDBUDDY__Work_Order_Item__c",
                                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                                    "field": "FIELDBUDDY__Increment__c"
                                                },
                                                "events": [
                                                    {
                                                        "eventName": "onGoEdit",
                                                        "triggeredEvent": "navigate",
                                                        "target": "Edit",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditOtherExpense}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onUpdateRecord",
                                                        "privileges": true,
                                                        "eventType": "updateable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onDeleteRecord",
                                                        "privileges": true,
                                                        "eventType": "deletable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "eventName": "onGoEditPlus",
                                                        "triggeredEvent": "navigate",
                                                        "target": "EditPlus",
                                                        "templateType": "OtherExpense",
                                                        "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditOtherExpense}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                        ]
                                                    }
                                                ],
                                                "searchText": [
                                                    "NAME"
                                                ]
                                            },
                                            "type": "FPartWithStatus"
                                        }
                                    },
                                    "item": {
                                        "config": {
                                            "dataBinding": [
                                                {
                                                    "field": "Id",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "ID"
                                                },
                                                {
                                                    "field": "Name",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "NAME"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Type__c",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "TYPE"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Item_Code__c",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "CODE"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Quantity__c",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "QTY"
                                                },
                                                {
                                                    "field": "FIELDBUDDY__Quantity__c",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "DQTY"
                                                },
                                                {
                                                    "field": "localId",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "LOCAL_ID"
                                                },
                                                {
                                                    "field": "__FB__TEMPLATE_TYPE",
                                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                    "var": "__FB__TEMPLATE_TYPE"
                                                }
                                            ]
                                        },
                                        "type": "FPartWithStatus"
                                    },
                                    "params": {
                                        "parentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                        "childrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                        "title": "${Label.Items}"
                                    }
                                },
                                "type": "FPartsWithOrderedFields"
                            },
                            "actions": "${AddMultipleItems}",
                            "header": {
                                "title": "${Label.Items}"
                            }
                        },
                        {
                            "body": {
                                "config": {
                                    "params": {
                                        "title": "${Label.Attachments}"
                                    }
                                },
                                "type": "FAttachments"
                            },
                            "actions": "${AddAttachments}",
                            "header": {
                                "title": "${Label.Attachments}"
                            }
                        }
                    ],
                    "type": "FTabs"
                }
            },
            "name": "WorkOrderDetails"
        },
        {
            "config": {
                "params": {
                    "mode": "summary"
                },
                "initAt": "FIELDBUDDY__Installed_Product__c",
                "items": [
                    {
                        "config": {
                            "dataBinding": [
                                {
                                    "label": "${Label.Number}",
                                    "field": "FIELDBUDDY__Installed_Product_Number__c",
                                    "table": "FIELDBUDDY__Installed_Product__c",
                                    "var": "_IP_"
                                },
                                {
                                    "label": "Name",
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Installed_Product__c",
                                    "var": "_NAME_"
                                },
                                {
                                    "path": "FIELDBUDDY__Installed_Product__c.FIELDBUDDY__Sub_location__c",
                                    "label": "${Label.Location}",
                                    "field": "FIELDBUDDY__Description__c",
                                    "table": "FIELDBUDDY__Location__c",
                                    "var": "_DESC_"
                                },
                                {
                                    "field": "FIELDBUDDY__Status__c",
                                    "table": "FIELDBUDDY__Installed_Product__c",
                                    "var": "_STATUS_"
                                },
                                {
                                    "field": "FIELDBUDDY__Installation_Notes__c",
                                    "table": "FIELDBUDDY__Installed_Product__c",
                                    "var": "_INSNOTES_"
                                },
                                {
                                    "label": "${Service_Agreement__c.label}",
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Service_Agreement__c",
                                    "var": "_SA_"
                                }
                            ],
                            "dataText": [
                                "_NAME_",
                                "_INSNOTES_",
                                "_IP_",
                                "_STATUS_",
                                "_SA_",
                                "_DESC_"
                            ]
                        },
                        "type": "FInstalledProduct"
                    },
                    {
                        "config": [
                            {
                                "body": {
                                    "config": {
                                        "sections": [
                                            {
                                                "iconName": "md-people",
                                                "iosIconName": "ios-people",
                                                "mapping": "Account",
                                                "title": "${Account.label}",
                                                "objects": [
                                                    {
                                                        "name": "Name"
                                                    }
                                                ],
                                                "table": "Account"
                                            },
                                            {
                                                "iconName": "md-people",
                                                "iosIconName": "ios-people",
                                                "mapping": "FIELDBUDDY__Product__c",
                                                "title": "${FIELDBUDDY__Product__c.label}",
                                                "objects": [
                                                    {
                                                        "name": "Name"
                                                    }
                                                ],
                                                "table": "FIELDBUDDY__Product__c"
                                            },
                                            {
                                                "iconName": "md-people",
                                                "iosIconName": "ios-people",
                                                "mapping": "FIELDBUDDY__Installed_Product__c",
                                                "title": "${FIELDBUDDY__Installed_Product__c.label}",
                                                "objects": [
                                                    {
                                                        "name": "FIELDBUDDY__Date_Installed__c"
                                                    },
                                                    {
                                                        "name": "FIELDBUDDY__Installation_Notes__c"
                                                    }
                                                ],
                                                "table": "FIELDBUDDY__Installed_Product__c"
                                            },
                                            {
                                                "iconName": "md-people",
                                                "iosIconName": "ios-people",
                                                "mapping": "FIELDBUDDY__Service_Agreement__c",
                                                "title": "${FIELDBUDDY__Service_Agreement__c.label}",
                                                "objects": [
                                                    {
                                                        "name": "Name",
                                                        "label": "${FIELDBUDDY__Service_Agreement__c.label}"
                                                    }
                                                ],
                                                "definedPath": "Service_Agreement__c2",
                                                "table": "FIELDBUDDY__Service_Agreement__c"
                                            }
                                        ]
                                    },
                                    "initAt": "FIELDBUDDY__Installed_Product__c",
                                    "type": "FProperties"
                                },
                                "actions": "${AddInstalledProductsDetails}",
                                "header": {
                                    "title": "${Label.ItemDetails}"
                                }
                            },
                            {
                                "body": {
                                    "initAt": "FIELDBUDDY__Installed_Product__c",
                                    "config": {
                                        "events": [
                                            {
                                                "dataBinding": [
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Installed_Product__c",
                                                        "var": "IP"
                                                    }
                                                ],
                                                "eventName": "onPress",
                                                "staticBinding": [
                                                    {
                                                        "value": "Activity",
                                                        "var": "TEMPLATE_TYPE"
                                                    },
                                                    {
                                                        "value": "Location",
                                                        "var": "TEMPLATE"
                                                    }
                                                ],
                                                "goBack": "MultiItems",
                                                "target": "WorkOrderItems",
                                                "triggeredEvent": "navigate"
                                            },
                                            {
                                                "dataBinding": [
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Installed_Product__c",
                                                        "var": "IP"
                                                    }
                                                ],
                                                "eventName": "onPress",
                                                "staticBinding": [
                                                    {
                                                        "value": "Part",
                                                        "var": "TEMPLATE_TYPE"
                                                    },
                                                    {
                                                        "value": "Location",
                                                        "var": "TEMPLATE"
                                                    }
                                                ],
                                                "goBack": "MultiItems",
                                                "target": "WorkOrderItems",
                                                "triggeredEvent": "navigate"
                                            },
                                            {
                                                "dataBinding": [
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Installed_Product__c",
                                                        "var": "IP"
                                                    }
                                                ],
                                                "eventName": "onPress",
                                                "staticBinding": [
                                                    {
                                                        "value": "Other expense",
                                                        "var": "TEMPLATE_TYPE"
                                                    },
                                                    {
                                                        "value": "Location",
                                                        "var": "TEMPLATE"
                                                    }
                                                ],
                                                "goBack": "MultiItems",
                                                "target": "WorkOrderItems",
                                                "triggeredEvent": "navigate"
                                            }
                                        ],
                                        "templateItem": {
                                            "Activity": {
                                                "config": {
                                                    "dataBinding": [
                                                        {
                                                            "field": "Id",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "ID"
                                                        },
                                                        {
                                                            "field": "Name",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "NAME"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Type__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "TYPE"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Item_Code__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "CODE"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Quantity__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "QTY"
                                                        },
                                                        {
                                                            "field": "localId",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "LOCAL_ID"
                                                        },
                                                        {
                                                            "field": "__FB__TEMPLATE_TYPE",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "__FB__TEMPLATE_TYPE"
                                                        }
                                                    ],
                                                    "dataText": {
                                                        "Id": "ID",
                                                        "Title": "NAME",
                                                        "Body": "CODE",
                                                        "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                                    },
                                                    "events": [
                                                        {
                                                            "eventName": "onGoEdit",
                                                            "staticBinding": [
                                                                {
                                                                    "value": "Location",
                                                                    "var": "TEMPLATE"
                                                                }
                                                            ],
                                                            "goBack": "MultiItems",
                                                            "triggeredEvent": "navigate",
                                                            "target": "Edit",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "screenTitle": "${Label.EditActivity}",
                                                            "typesFilters": [
                                                                "STRING",
                                                                "CURRENCY"
                                                            ],
                                                            "fieldsFilters": [
                                                                "Name"
                                                            ]
                                                        },
                                                        {
                                                            "triggeredEvent": "onUpdateRecord",
                                                            "privileges": true,
                                                            "eventType": "updateable",
                                                            "showAlert": true,
                                                            "targets": [
                                                                "FIELDBUDDY__Work_Order_Line_Item__c"
                                                            ]
                                                        },
                                                        {
                                                            "triggeredEvent": "onDeleteRecord",
                                                            "privileges": true,
                                                            "eventType": "deletable",
                                                            "showAlert": true,
                                                            "targets": [
                                                                "FIELDBUDDY__Work_Order_Line_Item__c"
                                                            ]
                                                        },
                                                        {
                                                            "eventName": "onGoEditPlus",
                                                            "triggeredEvent": "navigate",
                                                            "target": "EditPlus",
                                                            "templateType": "Activity",
                                                            "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "screenTitle": "${Label.EditActivity}",
                                                            "typesFilters": [
                                                                "STRING",
                                                                "CURRENCY"
                                                            ],
                                                            "fieldsFilters": [
                                                                "Name"
                                                            ]
                                                        }
                                                    ],
                                                    "searchText": [
                                                        "NAME"
                                                    ]
                                                },
                                                "type": "FPartWithStatus"
                                            },
                                            "Part": {
                                                "config": {
                                                    "dataBinding": [
                                                        {
                                                            "field": "Id",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "ID"
                                                        },
                                                        {
                                                            "field": "Name",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "NAME"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Type__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "TYPE"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Item_Code__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "CODE"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Quantity__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "QTY"
                                                        },
                                                        {
                                                            "field": "localId",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "LOCAL_ID"
                                                        },
                                                        {
                                                            "field": "__FB__TEMPLATE_TYPE",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "__FB__TEMPLATE_TYPE"
                                                        }
                                                    ],
                                                    "dataText": {
                                                        "Id": "ID",
                                                        "Title": "NAME",
                                                        "Body": "CODE PRICE",
                                                        "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                                    },
                                                    "events": [
                                                        {
                                                            "eventName": "onGoEdit",
                                                            "staticBinding": [
                                                                {
                                                                    "value": "Location",
                                                                    "var": "TEMPLATE"
                                                                }
                                                            ],
                                                            "goBack": "MultiItems",
                                                            "triggeredEvent": "navigate",
                                                            "target": "Edit",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "screenTitle": "${Label.EditPart}",
                                                            "typesFilters": [
                                                                "STRING",
                                                                "CURRENCY"
                                                            ],
                                                            "fieldsFilters": [
                                                                "Name"
                                                            ]
                                                        },
                                                        {
                                                            "triggeredEvent": "onUpdateRecord",
                                                            "privileges": true,
                                                            "eventType": "updateable",
                                                            "showAlert": true,
                                                            "targets": [
                                                                "FIELDBUDDY__Work_Order_Line_Item__c"
                                                            ]
                                                        },
                                                        {
                                                            "triggeredEvent": "onDeleteRecord",
                                                            "privileges": true,
                                                            "eventType": "deletable",
                                                            "showAlert": true,
                                                            "targets": [
                                                                "FIELDBUDDY__Work_Order_Line_Item__c"
                                                            ]
                                                        },
                                                        {
                                                            "eventName": "onGoEditPlus",
                                                            "triggeredEvent": "navigate",
                                                            "target": "EditPlus",
                                                            "templateType": "Part",
                                                            "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "screenTitle": "${Label.EditPart}",
                                                            "typesFilters": [
                                                                "STRING",
                                                                "CURRENCY"
                                                            ],
                                                            "fieldsFilters": [
                                                                "Name"
                                                            ]
                                                        }
                                                    ],
                                                    "searchText": [
                                                        "NAME"
                                                    ]
                                                },
                                                "type": "FPartWithStatus"
                                            },
                                            "Other expense": {
                                                "config": {
                                                    "dataBinding": [
                                                        {
                                                            "field": "Id",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "ID"
                                                        },
                                                        {
                                                            "field": "Name",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "NAME"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Type__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "TYPE"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Item_Code__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "CODE"
                                                        },
                                                        {
                                                            "field": "FIELDBUDDY__Quantity__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "QTY"
                                                        },
                                                        {
                                                            "field": "localId",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "LOCAL_ID"
                                                        },
                                                        {
                                                            "field": "__FB__TEMPLATE_TYPE",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "var": "__FB__TEMPLATE_TYPE"
                                                        }
                                                    ],
                                                    "dataText": {
                                                        "Body": "CODE",
                                                        "Id": "ID",
                                                        "Title": "NAME",
                                                        "Footer": "PRICE",
                                                        "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                                    },
                                                    "events": [
                                                        {
                                                            "eventName": "onGoEdit",
                                                            "staticBinding": [
                                                                {
                                                                    "value": "Location",
                                                                    "var": "TEMPLATE"
                                                                }
                                                            ],
                                                            "goBack": "MultiItems",
                                                            "triggeredEvent": "navigate",
                                                            "target": "Edit",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "screenTitle": "${Label.EditOtherExpense}",
                                                            "typesFilters": [
                                                                "STRING",
                                                                "CURRENCY"
                                                            ],
                                                            "fieldsFilters": [
                                                                "Name"
                                                            ]
                                                        },
                                                        {
                                                            "triggeredEvent": "onUpdateRecord",
                                                            "privileges": true,
                                                            "eventType": "updateable",
                                                            "showAlert": true,
                                                            "targets": [
                                                                "FIELDBUDDY__Work_Order_Line_Item__c"
                                                            ]
                                                        },
                                                        {
                                                            "triggeredEvent": "onDeleteRecord",
                                                            "privileges": true,
                                                            "eventType": "deletable",
                                                            "showAlert": true,
                                                            "targets": [
                                                                "FIELDBUDDY__Work_Order_Line_Item__c"
                                                            ]
                                                        },
                                                        {
                                                            "eventName": "onGoEditPlus",
                                                            "triggeredEvent": "navigate",
                                                            "target": "EditPlus",
                                                            "templateType": "OtherExpense",
                                                            "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                            "screenTitle": "${Label.EditOtherExpense}",
                                                            "typesFilters": [
                                                                "STRING",
                                                                "CURRENCY"
                                                            ],
                                                            "fieldsFilters": [
                                                                "Name"
                                                            ]
                                                        }
                                                    ],
                                                    "searchText": [
                                                        "NAME"
                                                    ]
                                                },
                                                "type": "FPartWithStatus"
                                            }
                                        },
                                        "item": {
                                            "config": {
                                                "dataBinding": [
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "Name",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "NAME"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Type__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "TYPE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Item_Code__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "CODE"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Quantity__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "QTY"
                                                    },
                                                    {
                                                        "field": "localId",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "LOCAL_ID"
                                                    },
                                                    {
                                                        "field": "__FB__TEMPLATE_TYPE",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "var": "__FB__TEMPLATE_TYPE"
                                                    }
                                                ],
                                                "dataText": {
                                                    "Id": "ID",
                                                    "Title": "NAME",
                                                    "Body": "CODE",
                                                    "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                                },
                                                "events": [
                                                    {
                                                        "eventName": "onGoEdit",
                                                        "staticBinding": [
                                                            {
                                                                "value": "Location",
                                                                "var": "TEMPLATE"
                                                            }
                                                        ],
                                                        "goBack": "MultiItems",
                                                        "triggeredEvent": "navigate",
                                                        "target": "Edit",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditActivity}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onUpdateRecord",
                                                        "privileges": true,
                                                        "eventType": "updateable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "triggeredEvent": "onDeleteRecord",
                                                        "privileges": true,
                                                        "eventType": "deletable",
                                                        "showAlert": true,
                                                        "targets": [
                                                            "FIELDBUDDY__Work_Order_Line_Item__c"
                                                        ]
                                                    },
                                                    {
                                                        "eventName": "onGoEditPlus",
                                                        "goBack": "MultiItems",
                                                        "triggeredEvent": "navigate",
                                                        "target": "EditPlus",
                                                        "templateType": "Activity",
                                                        "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                                        "screenTitle": "${Label.EditActivity}",
                                                        "typesFilters": [
                                                            "STRING",
                                                            "CURRENCY"
                                                        ],
                                                        "fieldsFilters": [
                                                            "Name"
                                                        ]
                                                    }
                                                ],
                                                "searchText": [
                                                    "NAME"
                                                ]
                                            },
                                            "initAt": "FIELDBUDDY__Installed_Product__c",
                                            "type": "FPartWithStatus"
                                        },
                                        "params": {
                                            "context": "WolisRelatedToInstalledProduct",
                                            "parentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "childrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                            "title": "${Label.Items}"
                                        }
                                    },
                                    "type": "FPartsWithOrderedFields"
                                },
                                "actions": "${AddItems}",
                                "header": {
                                    "title": "${Label.Items}"
                                }
                            },
                            {
                                "body": {
                                    "config": {
                                        "events": [
                                            {
                                                "dataBinding": [
                                                    {
                                                        "field": "Id",
                                                        "table": "FIELDBUDDY__Installed_Product__c",
                                                        "var": "ID"
                                                    },
                                                    {
                                                        "field": "FIELDBUDDY__Installed_Product_Number__c",
                                                        "table": "FIELDBUDDY__Installed_Product__c",
                                                        "var": "BARCODE"
                                                    }
                                                ],
                                                "eventName": "onBarcodeChanged",
                                                "target": "FIELDBUDDY__Installed_Product__c",
                                                "triggeredEvent": "updateRecord"
                                            }
                                        ],
                                        "params": {
                                            "title": "${Label.QRCode}"
                                        },
                                        "dataBinding": [
                                            {
                                                "label": "Barcode",
                                                "field": "FIELDBUDDY__Installed_Product_Number__c",
                                                "table": "FIELDBUDDY__Installed_Product__c",
                                                "var": "_BARCODE_"
                                            }
                                        ],
                                        "dataText": [
                                            "_BARCODE_"
                                        ],
                                        "propertiesLayout": {
                                            "config": {
                                                "sections": [
                                                    {
                                                        "headless": true,
                                                        "iconName": "md-people",
                                                        "iosIconName": "ios-people",
                                                        "mapping": "FIELDBUDDY__Installed_Product__c",
                                                        "title": "${FIELDBUDDY__Installed_Product__c.label}",
                                                        "objects": [
                                                            {
                                                                "name": "FIELDBUDDY__Installed_Product_Number__c"
                                                            }
                                                        ],
                                                        "table": "FIELDBUDDY__Installed_Product__c"
                                                    }
                                                ]
                                            },
                                            "initAt": "FIELDBUDDY__Installed_Product__c",
                                            "type": "FProperties"
                                        }
                                    },
                                    "initAt": "FIELDBUDDY__Installed_Product__c",
                                    "type": "FQr"
                                },
                                "actions": "${AddBarcode}",
                                "header": {
                                    "title": "${Label.QRCode}"
                                }
                            }
                        ],
                        "initAt": "FIELDBUDDY__Installed_Product__c",
                        "type": "FTabs"
                    }
                ],
                "events": [
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onCancel",
                        "target": "WorkOrderDetails",
                        "triggeredEvent": "navigate"
                    }
                ],
                "header": {
                    "item": {
                        "config": {
                            "dataBinding": [
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "_WO_NAME_"
                                },
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Installed_Product__c",
                                    "var": "_IP_"
                                }
                            ],
                            "dataText": [
                                "_WO_NAME_",
                                "_IP_"
                            ]
                        },
                        "initAt": "FIELDBUDDY__Installed_Product__c",
                        "type": "FText"
                    }
                }
            },
            "name": "MultiItems"
        },
        {
            "config": {
                "header": {
                    "title": "${Label.WorkOrderClosure}"
                },
                "events": [
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onCancel",
                        "target": "WorkOrderDetails",
                        "triggeredEvent": "navigate"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onApply",
                        "target": "WorkOrderSummary",
                        "triggeredEvent": "navigate",
                        "validation": [
                            {
                                "field": "ClosureStatus",
                                "rule": "required"
                            }
                        ]
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onApprove",
                        "target": "WorkOrderClosed",
                        "triggeredEvent": "navigate",
                        "validation": [
                            {
                                "field": "ClosureStatus",
                                "rule": "required"
                            }
                        ]
                    }
                ],
                "actionButtons": [
                    {
                        "condition": [
                            {
                                "executor": "jsonQuery",
                                "type": "filter",
                                "source": "${DATA}",
                                "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}&FIELDBUDDY__Closure_Status__c=SOLVED&Skip_Invoice_PDF_Email__c=false&Skip_WorkOrder_PDF_email__c=false]:isNotEmpty"
                            }
                        ],
                        "event": {
                            "validation": [
                                {
                                    "executor": "jsonQuery",
                                    "type": "validation",
                                    "alertType": "error",
                                    "source": "${DATA}",
                                    "alertMessage": "WorkOrder PDF email is Empty",
                                    "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}].Werkbon_PDF_naar_Contactpersoon__c:isNotEmpty"
                                },
                                {
                                    "executor": "jsonQuery",
                                    "type": "validation",
                                    "alertType": "error",
                                    "source": "${DATA}",
                                    "alertMessage": "Invoice PDF email is Empty",
                                    "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}].Factuur_PDF_naar_Contactpersoon__c:isNotEmpty"
                                }
                            ],
                            "dataBinding": [
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "NAME"
                                },
                                {
                                    "field": "Id",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "ID"
                                }
                            ],
                            "eventName": "onApply",
                            "target": "WorkOrderSummary",
                            "triggeredEvent": "navigate"
                        },
                        "config": {
                            "confirm": true
                        }
                    },
                    {
                        "condition": [
                            {
                                "executor": "jsonQuery",
                                "type": "filter",
                                "source": "${DATA}",
                                "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}&Skip_Invoice_PDF_Email__c=true&Skip_WorkOrder_PDF_email__c=false]:isNotEmpty"
                            }
                            
                        ],
                        "event": {
                            "validation": [
                                {
                                    "executor": "jsonQuery",
                                    "type": "validation",
                                    "alertType": "error",
                                    "source": "${DATA}",
                                    "alertMessage": "Work Order PDF email is Empty",
                                    "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}].Werkbon_PDF_naar_Contactpersoon__c:isNotEmpty"
                                }
                            ],
                            "dataBinding": [
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "NAME"
                                },
                                {
                                    "field": "Id",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "ID"
                                }
                            ],
                            "eventName": "onApply",
                            "target": "WorkOrderSummary",
                            "triggeredEvent": "navigate"
                        },
                        "config": {
                            "confirm": true
                        }
                    },
                    {
                        "condition": [
                            
                            {
                                "executor": "jsonQuery",
                                "type": "filter",
                                "source": "${DATA}",
                                "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}&Skip_WorkOrder_PDF_email__c=true&Skip_Invoice_PDF_Email__c=false]:isNotEmpty"
                            }
                        ],
                        "event": {
                            "validation": [
                                {
                                    "executor": "jsonQuery",
                                    "type": "validation",
                                    "alertType": "error",
                                    "source": "${DATA}",
                                    "alertMessage": "Invoice PDF email is Empty",
                                    "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}].Factuur_PDF_naar_Contactpersoon__c:isNotEmpty"
                                }
                            ],
                            "dataBinding": [
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "NAME"
                                },
                                {
                                    "field": "Id",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "ID"
                                }
                            ],
                            "eventName": "onApply",
                            "target": "WorkOrderSummary",
                            "triggeredEvent": "navigate"
                        },
                        "config": {
                            "confirm": true
                        }
                    },
                    
                    {
                        "condition": [
                            
                            {
                                "executor": "jsonQuery",
                                "type": "filter",
                                "source": "${DATA}",
                                "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}&Skip_Invoice_PDF_Email__c=true&Skip_WorkOrder_PDF_email__c=true]:isNotEmpty"
                            }
                        ],
                        "event": {
                            "validation": [
                                
                            ],
                            "dataBinding": [
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "NAME"
                                },
                                {
                                    "field": "Id",
                                    "table": "FIELDBUDDY__Work_Order__c",
                                    "var": "ID"
                                }
                            ],
                            "eventName": "onApply",
                            "target": "WorkOrderSummary",
                            "triggeredEvent": "navigate"
                        },
                        "config": {
                            "confirm": true
                        }
                    }
                ],
                "propertiesLayout": {
                    "config": {
                        "sections": [
                            {
                                "headless": true,
                                "iconName": "md-briefcase",
                                "iosIconName": "ios-briefcase",
                                "mapping": "FIELDBUDDY__Work_Order__c",
                                "title": "${FIELDBUDDY__Work_Order__c.Name.label}",
                                "objects": [
                                    {
                                        "name": "Name"
                                    }
                                ],
                                "table": "FIELDBUDDY__Work_Order__c"
                            },
                            {
                                "headless": true,
                                "iconName": "md-briefcase",
                                "iosIconName": "ios-briefcase",
                                "mapping": "FIELDBUDDY__Work_Order__c",
                                "title": "${FIELDBUDDY__Work_Order__c.FIELDBUDDY__Closure_Status__c.label}",
                                "objects": [
                                    {
                                        "name": "FIELDBUDDY__Closure_Status__c"
                                    }
                                ],
                                "table": "FIELDBUDDY__Work_Order__c"
                            },
                            {
                                "headless": true,
                                "iconName": "md-briefcase",
                                "iosIconName": "ios-briefcase",
                                "mapping": "FIELDBUDDY__Work_Order__c",
                                "title": "${FIELDBUDDY__Work_Order__c.Werkbon_PDF_naar_Contactpersoon__c.label}",
                                "objects": [
                                    {
                                        "name": "Werkbon_PDF_naar_Contactpersoon__c"
                                    }
                                ],
                                "table": "FIELDBUDDY__Work_Order__c"
                            },
                            {
                                "condition": [
                                    {
                                        "executor": "jsonQuery",
                                        "type": "filter",
                                        "source": "${DATA}",
                                        "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}].Werkbon_PDF_naar_Contactpersoon__c:isEmpty"
                                    }
                                ],
                                "headless": true,
                                "iconName": "md-briefcase",
                                "iosIconName": "ios-briefcase",
                                "mapping": "FIELDBUDDY__Work_Order__c",
                                "title": "${FIELDBUDDY__Work_Order__c.Skip_WorkOrder_PDF_email__c.label}",
                                "objects": [
                                    {
                                        "name": "Skip_WorkOrder_PDF_email__c"
                                    }
                                ],
                                "table": "FIELDBUDDY__Work_Order__c"
                            },
                            {
                                "headless": true,
                                "iconName": "md-briefcase",
                                "iosIconName": "ios-briefcase",
                                "mapping": "FIELDBUDDY__Work_Order__c",
                                "title": "${FIELDBUDDY__Work_Order__c.Factuur_PDF_naar_Contactpersoon__c.label}",
                                "objects": [
                                    {
                                        "name": "Factuur_PDF_naar_Contactpersoon__c"
                                    }
                                ],
                                "table": "FIELDBUDDY__Work_Order__c"
                            },
                            {
                                "condition": [
                                    {
                                        "executor": "jsonQuery",
                                        "type": "filter",
                                        "source": "${DATA}",
                                        "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}].Factuur_PDF_naar_Contactpersoon__c:isEmpty"
                                    }
                                ],
                                "headless": true,
                                "iconName": "md-briefcase",
                                "iosIconName": "ios-briefcase",
                                "mapping": "FIELDBUDDY__Work_Order__c",
                                "title": "${FIELDBUDDY__Work_Order__c.Skip_Invoice_PDF_Email__c.label}",
                                "objects": [
                                    {
                                        "name": "Skip_Invoice_PDF_Email__c"
                                    }
                                ],
                                "table": "FIELDBUDDY__Work_Order__c"
                            }
                        ]
                    },
                    "type": "FProperties"
                }
            },
            "name": "WorkOrderClosurePlus"
        },
        {
            "config": {
                "actions": [
                    {
                        "actionName": "CancelApprove",
                        "elems": [
                            {
                                "alias": "ApproveButton",
                                "eventName": "onApprove",
                                "ui_type": "BUTTON"
                            }
                        ]
                    }
                ],
                "body": [
                    {
                        "config": {
                            "active": false,
                            "columns": 1,
                            "items": [
                                {
                                    "dataBinding": [
                                        {
                                            "field": "Name",
                                            "table": "Account",
                                            "var": "CUSTOMER"
                                        },
                                        {
                                            "field": "FIELDBUDDY__Street__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "STREET"
                                        },
                                        {
                                            "field": "FIELDBUDDY__House_Number__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "HOUSE"
                                        },
                                        {
                                            "field": "FIELDBUDDY__House_Number_Suffix__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "SUFFIX"
                                        },
                                        {
                                            "field": "FIELDBUDDY__Postal_Code__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "ZIP"
                                        },
                                        {
                                            "field": "FIELDBUDDY__City__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "CITY"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "CUSTOMER",
                                        "LINE2": "STREET HOUSE SUFFIX",
                                        "LINE3": "ZIP CITY"
                                    },
                                    "staticBinding": [
                                        {
                                            "name": "CARD_TITLE",
                                            "text": "${Account.label}",
                                            "type": "TEXT"
                                        },
                                        {
                                            "android": "md-person",
                                            "ios": "ios-person",
                                            "name": "CARD_ICON",
                                            "type": "ICON"
                                        }
                                    ],
                                    "type": "FSingleCard"
                                },
                                {
                                    "dataBinding": [
                                        {
                                            "field": "Phone",
                                            "table": "Account",
                                            "var": "PHONE"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "PHONE"
                                    },
                                    "type": "FStaticCard"
                                },
                                {
                                    "dataBinding": [
                                        {
                                            "empty": "-",
                                            "field": "FirstName",
                                            "table": "Contact",
                                            "var": "${FN}"
                                        },
                                        {
                                            "empty": "",
                                            "field": "LastName",
                                            "table": "Contact",
                                            "var": "${LN}"
                                        },
                                        {
                                            "empty": "",
                                            "field": "Email",
                                            "table": "Contact",
                                            "var": "${EMAIL}"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "${FN} ${LN}",
                                        "LINE4": "${EMAIL}"
                                    },
                                    "staticBinding": [
                                        {
                                            "name": "CARD_TITLE",
                                            "text": "${Contact.label}",
                                            "type": "TEXT"
                                        },
                                        {
                                            "android": "md-person",
                                            "ios": "ios-person",
                                            "name": "CARD_ICON",
                                            "type": "ICON"
                                        }
                                    ],
                                    "type": "FSingleCard"
                                },
                                {
                                    "dataBinding": [
                                        {
                                            "empty": "-",
                                            "field": "FIELDBUDDY__Subject__c",
                                            "table": "FIELDBUDDY__Service_Request__c",
                                            "var": "${SB}"
                                        },
                                        {
                                            "empty": "-",
                                            "field": "FIELDBUDDY__Description__c",
                                            "table": "FIELDBUDDY__Service_Request__c",
                                            "var": "${DESC}"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "${SB}",
                                        "LINE4": "${DESC}"
                                    },
                                    "staticBinding": [
                                        {
                                            "name": "CARD_TITLE",
                                            "text": "${FIELDBUDDY__Service_Request__c.label}",
                                            "type": "TEXT"
                                        },
                                        {
                                            "android": "md-briefcase",
                                            "ios": "ios-briefcase",
                                            "name": "CARD_ICON",
                                            "type": "ICON"
                                        }
                                    ],
                                    "type": "FSingleCard"
                                }
                            ]
                        },
                        "type": "FCardLayout"
                    },
                    {
                        "config": {
                            "active": false,
                            "columns": 1,
                            "items": [
                                {
                                    "dataBinding": [
                                        {
                                            "empty": "",
                                            "field": "FIELDBUDDY__Type__c",
                                            "table": "FIELDBUDDY__Work_Order__c",
                                            "var": "P"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "P"
                                    },
                                    "staticBinding": [
                                        {
                                            "name": "CARD_TITLE",
                                            "text": "${FIELDBUDDY__Work_Order__c.FIELDBUDDY__Type__c.label}",
                                            "type": "TEXT"
                                        },
                                        {
                                            "android": "md-briefcase",
                                            "ios": "ios-briefcase",
                                            "name": "CARD_ICON",
                                            "type": "ICON"
                                        }
                                    ],
                                    "type": "FSingleCard"
                                },
                                {
                                    "condition": [
                                        {
                                            "executor": "jsonQuery",
                                            "type": "filter",
                                            "source": "${DATA}",
                                            "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}]:isServiceRequestBased"
                                        }
                                    ],
                                    "dataBinding": [
                                        {
                                            "field": "Name",
                                            "table": "FIELDBUDDY__Installed_Product__c",
                                            "var": "_Installed_Product__c_Naam_"
                                        },
                                        {
                                            "field": "FIELDBUDDY__Construction_Year__c",
                                            "table": "FIELDBUDDY__Installed_Product__c",
                                            "var": "_Installed_Product__c_Construction_Year__c_"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "_Installed_Product__c_Naam_",
                                        "LINE2": "_Installed_Product__c_Construction_Year__c_"
                                    },
                                    "staticBinding": [
                                        {
                                            "name": "CARD_TITLE",
                                            "text": "${Label.InstallationData}",
                                            "type": "TEXT"
                                        },
                                        {
                                            "android": "md-cog",
                                            "ios": "ios-cog",
                                            "name": "CARD_ICON",
                                            "type": "ICON"
                                        }
                                    ],
                                    "type": "FSingleCard"
                                }
                            ]
                        },
                        "type": "FCardLayout"
                    }
                ],
                "body2": [
                    {
                        "config": {
                            "active": false,
                            "columns": 1,
                            "items": [
                                {
                                    "dataBinding": [
                                        {
                                            "field": "Name",
                                            "table": "Account",
                                            "var": "CUSTOMER"
                                        },
                                        {
                                            "field": "FIELDBUDDY__Street__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "STREET"
                                        },
                                        {
                                            "field": "FIELDBUDDY__House_Number__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "HOUSE"
                                        },
                                        {
                                            "field": "FIELDBUDDY__House_Number_Suffix__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "SUFFIX"
                                        },
                                        {
                                            "field": "FIELDBUDDY__Postal_Code__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "ZIP"
                                        },
                                        {
                                            "field": "FIELDBUDDY__City__c",
                                            "table": "FIELDBUDDY__Location__c",
                                            "var": "CITY"
                                        },
                                        {
                                            "empty": "None",
                                            "field": "Phone",
                                            "table": "Account",
                                            "var": "PHONE"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "CUSTOMER",
                                        "LINE2": "STREET HOUSE SUFFIX",
                                        "LINE3": "ZIP CITY",
                                        "LINE4": "PHONE",
                                        "LINE5": "",
                                        "LINE6": ""
                                    },
                                    "staticBinding": [
                                        {
                                            "name": "CARD_TITLE",
                                            "text": "${Account.label}",
                                            "type": "TEXT"
                                        },
                                        {
                                            "android": "md-person",
                                            "ios": "ios-person",
                                            "name": "CARD_ICON",
                                            "type": "ICON"
                                        }
                                    ],
                                    "type": "FSingleCard"
                                }
                            ]
                        },
                        "type": "FCardLayout"
                    },
                    {
                        "config": {
                            "active": false,
                            "columns": 1,
                            "items": [
                                {
                                    "condition": [
                                        {
                                            "executor": "jsonQuery",
                                            "type": "filter",
                                            "source": "${DATA}",
                                            "formula": "FIELDBUDDY__Work_Order__c[Id=${CURRENT_WORK_ORDER}]:isLocationBased"
                                        }
                                    ],
                                    "dataBinding": [
                                        {
                                            "field": "Name",
                                            "table": "FIELDBUDDY__Installed_Product__c",
                                            "var": "_Installed_Product__c_Naam_"
                                        },
                                        {
                                            "field": "FIELDBUDDY__Construction_Year__c",
                                            "table": "FIELDBUDDY__Installed_Product__c",
                                            "var": "_Installed_Product__c_Construction_Year__c_"
                                        }
                                    ],
                                    "dataText": {
                                        "LINE1": "_Installed_Product__c_Naam_",
                                        "LINE2": "_Installed_Product__c_Construction_Year__c_"
                                    },
                                    "staticBinding": [
                                        {
                                            "name": "CARD_TITLE",
                                            "text": "${Label.InstallationData}",
                                            "type": "TEXT"
                                        },
                                        {
                                            "android": "md-cog",
                                            "ios": "ios-cog",
                                            "name": "CARD_ICON",
                                            "type": "ICON"
                                        }
                                    ],
                                    "type": "FSingleCard"
                                }
                            ]
                        },
                        "type": "FCardLayout"
                    }
                ],
                "dataBinding": [
                    {
                        "field": "FIELDBUDDY__Status__c",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "STATUS"
                    },
                    {
                        "field": "Name",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "ITEM_NAME"
                    },
                    {
                        "field": "FIELDBUDDY__Closure_Status__c",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "CLOSURE_STATUS"
                    },
                    {
                        "field": "FIELDBUDDY__Email__c",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "EMAIL_ADDRESS"
                    }
                ],
                "dataText": {
                    "ClosureStatus": "CLOSURE_STATUS",
                    "EmailAddress": "EMAIL_ADDRESS",
                    "ItemName": "ITEM_NAME",
                    "Status": "STATUS"
                },
                "events": [
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onCancel",
                        "target": "WorkOrderClosurePlus",
                        "triggeredEvent": "navigate"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onApprove",
                        "target": "WorkOrderSignature",
                        "triggeredEvent": "navigate",
                        "validation": [
                            {
                                "field": "CustomerName",
                                "rule": "required"
                            }
                        ]
                    }
                ],
                "fields": [
                    {
                        "alias": "Status",
                        "readOnly": true,
                        "ui_type": "STRING"
                    },
                    {
                        "alias": "ItemName",
                        "readOnly": true,
                        "ui_type": "STRING"
                    },
                    {
                        "alias": "ClosureStatus",
                        "ui_type": "PICKLIST"
                    },
                    {
                        "alias": "EmailAddress",
                        "ui_type": "EMAIL"
                    }
                ],
                "header": {
                    "signature_title": "${Label.WorkOrderSummarySignature}",
                    "title": "${Label.WorkOrderOverview}"
                },
                "logo": {
                    "config": {
                        "assetName": "FieldBuddyLogo"
                    },
                    "type": "FImageAsset"
                },
                "overview": [
                    {
                        "config": {
                            "filter": [
                                {
                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                    "field": "FIELDBUDDY__Type__c",
                                    "filter": "==",
                                    "value": "Activity"
                                }
                            ],
                            "item": {
                                "type": "FPart",
                                "config": {
                                    "searchText": [
                                        "NAME"
                                    ],
                                    "dataText": {
                                        "Id": "ID",
                                        "Title": "NAME",
                                        "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                    },
                                    "dataBinding": [
                                        {
                                            "var": "ID",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "Id"
                                        },
                                        {
                                            "var": "NAME",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "Name"
                                        },
                                        {
                                            "var": "CODE",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "FIELDBUDDY__Item_Code__c"
                                        },

                                        {
                                            "var": "QTY",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "FIELDBUDDY__Quantity__c"
                                        },
                                        {
                                            "var": "LOCAL_ID",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "localId"
                                        },
                                        {
                                            "var": "__FB__TEMPLATE_TYPE",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "__FB__TEMPLATE_TYPE"
                                        }
                                    ]
                                }
                            },
                            "params": {
                                "mode": "summary",
                                "parentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                "title": "${Label.Activities}",
                                "childrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c"
                            }
                        },
                        "type": "FParts"
                    },
                    {
                        "config": {
                            "filter": [
                                {
                                    "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                    "field": "FIELDBUDDY__Type__c",
                                    "filter": "==",
                                    "value": "Part"
                                }
                            ],
                            "params": {
                                "parentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                "childrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                "title": "${Label.Parts}",
                                "mode": "summary"
                            },
                            "item": {
                                "type": "FPart",
                                "config": {
                                    "dataText": {
                                        "Id": "ID",
                                        "Title": "NAME",
                                        "Body": "CODE DESC",
                                        "__FB__TEMPLATE_TYPE": "__FB__TEMPLATE_TYPE"
                                    },
                                    "dataBinding": [
                                        {
                                            "var": "ID",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "Id"
                                        },
                                        {
                                            "var": "NAME",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "Name"
                                        },
                                        {
                                            "var": "CODE",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "FIELDBUDDY__Item_Code__c"
                                        },
                                        {
                                            "var": "QTY",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "FIELDBUDDY__Quantity__c"
                                        },
                                        {
                                            "var": "LOCAL_ID",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "localId"
                                        },
                                        {
                                            "var": "__FB__TEMPLATE_TYPE",
                                            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                                            "field": "__FB__TEMPLATE_TYPE"
                                        }
                                    ]
                                }
                            }
                        },
                        "type": "FParts"
                    }
                ],
                "signature": {
                    "config": {
                        "actions": [
                            {
                                "actionName": "CancelApprove",
                                "elems": [
                                    {
                                        "alias": "ApproveButton",
                                        "eventName": "onApprove",
                                        "ui_type": "BUTTON"
                                    }
                                ]
                            }
                        ],
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ITEM_NAME"
                            },
                            {
                                "field": "FIELDBUDDY__Closure_Status__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "CLOSURE_STATUS"
                            },
                            {
                                "field": "FIELDBUDDY__Email__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "EMAIL_ADDRESS"
                            },
                            {
                                "field": "Approved_by__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "CUSTOMER_NAME"
                            },
                            {
                                "field": "Signature__FB",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "SIGNATURE"
                            },
                            {
                                "field": "FIELDBUDDY__Rating__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "RATING"
                            }
                        ],
                        "dataText": {
                            "ClosureStatus": "CLOSURE_STATUS",
                            "CustomerName": "CUSTOMER_NAME",
                            "EmailAddress": "EMAIL_ADDRESS",
                            "ItemName": "ITEM_NAME",
                            "Signature": "SIGNATURE",
                            "Rating": "RATING"
                        },
                        "events": [
                            {
                                "dataBinding": [
                                    {
                                        "field": "Name",
                                        "table": "FIELDBUDDY__Work_Order__c",
                                        "var": "NAME"
                                    },
                                    {
                                        "field": "Id",
                                        "table": "FIELDBUDDY__Work_Order__c",
                                        "var": "ID"
                                    }
                                ],
                                "eventName": "onApprove",
                                "target": "WorkOrderClosed",
                                "triggeredEvent": "navigate",
                                "validation": [
                                    {
                                        "field": "CustomerName",
                                        "rule": "required"
                                    }
                                ]
                            }
                        ],
                        "fields": [
                            {
                                "alias": "Signature",
                                "ui_type": "SIGNATURE_PAD"
                            },
                            {
                                "alias": "CustomerName",
                                "ui_type": "STRING"
                            },
                            {
                                "alias": "Rating",
                                "ui_type": "RATING",
                                "default": 2,
                                "count": 5
                            }
                        ]
                    },
                    "type": "FWorkOrderSignature"
                }
            },
            "name": "WorkOrderSummary"
        },
        {
            "config": {
                "actions": [
                    {
                        "actionName": "Finish",
                        "elems": [
                            {
                                "alias": "ApproveButton",
                                "eventName": "onFinish",
                                "ui_type": "BUTTON"
                            }
                        ]
                    }
                ],
                "body": {
                    "thankYouText": "${Label.WorkOrderClosedThankYou}"
                },
                "dataBinding": [
                    {
                        "field": "FIELDBUDDY__Status__c",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "STATUS"
                    },
                    {
                        "field": "FIELDBUDDY__Closure_Status__c",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "CLOSURE_STATUS"
                    },
                    {
                        "field": "Signature__FB",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "SIGNATURE"
                    }
                ],
                "dataText": {
                    "ClosureStatus": "CLOSURE_STATUS",
                    "Signature": "SIGNATURE",
                    "Status": "STATUS"
                },
                "events": [
                    {
                        "dataBinding": [
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            },
                            {
                                "field": "FIELDBUDDY__Closure_Status__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "CLOSURE_STATUS"
                            },
                            {
                                "field": "FIELDBUDDY__Status__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "STATUS"
                            },
                            {
                                "field": "Signature__FB",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "SIGNATURE"
                            },
                            {
                                "field": "Approved_by__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "CUSTOMER_NAME"
                            },
                            {
                                "field": "FIELDBUDDY__Email__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "EMAIL_ADDRESS"
                            },
                            {
                                "field": "FIELDBUDDY__Comments_from_Technician__c",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "COMMENTS"
                            }
                        ],
                        "eventName": "onInit",
                        "target": "FIELDBUDDY__Work_Order__c",
                        "triggeredEvent": "updateRecord"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onFinish",
                        "target": "WorkOrders",
                        "triggeredEvent": "navigate"
                    }
                ],
                "fields": [
                    {
                        "alias": "Status",
                        "readOnly": true,
                        "ui_type": "STRING"
                    },
                    {
                        "alias": "ItemName",
                        "readOnly": true,
                        "ui_type": "STRING"
                    },
                    {
                        "alias": "ClosureStatus",
                        "ui_type": "PICKLIST"
                    },
                    {
                        "alias": "EmailAddress",
                        "ui_type": "EMAIL"
                    }
                ],
                "logo": {
                    "config": {
                        "assetName": "confetti"
                    },
                    "type": "FImageAsset"
                }
            },
            "name": "WorkOrderClosed"
        },
        {
            "config": {
                "header": {
                    "title": {
                        "Activity": "${Label.AddActivities}",
                        "Other expense": "${Label.AddOtherExpenses}",
                        "Part": "${Label.AddParts}"
                    },
                    "searchText": {
                        "Activity": "",
                        "Other expense": "",
                        "Part": ""
                    }
                },
                "dataBinding": [
                    {
                        "field": "Name",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "NAME"
                    },
                    {
                        "field": "Id",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "var": "ID"
                    },
                    {
                        "field": "Id",
                        "table": "FIELDBUDDY__Installed_Product__c",
                        "var": "IP"
                    }
                ],
                "dataText": {
                    "Id": "ID",
                    "Name": "NAME"
                },
                "events": [
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onPress",
                        "target": "WorkOrderDetails",
                        "triggeredEvent": "navigate"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "IP"
                            }
                        ],
                        "eventName": "onPressMultiItems",
                        "target": "MultiItems",
                        "triggeredEvent": "navigate",
                        "screenTitle": "${Label.Unknown}"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "IP_ID"
                            }
                        ],
                        "eventName": "onGoBackCreation",
                        "target": "InstalledProductCreation",
                        "triggeredEvent": "navigate"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "IP_ID"
                            }
                        ],
                        "eventName": "onGoBackRelation",
                        "target": "InstalledProductRelation",
                        "triggeredEvent": "navigate"
                    }
                ],
                "params": {
                    "parentTableName": "FIELDBUDDY__Work_Order__c",
                    "targetTableName": {
                        "Activity": "FIELDBUDDY__Work_Order_Line_Item__c",
                        "Other expense": "FIELDBUDDY__Work_Order_Line_Item__c",
                        "Part": "FIELDBUDDY__Work_Order_Line_Item__c"
                    },
                    "templateTable": "FIELDBUDDY__Work_Order_Item__c",
                    "templateTableTypeField": "FIELDBUDDY__Type__c"
                },
                "templateItem": {
                    "Activity": {
                        "config": {
                            "dataBinding": [
                                {
                                    "field": "Id",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "ID"
                                },
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "NAME"
                                },
                                {
                                    "field": "FIELDBUDDY__Type__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "TYPE"
                                },
                                {
                                    "field": "FIELDBUDDY__Unit_Price_Excluding_Tax__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "EXC_PRICE"
                                },
                                {
                                    "field": "FIELDBUDDY__Tax_Rate__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "TAX_RATE"
                                },
                                {
                                    "field": "FIELDBUDDY__Unit_Price_Including_Tax_input__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "INCL_PRICE_INPUT"
                                },
                                {
                                    "field": "FIELDBUDDY__Item_Code__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "CODE"
                                },
                                {
                                    "field": "FIELDBUDDY__Display_Type__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "DT"
                                },
                                {
                                    "field": "FIELDBUDDY__Quantity__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "QTY"
                                }
                            ],
                            "dataText": {
                                "Id": "ID",
                                "Title": "NAME",
                                "Body": "CODE",
                                "Quantity": "QTY"
                            },
                            "increment": {
                                "table": "FIELDBUDDY__Work_Order_Item__c",
                                "field": "FIELDBUDDY__Increment__c"
                            },
                            "searchText": [
                                "NAME",
                                "CODE"
                            ],
                            "events": [
                                {
                                    "eventName": "onGoInsertPlus",
                                    "triggeredEvent": "navigate",
                                    "target": "InsertPlus",
                                    "templateType": "Activity",
                                    "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                    "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "screenTitle": "${FIELDBUDDY__Work_Order_Line_Item__c.plurallabel}",
                                    "typesFilters": [
                                        "STRING",
                                        "CURRENCY"
                                    ],
                                    "fieldsFilters": [
                                        "Name"
                                    ]
                                }
                            ]
                        },
                        "type": "FWorkOrderItem"
                    },
                    "Other expense": {
                        "config": {
                            "dataBinding": [
                                {
                                    "field": "Id",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "ID"
                                },
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "NAME"
                                },
                                {
                                    "field": "FIELDBUDDY__Type__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "TYPE"
                                },
                                {
                                    "field": "FIELDBUDDY__Unit_Price_Excluding_Tax__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "EXC_PRICE"
                                },
                                {
                                    "field": "FIELDBUDDY__Tax_Rate__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "TAX_RATE"
                                },
                                {
                                    "field": "FIELDBUDDY__Unit_Price_Including_Tax_input__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "INCL_PRICE_INPUT"
                                },
                                {
                                    "field": "FIELDBUDDY__Item_Code__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "CODE"
                                },
                                
                                {
                                    "field": "FIELDBUDDY__Tax_Rate__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "VAT"
                                },
                                {
                                    "field": "FIELDBUDDY__Display_Type__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "DT"
                                },
                                {
                                    "field": "FIELDBUDDY__Quantity__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "QTY"
                                }
                            ],
                            "increment": {
                                "table": "FIELDBUDDY__Work_Order_Item__c",
                                "field": "FIELDBUDDY__Increment__c"
                            },
                            "dataText": {
                                "Body": "CODE",
                                "Id": "ID",
                                "Title": "NAME",
                                "Quantity": "QTY"
                            },
                            "searchText": [
                                "NAME",
                                "CODE"
                            ],
                            "events": [
                                {
                                    "eventName": "onGoInsertPlus",
                                    "triggeredEvent": "navigate",
                                    "target": "InsertPlus",
                                    "templateType": "OtherExpense",
                                    "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                    "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "screenTitle": "${FIELDBUDDY__Work_Order_Line_Item__c.plurallabel}",
                                    "typesFilters": [
                                        "STRING",
                                        "CURRENCY"
                                    ],
                                    "fieldsFilters": [
                                        "Name"
                                    ]
                                }
                            ]
                        },
                        "type": "FWorkOrderItem"
                    },
                    "Part": {
                        "config": {
                            "dataBinding": [
                                {
                                    "field": "Id",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "ID"
                                },
                                {
                                    "field": "Name",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "NAME"
                                },
                                {
                                    "field": "FIELDBUDDY__Type__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "TYPE"
                                },
                                {
                                    "field": "FIELDBUDDY__Unit_Price_Excluding_Tax__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "EXC_PRICE"
                                },
                                {
                                    "field": "FIELDBUDDY__Tax_Rate__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "TAX_RATE"
                                },
                                {
                                    "field": "FIELDBUDDY__Unit_Price_Including_Tax_input__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "INCL_PRICE_INPUT"
                                },
                                {
                                    "field": "FIELDBUDDY__Item_Code__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "CODE"
                                },
                                {
                                    "field": "FIELDBUDDY__Display_Type__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "DT"
                                },
                                {
                                    "field": "FIELDBUDDY__Quantity__c",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "var": "QTY"
                                }
                            ],
                            "dataText": {
                                "Id": "ID",
                                "Title": "NAME",
                                "Body": "CODE"
                            },
                            "increment": {
                                "table": "FIELDBUDDY__Work_Order_Item__c",
                                "field": "FIELDBUDDY__Increment__c"
                            },
                            "searchText": [
                                "NAME"
                            ],
                            "events": [
                                {
                                    "eventName": "onGoInsertPlus",
                                    "fieldsFilters": [
                                        "Name"
                                    ],
                                    "screenTitle": "${FIELDBUDDY__Work_Order_Line_Item__c.plurallabel}",
                                    "table": "FIELDBUDDY__Work_Order_Item__c",
                                    "target": "InsertPlus",
                                    "targetChildrenTableName": "FIELDBUDDY__Work_Order_Line_Item_Detail__c",
                                    "targetParentTableName": "FIELDBUDDY__Work_Order_Line_Item__c",
                                    "templateType": "Part",
                                    "triggeredEvent": "navigate",
                                    "typesFilters": [
                                        "STRING",
                                        "CURRENCY"
                                    ]
                                }
                            ]
                        },
                        "type": "FWorkOrderItem"
                    }
                }
            },
            "name": "WorkOrderItems"
        },
        {
            "config": {
                "dataBinding": [
                    {
                        "field": "Id",
                        "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                        "var": "ID"
                    },
                    {
                        "field": "FIELDBUDDY__Work_Order__c",
                        "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                        "var": "Work_Order__c"
                    },
                    {
                        "field": "FIELDBUDDY__Start_Time__c",
                        "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                        "var": "START_TIME"
                    },
                    {
                        "field": "FIELDBUDDY__End_Time__c",
                        "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                        "var": "END_TIME"
                    },
                    {
                        "field": "FIELDBUDDY__Type__c",
                        "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                        "var": "TYPE"
                    },
                    {
                        "field": "FIELDBUDDY__Comments__c",
                        "table": "FIELDBUDDY__Timesheet_Lineitem__c",
                        "var": "COMMENTS"
                    }
                ],
                "dataText": {
                    "Comments": "COMMENTS",
                    "End time": "END_TIME",
                    "Id": "ID",
                    "Name": "NAME",
                    "Start time": "START_TIME",
                    "Type": "TYPE",
                    "Wo": "WO"
                },
                "events": [
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onCancel",
                        "target": "WorkOrderDetails",
                        "triggeredEvent": "navigate"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            }
                        ],
                        "eventName": "onApprove",
                        "target": "WorkOrderClosed",
                        "triggeredEvent": "navigate",
                        "validation": [
                            {
                                "field": "ClosureStatus",
                                "rule": "required"
                            },
                            {
                                "field": "EmailAddress",
                                "regex": "EMAIL_REGEX",
                                "rule": "regex"
                            }
                        ]
                    }
                ]
            },
            "name": "TimeSheetLineItem"
        },
        {
            "config": {
                "header": "${Label.CreateInstalledProduct}",
                "label": "${Label.InstallationData}",
                "table": "Installed_Product__c",
                "events": [
                    {
                        "eventName": "main",
                        "privileges": true,
                        "eventType": "createable",
                        "targets": [
                            "FIELDBUDDY__Installed_Product__c",
                            "FIELDBUDDY__Work_Order_Line_Item__c",
                            "FIELDBUDDY__Work_Order_Item__c"
                        ]
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "IP"
                            }
                        ],
                        "eventName": "addItems",
                        "staticBinding": [
                            {
                                "value": "Activity",
                                "var": "TEMPLATE_TYPE"
                            },
                            {
                                "value": "Location",
                                "var": "TEMPLATE"
                            }
                        ],
                        "goBack": "InstalledProductCreation",
                        "target": "WorkOrderItems",
                        "triggeredEvent": "navigate"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "IP"
                            }
                        ],
                        "eventName": "createIP",
                        "staticBinding": [
                            {
                                "value": "Activity",
                                "var": "TEMPLATE_TYPE"
                            },
                            {
                                "value": "Location",
                                "var": "TEMPLATE"
                            }
                        ],
                        "goBack": "InstalledProductCreation",
                        "target": "WorkOrderItems",
                        "triggeredEvent": "navigate"
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "IP"
                            }
                        ],
                        "eventName": "modifyIP",
                        "staticBinding": [
                            {
                                "value": "Activity",
                                "var": "TEMPLATE_TYPE"
                            },
                            {
                                "value": "Location",
                                "var": "TEMPLATE"
                            }
                        ],
                        "goBack": "InstalledProductCreation",
                        "target": "WorkOrderItems",
                        "triggeredEvent": "navigate"
                    }
                ],
                "geoLocation": false,
                "fieldsToRequest": [
                    {
                        "field": "Name",
                        "table": "FIELDBUDDY__Installed_Product__c",
                        "required": true
                    },
                    {
                        "field": "FIELDBUDDY__Product__c",
                        "table": "FIELDBUDDY__Installed_Product__c",
                        "online": true,
                        "required": true
                    },
                    {
                        "field": "FIELDBUDDY__Location__c",
                        "table": "FIELDBUDDY__Work_Order__c",
                        "required": false,
                        "online": true,
                        "preselected": true,
                        "readOnlyFromConfig": true
                    },
                    {
                        "field": "FIELDBUDDY__Status__c",
                        "table": "FIELDBUDDY__Installed_Product__c",
                        "required": false
                    },
                    {
                        "field": "FIELDBUDDY__Installed_Product_Number__c",
                        "table": "FIELDBUDDY__Installed_Product__c",
                        "required": false
                    }
                ]
            },
            "name": "InstalledProductCreation"
        },
        {
            "config": {
                "header": "${Label.RelateInstalledProduct}",
                "label": "${Label.InstallationData}",
                "table": "FIELDBUDDY__Installed_Product__c",
                "events": [
                    {
                        "eventName": "main",
                        "privileges": true,
                        "eventType": "createable",
                        "targets": [
                            "FIELDBUDDY__Work_Order_Line_Item__c",
                            "FIELDBUDDY__Work_Order_Item__c"
                        ]
                    },
                    {
                        "dataBinding": [
                            {
                                "field": "Name",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "NAME"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Work_Order__c",
                                "var": "ID"
                            },
                            {
                                "field": "Id",
                                "table": "FIELDBUDDY__Installed_Product__c",
                                "var": "IP"
                            }
                        ],
                        "eventName": "addItems",
                        "staticBinding": [
                            {
                                "value": "Activity",
                                "var": "TEMPLATE_TYPE"
                            },
                            {
                                "value": "Location",
                                "var": "TEMPLATE"
                            }
                        ],
                        "goBack": "InstalledProductRelation",
                        "target": "WorkOrderItems",
                        "triggeredEvent": "navigate"
                    }
                ],
                "geoLocation": false,
                "fieldsToRequest": [
                    {
                        "field": "FIELDBUDDY__Installed_Product__c",
                        "table": "FIELDBUDDY__Work_Order_Line_Item__c",
                        "online": true,
                        "required": true
                    }
                ]
            },
            "name": "InstalledProductRelation"
        }
    ]
}