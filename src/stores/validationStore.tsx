import { action } from 'mobx';
import _ from 'lodash';
import jsonQuery from 'json-query';
import * as Sentry from 'sentry-expo';
import validate from 'validate.js';
import {
  multipleCondition,
  singleCondition,
  condition_v1,
  Sources,
  operator,
  source,
  InputValidationResponse,
  alias,
  Constants,
  Operators
} from '../types'
import moment from "moment";

/**
 * MOBX STORE - for data validation
 */

type SwiftWhereType={
  fields: Array<string>,
  values: Array<string>

}
type SwiftHelperType={ 
  context: string,
  method: string,
  select: string,
  from: string,
  where: SwiftWhereType,
  whereNot?: SwiftWhereType,
  
  var: string
}

type SwiftFormulaType = {
  params: Array<ParamType>,
  formula: string,
  helpers?: Array<SwiftHelperType>
  formulas?: Array<SwiftHelperType>

}
 type ValidationRule = {
  Id: string,
  Name: string,
  Code__c?: string,
  Warning_Message__c: string,
  Error_Message__c: string,
  Formula__c: string,
  SwiftFormula: SwiftFormulaType,
  Mobile_Page__r?: string
}

type ParamType={
  var: string,
  field: string,
  table: string,
  defaultValue?: any
}

type ParamDictionary = {
  key: string,
  value?: any
}

export default class ValidationStore {

  constructor(rootStore: any) {
    this.rootStore = rootStore
    this.condition = null
  }

  condition: multipleCondition | null

  rootStore: any

  // Checking if we have any linked documents/attachments to currentWO
  _hasLinkedAttachments(arr : any) {
    const { dataStore } = this.rootStore

    let currentWorkOrder = dataStore.getCurrentWorkOrder()

    if (currentWorkOrder) {
      let result = arr.filter((x : any) => {
        
        return _.includes(x.Linked, currentWorkOrder.Id) && !x.IsBroken   
        
      })
      return !_.isEmpty(result)
    }
    return false
  }

  _hasFailedAttachments(arr : any) {
    const { dataStore } = this.rootStore

    let currentWorkOrder = dataStore.getCurrentWorkOrder()
    let failedAttachment = false

    if (currentWorkOrder) {
      let result = arr.filter((x : any) => {
        if (x.LinkedObjects) {
          return _.includes(x.LinkedObjects.FIELDBUDDY__Work_Order__c, currentWorkOrder.Id)
        }
        else { return null }
      })
      if (result != null) {
        result.forEach(a => {
          if (a.IsFailed || a.IsBeingUploaded || a.IsLocal || a.IsBroken) {
            failedAttachment = true
          }
        });
      }
      if (!failedAttachment) {
        return true
      }
    }
    return false
  }

  /**
   * Validates a single condition. The most basic form of a condition
   */
  validateSingleCondition(condition: singleCondition): boolean {

    try {

      let _condition = _.cloneDeep(condition)
      // If the conditions has an alias, then search for the formula
      if (_condition.alias) {
        _condition = this.getConditionByAlias(_condition.alias)
      }


      if (this.isSingleCondition(_condition)) {
        // Get source or default
        let _source = _condition.source ? _condition.source : Sources.DATA
        if (_condition.hasOwnProperty("alias") && _condition.alias === "ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS" && _source.replace("{", "").replace("}", "").replace("$", "") === Sources.MEDIA) {
          return this._hasFailedAttachments(this.selectSource(_source))
        }

        if (_condition.formula === "[*Linked=${CURRENT_WORK_ORDER}]:isNotEmpty" && _source.replace("{", "").replace("}", "").replace("$", "") === Sources.MEDIA) {
          return this._hasLinkedAttachments(this.selectSource(_source))
        }

        // Replace constants in the formula
        let _formula = _condition.formula ? this.replaceConstants(_condition.formula) : null

        if (_formula) {
          // Apply the formula and source
          let queryResult = jsonQuery(_formula, { data: this.selectSource(_source), locals: this.helpers })

          if (queryResult) {
            const { value } = queryResult

            // Validate results
            let result = (typeof value === "boolean") ? value : !_.isEmpty(value)

            return result
          }

        }

      }

      return false

    } catch (e) {
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });
      // Sentry.Native.captureException(new Error(`Error while validating a condition. condition:${condition}`), {
      //   logger: 'validationStore',
      //   context: 'validateSingleCondition'
      // });
      return false
    }
  }

  checkCondition(condition: Array<singleCondition> | multipleCondition | Array<condition_v1>, data: any): boolean {

    const { rootStore } = this
    const { loginStore } = rootStore


    if (_.isArray(condition)) {

      //@ts-ignore We checked that thi is an array
      let firstCondition = _.first(condition)

      if (this.isSingleCondition(firstCondition)) {

        //@ts-ignore Already checked that conditions is type singleCondition
        return this.resolveGroupOfSingleConditions(condition)

      } else {

        return loginStore.testConditions(data, condition)
      }
    }
    if (this.isMultipleCondition(condition)) {





      return this.validateCondition(condition)
    }

    return false
  }

  resolveGroupOfSingleConditions(conditions: Array<singleCondition>): boolean {
    return conditions.reduce((acc: boolean, singleCondition: singleCondition) => {

      return this.validateSingleCondition(singleCondition) && acc
    }, true)
  }

  /**
   * Validates a condition. Including an array of single conditions or multiple ones.
   */
  validateCondition(condition: multipleCondition): boolean {
    const { conditions } = condition
    const { operator } = condition

    let resolvedConditions = conditions.map((cond: singleCondition | multipleCondition) => {
      if (this.isSingleCondition(cond)) {
        // It's a single condition
        return this.validateSingleCondition(cond)
      } else {
        // It's a multiple condition
        return this.validateCondition(cond)
      }
    })
    return this.resolveLogicResults(resolvedConditions, operator)
  }

  /**
   * Returns a logic result given an array of booleans and a logic operator 
   */
  resolveLogicResults(results: Array<boolean>, operator: operator): boolean {
    switch (operator) {
      case Operators.AND: return results.reduce((acc: boolean, result: boolean) => result && acc, true)
      case Operators.OR: return results.reduce((acc: boolean, result: boolean) => result || acc, false)
      default: return results.reduce((acc: boolean, result: boolean) => result && acc, true)
    }
    return false
  }

  /**
   * Checks if the arg type is singleCondition
   * @param arg 
   */
  isSingleCondition(arg: any): arg is singleCondition {
    return (
      arg && (arg.hasOwnProperty("formula") || arg.hasOwnProperty("alias"))
    )
  }

  /**
   * Checks if the arg type is multipleCondition
   * @param arg 
   */
  isMultipleCondition(arg: any): arg is multipleCondition {
    return (
      arg && (arg.hasOwnProperty("operator") || arg.hasOwnProperty("conditions"))
    )
  }

  /**
   * Checks if the arg type is multipleCondition
   * @param arg 
   */
  iscondition_v1(arg: any): arg is condition_v1 {
    return (
      arg && (arg.hasOwnProperty("table") || arg.hasOwnProperty("field"))
    )
  }

  /**
   * Selects the source in the store. The formula will be executed against this source.
   */
  @action
  selectSource(source: source): object {
    const { loginStore } = this.rootStore
    // Convert legacy formulas to new ones
    let sourceV2 = source.replace("{", "").replace("}", "").replace("$", "")

    return (sourceV2 === Sources.DATA)
      ? loginStore.data
      : (sourceV2 === Sources.MEDIA)
        ? loginStore.mediaLibrary
        : (sourceV2 === Sources.CONTEXT)
          ? this.selectContextData()
          : loginStore.data
  }

  @action
  selectContextData() {
    const { dataStore } = this.rootStore
    let data = {}
    data.context = []
    data.context.push(dataStore.contextData)
    
    return data
  }

  /**
   * Replace a constant defined in CONSTANTS with the actual value in the app.
   * Example: Replaces CURRENT_WORK_ORDER with the Work Order Id that is currently active
   * @param {string} formula 
   */
  replaceConstants(formula: string): string {

    const ERROR = "error while replacing a constant"

    // Convert legacy formulas to new ones
    let fomulaV2 = formula.replace("{", "").replace("}", "").replace("$", "")


    if (!_.isEmpty(fomulaV2)) {

      const { dataStore } = this.rootStore

      let currentWorkOrder = dataStore.getCurrentWorkOrder()

      let currentWorkOrderId = currentWorkOrder ? currentWorkOrder.Id : null

      if (fomulaV2.includes(Constants.CURRENT_WORK_ORDER)) {
        let newFormula = fomulaV2.replace(Constants.CURRENT_WORK_ORDER, currentWorkOrderId === null ? ERROR : currentWorkOrderId)

        return newFormula
      }

      if (fomulaV2.includes("CURRENT_RELATED_WORK_ORDER")) {
        const { currentRelatedWorkOrder } = this.rootStore.dataStore
        let newFormula = fomulaV2.replace("CURRENT_RELATED_WORK_ORDER", currentRelatedWorkOrder === null ? ERROR : currentRelatedWorkOrder)
        return newFormula
      }

      if (fomulaV2.includes(Constants.CURRENT_ID)) {
        const { currentId } = this.rootStore.dataStore
        return fomulaV2.replace(Constants.CURRENT_ID, currentId === null ? ERROR : currentId)
      }

      if (fomulaV2.includes(Constants.CURRENT_IP_ID)) {
        const { installedProductId } = this.rootStore.dataStore
        return fomulaV2.replace(Constants.CURRENT_IP_ID, installedProductId === null ? ERROR : installedProductId)
      }
    }
    return fomulaV2


  }

  /**
      * Validates a string according to different regex depending on the type
      * @param value 
      * @param type 
      */
  validateInput(value: string, type: string): InputValidationResponse {
    const { loginStore } = this.rootStore
    let numericSeparator = loginStore.getKeyboardNumericSeparator()
    var constraints = {
      email: {
        presence: false,
        format: {
          pattern: /^$|^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/,
          message: "^${Label.InvalidEmail}"
        }
      },
      number: {
        presence: false,
        format: {
          pattern: numericSeparator === "." ? /^$|^[+-]?[0-9]*\.[0-9]+|[+-]?[0-9]+/ : /^$|^[+-]?[0-9]*\,[0-9]+|[+-]?[0-9]+/,
          message: "^${Label.InvalidNumber}"
        }
      },
      double: {
        presence: false,
        format: {
          pattern: numericSeparator === "." ? /^$|^[+-]?[0-9]*\.[0-9]+|[+-]?[0-9]+/ : /^$|^[+-]?[0-9]*\,[0-9]+|[+-]?[0-9]+/,
          message: "^${Label.InvalidNumber}"
        }
      },
      currency: {
        presence: false,
        format: {
          pattern: numericSeparator === "." ? /^$|^[+-]?[0-9]*\.[0-9]+|[+-]?[0-9]+/ : /^$|^[+-]?[0-9]*\,[0-9]+|[+-]?[0-9]+/,
          message: "^${Label.InvalidNumber}"
        }
      },
      phone: {
        presence: false,
        format: {
          pattern: /^$|^[0-9]*$/,
          message: "^${Label.InvalidPhone}"
        }
      },
    }

    switch (type) {
      case "EMAIL": {
        return validate({ email: value }, constraints)
      }
      case "PHONE": {
        return validate({ phone: value }, constraints)
      }

      case "CURRENCY":
        {
          return validate({ currency: value }, constraints)
        }
      case "NUMBER":
      case "DOUBLE":
        {

          let v =  validate({ double: value }, constraints)

          // console.warn(value + " => "+  JSON.stringify(v))
          return v;
        }
    }
    // undefined means that the validation was successfull 
    return undefined
  }


  formulas = {

    calculateTotalTime: () => {
      const { loginStore } = this.rootStore
      if (loginStore.currentWorkOrderId) {


        // var ts_store = Ext.getStore('Timesheet_Lineitems');
        // var ts = ts_store.findRecordsAll(function (record) { return (record.get('FIELDBUDDY__Work_Order__c') == wo_id); });
        const WO = loginStore.getPrefixedTableName("Work_Order__c")
        const TSLI = loginStore.getPrefixedTableName("Timesheet_Lineitem__c")
        const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
        const TYPE = loginStore.getPrefixedFieldName("Type__c")
        const LOGGED_TIME = loginStore.getPrefixedFieldName("Logged_time__c")

        const ST = loginStore.getPrefixedFieldName("Start_Time__c")
        const END = loginStore.getPrefixedFieldName("End_Time__c")



        const ts = loginStore.data[TSLI].filter(x => x[WO] === loginStore.currentWorkOrderId && x[TYPE] === 'Servicebezoek')
        var i, item, diff, timeMilliSeconds = 0;

        for (i = 0; i < ts.length; i += 1) {
          item = ts[i];

          diff = new Date(item[END]).getTime() - new Date(item[ST]).getTime();
          if (diff >= 0) {
            timeMilliSeconds += diff;
          }
        }

        var seconds = Math.floor(timeMilliSeconds / 1000);
        // round up to nearest 15 minutes (FB-1248)
        seconds = seconds === 0 ? 0 : ((((seconds - 1) / 900) | 0) + 1) * 900;
        var hours = Math.floor(seconds / 3600);
        var mins = Math.floor((seconds - hours * 3600) / 60);
        return "Totaal gewerkte tijd: " + (hours + ':' + (mins > 9 ? mins : '0' + mins) + ' uur');
      }
    },
    calculateTotalCostsForPartsAndOtherExpenses: () => {
      const { loginStore } = this.rootStore

      if (loginStore.currentWorkOrderId) {
        const WO = loginStore.getPrefixedTableName("Work_Order__c")

        const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
        const TYPE = loginStore.getPrefixedFieldName("Type__c")

        const TOTAL_COST = "Total_Costs__c"
        const TAX_RATE = loginStore.getPrefixedFieldName("Tax_Rate__c")


        const wop = loginStore.data[WOLI]
          .filter(x => {
            return x[WO] === loginStore.currentWorkOrderId
              && (x[TYPE] === 'Part' || (x[TYPE] || "").toLowerCase() === 'Other Expense'.toLowerCase())
              && Number(x[TAX_RATE]) >= 0;
          });


        let w = _.sum(wop.map(x => Number(x[TOTAL_COST])));

        return _.round(Number(w), 2) + "";
      }

    }
  }

  helpers = {
    isLocationBased: function (input: any) {
      try {
        let keys = Object.keys(input).map(key => {
          if (key.includes("Template__c")) {
            return (input[key] === "Location")
          } else {
            return false
          }
        })
        return (keys.includes(true)) ? true : false
      } catch (error) {
        return false
      }
    },
    isServiceRequestBased: function (input: any) {
      try {
        let keys = Object.keys(input).map(key => {
          if (key.includes("Template__c")) {
            return (input[key] === "Location")
          } else {
            return false
          }
        })
        return (keys.includes(true)) ? false : true
      } catch (error) {
        return true
      }
    },
    and: function (inputA: any, inputB: any) {

      return inputA && inputB
    },
    or: function (inputA: any, inputB: any) {

      return inputA || inputB
    },
    neg: function (input: any) {
      return !input
    },
    isEmpty: function (input: any) {
      return _.isEmpty(input)
    },
    isNotEmpty: function (input: any) {
      if (input === "") return []
      return !_.isEmpty(input)
    },
    then: function (input: any, thenValue: any, elseValue: any) {
      if (input) {
        return thenValue
      } else {
        return elseValue
      }
    },
    contains: function (input: any, arg: any) {
      if (typeof input === "string") {
        return input.includes(arg)
      }

      if (Array.isArray(input)) {
        return input.some(x => x.includes(arg))

      }
      return false
    },
    isNot: function (input: any, arg: any) {
      if (typeof input === "string") {
        return input != arg
      }

      if (Array.isArray(input)) {
        return !_.isEqual(input, arg)
      }
      return false
    },
    equals: function (input: any, arg: any) {
      if (typeof input === "string") {
        return input === arg
      }

      if (Array.isArray(input)) {
        return _.isEqual(input, arg)
      }
      return false
    }
  }

  getConditionByAlias(alias: alias): singleCondition {
    const { loginStore } = this.rootStore
    const P = loginStore.getPrefix()
    let aliases: { [aliasName: string]: singleCondition }
    aliases = {
      "ACTIVE_WORK_ORDER_HAS_ATTACHMENTS": { source: Sources.MEDIA, formula: `[*Linked=CURRENT_WORK_ORDER]` },
      "ACTIVE_WORK_ORDER_HAS_ACTIVITIES": { formula: `${P}Work_Order_Line_Item__c[${P}Work_Order__c=CURRENT_WORK_ORDER&${P}Type__c=Activity|${P}Type__c=activity]` },
      "ACTIVE_WORK_ORDER_HAS_PARTS": { formula: `${P}Work_Order_Line_Item__c[${P}Work_Order__c=CURRENT_WORK_ORDER&${P}Type__c=Part|${P}Type__c=part]` },
      "ACTIVE_WORK_ORDER_HAS_OTHER_EXPENSES": { formula: `${P}Work_Order_Line_Item__c[${P}Work_Order__c=CURRENT_WORK_ORDER&${P}Type__c=Other Expense|${P}Type__c=Other expense|${P}Type__c=other expense]` },
      "ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS": { formula: `wydfdtadwdfa`, source: Sources.MEDIA, alias : "ACTIVE_WORK_ORDER_HAS_NONUPLOADED_ATTACHMENTS" }
    }
    if (aliases.hasOwnProperty(alias)) {
      return aliases[alias]
    }
      return { formula: "INCORRECT ALIAS" }
    }


  @action
  getValidationRules(sourceComponent: string) {
    const rs = this.rootStore.appsStore.getValidationRules(sourceComponent)
    if (rs.success) {
      console.warn("Formula: " + rs.validationRules.map((x: any) => { return x.Mobile_Page__r + " - " + x.Name }))
    }

  }

  _applyDataBindingAndMeta(aParamater : ParamType) : ParamDictionary {

    const { classicValidationRulesDataContext } = this.rootStore.dataStore

    let _dict : ParamDictionary = {
      key: aParamater.var,
      value: null
    };

    if (aParamater.table && aParamater.field) {
      const { field, table } = aParamater;
      
      if (field === "Eindstand_E_nacht_1_8_1__c") {
        //debugger
      }

      let metaResult = this.rootStore.metaStore.getFieldMeta(table, field)
      
      if (metaResult && metaResult.type === "success" && metaResult.data) {

        let meta = metaResult.data;

        const TYPE = meta.type && meta.type.toUpperCase()
        let _table = this._getTable( table)
        if (field === "Eindstand_E_nacht_1_8_1__c") {
          //debugger
        }
        
        if (_table.hasOwnProperty(field)) {
          
          switch (TYPE) {
            case "DATE":
              _dict.value = moment(_table[field]);
              break;
            case "DATETIME":
              const { utcOffset } = this.rootStore.localizeStore
              const timeZone = utcOffset / 60000
              _dict.value = moment(_table[field]).utcOffset(timeZone).format("YYYY-MM-DD HH:mm")
              break;
            case "DOUBLE":
              if (field === "Eindstand_E_nacht_1_8_1__c") {
               // debugger
              }
              try {
                let _v = _table[field]
                

                if (_v === "" || _v === null) {
                  if (aParamater.hasOwnProperty("defaultValue")) {
                    _dict.value = aParamater.defaultValue
                  } else {
                    _dict.value = Number(_v);
                  }

                } else {
                  _dict.value = Number(_v);
                }
                
              } catch (error) {
                
                _dict.value = 0
              }
              break;
            default:
              //console.warn(TYPE)
              
              _dict.value = _table[field];
              break;
          }
        } else {

          if (field === "Eindstand_E_nacht_1_8_1__c") {
            //debugger
          }

          if (aParamater.hasOwnProperty("defaultValue")) {
            _dict.value = aParamater.defaultValue
            if (field === "Eindstand_E_nacht_1_8_1__c") {
              //debugger
            }
          } else {
            switch (TYPE) {
              case "BOOLEAN":
                _dict.value = false
                break;
              case "DOUBLE":
                _dict.value = 0
                break;
              default:
                _dict.value = ""
                break;
            }
          }
          
          
        }
      } else {
        


        _dict.value = ""
      }
    }

    return _dict
  }

  private _getTable(table: string) {
    const {dataStore} = this.rootStore
    const { classicValidationRulesDataContext } = this.rootStore.dataStore

    return classicValidationRulesDataContext.hasOwnProperty(table) ?
      classicValidationRulesDataContext[table] : (dataStore.contextData.hasOwnProperty(table) ? dataStore.contextData[table] : {});
  }

  assertValidationRule(validationRule : ValidationRule) : boolean{
    let DEBUG_FORMULA_ID = "34 validatie Eindstand E nacht 1.8.1 verplicht veld"

    let _formula = ""
    try {
      
   
    const { formula, params } = validationRule.SwiftFormula
      let dict = [];
 
      
      for (let _param   of params ) {
        
        let _d = this._applyDataBindingAndMeta(_param);
        
        dict.push(_d);
      }
      
      if (validationRule.SwiftFormula.helpers) {
        const {helpers} = validationRule.SwiftFormula
        
        
        for (let helper of helpers) {
          this._executeHelper(helper, dict);
        }

      }
      
      _formula = _.clone(formula);
      
      for (let _dict of dict) {  
          
          if(!_dict.value){
            
            if (_dict.value === null) {
              _formula = _formula.split(_dict.key).join(" null ");
            }
            else if (_dict.value === false) {
              _formula = _formula.split(_dict.key).join("false");
            } else if (_dict.value === 0) {
              
              _formula = _formula.split(_dict.key).join(0);
            } else {
              
              _formula = _formula.split(_dict.key).join("");
            }
            
          } else{
            
            _formula = _formula.split(_dict.key).join(_dict.value);                
          }   
          
          
      }
      
      
      

      if (validationRule.Id === DEBUG_FORMULA_ID) {
        console.warn("Formula: " + _formula);
        
      }
      
      //1. true - means that it's wrong
      const isInValid = eval(_formula)
      if (validationRule.Id === DEBUG_FORMULA_ID) {
       // debugger
      }

      this.rootStore.loginStore._slackValidationRules(_formula, !isInValid, validationRule  )
      
      //2. therefore, return a negation of isInValid
      return !isInValid
    } catch (error) {
      if (validationRule.Id === DEBUG_FORMULA_ID) {
       // debugger
      }
      this.rootStore.loginStore._slackValidationRules(_formula,false, validationRule, true)
      if (validationRule.Id === DEBUG_FORMULA_ID) {
       // debugger
      }
      //is inValid
      return  false
    }
  }

 
  private _executeHelper(helper: SwiftHelperType, dict: {}[]) {
    
    if (helper.hasOwnProperty("helpers")) {
      
      for (let _helper of helper.helpers) {
        this._executeHelper(_helper, dict);
      }
      
    }
    
    const { method, select, from, where } = helper;

    let _whereValues = [];
    let _whereNotValues = [];

    for (let _value of _.clone(where.values)) {
      for (let _dict of dict) {
        _value = _value.split(_dict.key).join(_dict.value);
      }
      _whereValues.push(_value);
    }

    if (helper.whereNot) {
      const { whereNot } = helper;

      for (let _valueNot of _.clone(whereNot.values)) {
        for (let _dict of dict) {
          _valueNot = _valueNot.split(_dict.key).join(_dict.value);
        }
        _whereNotValues.push(_valueNot);
      }
    }

    let _whereObj = _.zipObject(where.fields, _whereValues);


    let _d = {};
    _d.key = helper["var"];

    switch (method) {
      case "_.findIndex":
        this._executeFindIndex(_whereObj, _d, helper, _whereNotValues);
        break;

      case "_.sum":
        this._executeSum(_whereObj, _d, helper, _whereNotValues);
        break;

      case "_.count":
        this._executeCount(_whereObj, _d, helper, _whereNotValues);
        break;

      case "_.first":
        this._executeFirst(_whereObj, _d, helper, _whereNotValues);
        break;

      case "isValidIBANNumber":
        this._executeIsValidIBANNumber(_whereObj, _d, helper, _whereNotValues);
        break;

        case "EqualPartsTime":
          this._executeEqualPartsTime(_whereObj, _d, helper, _whereNotValues);
          break;

    }





    dict.push(_d);
  }

  private _executeFindIndex( _whereObj: _.Dictionary<string>, _d: {}, helper: SwiftHelperType, _whereNotValues: string[]) {
    const {from} = helper

    let data =  this.rootStore.loginStore.data

    let foundIndex = _.findIndex(_.get(data, from), _whereObj);
    _d.value = foundIndex;

    if (helper.whereNot) {
      const { whereNot } = helper;
      

      if(_.includes(whereNot.fields, "FIELDBUDDY__Value__c")){
        if (foundIndex >= 0) {

          let aaa = _.get(data, from)
          
          
          if (aaa[foundIndex]) {
           
            if (!aaa[foundIndex].hasOwnProperty("FIELDBUDDY__Value__c")) {
              _d.value = -1;
            } else if (aaa[foundIndex]["FIELDBUDDY__Value__c"] === undefined || aaa[foundIndex]["FIELDBUDDY__Value__c"] === "" || aaa[foundIndex]["FIELDBUDDY__Value__c"] === null) {
              _d.value = -1;
            }
          }
        }

      }else{
        let _whereNotObj = _.zipObject(whereNot.fields, _whereNotValues);
      

        let foundNotIndex = _.findIndex(_.get(data, from), _whereNotObj);
        
        if (foundNotIndex === foundIndex) {
          _d.value = -1;
        }
      }
      
    }
  }

  private _executeSum(_whereObj: _.Dictionary<string>, _d: {}, helper: SwiftHelperType, _whereNotValues: string[]) {
    const { select, from } = helper

    let data = this.rootStore.loginStore.data


    let all1 = _.filter(_.get(data, from), _whereObj)
    if (_.isEmpty(all1)) {
      _d.value = 0;
      return;
    } else {
      _d.value = _.sum(all1.map(x => this._numberifyArguments(x, select)))
      
    }

    if (helper.whereNot) {
      const { whereNot } = helper;
      let _whereNotObj = _.zipObject(whereNot.fields, _whereNotValues);

      let all2 = _.filter(_.get(data, from), _whereNotObj);
      if (_.isEmpty(all2)) {
        _d.value = 0;
      } else {
        _d.value = _.sum(all2.map(x => this._numberifyArguments(x, select)))
        
      }

    }

    if(__DEV__){
      console.warn("_d " + _d.value)
    }
    
  }

  private _numberifyArguments(x: any, select: string): any {
    try {
      // let y =  x[select] || 0;
      
      // return y
      let _n = Number(x[select]);

      if(!isNaN(_n)){
        return _n
      }
    } catch (error) {
      //pass
    }
    return 0;
    
  }

  private _executeCount(_whereObj: _.Dictionary<string>, _d: {}, helper: SwiftHelperType, _whereNotValues: string[]) {
    const { select, from } = helper

    let data = this.rootStore.loginStore.data
    
    

    let all1 = _.filter(_.get(data, from), _whereObj)
    if (_.isEmpty(all1)) {
      _d.value = 0;
      return;
    } else {
      _d.value = (all1.map(x => this._numberifyArguments(x, select))).length
      
    }

    if (helper.whereNot) {
      const { whereNot } = helper;
      let _whereNotObj = _.zipObject(whereNot.fields, _whereNotValues);
      
      let all2 = _.filter(_.get(data, from), _whereNotObj);
      if (_.isEmpty(all2)) {
        _d.value = 0;
      } else {
        _d.value = (all2.map(x => this._numberifyArguments(x, select))).length
        
      }

    }
  }

  private _executeFirst(_whereObj: _.Dictionary<string>, _d: {}, helper: SwiftHelperType, _whereNotValues: string[]) {
    const { select, from } = helper

    let data = this.rootStore.loginStore.data
    
    

    let all1 = _.filter(_.get(data, from), _whereObj)
    if (_.isEmpty(all1)) {
      _d.value = null
      return;
    } else {
      _d.value = _.first((all1.map(x => x[select] || 0)))
      
    }

    if (helper.whereNot) {
      const { whereNot } = helper;
      let _whereNotObj = _.zipObject(whereNot.fields, _whereNotValues);

      let all2 = _.filter(_.get(data, from), _whereNotObj);
      if (_.isEmpty(all2)) {
        _d.value = null
      } else {
        _d.value = _.first((all2.map(x => x[select] || 0)))
        
      }

    }
  }

  isValidIBANNumber(input : string) {
    

    var CODE_LENGTHS = {
      AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
      CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
      FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
      HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
      LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
      MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
      RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
    };
    var iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
      code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
      digits;
    // check syntax and length
    if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
      return false;
    }
    // rearrange country code and check digits, and convert chars to ints
    digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
      return letter.charCodeAt(0) - 55;
    });
    // final check

    function mod97(string) {
      var checksum = string.slice(0, 2), fragment;
      for (var offset = 2; offset < string.length; offset += 7) {
        fragment = String(checksum) + string.substring(offset, offset + 7);
        checksum = parseInt(fragment, 10) % 97;
      }
      return checksum;
    }

    return mod97(digits) === 1;
  }
    

  private _executeIsValidIBANNumber(_whereObj: _.Dictionary<string>, _d: {}, helper: SwiftHelperType, _whereNotValues: string[]) {
    const { select, from } = helper

    let data = this.rootStore.loginStore.data
    
    

    let all1 = _.filter(_.get(data, from), _whereObj)
    if (_.isEmpty(all1)) {
      _d.value = false
      return;
    } else {
      
      _d.value = this.isValidIBANNumber(_.first((all1.map(x => x[select] || ""))))
      
    }

    if (helper.whereNot) {
      const { whereNot } = helper;
      let _whereNotObj = _.zipObject(whereNot.fields, _whereNotValues);

      let all2 = _.filter(_.get(data, from), _whereNotObj);
      if (_.isEmpty(all2)) {
        _d.value = false
      } else {
        _d.value = this.isValidIBANNumber(_.first((all2.map(x => x[select] || ""))))
        
      }

    }
  }


  _equalLoggedTimeServiceTime(workOrderId: string) {
    try {

      const { loginStore } = this.rootStore
      // var ts_store = Ext.getStore('Timesheet_Lineitems');
      // var ts = ts_store.findRecordsAll(function (record) { return (record.get('FIELDBUDDY__Work_Order__c') == wo_id); });
      const WO = loginStore.getPrefixedTableName("Work_Order__c")
      const TSLI = loginStore.getPrefixedTableName("Timesheet_Lineitem__c")
      const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
      const TYPE = loginStore.getPrefixedFieldName("Type__c")
      const LOGGED_TIME = loginStore.getPrefixedFieldName("Logged_time__c")

      const ts = loginStore.data[TSLI].filter(x => x[WO] === workOrderId)

      // var wop_store = Ext.getStore('Work_Order_Parts');
      // var wop = wop_store.findRecordsAll(function (record) { return (record.get('FIELDBUDDY__Work_Order__c') == wo_id); });
      const wop = loginStore.data[WOLI].filter(x => { return x[WO] === workOrderId && x[TYPE] === 'Part'; })

      var total_time_parts = 0;
      for (var i = 0; i < wop.length; i++) {

        if (wop[i].hasOwnProperty(LOGGED_TIME)) {

          if (wop[i][LOGGED_TIME] !== null) {

            total_time_parts += Number(wop[i][LOGGED_TIME]);
          }
        }
      }


      const ST = loginStore.getPrefixedFieldName("Start_Time__c")
      const END = loginStore.getPrefixedFieldName("End_Time__c")

      var total_time_ts = 0;
      for (var i = 0; i < ts.length; i++) {
        if (ts[i].hasOwnProperty(ST) && ts[i].hasOwnProperty(END) && ts[i].hasOwnProperty(TYPE)) {
          if (ts[i][ST] != null && ts[i][END] != null && ts[i][TYPE] == 'Servicebezoek') {


            var minutes = (new Date(ts[i][END]) - new Date(ts[i][ST])) / 60000;

            total_time_ts += minutes;
          }
        }
      }

      console.warn('Formula: ts: ' + total_time_ts + "; p: " + total_time_parts)
      if (total_time_ts > 0 && total_time_ts < total_time_parts) {
        return false;
      } else {
        return true;
      }
    } catch (error) {
      console.warn("Formula: " + error)
    }
  }

  private _executeEqualPartsTime(_whereObj: _.Dictionary<string>, _d: {}, helper: SwiftHelperType, _whereNotValues: string[]) {
    const { select, from } = helper

    let data = this.rootStore.loginStore.data
    
    

    let all1 = _.filter(_.get(data, from), _whereObj)
    if (_.isEmpty(all1)) {
      _d.value = false
      return;
    } else {
      
      _d.value = this._equalLoggedTimeServiceTime(_.first((all1.map(x => x[select] || ""))))
      
    }

    if (helper.whereNot) {
      const { whereNot } = helper;
      let _whereNotObj = _.zipObject(whereNot.fields, _whereNotValues);

      let all2 = _.filter(_.get(data, from), _whereNotObj);
      if (_.isEmpty(all2)) {
        _d.value = false
      } else {
        _d.value = this._equalLoggedTimeServiceTime(_.first((all2.map(x => x[select] || ""))))
        
      }

    }
  }

  @action
  passValidationRules(sourceComponent: string, sourceComponentType: string = "") {
    const rs = this.rootStore.appsStore.getValidationRules(sourceComponent, sourceComponentType)
    if (rs.success) {
      console.warn("Formula: " + rs.validationRules.map((x: any) => { return x.Mobile_Page__r + " - " + x.Name }))
      let _errors = []
      let _warnings = []
      for (let _validation  of rs.validationRules) {
        /*
{
        "Id": "a0w0J00000Aad83QAB",
        "Name": "Arbeidsloon (Zon- en feestdagen) niet aanpasbaar",
        "Code__c": null,
        "Error_Message__c": "Het is niet toegestaan om het bedrag aan te passen. Het standaard tarief voor Arbeidsloon (Zon- en feestdagen) is €31,00",
        "Formula__c": "=AND(data['FIELDBUDDY__Unit_Price_Including_Tax_input__c'] != '31',data['S_SMScode__c']=='(Zon- en feestdagen)')",
        
        "Mobile_Page__r": "Edit Work Order Other Expenses"
    },
*/
/*
{
            "Id": "a0w0J00000Aad7tQAB",
            "Name": "TSLI start KM",
            "Code__c": null,
            "Error_Message__c": "De start KM stand kan niet negatief zijn",
            "Formula__c": "data['FIELDBUDDY__Type__c'] == 'Reizen' &&  NUMBERVALUE(data['FIELDBUDDY__Odometer_Start__c']) < 0",
            
            "Mobile_Page__r": "Timesheet Lineitem"
        },
*/

        let isValid = this.assertValidationRule(_validation);
       
        if (!isValid) {
          if (_validation.hasOwnProperty("Error_Message__c")) {
            _errors.push(_validation.Error_Message__c)
          }
          if (_validation.hasOwnProperty("Warning_Message__c")) {
            _warnings.push(_validation.Warning_Message__c)
          }
        }

      }

     
      if (!_.isEmpty(_errors) || !_.isEmpty(_warnings)) {
        
        return {
          success: false,
          
          errorMessage: _errors,
          warningMessage: _warnings
        }
      }
    }


    return {
      success: true
      
    }
  }

}
