import React, { Component } from 'react'
import { inject, observer } from "mobx-react/native"
import { NavigationContainer } from '@react-navigation/native';
import { DrawerNavigator } from './Drawer';
import { LoginStackNavigator } from './LoginStack';
import { ReleaseNotesStack } from './ReleaseNotesStack';
import { EmergencyStack } from './EmergencyStack';
import { HardSyncStack } from './HardSyncStack';
import _ from 'lodash';
import Loading from '../storybook/stories/LoadingScreen';
import { View, LogBox } from 'react-native';
interface Props {
  rootStore?: any 
}

interface State {

}

@inject('rootStore')
@observer
class RootNavigation extends Component<Props,State> {
 
  render2() {

  }
  render() {
    const { loginStore, metaStore, settingsStore } = this.props.rootStore
    const { isLoggedIn, isEmergencyAsync, isHardSyncing2 } = loginStore
    const { showReleaseNotes } = settingsStore.rules.otherDefaults
    
    const seenReleaseNotes = loginStore.tryToShowReleaseNotes()

    if (isEmergencyAsync) {
      return (
        <NavigationContainer>
          <>
            <ConfigurationProvider data={loginStore.getUserConfig} >
              <MetaDataProvider data={metaStore.meta}>
                <DataProvider data={loginStore.getData}>
                  <EmergencyStack />
                </DataProvider>
              </MetaDataProvider>
            </ConfigurationProvider>
          </>
        </NavigationContainer>
      )
    }

    if (isHardSyncing2) {
      return (
        <NavigationContainer>
          <>

            <HardSyncStack />

          </>
        </NavigationContainer>
      )
    }

    return (
      <NavigationContainer>
              {isLoggedIn ? (
                
                (!seenReleaseNotes && showReleaseNotes)
                  ? (
                    <>
                      <ConfigurationProvider data={loginStore.getUserConfig} >
                        <MetaDataProvider data={metaStore.meta}>
                          <DataProvider data={loginStore.getData}>
                            <ReleaseNotesStack />
                          </DataProvider>
                        </MetaDataProvider>
                      </ConfigurationProvider>
                    </>
                  )
                  : (
                    <>
                      <ConfigurationProvider data={loginStore.getUserConfig} >
                        <MetaDataProvider data={metaStore.meta}>
                          <DataProvider data={loginStore.getData}>
                          <DrawerNavigator/>
                          </DataProvider>
                        </MetaDataProvider>
                      </ConfigurationProvider>
                    </>
                  )
              ) : (
                <>
                  <LoginStackNavigator />
                </>
              )}
      </NavigationContainer>
    );
  }
}

interface ProviderProp {
  data:any
  children:JSX.Element
}

const ConfigurationProvider = ({children,data}:ProviderProp) => {
  return (_.isEmpty(data)) 
    ? <Loading />
    : children

}

const MetaDataProvider = ({children,data}:ProviderProp) => {
  return (_.isEmpty(data)) 
    ? <Loading />
    : children
}

const DataProvider = ({children,data}:ProviderProp) => {
  return (_.isEmpty(data)) 
    ? <Loading />
    : children
}




export default RootNavigation;

