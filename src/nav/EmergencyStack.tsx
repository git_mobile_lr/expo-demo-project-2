import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'


import EmergencyScreen from '../screens/EmergencyScreen';

export function EmergencyStack() {

  const EmergencyStack = createStackNavigator()

  return (
    <EmergencyStack.Navigator

      screenOptions={{
        gestureEnabled: false,
        headerShown: false,
      }}
  >
      <EmergencyStack.Screen name="Emergency" component={EmergencyScreen} />
      
    </EmergencyStack.Navigator>
  )
}
