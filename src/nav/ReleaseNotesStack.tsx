import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'

import FSwiper from '../screens/widgets/FSwiper/FSwiper';
import StartReleaseNotesScreen from '../screens/StartReleaseNotesScreen';

export function ReleaseNotesStack() {

  const ReleaseNotesStack = createStackNavigator()

  return (
    <ReleaseNotesStack.Navigator

      screenOptions={{
        gestureEnabled: false,
        headerShown: false,
      }}
  >
      <ReleaseNotesStack.Screen name="StartReleaseNotes" component={StartReleaseNotesScreen} />
      <ReleaseNotesStack.Screen name="ReleaseNotesSwiper" component={FSwiper} />
    </ReleaseNotesStack.Navigator>
  )
}
