import React, { useState, useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer'
import { View } from 'react-native';
import { MainStackNavigator } from './MainStack'
import ModalScreen from '../screens/ModalScreen/ModalScreen'
import DrawerScreen from '../screens/DrawerScreen'
import MultiAppDrawerScreen from '../screens/MultiAppDrawerScreen'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { useWindowDimensions } from 'react-native';
const Drawer = createDrawerNavigator();



export function DrawerNavigator(): JSX.Element {
  const dimensions = useWindowDimensions();
    const [isMounted, setIsMounted] = useState(false)
    const [width, setWidth] = useState(0);

    useEffect(() => {
      
        setWidth(wp('80%'));
        // setWidth(0.8 * dimensions.width);
        setIsMounted(true);
    });

    if (isMounted) {
      return (<Drawer.Navigator
        screenOptions={{
          drawerStyle: {
            backgroundColor: '#c6cbef',
            width: width
          },
          headerShown: false,
          drawerType: 'front'
                    
        }}
        detachInactiveScreens={false}
        drawerContent={() => <MultiAppDrawerScreen />}
      >
        <Drawer.Screen name="Home" component={MainStackNavigator} />
        <Drawer.Screen name="Modal" component={ModalScreen} />
      </Drawer.Navigator>
      );
        
      
        
    }

    return (<View/>);
}