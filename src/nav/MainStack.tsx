import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import WorkOrderDetailsScreen from '../screens/WorkOrderDetailsScreen';
import BarcodeScannerScreen from '../screens/BarcodeScannerScreen';
import CreateWorkOrderScreen from '../screens/CreateWorkOrderScreen';
import DemoFormScreen from '../screens/DemoFormScreen';
import DeveloperModeScreen from '../screens/DeveloperModeScreen';
import DrawerSettingsScreen from '../screens/DrawerSettingsScreen';
import LineItemInsertScreen from '../screens/LineItemInsertScreen';

import LineItemUpsertScreen from '../screens/LineItemUpsertScreen';
import MultiItemsScreen from '../screens/MultiItemsScreen';
import EditScreen from '../screens/EditScreen';
import EditPlusScreen from '../screens/EditPlusScreen';
import WorkOrderClosurePlusScreen from '../screens/WorkOrderClosurePlusScreen';
import InsertScreen from '../screens/InsertScreen';
import InsertPlusScreen from '../screens/InsertPlusScreen';
import ChecklistV15UpsertScreen from '../screens/CheckListUpsertScreen/ChecklistV15UpsertScreen';
import ReAuthScreen from '../screens/ReAuthScreen';
import TimeRegistrationDashboardScreen from '../screens/TimeRegistrationDashboardScreen/TimeRegistrationDashboardScreen'
import TimeSheetInsertEditScreen from '../screens/TimeSheetInsertEditScreen'
import ErrorScreen from '../screens/ErrorScreen';
import SearchScreen from '../screens/SearchScreen';
import TestScreen from '../screens/TestScreen';
import WorkOrderClosedScreen from '../screens/WorkOrderClosedScreen';
import BackFetch from '../screens/BackFetch';
import UILab from '../screens/UILab';
import WorkOrderClosureScreen from '../screens/WorkOrderClosureScreen';
import WorkOrderSummaryScreen from '../screens/WorkOrderSummaryScreen';
import WorkOrderPropertiesScreen from '../screens/WorkOrderPropertiesScreen';
import WorkOrdersScreen from '../screens/WorkOrderScreen/WorkOrdersScreen';
import WorkOrderItemsScreen from '../screens/WorkOrderItemsScreen';
import WorkOrderItemRelationScreen from '../screens/WorkOrderItemRelation/WorkOrderItemRelationScreen';
import InstalledProductCreationScreen from '../screens/InstalledProductCreation/InstalledProductCreationScreen';
import InstalledProductRelationScreen from '../screens/InstalledProductRelation/InstalledProductRelationScreen';
import LoadingScreen from '../screens/LoadingScreen';
import NotFoundScreen from '../screens/NotFoundScreen';
import SignatureScreen from '../screens/SignatureScreen'
import MapViewCarouselScreen from '../screens/MapViewCarouselScreen';

const MainStack = createStackNavigator()

export function MainStackNavigator() {

    

    return (
        <MainStack.Navigator

            screenOptions={{
                gestureEnabled: false,
                headerShown: false,

                
            }}
        >
            <MainStack.Screen name="WorkOrders" component={WorkOrdersScreen} />
            <MainStack.Screen name='MapViewCarousel' component={MapViewCarouselScreen} />
            <MainStack.Screen name="Loading" component={LoadingScreen} />
            <MainStack.Screen name="Error" component={ErrorScreen} />
            <MainStack.Screen name="ReAuth" component={ReAuthScreen} />
            <MainStack.Screen name="TimeRegistrationDashboard" component={TimeRegistrationDashboardScreen} />
            <MainStack.Screen name="TimeSheetInsertEdit" component={TimeSheetInsertEditScreen} />
            <MainStack.Screen name="WorkOrderDetails" component={WorkOrderDetailsScreen} />
            <MainStack.Screen name="WorkOrderClosing" component={WorkOrderClosureScreen} />
            <MainStack.Screen name="WorkOrderClosure" component={WorkOrderClosureScreen} />
            <MainStack.Screen name="WorkOrderSummary" component={WorkOrderSummaryScreen} />
            <MainStack.Screen name="WorkOrderClosed" component={WorkOrderClosedScreen} />
            <MainStack.Screen name="WorkOrderItems" component={WorkOrderItemsScreen} />
            <MainStack.Screen name="ChecklistV15Upsert" component={ChecklistV15UpsertScreen} />
            <MainStack.Screen name="WorkOrderItemRelation" component={WorkOrderItemRelationScreen} />
            <MainStack.Screen name="BarcodeScanner" component={BarcodeScannerScreen} />
            <MainStack.Screen name="LineItemInsert" component={LineItemInsertScreen} />
            
            <MainStack.Screen name="LineItemUpsert" component={LineItemUpsertScreen} />
            <MainStack.Screen name="DetailsWithTabs" component={MultiItemsScreen} />
            <MainStack.Screen name="MultiItems" component={MultiItemsScreen} />
            <MainStack.Screen name="DeveloperMode" component={DeveloperModeScreen} />
            <MainStack.Screen name="Settings" component={DrawerSettingsScreen} />
            <MainStack.Screen name="Edit" component={EditScreen} />
            <MainStack.Screen name="EditPlus" component={EditPlusScreen} />
            <MainStack.Screen name="InsertPlus" component={InsertPlusScreen} />
            <MainStack.Screen name="Insert" component={InsertScreen} />
            <MainStack.Screen name="Search" component={SearchScreen} />
            <MainStack.Screen name="CreateWorkOrder" component={CreateWorkOrderScreen} />
            <MainStack.Screen name="Test" component={TestScreen} />
            <MainStack.Screen name="InstalledProductCreation" component={InstalledProductCreationScreen} />
            <MainStack.Screen name="InstalledProductRelation" component={InstalledProductRelationScreen} />
            <MainStack.Screen name="WorkOrderClosurePlus" component={WorkOrderClosurePlusScreen} />
            <MainStack.Screen name="NotFound" component={NotFoundScreen} />
            <MainStack.Screen name="Signature" component={SignatureScreen} />


        </MainStack.Navigator>
    )
}