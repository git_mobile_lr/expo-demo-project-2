import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import LoginScreen from '../screens/LoginScreen'
import LoadingScreen from '../screens/LoadingScreen'
import ErrorScreen from '../screens/ErrorScreen'
import ReAuthScreen from '../screens/ReAuthScreen'
import NotFoundScreen from '../screens/NotFoundScreen'
import BackFetch from '../screens/BackFetch';

export function LoginStackNavigator(){

    const LoginStack = createStackNavigator()

    return (
        <LoginStack.Navigator
        screenOptions={{
            headerShown: false
          }}
        > 
            {/* <LoginStack.Screen name="BackFetch" component={BackFetch} /> */}
            <LoginStack.Screen name="Login" component={LoginScreen} />
            <LoginStack.Screen name="Loading" component={LoadingScreen} />
            <LoginStack.Screen name="Error" component={ErrorScreen} />
            <LoginStack.Screen name="ReAuth" component={ReAuthScreen} />
            <LoginStack.Screen name="NotFound" component={NotFoundScreen} />
        </LoginStack.Navigator>
    )
}