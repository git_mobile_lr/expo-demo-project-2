import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'

import HardSyncScreen from '../screens/HardSyncScreen';

export function HardSyncStack() {

  const HardSyncStack = createStackNavigator()

  return (
    <HardSyncStack.Navigator
      screenOptions={{
        gestureEnabled: false,
        headerShown: false,
      }}
    >
      <HardSyncStack.Screen name="HardSync" component={HardSyncScreen} />

    </HardSyncStack.Navigator>
  )
}