import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
  container:{    
      backgroundColor: '#E9EEF1',
      height: '100%',
      flex: 1,
      marginTop: 1,
      marginBottom: 20
  },
  footerStyle: {
    backgroundColor: "#00AAFF",
    shadowRadius: 2,
    shadowColor: "rgba(0,0,0,0.14)"
  },
  cardStyle: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#DDD',
    marginTop: 0
  },
  cardHeaderStyle: {
    backgroundColor: '#00AAFF',
    height: 29
  },
  cardViewStyle: {
    backgroundColor: 'transparent',
    flex: 1,
    flexDirection: 'row',
    marginBottom: 3
  },
  cardViewHeadlineStyle: {
    paddingTop: 10,
    paddingLeft: 10,
    marginBottom: 5,
    backgroundColor: 'transparent',
    flex: 1,
    flexDirection: 'row'
  },
  cardViewHeadlineTextStyle: {
    fontFamily: 'Roboto_medium',
    fontSize: 13,
    paddingLeft: 5
  },
  cardHeaderTextStyle: {
    fontFamily: 'Roboto_medium',
    fontSize: 13,
    backgroundColor: '#00AAFF',
    color: '#FFFFFF',
    letterSpacing: 0,
    height: 29,
    paddingTop: 5,
    alignItems: 'center'
  },
  cardHeaderIconStyle: {
    fontFamily: 'Ionicons',
    fontSize: 14,
    backgroundColor: '#00AAFF',
    color: '#FFFFFF',
    fontWeight: '500'
  },
  cardHeaderIconRightStyle: {
    fontFamily: 'Ionicons',
    fontSize: 14,
    backgroundColor: '#00AAFF',
    color: '#FFFFFF',
    fontWeight: '500',
    paddingLeft: 20
  },
  cardBodyStyle: {
    backgroundColor: 'transparent',
    height: 162
  },
  headlineStyle: {
    fontFamily: 'Roboto',
    fontSize: 13,
    color: 'rgba(13,55,84, 0.87)',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10
  },
  cardElemStyle: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderWidth: 0
  },
  cardElemHeaderStyle: {
    fontFamily: 'Roboto',
    fontSize: 13,
    backgroundColor: '#FFFFFF',
    color: '#5384A6',
    letterSpacing: 0,
    height: 18,
    alignItems: 'center'
  },
  cardElemBodyStyle: {
    fontFamily: 'Roboto',
    fontSize: 13,
    backgroundColor: '#FFFFFF',
    color: '#000',
    letterSpacing: 0,
    height: 18,
    alignItems: 'center'
  },
  cardElemHeaderIconStyle: {
    fontFamily: 'Ionicons',
    fontSize: 14,
    backgroundColor: '#FFFFFF',
    color: '#5384A6',
    fontWeight: '500'
  },
  showMoreSeparatorStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop: 10,
    height: 45
  },
  showMoreStyle: {
    fontFamily: "Roboto_medium",
    fontSize: 10,
    paddingTop: 5,
    color: "#00AAFF",
    letterSpacing: 0.77
  },
  serviceItem: {
    /* Background: */
    flex: 1,
    flexDirection: 'row',
    borderColor: 'rgba(0,0,0,0.12)',
    borderWidth: 0.5,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
    marginHorizontal: 10,
    marginVertical: 3,
    height: 50,

    alignItems: 'center',
    justifyContent: 'center'
  },
  row: {
    marginHorizontal: 5
  },
  col: {

    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    flexDirection: 'column'

  },
  serviceItemText: {
    /* Background: */

    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#40505A',
    letterSpacing: 0,
    paddingLeft: 5
  },
  addPartStyle: {
    fontFamily: 'Ionicons',
    fontSize: 34,
    fontWeight: '500',
    color: '#00AAFF',
    letterSpacing: -0.53,
    alignSelf: 'flex-end',
    marginRight: 5,
    marginTop: 7
  },
  centering: {
    marginVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1000
  },
  titleStyle: {
    marginLeft: 10,
    paddingLeft: 10,
    fontFamily: 'Roboto_medium',
    fontSize: 16,
    color: 'rgba(13,55,84,0.87)',
    letterSpacing: 0,
    lineHeight: 21
  },
  summaryStyle: {
    flex: 8,
    fontFamily: "Roboto_medium",
    fontSize: 12,
    color: "#0D3754",
    letterSpacing: 0.79,
    alignItems: 'flex-start',
    marginVertical: 0,
    marginLeft: 15
  },
  summarySubtotalStyle: {
    flex: 8,
    fontFamily: "Roboto_medium",
    fontSize: 16,
    color: "#5384A6",
    letterSpacing: 0,
    lineHeight: 20,
    alignItems: 'flex-start',
    marginVertical: 0,
    marginLeft: 15
  },
  sectionStyle: {
    flex: 1,

    borderRadius: 18,
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#5384A6',
    letterSpacing: 0,
    textAlign: 'center',
    marginVertical: 10
  },
  sectionHeaderTextStyle: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#5384A6',
    letterSpacing: 0,
    textAlign: 'center'
  }
});

const tabletStyles = StyleSheet.create({
  titleStyle: {
    marginLeft: 10,
    paddingLeft: 10,
    fontFamily: 'Roboto_medium',
    fontSize: 20,
    color: 'rgba(13,55,84,0.87)',
    letterSpacing: 0,
    lineHeight: 25
  },
  summaryStyle: {
    flex: 8,
    fontFamily: "Roboto_medium",
    fontSize: 20,
    color: "#0D3754",
    letterSpacing: 0.79,
    alignItems: 'flex-start',
    marginVertical: 0,
    marginLeft: 15
  },
  summarySubtotalStyle: {
    flex: 8,
    fontFamily: "Roboto_medium",
    fontSize: 20,
    color: "#5384A6",
    letterSpacing: 0,
    lineHeight: 20,
    alignItems: 'flex-start',
    marginVertical: 0,
    marginLeft: 15
  }
});

module.exports = {
  
  styles,
  tabletStyles
}