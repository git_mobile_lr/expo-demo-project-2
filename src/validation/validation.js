import validate from 'validate.js'
import jsonQuery from 'json-query'
import _ from 'lodash'


/**
 * @param {object} data 
 * @param {string} formula 
 * @param {object} context
 */
export default function valid(data, formula, context) {

  var helpers = {
    isLocationBased: function (input) {
      try {
        keys = Object.keys(input).map(key => {
          if (key.includes("Template__c")) {
            return (input[key] === "Location")
          } else {
            return false
          }
        })
        return (keys.includes(true)) ? true : false
      } catch (error) {
        return false
      }
    },
    isServiceRequestBased: function (input) {
      try {
        keys = Object.keys(input).map(key => {
          if (key.includes("Template__c")) {
            return (input[key] === "Location")
          } else {
            return false
          }
        })
        return (keys.includes(true)) ? false : true
      } catch (error) {
        return true
      }

    },
    and: function (inputA, inputB) {

      return inputA && inputB
    },
    or: function (inputA, inputB) {

      return inputA || inputB
    },
    neg: function (input) {
      return !input
    },
    isEmpty: function (input) {

      return _.isEmpty(input)
    },
    isNotEmpty: function (input) {

      return !_.isEmpty(input)
    },
    then: function (input, thenValue, elseValue) {
      if (input) {
        return thenValue
      } else {
        return elseValue
      }
    }
  }

  if (_.isEmpty(formula)) return false
  if (context === 'privileges') {
    result = jsonQuery(formula, { data: data, locals: helpers }).value
    return result
  }
  result = jsonQuery(formula, { data: data, locals: helpers }).value

  if (typeof result === 'boolean') {
    return result
  } else {
    return !_.isEmpty(result)
  }
}

// export function validateInput(value,type){
//   var constraints = {
//     email:{
//       presence:false,
//       format: {
//         pattern:  /^$|^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
//         message: "^${Label.InvalidEmail}"
//       }
//     },
//     double:{
//       presence:false,
//       format: {
//         pattern:  /[0-9]*\.[0-9]+|[0-9]+/,
//         message: "^${Label.InvalidNumber}"
//       }
//     },
//     phone:{
//       presence:false,
//       format: {
//         pattern:  /^[0-9]*$/,
//         message: "^${Label.InvalidPhone}"
//       }
//     },
//   }

//   switch (type){
//     case "EMAIL":{
//       return validate({email:value},constraints)
//     }
//     case "PHONE":{
//       return validate({phone:value},constraints)
//     }
//     case "DOUBLE":{
//       return validate({double:value},constraints)
//     }
//   }
// }
