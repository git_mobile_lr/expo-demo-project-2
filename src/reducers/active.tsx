import { ReduxActions } from '../types'

export default (state = {}, action: any) => {
    try {
        switch (action.type) {
            case ReduxActions.OPEN_WORK_ORDER:
                return {
                    ...state,
                    activeWorkOrder: action.payload
                }
            default:
                return state
        }
    } catch (ex) {

    }

    return state;
}

