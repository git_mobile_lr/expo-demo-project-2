import { ReduxActions , SalesForcePlatformState} from '../types';

const initialState = {
    data : [],
    platformStatus : []
}

export default (state = initialState, action: any) => {
    try {
        switch (action.type) {
            case ReduxActions.SET_DATA:
                return {
                    ...state,
                    data: action.payload
                }
            case ReduxActions.SET_PLATFORM_STATUS:
                return {
                    ...state,
                    platformStatus: action.payload
                }
            default:
                return state
        }
    } catch (ex) {

    }
    return state;

}

