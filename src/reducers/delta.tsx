import { ReduxActions } from '../types';
import moment from 'moment';
import { inject, observer } from "mobx-react/native";

const HUGE_BACKUP =  require('../ui_config/backups/anno_backup_data.json')

const initialState = {
    history: [],
    delta: []
}


export default (state = initialState, action: any) => {
    try {
        switch (action.type) {
            case ReduxActions.SET_DELTA:
                return action.payload ? action.payload : state
            case ReduxActions.SET_CHANGES_HISTORY:
                return {
                    ...state,
                    history: action.payload
                }
            case ReduxActions.CLEAR_CHANGES_HISTORY:
                // console.warn("CLEAR_CHANGES_HISTORY")
                return {
                    ...state,
                    history: []
                }

                case ReduxActions.FAKE_CHANGES_HISTORY:
                    
                    return {
                        ...state,
                        history: [...state.history, ...HUGE_BACKUP.changesHistory]
                    }    
                    
                    // return {
                    //     ...state,
                    //     history: HUGE_BACKUP.changesHistory
                    // }    
            case ReduxActions.SYNC:

                // Remove the first value if the changes history reached the configuration limit
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                const sync = {
                    "changeType": "SYNC",
                    ...action.payload,
                    "timestamp": moment()
                }
                return {
                    ...state,
                    history: [...state.history, sync]
                }

            case ReduxActions.SYNC_RESULT:

                // Remove the first value if the changes history reached the configuration limit
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                const sync_result = {
                    "changeType": ReduxActions.SYNC_RESULT,
                    "syncId": action.payload.syncId,
                    "syncResult": action.payload.syncResult,
                    "timestamp": moment()
                }
                return {
                    ...state,
                    history: [...state.history, sync_result]
                }

            case ReduxActions.FIRST_AID_STOP_STOPWATCH:

                // Remove the first value if the changes history reached the configuration limit
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                const firstAidStopStopwatch = {
                    "changeType": ReduxActions.FIRST_AID_STOP_STOPWATCH,
                    timerWO: action.payload.timerWO,
                    timerTSLI: action.payload.timerTSLI,
                    "timestamp": moment()
                }
                return {
                    ...state,
                    history: [...state.history, firstAidStopStopwatch]
                }   
            case ReduxActions.FIRST_AID_REFRESH_APP:

                // Remove the first value if the changes history reached the configuration limit
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                const firstAidRefreshApp = {
                    "changeType": ReduxActions.FIRST_AID_REFRESH_APP,                    
                    "timestamp": moment()
                }
                return {
                    ...state,
                    history: [...state.history, firstAidRefreshApp]
                }  
                
            case ReduxActions.FIRST_AID_SEND_BACKUP:

                // Remove the first value if the changes history reached the configuration limit
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                let firstAidSendBackup = {
                    "changeType": ReduxActions.FIRST_AID_SEND_BACKUP,

                    "timestamp": moment()
                }
                return {
                    ...state,
                    history: [...state.history, firstAidSendBackup]
                } 

            case ReduxActions.STOPWATCH_STARTED:

                
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                const stopwatchStarted = {
                    "changeType": ReduxActions.STOPWATCH_STARTED,
                    timerWO: action.payload.timerWO,
                    timerTSLI: action.payload.timerTSLI,
                    "timestamp": moment()
                }
                return {
                    ...state,
                    history: [...state.history, stopwatchStarted]
                }
            case ReduxActions.STOPWATCH_STOPPED:

                
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                let stopwatchStopped = {
                    "changeType": ReduxActions.STOPWATCH_STOPPED,
                    timerWO: action.payload.timerWO,
                    timerTSLI: action.payload.timerTSLI,
                    "timestamp": moment()
                }

                if (action.payload.triggeredBy !== null) {
                    stopwatchStopped["triggeredBy"] = action.payload.triggeredBy
                }


                return {
                    ...state,
                    history: [...state.history, stopwatchStopped]
                }

            case ReduxActions.STOPWATCH_REFERENCE_UPDATE:


                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                const stopwatchReference = {
                    "changeType": ReduxActions.STOPWATCH_REFERENCE_UPDATE,

                    timerWO: action.payload.timerWO,
                    originalTimerTSLI: action.payload.originalTimerTSLI,
                    timerTSLI: action.payload.timerTSLI,
                    "timestamp": moment()
                }
                return {
                    ...state,
                    history: [...state.history, stopwatchReference]
                }

            case ReduxActions.CLOSURE_STATUS_RESET:


                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                let _closureStatusPayload = {
                    "changeType": ReduxActions.CLOSURE_STATUS_RESET,
                    woId: action.payload.woId,
                    closureStatus: action.payload.closureStatus || null,
                    "timestamp": moment()
                }

                if (action.payload.triggeredBy !== null) {
                    _closureStatusPayload["triggeredBy"] = action.payload.triggeredBy
                }


                return {
                    ...state,
                    history: [...state.history, _closureStatusPayload]
                }    

            case ReduxActions.STOPWATCH_ENDTIME_ON_HARD_RESET:


                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                let _stopwatchEndtimeOnHardReset = {
                    "changeType": ReduxActions.STOPWATCH_ENDTIME_ON_HARD_RESET,
                    timerWO: action.payload.timerWO,
                    timerTSLI: action.payload.timerTSLI,
                    "timestamp": moment()
                }

                if (action.payload.triggeredBy !== null) {
                    _stopwatchEndtimeOnHardReset["triggeredBy"] = action.payload.triggeredBy
                }


                return {
                    ...state,
                    history: [...state.history, _stopwatchEndtimeOnHardReset]
                }

            case ReduxActions.SESSION_UUID_REFRESH_APP:

                // Remove the first value if the changes history reached the configuration limit
                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {
//pass
                }
                const sessionUuidRefreshApp = {
                    "changeType": ReduxActions.SESSION_UUID_REFRESH_APP,
                    "timestamp": moment()
                }

                return {
                    ...state,
                    history: [...state.history, sessionUuidRefreshApp]
                }


            case ReduxActions.STOPWATCH_REACTIVATION_ON_HARD_RESET:


                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                let _stopwatchReactivationOnHardReset = {
                    "changeType": ReduxActions.STOPWATCH_REACTIVATION_ON_HARD_RESET,
                    timerWO: action.payload.timerWO,
                    timerTSLI: action.payload.timerTSLI,
                    "timestamp": moment()
                }

                if (action.payload.triggeredBy !== null) {
                    _stopwatchReactivationOnHardReset["triggeredBy"] = action.payload.triggeredBy
                }


                return {
                    ...state,
                    history: [...state.history, _stopwatchReactivationOnHardReset]
                }

           

            case ReduxActions.ADD_CHANGE:

                try {
                    if (state.history.length >= action.state.appSettings.logs.changesHistory) {
                        state.history.shift()
                    }
                } catch (error) {

                }
                // Remove the first value if the changes history reached the configuration limit

                return {
                    ...state,
                    history: [...state.history, action.payload]
                }
            default:
                return state
        }
    } catch (ex) {

    }

    return state;

}


