import { combineReducers } from 'redux';
import data from './data';
import navigation from './navigation';
import appSettings from './appSettings';
import delta from './delta';
import active from './active'

export default combineReducers({
    active,
    data,
    delta,
    navigation,
    appSettings
})