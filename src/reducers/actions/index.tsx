import { ReduxActions } from '../../types'
import moment from 'moment';

export function onSetActiveWorkOrder(payload: any) {
    return (dispatch: any) => dispatch({
        type: ReduxActions.OPEN_WORK_ORDER,
        payload: payload
    })
}

export function onSetAppSettings(payload: any) {
    return (dispatch: any) => dispatch({
        type: ReduxActions.SET_APP_SETTINGS,
        payload: payload
    })
}

export function onSetData(payload: any) {
    return (dispatch: any) => dispatch({
        type: ReduxActions.SET_DATA,
        payload: payload
    })
}

export function onSetDelta(payload: any) {
    return (dispatch: any) => dispatch({
        type: ReduxActions.SET_DELTA,
        payload: payload
    })
}

export function onSetNavigation(payload: any) {
    return (dispatch: any) => dispatch({
        type: ReduxActions.SET_NAVIGATION,
        payload: payload
    })
}


export function onAddChange(payload: any) {
    
    return (dispatch: any, getState: Function) => {
        
        return dispatch({
            type: ReduxActions.ADD_CHANGE,
            payload: payload
        })
    }
}

