import AppLoading from 'expo-app-loading'
import Constants from 'expo-constants'
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState, useEffect } from 'react';
import { View, LogBox } from 'react-native';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { Provider } from 'mobx-react'
import * as ScreenOrientation from 'expo-screen-orientation';
import { StatusBar } from 'expo-status-bar';
import thunk from 'redux-thunk';
import stores from "./stores"

import RootNavigation from './nav/RootNavigation';
import * as Device from 'expo-device';

import * as Sentry from 'sentry-expo';
import FAppState from './screens/FAppState'
import { Provider as ReduxProvider, connect } from 'react-redux'
import { compose, createStore, applyMiddleware } from 'redux';
import { SafeAreaProvider, useSafeArea ,initialWindowMetrics, useSafeAreaInsets,SafeAreaInsetsContext} from 'react-native-safe-area-context';

import rootReducer from './reducers'
import PackageInfo from './helpers/PackageInfo';
import { IS_IPHONE_12_ISH } from './UILab/constants'
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import * as Network from 'expo-network';

const devServerPort = 8081;
let devServerIpAddress: string | null = null;
Network.getIpAddressAsync().then(ip => {
  devServerIpAddress = ip;
});

Sentry.init({
  dsn: PackageInfo.SENTRY,
  enableInExpoDevelopment: false,
  debug: false,
  release: `Swift@${PackageInfo.VERSION_DISPLAY}`,
  tracesSampleRate: 1.0,
  integrations: [
    new Sentry.Native.ReactNativeTracing({
      shouldCreateSpanForRequest: url => {
        return !__DEV__ || !url.startsWith(`http://${devServerIpAddress}:${devServerPort}/logs`);
      },
    }),
  ]
});

import * as SplashScreen from 'expo-splash-screen';

SplashScreen.preventAutoHideAsync()
.then(result => console.log(`SplashScreen.preventAutoHideAsync() succeeded: ${result}`))
.catch(console.log);

LogBox.ignoreLogs([
  "expo-permissions is now deprecated",
  "Require cycle:",
  "componentWillMount",
  "componentWillReceiveProps",
  "componentWillUpdate",
  "VirtualizedLists",
  "dots[0].key",
  "Storybook",
  "Each child in a list",
  "manifestId",
  "syncResult.data",
  "starting up...",
  "Constants.platform.ios.model has been deprecated in favor of expo-device's Device.modelName property in expo sdk 46",
  "Constants.platform.ios.model has been deprecated in favor of expo-device's Device.modelName property. This API will be removed in SDK 45",
  "expo-permissions is now deprecated",
  // "window.navigator.geolocation.getCurrentPosition",
  "Formula:",
  "Animated: `useNativeDriver",
  "incrementLastSyncId",
  "storePreviousWoStatus",
  "LIST:",
  "Regular done with upload.",
  "Regular about to upload",
  "On screen",
  "modalIsOpen",
  // "seekNextJob",
  "inFlight",
  "TSLIs:",
  "stop flight",
  "Gonna sleep",
  "navigateBack to",
  "preload",
  "reset stopwatch",
  "init started",
  "modalScreenIsOpen",
  "unload",
  "setDashboardWorkOrders",
  "setDashboardWorkOrders dataStore",
  "clearDashboardWorkOrders dataStore",
  "expo-app-loading is deprecated"
]);



// Redux
const middleware = [thunk];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const reduxStore = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)))
const ConnectedFAppState = connect((state: any) => ({}))(FAppState)
// ---


export default function AppContainer() {

  return (<GestureHandlerRootView style={{ flex: 1 }}>
    <App />
  </GestureHandlerRootView>)

  // return (
  //   <SafeAreaProvider>
  //     <App />
  //   </SafeAreaProvider>
  // );
}
//export default 
function App(props: any) {

  const [isLoadingComplete, setLoadingComplete] = useState(false);
  
  useEffect(() => {
    setTimeout(async () => {
      try {
        
        await SplashScreen.hideAsync()
      } catch (error) {
      }
      
      }, 6000);
  }, []);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (<AppLoading
      autoHideSplash={true}
      startAsync={loadResourcesAsync}
      onError={handleLoadingError}
      onFinish={() => handleFinishLoading(setLoadingComplete)} />);
  } else {

    const _rootNavigation = IS_IPHONE_12_ISH() ? (
      <View style={{
        backgroundColor: 'transparent',
        paddingTop: Constants.statusBarHeight / 3,
        flex: 1
      }}>
        <RootNavigation />
      </View>
    ) : (<RootNavigation />);
  
    return (
      <Provider {...stores} >
        <ReduxProvider store={reduxStore}>
          <ConnectedFAppState>
          {_rootNavigation}
          </ConnectedFAppState>
        </ReduxProvider>
      </Provider>
    );
  }
}

async function loadResourcesAsync() {  
  // console.warn("Starting up...")
  await Promise.all([
    Asset.loadAsync([
      
      require('./assets/images/fb-logo.png'),
      require('./assets/icons/swift-logo-alt.png'),
      require('./assets/icons/swift-logo-android.png'),
      require('./assets/icons/swift-logo.png'),
      require('./assets/icons/swift-logo-650x650.png'),
      require('./assets/images/splash/swift-splash.png'),
      require('./assets/images/splash/swift-splash-margin.png'),
      require('./assets/images/splash/ldpi-swift-splash.png'),
      require('./assets/images/splash/mdpi-swift-splash.png'),
      require('./assets/images/splash/hdpi-swift-splash.png'),
      require('./assets/images/splash/xhdpi-swift-splash.png'),
      require('./assets/images/screens/login/login-screen-circles-background.png'),
      require('./assets/images/screens/login/sign-in-button.png'),
      require('./assets/images/screens/login/sign-in-sandbox-button.png'),
      require('./assets/images/screens/login/close-button.png'),
      require('./assets/images/screens/login/contact-us.png'),
      require('./assets/images/screens/workorderdetails/apply-button.png'),
      require('./assets/images/screens/workorderdetails/approve-button.png'),
      require('./assets/images/screens/workorderdetails/approve2-button.png'),
      require('./assets/images/screens/workorderdetails/cancel-button.png'),
      require('./assets/images/screens/workorderdetails/status-down.png'),
      require('./assets/images/screens/workorderdetails/save-changes.png'),
      require('./assets/images/screens/workorderdetails/status-down-inactive.png'),
      require('./assets/images/screens/workorderdetails/status-up.png'),
      require('./assets/images/screens/workorderdetails/add-new-service.png'),
      require('./assets/images/screens/workorderdetails/add-new-activity.png'),
      require('./assets/images/screens/workorderdetails/add-new-expenses.png'),
      require('./assets/images/screens/workorderdetails/add-new-product.png'),
      require('./assets/images/screens/workorderdetails/status-up-inactive.png'),
      require('./assets/images/screens/workorderdetails/todashboard-button.png'),
      require('./assets/images/screens/workorderdetails/todashboard-button-nl.png'),
      require('./assets/images/screens/workorderdetails/your-boss-thankyou.png'),
      require('./assets/images/screens/common/menu-back.png'),
      require('./assets/images/screens/common/menu-back-disabled.png'),
      require('./assets/images/screens/common/start-navigation.png'),
      require('./assets/images/screens/common/start-navigation-en.png'),      
      require('./assets/images/screens/drawer/fb_logo_drawer.png'),
      require('./assets/images/screens/workorderdetails/swift-no-result.png'),
      require('./assets/images/screens/drawer/logging-out-button.png'),
      require('./assets/images/screens/drawer/logout-button.png'),
      require('./assets/images/screens/drawer/logout-offline.png'),
      require('./assets/images/screens/drawer/noconnection-info.png'),
      require('./assets/images/screens/drawer/refresh-button.png'),
      require('./assets/images/screens/drawer/refresh.png'),
      require('./assets/images/screens/drawer/refreshing-button.png'),
      require('./assets/images/screens/drawer/hard-refreshing-button.png'),
      require('./assets/images/screens/drawer/settings-button.png'),
      require('./assets/images/screens/drawer/sidebar-back.png'),
      require('./assets/images/screens/drawer/sidebar-menu-header.png'),
      require('./assets/images/screens/workordersignature/clear.png'),
      require('./assets/images/screens/workordersignature/clear-en.png'),
      require('./assets/images/screens/workordersignature/signature_pad.png'),
      require('./assets/images/screens/workorders/swift-sleepy-calendar.png'),
      require('./assets/images/screens/workorders/swift-sleepy-calendar.png'),
      require('./assets/images/screens/emergency/swift-emergency-sleepy.png'),

      require('./release_notes/assets/swift-rn-start-appicon.png'),
      require('./release_notes/assets/swift-rn-start-appiconn.png'),
      require('./assets/images/screens/checklist/checklist-masked-circle.png'),
      require('./assets/images/screens/workordersignature/briefcase-masked-circle.png'),

      require('./assets/images/screens/workorderdetails/swift-sleepy-marker.png'),

      require('./release_notes/TIME_REG_V3/assets/en/DayPlanner-en.png'),
      require('./release_notes/TIME_REG_V3/assets/en/WeekOverview-en.png'),
      require('./release_notes/TIME_REG_V3/assets/en/Stopwatch-en.png'),
      require('./release_notes/TIME_REG_V3/assets/nl/DayPlanner-nl.png'),
      require('./release_notes/TIME_REG_V3/assets/nl/WeekOverview-en.png'),
      require('./release_notes/TIME_REG_V3/assets/nl/Stopwatch-nl.png'),
      require('./release_notes/TIME_REG_V3/assets/contact-slide.png')


    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      ...FontAwesome.font,
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'FontAwesome': require('native-base/Fonts/FontAwesome.ttf'),
      'Foundation': require('native-base/Fonts/Foundation.ttf'),
      'Ionicons': require('native-base/Fonts/Ionicons.ttf')
    })
  ]);
}

function handleLoadingError(error: Error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete: any) {
  
  setLoadingComplete(true);
  
  _lockIfPhone();
  
}
function _lockIfPhone() {
  try {
    Device.getDeviceTypeAsync().then(deviceType => {
      
      if (deviceType === Device.DeviceType.PHONE) {
        ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP);
      }
    });

  } catch (error) {
    // pass
  }
}

