const EMAIL_REGEX = /^$|^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const NUMBER_REGEX = /^$|^[0-9]*$/;
const CURRENCY_REGEX = /^$|^[0-9]+(\,[0-9]{1,2})?$/;

export function validate(value, type) {
  switch (type) {
    case "EMAIL":
      return EMAIL_REGEX.test(value);
    case "PHONE":
    case "NUMBER":
      return NUMBER_REGEX.test(value);
    case "CURRENCY":
      return CURRENCY_REGEX.test(value);
    default:
      return true;
  }
}
