import React from 'react'
import {Image} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
const Device = require('react-native-device-detection')

const _style = Device.isTablet
  ? {
    flex: 1,
    width: 68,
    height: 68
  }
  : {}

const StatusDown = () => <Image
  style={_style}
  source={require('../../../assets/images/screens/workorderdetails/status-down.png')}
  disabled={true}
  resizeMode="stretch"/>

const StatusDownInactive = () => <Image
  style={_style}
  source={require('../../../assets/images/screens/workorderdetails/status-down-inactive.png')}
  disabled={true}
  resizeMode="stretch"/>

const StatusUp = () => <Image
  style={_style}
  source={require('../../../assets/images/screens/workorderdetails/status-up.png')}
  disabled={true}
  resizeMode="stretch"/>

const StatusUpInactive = () => <Image
  style={_style}
  source={require('../../../assets/images/screens/workorderdetails/status-up-inactive.png')}
  disabled={true}
  resizeMode="stretch"/>

const CancelButton = (p) => <Image
  {...p}
  source={require('../../../assets/images/screens/workorderdetails/cancel-button.png')}
  resizeMode="stretch"/>

const CancelButton2 = () => <Image
  source={require('../../../assets/images/screens/workorderdetails/cancel-button.png')}
  resizeMode="stretch"/>

const ApplyButton = (p) => <Image
  {...p}
  source={require('../../../assets/images/screens/workorderdetails/apply-button.png')}
  resizeMode="stretch"/>

const ApproveButton = (p) => <Image
  {...p}
  source={require('../../../assets/images/screens/workorderdetails/approve2-button.png')}
  resizeMode="stretch"/>

const _styleToDashboard = Device.isTablet
  ? {
    flex: 1,
    width: wp('50%')
  }
  : {}
const ToDashboardButton = () => <Image
  style={_styleToDashboard}
  source={require('../../../assets/images/screens/workorderdetails/todashboard-button-nl.png')}
  resizeMode="contain"/>

const ToDashboardButtonEN = () => <Image
style={_styleToDashboard}
source={require('../../../assets/images/screens/workorderdetails/todashboard-button.png')}
resizeMode="contain"/>

const YourBossThankYou = () => <Image
  source={require('../../../assets/images/screens/workorderdetails/your-boss-thankyou.png')}
  resizeMode="contain"/>

const AddNewService = () => <Image
  source={require('../../../assets/images/screens/workorderdetails/add-new-service.png')}
  resizeMode="contain"/>

const AddNewProduct = () => <Image
  source={require('../../../assets/images/screens/workorderdetails/add-new-product.png')}
  resizeMode="contain"/>

module.exports = {
  AddNewProduct,
  AddNewService,
  ApplyButton,
  ApproveButton,
  CancelButton,
  CancelButton2,
  StatusDown,
  StatusDownInactive,
  StatusUp,
  StatusUpInactive,
  ToDashboardButton,
  ToDashboardButtonEN,
  YourBossThankYou
}