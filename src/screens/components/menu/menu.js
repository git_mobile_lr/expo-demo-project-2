import React from 'react'
import {Image,View,Text} from 'react-native'
import { FontAwesome } from '@expo/vector-icons'

const MenuBack = () => <FontAwesome style={{ marginTop: 8  }} name="angle-left" size={36} color="#00aaff" />

const MenuBackRelated = ({name="Related"}) => <View style={{paddingTop:10,flex:1,flexDirection:"row",justifyContent:'flex-start',alignItems:'center'}}>
  <Image
  source={require('../../../assets/images/screens/common/menu-back.png')}
  resizeMode="contain"/>
  <Text style={{paddingLeft:5, fontFamily: 'Roboto',color: 'rgba(13,55,84, 0.87)',fontSize: 11}}>{name}</Text>
</View>

const MenuBackAcrossApp = ({ name="Related", appIcon }) => <View style={{paddingTop:10,flex:1,flexDirection:"row",justifyContent:'flex-start',alignItems:'center'}}>
  <Image
  source={require('../../../assets/images/screens/common/menu-back.png')}
  resizeMode="contain"/>
  <Text style={{paddingHorizontal: 5, fontFamily: 'Roboto',color: 'rgba(13,55,84, 0.87)',fontSize: 11}}>{name}</Text>
  <FontAwesome name={appIcon} size={12} color='rgba(13,55,84, 0.87)' />
</View>

const MenuBackDisabled = () => <Image
  source={require('../../../assets/images/screens/common/menu-back-disabled.png')}
  style={{
  marginTop: 20,
  marginLeft: 5
}}
  resizeMode="contain"/>

const FieldBuddyAlfred = () => <View />

const FieldBuddyAlfred_old = () => <Image
  source={require('../../../assets/images/onboarding/fb-alfred.png')}
  style={{
  marginTop: 10,
  marginLeft: 5,
  marginRight: 5
}}
  resizeMode="contain"/>

module.exports = {
  MenuBackRelated,
  MenuBack,
  MenuBackDisabled,
  MenuBackAcrossApp,
  FieldBuddyAlfred
}