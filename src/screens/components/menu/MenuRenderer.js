import { View } from 'react-native'
import { Ionicons, FontAwesome } from '@expo/vector-icons'
import React from 'react'

const MenuRenderer = ({ name, size }) => {

  const MenuNormal = () => (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ width: size, height: size }}>
        <Ionicons name="menu-sharp" size={size} color="#00aaff" />
      </View>
    </View>
  )

  const MenuRefreshing = () => (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ width: size, height: size }}>
        <Ionicons name="menu-sharp" size={size} color="#00aaff" />
        <View style={{  position: 'absolute', top: 0, right: 0  }}>
          <FontAwesome name="refresh" size={size/2} color="orange" />
        </View>
      </View>
    </View>
  )

  const MenuOffline = () => (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ width: size, height: size }}>
        <Ionicons name="menu-sharp" size={size} color="#00aaff" />
        <View style={{ position: 'absolute', top: 0, right: 0 }}>
          <FontAwesome name="bolt" size={size/1.5} color="orange" />
        </View>
      </View>
    </View>
  )

  switch (name) {
    case 'MenuNormal':
      return <MenuNormal/>
    case 'MenuRefreshing':
      return <MenuRefreshing/>
    case 'MenuOffline':
      return <MenuOffline/>
    default:
      return <MenuNormal/>
  }

}

export default MenuRenderer
