import React, {Component} from "react";
import {Image} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
const Device = require('react-native-device-detection')
const _style2 = Device.isTablet
  ? {
    flex: 1,
    marginLeft: 30,
    width: wp('25%')
  }
  : {
    flex: 1,
    marginLeft: 30
  }

const _style = {
  flex: 1,
  marginLeft: 10,
  width: wp('40%')
}

const _drawerStyle = Device.isTablet
  ? {
    flex: 1,
    height: hp('10%'),
    width: wp('80%')
  }
  : {}
const LoggingOutButton = () => <Image
  source={require('../../../assets/images/screens/drawer/logging-out-button.png')}
  disabled={true}
  style={_style}
  alignItems="center"
  resizeMode="contain"/>

const LogoutButton = () => <Image
  source={require('../../../assets/images/screens/drawer/logout-button.png')}
  style={_style}
  alignItems="center"
  resizeMode="contain"/>

const LogoutOfflineButton = () => <Image
  source={require('../../../assets/images/screens/drawer/logout-offline.png')}
  style={_style}
  alignItems="center"
  resizeMode="contain"
  accessibilityLabel="Offline Logout Button"
  testID="Offline Logout Button iOS"/>

const NoConnectionInfo = () => <Image
  style={_style}
  source={require('../../../assets/images/screens/drawer/noconnection-info.png')}
  resizeMode="contain"/>

const RefreshButton = () => <Image
  source={require('../../../assets/images/screens/drawer/refresh-button.png')}
  style={_style}
  alignItems="center"
  resizeMode="contain"/>

const RefreshingButton = () => <Image
  source={require('../../../assets/images/screens/drawer/refreshing-button.png')}
  disabled={true}
  style={_style}
  alignItems="center"
  resizeMode="contain"/>

const HardRefreshingButton = () => <Image
  source={require('../../../assets/images/screens/drawer/hard-refreshing-button.png')}
  disabled={true}
  style={_style}
  alignItems="center"
  resizeMode="contain"/>

const SettingsButton = () => <Image
  source={require('../../../assets/images/screens/drawer/settings-button.png')}
  style={_style}
  alignItems="center"
  resizeMode="contain"/>

// const FieldBuddyDrawerLogo = () => <Image
// source={require('../../../assets/images/screens/drawer/fb-publish-magic-drawe
// r .png')} alignItems="center" resizeMode="contain"/>

const FieldBuddyDrawerLogo = () => <Image
  style={_drawerStyle}
  source={require('../../../assets/images/screens/drawer/fb-logo-drawer.png')}
  alignItems="center"
  resizeMode="contain"/>

module.exports = {
  FieldBuddyDrawerLogo,
  LoggingOutButton,
  LogoutButton,
  LogoutOfflineButton,
  NoConnectionInfo,
  RefreshButton,
  HardRefreshingButton,
  RefreshingButton,
  SettingsButton
}