import React from 'react'
import { ActivityIndicator } from 'react-native'
import { Image } from 'react-native'
import { Text, View } from "native-base"
import { ScrollView as Content } from 'react-native'
import { PackageInfo } from '../../../helpers'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const Device = require('react-native-device-detection')

const Busy = () => <View
  style={{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: undefined,
    width: undefined
  }}>
  <ActivityIndicator color="#FFF" size="large" animating={true} />
</View>

const BusyDark = () => <Content scrollEnabled={true} style={{
  flex: 1
}}>
  <ActivityIndicator color="#1EB1F8" size="large" animating={true} />
</Content>

const BusyDrawer = () => {
  return (
    <View style={{ height: 10 }}>
      <ActivityIndicator color="#1EB1F8" size="large" animating={true} />
    </View>
  )
}

const DisplayHeader = () => <View style={{
  flex: 3
}}>
  <Text
    style={{
      fontSize: 11,
      color: '#FFF',
      backgroundColor: 'transparent',
      letterSpacing: 0.92,
      textAlign: 'center',
      marginTop: 120
    }}>{PackageInfo.VERSION}</Text>
  <Text
    style={{
      fontSize: 8,
      color: '#FFF',
      backgroundColor: 'transparent',
      letterSpacing: 0.92,
      textAlign: 'center',
      marginTop: 5
    }}>{PackageInfo.EXVERSION}</Text>
</View>

const DisplayHeaderDark = () => <View style={{
  flex: 3
}}>
  <Text
    style={{
      fontSize: 11,
      color: '#1EB1F8',
      letterSpacing: 0.92,
      marginTop: 100
    }}>{PackageInfo.VERSION}</Text>
  <Text
    style={{
      fontSize: 8,
      color: '#1EB1F8',
      backgroundColor: 'transparent',
      letterSpacing: 0.92,
      textAlign: 'center',
      marginTop: 5
    }}>{PackageInfo.EXVERSION}</Text>
</View>

const DisplayReleaseNotes = () => <View style={{
  flex: 2
}}>
  <Text
    style={{
      fontSize: 11,
      color: '#5384A6',
      letterSpacing: 0.92,
      textAlign: 'center',
      padding: 5
    }}>Recently: {PackageInfo.RELEASE_NOTES}</Text>
</View>

const EmptyView = () => <View style={{
  flex: 1
}} />

class StartNavigation extends React.Component {
  render() {
    const _style = Device.isTablet
      ? {
        flex: 1,
        width: wp('25%')
      }
      : {}

    return (<Image
      source={require('../../../assets/images/screens/common/start-navigation.png')}
      style={_style}
      resizeMode="contain" />)
  }
}

class StartNavigationEN extends React.Component {
  render() {
    const _style = Device.isTablet
      ? {
        flex: 1,
        width: wp('25%')
      }
      : {}

    return (<Image
      source={require('../../../assets/images/screens/common/start-navigation-en.png')}
      style={_style}
      resizeMode="contain" />)
  }
}

class Clear extends React.Component {
  render() {
    const _style = Device.isTablet
      ? {
        flex: 1,
        width: wp('25%')
      }
      : {}

    return (<Image
      source={require('../../../assets/images/screens/workordersignature/clear.png')}
      style={_style}
      resizeMode="contain" />)
  }
}

class ClearEN extends React.Component {
  render() {
    const _style = Device.isTablet
      ? {
        flex: 1,
        width: wp('25%')
      }
      : {}

    return (<Image
      source={require('../../../assets/images/screens/workordersignature/clear-en.png')}
      style={_style}
      resizeMode="contain" />)
  }
}

module.exports = {
  Busy,
  BusyDark,
  BusyDrawer,
  Clear,
  ClearEN,
  DisplayHeader,
  DisplayHeaderDark,
  DisplayReleaseNotes,
  EmptyView,
  StartNavigation,
  StartNavigationEN
}
