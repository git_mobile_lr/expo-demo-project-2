import React from 'react'
import {Image} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
const Device = require('react-native-device-detection')

class SignInWithSalesforce extends React.Component {

  render() {
    const _style = Device.isTablet
      ? {
        
        width: wp('50%')
      }
      : {}

    return (<Image
      source={require('../../../assets/images/screens/login/sign-in-button.png')}
      style={_style}
      resizeMode="contain"/>)
  }
}

class SignInToSandbox extends React.Component {

  render() {
    const _style = Device.isTablet
      ? {
        
        width: wp('50%')
      }
      : {}
    return (<Image
      source={require('../../../assets/images/screens/login/sign-in-sandbox-button.png')}
      style={_style}
      resizeMode="contain"/>)
  }
}

class SaveChanges extends React.Component {

  render() {
    const _style = Device.isTablet
      ? {
        
        width: wp('50%')
      }
      : {}
    return (<Image
      source={require('../../../assets/images/screens/workorderdetails/save-changes.png')}
      style={_style}
      resizeMode="contain"/>)
  }
}

class CloseButton extends React.Component {

  render() {
    const _style = Device.isTablet
      ? {
        
        width: wp('50%')
      }
      : {}
    return (<Image
      source={require('../../../assets/images/screens/login/close-button.png')}
      style={_style}
      resizeMode="contain"/>)
  }
}

module.exports = {
  CloseButton,
  SignInToSandbox,
  SignInWithSalesforce,
  SaveChanges
}