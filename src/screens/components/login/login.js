import React from 'react'
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import {Icon} from 'native-base'
import {inject, observer} from 'mobx-react/native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
const Device = require('react-native-device-detection')

const LoginAttemptFailed = () => <View
  style={{
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
}}>
  <Icon
    name="information-circle"
    style={{
    fontSize: 50,
    color: 'rgba(255,255,255,0.87)',
    backgroundColor: 'transparent'
  }}/>
  <Text
    style={{
    fontSize: 24,
    color: 'rgba(255,255,255,0.87)',
    backgroundColor: 'transparent'
  }}>
    Login attempt failed.
  </Text>
</View>

const NoInternetConnection = () => <View
  style={{
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
}}>
  <TouchableOpacity>
    <Icon
      name="wifi"
      style={{
      fontSize: 50,
      color: 'rgba(255,255,255,0.87)',
      backgroundColor: 'transparent'
    }}/>
  </TouchableOpacity>
  <Text
    style={{
    fontSize: 24,
    color: 'rgba(255,255,255,0.87)',
    backgroundColor: 'transparent'
  }}>
    No Internet connection.
  </Text>
</View>

//@inject('loginStore')
@inject('rootStore')
@observer
class SignInDemoMode2 extends React.Component {
  _signInDemoMode = () => {

    const {loginStore} = this.props.rootStore

    loginStore.navigate("DemoForm")
  }
  render() {
    const {isLoginScreenLocked} = this.props
    return (
      <TouchableOpacity onPress={this._signInDemoMode} disabled={isLoginScreenLocked}>
        <Image
          source={require('../../../assets/images/screens/login/demo-mode.png')}
          style={{
          marginTop: 0
        }}
          resizeMode="contain"/>
      </TouchableOpacity>
    )
  }
}

//@inject('loginStore')
@inject('rootStore')
@observer
class SignInDemoMode extends React.Component {

  render() {
    const _style = Device.isTablet
      ? {
        flex: 1,
        marginTop: 0,
        width: wp('50%')
      }
      : {}

    return (<Image
      source={require('../../../assets/images/screens/login/demo-mode.png')}
      style={_style}
      resizeMode="contain"/>)
  }
}

class ContactUs extends React.Component {

  render() {
    const _style = Device.isTablet
      ? {
        flex: 1,
        marginTop: 0,
        width: wp('50%')
      }
      : {}
    return (<Image
      source={require('../../../assets/images/screens/login/contact-us.png')}
      style={_style}
      resizeMode="contain"/>)
  }
}

class VideoTour extends React.Component {
  _videoTour = () => {
   
  }
  render() {
    const {isLoginScreenLocked} = this.props
    return (
      <TouchableOpacity onPress={this._videoTour} disabled={isLoginScreenLocked}>
        <Image
          source={require('../../../assets/images/screens/login/video-tour.png')}
          style={{
          marginTop: 0
        }}
          resizeMode="contain"/>
      </TouchableOpacity>
    )
  }
}

// const FieldBuddyLoginLogo = () => <Image
// source={require('../../../assets/images/screens/login/fb-publish-magic.png')}
//   alignItems="center"   resizeMode="contain"/>

//@inject('loginStore')
@inject('rootStore')
@observer
class FieldBuddyLoginLogo extends React.Component {

  render() {
    const _style = Device.isTablet
      ? {
        flex: 1,
        marginTop: 0,
        width: wp('100%'),
        height: hp('7%')

      }
      : {}

    return (<Image
      source={require('../../../assets/images/screens/login/fb-login-logo.png')}
      style={_style}
      resizeMode="contain"/>)
  }
}

const FieldBuddyLoginLogo2 = () => <Image
  source={require('../../../assets/images/screens/login/fb-login-logo.png')}
  alignItems="center"
  resizeMode="contain"/>

const FieldBuddyLoginOval = () => <Image
  source={require('../../../assets/images/screens/login/fb-login-oval.png')}
  alignItems="center"
  resizeMode="contain"/>

module.exports = {
  ContactUs,
  FieldBuddyLoginLogo,
  FieldBuddyLoginOval,
  LoginAttemptFailed,
  NoInternetConnection,
  SignInDemoMode,
  VideoTour
}