import React from 'react';
import { styles } from './InstalledProductCreationStyles'
import { TouchableOpacity, Alert } from 'react-native'
import { Text, Container, Header, Left, Body, Title, Right, View, Button } from 'native-base'
import { ScrollView as Content } from 'react-native'
import { inject, observer } from "mobx-react/native"
import { FUserInputList, FSpinner, FActionButton, FHeading, FInlineButton } from '../../UILab'
import { MenuBack } from '../../screens/components'
import _ from 'lodash'
import * as Animatable from 'react-native-animatable'
import jsonQuery from 'json-query'


//@inject('loginStore')
@inject('rootStore')
@observer
class InstalledProductCreationScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            config: [],
            errors: {},
            value: {},
            defaults: {},
            showValidationErrors: false,
            loading: false,
            openCondition: {},
            activeInstalledProduct: {}
        };
    }


    componentDidMount() {
        const { loginStore, dataStore } = this.props.rootStore

        // Define Status Value
        const appSettings = loginStore.getAppSettings()
        if (appSettings && appSettings.rules.items.chooseDefaultStatus) {
            this.setState({
                openCondition: appSettings.rules.items.chooseDefaultStatus.targetValue,
            })
        }
        //
        q = loginStore.getScreenConfig("InstalledProductCreation")
        q.success ? user_config = q.result : user_config = {}
        this.setState(
            { config: user_config, },
            () => { this.createErrorList(this.state.config) }
        )

        // Check if the IP has already been created
        _params = this.props.route.params

        if (_params && loginStore.isCreatingWithParents) {
            let installedProductId = _.toArray(_params).reduce((acc, param) => { return (param.field === 'Id' && param.table === loginStore.getPrefixedFieldName("Installed_Product__c")) ? param.value : acc }, null)
            let currentInstalledProduct = {}

            currentInstalledProduct = _.first(loginStore.changes.filter(item => item.localId === installedProductId))

            this.setState({
                activeInstalledProduct: currentInstalledProduct,
                value: _.isEmpty(currentInstalledProduct) ? {} : { ...currentInstalledProduct.changeData, ...{ "Id": installedProductId } }
            })
        }

        loginStore.setCreatingWithParentsFlag(true)
        let location = loginStore.getPrefixedFieldName("Location__c")

        if (!this.state.defaults[location] && dataStore.currentWorkOrderLocation) {

            this.state.defaults[location] = dataStore.currentWorkOrderLocation.Name
        }

    }

    createErrorList(config) {
        if (!_.isEmpty(config)) {
            if (config.fieldsToRequest) {
                config.fieldsToRequest.map(item => {
                    if (item.required === true) {
                        let errors = this.state.errors
                        errors[item.field] = false
                        this.setState({ errors: errors })
                    }
                })
            }
        }
    }

    onCancel = () => {
        const { loginStore } = this.props.rootStore
        loginStore.setCreatingWithParentsFlag(false)
        loginStore.relatingFlag(false)
        if (!_.isEmpty(this.state.activeInstalledProduct)) {
            this.props.rootStore.loginStore.FAlert({
                dialogs: "LOST_CHANGES_WARNING",
                onCancel: () => { },
                onConfirm: () => {
                    loginStore.deleteLastChange()
                    loginStore.navigate('WorkOrderDetails', [{nextActiveTab: 1}])
                }
            }
            )
        } else {
            
            this.props.rootStore.loginStore.navigate('WorkOrderDetails', [{nextActiveTab: 1}])
        }
    }

    updateFormStatus = (value) => {
        let errorlist = this.state.errors
        let values = this.state.value
        if (value.state.hasOwnProperty('name')) {
            errorlist[value.state.name] = value.state.valid
            values[value.state.name] = value.state.value
            this.setState({ errors: errorlist, value: values })
        }
    }

    onEvent = async (params) => {
        const { config } = this.state;
        const { loginStore } = this.props.rootStore

        if (config.events) {
            let events = this
                .state
                .config
                .events
                .filter(x => x.eventName === params.eventName);

            if ((events || []).length > 0) {
                const event = _.first(events);
                const eventName = event.eventName
                const { dataBinding, staticBinding } = event
                _params = {}
                let _params = await this._prepareParams(dataBinding, staticBinding, eventName);
                loginStore.triggerEvent(event, _params);
            }
        }

    }

    _prepareParams = async (dataBinding, staticBinding, eventName) => {

        const { loginStore } = this.props.rootStore

        // Add WO to the params
        wo = loginStore.getPrefixedFieldName("Work_Order__c")
        ip = loginStore.getPrefixedFieldName("Installed_Product__c")
        _data = {}
        _data[wo] = jsonQuery(`${wo}[Id=${loginStore.currentWorkOrderId}]`, { data: loginStore.data }).value

        // Add IP to the params
        const { value } = this.state
        tableName = loginStore.getPrefixedFieldName('Installed_Product__c')
        if (eventName === 'modifyIP') {
            value["Id"] = this.state.value.Id
        } else {
            let guid = loginStore.guid()
            __UNIQUE_ID__ = `__FB__TEMPLATE_TYPE_${tableName}_${guid}`
            value["Id"] = __UNIQUE_ID__
        }

        value[loginStore.getPrefixedFieldName("Location__c")] = jsonQuery(`${loginStore.getPrefixedFieldName("Work_Order__c")}[Id=${loginStore.currentWorkOrderId}][${loginStore.getPrefixedFieldName("Location__c")}]`, { data: loginStore.data }).value
        value[loginStore.getPrefixedFieldName("Account__c")] = jsonQuery(`${loginStore.getPrefixedFieldName("Location__c")}[Id=${value[loginStore.getPrefixedFieldName("Location__c")]}][${loginStore.getPrefixedFieldName("Account__c")}]`, { data: loginStore.data }).value
        this.setState({ loading: true })
        result = await loginStore.updateRecord(tableName, __UNIQUE_ID__, value)
        _data[ip] = value
        
        if(_data[ip] && _data[ip].hasOwnProperty("Id")){
            console.warn(value["Id"]);
            loginStore.currentRelatedInstalledProductId = value["Id"]
        }
        // Get Correct Values of Data Bindings
        const _params = dataBinding.map(x => {
            let _x = JSON.parse(JSON.stringify(x));
            if (_data[x.table] && _data[x.table][x.field]) {
                _x["value"] = _data[x.table][x.field] || "";
            }

            return _x;
        });

        // Add Statics Data Bindings
        if (!staticBinding) {
            return _params
        }
        const _statics = staticBinding.map(x => JSON.parse(JSON.stringify(x)));

        let p = []
            .concat
            .apply(_params, _statics);


        return p;
    }

    checkEmptyRequiredFields() {
        const { errors } = this.state
        if (!_.isEmpty(errors)) {
            let values = Object.values(errors)
            return values.filter(x => x==false).length
        }
        return false
    }

    render() {
        const { loginStore } = this.props.rootStore
        const { config, showValidationErrors, loading } = this.state

        let emptyRequiredFields = this.checkEmptyRequiredFields()
        
        return (
            <Container style={styles.container}>
                <Header style={styles.headerStyle}>
                    <Left style={{ flex: 1 }}>
                        <TouchableOpacity
                            style={styles.touchableRect}
                            onPress={this.onCancel}
                            transparent>
                            <MenuBack />
                        </TouchableOpacity>
                    </Left>
                    <Body style={styles.bodyRight}>
                        <Title style={styles.titleStyle}>
                            <Text adjustsFontSizeToFit={true} style={styles.titleStyle}>
                                {loginStore.getLabel("${Label.AddInstalledProduct}", "Add Installation")}
                            </Text>
                        </Title>
                    </Body>
                    <Right style={{ flex: 0 }} />
                </Header>
                <Content>
                    {loading && <FSpinner />}
                    {

                        !loading &&
                        <FUserInputList
                            data={_.isEmpty(this.state.activeInstalledProduct) ? this.state.defaults : this.state.activeInstalledProduct.changeData}
                            list={config.fieldsToRequest}
                            updateFormStatus={this.updateFormStatus}
                            showValidationErrors={showValidationErrors}
                        />
                    }
                    {

                        !loading &&
                        !emptyRequiredFields &&

                        <Animatable.View
                            animation="zoomInUp"
                            duration={750}
                            style={styles.animatedView1}
                            useNativeDriver>
                            <View style={styles.addItemsSection}>
                                <FHeading size={4} value={loginStore.getLabel("${Label.AddItems}", "Add Items")} />
                                <FInlineButton add onPress={() => { _.isEmpty(this.state.activeInstalledProduct) ? this.onEvent({ eventName: "createIP" }) : this.onEvent({ eventName: "modifyIP" }) }} />
                            </View>
                        </Animatable.View>
                    }
                </Content>
            </Container>
        )
    }
}

export default InstalledProductCreationScreen;