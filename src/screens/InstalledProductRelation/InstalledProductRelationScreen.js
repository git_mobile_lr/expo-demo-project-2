import React from 'react';
import { styles } from './InstalledProductRelationStyles'
import { TouchableOpacity, Alert } from 'react-native'
import { Text, Container, Header, Left, Body, Title, Right, Button, Icon, View } from 'native-base'
import { ScrollView as Content } from 'react-native'
import { inject, observer } from "mobx-react/native"
import { FUserInputList, FSpinner, FActionButton, FHeading, FInlineButton } from '../../UILab'
import { MenuBack } from '../components'
import _ from 'lodash'
import * as Animatable from 'react-native-animatable'
import jsonQuery from 'json-query'


//@inject('loginStore')
@inject('rootStore')
@observer
class InstalledProductRelationScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            config: [],
            errors: {},
            value: {},
            showValidationErrors: false,
            loading: false
        };
    }

    componentDidMount() {
        const { loginStore } = this.props.rootStore
        loginStore.setCreatingWithParentsFlag(false)
        q = loginStore.getScreenConfig("InstalledProductRelation")
        q.success ? user_config = q.result : user_config = {}
        this.setState({
            config: user_config,
        })
        this.createErrorList(user_config)

        // Check if the IP has already been created
        _params = this.props.route.params

        if (_params && loginStore.isRelating) {
            let installedProductId = _.toArray(_params).reduce((acc, param) => { return (param.field === 'Id' && param.table === loginStore.getPrefixedFieldName("Installed_Product__c")) ? param.value : acc }, null)
            activeInstalledProduct = { "Id": installedProductId }
            _value = {}
            _value[loginStore.getPrefixedFieldName("Installed_Product__c")] = installedProductId
            this.setState({
                activeInstalledProduct: { "Id": installedProductId },
                value: _.isEmpty(installedProductId) ? {} : _value
            })
        }
    }

    createErrorList(config) {

        if (!_.isEmpty(config)) {
            if (config.fieldsToRequest) {
                config.fieldsToRequest.map(item => {
                    if (item.required === true) {
                        let errors = this.state.errors
                        errors[item.field] = false
                        this.setState({ errors: errors })
                    }
                })
            }
        }
    }

    onCancel = () => {
        const { loginStore } = this.props.rootStore
        loginStore.FAlert({
            dialogs: "LOST_CHANGES_WARNING",
            onCancel: () => { },
            onConfirm: () => {
                loginStore.relatingFlag(false)
                this.props.rootStore.loginStore.navigate('WorkOrderDetails', [{nextActiveTab: 1}])
            }
        })
    }

    updateFormStatus = (value) => {
        let errorlist = this.state.errors
        let values = this.state.value
        if (value.state.hasOwnProperty('name')) {
            errorlist[value.state.name] = value.state.valid
            values[value.state.name] = value.state.value
            this.setState({ errors: errorlist, value: values })
        }
    }

    onEvent = (params) => {
        const { config } = this.state;
        const { loginStore } = this.props.rootStore

        if (config.events) {
            let events = this
                .state
                .config
                .events
                .filter(x => x.eventName === params.eventName);


            if ((events || []).length > 0) {
                const event = _.first(events);
                const { dataBinding, staticBinding } = event
                _params = {}
                let _params = this._prepareParams(dataBinding, staticBinding);
                loginStore.triggerEvent(event, _params);
            }
        }

    }

    _prepareParams = (dataBinding, staticBinding) => {

        const { loginStore } = this.props.rootStore

        loginStore.relatingFlag(true)

        wo = loginStore.getPrefixedFieldName("Work_Order__c")
        ip = loginStore.getPrefixedFieldName("Installed_Product__c")

        _data = {}
        _data[wo] = jsonQuery(`${wo}[Id=${loginStore.currentWorkOrderId}]`, { data: loginStore.data }).value
        _data[ip] = { "Id": this.state.value[ip] }
        loginStore.currentRelatedInstalledProductId = this.state.value[ip] 
        
        const _params = dataBinding.map(x => {
            let _x = JSON.parse(JSON.stringify(x));
            if (_data[x.table] && _data[x.table][x.field]) {
                _x["value"] = _data[x.table][x.field] || "";
            }

            return _x;
        });

        if (!staticBinding) {
            return _params
        }
        const _statics = staticBinding.map(x => JSON.parse(JSON.stringify(x)));

        let p = []
            .concat
            .apply(_params, _statics);


        return p;
    }

    render() {
        const { loginStore } = this.props.rootStore
        const { config, showValidationErrors, loading } = this.state

        return (
            <Container style={styles.container}>
                <Header style={styles.headerStyle}>
                    <Left style={{ flex: 1 }}>
                        <TouchableOpacity
                            style={styles.touchableRect}
                            onPress={this.onCancel}
                            transparent>
                            <MenuBack />
                        </TouchableOpacity>
                    </Left>
                    <Body style={styles.bodyRight}>
                        <Title style={styles.titleStyle}>
                            <Text adjustsFontSizeToFit={true} style={styles.titleStyle}>
                                {loginStore.getLabel("${Label.AddInstalledProduct}", "Add Installations")}
                            </Text>
                        </Title>
                    </Body>
                    <Right style={{ flex: 0 }} />
                </Header>
                <Content>
                    {loading && <FSpinner />}
                    {!loading &&
                        <FUserInputList
                            data={_.isEmpty(this.state.value) ? {} : this.state.value}
                            list={config.fieldsToRequest}
                            updateFormStatus={this.updateFormStatus}
                            showValidationErrors={showValidationErrors}
                        />}
                    {
                        !_.isEmpty(this.state.value[loginStore.getPrefixedFieldName("Installed_Product__c")]) &&
                        <Animatable.View
                            animation="zoomInUp"
                            duration={750}
                            style={styles.animatedView1}
                            useNativeDriver>
                            <View style={styles.addItemsSection}>
                                <FHeading size={4} value={loginStore.getLabel("${Label.AddItems}", "Add Items")} />
                                <FInlineButton add onPress={() => { this.onEvent({ eventName: "addItems" }) }} accessibilityLabel={"Add Items Button"} testID={"Add Items Button iOS"}/>
                            </View>
                        </Animatable.View>
                    }
                </Content>
            </Container>
        )
    }
}

export default InstalledProductRelationScreen;