import {StyleSheet, Platform} from 'react-native'
import Constants from 'expo-constants'
import {IS_TABLET,ANDROID} from '../widgets/styles'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
  } from "react-native-responsive-screen";
  
const MarginTop = () => {
    return Platform.OS === 'ios' ?
        undefined :
        Constants.statusBarHeight
}
const visibleHeight = 300

export const styles = StyleSheet.create({
    headingContainer:{},
    heading:{},
    animatedView: {
        flex: 1,
        height: '100%',
        marginTop: MarginTop()
    },
    card:{
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,

    },
    animatedView1: {
        flex: 1,
        height: '100%',
        marginTop: MarginTop()
    },
    addItemsSection:{
        alignItems:'center'
    },
    heading:{
        fontFamily: 'Roboto',
        fontSize: 24,
        color: '#5384A6',
        letterSpacing: 0,
        textAlign: 'center',
        lineHeight: 32,
    },
    label:{
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    picklistHeader:{
        backgroundColor: "#16A7FF" 
    },
    headerTitleStyle:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: "white"
    },
    headerBackButtonTextStyle:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: "white" 
    },
    input:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0,
        

    },
    cardBody:{
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    lockIcon:{
        fontSize: 15,
        color: '#B8C4CC',
        marginRight:20,
        marginTop:30
    },
    requiredIcon:{
        fontSize: 15,
        color: '#B8C4CC',
        marginRight:20,
        marginTop:30
    },
    textError:{
        width:'100%',
        color:"red",
        fontSize: 10,
    },
    error:{
        fontSize: 15,
        color: 'red',
        marginRight:20,
        marginTop:30
    },
    FInput:{
        height:10,
        width:IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent'
    },  
    FModalIcon:{
        paddingRight:0,
        marginRight:0,
        marginLeft:0,
        color:'#00AAFF'
    },
    modalScreen:{
        flex: 1,
          width: wp("100%"),
          height: hp("100%"),
          margin: 0
    },
    FModal:{
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'space-between',
        height:10,
        width:'100%',
        borderColor: 'transparent'
    },
    placeholder:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#B8C4CC',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0
    },
    filledValue:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0
    },
    pickListIcon : {
        marginRight:0,
        marginLeft:0,
        color:'#00AAFF'
    },
    
    disabledButton:{
        opacity: 0.6,
        marginBottom: '15%',
        marginRight: -20
    },
    actionButton:{
        marginBottom: '15%',
        marginRight: -20
    },
    actionResetButtonIcon: {
        color: '#FFF'
      },
    picker:{
        width: ANDROID? 500 : 200, 
        height: 40
    },
    container: {
        backgroundColor: '#F7F9FA',
        height: visibleHeight
    },
    headerStyle: {
        backgroundColor: '#FFFFFF'
    },
    touchableRect: {
        width: '100%',
        height: '100%'
    },
    bodyRight: {
        flex: 2,
        alignItems:"flex-start",
        //marginLeft: Platform.OS === 'ios' ? -10 :10
    },
    titleStyle: {
        fontFamily: 'Roboto',
        fontSize: (IS_TABLET)? 25 : 13,
        color: 'rgba(13,55,84, 0.87)',
        justifyContent: (IS_TABLET)? 'center' : 'flex-end',
        alignItems: 'flex-start',
        marginTop: 5
    },
    body:{
        backgroundColor:"#F7F9FA"
    },
    kcontent:{
        flex: 1,
        marginTop: 1,
        marginBottom: 20
      },
    headingContainer:{
        
    },
    cancel:{
        fontFamily: 'Roboto',
        fontSize: (IS_TABLET)? 25 : 13,
        color: 'rgba(13,55,84, 0.87)',
        justifyContent: (IS_TABLET)? 'center' : 'flex-end',
        alignItems: 'flex-start',
        marginTop: ANDROID? 20 : 16,
        marginLeft: 5
    },
    heading:{
        margin:'auto',
        padding:20,
        fontFamily: 'Roboto',
        fontSize: (IS_TABLET)? 30 : 24,
        color: '#D8D8D8',
        fontWeight:"200",
        alignSelf:'center',

    },
    inputContainer:{

    },
    confirmButton:{
        color:'white'
    },
    backButton:{
        color:'black'
    },
    stepView:{
        height:'100%',
    },

    scrollViewOverview:{
       
    }
})
