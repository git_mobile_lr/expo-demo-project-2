import React, { Fragment } from 'react'
import _ from 'lodash'
import { styles, stylesTablet } from './Styles'
import {
  Alert,
  ActivityIndicator,
  BackHandler,
  StatusBar,
  TouchableOpacity,
  Platform,
  View
} from 'react-native'
import { ScrollView as Content } from 'react-native'
import {
  Body,
  Button,
  Container,

  Footer,
  Header,
  Icon,
  Left,
  Right,
  Text,
  Title,
  FooterTab
} from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Col, Row, Grid } from 'react-native-easy-grid'
import * as Animatable from 'react-native-animatable'
import { MenuBack, MenuBackDisabled } from '../components'
import MenuRenderer from '../components/menu/MenuRenderer'
import { FRenderer, FPointer, FHelp, FToast } from '../widgets'
import moment from 'moment'
import ActionButton from 'react-native-action-button'
import Lottie from 'lottie-react-native'
import { FActionButton, FSpinner, FActionButtonItem } from '../../UILab'
import { inject, observer } from 'mobx-react/native'
import { Entypo, FontAwesome } from '@expo/vector-icons'
import FTimeSheetStopwatch from '../widgets/FTimeSheetStopwatch'
const JUDGEMENT = require('../../assets/lottie/progress_hard.json')

interface Props {
  rootStore?: any
  navigation?: any
}

interface State {
  dataRecipe: string
  data: Array<any>
  userConfig: any
  hasInvalidState: boolean
  invalidState: any
  onboarding: boolean
  configLoaded: boolean
  userConfigLoaded: boolean,
  activeAppName: string,
  weekMode: boolean
}

@inject('rootStore')
@observer
export default class TimeRegistrationDashboard extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props)
    this.state = {
      dataRecipe: "TimeRegistrationDashboard",
      data: [],
      userConfig: null,
      hasInvalidState: false,
      invalidState: {},
      onboarding: false,
      configLoaded: false,
      userConfigLoaded: false,
      activeAppName: "",
      weekMode: false
    }
  }


  _unsubscribe: any

  componentDidUpdate(prevProps: Props, prevState: State) {
    const { userConfig, userConfigLoaded } = this.state
    if (!_.isEmpty(userConfig) && (userConfigLoaded == false)) {
      this.setState({ userConfigLoaded: true })
    }
    if (this.state.activeAppName != this.props.rootStore.appsStore.activeAppName) {
      this._loadAsync()
    }

  }

  async getUserConfig(dataRecipe: string = "TimeRegistrationDashboard") {
    const { loginStore } = this.props.rootStore
    const resultConfigByRecipe = await loginStore.getUserConfigByRecipeName(dataRecipe)
    if (resultConfigByRecipe.type === 'success') {
      return resultConfigByRecipe.data
    } else {
      return null
    }
  }

  async _loadAsync() {
    let that = this
    const { dataRecipe } = this.state
    const { loginStore, appsStore } = this.props.rootStore
    const resultConfigByRecipe = await loginStore.getUserConfigByRecipeName(dataRecipe)

    if (resultConfigByRecipe.type === 'success') {

      that.setState({
        userConfig: resultConfigByRecipe.data,
        activeAppName: appsStore.activeAppName
      })
    }
    return { type: 'success' }
  }

  async _delayedLoadAsync() {
    this
      ._loadAsync()
     
  }

  refresh() {
    const { changesStore, loginStore } = this.props.rootStore
    let delta = !_.isEmpty(changesStore.changesPerSyncId[loginStore.syncId])
    if (delta) {
      this.setState({ userConfigLoaded: false })
    }
  }


  async componentDidMount() {
    const { loginStore, dataStore, permissionStore } = this.props.rootStore
    // Navigation Listener. This is used to reload items everytime the screen is focused again from the navigation. Ex: when the user taps on back button from WorkOrderItems
    const { navigation } = this.props
    this._unsubscribe = navigation.addListener('focus', () => {
      
      this.refresh()
      // this._delayedLoadAsync()
      loginStore.setSelectedTab(0)
      dataStore.clearActiveObjects()
      loginStore.clearCurrentWorkOrder()

    });

    loginStore.setNavigation(navigation)
    permissionStore.checkAll()
    dataStore.clearActiveObjects()
    loginStore.clearCurrentWorkOrder()

    setTimeout(() => {
      this._delayedLoadAsync()
      loginStore.setSelectedTab(0)
    }, Platform.OS === 'ios'
      ? 125
      : 75)

    BackHandler.addEventListener('hardwareBackPress', function () {
      return true
    })

  }


  componentWillUnmount() {
    
   
    this._unloadAsync()

    // this._clearInterval()

    
  }
  async _unloadAsync() {
    try {
      console.warn("unload")
      this._unsubscribe()
      BackHandler.removeEventListener('hardwareBackPress')
    } catch (error) {
      //pass   
    }
  }

  _menu = async () => {

    const { loginStore } = this.props.rootStore
    const { navigation } = this.props
    navigation.toggleDrawer();
  }

  _menuImage() {
    const { isOffline, forceIsOffline, isDrawerBusy, isTablet } = this.props.rootStore.loginStore

    if (this.props.rootStore.loginStore.getIsOffline) {
      return <MenuRenderer size={isTablet ? 40 : 30} name='MenuOffline' />
    }
    if (isDrawerBusy) {
      return <MenuRenderer size={isTablet ? 40 : 30} name='MenuRefreshing' />
    }
    return <MenuRenderer size={isTablet ? 40 : 30} name='MenuNormal' />
  }

  _helpImage() {
    const { showReleaseNotes } = this.props.rootStore.settingsStore.rules.otherDefaults
    if(!showReleaseNotes) return null;
    const { loginStore } = this.props.rootStore
    const { isTablet, isWeekMode } = loginStore

    return (
      <>
      {!isWeekMode && <TouchableOpacity
        style={[
          styles.touchableRect, {
            flex: 1,
            width: 15,
            alignItems: 'flex-end'
          }
        ]}
        transparent
        onPress={async ()=> {await loginStore.setReleaseNotesSeen(false)}}>
        <Entypo name="megaphone" size={isTablet ? 30 : 20} color='#00aaff' style={{paddingHorizontal: 5}} />
      </TouchableOpacity>}
      </>
    )
  }

  async _helpAction() {
    const { loginStore } = this.props.rootStore
    if (!loginStore.isTooltipVisible) {
      loginStore.setIsTooltipVisible = true
    }
    loginStore.setIsHelpVisible = true

  }

  _requestHelpAction = () => {
    const { loginStore } = this.props.rootStore

    if (!loginStore.isTooltipVisible) {
      loginStore.setIsTooltipVisible = true
    }

  }

  _getChildren() {

    try {
      const { data, userConfig } = this.state
      
      return <FRenderer data={data} config={userConfig} />
    } catch (error) {}

    return (<Container><Content><FSpinner/></Content></Container>)
    return (
      <View>
        <FSpinner />
      </View>
    )
  }

  _getTitle() {
    const { loginStore } = this.props.rootStore
    const { isDeveloperMode, isWeekMode, selectedDay } = loginStore
    const dialogs = loginStore.getLocalizedDialogs()
    const { WEEKNUMBER } = dialogs

    const _syncIdInfo = __DEV__ || isDeveloperMode ? `(N:${loginStore.getSyncId})` : ""
    const _usingLocalConfig = __DEV__   && loginStore.usingLocalConfig ? " LOCAL" : ""
    if (this.state.userConfig) {
      if(isWeekMode) {
        let weekNumber = this._getWeekNumber(new Date(selectedDay))
        return WEEKNUMBER.MESSAGE + "#"+ weekNumber
      }
      const { userConfig } = this.state
      const _title = userConfig.header && userConfig.header.title
        ? userConfig.header.title
        : ""

      const _titleLabel = this
        .props
        .rootStore
        .loginStore
        .findLabel(_title)


      return _titleLabel + " " + _syncIdInfo  + _usingLocalConfig
    }

    return _syncIdInfo
  }

  _getWeekNumber(selectedDay) {
    let oneJan = new Date(selectedDay.getFullYear(),0,1);
    let numberOfDays = Math.floor((selectedDay - oneJan) / (24 * 60 * 60 * 1000));
    let result = Math.ceil(( selectedDay.getDay() + 1 + numberOfDays) / 7);
    return result
  }
  

  _getHeaderStyle() {
    return this.props.rootStore.loginStore.isDeveloperMode
      ? styles.headerDeveloperStyle
      : styles.headerStyle
  }

  _goBackToList = async () => {
    const { loginStore } = this.props.rootStore

    loginStore.navigateAndGoSelectedDay("TimeRegistrationDashboard")

  }

  _goBackToDayMode = async () => {
    const { loginStore } = this.props.rootStore
    loginStore.navigateAndGoSelectedDay("TimeRegistrationDashboard")
    loginStore.setIsWeekMode = false
  }

  _renderLeft() {
    const {
      askedForPermissions,
      isSyncingLocked,
      isDrawerBusy,
      isOnboarding,
      isHelpVisible,
      isRestoring,
      isSyncing,
      isTriggerReload,
      endReached,
      goToday,
      isCalendarOpen,
      isWeekMode
    } = this.props.rootStore.loginStore

    if (isCalendarOpen) {
      const _menu = isSyncingLocked
        ? (<MenuBackDisabled />)
        : (<MenuBack />)

      return (
        <TouchableOpacity
          disabled={isSyncingLocked}
          style={styles.touchableRect}
          transparent
          onPress={() => {
            this._goBackToList()
          }}>
          {_menu}
        </TouchableOpacity>
      )
    }
    if(isWeekMode) {
      const _menu = isSyncingLocked
        ? (<MenuBackDisabled />)
        : (<MenuBack />)

      return (
        <TouchableOpacity
          accessibilityLabel="Back Button"
          testID="Back Button iOS"
          disabled={isSyncingLocked}
          style={styles.touchableRect}
          transparent
          onPress={() => {
            this._goBackToDayMode()
          }}>
          {_menu}
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity
        accessibilityLabel="Hamburger Button"
        testID="Hamburger Button iOS"
        disabled={isDrawerBusy || goToday}
        style={styles.touchableRect}
        transparent
        onPress={() => this._menu()}>
        {this._menuImage()}
      </TouchableOpacity>
    )
  }

  render() {
  
    const { loginStore, appsStore } = this.props.rootStore
    const {
      askedForPermissions,
      isDrawerBusy,
      isOnboarding,
      isHelpVisible,
      isRestoring,
      isSyncing,
      isTriggerReload,
      isWeekMode,
      endReached,
      goToday,
      isCalendarOpen,
      syncId,
      isDeveloperMode,
      isTablet
    } = this.props.rootStore.loginStore

  
    const { activeAppName } = appsStore

    if (this.state.userConfigLoaded == false) return (<Container><Content><FSpinner/></Content></Container>)

    if (syncId >= 0) {
      const { hasInvalidState, userConfig } = this.state


      const children = this._getChildren()

      const _actionButtons = this._renderActionButtons()
      const _titleStyle = this.props.rootStore.loginStore.isTablet
        ? stylesTablet.titleStyle
        : styles.titleStyle

      const _left = this._renderLeft()
      const _statusBar = this._renderStatusBar()
      const { isTooltipVisible, stopwatchIsRunning } = loginStore

      const _isStopwatch = this
        .props
        .rootStore.loginStore
        .isStopwatchTimeRegistration
      
      let isWeekMode = this
        .props
        .rootStore
        .loginStore
        .isWeekMode
            
      return (
        <Animatable.View
          animation="bounceInUp"
          duration={this.props.rootStore.loginStore.ANIMATION_DURATION}
          style={{
            flex: 1,
            backgroundColor: '#E9EEF1',
            height: '100%'
          }}
          useNativeDriver>
          <React.Fragment>
            {_statusBar}
            <Container>

              <Header style={this._getHeaderStyle()}>
                <Left>
                  {_left}
                </Left>
                <Body style={[_titleStyle, { flex:1, flexDirection : "row", alignItems : "center"}]}>
                {stopwatchIsRunning ? 
                  <TouchableOpacity
                  accessibilityLabel="Time Registration Screen Stopwatch"
                  testID="Time Registration Screen Stopwatch iOS"
                  onPress={this._stopwatchRedirection.bind(this)}>
                    <FontAwesome name="clock-o" size={isTablet ? 30 : 20} color="orange" style={{paddingRight : isTablet ? 20 : 10, top : isTablet ? 5 : 2.5}}/>
                  </TouchableOpacity> : <View></View>
                  }
                  <TouchableOpacity
                    accessibilityLabel="Time Registration Dashboard Title"
                    testID="Time Registration Dashboard Title iOS"
                    onLongPress={this
                      ._onSlackChanges
                      .bind(this)}>
                    <Title style={_titleStyle}>
                      <Text style={_titleStyle}>{this._getTitle()}</Text>
                    </Title>
                  </TouchableOpacity>
                </Body>
                <Right >
                  
                </Right>
              </Header>
              <Content scrollEnabled={false} ref="MainContent">
              {isTooltipVisible && <FToast />}
             

                {children}

              </Content>

                {_actionButtons}

                {!isWeekMode && <FTimeSheetStopwatch />}



            </Container>
          </React.Fragment>
        </Animatable.View>
      )
    } else {
      
      return (<Container><Content><FSpinner/></Content></Container>)
    }


  }

  _renderStatusBar() {
    const { isSyncing, isSyncingLocked } = this.props.rootStore.loginStore
    return (
      <StatusBar networkActivityIndicatorVisible={isSyncing}></StatusBar>
    )
  }


  _renderActionIconGoToday() {
    const _DD = moment().format("D")

    const _calendarFontSize = this.props.rootStore.loginStore.isTablet
      ? 60
      : 40
    const _fontSize = this.props.rootStore.loginStore.isTablet
      ? 32

      : (Platform.OS === 'ios'
        ? 18
        : 16)

    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}>

        <Icon
          ios='ios-calendar'
          android='md-calendar'
          name='calendar'
          style={{
            color: '#FFF',
            fontSize: _calendarFontSize,
            alignSelf: 'center',
            flex: 1,
            opacity: Platform.OS === 'ios'
              ? 0.7
              : 0.5,
            position: 'absolute',
            zIndex: 2
          }}></Icon>
        <Text
          style={{
            backgroundColor: 'transparent',
            color: '#FFF',
            opacity: 1,
            fontFamily: 'Ionicons',
            fontSize: _fontSize,
            fontWeight: 'bold',
            paddingTop: 4,
            alignSelf: 'center',
            flex: 1,
            position: 'absolute',
            zIndex: 4
          }}>{_DD}</Text>

      </View>
    )

  }

  _renderGoToday() {
    const { loginStore } = this.props.rootStore
    return (<ActionButton
      size={this.props.rootStore.loginStore.isTablet
        ? 100
        : 56}
      renderIcon={this
        ._renderActionIconGoToday
        .bind(this)}
      buttonColor="rgba(0,170,255, 1)"
      style={{
        zIndex: 99999999,
        marginBottom: '5%',
        marginRight: -20
      }}
      onPress={() => {
        loginStore.setGoToday = true
      }} />)
  }

  _changeMode() {
    const { loginStore } = this.props.rootStore
    const { isWeekMode, selectedDay } = this.props.rootStore.loginStore
    loginStore.setIsWeekMode = !isWeekMode
    // setTimeout(() => {
    //   loginStore.selectedDay = selectedDay
    // }, 1000)
  }

  _renderTSLIButtons() {
    const { loginStore } = this.props.rootStore
    const { isWeekMode } = this.props.rootStore.loginStore
    const _createLabel  = loginStore.getLabelorDefault("${Label.AddTimesheetLineItem}", "Add a TSLI")
    const dialogs = loginStore.getLocalizedDialogs()
    const { WEEKOVERVIEW } = dialogs
    const _changeModeLabel = WEEKOVERVIEW.MESSAGE

    const event = {
      "privileges": true,
      "eventType": "createable",
      "targets": [loginStore.getPrefixedFieldName("Timesheet_Lineitem__c")]
    }
    return (
     <>
      {!isWeekMode && <FActionButton
        summary        
        offsetX={20}
        >
          <ActionButton.Item
            title={_createLabel}
            onPress={() => { }}>
            <FActionButtonItem 
              orange
              event={event}
              onPress={() => {
                loginStore.navigate("TimeSheetInsertEdit", {
                  goBack: "TimeRegistrationDashboard"
                });
              }}
              icon={"Add"}
            />
          </ActionButton.Item>
          <ActionButton.Item
            title={_changeModeLabel}
            onPress={() => { }}>
            <FActionButtonItem
              onPress={() => {
                this._changeMode()
              }}
              icon={"calendar-week"}
              fontAwesome5={true}
              accessibilityLabel="week overview button"
              testID="week overview button iOS"
            />
          </ActionButton.Item>
      </FActionButton>}
      </>
    )

    
  }

  _renderActionButtons() {
    const { loginStore } = this.props.rootStore

    if (loginStore.isCalendarOpen) {
      return this._renderGoToday()
    } else {
      return this._renderTSLIButtons()
    }
  }

  _stopwatchRedirection() {
    const { loginStore, appsStore, dataStore } = this.props.rootStore
    const { timerWO, stopwatchStartTime } = loginStore

    if (timerWO) {
      const workOrderTable = loginStore.getPrefixedTableName("Work_Order__c")
      const timerWorkOrder = loginStore.getRecordById(workOrderTable, timerWO)
      let params = [
          {
              "var": "NAME",
              "table": "Work_Order__c",
              "field": "Name",
              "value": timerWorkOrder.Name
          },
          {
              "var": "ID",
              "table": "Work_Order__c",
              "field": "Id",
              "value": timerWorkOrder.Id
          },
          {
              "activeItems": dataStore.activeItems,
              "previousActiveTab": loginStore.selectedTab,
              "nextActiveTab": 0,
              "from": dataStore.getCurrentWorkOrder() ? dataStore.getCurrentWorkOrder().Name : ""
          }
      ]
      loginStore.currentWorkOrderId = timerWorkOrder.Id
      appsStore.setActiveAppName("WorkOrders")
      loginStore.navigation.navigate("WorkOrderDetails", params)
    } else {
      if (appsStore.activeAppName === "TimeRegistration") {
        if (stopwatchStartTime) {
        loginStore.selectedDay = moment(stopwatchStartTime).format("YYYY-MM-DD")
        }
      }
    }
  }

  async _onSlackChanges() {

    const { loginStore } = this.props.rootStore

    const _changes = this.props.rootStore.changesStore.getAllChanges()

    if (_.isEmpty(_changes)) {
      if (loginStore.isDeveloperMode || __DEV__) {
        Alert.alert('No changes', 'No changes registered', [
          {
            text: 'OK'
          }
        ], { cancelable: false })
      }

    } else {
      await loginStore._slackChanges()
    }


  }

  
}

