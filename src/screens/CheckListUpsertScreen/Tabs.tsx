import React, { Component } from 'react'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import FCustomTabBar from '../widgets/FCustomTabBar/FCustomTabBar'
import { inject, observer } from "mobx-react/native"
import { View } from 'native-base'
import { FSpinner, FActionButton } from '../../UILab'
import _ from 'lodash'

type Props = {
    isInsertMode: boolean
    lastIcon: boolean
    tabs: Array<JSX.Element> // These elements must contain a props called tabLabel. It's used in scrollableTabView
    status: string
    lastUpdate: string
    rootStore: any
}
type State = {
    selectedTab: number
    tabsContent: Array<JSX.Element>
    isInsertMode: boolean
    totalTabs: number
    status: string
    lastUpdate: string
}

type Config = {

}

@inject('rootStore')
@observer
class Tabs extends Component<Props, State> {

    static defaultProps: { tabs: JSX.Element[] }

    constructor(props: Props) {
        super(props);
        this.state = {
            selectedTab: 0,
            tabsContent: props.tabs,
            totalTabs: props.tabs.length,
            status: props.rootStore.dataStore.status,
            lastUpdate: props.rootStore.dataStore.lastUpdate
        }

    }

    componentDidUpdate(prevProps: Props) {
        if (!_.isEqual(prevProps.tabs, this.state.tabsContent)) {
            this.setState({ tabsContent: this.props.tabs })
        }
        if (!_.isEqual(prevProps.rootStore.dataStore.status, this.state.status)) {
            this.setState({ status: this.props.rootStore.dataStore.status })
        }
        if (!_.isEqual(prevProps.rootStore.dataStore.lastUpdate, this.state.lastUpdate)) {
            this.setState({ lastUpdate: this.props.rootStore.dataStore.lastUpdate })
        }
    }

    tabView: any

    onChangeTab({ i, ref, from }) {
        this.setState({ selectedTab: i })
    }

    isDisabled(index: number) {
        try {
            if (this.props.tabs[index].props.validate) {
                return this.props.tabs[index].props.validate()
            }
        } catch (error) {

        }
        return { result: false, rule: null }
    }

    render() {
        const { tabsContent, selectedTab } = this.state

        const { isInsertMode, lastIcon } = this.props
        const { lastUpdate, status } = this.props.rootStore.dataStore
        return _.isEmpty(tabsContent)
            ? <FSpinner />
            : <ScrollableTabView
                style={{ marginTop: 2 }}

                onChangeTab={this.onChangeTab.bind(this)}
                ref={(tabView) => { if (tabView != null) { this.tabView = tabView; } }}
                initialPage={0}
                renderTabBar={() => <FCustomTabBar
                    isInsertMode={isInsertMode}
                    lastIcon={lastIcon}
                    isDisabled={this.isDisabled.bind(this)}
                    selectedTab={selectedTab}
                />
                }
            >
                {tabsContent}
            </ScrollableTabView >

    }
}

Tabs.defaultProps = {
    lastIcon: true,
    tabs: [],
    
}

export default Tabs;