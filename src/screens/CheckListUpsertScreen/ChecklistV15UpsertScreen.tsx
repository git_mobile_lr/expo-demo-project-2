import React, { Component } from 'react';
import { ScrollView } from 'react-native'
import { View, Text } from 'native-base'
import _ from 'lodash'
import { inject, observer } from "mobx-react/native"
import * as Sentry from 'sentry-expo';
import { FMultiSelectPickList, FInfoCard, FField, FUserInput, FSpinner, FHeader, FFormula } from '../../UILab'
import { MetaData } from '../../types'
import { upsertScreenModes, upsertScreenMode } from '../../types'
import FTextButton from '../../UILab/atoms/FTextButton/FTextButton';
import { FontAwesome, MaterialCommunityIcons } from "@expo/vector-icons"
import * as Permissions from 'expo-permissions'
import Tabs from './Tabs'
import data from '../../reducers/data';
import { FPropertiesEx } from '../widgets';
type Props = {
  rootStore?: any
  UID: string
  parentTableName: string
  config: Config
  mode: upsertScreenMode
}
type State = {
  assetsAreLoaded: boolean,
  UID: string
  config: Config
  dataFieldsFromTemplate: Array<object>
  metaFields: Array<MetaData> | null
  metaFieldsLegacy: Array<MetaData> | null
  parentTableName: string
  workOrderItem: object
  woliStatus: string
  mode: upsertScreenMode
}
type Config = {
  targetTable: string,
  isOnline: boolean,
  originalWoi: object,
  originalWoids: Array<object>,  
  targetChildrenTable: string,
  propertiesLayout: Config,
  typesFilters: Array<string>
  fieldsFilters: Array<string>
}
@inject('rootStore')
@observer
class ChecklistV15UpsertScreen extends Component<Props, State> {
  static defaultProps: { UID: string; parentTableName: string; config: Config, mode: string };
  constructor(props: Props) {
    super(props);
    this.state = {
      assetsAreLoaded: false,
      mode: props.mode,
      UID: props.UID,
      workOrderItem: this.getWorkOrderItem(),
      woliStatus: '',
      config: props.config,
      dataFieldsFromTemplate: [{}],
      parentTableName: props.parentTableName,
      metaFields: this.getMetaFields(props.parentTableName),
      metaFieldsLegacy: []
    };
  }
  async componentDidMount() {
    const { dataStore, permissionStore, loginStore } = this.props.rootStore
    loginStore.isChecklistOpen = true
    permissionStore.checkCameraAndCameraRoll()
    this.setState({
      workOrderItem: this.getWorkOrderItem(),
      woliStatus: this.getWoliStatus(),
      dataFieldsFromTemplate: this.getDataFields(this.props.UID),
      metaFields: this.getMetaFields(this.props.parentTableName),
      metaFieldsLegacy: this.getMetaFieldsLegacy()
      
    }, () => {
      
      dataStore.addContextItem(this.state)
      dataStore.setMetaFieldsLegacy(this.state.metaFieldsLegacy)
      dataStore.status = this.state.woliStatus
      dataStore.renewLastUpdate()
      dataStore.callCalcFilterLogics()
      dataStore.setCurrentChecklistMode(this.props.mode)
      
      this.setState({
        assetsAreLoaded: true
      })
    })
  }
  componentWillUnmount(){
    const { loginStore } = this.props.rootStore
    loginStore.isChecklistOpen = false
  }
  componentDidUpdate(prevProps: Props) {
    if (!_.isEqual(prevProps.UID, this.state.UID)) {
      this.setState({
        UID: this.props.UID,
        dataFieldsFromTemplate: this.getDataFields(this.props.UID),
        metaFields: this.getMetaFields(this.props.parentTableName),
        workOrderItem: this.getWorkOrderItem()
      })
    }
  }
  getWorkOrderItem(): object {
    const { loginStore } = this.props.rootStore
    const { parentTableName, UID } = this.props
    const WOI = loginStore.getPrefixedFieldName("Work_Order_Item__c")
    const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
    const { mode } = this.props
   
    
    let item = {}
    switch (mode) {
      default:
      case upsertScreenModes.INSERT:
        if (this.props.config.isOnline) {
          return _.isEmpty(this.props.config.originalWoi) ? {} : this.props.config.originalWoi
        }
        item = loginStore.getRecordById(parentTableName, UID)
        try {
          if (item.Id === this.props.config.originalWoi.Id) {
            return _.isEmpty(this.props.config.originalWoi) ? {} : this.props.config.originalWoi
          }
        } catch (error) {
          //pass
        }
        
        /*
        {
    "attributes": {
        "type": "FIELDBUDDY__Work_Order_Item__c",
        "url": "/services/data/v50.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b3I000003GTvvQAG"
    },
    "CreatedDate": "2022-05-16T15:30:11.000+0000",
    "FIELDBUDDY__Item_Code__c": "d101",
    "FIELDBUDDY__Quantity__c": 12,
    "FIELDBUDDY__Tax_Amount__c": 2.08,
    "FIELDBUDDY__Tax_Rate__c": 21,
    "FIELDBUDDY__Type__c": "Activity",
    "FIELDBUDDY__Unit_Price_Including_Tax__c": 12,
    "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 12,
    "Id": "a0b3I000003GTvvQAG",
    "Name": "d101",
    "FIELDBUDDY__Display_Type__c": "Details",
    "FIELDBUDDY__Increment__c": 1,
    "FIELDBUDDY__Help_Text__c": "abc def",
    "LastModifiedDate": "2022-05-16T19:33:25.000+0000",
    "UserRecordAccess": {
        "attributes": {
            "type": "UserRecordAccess"
        },
        "HasEditAccess": true,
        "MaxAccessLevel": "Transfer",
        "HasDeleteAccess": false,
        "HasTransferAccess": true
    }
}
-
{
    "attributes": {
        "type": "FIELDBUDDY__Work_Order_Item__c",
        "url": "/services/data/v50.0/sobjects/FIELDBUDDY__Work_Order_Item__c/a0b3I000003GV1YQAW"
    },
    "Id": "a0b3I000003GV1YQAW",
    "CreatedDate": "2022-05-17T03:44:12.000+0000",
    "FIELDBUDDY__Quantity__c": 12,
    "FIELDBUDDY__Tax_Amount__c": 2.08,
    "FIELDBUDDY__Tax_Rate__c": 21,
    "FIELDBUDDY__Type__c": "Activity",
    "FIELDBUDDY__Unit_Price_Including_Tax__c": 12,
    "FIELDBUDDY__Unit_Price_Including_Tax_input__c": 12,
    "Name": "d102",
    "FIELDBUDDY__Display_Type__c": "Details",
    "FIELDBUDDY__Increment__c": 1,
    "FIELDBUDDY__Item_Code__c": "d101"
}
*/
        
        return _.isEmpty(item) ? {} : item
      case upsertScreenModes.UPDATE:
      case upsertScreenModes.READONLY:
        let woli = loginStore.getRecordById(WOLI, UID)
        let woi = loginStore.getRecordById(WOI, woli[WOI])
        return _.isEmpty(woi) ? {} : woi
    }
  }
  getWoliStatus(): string {
    const { loginStore } = this.props.rootStore
    const { UID } = this.props
    const STATUS = loginStore.getPrefixedFieldName("Status__c")
    const WOI = loginStore.getPrefixedFieldName("Work_Order_Item__c")
    const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
    const { mode } = this.props
    switch (mode) {
      default:
      case upsertScreenModes.INSERT: {
        let woi = this.getWorkOrderItem();
        
        try {
          const DISPLAY_TYPE = loginStore.getPrefixedFieldName("Display_Type__c")
          if (woi && woi.hasOwnProperty(DISPLAY_TYPE)) {
            if (woi[DISPLAY_TYPE] === "Details") {
              const _targetValue = loginStore.getChooseDefaultStatus()
              
              return _targetValue
            }
          }
        } catch (error) {
          //pass
        }
        if (woi && woi[STATUS]) {
          debugger
          return woi[STATUS]
        }
      } 
      
      case upsertScreenModes.UPDATE:
      case upsertScreenModes.READONLY:
        let woli = loginStore.getRecordById(WOLI, UID)
        if (woli && woli[STATUS]) {
          return woli[STATUS]
        }
    }
    return ""
  }
  getMetaFields(parentTableName: string): Array<MetaData> | null {
    const { loginStore } = this.props.rootStore
    const metaFieldsResult = loginStore.getMetaFields(parentTableName)
    if (metaFieldsResult.type === 'success') {
      let metaFields = metaFieldsResult.data
      const { config } = this.props
      const { typesFilters, fieldsFilters } = config
      if (!_.isEmpty(typesFilters) && !_.isEmpty(fieldsFilters)) {
        metaFields = metaFields.filter((x: MetaData) => {
          return _.includes(typesFilters, x.type) && _.includes(fieldsFilters, x.name)
        })
      }
      return metaFields
    }
    return null
  }
  
  /**
   * Given an ID, it returns a list of WOIDS or WOLIDS according to the component mode
   * @param UID UID
   */
  getDataFields(UID: string): Array<object> {
    const { mode } = this.props
    switch (mode) {
      default:
      case upsertScreenModes.INSERT:
        try {
          const { loginStore } = this.props.rootStore
          const WOID = loginStore.getPrefixedFieldName("Work_Order_Item_Detail__c")
          const WOI = loginStore.getPrefixedFieldName("Work_Order_Item__c")
          if (this.props.config.isOnline) {
            
            const _fields = this.props.config.originalWoids
            return _fields ? _fields : [{}]
          }
          const _fields = loginStore.data[WOID].filter((x: any) => x[WOI] === UID)
          return _fields ? _fields : [{}]
        } catch (err) {
          return [{}]
        }
      case upsertScreenModes.UPDATE:
      case upsertScreenModes.READONLY:
        try {
          const { loginStore } = this.props.rootStore
          const WOLID = loginStore.getPrefixedFieldName("Work_Order_Line_Item_Detail__c")
          const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
          const _data = loginStore.data[WOLID]
          const _fields2 = _data.filter((x: any) => x[WOLI] === UID)
          return _fields2 ? _fields2 : [{}]
        } catch (err) {
          return [{}]
        }
    }
  }
  noPagesDefined(){
    // return true
    const { loginStore } = this.props.rootStore
    const PAGE = loginStore.getPrefixedFieldName("Page__c")
    const { dataFieldsFromTemplate } = this.state
    return _.isEmpty((dataFieldsFromTemplate || []).filter(x=>x.hasOwnProperty(PAGE) && x[PAGE]!==""))
  }
  /**
   * Takes dataFieldsFromTemplate from the state and fitler those items that are type PAGE.
   * It also concats an extra page for the first page INFO
   */
  getDataPages(): Array<object> {
    const { mode } = this.props
    const _insertMode = mode == upsertScreenModes.INSERT
    try {
      const { dataFieldsFromTemplate } = this.state
      const { loginStore } = this.props.rootStore
      const { lastUpdate, status } = this.props.rootStore.dataStore
      const FIELDTYPE = loginStore.getPrefixedFieldName("Field_Type__c")
      const PAGE = loginStore.getPrefixedFieldName("Page__c")
      if(this.noPagesDefined()){
        if(_.isEmpty(dataFieldsFromTemplate)){
          return  _.concat({ Name: "Info" })
        }
        
        return  _.concat({ Name: "Info" }, { Name: "Details" })       
         
      }
      const dataPages = _.sortBy((dataFieldsFromTemplate || []).filter((x: any) => x[FIELDTYPE] === 'Page'), (x: any) => x[PAGE])
      const pages = _.concat({ Name: "Info" }, dataPages)
      if (!_insertMode) {
        pages.push({ Name: 'Closure' })
      }
      return pages
    } catch (err) {
      // Sentry.Native.captureException(new Error("Not able to get data pages:"+err), {
      //   logger: 'ChecklistV15UpsertScreen',
      //   context: 'getDataPages'
      // });
      return [{}]
    }
  }
  getMetaFieldsLegacy() {
    const { loginStore } = this.props.rootStore
    const { targetTable, targetChildrenTable, typesFilters, fieldsFilters, propertiesLayout } = this.props.config
    let metaFieldsLegacy = []
    const { cloneWoiToWoli } = this.props.rootStore.settingsStore.rules.items
    
    if (cloneWoiToWoli) {
      const metaFieldsLegacyResult = loginStore.getMetaFields(targetTable)
      if (metaFieldsLegacyResult.type === 'success') {
        metaFieldsLegacy = metaFieldsLegacyResult.data
      }
      
    } else if (propertiesLayout) {
      /*
      "sections": [
        {
            "table": "FIELDBUDDY__Work_Order_Line_Item__c",
            "iconName": "md-person",
            "iosIconName": "ios-person",
            "headless": true,
            "mapping": "FIELDBUDDY__Work_Order_Line_Item__c",
            "title": "",
            "objects": [        
      */
      if (propertiesLayout.config && propertiesLayout.config.sections) {
        for (let section of propertiesLayout.config.sections) {
          if (section.objects) {
            const fieldNames = _.values(section.objects.map(x => x.name))
            const metaFieldsLegacyResult = loginStore.getMetaFields(section.table)
            if (metaFieldsLegacyResult.type === 'success') {
              metaFieldsLegacy = metaFieldsLegacyResult.data
              if (!_.isEmpty(fieldNames)) {
                metaFieldsLegacy = metaFieldsLegacy.filter(x => _.includes(fieldNames, x.name))
              }
            }
          }
        }
      }
    } else {
      const metaFieldsLegacyResult = loginStore.getMetaFields(targetTable)
      if (metaFieldsLegacyResult.type === 'success') {
        metaFieldsLegacy = metaFieldsLegacyResult.data
        if (!_.isEmpty(typesFilters)) {
          metaFieldsLegacy = metaFieldsLegacy
            .filter(x => _.includes(typesFilters, x.type))
        }
        if (!_.isEmpty(fieldsFilters)) {
          metaFieldsLegacy = metaFieldsLegacy
            .filter(x => _.includes(fieldsFilters, x.name))
        }
      }
    }
    return metaFieldsLegacy
  }
  _renderLegacyMetaFields(): Array<JSX.Element> | null {
    const { loginStore } = this.props.rootStore
    const { mode } = this.props
    const { metaFieldsLegacy } = this.state
    const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
    
    let _objs = metaFieldsLegacy.map((meta: MetaData, index: number) => {
      return { name: meta.name, default: meta.value }
    })
    
    let _params = {}
    _params.isWOLI = true
    if (mode === upsertScreenModes.INSERT) {
      _params.useDefaultWOI = true
      /*
       {
 
   "CreatedDate": "2021-08-04T18:47:35.000+0000",
   "FIELDBUDDY__Item_Code__c": "007/2",
   "FIELDBUDDY__Type__c": "Activity",
   "Id": "a0b3I000000iOp7QAE",
   "Name": "default value details",
   "FIELDBUDDY__Display_Type__c": "Details",
   "FIELDBUDDY__Increment__c": 1,
   "FIELDBUDDY__Help_Text__c": "abc def",
   "LastModifiedDate": "2021-08-04T18:47:35.000+0000",
  
       }
       */
      _params.templateWOI = this.state.workOrderItem
    }
    _params.delegatesSave = true
    if (!_.isEmpty(_objs)) {
      if (this.props.config.propertiesLayout) {
        return (<FPropertiesEx config={this.props.config.propertiesLayout.config} params={_params} UID={mode === upsertScreenModes.INSERT ? null : this.props.UID} initAt={WOLI} />)
      }
      let _c = {
        "saveAll": true,
        "sections": [
          {
            "table": `${WOLI}`,
            "iconName": "md-person",
            "iosIconName": "ios-person",
            "headless": true,
            "mapping": `${WOLI}`,
            "objects": _objs
          }
        ]
      }
      return (<FPropertiesEx config={_c} params={_params} UID={mode === upsertScreenModes.INSERT ? null : this.props.UID} initAt={WOLI} />)
    }
    return null;
  }
  _renderMetaFields(): Array<JSX.Element> | null {
    const { useDetailsAsChecklist } = this.props.rootStore.settingsStore.rules.items
    if (useDetailsAsChecklist) {
      return this._renderLegacyMetaFields()
    }
    
    const { metaFields, parentTableName, workOrderItem } = this.state
    let _fields: Array<JSX.Element>
    _fields = []
    if (metaFields) {
      const HELPTEXT = this.props.rootStore.loginStore.getPrefixedFieldName("Help_Text__c")
      const helpText = metaFields.filter((meta: MetaData) => meta.name == HELPTEXT)
      const otherFields = metaFields.filter((meta: MetaData) => meta.name !== HELPTEXT)
      if (helpText) {
        if (workOrderItem.hasOwnProperty(HELPTEXT)) {
          //@ts-ignore
          _fields.push(<FInfoCard text={workOrderItem[HELPTEXT]} />)
        }
      }
      const jsxElements = otherFields.map((meta: MetaData, index: number) => {
        return <FField key={index} table={parentTableName} field={meta.name} data={{ [parentTableName]: [workOrderItem] }} />
      })
      return _.concat(_fields, jsxElements)
    }
    return null
  }
  updateWoliStatus = () => {
    const { dataStore } = this.props.rootStore
    const { status } = dataStore
    const statusIsOpen = status === "Open"
    const newStatus = statusIsOpen ? "Closed" : "Open"
    dataStore.status = newStatus
  }
  _renderClosureContent = () => {
    const { loginStore, dataStore } = this.props.rootStore
    const { mode } = this.props
    const { status } = dataStore
    const statusIsOpen = status === "Open"
    const _readOnly = mode == upsertScreenModes.READONLY
    const _color = _readOnly ? '#a8a8a8' : '#00AAFF'
    const fillColor = statusIsOpen ? _color : '#FFFFFF50'
    const textColor = statusIsOpen ? '#fff' : _color
    const dialogs = loginStore.getLocalizedDialogs()
    const { CHECKLIST_STATUS } = dialogs
    const _btnText = statusIsOpen ? CHECKLIST_STATUS.CLOSE : CHECKLIST_STATUS.OPEN
    return (
      <View style={{ paddingHorizontal: 20, paddingTop: 50 }}>
        <Text style={{ alignSelf: 'center', textAlign: 'center', color: '#0D3754', fontSize: 18, fontWeight: '400' }}>{CHECKLIST_STATUS.INFO}</Text>
        <FTextButton
          text={_btnText}
          textColor={textColor}
          fillColor={fillColor}
          borderColor={_color}
          isReadOnly={_readOnly}
          onPress={this.updateWoliStatus}
        />
      </View>
    )
  }
  onDetailInsert = (item: any) => {
    const { dataStore } = this.props.rootStore
    const { workOrderItem } = this.state
    let newItem = {}
    newItem[item.id ? item.id : item.Id ? item.Id : undefined] = item
    let WOLI = Object.assign(dataStore.contextData.workOrderLineItemDetail ? dataStore.contextData.workOrderLineItemDetail : workOrderItem)
    WOLI = { ...WOLI, ...newItem }
    dataStore.addContextItem({ ...dataStore.contextData, ...{ "workOrderLineItemDetail": WOLI } })
  }
  onDetailUpdate = (item: any) => {
    const { dataStore, loginStore } = this.props.rootStore
    const { workOrderItem } = this.state
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    const VALUE = loginStore.getPrefixedFieldName("Value__c")
    let newItem = _.clone(item)
    let details = {}
    let value = newItem.value ? newItem.value : (newItem[VALUE] ? newItem[VALUE] : "")
    newItem.value = value
    details[item.id ? item.id : item.Id ? item.Id : undefined] = newItem
    _.unset(newItem, "id")
    _.unset(newItem, "pickListValues")
    _.unset(workOrderItem, "id")
    let newWolids = dataStore.contextData.wolids ? dataStore.contextData.wolids : []
    if (!dataStore.contextData.wolids || _.isEmpty(dataStore.contextData.wolids)) {
      newWolids = [item]
    } else {
      if (_.isEmpty(dataStore.contextData.wolids.filter((wolid: any) => wolid.Id === item.id || wolid.Id === item.Id))) {
        newWolids.push(item)
      } else {
        newWolids = dataStore.contextData.wolids.map((wolid: any) => {
          if (wolid.Id === item.id || wolid.Id === item.Id) {
            return item
          }
          return wolid
        })
      }
    }
    let WOLI = Object.assign(dataStore.contextData.workOrderLineItemDetail ? dataStore.contextData.workOrderLineItemDetail : workOrderItem)
    WOLI = { ...WOLI, ...details }
    let _obj = { ...dataStore.contextData, ...{ "data": WOLI }, ...{ "wolids": newWolids } }
    dataStore.addContextItem(_obj)
    dataStore.callCalcFilterLogics()
    dataStore.renewLastUpdate()
  }
  renderMultiSelectPickList(dataItem: any) {
    const { mode } = this.props
    const { loginStore } = this.props.rootStore
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    const DEFAULT = loginStore.getPrefixedFieldName("Default_Value__c")
    const PICKLISTVALUES = loginStore.getPrefixedFieldName("Picklist_Values__c")
    let picklistValues = dataItem[PICKLISTVALUES] ? dataItem[PICKLISTVALUES].split("\n") : []
    const defaultValue = dataItem[DEFAULT] ? dataItem[DEFAULT].split(";") : []
    const FIELDTYPE = loginStore.getPrefixedFieldName("Field_Type__c")
    const REQUIRED = loginStore.getPrefixedFieldName("Required__c")
    const HELPTEXT = loginStore.getPrefixedFieldName("Help_Text__c")
    //@ts-ignore [TS-FIX]
    const _type = dataItem && (dataItem[FIELDTYPE] || "").toUpperCase()
    switch (mode) {
      case upsertScreenModes.READONLY:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            value={this.getInputValue(dataItem)}
            readonly={true}
          />
        )
      case upsertScreenModes.INSERT:
        return (
          <FUserInput
            //@ts-ignore -> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={""}
            type={_type}
            value={defaultValue}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
      default:
      case upsertScreenModes.UPDATE:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            value={this.getInputValue(dataItem)}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
    }
  }
  renderPicture(dataItem: any) {
    const { mode } = this.props
    const { loginStore } = this.props.rootStore
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    const DEFAULT = loginStore.getPrefixedFieldName("Default_Value__c")
    const defaultValue = dataItem[DEFAULT] ? dataItem[DEFAULT].split(";") : []
    const FIELDTYPE = loginStore.getPrefixedFieldName("Field_Type__c")
    const REQUIRED = loginStore.getPrefixedFieldName("Required__c")
    const HELPTEXT = loginStore.getPrefixedFieldName("Help_Text__c")
    //@ts-ignore [TS-FIX]
    const _type = dataItem && (dataItem[FIELDTYPE] || "").toUpperCase()
    switch (mode) {
      case upsertScreenModes.READONLY:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            scrollable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            value={this.getInputValue(dataItem)}
            readonly={true}
          />
        )
      case upsertScreenModes.INSERT:
        return (
          <FUserInput
            //@ts-ignore -> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            scrollable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={""}
            type={_type}
            value={defaultValue}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
      default:
      case upsertScreenModes.UPDATE:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            scrollable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            value={this.getInputValue(dataItem)}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
    }
  }
  renderIconPicklist(dataItem: any) {
    const { mode } = this.props
    const { loginStore } = this.props.rootStore
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    const DEFAULT = loginStore.getPrefixedFieldName("Default_Value__c")
    const PICKLISTVALUES = loginStore.getPrefixedFieldName("Picklist_Values__c")
    let picklistValues = dataItem[PICKLISTVALUES] ? dataItem[PICKLISTVALUES].split("\n") : []
    const defaultValue = dataItem[DEFAULT] ? dataItem[DEFAULT].split(";") : []
    const FIELDTYPE = loginStore.getPrefixedFieldName("Field_Type__c")
    const REQUIRED = loginStore.getPrefixedFieldName("Required__c")
    const HELPTEXT = loginStore.getPrefixedFieldName("Help_Text__c")
    //@ts-ignore [TS-FIX]
    const _type = dataItem && (dataItem[FIELDTYPE] || "").toUpperCase()
    switch (mode) {
      case upsertScreenModes.READONLY:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            value={this.getInputValue(dataItem)}
            readonly={true}
          />
        )
      case upsertScreenModes.INSERT:
        return (
          <FUserInput
            //@ts-ignore -> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={""}
            type={_type}
            value={defaultValue}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
      default:
      case upsertScreenModes.UPDATE:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            value={this.getInputValue(dataItem)}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
    }
  }
  renderLabelPicklist(dataItem: any) {
    const { mode } = this.props
    const { loginStore } = this.props.rootStore
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    const DEFAULT = loginStore.getPrefixedFieldName("Default_Value__c")
    const PICKLISTVALUES = loginStore.getPrefixedFieldName("Picklist_Values__c")
    let picklistValues = dataItem[PICKLISTVALUES] ? dataItem[PICKLISTVALUES].split("\n") : []
    const defaultValue = dataItem[DEFAULT] ? dataItem[DEFAULT].split(";") : []
    const FIELDTYPE = loginStore.getPrefixedFieldName("Field_Type__c")
    const REQUIRED = loginStore.getPrefixedFieldName("Required__c")
    const HELPTEXT = loginStore.getPrefixedFieldName("Help_Text__c")
    //@ts-ignore [TS-FIX]
    const _type = dataItem && (dataItem[FIELDTYPE] || "").toUpperCase()
    switch (mode) {
      case upsertScreenModes.READONLY:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            value={this.getInputValue(dataItem)}
            readonly={true}
          />
        )
      case upsertScreenModes.INSERT:
        return (
          <FUserInput
            //@ts-ignore -> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={""}
            type={_type}
            value={defaultValue}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
      default:
      case upsertScreenModes.UPDATE:
        return (
          <FUserInput
            //@ts-ignore-> extra property for multiselect picklist
            id={dataItem.Id}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            touchable={false}
            label={dataItem.Name}
            name={dataItem.Name}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            type={_type}
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={picklistValues}
            value={this.getInputValue(dataItem)}
            onSubmit={(item: any) => { this.onItemUpdate(item, dataItem) }}
          />
        )
    }
  }
  onItemUpdate = (item: any, dataItem: any) => {
    const { mode } = this.props
    const { loginStore } = this.props.rootStore
    const VALUE = loginStore.getPrefixedFieldName("Value__c")
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    try {
      if (typeof item != 'object') {
        let newItem = { ...dataItem }
        newItem[VALUE] = item
        item = newItem
      } else {
        let newItem = { ...dataItem }
        
        if (item) {
          newItem[VALUE] = item.value
          if (item.hasOwnProperty('comments')) {
            newItem[COMMENTS] = item.comments
          }
        }
        item = newItem
      }
      switch (mode) {
        case upsertScreenModes.INSERT:
          this.onDetailInsert(item)
        default:
        case upsertScreenModes.UPDATE:
          this.onDetailUpdate(item)
      }
    } catch (error) {
      console.warn('error', error)
    }
  }
  _getPlaceholder(dataItem: any) {
    try {
      const { loginStore, dataStore } = this.props.rootStore
      const PLACEHOLDER_WOID = loginStore.getPrefixedFieldName("Placeholder__c")
      const PLACEHOLDER_WOLID = loginStore.getPrefixedFieldNameByPackageVersion("Placeholder__c")
      switch (this.props.mode) {
        case upsertScreenModes.INSERT:
          return dataItem[PLACEHOLDER_WOID] ? dataItem[PLACEHOLDER_WOID] : ""
        default:
        case upsertScreenModes.UPDATE:
          return dataItem[PLACEHOLDER_WOLID] ? dataItem[PLACEHOLDER_WOLID] : ""
      }
    } catch (error) {
      return ""
    }
  }
  renderTextArea(dataItem:any) {
    const { loginStore } = this.props.rootStore
    const REQUIRED = loginStore.getPrefixedFieldName("Required__c")
    const FIELDTYPE = loginStore.getPrefixedFieldName("Field_Type__c")
    const PICKLISTVALUES = loginStore.getPrefixedFieldName("Picklist_Values__c")
    const HELPTEXT = loginStore.getPrefixedFieldName("Help_Text__c")
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    const UNIT = loginStore.getPrefixedFieldNameByPackageVersion("Unit__c")
    const API_NAME = loginStore.getPrefixedFieldNameByPackageVersion("API_Name__c")
    const FORMULA = loginStore.getPrefixedFieldNameByPackageVersion("Formula__c")
    const FILTER_LOGIC = loginStore.getPrefixedFieldNameByPackageVersion("Filter_Logic__c")
    const _placeholder = this._getPlaceholder(dataItem)
    const _type = dataItem && (dataItem[FIELDTYPE] || "").toUpperCase()
    return (
      //@ts-ignore - missing props
      <FUserInput
        id={dataItem.Id}
        readonly={upsertScreenModes.READONLY === this.props.mode || _type === "FORMULA"}
        required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
        label={dataItem.Name}
        name={dataItem.Name}
        displayTextArea={true}
        maxLength={255}
        apiName={dataItem[API_NAME]}
        formula={dataItem[FORMULA]}
        filterLogic={dataItem[FILTER_LOGIC]}
        placeholder={_placeholder}
        type={_type}
        comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
        unit={dataItem[UNIT]}
        scrollable={this.isScrollTouchcard(_type)}
        //@ts-ignore - missing props
        helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}        
        picklistvalues={this._getPicklistValues(dataItem, PICKLISTVALUES)}
        value={this.getInputValue(dataItem)}
        onSubmit={(item: string) => this.onItemUpdate(item, dataItem)}
      />
    )
  }
  _renderDataItem(dataItem: any) {
    const { loginStore, dataStore } = this.props.rootStore
    const REQUIRED = loginStore.getPrefixedFieldName("Required__c")
    const FIELDTYPE = loginStore.getPrefixedFieldName("Field_Type__c")
    const PICKLISTVALUES = loginStore.getPrefixedFieldName("Picklist_Values__c")
    const HELPTEXT = loginStore.getPrefixedFieldName("Help_Text__c")
    const COMMENTS = loginStore.getPrefixedFieldName("Comments__c")
    const UNIT = loginStore.getPrefixedFieldNameByPackageVersion("Unit__c")
    const API_NAME = loginStore.getPrefixedFieldNameByPackageVersion("API_Name__c")
    const FORMULA = loginStore.getPrefixedFieldNameByPackageVersion("Formula__c")
    const FILTER_LOGIC = loginStore.getPrefixedFieldNameByPackageVersion("Filter_Logic__c")
    
    const _placeholder = this._getPlaceholder(dataItem)
    //@ts-ignore [TS-FIX]
    const _type = dataItem && (dataItem[FIELDTYPE] || "").toUpperCase()
    switch (_type) {
      case "MULTISELECTPICKLIST":
        return this.renderMultiSelectPickList(dataItem)
      case "ICONPICKLIST":
        return this.renderIconPicklist(dataItem)
      case "LABELPICKLIST":
        return this.renderIconPicklist(dataItem)
      case "HEADER":
        return <FHeader text={dataItem.Name} />
      case "TEXT AREA":
        return this.renderTextArea(dataItem)
      case "PICTURE":
        return this.renderPicture(dataItem)
      case "FORMULA":
      case "PICKLIST":
      case "CHECKBOX":
      case "CURRENCY":
      case "NUMBER":
      case "DOUBLE":
      case "EMAIL":
      case "TEXT":
      case "DATE":
      case "DATE/TIME":
        return (
          //@ts-ignore - missing props
          <FUserInput
            id={dataItem.Id}
            readonly={upsertScreenModes.READONLY === this.props.mode || _type === "FORMULA"}
            required={dataItem[REQUIRED] ? dataItem[REQUIRED] : false}
            label={dataItem.Name}
            name={dataItem.Name}
            apiName={dataItem[API_NAME]}
            formula={dataItem[FORMULA]}
            filterLogic={dataItem[FILTER_LOGIC]}
            placeholder={_placeholder}
            type={_type}
            comments={!_.isEmpty(dataItem[COMMENTS]) ? dataItem[COMMENTS] : ""}
            unit={dataItem[UNIT]}
            scrollable={this.isScrollTouchcard(_type)}
            //@ts-ignore - missing props
            helpText={dataItem[HELPTEXT] ? dataItem[HELPTEXT] : null}
            picklistvalues={this._getPicklistValues(dataItem, PICKLISTVALUES)}
            value={this.getInputValue(dataItem)}
            onSubmit={(item: string) => this.onItemUpdate(item, dataItem)}
          />
        )
    }
    return null
  }
  private _getPicklistValues(dataItem: any, PICKLISTVALUES: any): string[] {
   
    let arr =  dataItem[PICKLISTVALUES] ? dataItem[PICKLISTVALUES].split("\n").map((item: string) => item.trim()) : [];
    return _.concat("", arr )
  }
  isScrollTouchcard(type: any) {
    return _.includes(
      [
        "PICKLIST",
        "CHECKBOX",
        "CURRENCY",
        "NUMBER",
        "DOUBLE",
        "EMAIL",
        "TEXT",
        "DATE",
        "DATE/TIME",
        "TEXT AREA"], type)
  }
  getInputValue(dataItem: any) {
    const { loginStore } = this.props.rootStore
    const DEFAULT = loginStore.getPrefixedFieldName("Default_Value__c")
    const VALUE = loginStore.getPrefixedFieldName("Value__c")
    if (dataItem[VALUE]) {
      return !_.isEmpty(dataItem[VALUE]) ? dataItem[VALUE] : null
    }
    return !_.isEmpty(dataItem[DEFAULT]) ? dataItem[DEFAULT] : ''
  }
  /**
   * 
   * @param page 
   */
  _renderDataFields(page: any): (Element | null)[] {
    const { dataFieldsFromTemplate } = this.state
    const { loginStore, dataStore } = this.props.rootStore
    const ORDER = loginStore.getPrefixedFieldName("Order__c")
    const PAGE = loginStore.getPrefixedFieldName("Page__c")
    if (!_.isEmpty(dataFieldsFromTemplate)) {
      
      const _dataFieldsPerPage = 
      this.noPagesDefined() ?
      dataFieldsFromTemplate :
      // [{Name:"Details"}] :
      (dataFieldsFromTemplate || []).filter((x: any) => x[PAGE] === page[PAGE])
      
      let _orderedDataFields = _.sortBy(_dataFieldsPerPage, (x: any) => x.Name)
      const contextWolids = dataStore.contextData.wolids ? dataStore.contextData.wolids : []
      if (!_.isEmpty(contextWolids)) {
        for (let wolid of contextWolids) {
          const index = _.findIndex(_orderedDataFields, x => x.Id === wolid.Id)
          if (index >= 0) {
            _orderedDataFields[index] = wolid
          }
        }
      }
      _orderedDataFields = this._trySortByOrder( _orderedDataFields);
      const _fieldsDetail = _orderedDataFields.map(dataItem => {
        // @ts-ignore
        return this._renderDataItem(dataItem)
      })
      return _fieldsDetail
    }
    return [<View />]
  }
  _trySortByOrder(objects: object[]) {
    try {
      const ORDER = this.props.rootStore.loginStore.getPrefixedFieldName("Order__c")
      //if there's at least 1 object with Order 
      let containsOrder = (objects.filter(x => x[ORDER]) || []).length > 0
      if (containsOrder) {
        //if an object doesn't have Order, that is, if Order is undefined, treat it as 0
        return _.sortBy(objects, x => x[ORDER] || 0);
      }
    } catch (error) {
      //pass
    }
    return objects;
  }
  _getContent(page: any, index: number, isLastPage: boolean) {
    const { mode } = this.props
    const _insertMode = mode == upsertScreenModes.INSERT
    if (isLastPage && !_insertMode) {
      return this._renderClosureContent()
    }
    return index === 0 ? this._renderMetaFields() : this._renderDataFields(page)
  }
  /**
   * 
   * @param page 
   * @param index 
   */
  _renderScrollViewTab(page: any, index: number, isLastPage: boolean) {
    const { loginStore } = this.props.rootStore
    const { isProccesingChanges } = loginStore
    const _content = this._getContent(page, index, isLastPage)
    let _title = this.props.rootStore.loginStore.findLabel(page.Name)
    
    let _validate = () => {
      try {
        return this._show(page)
      } catch (error) {
      }
      return true
    }
    return (
      //@ts-ignore ScrollView doesn't have the property tabLabel. Should be implemented diferently ?
      <ScrollView 
        keyboardShouldPersistTaps={'handled'}
        tabLabel={_title}
        validate={_validate}>
        { isProccesingChanges ? <FSpinner /> : _content}
      </ScrollView>)
  }
  _show(page, index) {
    
    const {dataStore, loginStore} = this.props.rootStore
    const API_NAME = loginStore.getPrefixedFieldNameByPackageVersion("API_Name__c")
    
    if (page && page[API_NAME]) {
      let f = (apiName: string) => {
        return dataStore.calcFilterLogic(apiName)
      }
      return f(page[API_NAME])
    }
    return true
  }
  getTabs() {
    const pages = this.getDataPages()
    const _tabs = pages.map((page, index) => {
      let _show = this._show(page, index)
      let _isLastPage = this.noPagesDefined() ? false : (pages.length === index + 1)
      
      if (!_show) {
        //let _title = this.props.rootStore.loginStore.findLabel(page.Name)
        // return (
        //   //@ts-ignore ScrollView doesn't have the property tabLabel. Should be implemented diferently ?
        //   <ScrollView tabLabel={_title} directionalLockEnabled={true} >
        //   </ScrollView >)
        return null
      }
      return this._renderScrollViewTab(page, index, _isLastPage)
    });
    return _tabs
  }
  render() {
    const { lastUpdate, status } = this.props.rootStore.dataStore
    const { mode } = this.props
    const _insertMode = mode == upsertScreenModes.INSERT
    if (!this.state.assetsAreLoaded) {
      return null
    }
    let tabs = this.getTabs()
    return (
      <Tabs isInsertMode={_insertMode} lastIcon={!this.noPagesDefined()} tabs={tabs}  />
    )
  }
}
ChecklistV15UpsertScreen.defaultProps = {
  mode: upsertScreenModes.INSERT,
  UID: "",
  parentTableName: "Work_Order_Item__c",
  config: {
    propertiesLayout: null,
    typesFilters: ["STRING", "PICKLIST", "NUMBER"],
    fieldsFilters: ["Name", "Help_Text__c", "Type__c", "Display_Type__c", "Quantity__c", "Status__c"]
  }
}
export default ChecklistV15UpsertScreen;