import { StyleSheet, Platform } from 'react-native'

export const styles = StyleSheet.create({
    touchableRect: {
      width: '100%',
      height: '100%'
    },
    bodyRight: {
      flex: 2,
      justifyContent: 'center'
    },
    right: {
      flex: 1,
      justifyContent: 'center'
    },
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: undefined,
      width: undefined
    },
    headerStyle: {
      backgroundColor: '#FFFFFF'
    },
    leftStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: 'bold',
      color: '#00AAFF',
      letterSpacing: -0.53
    },
    titleStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'center',
      alignItems: 'flex-start',
      marginTop: 5
    },
    titleSmallStyle: {
      fontFamily: 'Roboto',
      fontSize: 14,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      marginTop: 5
    },
    rightStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: '500',
      color: '#00AAFF',
      letterSpacing: -0.53,
      alignSelf: 'flex-end',
      marginRight: 5,
      marginTop: 7
    },
    contentStyle: {
      backgroundColor: '#F8F8F8'
    },
    insideStyle: {
      //color : '#00AAFF',
      flex: 1,
      backgroundColor: '#F8F8F8'
    },
    tabsStyle: {
      color: '#00AAFF',
      backgroundColor: '#F8F8F8'
    },
    tabStyle: {
      //color : '#AFBAC0',
      alignSelf: 'flex-start',
      backgroundColor: '#F8F8F8'
    },
    tabTextStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#AFBAC0'
    },
    activeTabStyle: {
      //color : '#00AAFF',
      alignSelf: 'flex-start',
      backgroundColor: '#F8F8F8'
    },
    activeTabTextStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#00AAFF'
    },
    tabBarUnderlineStyle: {
      backgroundColor: '#00AAFF'
    },
    listItem: {
      backgroundColor: '#FFFFFF'
    },
    cardStyle: {
      backgroundColor: 'transparent'
    },
    cardHeaderStyle: {
      fontFamily: 'Roboto_medium',
      fontSize: 13,
      backgroundColor: '#00AAFF',
      color: '#FFFFFF',
      letterSpacing: 0,
      height: 29,
      alignItems: 'center'
    },
    cardHeaderIconStyle: {
      fontFamily: 'Ionicons',
      fontSize: 14,
      backgroundColor: '#00AAFF',
      color: '#FFFFFF',
      fontWeight: '500'
    },
    cardBodyStyle: {
      backgroundColor: 'transparent',
      height: 162
    },
    headlineStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10
    },
    cardElemStyle: {
      backgroundColor: 'transparent',
      borderColor: 'transparent',
      borderWidth: 0
  
    },
    cardElemHeaderStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      backgroundColor: '#FFFFFF',
      color: '#5384A6',
      letterSpacing: 0,
      height: 18,
      alignItems: 'center'
    },
    cardElemBodyStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      backgroundColor: '#FFFFFF',
      color: '#000',
      letterSpacing: 0,
      height: 18,
      alignItems: 'center'
    },
    cardElemHeaderIconStyle: {
      fontFamily: 'Ionicons',
      fontSize: 14,
      backgroundColor: '#FFFFFF',
      color: '#5384A6',
      fontWeight: '500'
    },
    centering: {
      marginTop: 100,
      alignItems: 'center',
      justifyContent: 'center'
    },
    footerToolbar: {
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      backgroundColor: "#00AAFF",
  
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15,
      borderWidth: 1.5,
      shadowColor: "rgba(0,0,0,0.14)",
      borderColor: '#00AAFF'
    },
    summaryDivider: {
      marginHorizontal: 7,
      backgroundColor: 'rgba(83,132,166,0.23)',
      marginTop: 5,
      marginBottom: 5
    },
    label: {
      fontSize: 12
    },
    smallDevicePickerHeaderStyle: {
  
      color: '#FFF',
      backgroundColor: "#00AAFF",
  
      fontFamily: 'Roboto',
      fontSize: 12,
      letterSpacing: 0
    },
    standardDevicePickerHeaderStyle: {
  
      color: '#FFF',
      backgroundColor: "#00AAFF",
  
      fontFamily: 'Roboto',
      fontSize: 14,
      letterSpacing: 0
    },
    buttonText: {
  
      color: "#00AAFF",
  
      fontFamily: 'Roboto',
      fontSize: 11,
      letterSpacing: 0
    },
    _inputStyle: {
  
      backgroundColor: "transparent",
      fontSize: 14,
      color: "#40505A"
    },
    __inputStyle: {
  
      backgroundColor: "transparent",
      fontSize: 14,
      color: "#40505A"
    },
    itemInputTextStyle: {
      flex: 1,
      flexDirection: 'column',
      marginVertical: 0,
      // paddingLeft: Platform.OS === 'ios' ? undefined : 30,
      alignItems: 'flex-start',
      alignContent: 'center',
      justifyContent: 'center',
  
      backgroundColor: "#FFF",
      borderBottomWidth: 2,
      borderBottomColor: "rgba(198,198,198,0.30)"
  
  
    },
    itemTextAreaStyle: {
      flex: 1,
      flexDirection: 'column',
      marginVertical: 0,
      // paddingLeft: Platform.OS === 'ios' ? undefined : 30,
      alignItems: 'flex-start',
      alignContent: 'center',
      justifyContent: 'center',
  
      height: 170,
      backgroundColor: "#FFF",
      borderBottomWidth: 2,
      borderBottomColor: "rgba(198,198,198,0.30)"
    },
    itemStyle: {
      flex: 1,
      flexDirection: 'column',
      marginVertical: 0,
      // paddingLeft: Platform.OS === 'ios' ? undefined : 30,
      alignItems: 'flex-start',
      alignContent: 'center',
      justifyContent: 'center',
  
      height: 70,
      backgroundColor: "#FFF",
      borderBottomWidth: 2,
      borderBottomColor: "rgba(198,198,198,0.30)"
    },
    inputSwitchStyle: {
      flex: 1,
      alignSelf: 'flex-start',
      justifyContent: 'center',
      alignContent: 'flex-start',
      width: '90%',
      backgroundColor: "transparent",
      marginLeft: 15,
      paddingLeft: 0,
      marginVertical: 0,
      borderWidth: 0
  
    },
    itemDisabledStyle: {
      flex: 1,
      flexDirection: 'column',
      marginVertical: 0,
      // paddingLeft: Platform.OS === 'ios' ? undefined : 30,
      alignItems: 'flex-start',
      alignContent: 'center',
      justifyContent: 'center',
  
      height: 70,
      backgroundColor: "rgba(13,55,84,0.03)",
      borderBottomWidth: 2,
      borderBottomColor: "rgba(198,198,198,0.30)"
    },
    labelStyle: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
  
      fontFamily: "Roboto",
      backgroundColor: "#FFF",
      fontSize: 11,
      color: "#5384A6",
      letterSpacing: 0,
      marginLeft: 15,
      paddingLeft: 0,
      paddingTop: 2,
      top: 1
  
    },
    inputStyle: {
      flex: 2,
      alignSelf: 'flex-start',
      justifyContent: 'center',
      alignContent: 'center',
      width: '100%',
      backgroundColor: "#FFF",
      fontSize: 16,
      color: "#40505A",
      letterSpacing: 0,
      marginLeft: 15,
      paddingLeft: 0
    },
  
    inputDisabledStyle: {
      flex: 2,
      alignSelf: 'flex-start',
      justifyContent: 'center',
      alignContent: 'center',
      width: '100%',
      backgroundColor: "#FFF",
  
      fontSize: 16,
      color: "#AFBAC0",
      letterSpacing: 0,
      marginLeft: 15,
      paddingLeft: 0
    },
    inputPickerStyle: {
      flex: 1,
      alignSelf: 'flex-start',
      fontSize: 14,
  
      width: Platform.OS === "ios"
        ? undefined
        : "90%",
  
      paddingLeft: 0
    },
    pickerTextStyle: {
      padding: 0,
      fontSize: 14,
      color: "#40505A",
      textAlign: 'left'
    },
    actionButtonIcon: {
      color: '#FFF'
    },
    actionResetButtonIcon: {
      color: '#00aaFF'
    },
    readonly: {
      flex: 1,
      flexDirection: 'column',
      marginVertical: 0,
      // paddingLeft: Platform.OS === 'ios' ? undefined : 30,
      alignItems: 'flex-start',
      alignContent: 'center',
      justifyContent: 'center',
  
      height: 70,
      // backgroundColor: "#f2f3f4", backgroundColor: "#8c979a", backgroundColor:
      // "#c1c7c9", backgroundColor: "#f2f3f4", backgroundColor: "#dadedf",
      backgroundColor: "#f0f0f0",
      borderBottomWidth: 2,
      borderBottomColor: "rgba(198,198,198,0.30)"
    },
    readonlyLabel: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
  
      fontFamily: "Roboto",
      backgroundColor: "transparent",
      fontSize: 11,
      color: "#7f7f7f",
      letterSpacing: 0,
      marginLeft: 15,
      paddingLeft: 0,
      paddingTop: 2,
      top: 1
  
    },
    readonlyInput: {
      flex: 2,
      alignSelf: 'flex-start',
      justifyContent: 'center',
      alignContent: 'center',
      width: '100%',
      backgroundColor: "transparent",
      fontSize: 16,
      // color: "#7f7f7f",
      color: "#c1c7c9",
      letterSpacing: 0,
      marginLeft: 15,
      paddingLeft: 0
    },
    itemDividerBoxStyle: {
      // backgroundColor: "#F8F8F8", backgroundColor: "rgba(13,55,84,0.03)",
      // backgroundColor: '#E9EEF1', backgroundColor: "#f2f3f4", backgroundColor:
      // "#707c80", backgroundColor: "#a7b0b2",
      backgroundColor: '#d8f2ff',
      flex: 1,
      flexDirection: 'row',
      alignItems: 'flex-start',
      height: 70,
      borderBottomColor: 'rgba(198,198,198,0.30)',
      borderBottomWidth: 1
  
    },
    itemDividerBoxIconStyle: {
      paddingTop: Platform.OS === 'ios'
        ? 1
        : 0,
      // fontFamily: "Ionicons", fontSize: 15, color: "#00AAFF", letterSpacing: 0,
      // lineHeight: 20,
      marginRight: 7,
      marginLeft: 15,
      alignItems: 'flex-start',
      alignSelf: 'center'
  
    },
    itemDividerBoxTextStyle: {
      flex: 8,
      fontFamily: "Roboto_medium",
      fontSize: 15,
      color: "#0D3754",
      letterSpacing: 0.79,
      alignItems: 'flex-start',
      marginVertical: 0,
      alignSelf: 'center'
    },
    page2:{
      flex: 1,
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 100,
      height: 50,
      fontSize: 21,
      color: '#17B1FE',
      letterSpacing: 0,
      margin: 10
    },
    page2Container:{
      flex: 1,
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    page2Icon:{
      fontFamily: 'Ionicons',
      fontWeight: 'bold',
      fontSize: 30,
      color: '#00AAFF',
      letterSpacing: 0,
      lineHeight: 40,
      justifyContent: 'center',
      marginHorizontal: 2
    },
    page2TextContainer:{
      flex: 8,
      width: '100%',
      height: '100%',
      justifyContent: 'center'
    },
    page2LabelContainer:{
      fontSize: 12,
      height: 50,
      color: '#A9ABAC',
      lineHeight: 24,
      flex: 1,
      textAlignVertical: 'center'
    },
    pickerTextDisabledStyle:{
      padding: 0,
      fontSize: 14,
      color: "#40505A",
      textAlign: 'left'
    },
    doubleRow:{
      flex: 6,
      flexDirection: 'row'
    },
    doubleUniContainer:{
      flex: 8
    },
    doublePencilContainer:{
      flex: 1,
      alignItems: 'flex-end',
      marginRight: 15
    },
    containerAnim:{
      flex: 1,
      height: '100%'
    }
  })
  
  export const stylesTablet = StyleSheet.create({
  
    titleStyle: {
      fontFamily: 'Roboto',
      fontSize: 25,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'center',
      alignItems: 'flex-start',
      marginTop: 5
    }
  })