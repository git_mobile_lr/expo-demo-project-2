import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({

  
    touchableRect: {
      width: '100%',
      height: '100%'
    },
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: undefined,
      width: undefined
    },
    cover: {
      flex: 1,
      height: '100%',
      width: '100%'
    },
    headerStyle: {
      backgroundColor: '#FFFFFF'
    },
    headerDeveloperStyle: {
      backgroundColor: 'orange'
    },
    leftStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: 'bold',
      color: '#00AAFF',
      letterSpacing: -0.53
    },
    titleStyle: {
      fontFamily: 'Roboto',
      fontSize: __DEV__ ? 10 : 17,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 5
    },
    rightStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: '500',
      color: '#00AAFF',
      letterSpacing: -0.53,
      alignSelf: 'flex-end',
      marginRight: 5,
      marginTop: 7
    },
    rightDisabledStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: '500',
      color: '#F8F8F8',
      letterSpacing: -0.53,
      alignSelf: 'flex-end',
      marginRight: 5,
      marginTop: 7
    },
    rightGreenStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: '500',
      color: '#00FF00',
      letterSpacing: -0.53,
      alignSelf: 'flex-end',
      marginRight: 5,
      marginTop: 7
    },
    contentStyle: {
      backgroundColor: '#F8F8F8'
    },
    insideStyle: {
      //color : '#00AAFF',
      backgroundColor: '#F8F8F8'
    },
    tabsStyle: {
      color: '#00AAFF',
      backgroundColor: '#F8F8F8'
    },
    tabStyle: {
      //color : '#AFBAC0',
      backgroundColor: '#F8F8F8'
    },
    tabTextStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#AFBAC0'
    },
    activeTabStyle: {
      //color : '#00AAFF',
      backgroundColor: '#F8F8F8'
    },
    activeTabTextStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#00AAFF'
    },
    tabBarUnderlineStyle: {
      backgroundColor: '#00AAFF'
    },
    listItem: {
      backgroundColor: '#FFFFFF'
    },
    cardStyle: {
      backgroundColor: 'transparent'
    },
    cardHeaderStyle: {
      fontFamily: 'Roboto_medium',
      fontSize: 13,
      backgroundColor: '#00AAFF',
      color: '#FFFFFF',
      letterSpacing: 0,
      height: 29,
      alignItems: 'center'
    },
    cardHeaderIconStyle: {
      fontFamily: 'Ionicons',
      fontSize: 14,
      backgroundColor: '#00AAFF',
      color: '#FFFFFF',
      fontWeight: '500'
    },
    cardBodyStyle: {
      backgroundColor: 'transparent',
      height: 162
    },
    headlineStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10
    },
    cardElemStyle: {
      backgroundColor: 'transparent',
      borderColor: 'transparent',
      borderWidth: 0
  
    },
    cardElemHeaderStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      backgroundColor: '#FFFFFF',
      color: '#5384A6',
      letterSpacing: 0,
      height: 18,
      alignItems: 'center'
    },
    cardElemBodyStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      backgroundColor: '#FFFFFF',
      color: '#000',
      letterSpacing: 0,
      height: 18,
      alignItems: 'center'
    },
    cardElemHeaderIconStyle: {
      fontFamily: 'Ionicons',
      fontSize: 14,
      backgroundColor: '#FFFFFF',
      color: '#5384A6',
      fontWeight: '500'
    },
    centering: {
      marginTop: 100,
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 1000
    },
    errorStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#F00',
      alignSelf: 'center'
    },
    errorHeaderStyle: {
      fontFamily: 'Roboto',
      fontSize: 18,
      color: '#F00',
      alignSelf: 'flex-start'
    }
  });

export  const stylesTablet = StyleSheet.create({
    touchableRect: {
      width: '100%',
      height: '100%'
    },
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: undefined,
      width: undefined
    },
    cover: {
      flex: 1,
      height: '100%',
      width: '100%'
    },
    headerStyle: {
      backgroundColor: '#FFFFFF'
    },
    leftStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: 'bold',
      color: '#00AAFF',
      letterSpacing: -0.53
    },
    titleStyle: {
      fontFamily: 'Roboto',
      fontSize: 25,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 5
    },
    rightStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: '500',
      color: '#00AAFF',
      letterSpacing: -0.53,
      alignSelf: 'flex-end',
      marginRight: 5,
      marginTop: 7
    },
    rightDisabledStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: '500',
      color: '#F8F8F8',
      letterSpacing: -0.53,
      alignSelf: 'flex-end',
      marginRight: 5,
      marginTop: 7
    },
    rightGreenStyle: {
      fontFamily: 'Ionicons',
      fontSize: 22,
      fontWeight: '500',
      color: '#00FF00',
      letterSpacing: -0.53,
      alignSelf: 'flex-end',
      marginRight: 5,
      marginTop: 7
    },
    contentStyle: {
      backgroundColor: '#F8F8F8'
    },
    insideStyle: {
      //color : '#00AAFF',
      backgroundColor: '#F8F8F8'
    },
    tabsStyle: {
      color: '#00AAFF',
      backgroundColor: '#F8F8F8'
    },
    tabStyle: {
      //color : '#AFBAC0',
      backgroundColor: '#F8F8F8'
    },
    tabTextStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#AFBAC0'
    },
    activeTabStyle: {
      //color : '#00AAFF',
      backgroundColor: '#F8F8F8'
    },
    activeTabTextStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#00AAFF'
    },
    tabBarUnderlineStyle: {
      backgroundColor: '#00AAFF'
    },
    listItem: {
      backgroundColor: '#FFFFFF'
    },
    cardStyle: {
      backgroundColor: 'transparent'
    },
    cardHeaderStyle: {
      fontFamily: 'Roboto_medium',
      fontSize: 13,
      backgroundColor: '#00AAFF',
      color: '#FFFFFF',
      letterSpacing: 0,
      height: 29,
      alignItems: 'center'
    },
    cardHeaderIconStyle: {
      fontFamily: 'Ionicons',
      fontSize: 14,
      backgroundColor: '#00AAFF',
      color: '#FFFFFF',
      fontWeight: '500'
    },
    cardBodyStyle: {
      backgroundColor: 'transparent',
      height: 162
    },
    headlineStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      color: 'rgba(13,55,84, 0.87)',
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: 10
    },
    cardElemStyle: {
      backgroundColor: 'transparent',
      borderColor: 'transparent',
      borderWidth: 0
  
    },
    cardElemHeaderStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      backgroundColor: '#FFFFFF',
      color: '#5384A6',
      letterSpacing: 0,
      height: 18,
      alignItems: 'center'
    },
    cardElemBodyStyle: {
      fontFamily: 'Roboto',
      fontSize: 13,
      backgroundColor: '#FFFFFF',
      color: '#000',
      letterSpacing: 0,
      height: 18,
      alignItems: 'center'
    },
    cardElemHeaderIconStyle: {
      fontFamily: 'Ionicons',
      fontSize: 14,
      backgroundColor: '#FFFFFF',
      color: '#5384A6',
      fontWeight: '500'
    },
    centering: {
      marginTop: 100,
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 1000
    },
    errorStyle: {
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#F00',
      alignSelf: 'center'
    },
    errorHeaderStyle: {
      fontFamily: 'Roboto',
      fontSize: 18,
      color: '#F00',
      alignSelf: 'flex-start'
    }
  });