import React, { Fragment } from 'react'
import _ from 'lodash'
import { styles, stylesTablet } from './Styles'
import {
  Alert,
  ActivityIndicator,
  BackHandler,
  StatusBar,
  TouchableOpacity,
  Platform,
  Dimensions,
  View
} from 'react-native'
import { ScrollView as Content } from 'react-native'
import {
  Body,
  Button,
  Container,

  Footer,
  Header,
  Icon,
  Left,
  Right,
  Text,
  Title,
  FooterTab
} from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Col, Row, Grid } from 'react-native-easy-grid'
import * as Animatable from 'react-native-animatable'
import { MenuBack, MenuBackDisabled } from '../components'
import MenuRenderer from '../components/menu/MenuRenderer'
import { FRenderer, FPointer, FHelp, FToast } from '../widgets'
import moment from 'moment'
import ActionButton from 'react-native-action-button'
import Lottie from 'lottie-react-native'
import { FActionButton, FSpinner } from '../../UILab'
import { inject, observer } from 'mobx-react/native'
const JUDGEMENT = require('../../assets/lottie/progress_hard.json')
import { Entypo, FontAwesome, AntDesign, MaterialIcons, Ionicons } from '@expo/vector-icons'
import { DrawerActions } from '@react-navigation/native';
interface Props {
  rootStore?: any
  navigation?: any
}

interface State {
  dataRecipe: string
  data: Array<any>
  userConfig: any
  hasInvalidState: boolean
  invalidState: any
  onboarding: boolean
  configLoaded: boolean
  userConfigLoaded: boolean,
  activeAppName: string
}

@inject('rootStore')
@observer
export default class WorkOrdersScreen extends React.Component<Props, State> {


  constructor(props: Props) {
    super(props)
    this.state = {
      dataRecipe: "WorkOrders",
      data: [],
      userConfig: this.getUserConfig(),
      hasInvalidState: false,
      invalidState: {},
      onboarding: false,
      configLoaded: false,
      userConfigLoaded: false,
      activeAppName: "",
      intervalId: null
    }
  }


  _unsubscribe: any

  componentDidUpdate(prevProps: Props, prevState: State) {
    const { userConfig, userConfigLoaded } = this.state
    if (!_.isEmpty(userConfig) && (userConfigLoaded == false)) {
      this.setState({ userConfigLoaded: true })
    }
    if (this.state.activeAppName != this.props.rootStore.appsStore.activeAppName) {
      this._loadAsync()
    }

  }

  async getUserConfig(dataRecipe: string = "WorkOrders") {
    const { loginStore } = this.props.rootStore
    const resultConfigByRecipe = await loginStore.getUserConfigByRecipeName(dataRecipe)
    if (resultConfigByRecipe.type === 'success') {
      return resultConfigByRecipe.data
    } else {
      return null
    }
  }


  askPermissions() { }
  async _loadAsync() {
    this.askPermissions()
    let that = this
    const { dataRecipe } = this.state
    const { loginStore, appsStore } = this.props.rootStore
    const resultConfigByRecipe = await loginStore.getUserConfigByRecipeName(dataRecipe)
    if (resultConfigByRecipe.type === 'success') {

      that.setState({
        userConfig: resultConfigByRecipe.data,
        activeAppName: appsStore.activeAppName
      })
    }
    return { type: 'success' }
  }

  async _delayedLoadAsync() {
    this
      ._loadAsync()
      .then(r => {
        // setTimeout(() => {
        //   this._requestHelpAction()
        // }, 2500)

      })
  }

  refresh() {
    const { changesStore, loginStore } = this.props.rootStore
    let delta = !_.isEmpty(changesStore.changesPerSyncId[loginStore.syncId])
    
    if (loginStore.isTriggerReload3) {
      loginStore.setIsTriggerReload3 = false
      this.setState({ userConfigLoaded: false })
    } else if (delta) {
      this.setState({ userConfigLoaded: false })
    }

   
  }


  async _reloadAsync(){

    await this._unloadAsync();
    this.setState({
      dataRecipe: "WorkOrders",
      data: [],
      userConfig: this.getUserConfig(),
      hasInvalidState: false,
      invalidState: {},
      onboarding: false,
      configLoaded: false,
      userConfigLoaded: false,
      activeAppName: ""
    });
    
    await this._preLoadAsync()
  }

  async componentDidMount() {
    await this._preLoadAsync()

    this._adjustToOrientation()
  }

  _dimensionsListener : any

  async _adjustToOrientation() {
    const isPortrait = () => {
      const dim = Dimensions.get('screen');
      return dim.height >= dim.width;
    };
    let o = isPortrait() ? 'portrait' : 'landscape'
    this.props.rootStore.loginStore.setOrientation(o);
    

    this._dimensionsListener = Dimensions.addEventListener('change', () => {
      const { loginStore, dataStore, permissionStore, appsStore } = this.props.rootStore
      // console.warn("loginStore.activeScreen " + loginStore.activeScreen)
      if(loginStore.activeScreen !== "WorkOrders"){
        return;
      }

      let o = isPortrait() ? 'portrait' : 'landscape'
      
      loginStore.setOrientation(o);
      
      
      
      this.refresh()
      loginStore.setSelectedTab(0)
      dataStore.clearActiveObjects()
      dataStore.clearCurrentProduct()
      loginStore.clearCurrentWorkOrder()
      
    });
  }


  

  async _preLoadAsync() {
    
    const { loginStore, dataStore, permissionStore } = this.props.rootStore
    // Navigation Listener. This is used to reload items everytime the screen is focused again from the navigation. Ex: when the user taps on back button from WorkOrderItems
    const { navigation } = this.props
    this._unsubscribe = navigation.addListener('focus', () => {
      
      this.refresh()
      loginStore.setSelectedTab(0)
      dataStore.clearActiveObjects()
      dataStore.clearCurrentProduct()
      loginStore.clearCurrentWorkOrder()
      loginStore.setDashboardDataWorkOrders("WorkOrders (focus)")

    })

    loginStore.setNavigation(navigation)
    permissionStore.checkAll()
    dataStore.clearActiveObjects()
    dataStore.clearCurrentProduct()
    loginStore.clearCurrentWorkOrder()
    loginStore.setDashboardDataWorkOrders("WorkOrders (init)")

    let intervalId = setTimeout(() => {
      this._delayedLoadAsync()
      
      loginStore.setSelectedTab(0)
    }, Platform.OS === 'ios'
      ? 125
      : 75)

    this.setState({
      intervalId: intervalId
    })

    BackHandler.addEventListener('hardwareBackPress', function () {
      return true
    })
  }

  componentWillUnmount() {
    
    try {
      
      this._dimensionsListener()
      //Dimensions.removeEventListener('change')
    } catch (error) {
      //pass
    }
    this._unloadAsync()

    this._clearInterval()

    
  }

  _clearInterval() {
    
    const { intervalId } = this.state
    
    if (intervalId) {
      clearInterval(intervalId || 0)
      this.setState({ intervalId: null })
    }
  }

  async _unloadAsync() {
    try {
      console.warn("unload")
      this._unsubscribe()
      BackHandler.removeEventListener('hardwareBackPress')
    } catch (error) {
      //pass   
    }
  }

  _menu = async () => {    
    const { navigation } = this.props
    navigation.dispatch(DrawerActions.toggleDrawer());
  }

  _menuImage() {
    const {
      isOffline,
      forceIsOffline,
      isDrawerBusy,
      useCaughtErrorNetworkRequest,
      isTablet
    } = this.props.rootStore.loginStore
  
    
    if (this.props.rootStore.loginStore.getIsOffline) {
      return <MenuRenderer size={isTablet ? 40 : 30} name='MenuOffline' />
    }
    if (isDrawerBusy) {
      return <MenuRenderer size={isTablet ? 40 : 30} name='MenuRefreshing' />
    }

    return <MenuRenderer size={isTablet ? 40 : 30} name='MenuNormal' />
  }

  

  _filterIcon() {
    const { loginStore } = this.props.rootStore
    const { isTablet, filterOutClosedWO } = loginStore

    return (
      <TouchableOpacity
        style={[
          styles.touchableRect, {
            flex: 1,
            alignItems: 'flex-end'
          }
        ]}
        onPress={async () => loginStore.setFilterOutClosedWO = !filterOutClosedWO}>
          <View
            style={{
              width: isTablet ? 40 : 30,
              height: isTablet ? 40 : 30,
              borderRadius: isTablet ? 20 : 15,
              backgroundColor: filterOutClosedWO ? '#00aaff' : '#fff',
              borderColor: filterOutClosedWO ? '#fff' : '#00aaff',
              borderWidth: filterOutClosedWO ? 0 : 2,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <MaterialIcons name="filter-list" size={isTablet ? 30 : 20} color={filterOutClosedWO ? "#fff" : '#00aaff'} />
          </View>
      </TouchableOpacity>
    )
  }

  async _helpAction() {
    const { loginStore } = this.props.rootStore
    if (!loginStore.isTooltipVisible) {
      loginStore.setIsTooltipVisible = true
    }
    loginStore.setIsHelpVisible = true

  }

  _requestHelpAction = () => {
    const { loginStore } = this.props.rootStore

    if (!loginStore.isTooltipVisible) {
      loginStore.setIsTooltipVisible = true
    }

  }

  _getChildren() {

    try {
      const { loginStore, appsStore, dataStore } = this.props.rootStore
      const { dashboardWorkOrders } = dataStore
      const { data, userConfig } = this.state

      return (<FRenderer data={dashboardWorkOrders} config={userConfig} />)
    } catch (error) {

    }

    return (<Container><Content><FSpinner /></Content></Container>)
  }

  _getTitle() {
    const { loginStore } = this.props.rootStore
    const { isDeveloperMode } = loginStore

    const _syncIdInfo = __DEV__ || isDeveloperMode ? ` N:${loginStore.getSyncId} ` : ""

    const _usingLocalConfig = __DEV__   && loginStore.usingLocalConfig ? " LOCAL" : ""

    if (this.state.userConfig) {
      const { userConfig } = this.state
      const _title = userConfig.header && userConfig.header.title
        ? userConfig.header.title
        : ""

      const _titleLabel = this
        .props
        .rootStore
        .loginStore
        .findLabel(_title)


      return _titleLabel + " " + _syncIdInfo + _usingLocalConfig
    }

    return _syncIdInfo
  }
  
  _getDeveloperToolbar() {
    try {

      const { loginStore, changesStore } = this.props.rootStore
      const { syncId } = loginStore
      const { syncStatus, syncResponse } = changesStore
      if (loginStore.isDeveloperMode) {
        const { isSyncing } = loginStore
        const _lottie = isSyncing
          ? (<Lottie
            ref={animation => {
              this.animation = animation;
              if (this.animation) {
                this
                  .animation
                  .play()
              }
            }}
            style={{
              flex: 1,
              height: hp('40%'),
              width: wp('90%')
            }}
            loop={true}
            source={JUDGEMENT} />)
          : (<Icon name="refresh-circle" />)

        return (
          <FooterTab style={{
            backgroundColor: 'orange'
          }}>
            <View style={{ position: "absolute", top: -45, backgroundColor: 'orange', width: "100%", padding: 8, }}>
              <Text style={{ fontSize: 11, color: "white" }}>{`Last Sync Response : ${syncResponse}`}</Text>
              <Text style={{ fontSize: 11, color: "white" }}>{`Current Sync Id : ${syncId} - Sync Status : ${syncStatus}`}</Text>
            </View>
            <Button
              disabled={isSyncing}
              vertical
              onPress={this
                ._onHardSync
                .bind(this)}>
              <Icon
                name="refresh-circle"
                style={{
                  color: '#fff'
                }} />

              <Text style={{
                color: '#fff'
              }}>Hard sync</Text>
            </Button>

            <Button
              disabled={isSyncing}
              vertical
              onPress={() => {
                this.props.rootStore.loginStore.setIsDeveloperMode = false
              }}>
              <Icon name="close" style={{
                color: '#fff'
              }} />
              <Text style={{
                color: '#fff'
              }}>Close</Text>
            </Button>
          </FooterTab>
        )
      }
    } catch (error) { }
    return null
  }

  _onHardSync() {

    Alert.alert('Hard Sync', `Are you sure? You will lose your last changes.`, [
      {
        text: 'Cancel',

        style: 'cancel'
      }, {
        text: 'OK',
        onPress: async () => {
          await this
            .props
            .rootStore
            .loginStore
            .sync(true)
        }

      }
    ], { cancelable: false })

  }

  _getHeaderStyle() {
    return this.props.rootStore.loginStore.isDeveloperMode
      ? styles.headerDeveloperStyle
      : styles.headerStyle
  }

  _goBackToList = async () => {
    const { loginStore } = this.props.rootStore

    loginStore.navigateAndGoSelectedDay("WorkOrders")

  }

  _renderLeft() {
    const {
      askedForPermissions,
      isSyncingLocked,
      isDrawerBusy,
      isOnboarding,
      isHelpVisible,
      isRestoring,
      isSyncing,
      isTriggerReload,     
      endReached,
      goToday,
      isCalendarOpen,
      isTablet
    } = this.props.rootStore.loginStore

    if (isCalendarOpen) {
      const _menu = isSyncingLocked
        ? (<MenuBackDisabled />)
        : (<MenuBack />)

      return (
        <TouchableOpacity
          disabled={isSyncingLocked}
          style={styles.touchableRect}
          transparent
          onPress={() => {
            this._goBackToList()
          }}>
          {_menu}
        </TouchableOpacity>
      )
    }
    
    return (
      <TouchableOpacity
        accessibilityLabel="Hamburger Button"
        testID="Hamburger Button iOS"
        style={styles.touchableRect}
        transparent
        onPress={() => this._menu()}>
        {this._menuImage()}
      </TouchableOpacity>
    )
  }
  render() {

    const { loginStore, appsStore, dataStore } = this.props.rootStore
    const { dashboardWorkOrders } = dataStore

    const {
      askedForPermissions,
      inFlight,
      isDrawerBusy,
      isOnboarding,
      isHelpVisible,
      isRestoring,
      isSyncing,
      isTriggerReload,
      isTriggerReload6,
      endReached,
      goToday,
      isCalendarOpen,
      syncId,
      isDeveloperMode,
      stopwatchIsRunning,
      timerWO,
      isTablet
      
    } = this.props.rootStore.loginStore
    const { showReleaseNotes } = this.props.rootStore.settingsStore.rules.otherDefaults
    const { activeAppName } = appsStore

    if (this.state.userConfigLoaded == false)       return (<Container><Content><FSpinner/></Content></Container>)

    if (syncId >= 0) {
      const { hasInvalidState, userConfig } = this.state


      const children = this._getChildren()



      const _actionButtons = this._renderActionButtons()
      const _titleStyle = this.props.rootStore.loginStore.isTablet
        ? stylesTablet.titleStyle
        : styles.titleStyle

      const _left = this._renderLeft()
      const _statusBar = this._renderStatusBar()
      const { isTooltipVisible } = loginStore
      return (
        <Animatable.View
          animation="bounceInUp"
          
          duration={isTriggerReload6 ? 0 : this.props.rootStore.loginStore.ANIMATION_DURATION}
          style={{
            flex: 1,
            backgroundColor: '#E9EEF1',
            height: '100%'
          }}
          useNativeDriver>
          <React.Fragment>
            {_statusBar}
            <Container>

              <Header style={this._getHeaderStyle()}>
                <Left>
                  {_left}
                  
                </Left>
                <Body style={{ flex: 1, alignItems : "center",  flexDirection : "row", justifyContent: 'center'}}>
                  {stopwatchIsRunning ? 
                  <TouchableOpacity
                  accessibilityLabel="Work Order Screen Stopwatch"
                  testID="Work Order Screen Stopwatch iOS"
                  onPress={this._stopwatchRedirection.bind(this)}>
                    <FontAwesome name="clock-o" size={isTablet ? 30 : 20} color="orange" style={{paddingRight : isTablet ? 20 : 10, top : isTablet ? 5 : 2.5}}/>
                  </TouchableOpacity> : <View></View>
                  }
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                    accessibilityLabel="Work Order Screen Title"
                    testID="Work Order Screen Title iOS"
                    onLongPress={this
                      ._onSlackChanges
                      .bind(this)}>
                    <Title style={_titleStyle}>
                      <Text style={_titleStyle}>{this._getTitle()}</Text>
                    </Title>
                  </TouchableOpacity>
                </Body>
                <Right
                  style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}>
                  {this._useReloadScreen()}
                  {this._changeSessionId()}
                  {this._useCaughtErrorNetworkRequestAndGotoBackground()}
                  {this._useCaughtErrorNetworkRequest()}
                  {this._lbwoGenerator(loginStore)}
                  {this._filterIcon()}
                </Right>
              </Header>
              <Content 
                scrollEnabled={false}
                ref="MainContent"
                keyboardShouldPersistTaps={'handled'}
              >
                
                {isTooltipVisible && <FToast />}
               

                {children}

              </Content>
              <Footer
                style={{
                  height: isDeveloperMode
                    ? hp('10%')
                    : 1
                }}>
                {this._getDeveloperToolbar()}
              </Footer>
              {_actionButtons}

            </Container>
          </React.Fragment>
        </Animatable.View>
      )
    } else {
      return (<Container><Content><FSpinner/></Content></Container>)
    }


  }

  _renderStatusBar() {
    const { isSyncing, isSyncingLocked } = this.props.rootStore.loginStore
    return (
      <StatusBar networkActivityIndicatorVisible={isSyncing}></StatusBar>
    )
  }

  _partialRender() {
    const { isDrawerBusy } = this.props.rootStore.loginStore
    const _titleStyle = this.props.rootStore.loginStore.isTablet
      ? stylesTablet.titleStyle
      : styles.titleStyle

    return (
      <Container>

        <Header style={this._getHeaderStyle()}>
          <Left>
            <TouchableOpacity
              disabled={true}
              style={styles.touchableRect}
              transparent
              onPress={() => { }}>
              {this._menuImage()}
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={_titleStyle}>
              <Text style={_titleStyle}>{this._getTitle()}</Text>
            </Title>
          </Body>
          <Right ></Right>
        </Header>

        <Content keyboardShouldPersistTaps={'handled'} scrollEnabled={false}>
          {this._busy()}

        </Content>
      </Container>
    )
  }

  _partialError() {

    const { hasInvalidState, invalidState } = this.state
    const _titleStyle = this.props.rootStore.loginStore.isTablet
      ? stylesTablet.titleStyle
      : styles.titleStyle
    return (
      <Container>
        <Header style={this._getHeaderStyle()}>
          <Left>
            <TouchableOpacity
              style={styles.touchableRect}
              transparent
              onPress={() => this._menu()}>
              {this._menuImage()}
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={_titleStyle}>
              <Text style={_titleStyle}>{this._getTitle()}</Text>
            </Title>
          </Body>
          <Right ></Right>
        </Header>
        <Content keyboardShouldPersistTaps={'handled'} scrollEnabled={false}>

          <Grid
            style={{
              margin: 5,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
            <Row>
              <Col size={15}>
                <Icon
                  ios='ios-bug'
                  name='bug'
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'red'
                  }} />
              </Col>
              <Col size={75}>
                <Text style={styles.errorHeaderStyle}>Something went wrong...</Text>

              </Col>
            </Row>
            <Row>
              <Col size={15}>
                <Icon
                  ios='ios-bug'
                  name='bug'
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'red'
                  }} />
              </Col>
              <Col size={75}>
                <Text style={styles.errorHeaderStyle}>{invalidState.errno}: {invalidState.message}</Text>
              </Col>
            </Row>
            <Row>
              <Col size={15}>
                <Icon
                  ios='ios-bug'
                  name='bug'
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'red'
                  }} />
              </Col>
              <Col size={75}>
                <Text style={styles.errorHeaderStyle}>Logout might be required.</Text>
              </Col>
            </Row>

          </Grid>
        </Content>
      </Container>
    )
  }

  _renderActionIconCreateWO() {

    if (this.props.rootStore.loginStore.getIsOffline) {
      return null
    }

    const _calendarFontSize = this.props.rootStore.loginStore.isTablet
      ? 60
      : 40

    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}>

        <Icon
          ios='ios-add'
          android='md-add'
          name='calendar'
          style={{
            color: '#FFF',
            fontSize: _calendarFontSize,
            alignSelf: 'center',
            flex: 1,
            position: 'absolute',
            zIndex: 2
          }}></Icon>

      </View>
    )

  }
  _renderActionIconGoToday() {
    const _DD = moment().format("D")

    const _calendarFontSize = this.props.rootStore.loginStore.isTablet
      ? 60
      : 40
    const _fontSize = this.props.rootStore.loginStore.isTablet
      ? 32

      : (Platform.OS === 'ios'
        ? 18
        : 16)

    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}>

        <Icon
          ios='ios-calendar'
          android='md-calendar'
          name='calendar'
          style={{
            color: '#FFF',
            fontSize: _calendarFontSize,
            alignSelf: 'center',
            flex: 1,
            opacity: Platform.OS === 'ios'
              ? 0.7
              : 0.5,
            position: 'absolute',
            zIndex: 2
          }}></Icon>
        <Text
          style={{
            backgroundColor: 'transparent',
            color: '#FFF',
            opacity: 1,
            fontFamily: 'Ionicons',
            fontSize: _fontSize,
            fontWeight: 'bold',
            paddingTop: 4,
            alignSelf: 'center',
            flex: 1,
            position: 'absolute',
            zIndex: 4
          }}>{_DD}</Text>

      </View>
    )

  }

  _renderGoToday() {
    const { loginStore } = this.props.rootStore
    return (<ActionButton
      size={this.props.rootStore.loginStore.isTablet
        ? 100
        : 56}
      renderIcon={this
        ._renderActionIconGoToday
        .bind(this)}
      buttonColor="rgba(0,170,255, 1)"
      style={{
        zIndex: 99999999,
        marginBottom: '5%',
        marginRight: -20
      }}
      onPress={() => {
        loginStore.setGoToday = true
      }} />)
  }
  _renderCreateWorkOrder() {
    const { loginStore } = this.props.rootStore
    if (loginStore.allowsWorkOrderCreation()) {
      return (<FActionButton
        add
        warning
        onPress={() => {
          loginStore.navigate("CreateWorkOrder")
        }}
        event={loginStore.getWorkOrderCreationEvent()} />)
    }
  }

  _renderActionButtons() {
    const { loginStore } = this.props.rootStore

    if (loginStore.isCalendarOpen) {
      return this._renderGoToday()
    } else {
      if (!loginStore.getIsOffline) {
        return this._renderCreateWorkOrder()
      }
    }
  }

  _busy() {
    const { isDrawerBusy, isRestoring } = this.props.rootStore.loginStore
    const { hasInvalidState } = this.state

    if (isDrawerBusy || isRestoring || !hasInvalidState) {
      // this._checkConnectivity()

      return (<ActivityIndicator
        style={styles.centering}
        color="#1EB1F8"
        size="large"
        animating={true} />)
    }
  }

  _stopwatchRedirection() {
    const { loginStore, appsStore, dataStore } = this.props.rootStore
    try {


      const { timerWO, stopwatchStartTime } = loginStore
      const workOrderTable = loginStore.getPrefixedTableName("Work_Order__c")
      const timerWorkOrder = loginStore.getRecordById(workOrderTable, timerWO)
      let params = [
        {
          "var": "NAME",
          "table": "Work_Order__c",
          "field": "Name",
          "value": timerWorkOrder.Name
        },
        {
          "var": "ID",
          "table": "Work_Order__c",
          "field": "Id",
          "value": timerWorkOrder.Id
        },
        {
          "activeItems": dataStore.activeItems,
          "previousActiveTab": loginStore.selectedTab,
          "nextActiveTab": 0,
          "from": dataStore.getCurrentWorkOrder() ? dataStore.getCurrentWorkOrder().Name : ""
        }
      ]
      loginStore.currentWorkOrderId = timerWorkOrder.Id
      if (timerWO) {
        loginStore.navigation.navigate("WorkOrderDetails", params)
      }
      else {
        appsStore.setActiveAppName("TimeRegistration")
        if (stopwatchStartTime) {
          loginStore.selectedDay = moment(stopwatchStartTime).format("YYYY-MM-DD")
        }
        loginStore.navigation.navigate("TimeRegistrationDashboard", params)
      }
    } catch (error) {
      //pass   
    }
  }

  async _onSlackChanges() {

    const { loginStore } = this.props.rootStore
    //   if (__DEV__) {
    //     this.props.rootStore.changesStore.setChanges(4, [{
    //         "salesforceId": "a0w0N00000JYTBfQAP",
    //         "target": "Work_Order__c",
    //         "changeType": "UPDATE",
    //         "changeId": "c51a1bd74cbc1324cb91640631921a71",
    //         "changeData": {
    //             "Type__c": "INSTALLATION"
    //         }
    //     }])
    // }  
    const _changes = this.props.rootStore.changesStore.getAllChanges()

    if (_.isEmpty(_changes)) {
      if (loginStore.isDeveloperMode || __DEV__) {
        Alert.alert('No changes', 'No changes registered', [
          {
            text: 'OK'
          }
        ], { cancelable: false })

      }

    } else {

      // Alert.alert('changes', _changes, [
      //   {
      //     text: 'OK'
      //   }
      // ], {cancelable: false})


      await loginStore._slackChanges()
    }

    // await this._showInitAts()
  }

  async _showInitAts() {

    const { loginStore } = this.props.rootStore
    if (loginStore.isDeveloperMode || __DEV__) {
      const _wos = loginStore.getInitAtNames()
      const _list = (_wos || []).length
      Alert.alert(`${_list} item(s)`, _wos, [
        {
          text: 'OK'
        }
      ], { cancelable: false })
    }
  }

  async _generateLocationBasedWorkOrder(loginStore:any) {
    let testAutomationParams = loginStore.getTestAutomationParams()
    if (testAutomationParams) {
      let ownerId = testAutomationParams.ownerId
      let baseWorkOrderId = testAutomationParams.baseWorkOrderId
      let connectedUserId = loginStore.userInfo.user_id
      if (!ownerId || ownerId != connectedUserId) {
        ownerId = connectedUserId
      }

      await loginStore.generateLocationBasedWO(ownerId, baseWorkOrderId)
    }
  }

  _lbwoGenerator(loginStore:any) {
    const { isTablet } = loginStore
    let testAutomationParams = loginStore.getTestAutomationParams()
    if (testAutomationParams && testAutomationParams.displayTestAutomationElements) {
      return (
        <TouchableOpacity
          style={[
            styles.touchableRect, {
              flex: 1,
              width: 15,
              alignItems: 'flex-end',
              zIndex:1000
            }
          ]}
          accessibilityLabel="LBWO Generator"
          testID="LBWO Generator iOS"
          onPress={async() => {await this._generateLocationBasedWorkOrder(loginStore)}}>
          <Entypo name="database" size={isTablet ? 30 : 20} color='#00aaff' style={{paddingHorizontal: 5}} />
        </TouchableOpacity>
      )
    }
  }

  _toggleCaughtErrorNetworkRequest() {
    const { loginStore } = this.props.rootStore

    loginStore.useCaughtErrorNetworkRequest = !loginStore.useCaughtErrorNetworkRequest
  }

  _toggleCaughtErrorNetworkRequestAndGotoBackground() {
    const { loginStore } = this.props.rootStore

    loginStore.useCaughtErrorNetworkRequestAndGotoBackground = !loginStore.useCaughtErrorNetworkRequestAndGotoBackground
  }

  _useCaughtErrorNetworkRequest() {
    const { isTablet, useCaughtErrorNetworkRequest, isTesterMode, isDeveloperMode } = this.props.rootStore.loginStore

    if (isTesterMode ||  isDeveloperMode) {




      return (
        <TouchableOpacity
          style={[
            styles.touchableRect, {
              flex: 1,
              width: 15,
              alignItems: 'flex-end',
              zIndex: 1000
            }
          ]}
          accessibilityLabel="UseCaughtErrorNetworkRequest Button"
          testID="UseCaughtErrorNetworkRequest Button iOS"
          onPress={this._toggleCaughtErrorNetworkRequest.bind(this)}>
          <Entypo name={useCaughtErrorNetworkRequest ? "notifications-off" : "notification" } size={isTablet ? 30 : 20} color={useCaughtErrorNetworkRequest ? "red" : "green"} style={{ paddingHorizontal: 5 }} />
        </TouchableOpacity>
      )
    }

  }

  _useCaughtErrorNetworkRequestAndGotoBackground() {
    const { isTablet, useCaughtErrorNetworkRequestAndGotoBackground, isTesterMode, isDeveloperMode } = this.props.rootStore.loginStore

    if (isTesterMode ||  isDeveloperMode) {




      return (
        <TouchableOpacity
          style={[
            styles.touchableRect, {
              flex: 1,
              width: 15,
              alignItems: 'flex-end',
              zIndex: 1000
            }
          ]}
          accessibilityLabel="UseCaughtErrorNetworkRequestAndGotoBackground Button"
          testID="UseCaughtErrorNetworkRequestAndGotoBackground Button iOS"
          onPress={this._toggleCaughtErrorNetworkRequestAndGotoBackground.bind(this)}>
          <Entypo name={useCaughtErrorNetworkRequestAndGotoBackground ? "browser" : "browser" } size={isTablet ? 30 : 20} color={useCaughtErrorNetworkRequestAndGotoBackground ? "red" : "green"} style={{ paddingHorizontal: 5 }} />
        </TouchableOpacity>
      )
    }

  }

  async _toggleUseReloadScreen() {
    const { loginStore } = this.props.rootStore    
    loginStore.setIsTriggerReload6 = true
     await loginStore.forceSetActiveApp()
  }

  _useReloadScreen() {
    const { isTablet, isTriggerReload6, isTesterMode, isDeveloperMode } = this.props.rootStore.loginStore
    return null;
    
    if (isTesterMode || __DEV__ || isDeveloperMode) {
      return (
        <TouchableOpacity
          style={[
            styles.touchableRect, {
              flex: 1,
              width: 15,
              alignItems: 'flex-end',
              zIndex: 1000
            }
          ]}
          accessibilityLabel="UserReloadScreen Button"
          testID="UserReloadScreen Button iOS"
          onPress={this._toggleUseReloadScreen.bind(this)}>
          <AntDesign name={isTriggerReload6 ? "play" : "playcircleo"}
            size={isTablet ? 30 : 20} color={isTriggerReload6 ? "red" : "green"}
            style={{ paddingHorizontal: 5 }} />
        </TouchableOpacity>
      )
    }
  }

  async _goChangeSessionId(){

    await this.props.rootStore.loginStore.deleteSessionId()

  }

  _changeSessionId() {
    const { isTablet, isTesterMode, isDeveloperMode } = this.props.rootStore.loginStore
    
    
    if (isTesterMode ||  isDeveloperMode) {
      return (
        <TouchableOpacity
          style={[
            styles.touchableRect, {
              flex: 1,
              width: 15,
              alignItems: 'flex-end',
              zIndex: 1000
            }
          ]}
          accessibilityLabel="Change SessionId Button"
          testID="Change SessionId Button iOS"
          onPress={this._goChangeSessionId.bind(this)}>
          <AntDesign name="reload1"
            size={isTablet ? 30 : 20} color="orange"
            style={{ paddingHorizontal: 5 }} />
        </TouchableOpacity>
      )
    }
  }

}

