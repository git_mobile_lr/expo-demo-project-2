import React, { Component } from 'react';
import { View } from 'native-base';
import FRenderer from '../../../screens/widgets/FRenderer';
import {inject, observer} from "mobx-react/native";
import _ from "lodash";

interface Props {
    data:any
    rootStore:any
    config:any
    children?: JSX.Element
}

interface State {
    data: any
    item: any
    condition: any
    isValid: boolean
}


@inject('rootStore')
@observer
class Box extends Component<Props,State> {
    constructor(props:Props) {
        super(props);
        this.state = {
            data: props.data? props.data : null,
            item : props.config.item ? props.config.item : null,
            condition: props.config.condition ? props.config.condition : null,
            isValid: props.config.condition ? props.rootStore.validationStore.checkCondition(props.config.condition) : true
        };
    }

    componentDidUpdate(prevProps:Props,prevState:State){
        if ( !_.isEqual(prevProps.data, this.state.data )) {
            this.setState({
                data: this.props.data,
                item : this.props.config.item,
                isValid: this.props.config.condition ? this.props.rootStore.validationStore.checkCondition(this.props.config.condition) : true
            })
        }
    }

    render() {

        const { dataStore } = this.props.rootStore

        const { item, condition, data, isValid } = this.state

        return isValid ? <FRenderer data={data ? data : {}} config={ item } /> : <View />
    }
}

//@ts-ignore
Box.defaultProps = {
    children : <View />
}

export default Box;