import { StyleSheet, Platform } from 'react-native'

export const styles = StyleSheet.create({
    logoContainer:{
        alignSelf: 'center',
        alignContent: 'center',
        alignItems: 'center' 
    },
    backgroundImage:{
        flex: 1,
        margin: 0,
        paddingTop: (Platform.OS) === 'ios' ? 0 : 20,
        height: undefined,
        width: Platform.OS === 'ios' ? undefined : '100%'
    },
    spinnerContainer:{
        width:"100%",
        height:"100%",
        alignItems:"center",
        justifyContent:"center"
    }
})