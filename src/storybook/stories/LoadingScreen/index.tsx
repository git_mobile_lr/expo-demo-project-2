
import React from 'react'
import _ from 'lodash'
import { FSpinner } from '../../../UILab'
import { ImageBackground , Platform, View } from 'react-native'
import { FieldBuddyLoginLogo } from '../../../screens/components'
import {Col, Row, Grid} from 'react-native-easy-grid'
import { styles } from './styles'

const Loading = () =>{
    return (
          <Grid>
            <Col>
              <Row 
                size={1}
                style={styles.logoContainer}>
                <FieldBuddyLoginLogo/>
              </Row>
              <Row 
                size={2}>
                <ImageBackground
                  source={require('../../../assets/images/screens/login/login-screen-circles-background.png')}
                  style={styles.backgroundImage}
                  resizeMode={Platform.OS === 'ios' ? "cover" : "stretch"}>
                  <View 
                    style={styles.spinnerContainer} 
                    >
                    <FSpinner white/>
                  </View>
                </ImageBackground>
              </Row>
            </Col>
          </Grid>    
    )
  }

export default Loading