import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    roundedCard:{
        width:"100%",
        flexDirection:"column",
        borderTopLeftRadius:15,
        borderTopRightRadius:15,
        borderBottomLeftRadius:15,
        borderBottomRightRadius:15,
        backgroundColor:'white',
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#00AAFF',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 1
    },
    card:{
        width:"100%",
        flexDirection:"column",
        backgroundColor:'white',
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#00AAFF',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 1
    }
})

export default styles