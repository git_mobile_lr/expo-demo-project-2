import React, { Component } from 'react';
import { SafeAreaView } from 'react-native'
import { View, Text } from 'native-base'
import styles from './styles'
import FRenderer from '../../../screens/widgets/FRenderer'
import _ from 'lodash'
interface Props {
    rounded?:boolean
    children?: JSX.Element
    config:any
    data:any
}


export default function Card ( props : Props )  {
    const { rounded } = props
    const sections = props.config.sections ? props.config.sections : []
     
    return (
        <View style={rounded ? styles.roundedCard : styles.card}>
            {
                 _.isEmpty(sections) 
                    ? <View />
                    : sections.map((section:any, index:number)=>{
                        return <FRenderer key={index} data={props.data} config={section}/>
                    })
            }
        </View>
    )
}

Card.defaultProps = {
    children : <View />,
    rounded: true
}