import React,{Component} from 'react';
import { View } from 'native-base'
import styles from './styles'
import _ from "lodash"
import { Text, TouchableHighlight } from 'react-native'
import { inject, observer } from "mobx-react/native"
import { FIcon } from '../../../UILab'

interface Props {
    rounded: boolean
    data: any
    rootStore: any
}

interface State {

}

@inject('rootStore')
@observer
class index extends Component<Props,State> {
    constructor(props:Props) {
        super(props);
        this.state = {  };
    }
    render() {
        const { rounded, data } = this.props
        const { loginStore } = this.props.rootStore
        const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
        const TYPE = loginStore.getPrefixedFieldName("Type__c")
        const WO = loginStore.getPrefixedFieldName("Work_Order__c")
        const TEMPLATE = loginStore.getPrefixedFieldName("Template__c")
        const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
        let wolis = data[WOLI]
        wolis = _.uniqBy(wolis,(x:any)=>x.Id)
        let groupedWolis = _.groupBy(wolis,TYPE)
        let groupedWolisByIP = _.groupBy(wolis,IP)
        let isLocationBased = this.props.data[WO][TEMPLATE] == "Location"
 
        return (
                <View style={rounded ? styles.roundedCardFooter : styles.cardFooter}>
                    <View style={{flexDirection:'row',justifyContent:"space-around"}}>
                    {
                        !isLocationBased && 
                        _.isEmpty(groupedWolis) 
                            ? <Text style={{color:"white"}}>{loginStore.getLabelorDefault("${Label.NoItems}","No Items")}</Text> 
                            : null
                    }
                    {
                        !isLocationBased &&
                        Object.entries(groupedWolis).map((group:any,index:number)=>{
                              
                        return (
                            <View key={index} style={{flexDirection:'row',paddingRight:24,alignContent:"center",alignItems:"center"}}>
                                <Text style={{color:"white"}}> {group[1].length} </Text>
                                {
                                    group[0] == "Activity" 
                                        ? <FIcon color={"white"} size={13} icon={"Wrench"}/> 
                                        : group[0] == "Part" 
                                        ? <FIcon color={"white"} size={13} icon={"Cube"}/> 
                                        : <FIcon color={"white"} size={13} icon={"Card"} /> 
                                }
                                <Text style={{color:"white"}}> {group[0]} </Text>
                                
                            </View>
                        )
                        })
                    }
                    {
                        isLocationBased && 
                        <Text style={{color:"white"}}>
                            {Object.entries(groupedWolisByIP).length} {loginStore.getLabelorDefault("${Label.Installations}","Installation(s)")}
                        </Text>
                    }
                    </View>
                    
                </View>
            
        );
    }
}

export default index;

//@ts-ignore
index.defaultProps = {
    rounded:true
}