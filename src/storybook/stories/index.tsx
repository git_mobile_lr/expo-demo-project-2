import React from 'react';
import { View } from 'react-native'
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import CenterView from './CenterView';
import Welcome from './Welcome';
import CardHeader from './CardHeader'
import CardFooter from './CardFooter'
import CardBody from './CardBody'
import Card from './Card'
import LabelValue from './LabelValue'

storiesOf('Welcome', module).add('Swift Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Card', module)
   .addDecorator( (getStory:any) => <CenterView>{getStory()}</CenterView>)
   .add('rounded card', () => (
      <Card rounded>
         <View>
            <CardHeader rounded textLeft={"WO-123123"} textRight={"Open"} />
            <LabelValue inline label={"Name"} value={"WO-123123"}/>
            <LabelValue inline label={"Type"} value={"Installation"}/>
            <LabelValue inline label={"Installed Product"} value={"Solar Panel 150kw"}/>
            <CardFooter rounded amountItems={1} label={"item"} />
         </View>
      </Card>
   ))

   .add('rounded card no footer', () => (
      <Card rounded>
         <View>
            <CardHeader rounded textLeft={"WO-123123"} textRight={"Open"} />
            <LabelValue inline label={"Name"} value={"WO-123123"}/>
            <LabelValue inline label={"Type"} value={"Installation"}/>
            <LabelValue inline label={"Installed Product"} value={"Solar Panel 150kw"}/>
         </View>
      </Card>
   ))

   .add('squared card', () => (
      <Card>
         <View>
            <CardHeader textLeft={"WO-123123"} textRight={"Open"} />
            <LabelValue inline label={"Name"} value={"WO-123123"}/>
            <LabelValue inline label={"Type"} value={"Installation"}/>
            <LabelValue inline label={"Installed Product"} value={"Solar Panel 150kw"}/>
            <CardFooter amountItems={1} label={"item"} />
         </View>
      </Card>
   ))

storiesOf('CardHeader', module)
   .addDecorator((getStory:any) => <CenterView>{getStory()}</CenterView>)
   .add('with no props', () => (
      <CardHeader />
   ))
   .add('rounded ', () => (
      <CardHeader rounded textLeft={"WO-123123"} textRight={"Open"} />
   ))
   .add('with values', () => (
      <CardHeader textLeft={"WO-123123"} textRight={"Open"} />
   ))

storiesOf('CardFooter', module)
   .addDecorator((getStory:any) => <CenterView>{getStory()}</CenterView>)
   .add('with no props', () => (
      <CardFooter />
   ))
   .add('with values', () => (
      <CardFooter amountItems={1} label={"item"} />
   ))
   .add('rounded', () => (
      <CardFooter rounded amountItems={1} label={"item"} />
   ))

storiesOf('CardBody', module)
   .addDecorator((getStory:any) => <CenterView>{getStory()}</CenterView>)
   .add('with no props', () => (
      <CardBody />
   ))

   storiesOf('LabelValue', module)
   .addDecorator((getStory:any) => <CenterView>{getStory()}</CenterView>)
   .add('with no props', () => (
      <LabelValue />
   ))
   .add('inline', () => (
      <View>
         <LabelValue inline label={"Name"} value={"WO-123123"}/>
      </View>
      
   ))