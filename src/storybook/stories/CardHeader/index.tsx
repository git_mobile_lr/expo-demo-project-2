
import React,{Component, useState, useEffect} from 'react';
import styles from './styles'
import { Text, View, Grid, Col } from 'native-base'
import _ from 'lodash'
import FRenderer from '../../../screens/widgets/FRenderer'

interface Props  {
    config: any
    data: any
    rounded: boolean
    textLeft: string
    textRight: string
}

export default function CardHeader(props:Props){

    const [data,setData] = useState(props.data)
    const [config,setConfig] = useState(props.config)
    const [sections,setSections] = useState(props.config.sections)

    return (
        <View style={props.rounded ? styles.roundedCardHeader : styles.cardHeader}>
            <Grid>
            {
                sections.map((section:any,index:number)=>{
                    return <Col key={index} style={{alignContent:"center",alignItems:"center"}}><FRenderer data={data} config={section} /></Col>
                })
            }
            </Grid>
        </View>
    );
}

CardHeader.defaultProps = {
    rounded:true,
    config: {},
    data: {}
};
