import {StyleSheet} from 'react-native'
import { styled } from '@storybook/theming';

const styles = StyleSheet.create({
    roundedCardHeader:{
        height:50,
        width:"100%",
        paddingTop:12,
        paddingBottom:12,
        paddingLeft:24,
        paddingRight:24,
        borderTopLeftRadius:15,
        borderTopRightRadius:15,
        padding:5,
        flexDirection: 'row',
        backgroundColor:'#00AAFF',
        justifyContent: 'space-between',
        alignItems:"center"
    },
    cardHeader : {
        width:"100%",
        paddingTop:12,
        paddingBottom:12,
        paddingLeft:24,
        paddingRight:24,
        padding:5,
        flexDirection: 'row',
        backgroundColor:'#00AAFF',
        justifyContent: 'space-between',
        alignItems:"center"
    },
    textLeft:{
        color:"white",
        fontSize:14
    },
    textRight:{
        color:"white",
        fontSize:14
    }
})

export default styles