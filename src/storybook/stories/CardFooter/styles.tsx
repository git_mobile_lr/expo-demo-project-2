import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    roundedCardFooter:{
        height:50,
        width:"100%",
        borderBottomLeftRadius:15,
        borderBottomRightRadius:15,
        padding:10,
        backgroundColor:'#00AAFF',
        alignItems:"center",
        justifyContent:"center"
    },
    cardFooter:{
        width:"100%",
        padding:10,
        backgroundColor:'#00AAFF',
        alignItems:"center",
        
    },
    FModal:{
        height:10,
        borderColor: 'transparent',
        alignItems: "center",
        justifyContent: "center"
    },
    buttonRow:{
        flexDirection:"row"
    },
    buttonText:{
        color:"white",
        alignSelf:"center"
    },
    modalButton:{
        width:"100%",
        color:"white",
        fontFamily: 'Roboto',
        fontSize: 24,
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0,
        textTransform: 'capitalize',
        alignContent:"center",
        alignItems:"center"
    }
})

export default styles