import React,{Component} from 'react';
import { View } from 'native-base'
import styles from './styles'
import _ from "lodash"
import { Text, TouchableHighlight,TouchableOpacity } from 'react-native'
import { inject, observer } from "mobx-react/native"
import { FIcon } from '../../../UILab'
import FRenderer from '../../../screens/widgets/FRenderer';


interface Props {
    data:any
    rounded:boolean
    amountItems: number
    label: string
}

@inject('rootStore')
@observer
class index extends Component {
    constructor(props:Props) {
        super(props);
        this.state = {  };
    }

    handleFooterPress = async (workOrderName, workOrderId) => {
        const { loginStore, dataStore } = this.props.rootStore


        const WO = loginStore.getPrefixedFieldName("Work_Order__c")

        let targetWO = await loginStore.getRecordById(WO, workOrderId)
        let isTargetWoSRbWO = true
        if (targetWO !== null) {
            isTargetWoSRbWO = loginStore.isServiceRequestBased(targetWO)
        }
        
        dataStore.setCurrentRelatedWorkOrder(workOrderId)
        

        let nextActiveTab = isTargetWoSRbWO ? 2 : 1

        let params = [
            {
                "var": "NAME",
                "table": "Work_Order__c",
                "field": "Name",
                "value": workOrderName
            },
            {
                "var": "ID",
                "table": "Work_Order__c",
                "field": "Id",
                "value": workOrderId
            },
            {
                "nextActiveTab": nextActiveTab,
                "activeItems": dataStore.activeItems,
                "from": dataStore.getCurrentWorkOrder() ? dataStore.getCurrentWorkOrder().Name : ""
            }
        ]
        if (!loginStore.isRelatedWorkOrder()) {

            loginStore.navigation.push("WorkOrderDetails", params)
        }
    }

    render() {

        if ( this.props.data && this.props.config && this.props.config.item){
            const { loginStore } = this.props.rootStore
            const WO = loginStore.getPrefixedFieldName("Work_Order__c")
            const name = this.props.data[WO] ? this.props.data[WO].Name : ""
            const id = this.props.data[WO] ? this.props.data[WO].Id : ""
            return(
            <TouchableOpacity onPress={()=>{this.handleFooterPress(name,id)}}>
                <FRenderer data={this.props.data} config={this.props.config.item} />
            </TouchableOpacity>
            )
        }
        
        return <View />
    }
}

export default index;

//@ts-ignore
index.defaultProps = {
    rounded:true,
    amountItems: 0,
    label: "Items",
};