import React, { Component, useState } from 'react';
import { SafeAreaView } from 'react-native'
import { View, Text } from 'native-base'
import styles from './styles'
import _ from 'lodash'
import {inject, observer} from "mobx-react/native";
interface Props {
    config:any
    data:any
    label:string,
    value:string,
    inline?:boolean
}

interface State {

}
@inject('rootStore')
@observer
class LabelValue extends Component<Props,State> {
    constructor(props:Props) {
        super(props);
        this.state = {
            table: this.getTable(),
            field: this.getField(),
            value: this.getValue()
        };
    }

    getTable(){
        if ( this.props.config && this.props.config.dataBinding) {
            if ( !_.isEmpty( this.props.config.dataBinding ) ){
                let binding = _.first(this.props.config.dataBinding)
                return binding.table ? binding.table : ""
            }
        }
        return ""
    }

    getField(){
        if ( this.props.config && this.props.config.dataBinding) {
            if ( !_.isEmpty( this.props.config.dataBinding ) ){
                let binding = _.first(this.props.config.dataBinding)
                return binding.field ? binding.field : ""
            }
        }
        return ""
    }

    getValue(){
        if ( this.props.data && !_.isEmpty(this.props.data) ){
            const { data } = this.props
            const table = this.getTable()
            const field = this.getField()
            if ( data.hasOwnProperty(table) && data[table].hasOwnProperty(field)){
                return data[table][field]
            }
        }
        return null
    }


    render() {
        const { value, field, table } = this.state
        const { dataStore, loginStore } = this.props.rootStore

        let labelValue = dataStore.getLabelValue(table,field,value)
        let label = loginStore.getMetaLabel(table,field)
        if (this.props.config.display && this.props.config.display == "inline") {
        return (
            <View style={styles.container}>
                <View style={styles.underline}>
                    <View style={styles.labelContainer}>
                        <Text style={styles.label}>{label}</Text>
                    </View>
                    <View style={styles.valueContainer}>
                        <Text style={styles.value} numberOfLines={3}>{labelValue}</Text>
                    </View>
                </View>
            </View>
        )
    }



    return (
        <View >
            <Text style={{color:"white",fontSize:12}}>{labelValue}</Text>
        </View>
    )
    }
}

export default LabelValue;


//@ts-ignore
LabelValue.defaultProps = {
    inline : false,
    text:"",
    value:""
}