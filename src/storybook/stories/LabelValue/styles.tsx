import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container:{
        width:"100%",
        paddingLeft:12,
        paddingRight:12
    },
    labelContainer:{
        width:"100%",
    },
    valueContainer:{
        width:"100%"
    },
    label:{
        fontFamily: "Roboto",
        fontSize: 12,
        color: "#5384A6",
        letterSpacing: 0,
        lineHeight: 16
    },
    value:{
        fontFamily: "Roboto",
        fontSize: 14,
        color: "#0D3754",
        letterSpacing: 0,
        lineHeight: 24
    },
    underline:{
        paddingTop:10,
        paddingBottom:10,
        borderBottomColor:"lightgray",
        borderBottomWidth:0.5,
        flexDirection:"column",
        alignItems:"center",
        width:"100%"
    }
})

export default styles