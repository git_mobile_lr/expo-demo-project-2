import { StyleSheet } from 'react-native'
import { COLORS } from '../../../UILab/constants'

const styles = StyleSheet.create({
    cardBody:{
        marginLeft:24,
        marginRight:24,
        paddingBottom:10
    },
    fieldContainer:{
        borderBottomWidth:0.5,
        borderBottomColor:COLORS.GRAY,
        borderColor:'#EEEEEE',
        alignItems:"center",
        justifyContent:"center",
        paddingTop:10,
        paddingBottom:10,
        flexDirection:"row",
        
    },
    fieldNameContainer:{
        width:"30%"
    },
    fieldValueContainer:{
        width:"70%"
    },
    fieldName:{
        color:"#5384A6",
        fontSize:12,
        lineHeight:16,
        letterSpacing:0
    },
    fieldValue:{
        color:"black",
        fontSize:14,
        lineHeight:14,
        letterSpacing:0
    }
})

export default styles
