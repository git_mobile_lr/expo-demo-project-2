import React , { Component, useState } from 'react';
import { View, Text, Button } from 'native-base';
import styles from './styles';
import _ from 'lodash';
import FRenderer from '../../../screens/widgets/FRenderer';
import { TouchableOpacity } from 'react-native';
import { inject, observer } from "mobx-react/native";
interface Props {

}

interface State {

}
@inject('rootStore')
@observer
class index extends Component<Props,State> {
    constructor(props:Props) {
        super(props);
        this.state = {
            sections : this.props.config ? this.props.config.sections : []
        };
    }

    handleFooterPress = (workOrderName,workOrderId) => {
        const { loginStore,dataStore } = this.props.rootStore
        
        let params = [
            {
                "var": "NAME",
                "table": "Work_Order__c",
                "field": "Name",
                "value": workOrderName
            },
            {
                "var": "ID",
                "table": "Work_Order__c",
                "field": "Id",
                "value": workOrderId
            },
            {
                "activeItems":dataStore.activeItems,
                "previousActiveTab": loginStore.selectedTab,
                "nextActiveTab": 0,
                "from": dataStore.getCurrentWorkOrder() ? dataStore.getCurrentWorkOrder().Name : ""
            }
        ]
        if (!loginStore.isRelatedWorkOrder()){
            loginStore.navigation.push("WorkOrderDetails", params)
        }
        
    }

    render() {
        const { sections } = this.state
        const { data } = this.props
        const { loginStore } = this.props.rootStore
        const WO = loginStore.getPrefixedFieldName("Work_Order__c")
         
        let name = this.props.data[WO] ? this.props.data[WO].Name : ""
        let id = this.props.data[WO] ? this.props.data[WO].Id : ""
         
        return (
            <TouchableOpacity onPress={()=>{this.handleFooterPress(name,id)}}>
                <View style={styles.cardBody}>
                    {
                        sections.map((section:any,index:number)=>{
                            // let dataBinding = _.first(section.config.item.config.dataBinding)
                            //    
                            return <FRenderer key={index} data={data} config={section} />
                        })
                    }
                </View>
            </TouchableOpacity>
            
            
        );
    }
}

export default index;
