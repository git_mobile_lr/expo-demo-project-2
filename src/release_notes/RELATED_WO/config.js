export default {
  "intro": {
    "nl": {
      "title": "Swift is nóg slimmer geworden!",
      "description": "We vertellen je graag meer over de geavanceerde checklijst"
    },
    "en": {
      "title": "Swift just got a bit smarter!",
      "description": "We are glad to tell you more about the advanced checklist"
    }
  },
  "slides": [
    {
      "id": 1,
      "image": require("./assets/slide2-screen.jpg"),
      "nl": {
        "title": "Opmerkingen toevoegen en foto’s uploaden",
        "description": "Documenteer elke vraag in je checklijst door opmerkingen en foto's toe te voegen voor de Backoffice"
      },
      "en": {
        "title": "Comment and upload pictures",
        "description": "Document every question in your checklist by adding comments and uploading pictures for the Backoffice"
      }
    },
    {
      "id": 2,
      "image": require("./assets/slide3-screen.jpg"),
      "nl": {
        "title": "Checklijsten handmatig sluiten",
        "description": "Vanaf nu zullen geavanceerde checklijsten handmatig worden afgesloten zodat de Backoffice duidelijk weet wanneer deze zijn afgerond"
      },
      "en": {
        "title": "Closing checklists manually",
        "description": "From now on advanced checklists will be closed manually so that the Backoffice clearly knows when they are finalized"
      }
    },
    {
      "id": 3,
      "image": require("./assets/slide4-screen.jpg"),
      "nl": {
        "title": "Nieuwe veld typen beschikbaar",
        "description": "Het is nu nog makkelijker om door je checklijst heen te komen! Beantwoord de vragen door simpelweg op een label of pictogram te drukken"
      },
      "en": {
        "title": "New field types available",
        "description": "Now it's even easier to go trough your checklist! Simply answer the questions by tapping the label or icon"
      }
    },
    {
      "id": 4,
      "image": require("./assets/slide5.png"),
      "nl": {
        "title": "Neem contact met ons op",
        "description": "Neem contact met ons op om geavanceerde checklijsten in te zetten in jullie organisatie."
      },
      "en": {
        "title": "Contact us",
        "description": "Contact us to deploy advanced checklists in your organization"
      }
    },
  ],
  "buttons": {
    "nl": {
      "start": "Ja Graag!",
      "skip": "Overslaan",
      "dashboard": "Ga naar Dashboard"
    },
    "en": {
      "start": "Show me!",
      "skip": "Skip",
      "dashboard": "Go to Dashboard"
    }
  }
}