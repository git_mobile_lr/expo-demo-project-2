export default {
    "releaseNotesUrl" : "/3.412.1+-+Swift+-+Attachments+v03",
    "intro": {
      "nl": {
        "title": "Swift is nóg slimmer geworden!",
        "description": "We introduceren de nieuwe tool: Tijdregistratie Dashboard"
      },
      "en": {
        "title": "Swift just got a bit smarter!",
        "description": "Let us show you the improved Time Registration App"
      }
    },
    "slides": [
      {
        "id": 1,
        "nl": {
          "image": require("./assets/nl/Stopwatch-nl.png"),
          "title": "Wij hebben een Stopwatch toegevoegd",
          "description": "Registreer eenvoudig uw tijdregistraties met de nieuwe timer, standaard beschikbaar in de Tijdregistratie module."
        },
        "en": {
          "image": require("./assets/en/Stopwatch-en.png"),
          "title": "We have added a Stopwatch",
          "description": "Easily record your time entries with the new timer, available by default in the Time Registration module."
        }
      },
      {
        "id": 2,
        "nl": {
          "image": require("./assets/nl/WeekOverview-en.png"),
          "title": "Creëer een overzicht van jouw week",
          "description": "U kunt nu een duidelijk en volledig overzicht zien van uw geregistreerde uren voor elke week. Direct beschikbaar in de Tijdregistratie module."
        },
        "en": {
          "image": require("./assets/en/WeekOverview-en.png"),
          "title": "Have an overview of your week",
          "description": "You can now see a clear and complete overview of your registered hours for each week. Directly available in the Time Registration module."
        }
      },
      {
        "id": 3,
        "nl": {
          "image": require("./assets/nl/DayPlanner-nl.png"),
          "title": "Organiseer uw dagelijkse routine met de nieuwe dagplanner",
          "description": "Bekijk binnen de Tijdregistratie module uw geplande begin- en eindtijd voor elke dag en selecteer uw overwerkvergoeding. Voor deze functie is een configuratiewijziging vereist."
        },
        "en": {
          "image": require("./assets/en/DayPlanner-en.png"),
          "title": "Organise your daily routine with the new day planner component",
          "description": "Within the Time Registration module, view your scheduled start and end time for each day, and select your overtime compensation. This feature requires a configuration change."
        }
      },
      {
        "id": 4,
        "nl": {
          "image": require("./assets/contact-slide.png"),
          "title": "Wil jij deze nieuwe functies gebruiken?",
          "description": "De stopwatch en het weekoverzicht zijn direct beschikbaar voor u om te gebruiken."
        },
        "en": {
          "image": require("./assets/contact-slide.png"),
          "title": "Want to use these new features?",
          "description": "The stopwatch and week overview are directly available for you to use."
        }
      },
    ],
    "buttons": {
      "nl": {
        "start": "Ja Graag!",
        "skip": "Overslaan",
        "dashboard": "Ga naar Dashboard"
      },
      "en": {
        "start": "Show me!",
        "skip": "Skip",
        "dashboard": "Go to Dashboard"
      }
    }
  }