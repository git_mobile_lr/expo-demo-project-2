export default {
  "releaseNotesUrl": "",
  "intro": {
    "nl": {
      "title": "Swift is nóg slimmer geworden!",
      "description": "We introduceren de nieuwe tool: Tijdregistratie Dashboard"
    },
    "en": {
      "title": "Swift just got a bit smarter!",
      "description": "Let us show you the new Time Registration tool"
    }
  },
  "slides": [
    {
      "id": 1,
      "nl": {
        "image": require("./assets/nl/time-reg-app-menu.jpg"),
        "title": "Tijd Registratie",
        "description": "We hebben een nieuw dashboard toegevoegd in de app. Meteen beschikbaar!"
      },
      "en": {
        "image": require("./assets/en/time-reg-app-menu.jpg"),
        "title": "We have added a new dashboard in the app",
        "description": "Called Time Registration"
      }
    },
    {
      "id": 2,
      "nl": {
        "image": require("./assets/nl/time-reg-without-wo.jpg"),
        "title": "Registreer tijd zonder werkbon",
        "description": "Creëer overzicht in uw planning en facturatie"
      },
      "en": {
        "image": require("./assets/en/time-reg-without-wo.jpg"),
        "title": "Registrate time outside of a Work Order",
        "description": "Create overview in your planning and invoicing"
      }
    },
    {
      "id": 3,
      "nl": {
        "image": require("./assets/nl/time-reg-types.jpg"),
        "title": "Configureer uw eigen tijdregistratie opties",
        "description": "Zoals vakanties, vergaderingen, lunchtijd en meer!"
      },
      "en": {
        "image": require("./assets/en/time-reg-types.jpg"),
        "title": "Configure your own time registration options",
        "description": "Like meetings, lunch time and more"
      }
    },
    {
      "id": 4,
      "nl": {
        "image": require("./assets/contact-slide.png"),
        "title": "Vragen of wilt u meer weten?",
        "description": "Aarzel niet om contact met ons op te nemen!"
      },
      "en": {
        "image": require("./assets/contact-slide.png"),
        "title": "Questions or want to know more?",
        "description": "Don’t hesitate to contact us!"
      }
    },
  ],
  "buttons": {
    "nl": {
      "start": "Ja Graag!",
      "skip": "Overslaan",
      "dashboard": "Ga naar Dashboard"
    },
    "en": {
      "start": "Show me!",
      "skip": "Skip",
      "dashboard": "Go to Dashboard"
    }
  }
}