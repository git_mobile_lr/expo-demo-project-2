const enPermissions = require('./en.json')
const nlPermissions = require('./nl.json')
const dePermissions = require('./de.json')
const daPermissions = require('./da.json')
const frPermissions = require('./fr.json')


module.exports = {
    enPermissions,
    nlPermissions,
    dePermissions,
    daPermissions,
    frPermissions
}