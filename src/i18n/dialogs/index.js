const enDialogs = require('./en.json')
const nlDialogs = require('./nl.json')
const deDialogs = require('./de.json')
const daDialogs = require('./da.json')
const frDialogs = require('./fr.json')
module.exports = {
    enDialogs,
    nlDialogs,
    deDialogs,
    daDialogs,
    frDialogs
}