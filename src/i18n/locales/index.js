const EN = require('./en.json')
const NL = require('./nl.json')
const DE = require('./de.json')
const FR = require('./fr.json')
const DA = require('./da.json')

module.exports = {
    EN,
    NL,
    DE,
    DA,
    FR
}