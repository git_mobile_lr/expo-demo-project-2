import enOnboarding from './en.json'
import nlOnboarding from './nl.json'
import deOnboarding from './de.json'
import daOnboarding from './da.json'
import frOnboarding from './fr.json'


export default {
    enOnboarding,
    nlOnboarding,
    deOnboarding,
    daOnboarding,
    frOnboarding
}