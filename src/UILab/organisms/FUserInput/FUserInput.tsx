import React from 'react';
import _ from 'lodash'
import FPickList from '../../atoms/FPickList/FPickList'
import FLookUp from '../FLookUp/FLookUp'
import FRecordType from '../FRecordType/FRecordType'
import FInput from '../../atoms/FInput/FInput'

import FCard from '../../molecules/FCard/FCard'
import FReadOnly from '../../atoms/FReadOnly/FReadOnly'
import FNumber from '../../atoms/FNumber/FNumber'
import PropTypes from 'prop-types';
import { inject, observer } from "mobx-react/native"
import jsonQuery from 'json-query'
import FCheckBox from '../../molecules/FCheckBox/FCheckBox'
import FDate from '../../molecules/FDate/FDate'
import FDateTime from '../../molecules/FDateTime/FDateTime'
import { RecordTypes, RecordType, DateMode } from '../../../types'
import FFormula from '../../molecules/FFormula/FFormula';
import FMultiSelectPicklist from '../../molecules/FMultiSelectPickList/FMultiSelectPickList'
import FIconPicklist from '../../molecules/FIconPicklist/FIconPicklist'
import FLabelPicklist from '../../molecules/FLabelPicklist/FLabelPicklist'
import { Platform } from 'react-native';
import FLocationPicker from '../../molecules/FlocationPicker/FLocationPicker';
import FPicture from '../../molecules/FPicture/FPicture';


type Props = {
  type: RecordType
  label: string
  rootStore: any
  required: boolean
  field?: string
  value?: string
  apiName?: string
  formula?: string
  filterLogic?: string
  placeholder?: string
  name?: string
  locked?: boolean
  readOnlyFromConfig?: boolean
  readonly?: boolean,
  showValidationErrors?: boolean
  touchable: boolean
  updateFormStatus: (state: any) => void
  onSubmit: (value: any) => void
  picklistvalues: Array<string>
  permissionRequired: () => void
  comments?: string
  unit?: string
  scrollable?: boolean
  id?: string
  maxLength: number
  displayTextArea?: boolean,
  format: string,
  barcodeEnabled? : boolean,
  valueCoords: any
}

type State = {
  valid: boolean
  field: string
  value: string
  placeholder: string
  valueToDisplay: string
  name: string
  showValidationErrors: boolean
  isEditing: boolean
  readOnly: boolean
  touchable: boolean
  locked: boolean
}


@inject('rootStore')
@observer
class FUserInput extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      valid: props.required,
      field: props.field ? props.field : "",
      value: props.value ? props.value : "",
      placeholder: props.placeholder ? props.placeholder : "",
      valueToDisplay: '',
      name: props.name ? props.name : "",
      showValidationErrors: props.showValidationErrors ? props.showValidationErrors : false,
      isEditing: false,
      readOnly: false,
      touchable: props.touchable,
      locked: props.locked ? props.locked : false
    };
  }

  toggleIsEditing = () => {    
    this.setState({
      isEditing: !this.state.isEditing
    })
  }

  setReadOnly = () => {
    this.setState({ readOnly: true })
  }

  validate = (valid: boolean) => {
    this.setState({
      valid: (typeof valid === 'boolean') ? valid : false
    }, () => {
      this.props.updateFormStatus(this)
    })
  }

  onChange = (value: any) => {

    this.setState({ value: value }, () => {
      this.props.updateFormStatus(this)
    })
  }

  replaceConstants(condition: string) {
    const { currentWorkOrderId } = this.props.rootStore.loginStore
    return condition.replace(/\${CURRENT_WORK_ORDER}/g, currentWorkOrderId)
  }

  getValue(formula: string) {
    const { data } = this.props.rootStore.loginStore
    let result = jsonQuery(this.replaceConstants(formula), { data: data }).value
    return _.isEmpty(result)
      ? "FAIL"
      : result
  }

  getCorrectComponent(type: RecordType) {

    const { label, readOnlyFromConfig, readonly, rootStore, placeholder } = this.props
    const { value, isEditing } = this.state
    const { loginStore } = rootStore

    const MODAL_HEADER = loginStore.getLabelorDefault("${Label.Comments}", "Comments")
    const MODAL_PLACEHOLDER = loginStore.getLabelorDefault("${Label.AddComment}", "Please add a comment here...")
    const MODAL_SAVE = loginStore.getLabelorDefault("${Label.Save}", "Save")

    if (readOnlyFromConfig) {
      return (<FReadOnly
        value={(type == "REFERENCE")
          ? this.getValue(`${this.props.name}[Id=${this.getValue(`${loginStore.getPrefixedFieldName("Work_Order__c")}[Id=${this.replaceConstants("${CURRENT_WORK_ORDER}")}].${this.props.name}`)}].Id`)
          : this.getValue(`${loginStore.getPrefixedFieldName("Work_Order__c")}[Id=${this.replaceConstants("${CURRENT_WORK_ORDER}")}].${this.props.name}`)}
        label={label}
        valueToDisplay={(type == "REFERENCE")
          ? this.getValue(`${this.props.name}[Id=${this.getValue(`${loginStore.getPrefixedFieldName("Work_Order__c")}[Id=${this.replaceConstants("${CURRENT_WORK_ORDER}")}].${this.props.name}`)}].Name`)
          : this.getValue(`${loginStore.getPrefixedFieldName("Work_Order__c")}[Id=${this.replaceConstants("${CURRENT_WORK_ORDER}")}].${this.props.name}`)} />)
    }




    let _props = {
      toggleIsEditing: this.toggleIsEditing,
      value: value,
      readonly: readonly,
      isEditing: isEditing
    }

    switch (type) {
      case RecordTypes.FORMULA:

        return React.cloneElement(<FFormula />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.PICKLIST:
        return React.cloneElement(<FPickList />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.RECORDTYPE:
        return React.cloneElement(<FRecordType />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.REFERENCE:
        return React.cloneElement(<FLookUp />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.NUMBER:
      case RecordTypes.DOUBLE:
      case RecordTypes.CURRENCY:
        if (Platform.OS === 'ios') {
          return React.cloneElement(<FNumber keyboardType={"numbers-and-punctuation"} />, {
            ..._props,
            ...this.props
          })
        } else {
          return React.cloneElement(<FNumber  />, {
            ..._props,
            ...this.props
          })
        }
      case RecordTypes.DATE:
        return React.cloneElement(<FDate mode={DateMode.DATE} />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.DATETIME2:
      case RecordTypes.DATETIME:
        
        
        if (Platform.OS === 'android') {
          return React.cloneElement(<FDateTime mode={DateMode.DATETIME} format={this.props.format} />, {
            ..._props,
            ...this.props
          })

        } else {
          
          return React.cloneElement(<FDate mode={DateMode.DATETIME} format={this.props.format} toggleIsEditing={this.toggleIsEditing}/>, {
            ..._props,
            ...this.props
          })
        }
      case RecordTypes.PHONE:
        return React.cloneElement(<FInput keyboardType={"phone-pad"} />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.EMAIL:
        return React.cloneElement(<FInput keyboardType={"email-address"} />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.STRING:
        return React.cloneElement(<FInput />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.BOOLEAN:
      case RecordTypes.CHECKBOX:
        return React.cloneElement(<FCheckBox />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.CURRENCY:

        return React.cloneElement(<FNumber />, {
          ..._props,
          ...this.props
        })
      case RecordTypes.ICONPICKLIST:
        const { pickerIcons } = this.props.rootStore.settingsStore.rules.items.checklist
        //@ts-ignore props passed by arg
        return React.cloneElement(<FIconPicklist />, {
          ...{
            modalHeaderText: MODAL_HEADER,
            modalPlaceholderText: MODAL_PLACEHOLDER,
            modalSaveButtonText: MODAL_SAVE,
            icons: pickerIcons
          },
          ..._props,
          ...this.props
        })
      case RecordTypes.LABELPICKLIST:
        //@ts-ignore props passed by arg
        return React.cloneElement(<FLabelPicklist />, {
          ...{
            modalHeaderText: MODAL_HEADER,
            modalPlaceholderText: MODAL_PLACEHOLDER,
            modalSaveButtonText: MODAL_SAVE
          },
          ..._props,
          ...this.props
        })
      case RecordTypes.MULTISELECTPICKLIST:
        const { icons } = this.props.rootStore.settingsStore.rules.items.checklist
        //@ts-ignore props passed by arg
        return React.cloneElement(<FMultiSelectPicklist />, {
          ...{
            modalHeaderText: MODAL_HEADER,
            modalPlaceholderText: MODAL_PLACEHOLDER,
            modalSaveButtonText: MODAL_SAVE,
            icons: icons
          },
          ..._props,
          ...this.props
        })
        case RecordTypes.LOCATION:
          return React.cloneElement(<FLocationPicker />, {
            ..._props,
            ...this.props
          })
        case RecordTypes.PICTURE:
          return React.cloneElement(<FPicture />, {
            ..._props,
            ...this.props
          })
      default:
      
        return React.cloneElement(<FInput />, {
          ..._props,
          ...this.props
        })
    }
  }

  render() {
    const {
      type,
      id,
      required,
      label,
      value,

      readOnlyFromConfig,
      readonly,
      touchable,
      scrollable,
      filterLogic,

      apiName,
      formula,
      comments,
      unit,
      rootStore,
      onSubmit,
      displayTextArea,
      barcodeEnabled,
      valueCoords
    } = this.props
    
    const { loginStore } = rootStore

    const { showValidationErrors, locked, name, isEditing } = this.state

    let children = this.getCorrectComponent(type)

  
    let isReadOnly = (
      (locked) ||   
      (readOnlyFromConfig || readonly)
      
    )



    const _accessibilityLabel = label + " field"
    const _testID = _accessibilityLabel + " iOS"

    const MODAL_HEADER = loginStore.getLabelorDefault("${Label.Comments}", "Comments")
    const MODAL_PLACEHOLDER = loginStore.getLabelorDefault("${Label.AddComment}", "Please add a comment here...")
    const MODAL_SAVE = loginStore.getLabelorDefault("${Label.Save}", "Save")

    return (
      <FCard
        id={id}
        accessibilityLabel={_accessibilityLabel}
        testID={_testID}
        permissionRequired={this.props.permissionRequired}
        isEditing={isEditing}
        onPress={this.toggleIsEditing}
        touchable={touchable}
        scrollable={scrollable}
        comments={comments}
        unit={unit}
        onSubmit={onSubmit}
        modalHeaderText={MODAL_HEADER}
        modalPlaceholderText={MODAL_PLACEHOLDER}
        modalSaveButtonText={MODAL_SAVE}
        readOnly={isReadOnly}
        validate={this.validate}
        type={type}
        required={required}
        value={value}
        label={label}
        name={name}
        apiName={apiName}
        formula={formula}
        filterLogic={filterLogic}
        displayTextArea={displayTextArea}
        onChange={this.onChange}
        readOnlyFromConfig={isReadOnly}
        barcodeEnabled={barcodeEnabled}
        valueCoords={valueCoords}
        showValidationErrors={showValidationErrors}>
        {children}
      </FCard>
    )
  }
}

//@ts-ignore
FUserInput.defaultProps = {
  onChange: () => { },
  updateFormStatus: () => { },
  onSubmit: () => { },
  locked: false,
  label: "",
  referenceTo: "",
  type: "STRING",
  field: '',
  comments: '',
  required: false,
  apiName: "",
  formula: "",
  filterLogic: "",
  placeholder: "",
  name: "Default Name",
  picklistvalues: [],
  recordTypes: [],
  value: "",
  unit: "",
  maxLength: 80,
  valid: true,
  showValidationErrors: false,
  readOnlyFromConfig: false,
  readonly: false,
  touchable: true,
  scrollable: false,
  displayTextArea: false,
  format: "YYYY-MM-DD HH:mm",
  barcodeEnabled: false,
  valueCoords: null

}

export default FUserInput;
