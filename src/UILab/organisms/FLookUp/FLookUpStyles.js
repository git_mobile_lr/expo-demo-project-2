import {StyleSheet, Platform} from 'react-native'
import {IS_TABLET,ANDROID} from '../../../screens/widgets/styles'

export const styles = StyleSheet.create({

    label:{
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },

    FInput:{
        height:10,
        width:IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent'
    },  
    FModalIcon:{
        paddingRight:0,
        marginRight:0,
        marginLeft:0,
        color:'#00AAFF'
    },
    picklist:{
        height:50,
        width:IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent',
        paddingBottom:20,
    },
    FModal:{
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'space-between',
        height:10,
        width:'100%',
        borderColor: 'transparent'
    },
    picklistLabel:{
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
        marginBottom:15
    }
})
