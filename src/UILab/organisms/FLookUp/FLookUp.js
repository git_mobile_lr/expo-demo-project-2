import React from 'react';
import { styles } from './FLookUpStyles'
import { Item, Label } from 'native-base'
import { inject, observer } from "mobx-react/native"
import _ from 'lodash'
import PropTypes from 'prop-types'
import FModal from '../../molecules/FModal/FModal'
import FList from '../../molecules/FList/FList'
import FSpinner from '../../atoms/FSpinner/FSpinner'

//@inject('loginStore')
@inject('rootStore')
@observer
class FLookUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
            record: {},
            label:props.label,
            valueToDisplay:props.value,
            referenceTo: props.referenceTo,
            online: props.online,
            loaded:false,
            limit:props.limit,
            searchBar: props.searchBar
        };
    }

    componentDidUpdate(prevProps){
        if (!_.isEqual(prevProps,this.props)){
          // LISTEN TO PROPS CHANGES
          this.setState({isModalVisible:this.props.isEditing})
        }
      }


    async componentDidMount(){
        const {value,referenceTo,online} = this.state
        const {loginStore} = this.props.rootStore
        const {data} = loginStore

        // OFF LINE OPTION ?
        // if (!_.isEmpty(value) && !online && referenceTo!==""){
        //     record = jsonQuery(`${referenceTo}[Id=${value}]`,{data:data})
        // }

        if (!_.isEmpty(value) && online && referenceTo!=="" ){
            try{
               
                queryResult = await loginStore.searchTable(referenceTo, value, [],[fieldsToDisplay],1,{"conditions":[{"fieldName":`${"Id"}`,"operator":"=","searchValue":value}]})
                
                if (queryResult.searchError===null) {
                    record = queryResult.searchData[value]
                    this.setState({
                        valueToDisplay: record.Name,
                        loaded: true
                    })
                }
            }catch(err){
                 
            }
            
        }
    }

    handleChange = (value) => {
        try{
            this.setState({ 
                value: value.Id,
                valueToDisplay: value.Name,
                isModalVisible:false,
                loaded:true
            })
            this.props.toggleIsEditing()
            this.props.onChange(value.Id)
        }catch(err){
             
        }     
    }

    capitalize(label) {
        return label.charAt(0).toUpperCase() + label.slice(1);
      }

    labelToDisplay(){
        const {label,valueToDisplay} = this.state
        if ( valueToDisplay === '') {
            return this.capitalize(label)
        }
        return valueToDisplay
    }

    handleBack = () => {
        this.setState({isModalVisible:false})
        this.props.toggleIsEditing()
      }


    render() {
        const { loginStore } = this.props.rootStore
        const { fieldsToFilter, valueToSearch, fieldsToDisplay, fieldsToRequest, layout } = this.props
        const { 
            label, 
            isModalVisible, 
            valueToDisplay, 
            loaded, 
            value, 
            referenceTo, 
            limit, 
            searchBar, 
            online } = this.state

        formattedLabel = this.capitalize(label)

        return (
            <Item stackedLabel style={styles.FInput}>
                <Label style={styles.label}>{loginStore.getLabel(label)}</Label>
                    <Item style={styles.FModal}>
                        {
                            (_.isEmpty(value) || online===false || (!_.isEmpty(value) && (loaded))) && 
                            <FModal 
                                label={label}
                                onBack={this.handleBack}
                                isModalVisible={isModalVisible}
                                value={this.labelToDisplay()}
                                header={formattedLabel}>
                                <FList 
                                    onSelectOption={this.handleChange}
                                    referenceTo={referenceTo}
                                    fieldsToFilter={fieldsToFilter}
                                    valueToSearch={valueToSearch}
                                    fieldsToDisplay={fieldsToDisplay}
                                    fieldsToRequest={fieldsToRequest}
                                    layout={layout}
                                    limit={limit}
                                    online={online}
                                    searchBar={searchBar}
                                    listItem={referenceTo}/>
                            </FModal>   
                        }
                        {
                            !_.isEmpty(value) && (!loaded) && <FSpinner />
                        }           
                    </Item>
            </Item>
        )
    }
}

FLookUp.propTypes = {
    /**  Function to be called with every time the value changes */
    //onChange: PropTypes.function,

    /** Label already localized to display */
    label: PropTypes.string,

    /** If the lookup takes DATA as source then we need this VAR to reference to a target field  */
    referenceTo:PropTypes.arrayOf(PropTypes.string),

    /** Id of the record */
    value:PropTypes.string,

    /** If this is true it will fetch the values from SalesForce, If is false it will take local data as source */
    online: PropTypes.bool,

    /** Amount of records to display as max */
    limitRecords: PropTypes.number,

    /** If this option is true a searchbar will be displayed */
    searchBar:PropTypes.bool,

    /** If this option is true then the modal button text will take the props.value as data*/
    preselected:PropTypes.bool,

    layout: PropTypes.objectOf(PropTypes.string),
}


FLookUp.defaultProps = {
    onChange: () => { },
    toggleIsEditing: () => {},
    label: "$Label.LookUpLabelDefault",
    referenceTo: [],
    online: false,
    limitRecords: 30,
    searchBar: true,
    preselected: false,
    value: "",
    fieldsToFilter: ["Name"],
    layout: {},
};


export default FLookUp;