import React from 'react';
import {Text} from 'native-base'
import {toJS} from 'mobx'
import FUserInput from '../FUserInput/FUserInput'
import {inject, observer} from "mobx-react/native"
import _ from 'lodash'
import FSpinner from '../../atoms/FSpinner/FSpinner'
import PropTypes from 'prop-types';

//@inject('loginStore')
@inject('rootStore')
@observer
class FUserInputList extends React.Component {
  
  getPropsFromConfig(table,field){
    const {loginStore} = this.props.rootStore
    q = this.props.rootStore.metaStore.getFieldMeta(table, field)
        return q.type === "success"
          ? q.data
          : null
  }
  
  render() {
      
    const { list,data } = this.props
    if (!_.isEmpty(list)) {
      return list.map((item, index) => {
        let propsFromConfig = item
        let propsFromParentComponent = _.unset(this.props,this.props.list)
        let propsFromMetaData = this.getPropsFromConfig(item.table,item.field)
        if (!_.isEmpty(propsFromMetaData) && (!_.isEmpty(propsFromConfig))) {
          return React.cloneElement(<FUserInput value={ _.isEmpty(data[item.field]) ? item.label : data[item.field]}/>,{
            ...propsFromConfig,
            ...propsFromParentComponent,
            ...propsFromMetaData,
            ...{referenceTo:_.first(propsFromMetaData.referenceTo)},
            ...{updateFormStatus:this.props.updateFormStatus}
          })
        }
      })
    }
    return <FSpinner />
  }
}

FUserInputList.propTypes = { 
  // collection of objects to render
  list : PropTypes.array,
}

FUserInputList.defaultProps = {
  updateFormStatus: () => {},
  list: [],
  showValidationErrors: false,
  data:{}
}

export default FUserInputList;

