import React from 'react';

import {styles} from './FRecordTypeStyles'
import {

  Item,
  
  Label,
 
} from 'native-base'
import {inject, observer} from "mobx-react/native"

import _ from 'lodash'
import PropTypes from 'prop-types'
import FModal from '../../molecules/FModal/FModal'
import FList from '../../molecules/FList/FList'


@inject('rootStore')
@observer
class FRecordType extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      record: {},
      label: props.label,
      valueToDisplay: "",
      referenceTo: props.referenceTo,
      online: props.online,
      loaded: true,
      limit: props.limit,
      searchBar: props.searchBar,
      recordTypes: props.recordTypes,
      customList: [],
      readonly: props.readonly
    };
  }

  componentDidUpdate(prevProps) {
    if (!_.isEqual(prevProps, this.props)) {
      // LISTEN TO PROPS CHANGES
      this.setState({isModalVisible: this.props.isEditing})
    }
  }

  async componentDidMount() {

    if (!_.isEmpty(this.props.recordTypes)) {

      const _customList = _
        .keys(this.props.recordTypes)
        .map(x => {

          return {Id: x, label: this.props.recordTypes[x].developerName, Name: this.props.recordTypes[x].developerName, master:this.props.recordTypes[x].master};
        })
        .filter(x => !x.master)

      this.setState({customList: _customList})

    }

  }

  handleChange = (value) => {
    try {

      this.setState({value: value.Id, isModalVisible: false, loaded: true})

      this
        .props
        .onChange(value.Id)
      this
        .props
        .onSubmit(value.Id)
      this
        .props
        .toggleIsEditing()
    } catch (err) {
       
    }
  }

  capitalize(label) {
    return label
      .charAt(0)
      .toUpperCase() + label.slice(1);
  }

  handleBack = () => {
    this.setState({isModalVisible: false})
    this
      .props
      .toggleIsEditing()
  }

  getValueToDisplay() {
    if (this.props.hasOwnProperty("recordTypes")) {
      const _customList = _
        .keys(this.props.recordTypes)
        .map(x => {
          return {value: x, label: this.props.recordTypes[x].developerName, master:this.props.recordTypes[x].master};
        })
        .filter(x => !x.master)
      
      return _customList.reduce((acc, option) => {;
        return (option.value === this.state.value)
          ? option.label
          : acc
      }, "")
    }
    return this.state.value

  }

  render() {
    const {loginStore} = this.props.rootStore
    const {
      label,
      isModalVisible,
      valueToDisplay,
      loaded,
      value,
      referenceTo,
      limit,
      searchBar,
      online
    } = this.state
    formattedLabel = this.capitalize(label)
    return (
      <Item stackedLabel style={styles.FInput}>
        <Label style={styles.label}>{loginStore.getLabel(label)}</Label>
        <Item style={styles.FModal}>
          {(_.isEmpty(value) || online === false || (!_.isEmpty(value) && (loaded))) && <FModal
            label={label}
            onBack={this.handleBack}
            isModalVisible={isModalVisible}
            value={this.getValueToDisplay()}
            header={formattedLabel}
            disabled={this.props.readonly}>
            <FList
              onSelectOption={this.handleChange}
              referenceTo={referenceTo}
              searchBar={false}
              customList={this.state.customList}
              listItem={referenceTo}/>
          </FModal>
}

        </Item>
      </Item>
    )
  }
}

FRecordType.propTypes = {
  /**  Function to be called with every time the value changes */
  //onChange: PropTypes.function,

  /** Label already localized to display */
  label: PropTypes.string,

  /** If the lookup takes DATA as source then we need this VAR to reference to a target field  */
  // referenceTo: PropTypes.arrayOf(PropTypes.string),

  /** Id of the record */
  value: PropTypes.string,

  /** If this is true it will fetch the values from SalesForce, If is false it will take local data as source */
  online: PropTypes.bool,

  /** Amount of records to display as max */
  limitRecords: PropTypes.number,

  /** If this option is true a searchbar will be displayed */
  searchBar: PropTypes.bool,

  /** If this option is true then the modal button text will take the props.value as data*/
  preselected: PropTypes.bool
}

FRecordType.defaultProps = {
  onChange: () => {},
  toggleIsEditing: () => {},
  label: "$Label.RecordTypeLabelDefault",
  referenceTo: "",
  online: false,
  limitRecords: 30,
  searchBar: true,
  preselected: false,
  value: "",
  recordTypes: [],
  customList: [],
  readonly: false
};

export default FRecordType;