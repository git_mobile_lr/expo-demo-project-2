import React from 'react';

import { View, Text } from 'native-base'
import { inject, observer } from "mobx-react/native"
import _ from "lodash"
import { Container, Content } from 'native-base'
import FFilterBar from '../../molecules/FFilterBar/FFilterBar'
import FSpinner from '../../atoms/FSpinner/FSpinner'
import FSearchBar from '../../molecules/FSearchBar/FSearchBar'
import FPages from '../../molecules/FPages/FPages'
import FRenderer from '../../../screens/widgets/FRenderer'
interface Props {
    rootStore: any,
    config: any
}

interface State {
    assetsAreLoaded: boolean,
    valueToFilter: string,
    userConfig: any
    data: Array<object>
    itemsPerPage: number
}

@inject('rootStore')
@observer
class FList extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            assetsAreLoaded: false,
            valueToFilter: "",
            data: [],
            userConfig: props.config,
            itemsPerPage: props.config.itemsPerPage ? props.config.itemsPerPage : 5
        };
    }

    componentDidMount() {
        this.loadAsync()
    }

    componentWillReact() {
        const { loginStore } = this.props.rootStore

        if (loginStore.isTriggerReload) {
            loginStore.setIsTriggerReload = false
            this.setState({
                data: []
            }, () => {
                this.loadAsync()

            })

        }
    }

    async loadAsync() {
        const { loginStore } = this.props.rootStore
        const dash = await loginStore.getDashboardData(this.state.userConfig)

        if (dash.type == "success") {
            if (this.state.valueToFilter) {
                const filterData = dash.data.filter((item: any) => {
                    return this._filter(item);
                })

                this.setState({ data: filterData, assetsAreLoaded: true })
            } else {
                this.setState({ data: dash.data, assetsAreLoaded: true })
            }

        }
    }

    onSearch = (value) => {
        this.setState({ valueToFilter: value }, () => {
            this.loadAsync()
        })
    }

    //@ts-ignore
    private _filter(item: any) {
        try {
            let r = false

            const { userConfig } = this.state

            if (userConfig.search) {
                for (let _pair of userConfig.search) {
                    const { field, table } = _pair
                    r |= (item[table] && item[table][field] && _.includes(item[table][field].toLowerCase(), (this.state.valueToFilter).toLowerCase()))
                }
            }

            return r
        } catch (error) {
            return false
        }
    }

    render() {
        const { loginStore } = this.props.rootStore
        const {
            isDrawerBusy,
            isTriggerReload,
            isRestoring

        } = loginStore

        const { data, userConfig, itemsPerPage, assetsAreLoaded } = this.state

        if (assetsAreLoaded) {
            const _view = (<View style={{
                flex: 1,
                marginBottom: 80
            }}>
                {userConfig.search && <FSearchBar barCodeButton onSubmitSearch={this.onSearch} />}
                    {
                        loginStore.isSyncing 
                        ?  <FSpinner />
                        : <View />

                    }
                    <FFilterBar
                        initialData={data}
                        conditions={userConfig.filters}
                        renderItem={(dataItem: any) => {
                            if (dataItem.loading) {
                                return <FSpinner />
                            }
                            return (
                                    <FPages
                                        onRefresh={()=>{loginStore.sync()}}
                                        data={dataItem.payload}
                                        itemsPerPage={itemsPerPage}
                                        renderItem={(_item) => {
                                            return <FRenderer data={_item} config={this.props.config.item} />
                                        }}
                                    />
                            )

                        }}
                    />
          
                
            </View>)

            return (
                <Container style={{
                    flex: 1,
                    backgroundColor: '#F8F8F8'
                }}>
                    {_view}
                </Container>
            )
        }

        return <View />
    }
}

export default FList;