import React , { Component } from 'react';
import { View , Text } from 'native-base';
import { styles } from './Styles'

type Props = {
    text ? : string
}

type State = {
    text ? : string
}

class FHeader extends Component<Props,State> {
    constructor(props:Props) {
        super(props);
        this.state = {  
            text : props.text
        };
    }
    render() {
        const { text } = this.state
        return (
            <View style={styles.headerContainer}>
                <Text style={styles.text}>{text}</Text>
            </View>
        );
    }
}

//@ts-ignore
FHeader.defaultProps = {
    text : "Details"
}
export default FHeader;
