import { StyleSheet } from 'react-native'
import { COLORS, FONTS } from '../../constants'

export const styles = StyleSheet.create({
    headerContainer: {
        alignItems: "center",
        padding: 14,
        backgroundColor: COLORS.LIGHTPRIMARY
    },
    text: {
        fontFamily: FONTS.PRIMARY,
        color: COLORS.SECONDARY,
        fontSize: 26
    }
})