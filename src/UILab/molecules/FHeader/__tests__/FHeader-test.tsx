/**
 * @jest-environment jsdom
 */
import React from 'react';
import { mount } from 'enzyme';
import FHeader from '../FHeader';

// 
describe('FHeader',()=>{
  test('Set default values is no props',()=>{
    const wrapper = mount(
      <FHeader  />
    );
    //@ts-ignore
    expect(wrapper.state().text).toStrictEqual("Details")
  })
  test('Set correct values if there are props',()=>{
    const wrapper = mount(
      <FHeader  
        text={"TEST-TEXT"}
      />
    );
    //@ts-ignore
    expect(wrapper.state().text).toStrictEqual("TEST-TEXT")
  })
})



