import React from 'react';
import { Image, View, Text, Alert } from 'react-native'
import { Button } from 'native-base';
import { inject, observer } from "mobx-react/native"
import { styles } from './FNoPermissionStyles'
import _ from 'lodash'


@inject('rootStore')
@observer
class FNoPermission extends React.Component {
 
  
  
  _tablet() {    
    return (
      <View style={styles.tabletLabelCard}>
        <Text style={styles.tabletLabelText}>{this.props.label}</Text>
      </View>
    )
  }

  _smartphone() {    
    return (
      <View style={styles.labelCard}>
        <Text style={styles.labelText}>{this.props.label}</Text>
      </View>
    )
  }
  render() {
    if(!this.props.isVisible){
      return null
    }
    
    const { openSettings } = this.props.rootStore.permissionStore
    const { loginStore } = this.props.rootStore
    const { isSmallDevice, isTablet } = loginStore
    const _label = isTablet
        ? this._tablet()
        : this._smartphone()

   
   
    return (

      <View style={styles.container}>
        {_label}
        <View style={{ marginLeft: 24, marginRight: 24, marginTop: 12, marginBottom: 12, alignContent: 'center' }}>
          <Button rounded info onPress={openSettings} style={{ alignSelf: "center" }}>
            <Text style={isSmallDevice ? styles.smallDeviceEmptyFont : styles.emptyFont}>
              {
                loginStore.getLabelorDefault("${Label.OpenSettings}", "Open Settings")
              }
            </Text>
          </Button>
        </View>
      </View>

    )
  }


  
}

FNoPermission.defaultProps = {
  label: '',
  isVisible: false,
  defaultTabletIcon: () => {
    return (
      <Image
        style={styles.icon}
        source={ require('../../../assets/images/screens/workorderdetails/swift-no-result.png')}
      />
    )
  },
  defaultIcon: () => {
    return (
      <Image
        style={styles.icon}
        source={ require('../../../assets/images/screens/workorderdetails/swift-no-result.png')}
      />
    )
  }
}

export default FNoPermission;
