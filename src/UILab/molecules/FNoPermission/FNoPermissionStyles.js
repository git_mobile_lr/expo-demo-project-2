import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 15,
    alignSelf:'center'
    
  },
  smallDeviceEmptyFont: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 20,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingHorizontal: 20,
    paddingVertical: 5
  },
  emptyFont: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 30,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingHorizontal: 20
  },
  icon: {
    marginBottom: 20,
    marginLeft: 20, 
  },
  labelCard: {
    width: '100%',
    padding: 5,
    alignSelf:'center',  
    backgroundColor: '#E5F6FF',
    borderRadius: 10
  },
  labelText: {
    
    margin: 4,
   
    fontSize: 13,    
    color: '#0D3754',
    letterSpacing: 0
  },
  tabletLabelCard: {
    width: '100%',
    padding: 15,
    alignSelf:'center',        
    backgroundColor: '#E5F6FF',
    borderRadius: 10,
    borderColor:'#fff',
    borderWidth:1,
    shadowColor:'#fff'
  },
  tabletLabelText: {
    
    margin: 8,    
    fontSize: 24,
    
    letterSpacing: 1,
    
    color: '#0D3754',
    
  }
})