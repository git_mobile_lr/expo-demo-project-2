
import React , { Component } from 'react'
import {inject, observer} from "mobx-react/native"
import { styles } from './FFieldStyles'
import { Text, View } from 'native-base'
import { MetaData } from '../../../types'
import _ from 'lodash'

type Props = {
    data : {[key:string]:Array<object>}
    table : string
    field : string
    meta : MetaData
    rootStore ? : any
}
type State = {
    data : {[key:string]:Array<object>}
    meta : MetaData | null
    table : string
    field : string
}
type Config = {}

@inject('rootStore')
@observer
class FField extends Component<Props,State> {
    static defaultProps: { data: {}; table: string; field: string; meta: {} }
    constructor(props:Props) {
        super(props)
        this.state = {
            data: props.data,
            table : props.table,
            field : props.field,
            meta : this.getMetaData()
        }
    }

    componentDidUpdate(prevProps:Props){
        if (!_.isEqual(this.state.data,this.props.data)){
          this.setState({data:this.props.data})
        }
      }


    componentDidMount(){
        this.getMetaData()
    }

    getMetaData():MetaData | null{
        const { table, field } = this.props
        const { loginStore } = this.props.rootStore
        let TABLE_META_DATA = loginStore.getMetaFields(table)
        if (TABLE_META_DATA.type === 'success') {
            let PRE_FIELD_META_DATA = TABLE_META_DATA.data.filter((item:MetaData)=>item.name===field)
            if (!_.isEmpty(PRE_FIELD_META_DATA)){
                const FIELD_META_DATA = _.first(PRE_FIELD_META_DATA)
                //@ts-ignore can receive the corresponding type from loginStore
                return FIELD_META_DATA
            }
        }
        return null
    }

    getValue():string{
        const {data,meta} = this.state
        const {table,field} = this.props

        if (_.isEmpty(data)) return "-"

        if (meta){
            if (data.hasOwnProperty(table) && !_.isEmpty(data[table])){
                if (!_.isEmpty(data[table])){
                    if (data[table].length > 1) return "..."
                    const item = _.first(data[table])
                    if (item){
                        if (meta.type === "PICKLIST") return meta.picklistvalues.reduce((acc,option:any)=>{return (option.value===item[field]) ? option.label : acc },"")
                        return item[field]
                    }
                }
            }
        }
        return "-"
    }

    render() {
        const {meta} = this.state
        return meta ? (
            <View style={styles.container}>
                <View style={styles.row}>
                    <View style={styles.fieldNameColumn}>
                        <Text style={styles.label}>{meta.label}</Text>
                    </View>
                    <View style={styles.fieldValueColumn}>
                        <Text style={styles.value}>{this.getValue()}</Text>
                    </View>
                </View>
                <View style={styles.separator} />
            </View>
        )
        : <View />
    }
}

FField.defaultProps = {
    data:[{}],
    table : "Table__c",
    field : "Field__c",
    meta : {}
}

export default FField;