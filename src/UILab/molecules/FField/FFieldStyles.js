import React from 'react';
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container : {

    },
    row : {
        width:'100%',
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingBottom:10,
        paddingTop:10,
        paddingLeft: 10
    },
    iconColumn : {
        flex:1,
        alignItems: 'center',
    },
    fieldNameColumn : {
        flex:2,
        alignSelf: 'flex-start',
        paddingLeft:24,
        paddingRight: 24

    },
    icon:{
        alignItems: 'center',
    },  
    fieldValueColumn : {
        flex:2,
        alignSelf: 'flex-start',

    },
    label:{
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 24,

    },
    value:{
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,

    },
    separator:{
        marginLeft:50,
        marginRight:20,
        borderTopColor: '#EEEEEE',
        borderTopWidth: StyleSheet.hairlineWidth
    }


})