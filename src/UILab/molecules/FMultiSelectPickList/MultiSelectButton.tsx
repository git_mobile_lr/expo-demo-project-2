import React, { Component } from 'react';
import { View, Text } from 'native-base'
import FIcon from '../../atoms/FIcon/FIcon';
import { styles } from './Styles'
import { TouchableOpacity } from 'react-native'
import _ from 'lodash'
import { icon } from '../../../types'

type Props = {
    value?: string
    active?: boolean
    readonly?: boolean
    onToggle?: Function
    icon?: icon | undefined
}

type State = {
    value?: string
    active?: boolean
    icon?: icon | undefined
}
class MultiSelectButton extends Component<Props, State> {

    static defaultProps: { onToggle: () => void; icon: string };

    constructor(props: Props) {
        super(props);
        this.state = {
            value: props.value,
            active: props.active,
            icon: props.icon
        };
    }

    componentDidUpdate() {
        if (this.state.icon !== this.props.icon) {
            this.setState({ icon: this.props.icon })
        }
        if (this.state.active !== this.props.active) {
            this.setState({ active: this.props.active })
        }
    }

    toggleButton = () => {
        //@ts-ignore onToggle is defined in defaults
        this.props.onToggle(this.state.value)
        this.setState({ active: !this.state.active })
    }

    renderReadonly() {
        const { active, value } = this.state
        const { icon } = this.state
        return (


            <View style={active ? styles.activeReadonlyButton : styles.inactiveReadonlyButton}>
                <View style={styles.contentContainer}>
                    {
                        !_.isEmpty(icon)
                            ? <FIcon icon={icon} color={active ? "white" : "#c8c8c8"} size={12} />
                            : <Text style={active ? styles.activeReadonlyText : styles.inactiveReadonlyText}>{value}</Text>
                    }
                </View>
            </View>


        );
    }

    render() {
        if (this.props.readonly) {
            return this.renderReadonly()
        }
        const { active, value } = this.state
        const { icon } = this.state
        return (

            <TouchableOpacity onPress={this.toggleButton}>
                <View style={active ? styles.activeButton : styles.inactiveButton}>
                    <View style={styles.contentContainer}>
                        {
                            !_.isEmpty(icon)
                                ? <FIcon icon={icon} color={active ? "white" : "#48aaf8"} size={12} />
                                : <Text style={active ? styles.activeText : styles.inactiveText}>{value}</Text>
                        }
                    </View>
                </View>
            </TouchableOpacity>

        );
    }
}

MultiSelectButton.defaultProps = {
    onToggle: () => { },
    icon: "",
    readonly: false

}

export default MultiSelectButton;