import React, { Component } from 'react';
import { View, Text } from 'native-base'
import { styles } from './Styles'
import MultiSelectButton from './MultiSelectButton'
import { ScrollView, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import FIcon from '../../atoms/FIcon/FIcon'
import FModalInput from '../FModalInput/FModalInput'


import { upsertScreenModes } from '../../../types'
import _ from 'lodash'
import { COLORS, DEVICE_WIDTH } from '../../constants'
import { Icons, icon } from '../../../types'
import { FontAwesome } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as Sentry from 'sentry-expo';
import moment from 'moment';
import ImageBrowserScreen from '../../../screens/ImageBrowserScreen'
import CameraScreen from '../../../screens/CameraScreen'

type Props = {
    id?: string
    name?: string
    comments?: string
    picklistvalues?: Array<string> | null
    value?: Array<string> | string
    rootStore?: any
    icons?: Array<Icon>
    modalHeaderText?: string
    modalPlaceholderText?: string
    modalSaveButtonText: string
    helpText?: string
    readonly?: boolean

    onChange: (state: any) => void
    onSubmit: (state: any) => void
}

type State = {
    id?: string
    name?: string
    comments?: string
    picklistvalues?: Array<string> | null
    value?: Array<string> | null
    attachments?: number
    imageBrowserOpen: boolean
    isCameraOpen: boolean,
    screenWidth : number
}

type Icon = {
    [value: string]: string
}
/**
 * Multi-select PickList
 */
class FMultiSelectPickList extends Component<Props, State> {
    _isMounted = false
    static defaultProps: {
        onChange: (state: State) => void,
        modalSaveButtonText: String;
        name: string;
        comments: String;
        picklistvalues?: Array<string> | null;
        value?: Array<string> | null;
        icons: Array<object>
    };

    constructor(props: Props) {
        super(props);
        this.state = {
            id: props.id,
            name: props.name,
            comments: props.comments,
            attachments: 0,
            picklistvalues: props.picklistvalues,
            value: (typeof props.value == "string") ? props.value.split(";") : props.value,
            imageBrowserOpen: false,
            isCameraOpen: false,
            screenWidth : Dimensions.get('window').width
        };
    }

    _dimensionsListener : any

    async componentDidMount() {
        const { loginStore } = this.props.rootStore
        this._isMounted = true
        const amount = await loginStore.getMediaLengthById(this.props.id)
        this.setState({ attachments: amount })

        this._dimensionsListener = Dimensions.addEventListener('change', () => {
            let o = this.isPortrait() ? 'portrait' : 'landscape'
       //     this.props.rootStore.loginStore.setOrientation(o);
            const width = Dimensions.get('window').width;
            this.setState({
              screenWidth: width
            });
        });
    }

    componentWillUnmount() {
        this._isMounted = false;

        try {
      
            this._dimensionsListener()
            //Dimensions.removeEventListener('change')
          } catch (error) {
      
          }
    }

    isPortrait = () => {
        const dim = Dimensions.get('screen');
        return dim.height >= dim.width;
    };

    /**
     * Renders the blue comment button at the right of the screen
     */
    commentButton() {
        return (
            <View style={{ padding: 24 }}>
                <FIcon icon={Icons.COMMENTS} color={"white"} />
            </View>
        )
    }

    /**
     * Renders the blue camera button at the right of the screen
     */

    cameraButton() {
        if (!this.props.rootStore.settingsStore.rules.attachments.allowPhotosUpload || !this.props.rootStore.settingsStore.rules.items.checklistShowActions.attachments) return;

        if (this.props.rootStore.dataStore.currentChecklistMode === upsertScreenModes.UPDATE) {
        return (
            <TouchableOpacity onPress={() => this._useCameraHandler()}>
            <View style={{ padding: 24 }}>
                <FontAwesome name="camera" size={18} color="white" />
            </View>
            </TouchableOpacity>
        )
        }
    }

    /**
     * Renders the blue library button at the right of the screen
     */

    libraryButton() {
        if (!this.props.rootStore.settingsStore.rules.attachments.allowPhotosUpload || !this.props.rootStore.settingsStore.rules.items.checklistShowActions.attachments) return;

        if (this.props.rootStore.dataStore.currentChecklistMode === upsertScreenModes.UPDATE) {
        return (
            <TouchableOpacity onPress={() => this._useLibraryHandler()}>
            <View style={{ padding: 24 }}>
                <FontAwesome name="picture-o" size={18} color="white" />
            </View>
            </TouchableOpacity>
        )
        }
    }

    _getFileName(uri: any) {
        try {
            const _u = moment().format("YYYY-MM-DD HH:mm") + '.' + uri
                .split('/')
                .pop()
                .split('.')
                .pop();


            return _u
        } catch (e) {

            return null
        }
    }

    async uploadAsync(result: any) {
        const { loginStore } = this.props.rootStore
        loginStore.setIsAddingAttachment = true
        const { comments } = this.state

        try {

            const _name = this._getFileName(result.uri)

            if (_name) {

                const _file = {
                    PathOnClient: result.uri,
                    Title: _name,
                    VersionData: result.base64,
                    FirstPublishLocationId: this.props.id
                }

                //loginStore.setIsSyncing = true

                const _r = await loginStore.uploadAsync(_file)
                this.onBack()
                if (this._isMounted) {
                    this.setState(prevState => ({
                        //@ts-ignore
                        attachments: prevState.attachments + 1
                    }))
                }
                loginStore.displayAttachmentUploadSuccess()                

            }
        } catch (e) {
            let _error = {
                type: 'error',
                context: 'uploadAsync',
                message: e
            }

            // Sentry.captureException(new Error(_error.message), { logger: 'workOrderDetailsScreen' });

            Sentry.Native.withScope(function (scope) {
                scope.setLevel(Sentry.Native.Severity.Error);
                Sentry.Native.captureException(e);
            });

            // Sentry.Native.captureException(new Error(_error.message), { logger: 'workOrderDetailsScreen' });

        } finally {
            setTimeout(async () => {
                const { loginStore } = this.props.rootStore
                loginStore.setIsAddingAttachment = false
                loginStore.setHasMediaChanged = true
                loginStore.setIsSyncing = false
            }, 100)
        }
    }


    handleDataChange() {
        const { state } = this
        const { onChange } = this.props
        let newState = {}
        newState['value'] = state.value ? state.value.join(";") : ""
        newState = { ...state, ...newState }

        this.props.onSubmit(newState)
        //@ts-ignore
        onChange(newState.value)
        // We should also save the comments...... not only the value.
    }


    /**
     * Saves the comment that comes from the modal to the state of the Multi-select Picklist
     */
    onSaveComment = (value: string) => {
        this.setState({ comments: value }, this.handleDataChange)
        this.onBack()
    }

    /**
     * Hides the blue button comment from the right
     */
    onBack = () => {
        if (this.refs._scrollView) {
            // @ts-ignore
            this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
        }
        return
    }

    onValueSelected = (text: string) => {
        let newValue = this.getNewValue(text)
        this.setState({ value: newValue }, this.handleDataChange)
    }

    getNewValue(newValue: string): Array<string> {
        const { value } = this.state
        if (_.isEmpty(value)) return [newValue]

        if (value && !_.isEmpty(value)) {
            if (_.isEmpty(value.filter(item => item.trim() == newValue.trim()))) {

                return value.concat([newValue])
            } else {
                return value.filter(item => item.trim() != newValue.trim())
            }
        }
        return [newValue]
    }


    getIcon(value: string): icon | undefined {

        const { icons } = this.props

        if (icons) {
            const ICON = icons.filter((icon: any) => {
                let A = Object.keys(icon)[0].toLowerCase()
                let B = value.toLowerCase()
                return B.includes(A)
            })

            let values = Object.values(Icons)

            //@ts-ignore This function should return an Icon type
            return !_.isEmpty(ICON) ? Object.values(ICON[0])[0] : undefined

        }
        return undefined
    }


    renderReadonly() {
        const { name, comments, picklistvalues, attachments, value } = this.state
        const { modalHeaderText, modalPlaceholderText, modalSaveButtonText, helpText, readonly } = this.props
        const MODAL_HEADER = modalHeaderText
        const PLACEHOLDER = modalPlaceholderText

        let customStyles = StyleSheet.create({
            picklistContainerReadonly: {
                width: this.state.screenWidth,
                paddingLeft: 0,
                paddingRight: 12,
                borderLeftColor: "#f0f0f0",
                borderLeftWidth: 4,
            },
        })

        return (
            <View style={styles.containerReadonly}>
                <ScrollView
                    ref='_scrollView'
                    horizontal
                    showsHorizontalScrollIndicator={false}>
                    <View style={customStyles.picklistContainerReadonly}>
                        <View style={styles.card}>
                            <View style={styles.labelContainer}>
                                {!_.isEmpty(comments) ?
                                    <View style={styles.commentIndicatorContainer}>
                                        <FIcon icon={Icons.COMMENTSDOTS} color='#c8c8c8' />
                                    </View>
                                    : <View />}
                                {attachments && attachments > 0 ?
                                    <View style={styles.commentIndicatorContainer}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                            <FontAwesome name="file-picture-o" size={18} color='#c8c8c8' />
                                            <View style={{ padding: 1, backgroundColor: '#c8c8c8', position: 'absolute', borderRadius: 10, marginLeft: 8 }}>
                                                <Text style={{ color: '#fff', fontWeight: '600', fontSize: 10 }}>{attachments}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    : <View />}
                                <View style={styles.nameSection}>
                                    <Text style={styles.label}>{name}</Text>
                                    {!_.isEmpty(helpText) && <Text style={styles.helpText}>{helpText}</Text>}
                                </View>
                            </View>
                            <View style={styles.buttonContainer}>
                                {picklistvalues ? picklistvalues.map(_value => {
                                    return _.isEmpty(value)
                                        ? <MultiSelectButton
                                            readonly={readonly}
                                            icon={this.getIcon(_value)}
                                            onToggle={this.onValueSelected}
                                            value={_value}
                                            active={false}
                                        />
                                        : <MultiSelectButton
                                            readonly={readonly}
                                            icon={this.getIcon(_value)}
                                            onToggle={this.onValueSelected}
                                            value={_value}
                                            active={!_.isEmpty(value ? value.filter(item => item.trim() == _value.trim()) : undefined)}
                                        />
                                }) : null}
                            </View>
                        </View>
                    </View>
                    <View style={styles.readOnlyCommentsContainer}>
                        <FModalInput
                            value={comments}
                            header={MODAL_HEADER}
                            placeholder={PLACEHOLDER}
                            saveButtonText={modalSaveButtonText}
                            onBack={this.onBack}
                            triggerItem={this.commentButton()}
                            isReadOnly={readonly}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }

    _useLibraryHandler = async () => {
        this.setState({
            imageBrowserOpen: true
        })
    }

    _useCameraHandler = async () => {
        this.setState({
            isCameraOpen: true
        }
        )
    }

    cameraCallback = async (callback: any) => {
        this.setState({
            isCameraOpen: false
        }, () => {

            this._p(callback)
        });
    }

    imageBrowserCallback = async (callback: any) => {

        this.setState({
            imageBrowserOpen: false
        }, () => {
            this._p(callback)
        });

    }

    _p(callback: any) {
        const { loginStore } = this.props.rootStore


        callback.then((photos: any) => {
            if (!_.isEmpty(photos)) {
                loginStore.uploadMediaLibraryAsync(photos, "Work_Order_Line_Item_Detail__c", this.props.id)
                this.setState(prevState => ({
                    //@ts-ignore
                    attachments: prevState.attachments + photos.length
                }))
                loginStore.displayAttachmentUploadSuccess()
            }
        }).catch((e: any) => console.log(e))
    }

    render() {
        const { settingsStore } = this.props.rootStore

        if (this.state.isCameraOpen) {
            return <CameraScreen callback={this.cameraCallback} />
        }
        if (this.state.imageBrowserOpen) {
            return <ImageBrowserScreen max={settingsStore.rules.attachments.maxPictures} callback={this.imageBrowserCallback} />
        }
        const { loginStore } = this.props.rootStore
        if (this.props.readonly) {
            return this.renderReadonly()
        }

       

        const { name, comments, attachments, picklistvalues, value } = this.state
        const { modalHeaderText, modalPlaceholderText, modalSaveButtonText, helpText, readonly } = this.props
        const MODAL_HEADER = modalHeaderText
        const PLACEHOLDER = modalPlaceholderText
        const showCommentIcon = this.props.rootStore.settingsStore.rules.items.checklistShowActions.comments

        let customStyles = StyleSheet.create({
            picklistContainer: {
                width: this.state.screenWidth,
                paddingRight: 12,
                borderLeftColor: "white",
                borderLeftWidth: 4,
            }
        })

        return (
            <View style={styles.container}>
                <ScrollView
                    ref='_scrollView'
                    horizontal
                    showsHorizontalScrollIndicator={false}>
                    <View style={customStyles.picklistContainer}>
                        <View style={styles.card}>
                            <View style={styles.labelContainer}>
                                {!_.isEmpty(comments) ?
                                    <View style={styles.commentIndicatorContainer}>
                                        <FIcon icon={Icons.COMMENTSDOTS} color={COLORS.PRIMARY} />
                                    </View>
                                    : <View />}
                                {attachments && attachments > 0 ?
                                    <View style={styles.commentIndicatorContainer}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                            <FontAwesome name="file-picture-o" size={18} color={COLORS.PRIMARY} />
                                            <View style={{ padding: 1, backgroundColor: 'orange', position: 'absolute', borderRadius: 10, marginLeft: 8 }}>
                                                <Text style={{ color: '#fff', fontWeight: '600', fontSize: 10 }}>{attachments}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    : <View />}
                                <View style={styles.nameSection}>
                                    <Text style={styles.label}>{name}</Text>
                                    {!_.isEmpty(helpText) && <Text style={styles.helpText}>{helpText}</Text>}
                                </View>
                            </View>
                            <View style={styles.buttonContainer}>
                                {picklistvalues ? picklistvalues.map(_value => {
                                    return _.isEmpty(value)
                                        ? <MultiSelectButton
                                            readonly={readonly}
                                            icon={this.getIcon(_value)}
                                            onToggle={this.onValueSelected}
                                            value={_value}
                                            active={false}
                                        />
                                        : <MultiSelectButton
                                            readonly={readonly}
                                            icon={this.getIcon(_value)}
                                            onToggle={this.onValueSelected}
                                            value={_value}
                                            active={!_.isEmpty(value ? value.filter(item => item.trim() == _value.trim()) : undefined)}
                                        />
                                }) : null}
                            </View>
                        </View>
                    </View>
                    { showCommentIcon ? <View style={styles.commentsContainer}>
                    <FModalInput
                    value={comments}
                    header={modalHeaderText}
                    placeholder={modalPlaceholderText}
                    saveButtonText={modalSaveButtonText}
                    onBack={this.onBack}
                    onSave={this.onSaveComment}
                    triggerItem={this.commentButton()}
                    isReadOnly={readonly}
                    
                    />           
                    </View>
                    : <View/>}
                    <View style={styles.cameraContainer}>
                        {this.cameraButton()}
                    </View>
                    <View style={styles.cameraContainer}>
                        {this.libraryButton()}
                    </View>

                </ScrollView>
            </View>
        );
    }
}

//@ts-ignore
FMultiSelectPickList.defaultProps = {
    onChange: (s) => { },
    //@ts-ignore ???? 
    onSubmit: (s) => { },
    name: "Default Name",
    comments: "",
    picklistvalues: [],
    value: [],
    icons: [],
    modalSaveButtonText: "Save",
    helpText: "",
    readonly: false
}

export default FMultiSelectPickList;