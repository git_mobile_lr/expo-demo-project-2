import { StyleSheet } from 'react-native'
import { DEVICE_WIDTH, COLORS } from '../../constants'


export const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0
    },
    containerReadonly: {
        backgroundColor: "#f0f0f0",
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        flex: 1
    },
    picklistContainer: {
        width: DEVICE_WIDTH,
        paddingRight: 12,
        borderLeftColor: "white",
        borderLeftWidth: 4,
    },
    picklistContainerReadonly: {
        width: DEVICE_WIDTH,
        paddingLeft: 0,
        paddingRight: 12,
        borderLeftColor: "#f0f0f0",
        borderLeftWidth: 4,
    },

    picklistContainerWithComments: {
        width: DEVICE_WIDTH,
        paddingLeft: 12,
        paddingRight: 12,
        borderLeftColor: COLORS.PRIMARY,
        borderLeftWidth: 4,
    },
    card: {
        padding: 12,
        paddingLeft: 0,
        flexDirection: "row"
    },
    cardItem: {
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0
    },
    cardBody: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    label: {
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    helpText: {
        paddingLeft: 12,
        fontFamily: 'Roboto',
        color: COLORS.DARKGRAY,
        fontSize: 12,
        letterSpacing: 0,
        lineHeight: 16,
    },
    nameSection: {
        flex: 8,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    comments: {
        marginTop: 6,
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#BCC8CE',
        letterSpacing: 0,
        lineHeight: 16,
    },
    labelContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: 'center',
    },
    commentIndicatorContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 10
    },
    buttonContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    button: {
        height: 50,
        width: 50,
        borderRadius: 50,
        marginLeft: 12,
        backgroundColor: "#e8f6fe"
    },
    activeButton: {
        height: 40,
        width: 40,
        borderRadius: 40,
        marginLeft: 5,
        backgroundColor: "#48aaf8"
    },
    activeReadonlyButton: {
        height: 40,
        width: 40,
        borderRadius: 40,
        marginLeft: 5,
        backgroundColor: "#c8c8c8"
    },
    inactiveButton: {
        height: 40,
        width: 40,
        borderRadius: 40,
        marginLeft: 5,
        backgroundColor: "#e8f6fe"
    },
    inactiveReadonlyButton: {
        height: 40,
        width: 40,
        borderRadius: 40,
        marginLeft: 5,
        backgroundColor: "#f5f5f5"
    },
    activeText: {
        alignSelf: "center",
        color: "white",
        fontSize: 12,
        height: 13
    },
    activeReadonlyText: {
        alignSelf: "center",
        color: "white",
        fontSize: 12,
        height: 13
    },
    inactiveText: {

        alignSelf: "center",
        color: "#48aaf8",
        fontSize: 12,
        height: 13
    },
    inactiveReadonlyText: {

        alignSelf: "center",
        color: "#c8c8c8",
        fontSize: 12,
        height: 13
    },
    contentContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    commentsContainer: {
        height: "100%",
        backgroundColor: "#48aaf8"
    },
    readOnlyCommentsContainer: {
        height: "100%",
        backgroundColor: '#c8c8c8'
    },
    cameraContainer: {
        height: "100%",
        backgroundColor: "#48aaf8",
        borderLeftWidth: 2,
        borderColor: "#DAE8F2"
    }
})