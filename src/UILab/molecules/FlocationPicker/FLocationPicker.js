import { View, Text, StyleSheet, Dimensions, Platform } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import MapView, { Marker } from 'react-native-maps'
import React, { useState, useEffect, useRef } from 'react'
import { FModal, FUserInput } from '../..'

import * as Location from 'expo-location'
import { inject, observer } from 'mobx-react/native'
import { Item, Label } from 'native-base'
import _ from 'lodash'
import { Ionicons, MaterialIcons, FontAwesome5, MaterialCommunityIcons, FontAwesome } from '@expo/vector-icons'
import BottomSheet from 'reanimated-bottom-sheet'
import ActionButton from 'react-native-action-button'

const { width, height } = Dimensions.get('window')

const LocationPicker = ({ rootStore, props }) => {
  const [ isModalVisible, setIsModalVisible ] = useState(false)
  const [ assetsAreLoaded, setAssetsAreLoaded ] = useState(false)
  const [ data, setData ] = useState(null)
  const [ timeStamp, setTimeStamp ] = useState(null)
  const [ location, setLocation ] = useState(props.valueCoords)
  const [ locationValue, setLocationValue ] = useState(props.value)
  const [ userLocation, setUserLocation ] = useState(null)
  const [ userLocationAccuracy, setUserLocationAccuracy ] = useState(null)
  const [ displayAddress, setDisplayAddress ] = useState(null)
  const [ isRegionSet, setIsRegionSet ] = useState(false)
  // should be move to global permission handler
  const [errorMsg, setErrorMsg] = useState(null) 
  // to store, city, country and so on.
  const [address, setAddress] = useState({});
  
  const sheetRef = useRef(null)
  const mapRef = useRef(null)
  const markerRef = useRef(null)

  const { loginStore } = rootStore

  // componentDidMount
  useEffect(() => {
    (async () => {
      const { status } = await Location.requestForegroundPermissionsAsync()
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied')
        return
      }

      const loc = await Location.getCurrentPositionAsync({})
      if (loc) {
        setUserLocation(loc)
        setTimeStamp(loc.timestamp)
        setUserLocationAccuracy(Math.round(loc.coords.accuracy))
      }

      if (locationValue) {
        setLocation(props.valueCoords)
        geocodeAddress(props.valueCoords.coords)
      } else {
        setLocation(loc)
        geocodeAddress(loc)
      }
    })()
  }, [])

  useEffect(() => {
    
    return () => {
      console.warn("This is unmounted.");
    };
  }, []);

  // componentDidUpdate
  useEffect(() => {
    setIsModalVisible(props.isEditing)

    // if (!_.includes(["InstalledProductCreation", "InstalledProductRelation"], this.props.rootStore.loginStore.activeScreen)) {
    //   // console.warn(this.props.rootStore.appsStore.getActiveScreen() + " " + this.props.rootStore.loginStore.activeScreen)
    //   if (!this.state.isModalVisible && !this.props.isEditing) {
    //       if (this.state.value !== this.props.value) {
    //           this.setState({ value: this.props.value })
    //       }
    //   }
    // }
    
  }, [props.isEditing])

  const geocodeAddress = async (coords) => {
    try {
      const { latitude, longitude } = coords
      const response = await Location.reverseGeocodeAsync({
        latitude,
        longitude
      })

      for (let item of response) {
        let addressInfo = {
          street: item.street,
          postalCode: item.postalCode,
          city: item.city
        }

        let filteredAdressInfo = Object.keys(addressInfo)
        .filter((k) => addressInfo[k] != null)
        .reduce((a, k) => ({ ...a, [k]: addressInfo[k] }), {});

        let availableInfo = `${Object.values(filteredAdressInfo).map(i => i ? `${i}` : '').join(", ")}`
        setDisplayAddress(availableInfo)

        setAddress({
          ...addressInfo,
          country: item.country
        })
      }
    } catch (err) {
      console.log(err)
    }
  }

  const checkEditPrivilege = (readonly) => {
    try {
      if (!readonly && props.WO[loginStore.getPrefixedFieldName("Status__c")] == "CLOSED") {
        let editPrivilege = loginStore.checkForPrivilege("attachments", "onEdit")
        if (editPrivilege) {
          return false
        }
        else {
          return true
        }
      }
    } catch (error) {
      //pass
    }
    return readonly
  }

  const renderTextArea = () => {
    // const _isReadOnly = checkEditPrivilege(false)
    if(!props.field.geoMapping)return null
    const { table } = props.section
    const { geoMapping } = props.field
    if (!geoMapping.hasOwnProperty('DESCRIPTION')) return null
    const { contextData } = props.rootStore.dataStore

    const fieldName = loginStore.getPrefixedFieldName('Description__c')
    const meta = props.rootStore.metaStore.getFieldMeta(table, fieldName)
    const fieldData = contextData[table][fieldName] ? contextData[table][fieldName] : ''
    const Id = contextData[table].Id

    if (meta.type === 'success') {
      const { label, type, name, length } = meta['data']

      return (
        <FUserInput
          style={{
            width: width,
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            alignContent: "center",
            backgroundColor: "transparent"
          }}
          key={'key' + 'test'}
          label={label}
          name={name}
          type={type}
          value={fieldData}
          maxLength={length}
          returnKeyType="go"
          autoFocus={true}
          readonly={props.readonly}
          displayTextArea={true}
          onSubmit={async (value) => {
            try {
              contextData[table][fieldName] = value
              let _SUBCHANGE = Object.assign({}, {
                "Id": Id,
                "target": table
              })
              _SUBCHANGE[fieldName] = value

              return await loginStore.updateRecord(table, Id, _SUBCHANGE)
            } catch (e) { }
          }}
        />
      )
    }
  }

  const handleRegionChange = (coords) => {
    if (!isRegionSet) return
    setLocation({coords: coords})
  }

  const onSave = () => {
    const { latitude, longitude } = location.coords

    const { validationStore } = rootStore
    let validationResult = validationStore.validateInput(location, props.type)

    if (_.isEmpty(validationResult)) {
      console.log("address:", address)
      props.onSubmit({ latitude, longitude, ...address })
      setIsRegionSet(false)
      props.toggleIsEditing()

      return true
    }
    return false
  }

  // debug this for when value is empty
  const handleBack = () => {
    // if (props.value) {
    //   setLocation(props.valueCoords)
    // } else {
    //   setLocation(null)
    // }

    setLocation(props.valueCoords)
    setIsRegionSet(false)
    setIsModalVisible(false)
  }


  const getCenterOffsetForAnchor = (anchor, markerWidth, markerHeight) => ({
    x: markerWidth * 0.5 - markerWidth * anchor.x,
    y: markerHeight * 0.5 - markerHeight * anchor.y,
  })
  

  const MARKER_DIMENSION = 48
  const ANCHOR = { x: 0.5, y: 1 - 10 / MARKER_DIMENSION }
  const CENTEROFFSET = getCenterOffsetForAnchor(
    ANCHOR,
    MARKER_DIMENSION,
    MARKER_DIMENSION,
  )

  const renderSheetHeader = () => {
    return (
      <View 
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
          height: 20,
          paddingTop: 2
        }}
      >
        <View style={{ width: 40, height: 5, backgroundColor: '#b6c2cd', borderRadius: 5 }}/>
      </View>
    )
  }

  const renderContent = () => {

    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
          {/* HEADER */}
          <View style={{ padding: 15, flexDirection: 'row', paddingTop: 0 }}>
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center' }}>
              {renderContentHeader()}
            </View>
          </View>

          <View style={{ height: 1, width, backgroundColor: '#E2E2E3' }}/>

          {/* CONTENT */}
          <View style={{ flex: 1 }}>
            {renderTextArea()}
          </View>

          <View style={{ height }}/>
      </View>
    )
  }

  const renderContentHeader = () => {

      return (
        <View
          onPress={() => onSave()} // onSave with passing the currentUserLocation
          style={{ flexDirection: 'row', alignItems: 'center', width }}>
          <View
              style={{
                width: 55,
                height: 55,
                marginRight: 10,
                borderRadius: 10,
                backgroundColor: '#E4F1FD',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Ionicons name="pin" size={24} color='#00AAFF' />
          </View>

          <View>
            <View style={{ width, flexDirection: 'row' }}>
              <Text
                style={{
                  flex: 1,
                  flexWrap: 'wrap',
                  fontSize: 14,
                  fontWeight: '600',
                  color: '#666'
                }}
              >
                {displayAddress}
              </Text>
            </View>
          </View>

        </View>
      )
  }



  const _renderMyCurrentLocationButton = () =>{
    if(Platform.OS === 'android') return null;

    return  userLocation && !props.readonly
      ? (<ActionButton
          offsetY={loginStore.isTablet ? height/2.5 : height/6}
          size={loginStore.isTablet ? 100 : 56}
          position="right"
          buttonColor="#fff"
          onPress={() => {
            mapRef.current.animateToRegion({
              latitude: userLocation.coords.latitude,
              longitude: userLocation.coords.longitude,
              latitudeDelta: 0.0020,
              longitudeDelta: 0.0010
            })
          }}
          renderIcon={() => <FontAwesome5 name="location-arrow" size={loginStore.isTablet ? 35 : 24} color="#00AAFF" />} 
        />)
      : (<ActionButton
      offsetY={loginStore.isTablet ? height/2.5 : height/2.3}
          size={loginStore.isTablet ? 100 : 56}
          position="right"
          buttonColor="#a8a8a8"
          renderIcon={() => <FontAwesome5 name="location-arrow" size={loginStore.isTablet ? 35 : 24} color="#fff" />}
        />)
    
  }

  const _renderMyCurrentLocationButton2 = () =>{
    if(Platform.OS === 'android') return null;

    return userLocation && !props.readonly
    ? (<ActionButton
        offsetY={loginStore.isTablet ? isPortrait() ? '40%' : '27%' : height/4}
        size={loginStore.isTablet ? 100 : 56}
        position="right"
        buttonColor="#fff"
        onPress={() => {
          mapRef.current.animateToRegion({
            latitude: userLocation.coords.latitude,
            longitude: userLocation.coords.longitude,
            latitudeDelta: 0.0020,
            longitudeDelta: 0.0010
          })
        }}
        renderIcon={() => <FontAwesome5 name="location-arrow" size={loginStore.isTablet ? 35 : 24} color="#00AAFF" />} 
      />)
    : (<ActionButton
        offsetY={loginStore.isTablet ? isPortrait() ? '40%' : '27%' : height/4}
        size={loginStore.isTablet ? 100 : 56}
        position="right"
        buttonColor="#a8a8a8"
        renderIcon={() => <FontAwesome5 name="location-arrow" size={loginStore.isTablet ? 35 : 24} color="#fff" />}
      />)
  }

  const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;      
  };

  const onRegionChangeComplete = (region) => {
    if(!isRegionSet) return;
    handleRegionChange(region)
    geocodeAddress(region)
  }

  const _renderMap = () => {
    if (location) {
      const { latitude, longitude } = location.coords
      const prevLatitude = props.valueCoords.coords.latitude
      ? props.valueCoords.coords.latitude
      : latitude
      const prevLongitude = props.valueCoords.coords.longitude
      ? props.valueCoords.coords.longitude
      : longitude


      if (latitude === null && longitude === null) {
        return (
          <View style={{ flex: 1 }}>
            <MapView
              ref={mapRef}
              zoomEnabled
              showsUserLocation
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            />
  
            {!props.readonly
              ? <ActionButton
                  size={loginStore.isTablet ? 100 : 56}
                  renderIcon={() => isRegionSet
                    ? <FontAwesome name="undo" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
                    : <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
                  }
                  onPress={() => {
                    if (isRegionSet) {
                      mapRef.current.animateToRegion({
                        latitude: prevLatitude,
                        longitude: prevLongitude,
                        latitudeDelta: 0.0020,
                        longitudeDelta: 0.0010
                      })
                      setLocation(props.valueCoords)
                      setIsRegionSet((prevState) => !prevState)
                    } else {
                      setIsRegionSet((prevState) => !prevState)
                    }
                  }}
                  offsetY={height/4}
                  buttonColor="#fff"
                />
              : <ActionButton
                  size={loginStore.isTablet ? 100 : 56}
                  renderIcon={() => <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#fff'  />}
                  offsetY={height/4}
                  buttonColor="#a8a8a8"
                />
            }
  
  
            {_renderMyCurrentLocationButton()}
  
            <BottomSheet
              ref={sheetRef}
              renderHeader={renderSheetHeader}
              isBackDrop={true}
              backDropColor="red"
              snapPoints={[300, 175]}
              initialSnap={1}
              borderRadius={30}
              renderContent={renderContent}
              enabledInnerScrolling={false}
            />
          </View>
        )
      }

      return (
        <View style={{ flex: 1 }}>
          <MapView
            ref={mapRef}
            zoomEnabled
            // onRegionChange={(region) => handleRegionChange(region)}
            onRegionChangeComplete={onRegionChangeComplete}
            showsUserLocation
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center'
            }}
            initialRegion={{
              latitude: latitude,
              longitude: longitude,
              latitudeDelta: 0.0020,
              longitudeDelta: 0.0010
            }}
          >
            <Marker
              ref={markerRef}
              pinColor='#00AAFF'
              coordinate={{ latitude: latitude, longitude: longitude }}
            />
          </MapView>

          {!props.readonly
            ? <ActionButton
                size={loginStore.isTablet ? 100 : 56}
                renderIcon={() => isRegionSet
                  ? <FontAwesome name="undo" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
                  : <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
                }
                onPress={() => {
                  if (isRegionSet) {
                    mapRef.current.animateToRegion({
                      latitude: prevLatitude,
                      longitude: prevLongitude,
                      latitudeDelta: 0.0020,
                      longitudeDelta: 0.0010
                    })
                    setLocation(props.valueCoords)
                    setIsRegionSet((prevState) => !prevState)
                  } else {
                    setIsRegionSet((prevState) => !prevState)
                  }
                }}
                offsetY={loginStore.isTablet ? isPortrait() ? '25%' : '17%' : height/3}
                buttonColor="#fff"
              />
            : <ActionButton
                size={loginStore.isTablet ? 100 : 56}
                renderIcon={() => <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#fff'  />}
                offsetY={loginStore.isTablet ? isPortrait() ? '25%' : '17%' : height/3}
                buttonColor="#a8a8a8"
              />
          }


          {_renderMyCurrentLocationButton2()}

          <BottomSheet
            ref={sheetRef}
            renderHeader={renderSheetHeader}
            isBackDrop={true}
            backDropColor="red"
            snapPoints={[300, 175]}
            initialSnap={1}
            borderRadius={30}
            renderContent={renderContent}
            enabledInnerScrolling={false}
          />
        </View>
      )
    }
  }


  const { maxLength, type, value, label } = props
  return (
    <Item stackedLabel style={styles.FInput}>
      <Label style={styles.label}>{label}</Label>
      <Item style={styles.FModal}>
        <FModal
          maxLength={maxLength}
          scrollable={false}
          saveButton={!props.readonly}
          displayTextArea={false}
          disabled={false}
          label={label}
          type={type}
          isModalVisible={isModalVisible}
          value={value}
          multiSelection={false}          
          header={label}
          onBack={() => handleBack()}
          onSave={() => onSave()}
        >
          {_renderMap()}
        </FModal>
      </Item>
    </Item>
  )
}

const styles = StyleSheet.create({
  label:{
    fontFamily: 'Roboto',
    fontSize: 11,
    color: '#5384A6',
    letterSpacing: 0,
    lineHeight: 16,
  },
  input:{
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#0D3754',
    letterSpacing: 0,
    lineHeight: 24,
    paddingLeft:0,
    paddingLeft:24,

},
textError:{
    paddingLeft:24,
    fontFamily: 'Roboto',
    fontSize: 16,
    letterSpacing: 0,
    lineHeight: 24,
    borderColor: 'transparent',
    color:'red',
},
  FInput:{
    height:10,
    // width: isTablet ? '96%' : '92%',
    width: '92%',
    borderColor: 'transparent'
  },
  FModal:{
    flex: 1, 
    flexDirection: 'row',
    justifyContent: 'space-between',
    height:10,
    width:'100%',
    borderColor: 'transparent'
  },
  placeholder:{
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#B8C4CC',
    letterSpacing: 0,
    lineHeight: 24,
    paddingLeft:0   
  },
  FInputMultiLine: {

    minHeight: 65,
    height: "auto",
    // width: IS_TABLET ? '96%' : '92%',
    width: '92%',
    borderColor: 'transparent'
  }
  
})

@inject('rootStore')
@observer
class FLocationPicker extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <LocationPicker
        rootStore={this.props.rootStore}
        props={this.props}
      />
    )
  }
}

// const FLocationPicker = inject("rootStore")(observer(({ rootStore, props }) => {
//   return <LocationPicker rootStore={rootStore} props={props} />
// }))



// check / remove the unnecessary props
FLocationPicker.defaultProps = {
  onChange: () => { },
  onSubmit: () => { },
  label: "$Label.InputLabelDefault",
  maxLength: 80,
  value: "",
  locked: false,
  required: false,
  name: "defaultName",
  keyboardType: "default",
  showValidationErrors: true,
  inModal: true,
  isEditing: false,
  readonly: false,
  placeholder: "",
  displayTextArea: false
};

export default FLocationPicker
