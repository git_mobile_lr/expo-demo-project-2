import { View, Text, Dimensions, Platform } from 'react-native'
import MapView, { Marker } from 'react-native-maps'
import React, { useState, useEffect, useRef, useCallback } from 'react'
import { FModal, FUserInput } from '../..'
import * as Location from 'expo-location'
import { inject, observer } from 'mobx-react/native'
import _ from 'lodash'
import { Ionicons, FontAwesome5, FontAwesome } from '@expo/vector-icons'
import BottomSheet from 'reanimated-bottom-sheet'
import ActionButton from 'react-native-action-button'

const { width, height } = Dimensions.get('window')

const LocationPicker = ({ rootStore, props }) => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [timeStamp, setTimeStamp] = useState(null)
  const [location, setLocation] = useState(props.valueCoords)
  const [locationValue, setLocationValue] = useState(props.value)
  const [userLocation, setUserLocation] = useState(null)
  const [userLocationAccuracy, setUserLocationAccuracy] = useState(null)
  const [displayAddress, setDisplayAddress] = useState(null)
  const [isRegionSet, setIsRegionSet] = useState(false)
  // should be move to global permission handler
  const [errorMsg, setErrorMsg] = useState(null)
  const [address, setAddress] = useState({});
  const [description, setDescription] = useState("")

  const sheetRef = useRef(null)
  const mapRef = useRef(null)
  const markerRef = useRef(null)

  const { loginStore } = rootStore

  // componentDidMount
  useEffect(() => {
    (async () => {
      const { status } = await Location.requestForegroundPermissionsAsync()
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied')
        return
      }
      const loc = await Location.getCurrentPositionAsync({})
      if (loc) {
        setUserLocation(loc)
        setUserLocationAccuracy(Math.round(loc.coords.accuracy))
      }

      if (!props.valueCoords) {
        if (loc) {
          setLocation(loc)
          setTimeStamp(loc.timestamp)
          geocodeAddress(loc.coords)
        }
      } else {
        geocodeAddress(props.valueCoords.coords)
      }
    })()
  }, [])

  useEffect(() => {
    
    if (props.isNew) return; 
    // If it is editing existing location, set current description
    const fieldName = props.rootStore.loginStore.getPrefixedFieldName('Description__c')
    const markers = props.rootStore.dataStore.markers;
    const currentActiveMarkerIndex = props.rootStore.dataStore.getActiveMarkerIndex;
    const currentMarker = markers[currentActiveMarkerIndex];
    const fieldData = currentMarker.Pin[fieldName] 

    setDescription(fieldData)
  }, []);

  // componentDidUpdate
  useEffect(() => {
    setIsModalVisible(props.isEditing)
  }, [props.isEditing])

  const geocodeAddress = useCallback(async(coords) => {
    try {
      const { latitude, longitude } = coords
      const response = await Location.reverseGeocodeAsync({
        latitude,
        longitude
      })

      for (let item of response) {
        let addressInfo = {
          street: item.street,
          postalCode: item.postalCode,
          city: item.city,
        }

        let filteredAdressInfo = Object.keys(addressInfo)
          .filter((k) => addressInfo[k] != null)
          .reduce((a, k) => ({ ...a, [k]: addressInfo[k] }), {});

        let availableInfo = `${Object.values(filteredAdressInfo).map(i => i ? `${i}` : '').join(", ")}`
        setDisplayAddress(availableInfo)
        setAddress({
          ...addressInfo,
          country: item.country
        })
      }
    } catch (err) {
      console.log("geocoding error:", err)
    }

  }, []);

  const renderTextArea = () => {
    // const _isReadOnly = checkEditPrivilege(false)
    if (!props?.field?.geoMapping) return null
    const { table } = props.section
    const { geoMapping } = props.field
    if (!geoMapping.hasOwnProperty('DESCRIPTION')) return null

    const fieldName = loginStore.getPrefixedFieldName('Description__c')
    const meta = props.rootStore.metaStore.getFieldMeta(table, fieldName)

    if (meta.type === 'success') {
      const { label, type, name, length } = meta['data']

      return (
        <FUserInput
          style={{
            width: width,
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            alignContent: "center",
            backgroundColor: "transparent"
          }}
          key={'key' + 'test'}
          label={label}
          name={name}
          type={type}
          value={description}
          maxLength={length}
          returnKeyType="go"
          autoFocus={true}
          readonly={props.readonly}
          displayTextArea={true}
          onSubmit={async (value) => {
            setDescription(value)
          }}
        />
      )
    }
  }

  const handleRegionChange = useCallback((coords) => {
    setLocation({ coords: coords })
  }, [])

  const onSave = () => {
    const { latitude, longitude } = location.coords

    const { validationStore } = rootStore
    let validationResult = validationStore.validateInput(location, props.type)

    if (_.isEmpty(validationResult)) {
      props.onSubmit({ latitude, longitude, ...address, description })
      setIsRegionSet(false)
      props.toggleIsEditing && props.toggleIsEditing()

      return true
    }
    return false
  }

  // debug this for when value is empty
  const handleBack = () => {
    // setLocation(props.valueCoords)
    setIsRegionSet(false)
    setIsModalVisible(false)
    props.toggleIsEditing && props.toggleIsEditing()

  }

  const getCenterOffsetForAnchor = (anchor, markerWidth, markerHeight) => ({
    x: markerWidth * 0.5 - markerWidth * anchor.x,
    y: markerHeight * 0.5 - markerHeight * anchor.y,
  })


  const MARKER_DIMENSION = 48
  const ANCHOR = { x: 0.5, y: 1 - 10 / MARKER_DIMENSION }
  const CENTEROFFSET = getCenterOffsetForAnchor(
    ANCHOR,
    MARKER_DIMENSION,
    MARKER_DIMENSION,
  )

  const renderSheetHeader = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
          height: 20,
          paddingTop: 2
        }}
      >
        <View style={{ width: 40, height: 5, backgroundColor: '#b6c2cd', borderRadius: 5 }} />
      </View>
    )
  }

  const renderContent = () => {

    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {/* HEADER */}
        <View style={{ padding: 15, flexDirection: 'row', paddingTop: 0 }}>
          <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center' }}>
            {renderContentHeader()}
          </View>
        </View>

        <View style={{ height: 1, width, backgroundColor: '#E2E2E3' }} />

        {/* CONTENT */}
        <View style={{ flex: 1 }}>
          {renderTextArea()}
        </View>

        <View style={{ height }} />
      </View>
    )
  }

  const renderContentHeader = () => {
    return (
      <View
        onPress={() => onSave()} // onSave with passing the currentUserLocation
        style={{ flexDirection: 'row', alignItems: 'center', width }}>
        <View
          style={{
            width: 55,
            height: 55,
            marginRight: 10,
            borderRadius: 10,
            backgroundColor: '#E4F1FD',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Ionicons name="pin" size={24} color='#00AAFF' />
        </View>

        <View>
          <View style={{ width, flexDirection: 'row' }}>
            <Text
              style={{
                flex: 1,
                flexWrap: 'wrap',
                fontSize: 14,
                fontWeight: '600',
                color: '#666'
              }}
            >
              {displayAddress}
            </Text>
          </View>
        </View>

      </View>
    )
  }

  const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;      
  };

  const _renderMyCurrentLocationButton = () => {
    if (Platform.OS === 'android') return null;

    return userLocation && !props.readonly
      ? (<ActionButton
        offsetY={loginStore.isTablet ? isPortrait() ? '40%' : '27%' : height/4}
        size={loginStore.isTablet ? 100 : 56}
        position="right"
        buttonColor="#fff"
        onPress={() => {
          mapRef.current.animateToRegion({
            latitude: userLocation?.coords?.latitude,
            longitude: userLocation?.coords?.longitude,
            latitudeDelta: 0.0020,
            longitudeDelta: 0.0010
          })
        }}
        renderIcon={() => <FontAwesome5 name="location-arrow" size={loginStore.isTablet ? 35 : 24} color="#00AAFF" />}
      />)
      : (<ActionButton
        offsetY={loginStore.isTablet ? isPortrait() ? '40%' : '27%' : height/4}
        size={loginStore.isTablet ? 100 : 56}
        position="right"
        buttonColor="#a8a8a8"
        renderIcon={() => <FontAwesome5 name="location-arrow" size={loginStore.isTablet ? 35 : 24} color="#fff" />}
      />)

  }

  const onRegionChangeComplete = (region) => {
    if(!isRegionSet) return;
    handleRegionChange(region)
    geocodeAddress(region)
  }

  const _renderMap = () => {

    if (location) {
      const { latitude, longitude } = location.coords
      const prevLatitude = props?.valueCoords?.coords?.latitude
        ? props?.valueCoords?.coords?.latitude
        : latitude
      const prevLongitude = props?.valueCoords?.coords?.longitude
        ? props?.valueCoords?.coords?.longitude
        : longitude

      if (latitude === null && longitude === null) {
        return (
          <View style={{ flex: 1 }}>
            <MapView
              ref={mapRef}
              zoomEnabled
              showsUserLocation
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            />

            {!props.readonly
              ? <ActionButton
                size={loginStore.isTablet ? 100 : 56}
                renderIcon={() => isRegionSet
                  ? <FontAwesome name="undo" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
                  : <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
                }
                onPress={() => {
                  if (isRegionSet) {
                    mapRef.current.animateToRegion({
                      latitude: prevLatitude,
                      longitude: prevLongitude,
                      latitudeDelta: 0.0020,
                      longitudeDelta: 0.0010
                    })
                    setLocation(props.valueCoords)
                    setIsRegionSet((prevState) => !prevState)
                  } else {
                    setIsRegionSet((prevState) => !prevState)
                  }
                }}
                offsetY={loginStore.isTablet ? isPortrait() ? '25%' : '17%' : height/3}
                buttonColor="#fff"
              />
              : <ActionButton
                size={loginStore.isTablet ? 100 : 56}
                renderIcon={() => <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#fff' />}
                offsetY={loginStore.isTablet ? isPortrait() ? '25%' : '17%' : height/3}
                buttonColor="#a8a8a8"
              />
            }

            {_renderMyCurrentLocationButton()}

            <BottomSheet
              ref={sheetRef}
              renderHeader={renderSheetHeader}
              isBackDrop={true}
              backDropColor="red"
              snapPoints={[300, 175]}
              initialSnap={1}
              borderRadius={30}
              renderContent={renderContent}
              enabledInnerScrolling={false}
            />
          </View>
        )
      }

      return (
        <View style={{ flex: 1 }}>
          <MapView
            ref={mapRef}
            zoomEnabled
            // onRegionChange={(region) => handleRegionChange(region)}
            onRegionChangeComplete={onRegionChangeComplete}
            showsUserLocation
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center'
            }}
            initialRegion={{
              latitude: latitude,
              longitude: longitude,
              latitudeDelta: 0.0020,
              longitudeDelta: 0.0010
            }}
          >
            <Marker
              ref={markerRef}
              pinColor='#00AAFF'
              coordinate={{ latitude: latitude, longitude: longitude }}
            />
          </MapView>

          {!props.readonly
            ? <ActionButton
              size={loginStore.isTablet ? 100 : 56}
              renderIcon={() => isRegionSet
                ? <FontAwesome name="undo" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
                : <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#00AAFF' />
              }
              onPress={() => {
                if (isRegionSet) {
                  mapRef.current.animateToRegion({
                    latitude: prevLatitude,
                    longitude: prevLongitude,
                    latitudeDelta: 0.0020,
                    longitudeDelta: 0.0010
                  })
                  setLocation(props.valueCoords)
                  setIsRegionSet((prevState) => !prevState)
                } else {
                  setIsRegionSet((prevState) => !prevState)
                }
              }}
              offsetY={loginStore.isTablet ? isPortrait() ? '25%' : '17%' : height/3}
              buttonColor="#fff"
            />
            : <ActionButton
              size={loginStore.isTablet ? 100 : 56}
              renderIcon={() => <FontAwesome name="pencil" size={loginStore.isTablet ? 35 : 24} color='#fff' />}
              offsetY={loginStore.isTablet ? isPortrait() ? '25%' : '17%' : height/3}
              buttonColor="#a8a8a8"
            />
          }


          {_renderMyCurrentLocationButton()}

          <BottomSheet
            ref={sheetRef}
            renderHeader={renderSheetHeader}
            isBackDrop={true}
            backDropColor="red"
            snapPoints={[300, 175]}
            initialSnap={1}
            borderRadius={30}
            renderContent={renderContent}
            enabledInnerScrolling={false}
          />
        </View>
      )
    } else {
      return <View  />
    }
  }


  const { maxLength, type, value, label } = props
  return (
      <FModal
        maxLength={maxLength}
        scrollable={false}
        saveButton={!props.readonly}
        displayTextArea={false}
        disabled={false}
        label={label}
        type={type}
        isModalVisible={isModalVisible}
        value={value}
        multiSelection={false}
        header={label}
        onBack={() => handleBack()}
        onSave={() => onSave()}
        triggerItem={props.triggerItem}
        triggerItemStyle={props.triggerItemStyle}
        hideTriggerItem={props.hideTriggerItem}
      >
        {_renderMap()}
      </FModal>
  )
}

@inject('rootStore')
@observer
class FSubLocationPicker extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <LocationPicker
        rootStore={this.props.rootStore}
        props={this.props}
      />
    )
  }
}

// check / remove the unnecessary props
FSubLocationPicker.defaultProps = {
  onChange: () => { },
  onSubmit: () => { },
  label: "$Label.InputLabelDefault",
  maxLength: 80,
  value: "",
  locked: false,
  required: false,
  name: "defaultName",
  keyboardType: "default",
  showValidationErrors: true,
  inModal: true,
  isEditing: false,
  readonly: false,
  placeholder: "",
  displayTextArea: false,
  isNew: false,
};

export default FSubLocationPicker
