import {StyleSheet, Platform} from 'react-native'
import { DE } from '../../../i18n/locales'

import { DEVICE_WIDTH, IS_TABLET } from '../../constants'

export const styles = StyleSheet.create({
    card:{
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        width: "100%"
    },
    card2:{
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:0
    },
    nbCard: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        width: DEVICE_WIDTH
    },
    roCard:{
        flex: 1,
        
        width: "100%",
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        elevation:0  
    },
    required:{
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        borderLeftColor:'#ff0000',
        borderColor:'#ff0000',
        borderLeftWidth:4,
        width : DEVICE_WIDTH
    },
    inlineInputIcon:{
        fontSize:15,
        color:"#00AAFF",
        alignSelf:"center",
        paddingRight:15
    },
    commentIndicatorContainer: {
        marginRight: 10
    },
    cardBody:{
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    cardItem:{
        flex: 1,
        paddingRight:0,
        paddingTop:0,
        paddingBottom:0
    },
    readOnlyCard:{
        paddingRight:0,
        paddingTop:0,
        paddingBottom:0,       
        backgroundColor:'#f0f0f0'
    },
    lockedText:{
        fontSize: 16,
        color: '#B8C4CC',
        marginRight:20,
        marginTop:30
    },
    lockIcon:{
        fontSize: 20,
        color: '#B8C4CC',
        marginRight:25,
        marginTop:30
    },
    requiredIcon:{
        fontSize: 15,
        color: '#B8C4CC',
        marginRight:15,
        marginTop:20
    },
    error:{
        fontSize: 15,
        color: 'red',
        marginRight:20,
        marginTop:30
    },
    label:{
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    input:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 16,
        paddingLeft:0,
        borderColor: 'transparent'
    },
    FInput:{
        height:10,
        width:IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent'
    },
    inlineUnitText: {
        fontSize:15,
        color: "#00AAFF",
        alignSelf:"center",
        paddingRight: 15,
        fontWeight: '500'
    },
    roInlineUnitText: {
        fontSize:15,
        color: "#A9ABAC",
        alignSelf:"center",
        paddingRight: 15,
        fontWeight: '500'
    },
    placeholder:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#B8C4CC',
        letterSpacing: 0,
        lineHeight: 16,
        paddingLeft:0   
    },
    commentsContainer: {
        height: "100%",
        backgroundColor: "#48aaf8"
    },
    readonlyCommentsContainer: {
        height: "100%",
        backgroundColor: "#c8c8c8",
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1
    },
    cameraContainer: {
        height: "100%",
        backgroundColor: "#48aaf8",
        borderLeftWidth: 2,
        borderColor: "#DAE8F2"
    }
})
