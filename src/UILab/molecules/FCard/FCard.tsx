import React from 'react';
import { TouchableOpacity, Linking, Platform, ScrollView, Text, Alert, StyleSheet } from 'react-native'
import { Dimensions } from 'react-native';
import { upsertScreenModes } from '../../../types'
import * as Sentry from 'sentry-expo';
import FModalInput from '../../molecules/FModalInput/FModalInput'

import {
  Card,
  CardItem,
  Body,
  Item,
  Label,
  Input,
  View
} from 'native-base'
import { styles } from './FCardStyles'
import { FontAwesome, FontAwesome5, MaterialCommunityIcons } from "@expo/vector-icons"
import _  from 'lodash'
import moment from 'moment'
import { RecordTypes, RecordType } from '../../../types'
import * as Animatable from 'react-native-animatable'
import ImageBrowserScreen from '../../../screens/ImageBrowserScreen'
import CameraScreen from '../../../screens/CameraScreen'


type Props = {
  onChange: (value: any) => void
  validate: (result: boolean) => void
  onPress: () => void
  label: string
  placeholder: string
  type: RecordType
  id?: string
  value?: any
  required?: boolean
  readOnly?: boolean
  readOnlyFromConfig?: boolean
  rootStore?: any
  locked: boolean
  children?: JSX.Element
  touchable: boolean
  name: string
  showValidationErrors?: boolean
  filterLogic?: string
  apiName?: string
  formula?: string
  isEditing: boolean
  valid?: boolean
  permissionRequired: () => boolean
  accessibilityLabel: string
  testID: string
  scrollable?: boolean
  comments?: string
  unit?: string
  onSubmit: (value: any) => void
  modalHeaderText?: string
  modalPlaceholderText?: string
  modalSaveButtonText: string ,
  barcodeEnabled? : boolean,
  valueCoords?: any
}

type State = {
  value: any
  visible: boolean
  valid: boolean
  showValidationErrors: boolean
  readOnly: boolean
  children: JSX.Element
  isEditing: boolean
  lastUpdate?: string
  comments?: string
  attachments?: number
  imageBrowserOpen: boolean
  isCameraOpen: boolean
  isBarcodeOpen: boolean,
  scannerData: string,
  validationError: string,
  screenWidth : number
}

import { inject, observer } from "mobx-react/native"
import { COLORS, DEVICE_WIDTH } from '../../constants';
import { FModal } from '../..';

@inject('rootStore')
@observer
class FCard extends React.Component<Props, State> {
  _isMounted = false
  constructor(props: Props) {
    super(props);
    this.state = {
      visible: true,
      value: props.value,
      comments: props.comments,
      attachments: 0,
      valid: true,
      showValidationErrors: false,
      readOnly: false,
      children: props.children ? props.children : <View />,
      isEditing: props.isEditing,
      lastUpdate: props.rootStore.dataStore.lastUpdate,
      imageBrowserOpen: false,
      isCameraOpen: false,
      isBarcodeOpen: false,
      scannerData: "",
      validationError: "",
      screenWidth : Dimensions.get('window').width
    };
  }

  componentWillReact() {

    if (this.state.lastUpdate !== this.props.rootStore.dataStore.lastUpdate) {
      this._loadAsync()
    }
  }

  isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
  };

  async _loadAsync() {
    this.setState({
      lastUpdate: this.props.rootStore.dataStore.lastUpdate
    }, async () => {
      await this._applyFilterLogic()

    })
  }

  async _applyFilterLogic() {
    if (this.props.filterLogic) {
      const { loginStore, dataStore } = this.props.rootStore

      const { result, formula } = dataStore.calcFilterLogic(this.props.apiName)

      this.setState({
        visible: result
      })
    }
  }

  _dimensionsListener : any

  async componentDidMount() {
    const { loginStore } = this.props.rootStore
    this._isMounted = true
    this._loadAsync()
    try {
      if (this.props && this.props.id) {
        const amount = await loginStore.getMediaLengthById(this.props.id)
        this.setState({ attachments: amount })
        this.setState({ valid: this.checkValidation(this.props.value) })
      }
    } catch (error) {
      
    }

    this._dimensionsListener = Dimensions.addEventListener('change', () => {
      let o = this.isPortrait() ? 'portrait' : 'landscape'
      
      // this.props.rootStore.loginStore.setOrientation(o);
      const width = Dimensions.get('window').width;
      this.setState({
        screenWidth: width
      });
    });
    
  }

  componentWillUnmount() {
    this._isMounted = false;

    try {
      
      this._dimensionsListener()
      //Dimensions.removeEventListener('change')
    } catch (error) {

    }
  }

  componentDidUpdate(prevProps: Props) {

    if (!_.isEqual(prevProps.readOnly, this.props.readOnly)) {
      this.setState({
        readOnly: this.props.readOnly ? this.props.readOnly : false
      })
    }
    if (this.state.value != this.props.value) {
      
      this.setState({ value: this.props.value })
    }
    if (this.state.isEditing != this.props.isEditing) {
      this.setState({ isEditing: this.props.isEditing, children: this.props.children ? this.props.children : <View /> })
    }


  }

  onChange = (value: any) => {
    if (this.props.type === 'PICTURE') {
      this.onChangePictureCase()
    }
    else {
      this.setState({
        value: value,
        valid: this.checkValidation(value)
      })

      this.props.onChange(value)
    }
  }
  
  onChangePictureCase = async (value: any) => {
    if (this.props.type === 'PICTURE') {
      this._p(value)
    }

    const amount = await this.props.rootStore.loginStore.getMediaLengthById(this.props.id)
    this.setState({ attachments: amount })

    this.setState({
      value: value,
      valid: this.checkValidation(value)
    })

    this.props.onChange(value)
  }


  handleDataSubmit() {
    const { state } = this
    this.props.onSubmit(state)
  }

  onSaveComment = (value: string) => {
    this.setState({ comments: value }, this.handleDataSubmit)
    // @ts-ignore
    this.onBack()
  }

  onBack = () => {
    if (this.refs._scrollView) {
      // @ts-ignore
      this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
    }
    return
  }

  checkValidation(value: any) {
    const { validate, type, required } = this.props

    if (required) {
      if (type === 'PICTURE') {
        if (this.state.attachments > 0) {
          validate(true)
          return true
        }
      }

      if (!_.isEmpty(value) && value != "false") {
        validate(true)
        return true
      } else {
        validate(false)
        return false
      }
    } else {
      validate(true)
      return true
    }
  }


  tryOpenUrl(url: any) {
    Linking.canOpenURL(url).then((supported) => {
      supported && Linking.openURL(url)
    })
      .catch(err => {
        console.error('An error occurred: ', err)
      })
  }

  openUrl = async () => {
    try {
      const { type, value } = this.props
      switch (type) {
        case RecordTypes.EMAIL:
          return this.tryOpenUrl(`mailto:${value}`)
        case RecordTypes.PHONE:
          return this.tryOpenUrl(`tel:${value}`)
        case RecordTypes.DATE:
        case RecordTypes.DATETIME:
          return this.openCalendar(value)
      }
    } catch (e) { }
  }

  openCalendar = async (value: any) => {
    if (!value) return;
    try {
      let calendarPerm = await this.props.permissionRequired()

      if (calendarPerm) {
        let _link = ''

        if (Platform.OS === 'ios') {
          const referenceDate = moment.utc("2001-01-01")
          const secondsSinceRefDate = moment(value).unix() - referenceDate.unix();
          _link = `calshow:${secondsSinceRefDate}`
        } else if (Platform.OS === 'android') {
          const msSinceEpoch = moment(value).valueOf()
          _link = `content://com.android.calendar/time/${msSinceEpoch}`
        }
        this.tryOpenUrl(_link)
      }

    } catch (err) {
      console.log('An error occurred: ', err)
    }
  }


  getIcon() {
    const { type, readOnly, readOnlyFromConfig, unit } = this.props

    if (readOnlyFromConfig || readOnly) {
      switch (type) {
        case RecordTypes.FORMULA:
          return (<MaterialCommunityIcons name="calculator-variant" style={styles.inlineInputIcon} />)
        default:
          return <FontAwesome name={'lock'} style={styles.inlineInputIcon} />
      }
    }

    if (unit) {
      return <Text style={styles.inlineUnitText}>{unit}</Text>
    }

    switch (type) {
      case RecordTypes.BOOLEAN:
      case RecordTypes.CHECKBOX:
        return <FontAwesome name={'check'} style={styles.inlineInputIcon} />
      case RecordTypes.EMAIL:
        return <FontAwesome name={'envelope'} style={styles.inlineInputIcon} />
      case RecordTypes.FORMULA:
        return (<MaterialCommunityIcons name="calculator-variant" style={styles.inlineInputIcon} />)
      case RecordTypes.PHONE:
        return <FontAwesome name={'phone'} style={styles.inlineInputIcon} />
      case RecordTypes.REFERENCE:
        return <FontAwesome name={'search'} style={styles.inlineInputIcon} />
      case RecordTypes.PICKLIST:
        return <FontAwesome name={'list'} style={styles.inlineInputIcon} />
      case RecordTypes.RECORDTYPE:
        return <FontAwesome name={'ellipsis-v'} style={styles.inlineInputIcon} />
      case RecordTypes.LOCATION:
        return <FontAwesome name={'map-marker'} style={styles.inlineInputIcon} />
      case RecordTypes.DATE:
      case RecordTypes.DATETIME:
      case RecordTypes.DATETIME2:
        return <FontAwesome name={'calendar'} style={styles.inlineInputIcon} />
      default:
        return <FontAwesome name={'pencil'} style={styles.inlineInputIcon} />
    }
  }


  commentButton() {
      return (
        <View style={{ padding: 24 }}>
          <FontAwesome name="comments" color="white" size={18} />
        </View>
      )
  }

  _goBarcode = async () => {
    this.setState({
      isBarcodeOpen: true      
    }
    )
  }


  scanLineButton() {
    const { type, barcodeEnabled } = this.props    
    const { globalBarcode, typeEnable} = this.props.rootStore.loginStore.doesTypeSupportBarcode(type)
    if ((globalBarcode && typeEnable) || (barcodeEnabled && typeEnable)) {
      return (
        <TouchableOpacity onPress={() => this._goBarcode()}>
          <View style={{ padding: 24 }}>
            <FontAwesome name="barcode" size={18} color="white" />
          </View>
        </TouchableOpacity>
      )
    }
  }

  async _cleanValues(){
    this.props.onSubmit("")
    this.onBack()
  }


  cleanValueButton() {

    const { type } = this.props
    
    const hasValue = !_.isEmpty(this.state.value)

    const supportedTypes = ["PICKLIST", "LOCATION"].map(x=>x.toLowerCase())
    const isSupported = _.includes(supportedTypes, type.toLowerCase())
    const show = isSupported && hasValue

    if (show){
      
      return (
        <TouchableOpacity onPress={() => this._cleanValues()} disabled={!hasValue}>
          <View style={{ padding: 24 }}>
            <FontAwesome5 name="trash" size={18} color="white" />            
          </View>
        </TouchableOpacity>
      )
    }
  }

  async _navigate(){
    const { loginStore } = this.props.rootStore
    if(this.props.valueCoords !== null){
      const {coords} = this.props.valueCoords
      const {longitude, latitude} =coords
    //  alert(JSON.stringify(coords))  
      loginStore.setNavigationOptionsPopupCoordinates(longitude, latitude)
      loginStore.setIsNavigationOptionsPopupVisible = true
    }
    

    // loginStore.navigationOptionsPopupCoordinates = 
    // loginStore.setIsNavigationOptionsPopupVisible = true
  }

  navigateButton() {

    const { type } = this.props
    
    const hasValue = !_.isEmpty(this.state.value)

    const supportedTypes = ["LOCATION"].map(x=>x.toLowerCase())
    const isSupported = _.includes(supportedTypes, type.toLowerCase())
    const show = isSupported && hasValue

    if (show){
      
      return (
        <TouchableOpacity onPress={() => this._navigate()} disabled={!hasValue}>
          <View style={{ padding: 24 }}>
            <FontAwesome5 name="car" size={18} color="white" />            
          </View>
        </TouchableOpacity>
      )
    }
  }

  cameraButton() {
    if (!this.props.rootStore.settingsStore.rules.attachments.allowPhotosUpload || !this.props.rootStore.settingsStore.rules.items.checklistShowActions.attachments) return;

    if (this.props.rootStore.dataStore.currentChecklistMode === upsertScreenModes.UPDATE) {
      return (
        <TouchableOpacity onPress={() => this._useCameraHandler()}>
          <View style={{ padding: 24 }}>
            <FontAwesome name="camera" size={18} color="white" />
          </View>
        </TouchableOpacity>
      )
    }
  }

  libraryButton() {
    if (!this.props.rootStore.settingsStore.rules.attachments.allowPhotosUpload || !this.props.rootStore.settingsStore.rules.items.checklistShowActions.attachments) return;

    if (this.props.rootStore.dataStore.currentChecklistMode === upsertScreenModes.UPDATE) {
      return (
        <TouchableOpacity onPress={() => this._useLibraryHandler()}>
          <View style={{ padding: 24 }}>
            <FontAwesome name="picture-o" size={18} color="white" />
          </View>
        </TouchableOpacity>
      )
    }
  }

  _getFileName(uri: any) {
    try {
      const _u = moment().format("YYYY-MM-DD HH:mm") + '.' + uri
        .split('/')
        .pop()
        .split('.')
        .pop();

      return _u
    } catch (e) {

      return null
    }
  }

  async uploadAsync(result: any) {
    const { loginStore } = this.props.rootStore
    loginStore.setIsAddingAttachment = true
    const { comments } = this.state

    try {

      const _name = this._getFileName(result.uri)

      if (_name) {

        const _file = {
          PathOnClient: result.uri,
          Title: _name,
          VersionData: result.base64,
          FirstPublishLocationId: this.props.id
        }

        //loginStore.setIsSyncing = true

        const _r = await loginStore.uploadAsync(_file)
        this.onBack()
        if (this._isMounted) {
          this.setState(prevState => ({
            //@ts-ignore
            attachments: prevState.attachments + 1
          }))
        }
        loginStore.displayAttachmentUploadSuccess()
      

      }
    } catch (e) {
      let _error = {
        type: 'error',
        context: 'uploadAsync',
        message: e
      }

      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(e);
      });

    } finally {
      setTimeout(async () => {
        const { loginStore } = this.props.rootStore
        loginStore.setIsAddingAttachment = false
        loginStore.setHasMediaChanged = true
        loginStore.setIsSyncing = false
      }, 100)
    }
  }


  renderScrollableReadonlyCard() {
    const {
      locked,
      required,
      label,
      placeholder,
      modalHeaderText,
      modalPlaceholderText,
      modalSaveButtonText,
      readOnly
    } = this.props

    const { value, children, comments, attachments } = this.state

    let customStyles = StyleSheet.create({
      readOnlyCard:{
        paddingRight:0,
        paddingTop:0,
        paddingBottom:0,       
        backgroundColor:'#f0f0f0',
        width : this.state.screenWidth
      },
      roCard:{
        flex: 1,
        width : this.state.screenWidth,
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        elevation:0  
    }
    })

    return (

      <View style={customStyles.readOnlyCard}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          keyboardShouldPersistTaps={'handled'}
          ref='_scrollView'>
          <Card
            pointerEvents="none"
            transparent
            style={customStyles.roCard}>
            <CardItem
              style={[styles.cardItem, { backgroundColor: COLORS.GREY4 }]}>
              {!_.isEmpty(comments) ?
                <View style={styles.commentIndicatorContainer}>
                  <FontAwesome name='comment' color='#c8c8c8' size={18} />
                </View>
                : <View />}
              {attachments && attachments > 0 ?
                <View style={styles.commentIndicatorContainer}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <FontAwesome name="file-picture-o" size={18} color='#c8c8c8' />
                    <View style={{ padding: 1, backgroundColor: '#c8c8c8', position: 'absolute', borderRadius: 10, marginLeft: 8 }}>
                      <Text style={{ color: '#fff', fontWeight: '600', fontSize: 10 }}>{attachments}</Text>
                    </View>
                  </View>
                </View>
                : <View />}
              <Body style={styles.cardBody}>
                {locked && <Item stackedLabel style={styles.input}>
                  <Label style={styles.label}>{label}</Label>
                  <Input
                    disabled
                    style={styles.placeholder}
                    placeholderTextColor='#B8C4CC'
                    value={value === ''
                      ? placeholder
                      : value} />
                </Item>
                }
                {!locked && React.cloneElement(children, { onChange: this.props.type === 'PICTURE' ? this.onChangePictureCase : this.onChange })}
                {this.getIcon()}
              </Body>
            </CardItem>
          </Card>

          <View style={styles.readonlyCommentsContainer}>
            <FModalInput
              value={comments}
              header={modalHeaderText}
              placeholder={modalPlaceholderText}
              saveButtonText={modalSaveButtonText}
              onBack={this.onBack}
              onSave={this.onSaveComment}
              triggerItem={this.commentButton()}
              isReadOnly={readOnly}
            />
          </View>
        </ScrollView>
      </View>
    )
  }

  renderScrollableTouchCard() {
    const {
      locked,
      required,
      label,
      placeholder,
      modalHeaderText,
      modalPlaceholderText,
      modalSaveButtonText,
      readOnly
    } = this.props

    const { value, children, valid, comments, attachments } = this.state
    const showCommentIcon = this.props.rootStore.settingsStore.rules.items.checklistShowActions.comments

    let customStyles = StyleSheet.create({
      card:{
          padding:0,
          marginTop:0,
          marginBottom:0,
          marginLeft:0,
          marginRight:0,
          borderBottomColor:"#DAE8F2",
          borderBottomWidth:1,
          width : this.state.screenWidth
      },
      nbCard: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          padding:0,
          marginTop:0,
          marginBottom:0,
          marginLeft:0,
          marginRight:0,
          width : this.state.screenWidth
      },
      required:{
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        borderLeftColor:'#ff0000',
        borderColor:'#ff0000',
        borderLeftWidth:4,
        width : this.state.screenWidth
      }
    })

    return (

      <View style={valid ? customStyles.card : customStyles.required}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          keyboardShouldPersistTaps={'handled'}
          ref='_scrollView'>
          <TouchableOpacity
            onPress={this.props.onPress}
            onLongPress={this.openUrl}
            testID={this.props.testID}
            accessibilityLabel={this.props.accessibilityLabel}>
            <Card
              pointerEvents="none"
              transparent
              style={customStyles.nbCard}>
              <CardItem
                style={styles.cardItem}>
                {!_.isEmpty(comments) ?
                  <View style={styles.commentIndicatorContainer}>
                    <FontAwesome name='comment' color="#17B1FE" size={18} />
                  </View>
                  : <View />}
                {attachments && attachments > 0 ?
                  <View style={styles.commentIndicatorContainer}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                      <FontAwesome name="file-picture-o" size={18} color={COLORS.PRIMARY} />
                      <View style={{ padding: 1, backgroundColor: 'orange', position: 'absolute', borderRadius: 10, marginLeft: 8 }}>
                        <Text style={{ color: '#fff', fontWeight: '600', fontSize: 10 }}>{attachments}</Text>
                      </View>
                    </View>
                  </View>
                  : <View />}
                <Body style={styles.cardBody}>
                  {locked && <Item stackedLabel style={styles.input}>
                    <Label style={styles.label}>{label}</Label>
                    <Input
                      disabled
                      style={styles.placeholder}
                      placeholderTextColor='#B8C4CC'
                      value={value === ''
                        ? placeholder
                        : value} />
                  </Item>
                  }
                  {!locked && React.cloneElement(children, {value: value, onChange: this.props.type === 'PICTURE' ? this.onChangePictureCase : this.onChange })}
                </Body>
                {this.getIcon()}
              </CardItem>
            </Card>
          </TouchableOpacity>          
          { showCommentIcon ? <View style={styles.commentsContainer}>
            <FModalInput
              value={comments}
              header={modalHeaderText}
              placeholder={modalPlaceholderText}
              saveButtonText={modalSaveButtonText}
              onBack={this.onBack}
              onSave={this.onSaveComment}
              triggerItem={this.commentButton()}
              isReadOnly={readOnly}
              
            />           
            </View>
            : <View/>}
          <View style={styles.cameraContainer}>
            {this.scanLineButton()}
          </View>

          <View style={styles.cameraContainer}>
            {this.cameraButton()}
          </View>
          <View style={styles.cameraContainer}>
            {this.libraryButton()}
          </View>       

        </ScrollView>
      </View>
    )
  }

  renderTouchableCard() {
    const {
      locked,
      required,
      label,
      placeholder
    } = this.props

    const {loginStore} = this.props.rootStore

    const { value, children, valid } = this.state

    let customStyles = StyleSheet.create({
      card:{
          padding:0,
          marginTop:0,
          marginBottom:0,
          marginLeft:0,
          marginRight:0,
          borderBottomColor:"#DAE8F2",
          borderBottomWidth:1,
          width : this.state.screenWidth
      },
      nbCard: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          padding:0,
          marginTop:0,
          marginBottom:0,
          marginLeft:0,
          marginRight:0,
          width : this.state.screenWidth
      },
      required:{
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        borderLeftColor:'#ff0000',
        borderColor:'#ff0000',
        borderLeftWidth:4,
        width : this.state.screenWidth
      }
    })

    return (
      <View style={valid ? customStyles.card : customStyles.required}>
        <ScrollView
          horizontal
          keyboardShouldPersistTaps={'handled'}
          showsHorizontalScrollIndicator={false}
          ref='_scrollView'>
          <TouchableOpacity onPress={this.props.onPress} onLongPress={this.openUrl} style={{marginVertical:0}}
          testID={this.props.testID} accessibilityLabel={this.props.accessibilityLabel}>
            <Card
              pointerEvents="none"
              transparent
              style={valid ?  [styles.card2, {width: loginStore.S_W}]: customStyles.required}>
              <CardItem
                style={styles.cardItem}>
                <Body style={styles.cardBody}>
                  {locked && <Item stackedLabel style={styles.input}>
                    <Label style={styles.label}>{label}</Label>
                    <Input
                      disabled
                      style={styles.placeholder}
                      placeholderTextColor='#B8C4CC'
                      value={value === ''
                        ? placeholder
                        : value} />
                  </Item>
                  }
                  {!locked && React.cloneElement(children, { value: value, onChange: this.props.type === 'PICTURE' ? this.onChangePictureCase : this.onChange })}
                </Body>
                {this.getIcon()}
              </CardItem>
            </Card>
          </TouchableOpacity>
          <View style={styles.cameraContainer}>
            {this.scanLineButton()}            
          </View>
          <View style={styles.cameraContainer}>
            {this.cleanValueButton()}            
          </View>
          <View style={styles.cameraContainer}>
            {this.navigateButton()}            
          </View>
      </ScrollView>
      </View>
    )
  }

  renderDefaultCard() {
    const {
      label,
      placeholder,
      touchable,
      locked
    } = this.props

    const { value, readOnly, children, valid } = this.state

    let customStyles = StyleSheet.create({
      card:{
          padding:0,
          marginTop:0,
          marginBottom:0,
          marginLeft:0,
          marginRight:0,
          borderBottomColor:"#DAE8F2",
          borderBottomWidth:1,
          width : this.state.screenWidth
      },
      nbCard: {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          padding:0,
          marginTop:0,
          marginBottom:0,
          marginLeft:0,
          marginRight:0,
          width : this.state.screenWidth
      },
      required:{
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        borderLeftColor:'#ff0000',
        borderColor:'#ff0000',
        borderLeftWidth:4,
        width : this.state.screenWidth
      }
    })

    return (
      <Card
        transparent
        style={valid ? customStyles.card : customStyles.required}>
        <CardItem
          style={styles.cardItem}>
          <Body style={styles.cardBody}>
            {locked && <Item stackedLabel style={styles.input}>
              <Label style={styles.label}>{label}</Label>
              <Input
                disabled
                style={customStyles.card}
                placeholderTextColor='#B8C4CC'
                value={value === ''
                  ? placeholder
                  : value} />
            </Item>
            }
            {!locked && React.cloneElement(children, { onChange: this.props.type === 'PICTURE' ? this.onChangePictureCase : this.onChange })}
            {this.getIcon()}
          </Body>
        </CardItem>
      </Card>
    )
  }
  renderReadonlyCard() {
    const {type} = this.props
    if((type || "").toLocaleLowerCase() === "location"){
      return this.renderReadonlyCard2()
    }

    const {
      label,
      placeholder,
      touchable,
      unit,
      locked
    } = this.props


    const { value, children, valid, visible } = this.state

    let customStyles = StyleSheet.create({
      roCard:{
        flex: 1,
        width: this.state.screenWidth,
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        elevation:0 
      }
    })

    const _card = (<Card
      style={customStyles.roCard}>
      <CardItem
        style={[styles.cardItem, { backgroundColor: COLORS.GREY4 }]}>
        <Body style={styles.cardBody}>
          {React.cloneElement(children, { readonly: true })}
        </Body>
        <Text style={styles.roInlineUnitText}>{unit}</Text>
        {this.getIcon()}
      </CardItem>
    </Card>)


    if (!visible) { return null; }
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={this.props.rootStore.loginStore.ANIMATION_DURATION}
        style={{
          flex: 1,
          backgroundColor: COLORS.GREY3,
          height: '100%'
        }}
        useNativeDriver>
        {_card}
      </Animatable.View>)



  }

  renderReadonlyCard2() {
    const {
      label,
      placeholder,
      touchable,
      unit,
      locked
    } = this.props


    const { value, children, valid, visible } = this.state

    let customStyles = StyleSheet.create({
      roCard:{
        flex: 1,
        width: this.state.screenWidth,
        padding:0,
        marginTop:0,
        marginBottom:0,
        marginLeft:0,
        marginRight:0,
        borderBottomColor:"#DAE8F2",
        borderBottomWidth:1,
        elevation:0 
      }
    })

    const _card = (<Card
      style={customStyles.roCard}>
      <CardItem
        style={[styles.cardItem, { backgroundColor: COLORS.GREY4 }]}>
        <Body style={styles.cardBody}>
          {React.cloneElement(children, { readonly: true })}
        </Body>
        <Text style={styles.roInlineUnitText}>{unit}</Text>
        {this.getIcon()}
      </CardItem>
    </Card>)


    if (!visible) { return null; }

     return (
      <View style={customStyles.roCard}>
        <ScrollView
          horizontal
          keyboardShouldPersistTaps={'handled'}
          showsHorizontalScrollIndicator={false}
          ref='_scrollView'>
          <TouchableOpacity disabled={true} style={{marginVertical:0}}
          testID={this.props.testID} accessibilityLabel={this.props.accessibilityLabel}>
            {_card}
          </TouchableOpacity>         
          <View style={styles.cameraContainer}>
            {this.navigateButton()}            
          </View>
      </ScrollView>
      </View>
    )
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={this.props.rootStore.loginStore.ANIMATION_DURATION}
        style={{
          flex: 1,
          backgroundColor: COLORS.GREY3,
          height: '100%'
        }}
        useNativeDriver>
        {_card}
      </Animatable.View>)



  }

  _useLibraryHandler = async () => {
    this.setState({
      imageBrowserOpen: true
    })
  }

  _useCameraHandler = async () => {
    this.setState({
      isCameraOpen: true
    }
    )
  }

  cameraCallback = async (callback: any) => {
    this.setState({
      isCameraOpen: false
    }, () => {

      this._p(callback)
    });
  }
  

  barcodeCallback = async (data : any) => {
    const { validationStore, loginStore } = this.props.rootStore
    let validationError = validationStore.validateInput(data, this.props.type)
    const { BARCODE_FORMAT_ERROR } = loginStore.getLocalizedDialogs()

    if (!validationError) {
      this.setState({
        isBarcodeOpen: false,
        value: data
      }, ()=>{
        try {
          const isNumeric = this.props.rootStore.loginStore.isTypeBarcodeNumeric(this.props.type)
          if (isNumeric) {
            if (_.isNumber(data)) {
  
              this.props.onSubmit(_.toNumber(data))
            } else {
              this.props.onSubmit(data)
            }
  
          } else {
  
            this.props.onSubmit(data)
          }
        } catch (error) {
          //pass   
          console.log("Error here----", error);
        }
        this.onBack()
      });
    }
    else {
      if (Platform.OS==='ios'){
        loginStore.displayError(BARCODE_FORMAT_ERROR.MESSAGE)
        } else {
          Alert.alert(BARCODE_FORMAT_ERROR.MESSAGE)
        }
    }  
  }

  imageBrowserCallback = async (callback : any) => {
    
    this.setState({
      imageBrowserOpen: false
    },()=>{
      this._p(callback)
    });
    
  }

  async deleteRemoteFile(file) {
    const { loginStore } = this.props.rootStore
    if (file.IsDocument == true && file.IsAttachment == false) {
      try {
        await loginStore.deleteRecord("ContentDocument", file.Id, {}, false)
      }
      catch (e) {

      }
    }
    else {
      try {
        await loginStore.deleteRecord("Attachment", file.Id, {}, false)
      }
      catch (e) {

      }
    }
  }

  async deleteAllMediaObjects(files) {
    const { loginStore } = this.props.rootStore

    let removeIds = []

    for (let file of files) {
      if (!loginStore.isNotMyOwnerId(file)) {
        if (!file.hasOwnProperty("IsLocal") || file.IsLocal == false) {
          await this.deleteRemoteFile(file)
        }
        removeIds.push(file.Id)
      }
    }

    if (!_.isEmpty(removeIds)) {
      loginStore._removeFromMediaLibrary(removeIds)
    }
  }

  _p(callback : any){
    const { type } = this.props
    const { loginStore } = this.props.rootStore

    // All the files related to the WOLID (to overwrite)
    let currentFiles
    if (type === 'PICTURE') {
      currentFiles = loginStore.mediaLibrary.filter(file => file.Linked.includes(this.props.id))
    }

    callback.then((photos : any) => {
      if (!_.isEmpty(photos)) { 
        loginStore.uploadMediaLibraryAsync(photos, "Work_Order_Line_Item_Detail__c", this.props.id)
        this.setState(prevState => ({
          //@ts-ignore
          attachments: prevState.attachments + photos.length
        }))

        if (!_.isEmpty(currentFiles)) {
          this.deleteAllMediaObjects(currentFiles)
        }
        
        loginStore.displayAttachmentUploadSuccess()
      }
    }).catch((e : any) => console.log(e))
  }

  render() {
    const { settingsStore } = this.props.rootStore
    const { attachments } = settingsStore.rules
    const { lastUpdate } = this.props.rootStore.dataStore
    const { value } = this.state

   

    if(this.state.isBarcodeOpen){      
      return <CameraScreen value={value} callback={this.barcodeCallback} barcode from={"barcode"}/>
    }

    if(this.state.isCameraOpen){      
      return <CameraScreen callback={this.cameraCallback} from={"camera"}/>
    }
    if(this.state.imageBrowserOpen){
      return <ImageBrowserScreen max={attachments.maxPictures} callback={this.imageBrowserCallback} />      
    }

    
    if (!this.state.visible) {
      return this.renderReadonlyCard()
    }
    if (this.props.readOnly) {
      if (this.props.scrollable) {
        return this.renderScrollableReadonlyCard()
      }
      return this.renderReadonlyCard()
    }
    if (this.props.scrollable) {
      return this.renderScrollableTouchCard()
    }
    return this.props.touchable
      ? this.renderTouchableCard()
      : this.renderDefaultCard()

  }


 
}

//@ts-ignore
FCard.defaultProps = {
  onPress: () => { },
  validate: () => { },
  onChange: () => { },
  placeholder: "Default Placeholder",
  locked: false,
  required: false,
  showErrorMessage: false,
  errorMessage: "${Label.thisFieldIsRequired",
  showValidationErrors: false,
  name: "",
  label: "Default Label",
  type: "STRING",
  readOnlyFromConfig: false,
  touchable: true,
  filterLogic: "",
  formula: "",
  apiName: "",
  isEditing: false,

  valid: true,
  accessibilityLabel: "FCard",
  testID: "FCard iOS",
  barcodeEnabled: false,
  valueCoords: null
};

export default FCard;