import React, { Component } from 'react';
import { View, Text } from 'native-base'
import { styles } from './Styles'
import { FIcon } from '../../index'
import MultiSelectButton from '../FMultiSelectPickList/MultiSelectButton'
import { Icons } from '../../../types'

type Props = {
    text ? : string
}
type State = {
    text ? : string
}

class FInfoCard extends Component<Props,State> {
    static defaultProps: { text: string; };
    constructor(props:Props) {
        super(props);
        this.state = {
            text : props.text
        };
    }
    componentDidUpdate(){
        if (this.state.text!=this.props.text){
            this.setState({text:this.props.text})
        }
    }

    render() {
        const { text } = this.state
        return (
            <View style={styles.container}>
                <View style={styles.card}>
                    <View style={styles.iconContainer}>
                        <MultiSelectButton icon={Icons.WARNING}/>
                    </View>
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>{text}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

FInfoCard.defaultProps = {
    text : ""
}

export default FInfoCard;