import { StyleSheet } from 'react-native'
import { COLORS } from '../../constants'

export const styles = StyleSheet.create({
    container : {
        padding : 12
    },
    card : {
        shadowColor:COLORS.LIGHTPRIMARY,
        shadowRadius:2,
        shadowOpacity:1,
        shadowOffset:{width:3,height:3},
        borderRadius:25,
        paddingTop : 12,
        paddingBottom : 12,
        paddingLeft : 24,
        paddingRight : 24,
        flexDirection:"row",
        backgroundColor:"white"
    },
    iconContainer : {
        justifyContent:"center",
        alignItems:"center",
        flex:1
    },
    textContainer : {
        flex:5
    },
    text : {
        color:COLORS.DARKGRAY
    }
})