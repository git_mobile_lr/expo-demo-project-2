import { StyleSheet } from 'react-native'

import { IS_TABLET, COLORS } from '../../constants'
export const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection: "row",
        alignItems: "center"
    },
    checkBox: {
        borderWidth:0,
        alignSelf: "flex-start",
        backgroundColor: "transparent",
        paddingLeft:0,
        marginLeft: 3,
        marginTop: -5,

      },
      checkBoxContainer : {
        flexDirection: "row",
        marginBottom: 0
      },
      label:{
        fontFamily: 'Roboto',
        alignSelf: "flex-start",
        fontSize: 11,
        marginTop: 8,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 13,
        paddingRight: 15,
        marginLeft: 4
    },
      labelContainer:{
      },
      FInput:{
        height:10,
        width:IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent',
    },
    helpText: {
      marginLeft: 4,
      alignSelf: "flex-start",
      fontFamily: 'Roboto',
      color: COLORS.DARKGRAY,
      fontSize: 12,
      letterSpacing: 0,
      lineHeight: 16,
    }

})