import React, { Component } from 'react';

import { View, Text } from 'react-native'
import { Item } from 'native-base'
import { inject, observer } from "mobx-react/native"
import { styles } from './Styles'
import { CheckBox } from "react-native-elements"
import { COLORS } from '../../constants'
import _ from 'lodash'

type Props = {
    label?: string,
    helpText?: string,
    value?: any,
    readonly?: boolean
    isEditing?: boolean
    onChange?: (value: string) => void
    onSubmit?: (value: string) => void
}

type State = {
    label?: string
    value: boolean
    isEditing?: boolean
}

@inject('rootStore')
@observer
class FCheckBox extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            value: (props.value === "true" ||  props.value===true),
            label: props.label,
            isEditing: props.isEditing,
        };
    }

    componentDidUpdate() {
        if (this.props.isEditing != this.state.isEditing) {
            this.setState({ isEditing: this.props.isEditing })
            this.toggleCheckbox()
        }
    }

    toggleCheckbox = () => {
        this.setState({ value: !this.state.value }, this.onChange)
    }

    onChange() {
        const { value } = this.state
        //@ts-ignore Defined in default props
        this.props.onChange(value ? "true" : "false")
        //@ts-ignore Defined in default props
        this.props.onSubmit(value ? "true" : "false")
    }

    render() {
        const { loginStore } = this.props.rootStore
        const { label, value } = this.state
        const { helpText } = this.props

        return (
            <Item stackedLabel style={styles.FInput}>
                <View style={{ alignSelf: 'flex-start' }}>
                    <Text style={styles.label} numberOfLines={2} >{loginStore.getLabel(label)}</Text>
                    {!_.isEmpty(helpText) && <Text style={styles.helpText}>{helpText}</Text>}
                </View>
                <CheckBox
                    disabled={this.props.readonly}
                    containerStyle={styles.checkBox}
                    checkedColor={this.props.readonly ? COLORS.DARKGRAY : COLORS.PRIMARY}
                    checked={value}
                    onPress={this.toggleCheckbox}
                />
            </Item>            
        )
    }
}

//@ts-ignore
FCheckBox.defaultProps = {
    onChange: () => { },
    onSubmit: () => { },
    label: "Default",
    value: false,
    readonly: false
}

export default FCheckBox;