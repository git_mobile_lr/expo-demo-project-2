/**
 * @jest-environment jsdom
 */
import React from 'react';
import { mount } from 'enzyme';
import FCheckBox from '../FCheckBox';

// 
describe('FCheckBox',()=>{
  test('Set default values is no props',()=>{
    const wrapper = mount(
      <FCheckBox  />
    );
    //@ts-ignore
    expect(wrapper.state().value).toStrictEqual(false)
    //@ts-ignore
    expect(wrapper.state().label).toStrictEqual("Default")

  })
  test('Set correct values if there are props',()=>{
    const wrapper = mount(
      <FCheckBox  
        value={"true"}
        label={"TEST-LABEL"}
      />
    );
    //@ts-ignore
    expect(wrapper.state().value).toStrictEqual(true)
    //@ts-ignore
    expect(wrapper.state().label).toStrictEqual("TEST-LABEL")
  })
})