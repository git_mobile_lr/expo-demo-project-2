import { StyleSheet } from 'react-native'
import { IS_TABLET,DEVICE_HEIGHT } from '../../constants'

export const styles = StyleSheet.create({
    lottieScanner:{
        height:200,
        alignSelf: 'center'
    },
    container:{
        flex: 1,
        backgroundColor: '#E9EEF1',
        height: DEVICE_HEIGHT
      },
    scannerWindow:{
        flex: 1,
        alignContent: 'center'
      },
    lottieWindow:{
        flex: 1,
        alignContent: 'center'
      },
    textNoPermission:{
      color: '#7a92a5',
      fontWeight: '200',
      fontFamily: 'Roboto',
      fontSize: 10,
      backgroundColor: '#FFFFFF',
      letterSpacing: 0,
      alignSelf: 'flex-start'
    },
    buttonNoPermission:{
      marginLeft:24,
      marginRight:24,
      marginTop:12,
      marginBottom:12, 
      alignContent: 'center'
    }

})