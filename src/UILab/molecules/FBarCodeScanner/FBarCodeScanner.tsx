import React, { Component } from 'react';
import { styles } from './Styles'
import { View, Text, Button } from 'native-base'
import { inject, observer } from "mobx-react/native"
import { GRANTED } from '../../../stores/appSettings/permissionStore'
import * as Animatable from 'react-native-animatable'
import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Sentry from 'sentry-expo';
import Lottie from 'lottie-react-native'

type Props = {
  rootStore?: any
  onBarCodeScanned: (barcode: string) => void
}

type State = {
  scanned: boolean
  success: (barcode: string) => void
}

@inject('rootStore')
@observer
class FBarCodeScanner extends Component<Props, State> {
  static defaultProps: { onBarCodeScanned: (barcode: string) => void; };
  constructor(props: Props) {
    super(props);
    this.animation = null
    this.state = {
      scanned: false,
      success: (barcode) => { },
    };
  }

  animation: Lottie | null

  _renderNoPermission() {
    const { openSettings } = this.props.rootStore.permissionStore
    const { loginStore } = this.props.rootStore
    const PERMISSIONS = loginStore.getPermissions()
    const { CAMERA } = PERMISSIONS

    return (
      <View style={{
        padding: 25
      }}>
        <Text
          style={styles.textNoPermission}>
          {CAMERA.NO_PERMISSION}
        </Text>
        <View style={styles.buttonNoPermission}>
          <Button rounded info onPress={openSettings} style={{ alignSelf: "center" }}>
            <Text>
              {
                loginStore.getLabelorDefault("${Label.OpenSettings}", "Open Settings")
              }
            </Text>
          </Button>
        </View>

      </View>
    )
  }

  //@ts-ignore
  _handleBarCodeRead = ({ type, data }) => {
    try {
      this.setState({ scanned: true })
      data ? this.onSucess(data) : null
    } catch (error) {
      let _error = {
        type: 'error',
        message: `An error when scanneing barcode.`
      }
      Sentry.Native.withScope(function (scope) {
        scope.setLevel(Sentry.Native.Severity.Error);
        Sentry.Native.captureException(error);
      });
      // Sentry.Native.captureException(new Error(_error.message), {logger: 'BarcodeScannerScreen'})
      this.setState({ scanned: false })
    }
  }

  onSucess(barcode: string) {
    const { onBarCodeScanned } = this.props
    onBarCodeScanned(barcode)

  }

  _renderScannerWindow() {
    const { scanned } = this.state;
    return (<BarCodeScanner
      onBarCodeScanned={scanned ? () => { } : this._handleBarCodeRead}
      style={{
        height: 300,
        marginHorizontal: 10,
        marginVertical: 15
      }} />
    )
  }

  _renderLottieScanner() {
    const ani = require('../../../assets/lottie/barcode.json')
    return (
      <Lottie
        ref={animation => {
          this.animation = animation;
          this.animation ? this.animation.play() : null
        }}
        style={styles.lottieScanner}
        loop={true}
        source={ani} />
    )
  }


  render() {
    const { permissionStore } = this.props.rootStore
    const { camera, cameraRoll } = permissionStore

    if ((camera !== GRANTED || cameraRoll !== GRANTED)) {
      return this._renderNoPermission()
    } else {
      return (
        <Animatable.View
          animation="fadeInDown"
          duration={500}
          style={styles.container}
          useNativeDriver>
          <View
            style={styles.scannerWindow}>
            {this._renderScannerWindow()}
          </View>
          <View
            style={styles.lottieWindow}>
            {this._renderLottieScanner()}
          </View>
        </Animatable.View>
      )
    }
  }
}

FBarCodeScanner.defaultProps = {
  onBarCodeScanned: (barcode: string) => { }
}

export default FBarCodeScanner;