import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingTop: 100,
    alignItems: 'center',
    paddingVertical: 20
  },
  smallDeviceEmptyFont: {
    color: '#7a92a5',
    fontWeight: '200',
    fontSize: 23,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingHorizontal: 20,
    paddingVertical: 5
  },
  emptyFont: {
    color: '#7a92a5',
    fontWeight: '200',
    fontSize: 30,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingHorizontal: 20
  },
  icon: {
    marginBottom: 20,
    marginLeft: 20, 
  }
})