import React from 'react';
import { Image, View, Text, Alert } from 'react-native'
import { inject, observer } from "mobx-react/native"
import { styles } from './FNoResultStyles'
import _ from 'lodash'


@inject('rootStore')
@observer
class FNoResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      label: ''
    };
  }
  
  componentDidMount() {
    const { loginStore } = this.props.rootStore
    this.setState({label: loginStore.getEmptyResultLabel(this.props.labelProp)})
  }

  render() {
    const { loginStore } = this.props.rootStore
    const { isSmallDevice, isTablet } = loginStore

    return (
      <View 
        style={styles.container}
      >
        {isTablet ? this.props.defaultTabletIcon() : this.props.defaultIcon()}
        <Text
          style={isSmallDevice ? styles.smallDeviceEmptyFont : styles.emptyFont}
        >
          {this.state.label}
        </Text>
      </View>
    )
  }
}

FNoResult.defaultProps = {
  defaultTabletIcon: () => {
    return (
      <Image
        style={styles.icon}
        source={ require('../../../assets/images/screens/workorderdetails/swift-no-result.png')}
      />
    )
  },
  defaultIcon: () => {
    return (
      <Image
        style={styles.icon}
        source={ require('../../../assets/images/screens/workorderdetails/swift-no-result.png')}
      />
    )
  }
}

export default FNoResult;
