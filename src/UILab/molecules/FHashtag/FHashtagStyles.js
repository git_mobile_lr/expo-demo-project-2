import React from 'react';
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {

        flex: 1,
        marginVertical: 3,
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        flexDirection: 'row'

    },
    hash: {
        alignSelf: "center",
        color: "#fff",
        fontSize: 9,
        height: 12,
        paddingRight: 5,
        lineHeight: 12,
    },
    hashtag: {
        flexDirection: 'row',
        backgroundColor: '#48aaf8',
        borderRadius: 15,
        marginRight: 3,
        marginVertical: 4,
        padding: 7
    },
    text: {
        alignSelf: "center",
        color: "#fff",
        fontSize: 9,
        height: 12,
        includeFontPadding: false,
        lineHeight: 12,
    }
})