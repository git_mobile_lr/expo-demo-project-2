
import React, { Component } from 'react'
import { inject, observer } from "mobx-react/native"
import { styles } from './FHashtagStyles'
import { Text, View } from 'native-base'

import _ from 'lodash'
import {  FontAwesome5, MaterialCommunityIcons } from '@expo/vector-icons'

type Props = {
    item: any
}
type State = {
    item: any,
    hashtags: any,
    assetsAreLoaded: boolean
}


@inject('rootStore')
@observer
class FHashtag extends Component<Props, State> {

    constructor(props: Props) {
        super(props)
        this.state = {
            item: props.item,
            hashtags: [],
            assetsAreLoaded: false
        }

    }

    componentDidUpdate(prevProps: Props) {
        if (!_.isEqual(this.state.item, this.props.item)) {
            this.setState({ item: this.props.item }, async () => { this._loadAsync() })
        }
    }


    componentDidMount() {
        this._loadAsync()
    }

    async _loadAsync() {
        const { settingsStore } = this.props.rootStore
        const { showApplicableTypesAsHashtags } = settingsStore.rules.items;
        const { isTesterMode } = this.props.rootStore.loginStore
        if (showApplicableTypesAsHashtags || isTesterMode) {
            this.checkHashTags()
        }

    }

    async checkHashTags() {
        const { loginStore, settingsStore } = this.props.rootStore
        const WOI = loginStore.getPrefixedTableName("Work_Order_Item__c");
        const APT = loginStore.getPrefixedFieldName("Applicable_Product_Types__c");
        const AWOT = loginStore.getPrefixedFieldName("Applicable_Work_Order_Types__c");
        
        if (this.state.item && this.state.item.hasOwnProperty(WOI)) {

            const { item } = this.state
            let hashTags : any = [];

            const { useApplicableProductTypes, useApplicableWorkOrderTypes } = settingsStore.rules.items;

            if (useApplicableProductTypes) {
                try {

                    if (item[WOI].hasOwnProperty(APT)) {
                        let a = _.split((item[WOI][APT]), ";");
                        hashTags = a;
                    }
                } catch (error) {
                    //pass
                }
            }

            if (useApplicableWorkOrderTypes) {
                try {
                    if (item[WOI].hasOwnProperty(AWOT)) {

                        let b = _.split((item[WOI][AWOT]), ";");
                        
                        hashTags = _.concat(hashTags, b);
                    }
                } catch (error) {                    
                    //pass
                }
            }

            this.setState({
                hashTags: hashTags,
                assetsAreLoaded: true
            })
        }
    }

    _renderTag: any = (x: string = "") => {
        try {
            if (x === "") {
                return null
            }

            return (<View style={styles.hashtag}>
                <MaterialCommunityIcons
                    name={"tag"}
                    style={styles.hash} />
                <Text style={styles.text}>{x.toLowerCase()}
                </Text></View>)
        } catch (error) {
            //pass
        }
        return null
    }

    render() {
        
        if (this.state.assetsAreLoaded) {
            return (<View
                style={styles.container}>
                {this.state.hashTags.map(this._renderTag)}
            </View>)
        }

        return <View />

    }
}

FHashtag.defaultProps = {
    item: null

}

export default FHashtag;