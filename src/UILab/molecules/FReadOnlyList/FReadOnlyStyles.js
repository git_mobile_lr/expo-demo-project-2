import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    section : {
        marginBottom:24
    },
    h1 : {
        fontFamily: "Roboto",
        fontSize: 16,
        color: "#5384A6",
        letterSpacing: 0,
        lineHeight: 20
    },
    h2 : {
        marginLeft:24,
        fontFamily: "Roboto",
        fontSize: 14,
        color: "#0D3754",
        letterSpacing: 0,
        lineHeight: 24
    },
    h3 : {
        marginLeft:48,
        fontFamily: "Roboto",
        fontSize: 11,
        color: "#5384A6",
        letterSpacing: 0,
        lineHeight: 16
    }
})