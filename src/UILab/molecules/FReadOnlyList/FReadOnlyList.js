import React, { Component } from 'react';
import { View, Text } from 'native-base'
import { inject, observer } from "mobx-react/native"
import * as Sentry from 'sentry-expo';
import _ from 'lodash'
import { styles } from './FReadOnlyStyles'
import { DEFAULT_CONFIG } from './FReadOnlyConfig'
import { FlatList } from 'react-native'
import FRenderer from '../../../screens/widgets/FRenderer'

@inject('rootStore')
@observer
class FReadOnlyList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            config: { ...DEFAULT_CONFIG, ...props.config },
            data: props.data,
            workOrderLineItems: [],
            workOrderLineItemsDetails: [],
            installedProducts: [],
            IP: props.rootStore.loginStore.getPrefixedFieldName("Installed_Product__c"),
            WOLI: props.rootStore.loginStore.getPrefixedFieldName("Work_Order_Line_Item__c"),
            WOLID: props.rootStore.loginStore.getPrefixedFieldName("Work_Order_Line_Item_Detail__c")
        };
    }


    componentDidMount() {
        this.setState({
            workOrderLineItems: this.getWolis(),
            config: { ...this.state.config, ...this.props.config }
        }, () => {
            this.getInstalledProducts()
            this.setState({
                workOrderLineItemsDetails: this.getDetails()
            })
        })
    }


    componentDidUpdate(prevProps) {
        if (!_.isEqual(prevProps.data, this.props.data)) {
            this.setState({
                data: this.props.data,
                config: { ...this.state.config, ...this.props.config }
            },
                this.setState({
                    workOrderLineItems: this.getWolis()
                }, () => {
                    this.getInstalledProducts()
                    this.setState({
                        workOrderLineItemsDetails: this.getDetails()
                    })
                }))
        }
    }



    getWolis() {
        try {
            const { data } = this.props
            const { WOLI } = this.state
            return data.hasOwnProperty(WOLI) ? data[WOLI] : []
        } catch (err) {
            // Sentry.captureException(new Error("Not able to get WOLIS."), {
            // Sentry.Native.captureException(new Error("Not able to get WOLIS."), {
            //     logger: 'FReadOnlyList',
            //     context: 'getWolis'
            //   });
        }

    }

    getDetails() {
        const { workOrderLineItems } = this.state
        try {
            const { data, rootStore } = this.props
            const { loginStore } = rootStore
            const { WOLID, WOLI } = this.state
            let wolids = []
            wolids = data.hasOwnProperty(WOLID) ? data[WOLID] : []
            wolisIds = workOrderLineItems.map(woli => woli.Id)
            if (_.isEmpty(wolids)) {
                wolids = loginStore.data[WOLID].filter(wolid => wolisIds.includes(wolid[WOLI]))
            }
            return wolids
        } catch (err) {
            // Sentry.captureException(new Error("Not able to get WOLIDs."), {
            // Sentry.Native.captureException(new Error("Not able to get WOLIDs."), {
            //     logger: 'FReadOnlyList',
            //     context: 'getDetails'
            //   });
        }

    }

    getInstalledProducts() {
        const { workOrderLineItems, IP } = this.state
        const { loginStore, dataStore } = this.props.rootStore
        let ips = []
        ips = _.uniq(workOrderLineItems.map(woli => woli[IP])).filter(item => !_.isEmpty(item))
        ips = ips.map(ip => {
            let _ip = loginStore.getRecordById(IP, ip)
            return _.isEmpty(_ip)
                ? dataStore.getItemById(ip, "ONLINE_DATA").data
                : _ip
        })

        this.setState({ installedProducts: ips })
    }


    render() {
        const { IP, WOLI } = this.state

        const { workOrderLineItems, workOrderLineItemsDetails, config, installedProducts } = this.state

        return <View>
            {
                !_.isEmpty(installedProducts) &&
                config.hasOwnProperty("installedProduct") &&
                <FlatList
                    data={installedProducts}
                    keyboardShouldPersistTaps={'handled'}
                    keyExtractor={(item, i) => i}
                    renderItem={(ip) => {
                        return (
                            <View style={styles.section}>
                                <FRenderer data={ip.item} config={config.installedProduct} />
                                {
                                    !_.isEmpty(workOrderLineItems) &&
                                    config.hasOwnProperty("workOrderLineItem") &&
                                    <FlatList
                                        keyExtractor={(item, i) => i}
                                        keyboardShouldPersistTaps={'handled'}
                                        data={workOrderLineItems.filter(woli => !_.isEmpty(woli[IP])).filter(woli => woli[IP].includes(ip.item.Id))}
                                        renderItem={woli => {
                                            return (
                                                <View>
                                                    <FRenderer data={woli.item} config={config.workOrderLineItem} />
                                                    {
                                                        !_.isEmpty(workOrderLineItemsDetails) &&
                                                        config.hasOwnProperty("workOrderLineItemDetail") &&
                                                        <FlatList
                                                            keyboardShouldPersistTaps={'handled'}
                                                            keyExtractor={(item, i) => i}
                                                            data={workOrderLineItemsDetails.filter(wolid => wolid[WOLI].includes(woli.item.Id))}
                                                            renderItem={wolid => {
                                                                return (
                                                                    <View>
                                                                        <FRenderer data={wolid.item} config={config.workOrderLineItemDetail} />
                                                                    </View>
                                                                )
                                                            }}
                                                        />
                                                    }

                                                </View>
                                            )
                                        }}
                                    />
                                }

                            </View>
                        )

                    }}
                />
            }

        </View>
    }
}

FReadOnlyList.defaultProps = {
    config: {},
    data: {}
}

export default FReadOnlyList;