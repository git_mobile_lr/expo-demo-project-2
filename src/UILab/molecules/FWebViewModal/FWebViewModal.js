import React, { useState, useEffect, useRef } from 'react'
import { StyleSheet, ActivityIndicator } from 'react-native'
import { Container } from "native-base"
import { WebView } from 'react-native-webview'
import _ from 'lodash'

import FModal from '../../molecules/FModal/FModal'
import { GetSessionInfo } from '../../../api/fieldbuddy'

function FWebViewModal ({ drawerLink, title, handleBack, isModalVisible, currentWorkOrderId }) {
  const [isLoading, setIsLoading] = useState(true)
  const [instance_url, setInstance_url] = useState(null)
  const [access_token, setAccess_token] = useState(null)

  const getSessionInfo = async () => {
    const { instance_url, access_token } = await GetSessionInfo()
    setInstance_url(instance_url)
    setAccess_token(access_token)
  }

  useEffect(() => {
    if (drawerLink.url.isVisualForce) {
      getSessionInfo()
    }
  }, [])

  if (!drawerLink || !drawerLink.url || !drawerLink.url.endpoint) {
    handleBack()
  }

  let uri = drawerLink.url.endpoint
  

  if (!_.isEmpty(drawerLink.url.params)) {
    const queryString = Object.keys(drawerLink.url.params).map(key => {
      let value = drawerLink.url.params[key]
      
      if (currentWorkOrderId && (value === "${CURRENT_WORK_ORDER}")) {
        // replace GLOBAL VARIABLE / ALIAS
        value = value.replace("${CURRENT_WORK_ORDER}", currentWorkOrderId)
        
      }
      return `${key}=${value}`
    }).join('&')
    uri = `${uri}?${queryString}`
  }

  const _retURL = `${instance_url}/${uri}`
  let _assetURL = `${instance_url}/secur/frontdoor.jsp?sid=${access_token}&retURL=${_retURL}`

  const webViewRef = useRef()

  const _webView = (
    <WebView
      style={{ marginHorizontal: 20 }}
      ref={webViewRef}
      onLoadEnd={() => setIsLoading(false)}
      source={{ uri: drawerLink.url.isVisualForce ? _assetURL : uri }}
    />
  )

  /*
  1) instance_url is not yet loaded, while WebView tries to open a link
  therefore it opens a very weird _assetURL;
  2) then instance_url is loaded, and that's why eventually Salesforce url is loaded correctly.
  */
  if( drawerLink.url.isVisualForce && instance_url === null){
    return (
      <FModal
        isDrawerLink={true}
        label="label"
        onBack={handleBack}
        isModalVisible={isModalVisible}
        header={title}
      >
        <Container>
            <ActivityIndicator
              style={styles.indicator}
              color={'#00AAFF'}
              size="large"
            />
        </Container>
      </FModal>
    )
  }

  return (
    <FModal
      scrollable={false}
      isDrawerLink={true}
      label="label"
      onBack={handleBack}
      isModalVisible={isModalVisible}
      header={title}
    >
      <Container>
        {_webView}
        {isLoading && (
          <ActivityIndicator
            style={styles.indicator}
            color={'#00AAFF'}
            size="large"
          />)}
      </Container>
    </FModal>
  )
}

const styles = StyleSheet.create({
  indicator: {
    position: "absolute",
    left: 0, 
    right: 0, 
    top: 20 
  }
})

export default FWebViewModal