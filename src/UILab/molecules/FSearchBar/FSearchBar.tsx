import React, { Component } from 'react';
import { View, Header,Item as NBItem,Icon,Input as NBInput } from 'native-base'
import {styles} from './Styles'
import { inject, observer } from "mobx-react/native"
import FInlineButton from '../../atoms/FInlineButton/FInlineButton'
import FModal from '../FModal/FModal'
import FBarCodeScanner from '../../molecules/FBarCodeScanner/FBarCodeScanner'
import _ from 'lodash'

type Props = {
    onSubmitSearch : (value:string) => void
    searchButton : boolean
    clearButton : boolean
    barCodeButton : boolean
    rootStore : any
    
}

type State = {
    value : string
    isBarcodeModalVisible : boolean
}

@inject('rootStore')
@observer
export default class FSearchBar extends Component<Props,State> {

    static defaultProps: { onSubmitSearch: () => void; searchButton: boolean; clearButton: boolean; barCodeButton: boolean; };

    constructor(props:Props) {
        super(props);
        this.state = {
            value : "",
            isBarcodeModalVisible : false
        }
    } 

    onChange = (value:string) => {
        this.setState({value:value})  
    } 

    handleOnClear = () => {
        this.setState({value:''})
        this.props.onSubmitSearch('')
    }

    handleOnSearch = () => {
        this.props.onSubmitSearch(this.state.value)
    }

    barcodeIcon(){
        return (
            <Icon
                name={"barcode"}
                ios="ios-barcode"
                android="md-barcode"
                style={{
                color: '#00AAFF',
                fontSize: 26
                }}/>
        )
    }

    toggleBarcodeModal(){
        this.setState({isBarcodeModalVisible:!this.state.isBarcodeModalVisible})
    }

    onBarCodeScanned = (barcode:string) =>{
        this.setState({value:barcode})
        this.toggleBarcodeModal()
        this.props.onSubmitSearch(barcode)
    }

    barcodeButton(){
        const { loginStore } = this.props.rootStore
        const { isBarcodeModalVisible } = this.state
        return (
            <View style={{width:50}}>
                <FModal 
                isVisible={isBarcodeModalVisible}
                header={loginStore.getLabelorDefault("Barcode scanner","Barcode scanner")}
                triggerItem={this.barcodeIcon()}>
                    <FBarCodeScanner 
                        onBarCodeScanned={this.onBarCodeScanned}
                    />
                </FModal>
            </View>
        )
    }

    render() {
        const {searchButton,clearButton,barCodeButton} = this.props
        return (
            <Header
                hasTabs={false}
                searchBar
                transparent
                rounded
                style={styles.header}>
                <NBItem
                    style={styles.item}>
                    <Icon
                        name="search"
                        style={styles.icon}
                        ios="ios-search"
                        android="md-search" />
                    <NBInput
                        returnKeyType="search"
                        style={styles.input}
                        value={this.state.value}
                        placeholderTextColor="#c0c0c0"
                        onChangeText={this.onChange}
                        onSubmitEditing={this.handleOnSearch}
                    >
                    </NBInput>
                    {clearButton && !_.isEmpty(this.state.value) && <FInlineButton clear onPress={this.handleOnClear}/>}
                    {barCodeButton && _.isEmpty(this.state.value) && this.barcodeButton()}
                    <View />
                </NBItem>
            </Header>
        );
    }
}

FSearchBar.defaultProps = {
    onSubmitSearch: ()=>{},
    searchButton: true,
    clearButton: true,
    barCodeButton: false
}
