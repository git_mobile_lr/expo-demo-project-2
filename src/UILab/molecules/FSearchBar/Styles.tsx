import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
    header:{
        margin: 0,
        backgroundColor: '#E9EEF1',
        borderColor: '#F5F5F5',
        borderWidth: 2
    },
    input:{
        fontFamily: 'Ionicons',
        color: '#00AAFF',
        letterSpacing: 1.4
    },
    icon:{
        color: '#00AAFF',
        fontSize: 26
    },
    item:{
        flex:1,
        flexDirection:'row',
        justifyContent:"space-between",
        backgroundColor: '#FFF',
        color: '#00AAFF'
    }
})