import React, { Component } from "react";
import { Button, Text, View, Container, Header, Left, Body, Footer, Right, Title } from "native-base";
import Modal from "react-native-modal";
import { ScrollView as Content, Dimensions, ActivityIndicator, Platform, TouchableWithoutFeedback } from 'react-native'
import { TouchableOpacity, GestureHandlerRootView } from 'react-native-gesture-handler';
import { TouchableOpacity as TouchableOpacityAndroid } from 'react-native'
import { styles } from './FModalStyles'

import FActionButton from '../../atoms/FActionButton/FActionButton'
import { inject, observer } from "mobx-react/native"
import { FToast } from '../../../screens/widgets'
import FSaveButton from './../../atoms/FSaveButton/FSaveButton'
import FLoadingContent from '../../atoms/FLoadingContent/FLoadingContent'
import _ from 'lodash'
import Constants from 'expo-constants'
import { IS_TABLET, IS_IPHONE_12_ISH } from '../../constants';
import { MenuBack } from "../../../screens/components";
//@inject('loginStore')
@inject('rootStore')
@observer
export default class FModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSaving: false,
      isModalVisible: props.isModalVisible,
      validationError: props.validationError
    };
  }

  componentWillUnmount() {
    const { loginStore } = this.props.rootStore

    // if (loginStore.isModalVisible) {
    loginStore.setModalIsOpen(false, "fmodal cwu")
    // }

  }
  componentDidUpdate(prevProps) {
    const { loginStore } = this.props.rootStore

    // if (this.state.isModalVisible !== loginStore.isModalVisible) {
    //   loginStore.setModalIsOpen(this.state.isModalVisible, "b")
    // }

    if (!_.isEqual(prevProps.isModalVisible, this.props.isModalVisible)) {
      // LISTEN TO PROPS CHANGES
      this.setState({ isModalVisible: this.props.isModalVisible }, () => {
        loginStore.setModalIsOpen(this.state.isModalVisible, "fmodal cdu")
      })

    }
    else if (!_.isEqual(prevProps.validationError, this.props.validationError)) {
      this.setState({ validationError: this.props.validationError })
    }
  }
  toggleModal = () => {
    if (this.props.rootStore.loginStore.isSyncing) {
      return;
    }
    this.setState({ isModalVisible: !this.state.isModalVisible, isSaving: false })
  };
  handleBack = async () => {
    if (this.props.warningOnBack || this.props.hasChanged()) {
      const { loginStore } = this.props.rootStore
      loginStore.FAlert({
        dialogs: "LOST_CHANGES_WARNING",
        onCancel: () => { },
        onConfirm: () => {
          this.props.onBack()
          this.toggleModal()
        }
      })
    } else {
      if (!this.props.disabled) {
        this.props.onBack()
      }

      this.toggleModal()
    }
  }
  defaultTriggerButton() {
    const { value, label, disabled, buttonStyle, displayTextArea, type } = this.props
    if (displayTextArea || type.toUpperCase() === "TEXTAREA") {
      return (
        <TouchableOpacity onPress={this.toggleModal}>
          <Text numberOfLines={10}
            ellipsizeMode="tail"
            style={((label === value) || (value === '')) ? styles.placeholder : styles.text}>
            {value === "" ? label : value}
          </Text>
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity onPress={this.toggleModal}>
        <View style={styles.placeholderContainer}>
          <Text numberOfLines={1} style={((label === value) || (value === '')) ? styles.placeholder : styles.text}>
            {value === "" ? label : value}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
  triggerItem() {
    const { triggerItem, triggerItemStyle, disabled, label, displayTextArea } = this.props

    if (disabled) {
      const { value, label, disabled, buttonStyle } = this.props
      return (
        <TouchableWithoutFeedback onPress={() => this.toggleModal()}>
          <Text numberOfLines={50} ellipsizeMode='tail' style={((label === value) || (value === '')) ? styles.placeholder : styles.text}>
            {value === "" ? label : value}
          </Text>
        </TouchableWithoutFeedback>
      )
    }
    return _.isEmpty(triggerItem)
      ? this.defaultTriggerButton()
      : <TouchableOpacity 
          onPress={this.toggleModal}
          onLongPress={this.props.triggerItemLongPress} 
          accessibilityLabel="Arrow Forward Modal" 
          testID="Arrow Forward Modal iOS" 
          style={triggerItemStyle ? { ...triggerItemStyle} : { width: "100%", height: "100%", justifyContent: "center" }}>
        {
          React.isValidElement(triggerItem) && triggerItem
        }
      </TouchableOpacity>
  }
  handleSave = () => {
    this.props.rootStore.loginStore.startDataUpdate()
    this.setState({ isSaving: true })
    setTimeout(() => {
      this.props.onSave()

      if (!this.props.rootStore.loginStore.isProccesingChanges) {
        this.toggleModal()
      }
    }, 100)
  }

  _renderLoading() {
    let dialogs = this.props.rootStore.loginStore.getLocalizedDialogs()
    const { LOADING_SCREEN } = dialogs
    const { isSavingBasket, isProccesingChanges, isSyncing } = this.props.rootStore.loginStore
    if (isProccesingChanges || isSavingBasket || this.state.isSaving) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: '#fff',
            paddingTop: 50
          }}
        >
          <FLoadingContent img={require('../../../assets/images/screens/checklist/checklist-masked-circle.png')} title={LOADING_SCREEN.MESSAGE} />
        </View>
      )
    }
    return null
  }

  _getActionbutton() {
    const { onSave, inputInline, disabled, rootStore } = this.props
    const height = Dimensions.get('window').height
    const marginTop = rootStore.loginStore.isTablet ? height / 2 - 200 : height / 2 - 56

    if (inputInline && !disabled) {
      if (Platform.OS === 'ios') {
        return (

          <View style={{ flex: 1, position: 'absolute', top: marginTop, right: rootStore.loginStore.isTablet ? 90 : 40 }}>
            <FActionButton save onPress={() => { onSave() ? this.toggleModal() : null }} />
          </View>
        )
      }
      return (
        <FActionButton save offsetY={Platform.OS === 'ios' ? 500 : 100} onPress={() => { onSave() ? this.toggleModal() : null }} />
      )
    }
  }

  renderModal = () => {
    const { header, onSave, initAt, inputInline, saveButton, scrollable, addButton, disabled, customButton } = this.props
    const { loginStore } = this.props.rootStore
    const { isProccesingChanges, isSyncing } = loginStore
    const { isSaving, validationError } = this.state
    let dialogs = this.props.rootStore.loginStore.getLocalizedDialogs()
    const { ERROR_500_ABOUT_TO_ASYNC_SYNC } = dialogs
    const isPortrait = () => {
      const dim = Dimensions.get('screen');
      return dim.height >= dim.width;
    };

    let modelContainerScreen = this.props.isDrawerLink ? {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,
      paddingTop: IS_IPHONE_12_ISH() ?
        (isPortrait() ? Constants.statusBarHeight / 3 : undefined) :
        undefined,
      margin: 0
    } : {}



    let _touch = (<TouchableOpacity onPress={this.handleBack} style={styles.handleBackStyle} accessibilityLabel="Back Button" testID="Back Button iOS">
      <MenuBack />
    </TouchableOpacity>)
    //Backup gesture for Android
    // <TouchableOpacityAndroid onPress={this.handleBack} style={styles.handleBackStyle} accessibilityLabel="Back Button" testID="Back Button iOS">
    //   <MenuBack />
    // </TouchableOpacityAndroid>

    return (<Modal
      style={styles.modalScreen}
      supportedOrientations={['portrait', 'landscape']}
      coverScreen={true}
      backdropOpacity={1}
      backdropColor={'white'}
      hideModalContentWhileAnimating={true}
      isVisible={this.state.isModalVisible}
    >
      <GestureHandlerRootView style={{ flex: 1 }}>
        <View style={{
          position: 'absolute', top: 87, opacity: 0.89, width: '100%', backgroundColor: 'transparent',
          zIndex: 1000000000
        }}>
          <FToast />
          {isSyncing && <ActivityIndicator
            style={{
              marginVertical: 45,
              alignItems: "center",
              justifyContent: "center",
              zIndex: 1000
            }}
            color="#1EB1F8"
            size="large"
            animating={true} />}
        </View>
        <Container style={modelContainerScreen}>
          {!isSaving && !isProccesingChanges &&
            (<Header style={styles.header}>
              <Left>
                {_touch}
              </Left>
              <Body>
                <Title style={styles.headerTitleStyle}>{!isSaving ? header : ERROR_500_ABOUT_TO_ASYNC_SYNC.TITLE}</Title>
              </Body>
              <Right>
                {
                  saveButton && (<FSaveButton onValidateRules={this.props.onValidateRules} onPress={saveButton ? (() => { onSave() ? this.toggleModal() : null }) : this.handleSave} busy={isProccesingChanges} disabled={validationError} />)
                }
                {
                  addButton && (<FSaveButton onValidateRules={this.props.onValidateRules} onPress={this.handleSave} buttonText={loginStore.getLabelorDefault("${Label.Add}", "Add")} busy={isProccesingChanges} />)
                }
                {
                  customButton && (customButton)
                }
                {
                  !saveButton && (this.right())
                }
              </Right>
            </Header>)}

          {(isSaving || isProccesingChanges) && (
            <Header style={styles.header}>
              <Body>
                <Title style={styles.headerTitleStyle}>{ERROR_500_ABOUT_TO_ASYNC_SYNC.TITLE}</Title>
              </Body>
            </Header>
          )}


          {
            (!isProccesingChanges && !isSaving && !scrollable) && React.cloneElement(this.props.children, { toggleModal: this.toggleModal })
          }
          {
            (!isProccesingChanges && !isSaving && scrollable) &&
            <Content keyboardShouldPersistTaps={'handled'}>
              {React.cloneElement(this.props.children, { toggleModal: this.toggleModal })}
            </Content>
          }
          {
            this._renderLoading()
          }
          {!saveButton ? this._getActionbutton() : null}
        </Container>
      </GestureHandlerRootView>
    </Modal>)
  }
  right() {
    const { type, displayTextArea } = this.props
    if (displayTextArea || type.toUpperCase() === "STRING" || type.toUpperCase() === "TEXTAREA") {
      const _titleStyle = IS_TABLET ? styles.maxLengthTablet : styles.maxLength;
      return (<Right>
        <Title style={_titleStyle}>
          <Text adjustsFontSizeToFit={true} style={_titleStyle}>{`Max. ${this.props.maxLength}`}</Text>
        </Title>
      </Right>)
    }
    return (<View />)
  }
  render() {
    const {hideTriggerItem,  triggerItem, disabled, displayTextArea } = this.props

    const _style = this.props.isDrawerLink ?
      { width: Dimensions.get('window').width, height: Dimensions.get('window').height, flex: 1, justifyContent: "center" }
      :
      { width: Dimensions.get('window').width, height: "100%", flex: 1, justifyContent: "center" }



    return (
      <View style={_style}>
        { !hideTriggerItem && this.triggerItem()}
        {
          this.renderModal()
        }
      </View>
    );
  }
}
FModal.defaultProps = {
  triggerItemLongPress: () => { },
  onValidateRules: () => {

    return {
      success: true
    };
  },
  onSave: () => { },
  onBack: () => { },
  hasChanged: () => { },
  type: "Reference",
  searchBar: true,
  header: "Select an option",
  value: "Please select an option",
  isModalVisible: false,
  label: "defaultLabel",
  inputInline: false,
  buttonStyle: {},
  triggerItemStyle: {},
  triggerItem: {},
  hideTriggerItem: false,
  saveButton: false,
  scrollable: true,
  addButton: false,
  customButton: true,
  warningOnBack: false,
  disabled: false,
  displayTextArea: false,
  maxLength: 0,
  isDrawerLink: false
};