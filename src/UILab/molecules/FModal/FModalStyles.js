import { StyleSheet, Platform, Dimensions } from 'react-native'
import Constants from 'expo-constants'
import { COLORS, IS_IPHONE_12_ISH } from '../../constants'

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";


export const styles = StyleSheet.create({
    FModalIcon: {
        paddingRight: 0,
        marginRight: 0,
        marginLeft: 0,
        color: '#00AAFF'
    },
    modalScreen: {        
        paddingTop: IS_IPHONE_12_ISH() ? Constants.statusBarHeight /3 : undefined,
        margin: 0
    },
    modalScreen2: {
        flex: 1,
        width: wp("100%"),
        height: hp("100%"),        
        paddingTop: IS_IPHONE_12_ISH() ? Constants.statusBarHeight /3 : undefined,
        margin: 0
    },
    button: {

    },
    saveButton: {
        color: COLORS.PRIMARY
    },
    FModal2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 50,
        borderColor: 'transparent'
    },
    FModal: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 10,
        width: '100%',
        borderColor: 'transparent'
    },
    placeholder: {


        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#B8C4CC',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0,
        marginRight: 60
    },
    header: {
        backgroundColor: "white"
    },
    headerTitleStyle: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0
    },
    backIcon: {
        paddingLeft: 12,
        fontSize: 14,
        fontWeight: "100",
        fontFamily: 'Roboto',
        color: COLORS.PRIMARY,
        marginTop: 17.5
    },
    text: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0,
        marginRight: 60
    },
    placeholderContainer: {
        height: 24
    },

    handleBackStyle: {        
        width: Dimensions.get('screen').width/3,
        height: '100%'
    },
    maxLength: {
        fontFamily: "Roboto",
        fontSize: 13,
        color: '#0D3754',
        justifyContent: "center",
        marginTop: Platform.OS === "ios"
            ? 5
            : 0,
        alignItems: Platform.OS === "ios"
            ? "center"
            : "flex-start"
    },
    maxLengthTablet: {
        fontFamily: "Roboto",
        fontSize: 25,
        color: '#0D3754',
        justifyContent: "flex-start",
        marginTop: Platform.OS === "ios"
            ? 5
            : 0,
        alignItems: Platform.OS === "ios"
            ? "center"
            : "flex-start"
    }
})
