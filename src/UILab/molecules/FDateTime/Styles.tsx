import { StyleSheet } from 'react-native'
import { IS_TABLET, COLORS, FONTS } from '../../constants'

export const styles = StyleSheet.create({
  inputItem: {
    width: IS_TABLET ? '96%' : '92%',
    borderColor: 'transparent',
    alignItems: 'flex-start',
    borderWidth: 0,
    marginBottom: 0,
    paddingBottom: 0,

  },
  label: {
    fontFamily: FONTS.PRIMARY,
    fontSize: 11,
    color: COLORS.SECONDARY,
    letterSpacing: 0,
    lineHeight: 16
  },
  placeholder: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 20,
    width: '100%',
    borderColor: 'transparent',
    borderWidth: 0
  },
  dateTime: {
    flexDirection: 'row',
    borderColor: 'transparent'
  },
  datePicker: {


    flex: 1
  },
  timePicker: {


    flex: 2
  }
})

export const customStyles = StyleSheet.create({
  dateText: {
    fontFamily: FONTS.PRIMARY,
    color: COLORS.FONTS_PRIMARY,
    fontSize: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dateTextWhenOpen: {
    fontFamily: FONTS.PRIMARY,
    color: COLORS.FONTS_PRIMARY,
    fontSize: 16,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 9
  },
  placeholderText: {
    fontFamily: FONTS.PRIMARY,
    color: COLORS.GRAY,
    fontSize: 16
  },
  datePickerCon: {
    backgroundColor: "#00AAFF"
  },
  datePicker: {
    backgroundColor: "#00AAFF"
  },
  btnCancel: {
    backgroundColor: "#00AAFF"
  },
  btnTextCancel: {
    fontSize: 16,
    color: "#FFF"
  },
  btnConfirm: {
    backgroundColor: "#00AAFF"
  },
  btnTextConfirm: {
    fontSize: 16,
    color: "#FFF"
  },
  dateInput: {
    alignItems: 'flex-start',
    borderWidth: 0
  }
})