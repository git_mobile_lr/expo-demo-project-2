/**
 * @jest-environment jsdom
 */
import React from 'react';
import { mount } from 'enzyme';

import { DateMode, Locale } from '../../../../types'
import FDateTime from '../FDateTime';
// 
describe('FDateTime', () => {
  test('Set default values is no props', () => {
    const wrapper = mount(
      <FDateTime />
    );
    //@ts-ignore
    expect(wrapper.state().value).toStrictEqual(undefined)
    //@ts-ignore
    expect(wrapper.state().mode).toStrictEqual(DateMode.DATETIME)
    //@ts-ignore
    expect(wrapper.state().placeholder).toStrictEqual("Date")
    //@ts-ignore
    expect(wrapper.state().locale).toStrictEqual(Locale.enUS)
    //@ts-ignore
    expect(wrapper.state().confirmButtonText).toStrictEqual("Confirm")
    //@ts-ignore
    expect(wrapper.state().cancelButtonText).toStrictEqual("Cancel")
    //@ts-ignore
    expect(wrapper.state().utcOffset).toStrictEqual(0)
  })
  test('Set correct values if there are props', () => {
    const wrapper = mount(
      <FDateTime
        value={"2020-04-22T00:00:00Z"}
        mode={DateMode.DATE}
      />
    );
    //@ts-ignore
    expect(wrapper.state().value).toStrictEqual("2020-04-22T00:00:00Z")
    //@ts-ignore
    expect(wrapper.state().mode).toStrictEqual(DateMode.DATE)
  })
})



