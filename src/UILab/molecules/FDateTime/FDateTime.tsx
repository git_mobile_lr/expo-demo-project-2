import React, { Component } from 'react';
import { Item, Label, Text, } from 'native-base'
import { styles, customStyles } from './Styles'
import DatePicker from 'react-native-datepicker'
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { FontAwesome } from "@expo/vector-icons";
import { DateMode, Locale } from '../../../types'
import { Alert, Platform, TouchableOpacity } from 'react-native';


type Props = {
  isEditing?: boolean
  value?: string | undefined
  mode?: DateMode.DATE | DateMode.DATETIME
  placeholder?: string
  locale?: Locale.enNL | Locale.enUS | Locale.nlNL
  confirmButtonText?: string
  cancelButtonText?: string
  onChange?: (date: string) => void
  onSubmit?: (date: string) => void
  utcOffset?: number
  readonly?: boolean
  label: string,
  iconComponent?: () => object
  showIcon?: boolean,
  format?: string,
}

type State = {
  isEditing?: boolean
  value?: string | undefined
  mode?: DateMode.DATE | DateMode.DATETIME
  placeholder?: string
  locale?: Locale.enNL | Locale.enUS | Locale.nlNL
  confirmButtonText?: string
  cancelButtonText?: string
  utcOffset?: number,
  showDate: boolean,
  showTime: boolean
}

class FDateTime extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      value: props.value,
      isEditing: props.isEditing,
      mode: props.mode,
      placeholder: props.placeholder,
      locale: props.locale,
      confirmButtonText: props.confirmButtonText,
      cancelButtonText: props.cancelButtonText,
      utcOffset: props.utcOffset,
      showDate: false,
      showTime: false
    };
  }

  componentDidUpdate() {
    if (this.props.isEditing != this.state.isEditing) {
      //@ts-ignore -> Should component FDate extend DatePicker ??
      // this.setState({ isEditing: this.props.isEditing }, this.refs.datepicker.onPressDate)
      this.setState({ isEditing: this.props.isEditing, showDate: true })
    }
  }

  handleDateChange = (date: any) => {
    if (date.type === "dismissed") {
      if (this.state.showDate) {
        this.setState({

          showDate: false,
          showTime: false
        })
      } else if (this.state.showTime) {
        const { oldValue } = this.state
        this.setState({
          value: oldValue,
          showDate: false,
          showTime: false
        })
      }
    } else {

      // this.setState({ value: date, showDate: false, })
      if (this.state.showDate) {
        const { value } = this.state

        this.setState({
          value: new Date(date.nativeEvent.timestamp),
          oldValue: value,
          showDate: false,
          showTime: true
        })
      } else if (this.state.showTime) {

        this.setState({
          value: new Date(date.nativeEvent.timestamp),

          showTime: false
        }, () => {
          this.updateParent(date);
        })
      }
    }
  }

  private updateParent(date: any) {
    const { utcOffset } = this.props;

    let formattedDate = moment(date.nativeEvent.timestamp).utcOffset(utcOffset ? utcOffset : 0, true).format();


    //@ts-ignore Defined in default Props

    this.props.onChange(formattedDate);

    // //@ts-ignore Defined in default Props
    this.props.onSubmit(formattedDate);
  }

  formatDate(date: any) {
    if (date == "") return undefined
    const { utcOffset } = this.props
    let timeZone = utcOffset ? utcOffset / 60000 : 0
    let f_date = moment(date).utcOffset(timeZone).format()
    return f_date
  }

  isValidDate = d => new Date(d)
    .toString() !== 'Invalid Date';
    
  getValueString() {
    const { value } = this.state

    try {
      if (this.isValidDate(value)) {
        return moment(value).toDate()
      }
      return ""
    }
    catch (exc) {
      //pass
      return ""
    }
  }

  getValueAndroid() {
    const { value } = this.state

    try {
      if (this.isValidDate(value)) {
        return moment(value).toDate()
      }
      return new Date()
    }
    catch (exc) {
      //pass
      return new Date()
    }
  }

  getFormattedValueAndroid() {
		try {
			const { value } = this.state
			if(value) {
				return moment(value).format("YYYY-MM-DD HH:mm").toString()
			} else {
				return moment().format("YYYY-MM-DD HH:mm").toString()
			}
		} catch(e) {
			return moment().format("YYYY-MM-DD HH:mm").toString()
		}
	}
 
  getValue() {
    if (Platform.OS === 'android') {
      return this.getValueAndroid()
    }

		try {
			const { value } = this.state
			if (value) {
				try {
					if (!this.isValidDate(value)) {
						return new Date()
					}
				} catch (error) {
					return new Date()
					//pass
				}
				return value
			} else {
				return new Date()
			}
		}
		catch (exc) {
			return new Date();
		}
	}

  getFormattedValue() {
    if (Platform.OS === 'android') {
      return this.getFormattedValueAndroid()
    }

		const { value } = this.state
		let _format = "YYYY-MM-DD HH:mm"
		try {
      console.warn("v " + value)
			if (value === null) {
				return ""
			}

			if (!this.isValidDate(value)) {
				return ""

			}

			return moment(new Date(this.getValue())).format(_format).toString()
		} catch (e) {
			//pass
		}

		return ""
	}


  /*
  <DatePicker
              disabled={this.props.readonly}
              ref='datepicker'
              mode="date"
              locale={locale}
              date={value}
              format="YYYY-MM-DD"
              placeholder={label}
              customStyles={customStyles}
              style={styles.datePicker}

              androidMode="calendar"
              showIcon={false}
              is24Hour={true}
              onDateChange={this.handleDateChange}
              confirmBtnText={confirmButtonText}
              cancelBtnText={cancelButtonText}
            />
  */



  render() {
    const { value, mode, placeholder, locale, confirmButtonText, cancelButtonText } = this.state
    const { label, iconComponent, format, showIcon } = this.props
    return (
      <>
        <Item stackedLabel style={styles.inputItem}>
          <Label style={styles.label}>{label}</Label>
          <Item style={styles.dateTime}>
            {!(this.state.showDate || this.state.showTime) && (<DatePicker

              ref='datepicker'
              mode={mode}
              locale={locale}
              date={Platform.OS === 'ios' ? this.getFormattedValue() : this.getValueString()}
              format={format}
              placeholder={label}
              customStyles={customStyles}
              style={styles.datePicker}

              androidMode="spinner"
              showIcon={false}
              is24Hour={true}
            />)}


            {this.state.showDate && <DateTimePicker
              mode="date"
              androidDisplay="default"
              locale={locale}
              value={this.getValue()}
              format="YYYY-MM-DD"
 
              placeholder={label}
              customStyles={customStyles}
              style={styles.datePicker}

              onChange={this.handleDateChange}
              confirmBtnText={confirmButtonText}
              cancelBtnText={cancelButtonText}
            />}

            {this.state.showTime && <DateTimePicker
              mode="time"
              locale={locale}
              value={this.getValue()}
              format="hh:mm"
              
              placeholder={label}
              customStyles={customStyles}

              style={styles.timePicker}

              showIcon={true}
              is24Hour={true}
              onChange={this.handleDateChange}
              confirmBtnText={confirmButtonText}
              cancelBtnText={cancelButtonText}
            />}
            {(this.state.showTime || this.state.showDate) && <Text 
					    style={customStyles.dateTextWhenOpen}>{this.getFormattedValue()}
				    </Text>}
          </Item>
        </Item>
      </>

    );
  }

}

//@ts-ignore
FDateTime.defaultProps = {
  onChange: () => { },
  onSubmit: () => { },
  isEditing: false,
  value: undefined,
  mode: DateMode.DATETIME,
  placeholder: "Date",
  locale: Locale.enUS,
  confirmButtonText: "Confirm",
  cancelButtonText: "Cancel",
  utcOffset: 0,
  readonly: false,
  format: "YYYY-MM-DD",
  showIcon: false
}

export default FDateTime;