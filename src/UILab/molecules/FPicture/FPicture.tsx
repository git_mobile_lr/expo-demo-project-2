import React, { Component } from "react"
import { View, Text } from "native-base"
import { styles } from "./Styles"
import {
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from "react-native"
import FIcon from "../../atoms/FIcon/FIcon"
import FModalInput from "../FModalInput/FModalInput"
import _ from "lodash"
import { COLORS, DEVICE_WIDTH } from "../../constants"
import { Icons, icon } from "../../../types"
import { FontAwesome } from "@expo/vector-icons"

import { upsertScreenModes } from "../../../types"
import * as Sentry from "sentry-expo"
import moment from "moment"
import ImageBrowserScreen from "../../../screens/ImageBrowserScreen"
import CameraScreen from "../../../screens/CameraScreen"

type Props = {
  id?: string
  name?: string
  comments?: string
  value?: Array<string> | string
  rootStore?: any
  modalHeaderText?: string
  modalPlaceholderText?: string
  modalSaveButtonText: string
  helpText?: string
  readonly?: boolean

  onChange: (state: any) => void
  onSubmit: (state: any) => void
}

type State = {
  id?: string
  name?: string
  comments?: string
  value?: Array<string> | null
  attachments?: number
  imageBrowserOpen: boolean
  isCameraOpen: boolean
  screenWidth: number
}

type Icon = {
  [value: string]: string
}

class FPicture extends Component<Props, State> {
  _isMounted = false
  static defaultProps: {
    onChange: (state: State) => void
    modalSaveButtonText: String
    name: string
    comments: String
    value?: Array<string> | null
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      id: props.id,
      name: props.name,
      comments: props.comments,
      value:
        typeof props.value == "string" ? props.value.split(";") : props.value,
      attachments: 0,
      imageBrowserOpen: false,
      isCameraOpen: false,
      screenWidth: Dimensions.get("window").width,
    }
  }

  _dimensionsListener: any

  async componentDidMount() {
    const { loginStore } = this.props.rootStore
    this._isMounted = true
    const amount = await loginStore.getMediaLengthById(this.props.id)
    this.setState({ attachments: amount })

    this._dimensionsListener = Dimensions.addEventListener("change", () => {
      let o = this.isPortrait() ? "portrait" : "landscape"
      // this.props.rootStore.loginStore.setOrientation(o);
      const width = Dimensions.get("window").width
      this.setState({
        screenWidth: width,
      })
    })
  }

  componentWillUnmount() {
    this._isMounted = false
    try {
      // Dimensions.removeEventListener('change')
      this._dimensionsListener()
    } catch (error) {}
  }

  isPortrait = () => {
    const dim = Dimensions.get("screen")
    return dim.height >= dim.width
  }

  commentButton() {
    return (
      <View style={{ padding: 24 }}>
        <FIcon icon={Icons.COMMENTS} color={"white"} />
      </View>
    )
  }

  cameraButton() {
    if (
      !this.props.rootStore.settingsStore.rules.attachments.allowPhotosUpload ||
      !this.props.rootStore.settingsStore.rules.items.checklistShowActions
        .attachments
    )
      return;
    const active = this.props.rootStore.dataStore.currentChecklistMode === upsertScreenModes.UPDATE

    return (
      <TouchableOpacity
        disabled={!active}
        onPress={() => this._useCameraHandler()}
      >
        <View style={active ? styles.pictureButton : styles.readOnlyPictureButton}>
          <FontAwesome name="camera" size={18} color="white" />
        </View>
      </TouchableOpacity>
    )
  }

  libraryButton() {
    if (
      !this.props.rootStore.settingsStore.rules.attachments.allowPhotosUpload ||
      !this.props.rootStore.settingsStore.rules.items.checklistShowActions.attachments
    )
      return;
    const active = this.props.rootStore.dataStore.currentChecklistMode === upsertScreenModes.UPDATE

    return (
      <TouchableOpacity
        disabled={!active}
        onPress={() => this._useLibraryHandler()}
      >
        <View style={active ? styles.pictureButton : styles.readOnlyPictureButton}>
          <FontAwesome name="picture-o" size={18} color="white" />
        </View>
      </TouchableOpacity>
    )
  }

  _getFileName(uri: any) {
    try {
      const _u =
        moment().format("YYYY-MM-DD HH:mm") +
        "." +
        uri
          .split("/")
          .pop()
          .split(".")
          .pop()

      return _u
    } catch (e) {
      return null
    }
  }

  async uploadAsync(result: any) {
    const { loginStore } = this.props.rootStore
    loginStore.setIsAddingAttachment = true
    const { comments } = this.state

    try {
      const _name = this._getFileName(result.uri)

      if (_name) {
        const _file = {
          PathOnClient: result.uri,
          Title: _name,
          VersionData: result.base64,
          FirstPublishLocationId: this.props.id,
        }

        //loginStore.setIsSyncing = true

        const _r = await loginStore.uploadAsync(_file)
        this.onBack()
        if (this._isMounted) {
          this.setState((prevState) => ({
            //@ts-ignore
            attachments: prevState.attachments + 1,
          }))
        }
        loginStore.displayAttachmentUploadSuccess()
      }
    } catch (e) {
      let _error = {
        type: "error",
        context: "uploadAsync",
        message: e,
      }

      // Sentry.captureException(new Error(_error.message), { logger: 'workOrderDetailsScreen' });

      Sentry.Native.withScope(function(scope) {
        scope.setLevel(Sentry.Native.Severity.Error)
        Sentry.Native.captureException(e)
      })

      // Sentry.Native.captureException(new Error(_error.message), { logger: 'workOrderDetailsScreen' });
    } finally {
      setTimeout(async () => {
        const { loginStore } = this.props.rootStore
        loginStore.setIsAddingAttachment = false
        loginStore.setHasMediaChanged = true
        loginStore.setIsSyncing = false
      }, 100)
    }
  }

  handleDataChange() {
    const { state } = this
    const { onChange, onSubmit } = this.props

    let newState = {}
    newState["value"] = state.value ? state.value.join(";") : ""
    newState = { ...state, ...newState }

    onSubmit(newState)
    //@ts-ignore
    onChange(newState.value)
  }

  /**
   * Saves the comment that comes from the modal to the state of the Multi-select Picklist
   */
  onSaveComment = (value: string) => {
    this.setState({ comments: value }, this.handleDataChange)
    // @ts-ignore
    this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
  }

  /**
   * Hides the blue button comment from the right
   */
  onBack = () => {
    // @ts-ignore
    this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
  }

  renderReadonly() {
    const { name, comments } = this.state
    const {
      modalHeaderText,
      modalPlaceholderText,
      modalSaveButtonText,
      helpText,
      readonly,
    } = this.props
    const MODAL_HEADER = modalHeaderText
    const PLACEHOLDER = modalPlaceholderText

    let customStyles = StyleSheet.create({
      picklistContainerReadonly: {
        width: this.state.screenWidth,
        paddingLeft: 0,
        paddingRight: 12,
        borderLeftColor: "#f0f0f0",
        borderLeftWidth: 4,
        justifyContent: "center",
      },
    })

    return (
      <View style={styles.containerReadonly}>
        <ScrollView
          ref="_scrollView"
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          <View style={customStyles.picklistContainerReadonly}>
            <View style={styles.card}>
              <View style={styles.labelContainer}>
                {!_.isEmpty(comments) ? (
                  <View style={styles.commentIndicatorContainer}>
                    <FIcon icon={Icons.COMMENTSDOTS} color="#c8c8c8" />
                  </View>
                ) : (
                  <View />
                )}
                <View style={styles.nameSection}>
                  <Text style={styles.label}>{name}</Text>
                  {!_.isEmpty(helpText) && (
                    <Text style={styles.helpText}>{helpText}</Text>
                  )}
                </View>
              </View>

              <View style={styles.buttonContainer}>
                <View style={styles.cameraContainer}>
                  {this.cameraButton()}
                </View>
                <View style={styles.cameraContainer}>
                  {this.libraryButton()}
                </View>
              </View>

            </View>
          </View>
          <View style={styles.readOnlyCommentsContainer}>
            <FModalInput
              value={comments}
              header={MODAL_HEADER}
              placeholder={PLACEHOLDER}
              saveButtonText={modalSaveButtonText}
              onBack={this.onBack}
              isReadOnly={readonly}
              triggerItem={this.commentButton()}
            />
          </View>
        </ScrollView>
      </View>
    )
  }

  _useLibraryHandler = async () => {
    this.setState({
      imageBrowserOpen: true,
    })
  }

  _useCameraHandler = async () => {
    this.setState({
      isCameraOpen: true,
    })
  }

  cameraCallback = async (callback: any) => {
    this.setState({
      isCameraOpen: false,
    },
    () => {
      this.props.onChange(callback)
    })

    callback.then((photos: any) => {
      if (!_.isEmpty(photos)) {
        this.setState({
          //@ts-ignore
          attachments: 1
        })
      }
    })
  }

  imageBrowserCallback = async (callback: any) => {

    this.setState({
      imageBrowserOpen: false,
    },
    () => {
      this.props.onChange(callback)
    })

    callback.then((photos: any) => {
      if (!_.isEmpty(photos)) {
        this.setState({
          //@ts-ignore
          attachments: 1
        })
      }
    })
  }

  render() {
    if (this.state.isCameraOpen) {
      return <CameraScreen callback={this.cameraCallback} />
    }
    if (this.state.imageBrowserOpen) {
      return (
        <ImageBrowserScreen
          max={1}
          callback={this.imageBrowserCallback}
        />
      )
    }
    if (this.props.readonly) {
      return this.renderReadonly()
    }

    const { name, comments, attachments } = this.state
    const {
      modalHeaderText,
      modalPlaceholderText,
      modalSaveButtonText,
      helpText,
      readonly,
    } = this.props

    const showCommentIcon = this.props.rootStore.settingsStore.rules.items
      .checklistShowActions.comments

    let customStyles = StyleSheet.create({
      picklistContainer: {
        width: this.state.screenWidth,
        paddingRight: 12,
        borderLeftColor: "white",
        borderLeftWidth: 4,
      },
    })

    return (
      <View style={styles.container}>
        <ScrollView
          ref="_scrollView"
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          <View style={customStyles.picklistContainer}>
            <View style={styles.card}>
              <View style={styles.labelContainer}>
                {!_.isEmpty(comments) ? (
                  <View style={styles.commentIndicatorContainer}>
                    <FIcon icon={Icons.COMMENTSDOTS} color={COLORS.PRIMARY} />
                  </View>
                ) : (
                  <View />
                )}
                {attachments && attachments > 0 ? (
                  <View style={styles.commentIndicatorContainer}>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-around",
                      }}
                    >
                      <FontAwesome
                        name="file-picture-o"
                        size={18}
                        color={COLORS.PRIMARY}
                      />
                      <View
                        style={{
                          padding: 1,
                          backgroundColor: "orange",
                          position: "absolute",
                          borderRadius: 10,
                          marginLeft: 8,
                        }}
                      >
                        <Text
                          style={{
                            color: "#fff",
                            fontWeight: "600",
                            fontSize: 10,
                          }}
                        >
                          {attachments}
                        </Text>
                      </View>
                    </View>
                  </View>
                ) : (
                  <View/>
                )}
                <View style={styles.nameSection}>
                  <Text style={styles.label}>{name}</Text>
                  {!_.isEmpty(helpText) && (
                    <Text style={styles.helpText}>{helpText}</Text>
                  )}
                </View>
              </View>

              <View style={styles.buttonContainer}>
                <View style={styles.cameraContainer}>
                  {this.cameraButton()}
                </View>
                <View style={styles.cameraContainer}>
                  {this.libraryButton()}
                </View>
              </View>
            </View>
          </View>
          {showCommentIcon ? (
            <View style={styles.commentsContainer}>
              <FModalInput
                value={comments}
                header={modalHeaderText}
                placeholder={modalPlaceholderText}
                saveButtonText={modalSaveButtonText}
                onBack={this.onBack}
                onSave={this.onSaveComment}
                triggerItem={this.commentButton()}
                isReadOnly={readonly}
              />
            </View>
          ) : (
            <View />
          )}
        </ScrollView>
      </View>
    )
  }
}

//@ts-ignore
FPicture.defaultProps = {
  onChange: (s) => {},
  //@ts-ignore ????
  onSubmit: (s) => {},
  name: "Default Name",
  comments: "",
  value: [],
  modalSaveButtonText: "Save",
  helpText: "",
  readonly: false,
}

export default FPicture
