import { StyleSheet } from 'react-native'
import { DEVICE_WIDTH, COLORS } from '../../constants'

export const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0
    },
    containerReadonly: {
        backgroundColor: "#f0f0f0",
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        flex: 1
    },
    picklistContainer: {
        width: DEVICE_WIDTH,
        paddingRight: 12,
        borderLeftColor: "white",
        borderLeftWidth: 4,
        justifyContent: 'center'
    },
    picklistContainerReadonly: {
        width: DEVICE_WIDTH,
        paddingLeft: 0,
        paddingRight: 12,
        borderLeftColor: "#f0f0f0",
        borderLeftWidth: 4,
        justifyContent: 'center'
    },
    picklistContainerWithComments: {
        width: DEVICE_WIDTH,
        paddingLeft: 12,
        paddingRight: 12,
        borderLeftColor: COLORS.PRIMARY,
        borderLeftWidth: 4,
    },
    card: {
        padding: 12,
        paddingLeft: 0,
        flexDirection: "row"
    },
    cardItem: {
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0
    },
    cardBody: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    label: {
        paddingRight: 12,
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    helpText: {
        paddingLeft: 12,
        fontFamily: 'Roboto',
        color: COLORS.DARKGRAY,
        fontSize: 12,
        letterSpacing: 0,
        lineHeight: 16,
    },
    nameSection: {
        flex: 8,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    comments: {
        marginTop: 6,
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#BCC8CE',
        letterSpacing: 0,
        lineHeight: 16,
    },
    labelContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: 'center',
    },
    commentIndicatorContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 10
    },
    buttonContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: "row"
    },
    activeReadonlyButton: {
        flexDirection: 'row',
        borderRadius: 20,
        marginRight: 3,
        marginVertical: 4,
        padding: 7,
        backgroundColor: "#c8c8c8"
    },
    inactiveReadonlyButton: {
        backgroundColor: "#f5f5f5",
        flexDirection: 'row',
        borderRadius: 20,
        marginRight: 3,
        marginVertical: 4,
        padding: 7
    },
    button: {
        flex: 1,
        alignItems: 'center',
    },
    activeText: {
        alignSelf: "center",
        color: "#fff",
        fontSize: 12,
        height: 12,
        includeFontPadding: false,
        lineHeight: 12,
    },
    activeReadonlyText: {
        alignSelf: "center",
        color: "white",
        fontSize: 12,
        height: 12,
        includeFontPadding: false,
        lineHeight: 12,
    },
    inactiveText: {
        alignSelf: "center",
        color: "#48aaf8",
        fontSize: 12,
        height: 12,
        includeFontPadding: false,
        lineHeight: 12,
    },
    inactiveReadonlyText: {
        alignSelf: "center",
        color: "#c8c8c8",
        fontSize: 12,
        height: 12,
        includeFontPadding: false,
        lineHeight: 12,

    },
    inactiveContentContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#e8f6fe',
        borderRadius: 20,
        marginRight: 3,
        marginVertical: 4,
        padding: 7
    },
    activeContentContainer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#48aaf8',
        borderRadius: 20,
        marginRight: 3,
        marginVertical: 4,
        padding: 7
    },
    readOnlyCommentsContainer: {
    height: "100%",
    backgroundColor: '#c8c8c8'
    },
    commentsContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#48aaf8',
    },
    cameraContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: "100%"
    },
    pictureButton: {
        padding: 24,
        borderColor: "#DAE8F2",
        borderWidth: 2,
        borderRadius: 20,
        backgroundColor: "#48aaf8"
    },
    readOnlyPictureButton: {
        padding: 24,
        borderColor: "#f0f0f0",
        borderWidth: 2,
        borderRadius: 20,
        backgroundColor: "#c8c8c8"
    }
})