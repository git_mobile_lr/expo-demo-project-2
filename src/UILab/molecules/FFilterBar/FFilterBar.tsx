import React from 'react';
import { Text } from 'native-base'
import { ScrollView, View } from 'react-native'
import FFilter from '../../atoms/FFilter/FFilter'
import FSpinner from '../../atoms/FSpinner/FSpinner'
import FInlineButton from '../../atoms/FInlineButton/FInlineButton'
import _ from 'lodash'
import { inject, observer } from "mobx-react/native"
import { FilterCondition } from '../../../types'
import { styles } from './Styles'

type Props = {
  persistent: boolean,
  initialData: Array<object>
  conditions: Array<FilterCondition>
  rootStore?: any
  renderItem: (item: any) => JSX.Element
}

type State = {
  persistent: boolean,
  condition: FilterCondition | null
  conditions: Array<FilterCondition>
  initialData: Array<object>
  loading: boolean
  assetsAreLoaded: boolean
}

@inject('rootStore')
@observer
class FFilterBar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      condition: null,
      persistent: props.persistent,
      conditions:  this.filterConditions(props.conditions),
      initialData: props.initialData,
      loading: true,
      assetsAreLoaded: false

    }
  }

  componentDidMount() {

    this.setState({
      conditions: this.tryGetCookies(),
      initialData: this.props.initialData,
      assetsAreLoaded: true
    })
    

  }

  componentDidUpdate(prevProps: Props) {
    if (!_.isEqual(this.props.initialData, this.state.initialData) ||
      !_.isEqual(prevProps.conditions, this.props.conditions)) {
      this.setState({
        initialData: this.props.initialData,
        loading: true,
        conditions: this.sanityConditions(this.filterConditions(this.props.conditions))
      }, this.tryStoreCookies)

    }
  }

  tryGetCookies() {

    if (this.props.persistent) {
      let _cookie = this.props.rootStore.loginStore.getCookiePerActiveRouteKey


      if (_cookie != null) {
        return this.sanityConditions(_cookie)
      }
    }

    return this.sanityConditions(this.filterConditions(this.props.conditions))
  }

  tryStoreCookies() {

    if (this.props.persistent) {
      this.props.rootStore.loginStore.setCookiePerActiveRouteKey(this.state.conditions)
    }
  }

  sanityConditions(conditions: Array<FilterCondition>) {
    
    const { dataStore } = this.props.rootStore
    if (!_.isEmpty(conditions)) {
      return conditions.map((condition, index) => {
        
        let newCondition = _.clone(condition)
        newCondition['id'] = index
        if (condition.hasOwnProperty("dynamic")) {
          newCondition['value'] = dataStore.getValue(condition.table, condition.field)
        }
        return newCondition
      })
    }

    return conditions
  }

  handleChange(condition: FilterCondition) {
    const prevCond = this.state.conditions.filter((condition: FilterCondition) => condition.active)

    let newConditions = this
      .state
      .conditions
      .map(stateCondition => {
        if (stateCondition.id === condition.id) {
          condition.hasOwnProperty("unique")
            ? !stateCondition.active ? stateCondition.active = !stateCondition.active : ''
            : prevCond.length === 1 && stateCondition.active ? '' : stateCondition.active = !stateCondition.active
            
          return stateCondition
        }
        return stateCondition
      })
      
    // If the filter is unique -> deactivate others
    if (condition.hasOwnProperty("unique") && condition.unique && condition.active) {
      newConditions = newConditions.map(cond => {
        if (cond.id != condition.id) {
          cond.active = false
        }
        return cond
      })
    } else {
      // If is not unique deactivate the uniques
      newConditions = newConditions.map(cond => {
        if (cond.hasOwnProperty("unique") && cond.unique) {

          cond.active = false
        }
        return cond
      })
    }

    if ( !_.isEmpty(newConditions.filter((condition:FilterCondition)=>condition.active)) ){
      this.setState({ conditions: newConditions, loading: true }, this.tryStoreCookies)
    }
    
  }

  filterConditions(conditions:Array<FilterCondition>){
    
    const { validationStore } = this.props.rootStore
    if (!_.isEmpty(conditions)){
      return conditions.filter((condition:FilterCondition)=>{
        if (condition.hasOwnProperty("condition")){
          
          let result = validationStore.checkCondition(condition.condition)
          return result
        }else{
          return true
        }
      })
    }
  }

  render() {
    const { loginStore } = this.props.rootStore
    const { renderItem } = this.props
    const { conditions } = this.state
    let filters = this.sanityConditions(this.state.conditions)
    
     
    return (
      <>
        <View>
          <ScrollView keyboardShouldPersistTaps={'handled'} horizontal={true}
            contentContainerStyle={{ justifyContent: "center", alignItems: "center", alignContent: "center", height: '100%', margin: 2 }}>
            {!_.isEmpty(conditions) && conditions.map((condition, index) => {
              if (condition.hidden) return null
              let label = loginStore.getLabelorDefault(condition.label ? condition.label : "", condition.defaultText ? condition.defaultText : "")

              return condition.active
                ? (
                  <FInlineButton accessibilityLabel={label} testID={label + " iOS"} key={index} text bordered onPress={() => this.handleChange(condition)}>
                    <Text style={[{ color: 'white' }]}>
                      {label}
                    </Text>
                  </FInlineButton>
                )
                : (
                  <FInlineButton accessibilityLabel={label} testID={label + " iOS"} key={index} text inactive onPress={() => this.handleChange(condition)}>
                    <Text style={{ color: '#00AAFF' }}>
                      {label}
                    </Text>
                  </FInlineButton>
                )

            })
            }
          </ScrollView>
        </View>
        <View>
          <FFilter
            onNoConditionsRenderEmpty
            getLinkedObjectMediaById={loginStore.getLinkedObjectMediaById}
            initialData={this.state.initialData}
            conditions={filters}
            render={data => {
              return data.loading
                ? <FSpinner />
                : renderItem({
                  ...data,
                  loading: data.loading,
                  state: this.state
                })
            }} />
        </View>
      </>
    )
  }

}

//@ts-ignore
FFilterBar.defaultProps = {
  persistent: false,
  onNoConditionsRenderEmpty: false,
  conditions: [],
  initialData: [],
  paging: true,
  pageSize: 10,
  currentPage: 0,
  onUpdateConditions: () => { },
  renderItem: () => { }
}

export default FFilterBar;