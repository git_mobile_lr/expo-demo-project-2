import { StyleSheet } from 'react-native'
import { COLORS, FONTS } from '../../constants'
import { IS_TABLET, ANDROID } from '../../../screens/widgets/styles'
export const styles = StyleSheet.create({
    text: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: "#40505A",
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0

    },

    label: {
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    FInput: {
        height: 10,
        width: IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent',
        flex: 2,
        alignSelf: "flex-start",
        justifyContent: "center",
        alignContent: "center",


        fontSize: 16,

        color: "#40505A",
        letterSpacing: 0,

        paddingLeft: 0
    }
})