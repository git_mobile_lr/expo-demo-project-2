import React, { Component } from 'react';
import { Text, Card, Body, CardItem, Item, Input, Label, Icon, View } from 'native-base'
import { Ionicons, FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';
import { styles } from './Styles'
import _, { result } from 'lodash'
import { inject, observer } from "mobx-react/native"
import { StyleSheet, Alert, Platform, TouchableOpacity } from 'react-native'
type Props = {
  formula?: string,
  apiName?: string,
  label?: string
}

type State = {
  value?: any,
  lastUpdate?: string
}

@inject('rootStore')
@observer
class FFormula extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      value: "",
      lastUpdate: this.props.rootStore.dataStore.lastUpdate
    }

  }

  componentWillReact() {

    if (this.state.lastUpdate !== this.props.rootStore.dataStore.lastUpdate) {
      this.setState({
        lastUpdate: this.props.rootStore.dataStore.lastUpdate
      }, () => {
        this._loadAsync()
      })
    }
  }

  componentDidMount() {
    this._loadAsync()
  }

  async _loadAsync() {

    const _value = this.props.rootStore.dataStore.calcFormula(this.props.apiName)

    this.setState({
      lastUpdate: this.props.rootStore.dataStore.lastUpdate,
      value: _value
    })

  }


  render() {
    const { lastUpdate } = this.props.rootStore.dataStore

    return (
      <Item stackedLabel style={styles.FInput}>
        <Label style={styles.label}>{this.props.label}</Label>
        <Input style={styles.text}>{this.state.value}</Input>
      </Item>
    )
  }
}
//@ts-ignore
FFormula.defaultProps = {
  formula: "",
  apiName: "",
  label: ''
}
export default FFormula;
