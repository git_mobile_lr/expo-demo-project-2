import React, { Component } from 'react';
import { Item, Label, Text } from 'native-base'
import { styles, customStyles } from './Styles'
import DatePicker from 'react-native-datepicker'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { FontAwesome } from "@expo/vector-icons";
import { DateMode, Locale } from '../../../types'
import { Platform, Appearance } from 'react-native';
import _ from 'lodash';


type Props = {
	isEditing?: boolean
	value?: string | undefined
	mode?: DateMode.DATE | DateMode.DATETIME
	placeholder?: string
	locale?: Locale.enNL | Locale.enUS | Locale.nlNL
	confirmButtonText?: string
	cancelButtonText?: string
	onChange?: (date: string) => void
	onSubmit?: (date: string) => void
	utcOffset?: number
	readonly?: boolean
	label: string,
	iconComponent?: () => object
	showIcon?: boolean,
	format?: string,
	toggleIsEditing: () => void

}

type State = {
	isEditing?: boolean
	value?: string | undefined
	mode?: DateMode.DATE | DateMode.DATETIME
	placeholder?: string
	locale?: Locale.enNL | Locale.enUS | Locale.nlNL
	confirmButtonText?: string
	cancelButtonText?: string
	utcOffset?: number
	isVisible?: boolean
}

class FDate extends Component<Props, State> {
	constructor(props: Props) {
		super(props);
		this.state = {
			value: props.value,
			isEditing: props.isEditing,
			mode: props.mode,
			placeholder: props.placeholder,
			locale: props.locale,
			confirmButtonText: props.confirmButtonText,
			cancelButtonText: props.cancelButtonText,
			utcOffset: props.utcOffset,
			isVisible: props.isEditing
		};

	}

	componentDidUpdate() {
		if (this.props.isEditing != this.state.isEditing) {
			//@ts-ignore -> Should component FDate extend DatePicker ??
			this.setState({ isEditing: this.props.isEditing })
			if(this.state.isVisible === true) {
				this.setState({ isVisible: false })
			} else {
				this.setState({ isVisible: this.props.isEditing })
			}
		}
	}

	_handleDateChangeAndroid = async (date: any) =>{
		
		setTimeout(() => {
			this.setState({ value: date, isEditing: true }, () => {
				const { utcOffset } = this.props
				let formattedDate = moment(date).utcOffset(utcOffset ? utcOffset : 0, true).format()
				//@ts-ignore Defined in default Props
				this.props.onChange(formattedDate)
				//@ts-ignore Defined in default Props
				this.props.onSubmit(formattedDate)

				// this.props.toggleIsEditing()

				this.setState({ isEditing: !this.props.isEditing })
			})
		}, 100);
	}

	handleDateChange = (date: any) => {
		this.setState({ value: date })
		const { utcOffset } = this.props
		let formattedDate = moment(date).utcOffset(utcOffset ? utcOffset : 0, true).format()
		//@ts-ignore Defined in default Props
		this.props.onChange(formattedDate)
		//@ts-ignore Defined in default Props
		this.props.onSubmit(formattedDate)

		this.props.toggleIsEditing()
	
		this.setState({ isEditing: !this.props.isEditing})
		// this.setState({ isEditing: !this.props.isEditing},this.props.toggleIsEditing)
	}

	formatDate(date: any) {
		if (date == "") return undefined
		const { utcOffset } = this.props
		let timeZone = utcOffset ? utcOffset / 60000 : 0
		let f_date = moment(date).utcOffset(timeZone).format(this.props.format)
		return f_date
	}

	isValidDate = d => new Date(d)
		.toString() !== 'Invalid Date';

	getValue() {
		try {
			const { value } = this.state
			if (value) {
				try {
					if (!this.isValidDate(value)) {
						return new Date()
					}
				} catch (error) {
					return new Date()
					//pass
				}

				return value
			} else {
				return new Date()
			}
		}
		catch (exc) {
			return new Date();
		}
	}

	hide = () => {
		this.setState({ isEditing: !this.props.isEditing},this.props.toggleIsEditing)
	}

	getFormattedValue() {

		const { value } = this.state
		
		let _format = Platform.OS === 'ios' ? this.props.format : "YYYY-MM-DD"
		try {
			if (value === null) {
				return ""
			}

			if (!this.isValidDate(value)) {
				return ""

			}

			return moment(new Date(this.getValue())).format(_format).toString()
		} catch (e) {
			//pass
		}

		return ""
	}

	render(){
		return Platform.OS === 'ios' ? this._renderIOS() : this._renderAndroid()

	}

	_renderAndroid(){
		const { value, mode, placeholder, locale, confirmButtonText, cancelButtonText, isEditing, isVisible } = this.state
		const { label, iconComponent, format, showIcon } = this.props

		const _text =  this._renderEmptyText()
		return (
			<Item stackedLabel style={styles.inputItem}>
				<Label style={styles.label}>{label}</Label>
				{isVisible && <DateTimePickerModal
					isVisible={true}
					is24Hour={true}
					cancelTextIOS={cancelButtonText}
					confirmTextIOS={confirmButtonText}
					date={new Date(this.getValue())}
					
					locale={locale}
					format="YYYY-MM-DD"
					androidDisplay="default"
					style={{color:'#00aaff'}}
					mode="date"
					onConfirm={this._handleDateChangeAndroid}
					onCancel={this.hide}
				
		  		/>}
				{_text}
			</Item>
		);
	}


	private _renderEmptyText() {
		const _text = this.getFormattedValue()
		if (_text === "") {
			return (<Text
				style={customStyles.placeholderText}>{this.props.label}
			</Text>)
		}
		return (<Text
			style={customStyles.dateText}>{_text}
		</Text>)
	}

	_renderIOS() {
		const { value, mode, placeholder, locale, confirmButtonText, cancelButtonText, isEditing, isVisible } = this.state
		const { label, iconComponent, format, showIcon } = this.props
		const isDarkMode = Appearance.getColorScheme()==='dark'
		const _text =  this._renderEmptyText()
		return (
			<Item stackedLabel style={styles.inputItem}>
				<Label style={styles.label}>{label}</Label>
				{isVisible && <DateTimePickerModal
					isVisible={true}
					is24Hour={true}
					cancelTextIOS={cancelButtonText}
					confirmTextIOS={confirmButtonText}
					date={new Date(this.getValue())}
					locale={locale}
					display="spinner"
					mode={mode===DateMode.DATE ? "date" : "datetime"}
					onConfirm={this.handleDateChange}
					onCancel={this.hide}
					isDarkModeEnabled={isDarkMode}
		  		/>}
				{_text}
			</Item>
		);
	}
}

//@ts-ignore
FDate.defaultProps = {
	onChange: () => { },
	onSubmit: () => { },
	isEditing: false,
	value: undefined,
	mode: DateMode.DATETIME,
	placeholder: "Date",
	locale: Locale.enUS,
	confirmButtonText: "Confirm",
	cancelButtonText: "Cancel",
	utcOffset: 0,
	readonly: false,
	format: "YYYY-MM-DD HH:mm",
	showIcon: false,
	isVisible: false,
	toggleIsEditing: ()=> {}
}

export default FDate;