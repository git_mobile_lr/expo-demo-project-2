import React from 'react';
import { List, ListItem, View, Text } from 'native-base'
import { inject, observer } from "mobx-react/native"
import ListItemDetails from '../../atoms/ListItemDetails/ListItemDetails'
import { FSearchBar } from '../../index'
import FSpinner from '../../atoms/FSpinner/FSpinner'
import _ from 'lodash'
import FOnlineResource from '../../atoms/FOnlineResource/FOnlineResource'
import FNoResult from '../FNoResult/FNoResult'
import jsonQuery from 'json-query'

//@inject('loginStore')
@inject('rootStore')
@observer
class FList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            valueToFilter: "",
            values: {},
            
        };
    }

    handleSelection = (target) => {
        const { onSelectOption, toggleModal } = this.props
        onSelectOption(target)
        toggleModal()
    }

    onSearch = (value) => {
        this.setState({ valueToFilter: value })
    }

    getFilterCondition () {
        const {loginStore} = this.props.rootStore
        const { valueToFilter } = this.state
        const { fieldsToFilter } = this.props
        return {
            "conditions": [
                {
                    "fieldName": "Location__c",
                    "operator": "=",
                    "searchValue": jsonQuery(`${loginStore.getPrefixedFieldName("Work_Order__c")}[Id=${loginStore.currentWorkOrderId}][${loginStore.getPrefixedFieldName("Location__c")}]`,{data:loginStore.data}).value
                },
                {
                    "conditions":fieldsToFilter.map(field=>{
                        return({
                            "fieldName": field,
                            "operator": "Like",
                            "searchValue": `%${valueToFilter}%`
                        })
                    }),
                    "operator":"OR"
                }
            ],
            "operator": "AND"
          }
    }

    render() {
        const { searchBar, referenceTo, limit, online, customList, fieldsToRequest, fieldsToDisplay, sourcefield, layout} = this.props
        const { loginStore } = this.props.rootStore
        const { valueToFilter } = this.state

        // Must be filtered by location ?
        restrictedByLocation = (referenceTo === loginStore.getPrefixedFieldName("Installed_Product__c"))

        return (
            <View>
                {searchBar && <FSearchBar onSubmitSearch={this.onSearch} barCodeButton/>}
                {online && restrictedByLocation && <FOnlineResource
                    table={referenceTo}
                    limit={limit}
                    condition={this.getFilterCondition()}
                    key={valueToFilter}
                    onFail={'reAuth'}
                    fieldsToRequest={fieldsToRequest}
                    render={data => {
                        if (data.loading) return <FSpinner showTimeOut={5000} />
                        if (_.isEmpty(data.payload)) return <FNoResult labelProp='${Label.NoResultFound}' />
                        
                        return (
                            <List>
                                {data.payload
                                    .map((item, index) => {
                                            return (
                                                <ListItem key={index} onPress={() => { this.handleSelection(item) }}>
                                                    <ListItemDetails sourcefield={sourcefield} fieldsToDisplay={fieldsToDisplay} item={item} layout = {layout} />
                                                </ListItem>
                                            )
                                    })}
                            </List>)
                    }}
                />}

                {online && !restrictedByLocation && <FOnlineResource
                    table={referenceTo}
                    limit={limit}
                    fieldsToFilter={this.props.fieldsToFilter}
                    valueToSearch={valueToFilter}
                    key={valueToFilter}
                    mode={'contains'}
                    onFail={'reAuth'}
                    fieldsToRequest={fieldsToRequest}
                    render={data => {
                        if (data.loading) return <FSpinner showTimeOut={5000} />
                        if (_.isEmpty(data.payload)) return <FNoResult labelProp='${Label.NoResultFound}' />
                        return (
                            <List>
                                {data.payload
                                    .map((item, index) => {
                                            return (
                                                <ListItem key={index} onPress={() => { this.handleSelection(item) }}>
                                                    <ListItemDetails sourcefield={sourcefield} fieldsToDisplay={fieldsToDisplay} item={item} layout = {layout}/>
                                                </ListItem>
                                            )
                                    })}
                            </List>)
                    }}
                />}
                
                {!_.isEmpty(customList) && 
                    <List>
                    {customList
                        .map((item, index) => {
                            const _accessibilityLabel = item.label + " List Item"
                            const _testID = _accessibilityLabel + " iOS" 
                            if (typeof item == 'string'){
                                return (
                                    <ListItem key={index} onPress={()=>this.handleSelection(item)}>
                                        <Text>{item}</Text>
                                    </ListItem> 
                                )
                            }
                            return (
                                    <ListItem key={index} onPress={() => { this.handleSelection(Object.assign(item,{"picklist":true})) }}>
                                        <Text accessibilityLabel={_accessibilityLabel} testID={_testID}>{item.label}</Text>
                                    </ListItem>
                                )
                            }
                        )}
                </List>
                }
                {!online && _.isEmpty(customList) && <FNoResult labelProp='${Label.NoResultFound}' />}
            </View>)
    }
}

FList.defaultProps = {
    toggleModal: () => { },
    onSelectOption: () => { },
    onSave: () =>{},
    listItem: "defaultItem",
    list: [],
    limit:10,
    searchBar: true,
    referenceTo: "Table__c",
    searchBar:true,
    customList:[],
    fieldsToRequest :  ["Id", "Name"],
    searchBy:["Name"]
}

export default FList;
