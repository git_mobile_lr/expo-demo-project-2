/**
 * @jest-environment jsdom
 */
import React from 'react';
import { mount } from 'enzyme';
import FPages from '../FPages';
import { View, Text } from 'react-native'
import _ from 'lodash'

// 
describe('FPages',()=>{
  const data = [{title:"Item 1"},{title:"Item 2"},{title:"Item 3"}]
  test('Set correct values if there are props',()=>{

    const wrapper = mount(
      <FPages  
        data={data}
        itemsPerPage={1}
        renderItem={(item:any)=><View><Text>{item.title}</Text></View>}
      />
    );
    //@ts-ignore
    expect(wrapper.state().data).toStrictEqual(data)
    //@ts-ignore
    expect(wrapper.state().chunks).toStrictEqual(_.chunk(data,1))
    //@ts-ignore
    expect(wrapper.state().active).toStrictEqual(0)
  })

})



