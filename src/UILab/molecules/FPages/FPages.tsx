import React, { Component } from 'react';
import { Dimensions, FlatList, Image, Text } from 'react-native'
import { View } from 'native-base'
import _ from 'lodash'
import { inject, observer } from "mobx-react/native"
import { styles } from './Styles'
import FNoResult from '../FNoResult/FNoResult';
import { Card } from 'react-native-elements'

type Props = {
    data: Array<object>
    itemsPerPage: number
    renderItem: (item: object) => JSX.Element
    onRefresh?: () => void
    sortBy?: () => Array<object>
    emptyLabel: String
    defaultIcon: () => void,
    addAssetLinkToMedia: boolean,
    initialNumToRender?: number
}

type State = {

    data: Array<object>
    chunks: Array<Array<object>>
    active: number
    isRefreshing: boolean
}

@inject('rootStore')
@observer
class FPages extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            data: props.data,

            chunks: this.chunk(),
            active: 0,
            isRefreshing: false
        };
    }

    flatListRef : String = "flatListRef"

    getItemLayout = (data, index) => {
        const W = Dimensions.get('window').width;
        return { length: W, offset: W * index, index }
    }

    onMomentumScrollEnd = (e: Event) => {
        let contentOffset = e.nativeEvent.contentOffset;
        let viewSize = e.nativeEvent.layoutMeasurement;
        let currentPage = Math.floor(contentOffset.x / viewSize.width);
        this.props.rootStore.loginStore.cookiedPage = currentPage

    }
    componentDidUpdate() {
        if (!_.isEqual(this.props.data, this.state.data)) {
            this.setState({
                data: this.props.data,
                chunks: this.chunk()
            })
        }
    }

    chunk() {
        const { data, itemsPerPage } = this.props
        // Sort Data
        let sortedData = this.props.sortBy ? _.sortBy(data, this.props.sortBy).reverse() : data
        let chunk =  _.chunk(sortedData, itemsPerPage)

        this._getChunkMedia(chunk);
        return chunk
    }


    onRefresh = () => {
        this.setState({ isRefreshing: true })
        this.props.onRefresh()
        this.setState({ isRefreshing: false }, () => {
        })
    }


    async _getChunkMedia(chunk: object[][]) {
        
        if (this.props.addAssetLinkToMedia) {
            const {loginStore} = this.props.rootStore
            
            function _shouldDownload(item : any) {
                return !(item.IsLocal || item.IsBeingUploaded || item.IsFailed || item.IsUploaded || item.IsCached || item.IsRemoved || item.IsBroken) || item.QuickDownload
            }

            for (let _elem of chunk) {
                //debugger
                let el = _elem.filter(x => { return _shouldDownload(x); })
                if ((el || []).length > 0) {
                    console.warn("downloading... " + ((el || []).length))
                }
                if (!_.isEmpty(el)) {
                    el = _.sortBy(el, x=>x["FileSize"])

                    await loginStore._addAssetLinkToMedia(el);
                }
                
            }
        }
    }

    getInitialScrollIndex() {
        try {
            let index = this.props.rootStore.loginStore.cookiedPage || 0
            return (index >= this.state.chunks.length) ? 0 : index
        } catch (error) {
            return 0
        }
    }

    render() {

        const { chunks, isRefreshing } = this.state
        const isSmallDevice = this.props.rootStore.loginStore.isSmallDevice()
        const { renderItem } = this.props
        const {S_W, orientation} = this.props.rootStore.loginStore
        const pages = chunks.length

        if (!pages) return <FNoResult labelProp='${Label.NoResultFound}' />

        return (
            <FlatList
                ref={(ref) => { this.flatListRef = ref; }}
                keyExtractor={(item: any) => {
                    if (!_.isEmpty(item)) {
                        if (_.first(item)) {
                            
                            return `page${_.first(item).Id}`
                        }
                    }
                    return null
                }}
                keyboardShouldPersistTaps={'handled'}
                initialScrollIndex={this.getInitialScrollIndex()}
                onEndReachedThreshold={0.5}
                maxToRenderPerBatch={20}
                initialNumToRender={this.props.initialNumToRender}
                data={chunks}
                getItemLayout={this.getItemLayout}
                onMomentumScrollEnd={this.onMomentumScrollEnd}
                pagingEnabled
                horizontal={true}
                renderItem={chunk => {
                    let _flatList = (<FlatList

                        onRefresh={this.onRefresh}
                        refreshing={isRefreshing}
                        keyExtractor={(item: any) => {
                            if (!_.isEmpty(item)) {
                                return item.recordId
                            }
                            return null
                        }}
                        ListHeaderComponent={<View style={styles.ListHeaderComponent}><Text>{`${chunk.index + 1}/${pages}`}</Text></View>}
                        ListFooterComponent={<View style={styles.ListFooterComponent}></View>}
                        onEndReachedThreshold={1}
                        keyboardShouldPersistTaps={'handled'}
                        maxToRenderPerBatch={1}
                        initialNumToRender={1}
                        data={chunk.item}
                        renderItem={item => renderItem(item.item)}
                    />)
                    return (<Card containerStyle={[styles.listItemCardContainer, { width: this.props.rootStore.loginStore.S_W }]}>
                        {_flatList}
                    </Card>)                   
                }}
            />
        )

        
    }
}

//@ts-ignore
FPages.defaultProps = {
    data: [],
    itemsPerPage: 5,
    renderItem: () => <View />,
    onRefresh: () => { },
    sortBy: null,
    initialNumToRender: 1
}
export default FPages;