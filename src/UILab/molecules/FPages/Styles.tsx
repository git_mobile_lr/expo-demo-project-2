import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

export const styles = StyleSheet.create({
    listItemContainer: {
        width: wp("100%")
    },
    listItemCardContainer:
    {
        margin: 0,
        padding: 0,
        backgroundColor: 'transparent',        
        borderWidth: 0
    },
    ListHeaderComponent:{
        width:"100%",
        alignItems:"flex-end",
        paddingRight:24
    },
    ListFooterComponent:{
        height:100
    },
    smallDeviceEmptyFont: {
        color: '#7a92a5',
        fontWeight: '200',
        fontSize: 18,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
        paddingHorizontal: 20,
        paddingVertical: 5
    },
    emptyFont: {
        color: '#7a92a5',
        fontWeight: '200',
        fontSize: 25,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
        paddingHorizontal: 20
    },
    icon: {
        marginBottom: 20,
        marginLeft: 20, 
    }
})