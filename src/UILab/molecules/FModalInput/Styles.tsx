import {StyleSheet, Platform} from 'react-native'
import Constants from 'expo-constants'

import { COLORS, IS_IPHONE_12_ISH } from '../../constants'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
  } from "react-native-responsive-screen";
  

export const styles = StyleSheet.create({
    textInput:{
        marginTop:12,
        height:hp("50%"),
        padding:24
    },
    FModalIcon:{
        paddingRight:0,
        marginRight:0,
        marginLeft:0,
        color:'#00AAFF'
    },
    modalScreen2:{
        flex: 1,
          width: wp("100%"),
          height: hp("100%"),
          margin: 0
    },
    modalScreen: {
   
        paddingTop: IS_IPHONE_12_ISH() ? Constants.statusBarHeight / 3 : undefined,
        margin: 0
      },
    button:{
        
    },
    saveText:{
        color:COLORS.PRIMARY
    },
    FModal:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        height:50,
        borderColor: 'transparent'
    },
    placeholder:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#B8C4CC',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0
    },
    header:{
        backgroundColor: "white" 
    },
    headerTitleStyle:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0  
    },
    backIcon:{
        fontFamily: 'Roboto',
        color: "#00AAFF" 
    },
    text:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0
    }
})
