import React, { Component } from "react";
import { Button, Text, View, Container, Header, Left, Body, Footer, Right, Title } from "native-base";
import Modal from "react-native-modal";
import { ScrollView as Content } from 'react-native'
import { styles } from './Styles'
import { FontAwesome } from "@expo/vector-icons"
import { Input } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Platform, TouchableOpacity, TextInput, Dimensions } from 'react-native'
import _ from 'lodash'
import Constants from 'expo-constants'
import { IS_TABLET, IS_IPHONE_12_ISH } from '../../constants';

type Props = {
  isModalVisible: boolean
  onBack?: Function
  onSave?: Function
  triggerItem?: Element
  header?: string
  value: string
  placeholder?: string
  rootStore?: any
  saveButtonText: string
  isReadOnly: boolean
}

type State = {
  isModalVisible: boolean
  header?: string
  value: string
}

export default class FModalInput extends Component<Props, State> {

  static defaultProps: { onSave: () => void; onBack: () => void; header: string; value: string; isModalVisible: boolean; triggerItem: {}; placeholder: string; isReadOnly: boolean };

  constructor(props: Props) {
    super(props);
    this.state = {
      isModalVisible: props.isModalVisible,
      header: props.header,
      value: props.value
    };
  }

  componentDidUpdate(prevProps: Props) {
    if (!_.isEqual(prevProps.isModalVisible, this.props.isModalVisible)) {
      this.setState({ isModalVisible: this.props.isModalVisible })
    }
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  handleBack = () => {
    this.props.onBack ? this.props.onBack() : null
    this.toggleModal()
  }

  defaultTriggerButton() {
    return (
      <TouchableOpacity onPress={this.toggleModal}>
        <Text>Click Me</Text>
      </TouchableOpacity>
    )
  }

  triggerItem() {
    const { triggerItem } = this.props
    return _.isEmpty(triggerItem)
      ? this.defaultTriggerButton()
      : <TouchableOpacity onPress={this.toggleModal}>
        {
          React.isValidElement(triggerItem) && triggerItem
        }
      </TouchableOpacity>
  }

  updateValue = (value: any) => {
    this.setState({ value: value })
  }

  onSave = () => {
    if (!this.props.isReadOnly) {
      this.props.onSave ? this.props.onSave(this.state.value) : null
      this.toggleModal()
    }
  }

  render() {
    const { header, value } = this.state
    const { placeholder, saveButtonText, isReadOnly } = this.props
    const SAVE = saveButtonText

    const _saveButton = !isReadOnly ? (
      <Right>
        <Button transparent onPress={this.onSave}>
          <Text style={styles.saveText}>{SAVE}</Text>
        </Button>
      </Right>) : (<Right></Right>)

    const isPortrait = () => {
      const dim = Dimensions.get('screen');
      return dim.height >= dim.width;
    };
    let modelContainerScreen = {
      height: Dimensions.get('window').height,
      width: Dimensions.get('window').width,
      paddingTop: IS_IPHONE_12_ISH() ?
        (isPortrait() ? Constants.statusBarHeight / 3 : undefined) :
        undefined,
      margin: 0
    }

    let _textInput = this._renderTextInput()

    return (
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        {
          this.triggerItem()
        }

        <Modal
          style={styles.modalScreen}
          backdropOpacity={1}
          backdropColor={'white'}
          hideModalContentWhileAnimating={true}
          isVisible={this.state.isModalVisible}>
          <Container style={modelContainerScreen}>
            <Header style={styles.header}>
              <Left>
                <Button transparent onPress={this.handleBack}>
                  <FontAwesome
                    name={'chevron-left'}
                    size={13}
                    style={styles.backIcon}
                  />
                </Button>
              </Left>
              <Body>
                <Title style={styles.headerTitleStyle}>{header}</Title>
              </Body>
              {_saveButton}
            </Header>
            <Content keyboardShouldPersistTaps={'handled'}>
              {_textInput}
            </Content>
          </Container>
        </Modal>
      </View>
    );
  }

  private _renderTextInput() {
    const { value } = this.state
    const { placeholder } = this.props

    return Platform.OS === 'ios' ? (<TextInput
      style={styles.textInput}
      multiline
      value={value}
      placeholder={placeholder}
      onEndEditing={this.onSave}
      onChangeText={this.updateValue}
      editable={!this.props.isReadOnly} />) : (<KeyboardAwareScrollView>
        <Input

          placeholder={placeholder}
          placeholderTextColor='#B8C4CC'
          onChangeText={this.updateValue}

          style={styles.textInput}
          value={value}
          multiline={true}
          autoFocus={true} />
      </KeyboardAwareScrollView>);
  }
}

FModalInput.defaultProps = {
  onSave: () => { },
  onBack: () => { },
  header: "Select an option",
  value: "Please select an option",
  isModalVisible: false,
  triggerItem: {},
  placeholder: "Please type here",
  isReadOnly: false
};