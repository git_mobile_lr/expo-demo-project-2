import React from 'react';
import {styles} from './FQuantityStyles'
import {View} from 'native-base'
import FInlineButton from '../../atoms/FInlineButton/FInlineButton'
import FBadge from '../../atoms/FBadge/FBadge'

class FQuantity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  
            value:props.value,
            id:props.id,
            name:props.name,
            extendedUI:false,
        };
    }

    onMinus = () => {
        const {value} = this.state
        const {onChange} = this.props
        if (value>0){
            this.setState({value:value - 1},
                ()=>{onChange(this)}) 
        }
        if (value===1){
            this.toggleUI()
        }
    }

    onAdd = () => {
        const {value} = this.state
        const {onChange} = this.props
        this.setState({value:value + 1},
            ()=>{onChange(this)})
    }

    toggleUI =  () => {
        const {extendedUI} = this.state
        this.setState({extendedUI:!extendedUI})
    }

    render() {        
            const {extendedUI} = this.state
            return (
              <View style={styles.container}>
                {extendedUI && <View style={styles.container}>
                    <FInlineButton 
                    minus 
                    onPress={this.onMinus}
                    onPressOut={this.onClearInterval}
                />
                <View style={styles.badge}>
                    <FBadge
                        style={styles.badge}
                        value={this.state.value}
                    />
                </View>
                <FInlineButton 
                    add
                    onPress={this.onAdd}
                    onPressOut={this.onClearInterval}
                />
                </View>}
                {!extendedUI && <FInlineButton 
                    add
                    onPress={()=>{
                        this.onAdd()
                        this.toggleUI()
                    }}
                    onPressOut={this.onClearInterval}
                />}
              </View>
        );
    }
}

FQuantity.defaultProps = {
    id:"DefaultId",
    name:"Default Name",
    value : 0,
    onChange:()=>{},
    collapsable:false
}


export default FQuantity;