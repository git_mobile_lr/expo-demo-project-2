import {StyleSheet} from 'react-native'
import {IS_SMALL_DEVICE} from '../../constants'

export const styles = StyleSheet.create({
    container : {
        flex: 1, 
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    badge:{
        marginLeft:24,
        marginRight:24
    }
})