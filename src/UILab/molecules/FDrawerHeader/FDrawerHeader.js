import React from 'react'
import { View, Text, Image } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { styles } from './styles'


const FDrawerHeader = ({ isHorizontal, name, username, headerFont, chatterSize, headerHeight, chatterBorder, imageUri, isUploadingUserPicture, disabled, libraryHandler, uploadEnabled }) => {

  return (
    <View style={styles({ isHorizontal, headerHeight }).header}>
      <View style={styles(chatterSize).chatterWrapper}>
        {isUploadingUserPicture ?
          (
            <Image
              source={{ uri: imageUri }}
              style={styles({ chatterSize, chatterBorder }).defaultChatter}
            />)
          :
          ((uploadEnabled && imageUri) ?
            <TouchableOpacity
              disabled={disabled}
              style={{ flexDirection: 'row' }} onPress={() => libraryHandler()}>
              <Image source={{ uri: imageUri }} style={styles({ chatterSize, chatterBorder }).chatterImg} />
              <View
                style={!isHorizontal ? styles(isHorizontal).cameraBtn : styles(isHorizontal).tabletCameraBtn}>
                <FontAwesome name="camera" size={isHorizontal ? 20 : 10} color="#fff" />
              </View>
            </TouchableOpacity>
            :
            <Image
              source={require('../../../assets/icons/swift-logo-alt.png')}
              style={styles({ chatterSize, chatterBorder }).defaultChatter}
            />)
        }
      </View>

      <View style={{ justifyContent: 'center', }}>
        <Text style={styles({ isHorizontal, name, headerFont }).name}>{name}</Text>
        <Text style={styles({ isHorizontal, username, headerFont }).username}>{username}</Text>
      </View>
    </View>
  )
}



export default FDrawerHeader
