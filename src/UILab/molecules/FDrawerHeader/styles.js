import { StyleSheet } from 'react-native'
export const styles = ({
  headerHeight,
  isHorizontal,
  chatterBorder,
  chatterSize,
  headerFont,
  name,
  username
}) => StyleSheet.create({
  header: {
    paddingTop: 20,
    backgroundColor: '#00AAFF',
    height: headerHeight,
    paddingLeft: 30,
    justifyContent: !isHorizontal ? 'center' : 'flex-start',
    alignItems: isHorizontal ? 'center' : 'flex-start',
    flexDirection: isHorizontal ? 'row' : 'column'
  },
  name: {
    color: '#fff', 
    fontSize: isHorizontal && (name || '').length > 20 ? headerFont/1.3 : headerFont,
    fontWeight: '700', 
    marginTop: isHorizontal ? 0 : 15
  },
  username: {
    color: '#fff',
    fontSize: isHorizontal && (username || '').length > 25 ? headerFont/1.6 : headerFont/1.2
  },
  cameraBtn: {
    backgroundColor: '#F0945E',
    borderRadius: 25,
    height: 20,
    width: 20,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 5,
    left: 65
  },
  tabletCameraBtn: {
    backgroundColor: '#F0945E',
    borderRadius: 25,
    height: 35,
    width: 35,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 10,
    left: 120
  },
  chatterImg: {
    width: chatterSize,
    height: chatterSize,
    borderRadius: chatterSize/2,
    borderWidth: chatterBorder ? 3 : 0,
    borderColor: '#F0945E'
  },
  defaultChatter: {
    width: chatterSize,
    height: chatterSize,
    borderRadius: chatterSize/2
  },
  chatterWrapper: {
    justifyContent: 'center',
    backgroundColor:'transparent',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.5,
    shadowRadius: 4.65,
    elevation: 6,
    marginRight: 15
  }
})