import React, { Component } from 'react';
import { View, Text } from 'native-base'
import _ from 'lodash'
import * as Sentry from 'sentry-expo';
import DataProvider from '../../atoms/DataProvider/DataProvider'
import FSpinner from '../../atoms/FSpinner/FSpinner'
import FRenderer from '../../../screens/widgets/FRenderer'
import { inject, observer } from "mobx-react/native"
import FIcon from '../../atoms/FIcon/FIcon'

@inject('rootStore')
@observer
class FRelatedItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      config: this.getConfig(),
      activeObject: props.data.activeObject
    };
  }

  componentDidUpdate(prevProps) {
    if (!_.isEqual(prevProps, this.props)) {
      this.setState({
        config: this.getConfig(),
        activeObject: this.props.data.activeObject,
        relatedItems: this.getRelatedItem()
      })
    }
  }

  componentDidMount() {
    this.setState({
      activeObject: this.props.data.activeObject,
      relatedItems: this.getRelatedItem()
    })
  }

  // getData(){
  //     try {
  //         const { config } = this.props

  //     }catch(err){
  //         Sentry.Native.captureException(new Error("Not able to get data inside a component"), {
  //             logger: 'FRelatedItems',
  //             context: 'getData'
  //           });
  //     }
  // }

  getConfig() {
    try {
      return this.props.config
    } catch (err) {
      // Sentry.captureException(new Error("Not able to get config from component."), {
      // Sentry.Native.captureException(new Error("Not able to get config from component."), {
      //     logger: 'FRelatedItems',
      //     context: 'getConfig'
      //   });
    }
  }

  getRelatedItem() {
    try {
      return this.props.config.hasOwnProperty("relatedItems")
        ? this.props.config.relatedItems
        : ""
    } catch (err) {
      // Sentry.captureException(new Error("Not able to get related Items."), {
      // Sentry.Native.captureException(new Error("Not able to get related Items."), {
      //     logger: 'FRelatedItems',
      //     context: 'getRelatedItem'
      //   });
    }
  }

  render() {
    const { config, relatedItems } = this.state
    const { data } = this.props
    const { activeObject } = data

    return (
      <View>
        <DataProvider
          initAt={activeObject.attributes.type}
          UID={activeObject.Id}
          config={config}
          render={data => {
            const { loginStore } = this.props.rootStore
            const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
            const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
            const WOLID = loginStore.getPrefixedFieldName("Work_Order_Line_Item_Detail__c")

            let ordererWolis

            const WOLIDs = this.props.rootStore.loginStore.data[WOLID]

            if (!_.isEmpty(data.payload)) {

              let ips = _.uniq(data.payload[WOLI].map(woli => woli[IP]))

              ordererWolis = ips.map(ip => {
                return data.payload[WOLI].filter(WOLI => WOLI[IP] === ip)
              })


            }
            return data.loading
              ? <FSpinner />
              : ordererWolis.map((woliArray, i) => {

                if (_.isEmpty(woliArray)) {
                  return <View />
                } else {
                  return (
                    <View>
                      <Text>{_.isArray(data.payload[IP]) ? data.payload[IP].find(ip => ip.Id === _.first(woliArray)[IP]).Name : data.payload[IP].Name}</Text>
                      {
                        woliArray.map((woli, i) => {
                          let filteredWolids = WOLIDs.filter(WOLID => WOLID[WOLI] === woli.Id)
                          return (
                            <View style={{ marginLeft: 12, alignItems: 'stretch' }}>
                              <View key={i}>
                                <View>
                                  <Text>{woli.Name}</Text>
                                </View>
                                <View style={{ marginLeft: 12 }}>
                                  {
                                    filteredWolids.map((WOLID, i) => {
                                      return (
                                        <View>
                                          <Text>{WOLID.Name} {WOLID.Value__c}</Text>
                                        </View>
                                      )
                                    })
                                  }
                                </View>

                              </View>


                            </View>
                          )
                        })
                      }
                    </View>
                  )
                }

              })




            const filteredWolids = WOLIDs.filter(WOLID => WOLID[WOLI] === item.Id)
            return (
              <View key={key}>
                <View>
                  <Text>{item.Name}</Text>
                </View>
                {
                  !_.isEmpty(filteredWolids) &&
                  filteredWolids.map((WOLID, i) => <Text key={i}>{WOLID.Name}</Text>)
                }
              </View>
            )

          }}
        />
      </View>
    );
  }
}

FRelatedItems.defaultProps = {

  config: {},

  itemToList: "",

  groupedBy: "",

  data: {
    activeObject: {
      "attributes": {
        "type": "FIELDBUDDY__Work_Order__c",
        "url": "/services/data/v43.0/sobjects/FIELDBUDDY__Work_Order__c/a0j3I0000008T3MQAU"
      },
      "Id": "a0j3I0000008T3MQAU",
      "Name": "WO-001138",
      "CreatedDate": "2019-11-19T10:09:32.000+0000",
      "FIELDBUDDY__Status__c": "CLOSED",
      "T_Start_Time__c": "2019-11-20T10:09:00.000+0000",
      "T_End_Time__c": "2019-11-20T15:09:00.000+0000",
      "FIELDBUDDY__Comments_from_Backoffice__c": "Please Fix the Modem",
      "FIELDBUDDY__Comments_from_Technician__c": "Trui",
      "FIELDBUDDY__Closure_Status__c": "FOLLOW_UP",
      "FIELDBUDDY__Generate_WO_PDF_Email_Attachment__c": false,
      "FIELDBUDDY__Style__c": "info2 fb-icon-white fb-header-blue",
      "FIELDBUDDY__Send_WO_PDF_Email_To_Customer__c": true,
      "Approved_by__c": "Rrt",
      "FIELDBUDDY__Location__c": "a0D3I0000006ieiUAA",
      "FIELDBUDDY__Template__c": "Location",
      "UserRecordAccess": {
        "attributes": {
          "type": "UserRecordAccess"
        },
        "HasDeleteAccess": true,
        "HasEditAccess": true,
        "HasTransferAccess": true,
        "MaxAccessLevel": "All"
      }
    }
  }


}

export default FRelatedItems;