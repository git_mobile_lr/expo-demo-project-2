import { Alert, Dimensions } from 'react-native';
import Constants from 'expo-constants'
import { Platform } from 'react-native';
import _ from 'lodash'
import * as Device from 'expo-device';
export const DEVICE_WIDTH = Dimensions.get('window').width;
export const DEVICE_HEIGHT = Dimensions.get('window').height;
export const IS_TABLET = (DEVICE_WIDTH > 600)
export const IS_MOBILE = (DEVICE_WIDTH <= 600)

const IPHONE12 = 844;
const IPHONE12_PRO = 844;
const IPHONE12_PRO_MAX = 926;



export const IS_SMALL_DEVICE = (DEVICE_WIDTH < 375)
export const FONTS = {
    PRIMARY: "Roboto"
}
export const COLORS = {
    PRIMARY: "#17B1FE",
    SECONDARY: "#628FAE",
    LIGHTPRIMARY: "#DAE8F2",
    LIGHTGRAY: "#F8F8F8",
    GRAY: "#BCC8CE",
    DARKGRAY: "#A9ABAC",
    FONTS_PRIMARY: '#0D3754',
    GREY1: "#F7F9FA",
    GREY2: "#DAE8F2",
    GREY3: "#E9EEF1",
    GREY4: "#f0f0f0"
}

export const ORIENTATION = () => {
  const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
  };
  return isPortrait() ? 'portrait' : 'landscape'
}



export const IS_IPHONE_12_ISH = () => {
    return false
    try {
      // if (__DEV__) {
        return false
      // }
    
      if (Platform.OS === 'ios') {
          return (DEVICE_HEIGHT === IPHONE12 ||
              DEVICE_HEIGHT === IPHONE12_PRO ||
              DEVICE_HEIGHT === IPHONE12_PRO_MAX)
      }
    } catch (error) {
      //
    }
    return false;
  }
