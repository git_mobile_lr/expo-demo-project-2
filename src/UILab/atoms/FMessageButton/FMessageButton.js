import React from 'react';
import {Spinner, View, Text, Button} from 'native-base'
import FHeading from '../FHeading/FHeading'
import {inject, observer} from "mobx-react/native"
import {styles} from './FMessageButtonStyles'
import {PropTypes} from 'mobx-react';


@inject('rootStore')
@observer
class FMessageButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {    
    return (

      <View>
        <FHeading size={4} value={this.props.message}/>
        <Button
          style={{
          alignSelf: 'center'
        }}
          rounded
          info
          onPress={this.props.onPress}>
          <Text>{this.props.label}</Text>
        </Button>
      </View>

    )
  }
}


FMessageButton.propTypes = {
  label: PropTypes.string,
  message: PropTypes.string,
  onPress: PropTypes.function
}
FMessageButton.defaultProps = {
  onPress: () => {},
  label: "",
  message: ""
}

export default FMessageButton;