import React, { Component } from 'react';
import {inject, observer} from "mobx-react/native"
import _ from 'lodash'

@inject('rootStore')
@observer
class StyleProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    componentDidUpdate(prevProps){
        if (!_.isEqual(prevProps,this.props)){
            this.setState(this.getStyles())
        }
      }
    
    componentDidMount(){
        this.setState(this.getStyles())
    }
    
    getStyles(){
        const {style,component} = this.props
        switch (component) {
            case "FCounter": return {text:{color:"white",alignSelf:"center"}}
            default:
        }
    }

    render() {
        return this.props.render(this.getStyles())
    }
}

StyleProvider.defaultProps = {
    style : "default",
    component : "",
    render : ()=>{}
}

export default StyleProvider;