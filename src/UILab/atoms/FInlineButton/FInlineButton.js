import React from 'react';
import { styles } from './FInlineButtonStyles'
import { TouchableOpacity } from 'react-native'
import * as Animatable from 'react-native-animatable';
import { Icon, Button, Text, View } from 'native-base'
import { inject, observer } from "mobx-react/native";

//@inject('loginStore')
@inject("rootStore")
@observer
class FInlineButton extends React.Component {

    state = {
    }

    onShowPanel = () => {

    }

    getCorrectIcon() {
        const { add, minus, search, clear, onPress, text, inactive, green } = this.props
        if (add) {
            return (<Icon
                style={styles.serviceItemPlus}
                ios="ios-add-circle"
                android="md-add-circle" />)
        }

        if (minus) {
            return (<Icon
                style={styles.serviceItemPlus}
                ios="ios-remove-circle"
                android="md-remove-circle" />)
        }

        if (search) {
            return (
                <Button transparent onPress={onPress}>
                    <Text style={styles.searchButton}>
                        {this.props.rootStore.loginStore.findLabel("${Label.Search}")}
                    </Text>
                </Button>)
        }


        if (clear) {
            return (
                <Button transparent onPress={onPress}>
                    <Text>
                        {this.props.rootStore.loginStore.findLabel("${Label.Clear}")}
                    </Text>
                </Button>)
        }

        if (text) {
            return (
                <TouchableOpacity onPress={onPress} >
                    <View style={inactive ? styles.buttonTextInactive : styles.buttonText}>
                        {this.props.children}
                    </View>
                </TouchableOpacity>)
        }

    }


    render() {
        const { onPress, accessibilityLabel, testID } = this.props
        return (
            <TouchableOpacity
                style={styles.button}
                onPress={onPress}
                accessibilityLabel={accessibilityLabel} testID={testID}>
                <Animatable.View
                    animation="fadeInLeft"
                    duration={150}
                    useNativeDriver>
                    {this.getCorrectIcon()}

                </Animatable.View>
            </TouchableOpacity>
        );
    }
}

FInlineButton.defaultProps = {
    inactive: false,
    text: false,
    add: false,
    minus: false,
    search: false,
    clear: false,
    green: false,
    accessibilityLabel : "",
    testID : "",
    onPress: () => { }
}

export default FInlineButton;