import {StyleSheet} from 'react-native'
export const styles = StyleSheet.create({
    button:{
      marginLeft:10,
      marginBottom:5
    },
    serviceItemPlus: {
        fontFamily: 'Ionicons',
        fontWeight: 'bold',
        fontSize: 30,
        color: '#00AAFF',
        letterSpacing: 0,
        lineHeight: 40,
        justifyContent: 'center',
      },
    searchButton:{
      color: 'black',
      fontSize: 16,
      fontFamily: "Ionicons",
      letterSpacing: 1.8
    },
    buttonText:{
      shadowColor: '#00AAFF',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.2,
      shadowRadius: 2,
      elevation: 1,
      paddingLeft:10,
      paddingRight:10,
      paddingTop:5,
      paddingBottom:5,
      backgroundColor: '#00AAFF',
      borderRadius:20,
      color:'white',
      borderWidth:0.5,
      borderColor:'#00AAFF',
      fontFamily: "Ionicons",
      letterSpacing: 1.8
    },
    buttonTextInactive:{
      justifyContent:"center",
      alignItems:"center",
      shadowColor: '#00AAFF',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.2,
      shadowRadius: 2,
      elevation: 1,
      paddingLeft:10,
      paddingRight:10,
      paddingTop:5,
      paddingBottom:5,
      backgroundColor: 'white',
      borderRadius:20,
      color:'#00AAFF',
      borderWidth:0.5,
      borderColor:'#00AAFF',
      fontFamily: "Ionicons",
      letterSpacing: 1.8
    }
})