import React, {Component} from 'react'
import { List, ListItem, Text} from 'native-base'
import {ScrollView} from 'react-native'

class FErrorList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          errorList : props.errorList
        };
      }
    render() {
        return (
            <ScrollView>
            <List>
                {this.props.errorList.map(error=>{
                    return (
                        <ListItem>
                            <Text>{error}</Text>
                        </ListItem>
                    )
                })}
          </List>
          </ScrollView>
        );
    }
}

FErrorList.defaultProps = {
    errorList : []
}

export default FErrorList;