import React from 'react';
import { Platform,TouchableHighlight } from 'react-native'
import { Form, Picker, Label, Icon, View, Text, Header, Button, Title, Item } from 'native-base'
import { styles } from './FPickListStyles'
import FModal from '../../../molecules/FModal/FModal'
import FList from '../../../molecules/FList/FList'
import _ from 'lodash'



class FPickList_Dependent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      isModalVisible: props.isEditing,
      label: props.label,
      valueToDisplay: "",
      picklistvalues: props.picklistvalues,
      searchBar: props.searchBar
    };
  }

  componentDidUpdate(prevProps){
    if (!_.isEqual(prevProps,this.props)){
      // LISTEN TO PROPS CHANGES
      this.setState({isModalVisible:this.props.isEditing})
    }
  }

  handleChange = (value) => {
    try {
      this.setState({
        value: value.value,
        valueToDisplay: value.label,
        isModalVisible: false,
      })
      this.props.onChange(value.value)
      this.props.onSubmit(value.value)
      this.props.toggleIsEditing()
    } catch (err) {
       
    }
  }

  handleBack = () => {
    this.setState({isModalVisible:false})
    this.props.toggleIsEditing()
  }


  getValueToDisplay () {
    if (this.props.hasOwnProperty("picklistvalues")){
      return this.props.picklistvalues.reduce((acc,option)=>{return (option.value===this.state.value) ? option.label : acc },"")
    }
    return this.state.value
    
  }

  capitalize(label) {
    return label.charAt(0).toUpperCase() + label.slice(1);
  }

  labelToDisplay() {
    const { label, valueToDisplay } = this.state
    if (valueToDisplay === '') {
      return this.capitalize(label)
    }
    return valueToDisplay
  }

  render() {
    const { loginStore } = this.props.rootStore
    const { label, isModalVisible, picklistvalues} = this.state

    return (
        <Item stackedLabel style={_.isEmpty(picklistvalues) ? styles.FInput : styles.picklist}>
        <Label style={styles.picklistLabel}>{loginStore.getLabel(label)}</Label>
        <Item style={styles.FModal}>
          <FModal
            onBack={this.handleBack}
            label={label}
            isModalVisible={isModalVisible}
            value={this.getValueToDisplay()}
            header={this.capitalize(label)}
          >
            <FList
              onSelectOption={this.handleChange}
              list={this.state.list}
              customList={picklistvalues}
              searchBar={false}
            />
          </FModal>
        </Item>
      </Item>

      
    )
  }
}

FPickList_Dependent.defaultProps = {
  onChange: () => { },
  onSubmit:()=>{},
  value: "",
  label: "$Label.PickListLabelDefault",
  picklistvalues: [],
  isEditing: false
};

export default FPickList_Dependent;
