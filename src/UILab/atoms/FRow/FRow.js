import React, { Component } from 'react';
import { View } from 'native-base'
import FRenderer from '../../../screens/widgets/FRenderer'
import { styles } from './FRowStyles'
import _ from "lodash"

class FRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            columns : this.getColumns(),
            item : this.getItems()
        };
    }

    componentDidUpdate(prevProps){
        if (!_.isEqual(prevProps.data,this.props.data)){
          this.setState({data:this.props.data})
        }
      }

    getColumns(){
        const {config} = this.props
        return config.hasOwnProperty("columns")
            ? config.columns
            : []
    }

    getItems(){
        const {config} = this.props
        return config.hasOwnProperty("item")
            ? config.item
            : []
    }

    render() {
        const {item,columns,data} = this.state
        const { UID } = this.props
        return (
            <View style={styles.rowContainer}>
                {
                    columns.map((columnWidth,index)=>{
                        return (
                            <View style={{flex:columnWidth,justifyContent:"center"}}> 
                                <View style={{justifyContent:"center"}}>
                                    <FRenderer data={{ "UID": UID }} config={item[index]}/>
                                </View>
                            </View>
                        )
                    })
                }
            </View>
        );
    }
}

FRow.defaultProps = {
    UID : "",
    config : {},
    data:{}
}

export default FRow;