import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    rowContainer : {
        flex:1,
        flexDirection: "row",
        justifyContent: "center"
    }
})