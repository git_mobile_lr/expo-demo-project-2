import React, { useRef, useState, useEffect } from 'react'
import { Animated, View, Text, StyleSheet } from 'react-native'

const FProgressBar = ({ step, steps, height, title, displayHeader }) => {
  const [width, setWidth] = useState(0)
  const animatedValue = useRef(new Animated.Value(-1000)).current
  const reactive = useRef(new Animated.Value(-1000)).current

  useEffect(() => {
    // set initial
    Animated.timing(animatedValue, {
      toValue: reactive,
      duration: 300,
      useNativeDriver: true
    }).start()
  }, [])

  useEffect(() => {
    // set Percentage
    let current = steps
    if (step < steps) {
      current = step
    }
    reactive.setValue(-width + width * current / steps)
  }, [step, width])

  const renderHeader = title => {
    return (
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 5 }}>
        <Text>{title}</Text>
        <Text>{step} / {steps}</Text>
      </View>
    )
  }

  return (
    <>
      {displayHeader && renderHeader(title)}
      <View style={{
        height,
        borderRadius: height,
        overflow: 'hidden',
        backgroundColor: '#E8F6FE'
      }}>
        <Animated.View
          onLayout={e => {
            const newWidth = e.nativeEvent.layout.width
            setWidth(newWidth)
          }}
          style={{
            height,
            width: '100%',
            borderRadius: height,
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
            backgroundColor: '#00AAFF',
            position: 'absolute',
            left: 0,
            top: 0,
            transform: [{
              translateX: animatedValue
            }]
          }}>
          </Animated.View>
      </View>
    </>
  )
}

export default FProgressBar
