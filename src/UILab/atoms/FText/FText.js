import React, {Component} from 'react';
import styles from './FTextStyles'
import { Text } from 'native-base'
import {inject, observer} from "mobx-react/native"
import _ from 'lodash'

@inject('rootStore')
@observer
class FText extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type:this.defineType()
        };
    }

    defineType(){
        const { field } = this.props
        let lowerField = field.toLowerCase()
        if (lowerField.includes("quantity")) return "QUANTITY"
        if (lowerField.includes("price")) return "PRICE"
        return "TEXT"
    }

    getValueFormatted(){
        const { value, formatted } = this.props
        const { type } = this.state
        const { currency } = this.props.rootStore.localizeStore
        if (formatted) {
            switch (type) {
                case "QUANTITY":
                    return value + " x "
                case "PRICE":
                    return value + " " + currency
                case "TEXT":
                default:
                    return value
            }
        }
    }

    render() {
        const VALUE = this.getValueFormatted()
        const {value} = this.props
         
        return (
            <Text>{ _.isEmpty(value)? "" : VALUE}</Text>
        )
    }
}

FText.defaultProps = {
    value : "",
    field: "Name",
    formatted: false
}

export default FText;