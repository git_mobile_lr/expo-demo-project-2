import {StyleSheet, Platform} from 'react-native'
import Constants from 'expo-constants'
const Device = require('react-native-device-detection')

export const styles = StyleSheet.create({
    actionButton:{
        marginLeft: -45
    },
    confirmButton:{
        color:'white',
    },
    backButton:{
        color:'black',
    },
    addButton : {
        color:'white',
        fontSize: Device.isTablet? 50 : 20,
    },
    scanButton : {
        color:'white',
        fontSize: Device.isTablet? 50 : 20,
    },
    actionButtonIcon:{}
})
