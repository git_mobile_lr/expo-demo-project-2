import React from 'react';
import ActionButton from 'react-native-action-button'
import {IS_TABLET} from '../../../constants/Layout'
import {styles} from './FActionButtonStyles'
import {Icon} from 'native-base'
import PropTypes from 'prop-types';
import {inject, observer} from "mobx-react/native"
import {View, Platform} from 'react-native'
import _ from 'lodash'

//@inject('loginStore')
@inject('rootStore')
@observer
class FActionButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const {loginStore} = this.props.rootStore
    const appSettings = loginStore.getAppSettings()
    if (appSettings) {
      appSettings.rules
        ? checkGlobalPrivileges = appSettings
          .rules
          .hasOwnProperty("privileges")
          ? appSettings.rules.privileges.checkPrivileges
          : false
        : checkGlobalPrivileges = false
      this.setState({checkGlobalPrivileges})
    }

  }

  handlePress = () => {
    const {validation, onPress, event} = this.props
    const {loginStore} = this.props.rootStore
    if (this.state.checkGlobalPrivileges) {
      if (loginStore.hasPrivileges(event)) {
        if (!validation) {
          onPress()
        } else {
          alert(loginStore.getLabelorDefault("${Label.ValidationErrors}", "Validation Errors"))
        }
      }
    } else {
      if (!validation) {
        onPress()
      } else {
        alert(loginStore.getLabelorDefault("${Label.ValidationErrors}", "Validation Errors"))
      }
    }
  }

  render() {
    const {loginStore} = this.props.rootStore

    const {
      isDrawerBusy,
      isRestoring,
      isSyncing,
      isTriggerReload,
      inFlight,
      inFlightQuickSync,
      syncId
    } = this.props.rootStore.loginStore


    if (isRestoring || isTriggerReload) {
      return (<View />)
    }

    if (inFlight && !inFlightQuickSync) {
      return (<View />)
    }

    if (this.state.checkGlobalPrivileges) {
      const appSettings = loginStore.getAppSettings()
      if (appSettings) {
        if (appSettings.rules && appSettings.rules.privileges) {
          if (appSettings.rules.privileges.hidePrivilegesUI) {
            return <View/>
          }
        }
      }

    }
    
    return <ActionButton
      hideShadow={false}
      shadowStyle={{ elevation: 8}}
      fixNativeFeedbackRadius={true}
      accessibilityLabel={this.props.back
        ? "FActionButton Back"
        : this.props.save
          ? "FActionButton Save"
          : this.props.add
            ? "FActionButton Add"
            : this.props.summary
              ? "FActionButton Summary"
              : this.props.scan
                ? "FActionButton Scan"
                : "FActionButton Confirm"}
      testID={this.props.back
        ? "FActionButton Back iOS"
        : this.props.save
          ? "FActionButton Save iOS"
          : this.props.add
            ? "FActionButton Add iOS"
            : this.props.summary
              ? "FActionButton Summary iOS"
              : this.props.scan
                ? "FActionButton Scan iOS"
                : "FActionButton Confirm iOS"}
      size={loginStore.isTablet
      ? 100
      : 56}
      position={this.props.left
      ? "left"
      : "right"}
      offsetY={this.props.offsetY}
      offsetX={this.props.offsetX}
      onPress={this.handlePress}
      buttonColor={this.props.warning || this.props.red
      ? "red"
      : this.props.validation
        ? "#B8C4CC"
        : this.props.back
          ? "white"
          : this.props.orange
            ? "#EFA854"
            : this.props.disabled
              ? "lightgray"
              : this.props.offline
              ? "lightgray"
              : "rgba(0,170,255, 1)"}


      renderIcon={active => <Icon
        type={this.props.type ? this.props.type : "Ionicons"  }
      ios={this.props.save
      ? "ios-checkmark"
      : this.props.next
        ? "ios-arrow-forward"
        : this.props.back
          ? "ios-arrow-back"
          : this.props.add
            ? 'ios-add'
            : this.props.scan
              ? "ios-barcode"
              : this.props.summary
              ? "clipboard"
              : this.props.offline
              ? "ios-airplane"
              : "ios-checkmark"}
      android={this.props.save
      ? "md-checkmark"
      : this.props.next
        ? "md-arrow-forward"
        : this.props.back
          ? "md-arrow-back"
          : this.props.add
            ? 'md-add'
            : this.props.scan
              ? "md-barcode"
              : this.props.summary
              ? "clipboard"
              : this.props.offline
              ? "md-airplane"
              : "md-checkmark"}
      style={this.props.back
      ? styles.backButton
      : this.props.save
        ? styles.confirmButton
        : this.props.add
          ? styles.addButton
          : this.props.scan
            ? styles.scanButton
            : styles.confirmButton}/>}>
      {this.props.children}
    </ActionButton>
  }
}

FActionButton.propTypes = {
  /** Function to call when the button is pressed */
  //onPress: PropTypes.function,

  /** Render as disabled*/
  disabled: PropTypes.bool,
  /** Render as offine*/
  offline: PropTypes.bool,
  /** Render a SAVE button */
  save: PropTypes.bool,

  /** Render a PLUS button */
  add: PropTypes.bool,

  /** Render a BACK button */
  back: PropTypes.bool,

  /** Render a NEXT button */
  next: PropTypes.bool,

  /** If is set to true the color of the button will be RED */
  warning: PropTypes.bool,

  /** Aligns the button to the left */
  left: PropTypes.bool,

  /** Aligns the button in the middle of the screen*/
  middle: PropTypes.bool,

  /** If this option is true then the ActionButton will take into account the targets/eventType/meta to decide if the action can be executed and if the button is displayed */
  privileges: PropTypes.bool,

  /** Type of the event. This is going to take into account in the privileges logic */
  eventType: PropTypes.string,

  /**  Determines if it should render a disabled button. This option is usefull to show if the form state is valid */
  validation: PropTypes.bool,

  /** Set the color of the button to #EFA854 */
  orange: PropTypes.bool,

  /** Set the color of the button to red */
  red: PropTypes.bool,

  /**  Event received from the config */
  event: PropTypes.shape({
    privileges: PropTypes.bool,
    eventType: PropTypes.oneOf([`createable`, `deletable`, `accessible`, `updateable`, ""]),
    targets: PropTypes.arrayOf(PropTypes.string) //Sales force fields
  })
}

FActionButton.defaultProps = {
  onPress: () => {},
  red: false,
  scan: false,
  disabled: false,
  offline: false,
  save: false,
  add: false,
  back: false,
  next: false,
  left: false,
  middle: false,
  warning: false,
  validation: false,
  orange: false,
  offsetY: 20,
  offsetX: 30,
  event: {
    privileges: false,
    eventType: "",
    targets: ["field_default"]
  }
}

export default FActionButton;