import React from 'react';
import ActionButton from 'react-native-action-button'
import {IS_TABLET} from '../../../constants/Layout'
import {styles} from './FActionButtonStyles'
import {Icon,Button} from 'native-base'
import PropTypes from 'prop-types';
import { inject, observer } from "mobx-react/native"
import {View,TouchableOpacity} from 'react-native'
import { FontAwesome, FontAwesome5 } from '@expo/vector-icons';


//@inject('loginStore')
@inject('rootStore')
@observer
class FActionButtonItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    
    handlePress = () => {
        const {validation, onPress, event} = this.props
        const {loginStore} = this.props.rootStore
        const appSettings = loginStore.getAppSettings()
        if (appSettings) {
            appSettings.hasOwnProperty("rules")
                ? checkGlobalPrivileges = appSettings.rules.hasOwnProperty("privileges") ? appSettings.rules.privileges.checkPrivileges : false
                : checkGlobalPrivileges = false
                if (checkGlobalPrivileges){
                    if (loginStore.hasPrivileges(event)){
                        if(!validation){
                            onPress()
                        }else{
                            alert(loginStore.getLabelorDefault("${Label.ValidationErrors}","Validation Errors"))
                        }
                    }
                }else{
                    if(!validation){
                        onPress()
                    }else{
                        alert(loginStore.getLabelorDefault("${Label.ValidationErrors}","Validation Errors"))
                    }
                }
        }
        
    }

    getIcon(name){
        const { loginStore } = this.props.rootStore
        const appSettings = loginStore.getAppSettings()

        return appSettings.hasOwnProperty("rules")
        ? appSettings.rules.hasOwnProperty("items")
            ? appSettings.rules.items.hasOwnProperty("icons")
                ? loginStore.getParameterCaseInsensitive(appSettings.rules.items.icons, name)
                    ? loginStore.getParameterCaseInsensitive(appSettings.rules.items.icons, name)
                    : name
                :name
            : name
        : name
    }

    render() {
        const {icon,color,rootStore, fontAwesome5} = this.props
        const {loginStore} = rootStore
        const {isTablet} = loginStore
        return <TouchableOpacity 
                 onPress={this.handlePress}
                 accessibilityLabel={icon + " Button"}
                 testID={icon + " Button iOS"}>
                     {!fontAwesome5 && <FontAwesome 
                        name={this.getIcon(icon)} 
                        size={isTablet? 25 :20} 
                        style={{fontSize:isTablet?25:20,color:color,padding: isTablet? 25 : 15}} />}
                     {fontAwesome5 && <FontAwesome5 
                        name={this.getIcon(icon)} 
                        size={isTablet? 25 :20} 
                        style={{fontSize:isTablet?25:20,color:color,padding: isTablet? 25 : 15}} />}
                </TouchableOpacity>

    }
}

FActionButtonItem.propTypes = {
    /** icon to render from Font Awesome  */
    icon: PropTypes.string,

    /** color of the icon*/
    color: PropTypes.string,

    /**  Event received from the config */
    event:PropTypes.shape({
        privileges: PropTypes.bool,
        eventType: PropTypes.oneOf([`createable`, `deletable`,`accessible`,`updateable`,""]),
        targets: PropTypes.arrayOf(PropTypes.string) //Sales force fields
      })
}

FActionButtonItem.defaultProps = {
    icon: "ios-checkmark",
    color: "white",
    event:{
        privileges:false,
        eventType:"",
        targets:["field_default"]
    }
}


export default FActionButtonItem;