import React from 'react';
import { View, Image, Text, ActivityIndicator, StyleSheet } from 'react-native';

function FLoadingContent({ title, img }) {
  return (
    <>
      <ActivityIndicator
        style={styles.centering}
        color="#1EB1F8"
        size="large"
        animating={true}
      />
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20
        }}
      >
        <View 
          style={{
            alignItems: 'center',
            padding: 20,
            justifyContent: 'center'
          }}
        >
          <View 
            style={{
              alignItems: 'center',
              padding: 10,
              justifyContent: 'center',
            }}
          >
            <Image source={img} />
          </View>
          <Text 
            style={{
              marginTop: 20,
              fontFamily: 'Roboto',
              fontSize: 17,
              color: 'rgba(13,55,84, 0.87)',
              textAlign: 'center'
            }}
          >
            {title}
          </Text>
        </View>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  centering: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default FLoadingContent