import React, { Component } from 'react';
import {inject, observer} from "mobx-react/native"
import _ from 'lodash'
import jsonQuery from 'json-query'

@inject('rootStore')
@observer
class DataProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            payload : {},
            loading : true
        };
    }

    componentDidUpdate(prevProps){
        if (!_.isEqual(prevProps,this.props)){
          this.loadData()
        }
      }

    componentDidMount(){
        this.loadData()
    }

    

    async loadData(){
        const { config , rootStore } = this.props
        const { loginStore , dataStore} = rootStore
        const WO = loginStore.getPrefixedFieldName("Work_Order__c")
        const WOLI = loginStore.getPrefixedFieldName("Work_Order_Line_Item__c")
        const WOLID = loginStore.getPrefixedFieldName("Work_Order_Line_Item_Detail__c")
        const IP = loginStore.getPrefixedFieldName("Installed_Product__c")
        const WOI = loginStore.getPrefixedFieldName("Work_Order_Item__c")
        const { UID } = this.props 
        

        let result

            try{
                if (!_.isEmpty(UID)&&!_.isEmpty(config)){
                    result = await loginStore.getDataByConfigAndUID(WO,UID, config)
                }
            }catch(err){
                 
            }
            

            if (!_.isEmpty(result)){
                if (result.hasOwnProperty("type")){
                    if (result.type==="success" && !_.isEmpty(result.data) && result.data.UID){
                            this.setState({loading:false,payload:result.data})
                    }else{
                        let onlineWorkOrders = dataStore.onlineData[WO].map(item=>item.recordData)
                        let filteredWO = onlineWorkOrders.filter(wo=>wo.Id===UID)

                        if (!_.isEmpty(filteredWO)){
                            let _payload = {}
                            _payload["UID"] = UID
                            _payload[WO] = _.first(filteredWO)
                            let _pre_payload = dataStore.onlineData[WOLI].filter(woli=>woli.recordData[WO]===UID)
                            _payload[WOLI] = _pre_payload.map(x=>x.recordData)
                            let wolisIds = _payload[WOLI].map(woli=>woli.Id)
                            _payload[WOI] = _payload[WOLI].map(woli=>dataStore.onlineData[WOI].filter(woi=>woi.recordData.Id===woli.recordData[WOI]))
                            _payload[IP] = _.uniqBy(_payload[WOLI].map(woli=>{
                                let ip = dataStore.getItemById(woli[IP],"ONLINE_DATA")
                                return _.isEmpty(ip) ? {} : ip.data
                            }),"Id")
                            _payload[WOLID] = dataStore.onlineData[WOLID].filter(wolid=>wolisIds.includes(wolid.recordData[WOLI])).map(x=>x.recordData)
                            this.setState({loading:false,payload:_payload})
                        }
                    }
                }
            }

            
                        
    }

    render() {
        return this.props.render(this.state)
    }
}

DataProvider.defaultProps = {
    UID : "",
    config : {},
    render : ()=>{}
}

export default DataProvider;