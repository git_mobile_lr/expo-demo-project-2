import React from 'react';
import { Badge } from 'react-native-elements'
import { styles } from './FBadgeStyles'
import { View, Text } from 'native-base'
import * as Animatable from 'react-native-animatable';

class FBadge extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
        
    }

    UNSAFE_componentWillReceiveProps(e){
        this.setState({value:e.value})
    }

    render() {
        return (
            <View style={styles.itemBadge}>
                <Animatable.View animation="flipInY" duration={150} useNativeDriver>                    
                    <Badge badgeStyle={styles.badgeContainer} value={this.state.value} textStyle={styles.badgeText} status="warning" />
                </Animatable.View>
            </View>)
    }
}
FBadge.defaultProps = {
    value: 0
}

export default FBadge;