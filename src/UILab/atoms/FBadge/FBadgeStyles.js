import {
    StyleSheet
} from 'react-native'

export const styles = StyleSheet.create({
    itemBadge: {
    },
    badgeContainer: {
        marginVertical: 15,
        minWidth: 50,
        minHeight: 30,
        borderRadius:15    
    },
    badgeText: {
        fontSize: 20,
        fontFamily: 'Ionicons',
        color: '#FFF'
    }
})