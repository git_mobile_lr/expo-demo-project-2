import React from 'react';
import { TouchableOpacity, NativeModules } from 'react-native'
import { styles } from './FNumberStyles'
import { Text, Card, Body, CardItem, Item, Input, Label, Icon, View } from 'native-base'
import { inject, observer } from "mobx-react/native"
import { FontAwesome } from "@expo/vector-icons"
import _ from 'lodash'
import PropTypes from 'prop-types'
import FModal from '../../molecules/FModal/FModal'
import FActionButton from '../FActionButton/FActionButton'

@inject('rootStore')
@observer
class FNumber extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value.toLocaleString(props.rootStore.loginStore.getUserInfo.language),
            type: props.type,
            label: props.label,
            name: props.name,
            showValidationErrors: true,
            validationError: false,
            isModalVisible: props.isEditing,
            precision: props.precision
        }
    }

    componentDidUpdate(prevProps) {
        if (!_.isEqual(prevProps, this.props)) {
            // LISTEN TO PROPS CHANGES
            this.setState({ isModalVisible: this.props.isEditing })
        }
    }

    componentDidMount() {
        const { validationStore } = this.props.rootStore
        this.setState({
            validationError: validationStore.validateInput(this.value, this.props.type)
        })
    }

    handleChange = (value) => {
        this.setState({ value: value })
        const { validationStore } = this.props.rootStore
        this.props.onChange(value)
        const validationResult = validationStore.validateInput(value, this.props.type)
        if (_.isEmpty(validationResult)) {
            this.setState({ validationError: false })
        } else {
            this.setState({
                validationError: true,
                errorMessage: validationResult[this.props.type.toLowerCase()]
            })
        }
    }

    onSave = () => {
        const { validationStore } = this.props.rootStore
        let validationResult = validationStore.validateInput(this.state.value, this.props.type)
        if (_.isEmpty(validationResult)) {
            this.props.onSubmit(this.state.value)
            this.props.toggleIsEditing()
            return true
        }
        return false
    }

    handleBack = () => {
        this.setState({ isModalVisible: false })
        this.props.toggleIsEditing()
        const { value } = this.state
        const prevValue = this.props.value
        if (prevValue !== value) {
            this.setState({ value: prevValue },
            this.props.onChange(prevValue))
        }
    }

    render() {
        const { locked, maxLength, keyboardType, inModal, type, placeholder, helpText } = this.props

        const { loginStore } = this.props.rootStore
        const { label, value, isModalVisible, validationError, precision } = this.state

        return (
            <Item stackedLabel style={styles.FInput}>
                <View style={{ alignSelf: 'flex-start' }}>
                    <Label style={styles.label}>{loginStore.getLabel(label)}</Label>
                    {!_.isEmpty(helpText) && <Text style={styles.helpText}>{helpText}</Text>}
                </View>
                <Item style={styles.FModal}>
                    <FModal
                        validationError={validationError}
                        saveButton={true}
                        onBack={this.handleBack}
                        label={placeholder}
                        type={type}
                        isModalVisible={isModalVisible}
                        value={value}
                        multiSelection={false}
                        header={label}
                        onSave={this.onSave}
                        inputInline={true}
                    >
                        <View>
                            <Item style={{ borderColor: 'transparent' }}>

                                <Input
                                    keyboardType={keyboardType}
                                    placeholder={placeholder}
                                    placeholderTextColor='#B8C4CC'
                                    placeholderStyle={styles.placeholder}
                                    name={label}
                                    maxLength={precision}
                                    onChangeText={this.handleChange}
                                    style={validationError ? styles.textError : styles.input}
                                    value={value}
                                    autoFocus={true}
                                />
                                {validationError && <Icon name='close-circle' style={{ color: 'red' }} />}
                            </Item>
                            <Item style={{ borderColor: 'transparent' }}>
                                {validationError && <Text style={styles.textError}>{loginStore.getLabel(this.state.errorMessage)}</Text>}
                            </Item>
                        </View>
                    </FModal>
                </Item>
            </Item>
        )
    }
}

FNumber.propTypes = {
    /** This function should be called to change the value of the parent component */
    //onChange: PropTypes.function,

    /** This function should be called to submit the final the value. This function is usually defined in the HOC and triggers syncs. */
    //onSubmit: PropTypes.function,

    /** Label to display above the input */
    label: PropTypes.string.isRequired,

    /** max length of the input */
    precision: PropTypes.number,

    /** If it's set to true then the user won't be able to modify its content */
    locked: PropTypes.bool,

    /** The value of the input */
    value: PropTypes.string,

    /** Set to true to force the user to fill this field */
    required: PropTypes.bool,

    /** The name of the object in Sales Force. It's used as a indentifier to send the correct value */
    name: PropTypes.string,

    /** Set the keyboard that is going to be displayed */
    keyboardType: PropTypes.string,

    /** If this option is true the input will be displayed inside of a modal. By default is true*/
    inModal: PropTypes.bool,

    /** Placeholder to display above the input */
    placeholder: PropTypes.string
}


FNumber.defaultProps = {
    onChange: () => { },
    onSubmit: () => { },
    label: "$Label.InputLabelDefault",
    precision: 8,
    value: "",
    locked: false,
    required: false,
    name: "defaultName",
    keyboardType: "default",
    showValidationErrors: true,
    inModal: true,
    type: "DOUBLE",
    isEditing: false,
    placeholder: ""
};


export default FNumber;