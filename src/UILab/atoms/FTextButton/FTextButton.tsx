import React, { Component } from 'react';
import { TouchableOpacity, Text, View } from 'react-native'
import {inject, observer} from "mobx-react/native"


@inject('rootStore')
@observer
class FTextButton extends Component {
  
  render() {
    const { text, isReadOnly, onPress, textColor, fillColor, borderColor } = this.props
    return (
      <TouchableOpacity onPress={() => onPress()} disabled={isReadOnly}>
        <View style={{  backgroundColor: fillColor, width: '100%', height: 50, borderColor: borderColor, borderRadius: 10, borderWidth: 2, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
          <Text style={{ color: textColor, fontWeight: '400', fontSize: 25 }}>
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

FTextButton.defaultProps = {
  config: {}
}

export default FTextButton;