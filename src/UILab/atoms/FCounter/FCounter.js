import React, { Component } from 'react';
import { View, Text } from 'native-base'
import DataProvider from '../DataProvider/DataProvider'
import StyleProvider from '../StyleProvider/StyleProvider'
import FSpinner from '../FSpinner/FSpinner'
import _ from 'lodash'
import { inject, observer } from "mobx-react/native"

@inject('rootStore')
@observer
class FCounter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            extraText : this.getExtraText(),
            pathToCount : this.pathToCount(),
            style: this.getStyle()
        };
    }

    getExtraText(){
        const {config} = this.props
        return config.hasOwnProperty("extraText")
            ? config.extraText
            : []
    }

    pathToCount(){
        const {config} = this.props
        return config.hasOwnProperty("pathToCount")
            ? config.pathToCount
            : []
    }

    getStyle(){
        const {config} = this.props
        return config.hasOwnProperty("style")
            ? config.style
            : "defaultStyle"
    }

    render() {
        const {pathToCount,extraText,style} = this.state
        const {UID,config} = this.props
        const {loginStore} = this.props.rootStore
        return (
            <StyleProvider
                style={style}
                component={"FCounter"}
                render = { styles => {
                    return <DataProvider 
                        UID = {UID}
                        config = {config}
                        render = { data => {
                            return data.loading 
                            ? <FSpinner />
                            : <Text style={styles.text}>
                                { data.payload.hasOwnProperty(pathToCount) 
                                    ? _.isArray(data.payload[pathToCount]) 
                                        ? data.payload[pathToCount].length + " " + loginStore.getLabel(extraText)
                                        : null 
                                    : null}
                            </Text>
                        }}
                    />
                }}
            />
        );
    }
}

FCounter.defaultProps = {
    UID : "",
    config : {}
}

export default FCounter;