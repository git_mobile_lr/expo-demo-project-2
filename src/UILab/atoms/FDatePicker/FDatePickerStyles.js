import {
    StyleSheet,
    Platform
} from 'react-native'
import Constants from 'expo-constants'
import {IS_TABLET,ANDROID} from '../../../screens/widgets/styles'

export const styles = StyleSheet.create({
    picker: {
        width: "100%",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        alignContent: "center",
        backgroundColor: "transparent"
    },
    item:{
        height:10,
        width:IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent'
    },
    label:{
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    }
})

export const customStyles = StyleSheet.create({
    dateText: {
        flex: 1,
        alignSelf: "flex-start",
        width: "100%",
        backgroundColor: "transparent",
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0,
        letterSpacing: 0,
        marginLeft: 0,
        paddingLeft: 0,
        paddingTop: 10
      },
      datePickerCon: {
        backgroundColor: "#FFF"
      },
      datePicker: {
        backgroundColor: "#00AAFF"
      },
      btnCancel: {
        backgroundColor: "#FFF"
      },
      btnTextCancel: {
        fontSize: 16,
        color: "#00AAFF"
      },
      btnConfirm: {
        backgroundColor: "#FFF"
      },
      btnTextConfirm: {
        fontSize: 16,
        color: "#00AAFF"
      },
      dateInput: {
        flex: 1,
        alignSelf: "flex-start",
        width: "100%",
        backgroundColor: "transparent",
        color: "#40505A",
        letterSpacing: 0,
        marginLeft: 0,
        borderWidth: 0,
        paddingLeft: 0
      }
})