import React from 'react';
import DatePicker from "react-native-datepicker";
import {Text,Item,Label} from 'native-base'
import {styles,customStyles} from './FDatePickerStyles'
import { inject, observer } from "mobx-react/native"
import moment from "moment";
import "moment/locale/nl";
import "moment/locale/pl";
import "moment/locale/uk";
import "moment/locale/it";
import "moment/locale/pt-br";

//@inject('loginStore')
@inject('rootStore')
@observer
class FDatePicker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date:props.date
        };
    }

    componentDidMount(){
       
    }

    onDateChanged = (e) => {
        this.setState({
            date:e
        })
        this.props.onChange(e)
    }

    render() {
        const {locked,label} = this.props
        const {loginStore} = this.props.rootStore
        const {date} = this.state
        return (
            <Item stackedLabel style={styles.item}>
                <Label style={styles.label}>{loginStore.getLabel(label)}</Label>
                {locked &&
                    <Text>{value}</Text>

                }
                {!locked &&
                    <DatePicker
                    style={styles.picker}
                    locale={loginStore.getUserLocale()}
                    date={date}
                    mode="date"
                    androidMode="spinner"
                    showIcon={false}
                    placeholder={label}
                    is24Hour={true}
                    //format={format}
                    onDateChange={this.onDateChanged}
                    confirmBtnText={loginStore.getLabel("${Label.Confirm}","Confirm")}
                    cancelBtnText={loginStore.getLabel("${Label.Cancel}","Cancel")}
                    customStyles={customStyles}
                  />}
            </Item>
            
        );
    }
}

FDatePicker.defaultProps = {
    onChange:()=>{},
    label:"defaultLabel",
    locked:false,
    date:moment()
}

export default FDatePicker;