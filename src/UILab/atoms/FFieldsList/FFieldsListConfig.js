export const DEFAULT_CONFIG = {
    style : "defaultStyle",
    dataBinding : [
        {
            "field": "defaultField",
            "var": "_DEFAULT_VAR_"
        }
    ],
    dataText : {
        "LINE1" : "_DEFAULT_VAR_"
    }
}