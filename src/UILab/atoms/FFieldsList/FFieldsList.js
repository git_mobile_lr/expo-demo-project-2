import React,{Component} from 'react'
import {View,Text} from 'native-base'
import { styles } from './FFieldsListStyles'
import { DEFAULT_CONFIG }  from './FFieldsListConfig'
import _ from 'lodash'
import { inject, observer } from "mobx-react/native"

@inject('rootStore')
@observer
class FFieldsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultConfig : DEFAULT_CONFIG,
            data : props.data,
            config : {...DEFAULT_CONFIG,...props.config}
        };
    }

    componentDidUpdate(prevProps) {
        const { defaultConfig } = this.state
        if (!_.isEqual(prevProps, this.props)) {
            this.setState({
                data : this.props.data,
                config : {...defaultConfig,...this.props.config}
            })
        }
    }

    componentDidMount() {
        const { defaultConfig } = this.state
            this.setState({
                defaultConfig : DEFAULT_CONFIG,
                data : this.props.data,
                config : {...defaultConfig,...this.props.config}
            })
    }

    getValue(_var_){
        const { data, config } = this.state
        const { dataBinding } = config
        let item = dataBinding.find(item=>item.var===_var_)
        if (!_.isEmpty(item)){
            if (data.hasOwnProperty(item.field)){
                return _.isEmpty(data[item.field]) || data[item.field]==undefined  ? "-" : data[item.field]
            }
        }
        return "-"
    }

    render() {
        const { config } = this.state
        if (!_.isEmpty(config)){
            const { dataText, style } = config
            return (
                <View>
                    <Text style={styles[style]}>
                            {
                                Object.values(dataText).map(_var_=> { 
                                    return this.getValue(_var_) + " "})
                            }
                    </Text>
                </View>
            );
        }else{
            return <View />
        }
        
    }
}

FFieldsList.defaultProps = {
    data : {},
    config : {}
}
export default FFieldsList;