import React from 'react';
import { inject, observer } from "mobx-react/native"
import _ from 'lodash'
import FSpinner from '../FSpinner/FSpinner'
import jsonQuery from 'json-query'

//@inject('loginStore')
@inject('rootStore')
@observer
class FLocalResource extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data:props.data,
            conditions:props.condition,
            loading: false,
            payload: []
        };
    }


    componentDidMount() {
        this.setState({ loading: true }, async ()=>{
            try{
                const {data,condition} = this.state
                let result =  await data() 
                if (result.type==='success'){
                    this.setState({
                        payload: jsonQuery(condition, {data: result.data}).value,
                        loading: false
                    })
                }else{
                    this.state({
                        payload:null,
                        loading: true
                    })
                }
            }catch(err){
                 
            }
        })
    }

    render() {
        return this.props.render(this.state)
    }
}

FLocalResource.defaultProps = {
    condition:"",
    data:null,
    render:()=>{return <FSpinner />}
}

export default FLocalResource;