import React, { Component } from 'react'
import _ from "lodash"

class MergeData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            payload : {}
        };
    }

    componentDidMount(){
        this.mergeData()
    }

    componentDidUpdate(prevProps){
        if (!_.isEqual(prevProps,this.props)){
            this.mergeData()
        }
      }

    mergeData(){
        const { source1 , source2 } = this.props
        this.setState({ loading: false,payload : _.concat(source1,source2)})
    }

    render() {
        return this.props.render(this.state)
    }
}

MergeData.defaultProps = {
    render : () => {},
    source1 : [],
    source2 : []
}


export default MergeData;