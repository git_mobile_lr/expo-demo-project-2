import React from 'react';
import {View,Text} from 'native-base'
import { inject, observer } from "mobx-react/native"
import _ from 'lodash'


//@inject('loginStore')
@inject('rootStore')
@observer
class ListItemDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }

    replacePlaceholder = (template, itemData) => {
        return template.replace(/\$\{([a-zA-Z0-9_]+)\}/g, (_, key) => {
            return itemData[key] || '';
        });
    };

    render() {
        const { item, fieldsToDisplay, layout } = this.props

        if(!layout || !_.isEmpty(layout)) {

            return (
                <View>
                    {layout && Object.keys(layout).map((key, index) => (
                        <Text key={index}>
                            {this.replacePlaceholder(layout[key], item)}
                        </Text>
                    ))}
                </View>
            )
        }
        else {
            return (
                    <View>
                        {item.hasOwnProperty("Name")? <Text>{item.Name}</Text> : null}
                        {fieldsToDisplay ? fieldsToDisplay.map(displayItem => item.hasOwnProperty(displayItem) ? <Text>{item[displayItem]}</Text> : null) : null}
                    </View>
                )
        }
    }
}

ListItemDetails.defaultProps = {
    item:{}
}

export default ListItemDetails;