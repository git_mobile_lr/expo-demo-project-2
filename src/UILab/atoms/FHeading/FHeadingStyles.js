import {StyleSheet, Platform} from 'react-native'
import {IS_TABLET,ANDROID} from '../../../screens/widgets/styles'
const visibleHeight = 300

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F7F9FA',
        height: visibleHeight
    },

    heading:{
        margin:'auto',
        padding:20,
        fontFamily: 'Roboto',
        fontSize: (IS_TABLET)? 30 : 24,
        color: '#000',
        fontWeight:"200",
        alignSelf:'center',
    },
    heading2:{
        margin:'auto',
        padding:20,
        fontFamily: 'Roboto',
        fontSize: (IS_TABLET)? 28 : 22,
        color: '#000',
        fontWeight:"200",
        alignSelf:'center',
    },
    heading3:{
        margin:'auto',
        padding:20,
        fontFamily: 'Roboto',
        fontSize: (IS_TABLET)? 24 : 20,
        color: '#000',
        fontWeight:"200",
        alignSelf:'center',
    },
    heading4:{
        margin:'auto',
        padding:20,
        fontFamily: 'Roboto',
        fontSize: (IS_TABLET)? 18 : 14,
        color: '#000',
        fontWeight:"200",
        alignSelf:'center',
    },
})
