import React from 'react'
import { Text, View } from 'native-base'
import { styles } from './FHeadingStyles'


function FHeading(props) {
    const {size} = props
    return (
        <View style={styles.headingContainer}>
            <Text style={
                    (size === 1) 
                        ?  styles.heading
                        :  (size === 2)
                            ? styles.heading2
                            : (size === 3)
                                ? styles.heading3
                                : styles.heading4
                }>
                {props.value}
            </Text>
        </View>
    )
}

FHeading.defaultProps = {
    size:1,
    value:"Default Title"
}

export default FHeading; 
