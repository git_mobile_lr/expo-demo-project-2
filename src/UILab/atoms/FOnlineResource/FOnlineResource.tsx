
import React, { Component } from 'react';
import { inject, observer } from "mobx-react/native"
import _ from 'lodash'


type Props = {
  type: "RELATED_RECORDS" | "SEARCH"
  relatedRecord?: any
  commonParentsPath?: any
  table: string
  fieldsToFilter?: Array<string>
  fieldsToRequest?: Array<string>
  valueToSearch?: any
  condition?: any
  rootStore?: any
  limit?: number
  mode?: 'strictlyEqual' | 'contains'
  onFail?: 'reAuth' | 'nothing'
  relatedRecords?: any
  onPayloadChanged?: any
  skipRecordsFromBriefcase?: boolean
  render: (state: State) => JSX.Element
}

type State = {
  loading: boolean
  payload: Array<any>
  offline: boolean
}

@inject('rootStore')
@observer
class FOnlineResource extends Component<Props, State>{
  constructor(props: Props) {

    super(props);
    this.state = {
      loading: true,
      payload: [],
      offline: false

    };
  }

  async componentDidMount() {
    const { loginStore } = this.props.rootStore

    if (loginStore.getIsOffline) {
      this.setState({ offline: true, loading: false })
    } else {
      try {
        await this._loadAsync()
      } catch (err) {

      }

    }

  }

  componentDidUpdate(prevProps: Props) {

    if (this.props.type === "RELATED_RECORDS") {

      const { relatedRecord, commonParentsPath, table } = this.props
      if ((!_.isEqual(prevProps.relatedRecord, relatedRecord)) || (!_.isEqual(prevProps.commonParentsPath, commonParentsPath)) || (!_.isEqual(prevProps.table, table))) {

        this.setState({
          loading: true,
          payload: []
        }, () => {
          this._loadAsync()
        })
      }
    }

    if (this.props.type === "SEARCH") {
      const { table, fieldsToFilter, fieldsToRequest, valueToSearch, condition } = this.props
      if ((!_.isEqual(prevProps.table, table)) || (!_.isEqual(prevProps.fieldsToFilter, fieldsToFilter)) || (!_.isEqual(prevProps.fieldsToRequest, fieldsToRequest)) || (!_.isEqual(prevProps.valueToSearch, valueToSearch)) || (!_.isEqual(prevProps.condition, condition))) {
        this.setState({
          loading: true,
          payload: []
        }, () => {
          this._loadAsync()
        })
      }
    }

  }

  _isOffline() {
    return this.props.rootStore.loginStore.getIsOffline
  }

  componentWillUnmount() {

  }



  async _loadAsync() {


    this.setState({ loading: true, payload: [] })

    const { loginStore, dataStore } = this.props.rootStore

    const {
      table,
      limit,
      fieldsToFilter,
      fieldsToRequest,
      valueToSearch,
      mode,
      condition,
      relatedRecords,
      commonParentsPath,
      type,
      onPayloadChanged,
      skipRecordsFromBriefcase
    } = this.props



    try {


      if (this._isOffline()) {
        this.setState({ offline: true, loading: false })
        return;
      }

      if (type === "RELATED_RECORDS") {
        if (!_.isEmpty(table) && !_.isEmpty(relatedRecords) && !_.isEmpty(commonParentsPath)) {

          let result = await loginStore.requestRelatedRecords(table, relatedRecords, commonParentsPath)


          if (result.type === "success") {



            let onlineDataFormatted = {}
            for (const [key, value] of Object.entries(result.data.payload)) {
              if (!_.isEmpty(value) && _.isArray(value)) {
                onlineDataFormatted[key] = value.map((results: any) => results.recordData ? results.recordData : null)
              }
            }

            dataStore.addOnlineData(onlineDataFormatted)
            this.setState({ loading: false, payload: result.data.payload }, () => {
              if (onPayloadChanged) {
                onPayloadChanged(this.state.payload.length)
              }
            })
            

          } else {
            if (result.type == "error") {
              loginStore.navigate("ReAuth", {}, true)
            }
            this.setState({ loading: true, payload: [] })
          }
        } else {
          this.setState({ loading: false, payload: [] })
        }
      }

      if (type === "SEARCH") {
        
        let limitOfRecords = 100
        if (this.props.hasOwnProperty("limitOfRecords")) {
          limitOfRecords = this.props.limitOfRecords
        }

        const result2 = await loginStore.searchTable(table, valueToSearch, fieldsToFilter, fieldsToRequest, limitOfRecords, condition, skipRecordsFromBriefcase)

        if (result2.errorMessage === null) {

          let data = Object.values(result2.searchData)
          if (mode === 'contains') {
            data = Object
              .values(result2.searchData)
              .filter((item: any) => item.Name.toLowerCase().includes(valueToSearch.toLowerCase()))
          }

          let finalResult = _.take(data, limit)

          this.setState({ payload: finalResult, offline: false, loading: false }, () => {
            if (onPayloadChanged) {
              onPayloadChanged(this.state.payload.length)
            }
          })
        } else {

          const { onFail } = this.props
          switch (onFail) {
            case 'reAuth':
              loginStore.navigate("ReAuth", {}, true)
            default:
          }
        }
      }




    } catch (err) {


    }
  }

  render() {
    return this
      .props
      .render(this.state)
  }
}



//@ts-ignore
FOnlineResource.defaultProps = {
  render: () => { },
  table: "Table__c",
  limit: "10",
  fieldsToFilter: [],
  fieldsToRequest: ["Id"],
  valueToSearch: "",
  mode: 'strictlyEqual',
  onFail: 'nothing',
  condition: {},
  type: "SEARCH"
}

export default FOnlineResource;