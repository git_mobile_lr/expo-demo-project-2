
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
    faBriefcase,
    IconDefinition,
    faDotCircle,
    faPlusCircle,
    faMinusCircle,
    faMinus,
    faPlus,
    faHome,
    faComments,
    faCheck,
    faTimes,
    faExclamation,
    faCommentDots,
    faComment,
    faFile,
    faWrench,
    faCube,
    faCreditCard,
    faDatabase,
    faStop,
    faInfoCircle
} from '@fortawesome/free-solid-svg-icons'

import { styles } from './FIconStyles'
import _ from 'lodash'
import { icon, Icons } from '../../../types'


type Props = {
    config: any
    color: any
    icon: icon
    size: number
}
type State = {
    icon: IconDefinition
    color: string
}
class FIcon extends Component<Props, State> {

    static defaultProps: { size: number; icon: string; color: string; config: {}; };

    constructor(props: Props) {
        super(props);
        this.state = {
            icon: this.getIcon(),
            color: this.getColor()
        };
    }

    componentDidUpdate(prevProps: Props) {
        if (!_.isEqual(prevProps, this.props)) {
            this.setState({
                icon: this.getIcon(),
                color: this.getColor()
            })
        }
    }

    getIcon(): IconDefinition {
        const { config, icon } = this.props
        return config.hasOwnProperty("icon")
            ? this.getCorrectIconName(config.icon)
            : this.getCorrectIconName(this.props.icon)
    }

    getColor() {
        const { config } = this.props
        return config.hasOwnProperty("color")
            ? config.color
            : this.props.color
    }

    getCorrectIconName(icon: icon): IconDefinition {
        return icon === Icons.WORK
            ? faBriefcase
            : icon === Icons.DOT
                ? faDotCircle
                : icon === Icons.PLUS
                    ? faPlus
                    : icon === Icons.MINUS
                        ? faMinus
                        : icon === Icons.PLUSCIRCLE
                            ? faPlusCircle
                            : icon === Icons.MINUSCIRCLE
                                ? faMinusCircle
                                : icon === Icons.HOME
                                    ? faHome
                                    : icon === Icons.COMMENTS
                                        ? faComments
                                        : icon === Icons.WARNING
                                            ? faExclamation
                                            : icon === Icons.CHECK
                                                ? faCheck
                                                : icon === Icons.WRONG
                                                    ? faTimes
                                                    : icon === Icons.COMMENTSDOTS
                                                        ? faComment
                                                        : icon === Icons.FILE
                                                            ? faFile
                                                            : icon === "Wrench"
                                                                ? faWrench
                                                                : icon === "Cube"
                                                                    ? faCube
                                                                    : icon === "Card"
                                                                        ? faCreditCard
                                                                        : icon === Icons.DATABASE
                                                                            ? faDatabase
                                                                            : icon === Icons.BLOCK
                                                                                ? faInfoCircle
                                                                                : faBriefcase

    }

    render() {
        const { size } = this.props
        const { icon, color } = this.state
        return (
            <FontAwesomeIcon
                size={size}
                color={color}
                icon={icon}
                style={styles.icon}
            />
        );
    }
}

FIcon.defaultProps = {
    size: 18,
    icon: 'work',
    color: "#5384A6",
    config: {}


}

export default FIcon;