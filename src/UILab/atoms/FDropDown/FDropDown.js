import React, { useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'

function FDropDown({
    title,
    children,
    customHeader,
    isTablet,
    initialState,
    arrowColor,
    headerBackgroundColor,
    headerIconOpen,
    headerIconClosed,
    style 
  }) {
  const [isOpen, setISOpen] = useState(initialState ? initialState : false)

  const _arrowColor = arrowColor ? arrowColor : "#EFA854"
  const _size = isTablet ? 35 : 25

  const _getdefaultHeaderText = () => (
    <Text 
      style={{
      fontSize: 23,
      fontWeight: '500', 
      color:'#333' }}
    >
      {title}
    </Text>
  )

  return (
    <TouchableOpacity onPress={() => setISOpen(prev => !prev)} accessibilityLabel={title + " dropdown"} testID={title + " dropdown iOS"}>
      <View
        style={[
          isTablet ? styles.container : stylesTablet.container,
          { backgroundColor: headerBackgroundColor ? headerBackgroundColor : '#fff' }
        ]}
      >
        {!customHeader ? _getdefaultHeaderText() : customHeader}
        <View style={[{ paddingLeft: 10 }, style]}>
          {headerIconOpen && headerIconClosed 
            ? isOpen ? headerIconOpen : headerIconClosed
            : <FontAwesome 
                name={isOpen ? "chevron-circle-up" : "chevron-circle-down"}
                size={_size} 
                color={_arrowColor} 
              />
          }
        </View>
      </View>
      {isOpen ? children : <View/>}
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center'
  }
});

const stylesTablet = StyleSheet.create({
  container: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center'
  }
});

export default FDropDown;