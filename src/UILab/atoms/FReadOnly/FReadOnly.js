import React from 'react';
import { styles } from './FReadOnlyStyles'
import { Text, Item, Label } from 'native-base'
import { inject, observer } from "mobx-react/native"
import _ from 'lodash'
import PropTypes from 'prop-types'


//@inject('loginStore')
@inject('rootStore')
@observer
class FReadOnly extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
            label:props.label,
            valueToDisplay:props.valueToDisplay
        };
    }

    render() {
        const {loginStore} = this.props.rootStore
        const {label,valueToDisplay} = this.state
        return (
            <Item stackedLabel style={styles.FInput}>
                <Label style={styles.label}>{loginStore.getLabel(label)}</Label>
                <Text style={styles.placeholder}>{valueToDisplay}</Text>
            </Item>
        )
    }
}

FReadOnly.propTypes = {
    /** The value in SalesFroce */
    value: PropTypes.string,

    /** The value to render */
    valueToDisplay: PropTypes.string,

    /** The label to render above the value */
    label: PropTypes.string,

   

}


FReadOnly.defaultProps = {
    label: "$Label.InputLabelDefault",
    valueToDisplay:"",
    value: "",
};


export default FReadOnly;