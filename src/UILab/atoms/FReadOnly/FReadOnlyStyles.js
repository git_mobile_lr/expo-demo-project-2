import {StyleSheet, Platform} from 'react-native'
import {IS_TABLET,ANDROID} from '../../../screens/widgets/styles'

export const styles = StyleSheet.create({
    label:{
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    input:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0,
        paddingLeft:24,

    },
    textError:{
        paddingLeft:24,
        fontFamily: 'Roboto',
        fontSize: 16,
        letterSpacing: 0,
        lineHeight: 24,
        borderColor: 'transparent',
        color:'red',
    },
    FInput:{
        height:10,
        width:IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent',
        alignItems:'flex-start'
    },  
    FModal:{
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'space-between',
        height:10,
        width:'100%',
        borderColor: 'transparent'
    },
    placeholder:{
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#B8C4CC',
        paddingTop:6,
        paddingBottom:1,
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0,
        marginLeft:0  
    }
})
