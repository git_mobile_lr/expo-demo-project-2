import { StyleSheet, Platform } from 'react-native'
import { IS_TABLET, ANDROID } from '../../../screens/widgets/styles'

export const styles = StyleSheet.create({
    picklistHeader: {
        backgroundColor: "#16A7FF"
    },
    headerTitleStyle: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: "white"
    },
    picklist: {
        paddingBottom: 10,
        width: IS_TABLET ? '96%' : '92%',
    },
    headerBackButtonTextStyle: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: "white"
    },
    placeholder: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#B8C4CC',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0
    },
    filledValue: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0
    },
    pickListIcon: {
        marginRight: 0,
        marginLeft: 0,
        color: '#00AAFF'
    },
    picker: {
        width: ANDROID ? 500 : 200,
        height: 40
    },
    pickerItem: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: 'black',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0
    },
    label: {
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    FInput: {
        height: 10,
        width: IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent'
    },
    FModalIcon: {
        paddingRight: 0,
        marginRight: 0,
        marginLeft: 0,
        color: '#00AAFF'
    },
    emptyPicklist: {
        height: 50,
        width: IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent',
        paddingBottom: 20,
        backgroundColor: 'gray'
    },
    picklist: {
        height: 50,
        width: IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent',
        paddingBottom: 20,
    },
    FModal: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 10,
        width: '100%',
        borderColor: 'transparent'
    },
    FModal2: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 20,
        width: '100%',
        borderColor: 'transparent'
    },
    picklistLabel: {
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        marginBottom: Platform.OS === 'ios' ?  15 : 0,
        lineHeight: 16,
    },
    text: {
        fontFamily: 'Roboto',
        fontSize: 16,
        paddingTop: Platform.OS === 'ios' ? 0 : 5,
        
        
        color: '#0D3754',
        letterSpacing: 0,
        paddingLeft: 0
    },
    placeholderContainer: {
        height: 24

    }
})
