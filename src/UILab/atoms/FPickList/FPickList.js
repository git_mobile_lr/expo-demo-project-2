import React from 'react';
import { TouchableOpacity } from 'react-native'
import { Label, Text, Item, View } from 'native-base'
import { styles } from './FPickListStyles'
import FModal from '../../molecules/FModal/FModal'
import FList from '../../molecules/FList/FList'
import _ from 'lodash'
import * as Sentry from 'sentry-expo';
import { inject, observer } from "mobx-react/native"
import Base64 from '../../../helpers/base64'
import PropTypes from 'prop-types';
import { initialWindowSafeAreaInsets } from 'react-native-safe-area-context';


@inject('rootStore')
@observer
class FPickList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      isModalVisible: props.isEditing,
      label: props.label,
      valueToDisplay: "",
      picklistvalues: props.picklistvalues,
      
      searchBar: props.searchBar
    };
  }

  componentDidUpdate(prevProps) {
    if (!_.isEqual(this.state.isModalVisible, this.props.isEditing)) {
      this.setState({ isModalVisible: this.props.isEditing })
    }
    if (!_.isEqual(this.state.value, this.props.value)) {
      

      this.setState({ value: this.props.value }, () => {
        const { value } = this.props
        if (value === "" || value === null) {
          this.setState({ valueToDisplay: "" })
        }
      })
    }
  }

  getPickListValues(){
    const {  loginStore } = this.props.rootStore
    
    let _picklistValues = this._getPickListValues()

    const TSLI = loginStore.getPrefixedTableName("Timesheet_Lineitem__c")
    //console.warn('getPickListValues', this.props.name, this.props.table, TSLI)
    
    if(this.props.name === TSLI || this.props.table === TSLI){
    
      const _filterOut = loginStore.getTimesheetFilterOutFromSettings()
      if(!_.isEmpty(_filterOut)){
        
        /*
        {
        "active": true,
        "defaultValue": false,
        "label": "Vergadering",
        "validFor": null,
        "value": "Vergadering"
        },
        {
            "active": true,
            "defaultValue": false,
            "label": "Reizen naar huis",
            "validFor": null,
            "value": "Travel home"
        },
        */
        return _picklistValues.filter(item => {return !_.includes(_filterOut, item.value);})
      }
      
    }
    
    return _picklistValues
  }

  _getPickListValues() {
    const { dataStore, loginStore } = this.props.rootStore
    const { table } = this.props

    // Values 
    const CONTROLLER = this.props.controllerName
    const CONTEXT = dataStore.contextData
    const DEPENDENT_PICKLIST_VALUES = this.props.picklistvalues



    // Check if It is a controlled picklist
    if (!_.isEmpty(CONTROLLER)) {

      // Get the meta data of the controller field
      const meta = this.props.rootStore.metaStore.getFieldMeta(table, CONTROLLER)

      // Get the controller value
      if (CONTEXT.hasOwnProperty(table) && CONTEXT[table].hasOwnProperty(CONTROLLER)) {
        let controllerValue = CONTEXT[table][CONTROLLER]

        // Special Exception ( otherwise the closure plus screen won't be able to retrive the values)
        if (this.props.name === loginStore.getPrefixedFieldName("Closure_Status__c")) {
          controllerValue = loginStore.isClosingWorkOrder ? "CLOSED" : CONTEXT[table][CONTROLLER]
        }

        // Get the correct picklist values. Decoding the validfor field
        if (meta.type === 'success' && !_.isEmpty(meta.data)) {
          const CONTROLLER_PICKLIST_VALUES = meta.data.picklistvalues
          const VALID_VALUES = this.getValidPicklistValues(controllerValue, DEPENDENT_PICKLIST_VALUES, CONTROLLER_PICKLIST_VALUES)
          if (_.isEmpty(VALID_VALUES)) {
            this.props.setReadOnly()
          }
          return VALID_VALUES
        }
      }


      this.props.setReadOnly()

      return []

    }
    return this.props.picklistvalues
  }

  handleChange = (value) => {

    if (typeof value == "string") {
      this.setState(
        {
          value: value,
          valueToDisplay: value,
          isModalVisible: false
        },
        () => {
          this.props.onChange(value)
          this.props.onSubmit(value)
          this.props.toggleIsEditing()
        })
    } else {
      try {
        this.setState(
          {
            value: value.value,
            valueToDisplay: value.label,
            isModalVisible: false
          },
          () => {
            this.props.onChange(value.value)
            this.props.onSubmit(value.value)
            this.props.toggleIsEditing()
          })

      } catch (err) {

      }
    }


  }

  handleBack = () => {
    this.setState({ isModalVisible: false })
    this.props.toggleIsEditing()
  }


  /**
   * Retrives the label of the picklist value.
   * If it fails returns the value
   */
  getValueToDisplay() {
    const { value } = this.state
    try {      
      if (this.props.hasOwnProperty("picklistvalues") && !_.isEmpty(this.props.picklistvalues)) {
        
        const { picklistvalues } = this.props
        if (typeof _.first(picklistvalues) == "string") {
          return value;
        }

        return picklistvalues.reduce((acc, option) => { return (option.value === this.state.value) ? option.label : acc }, this.labelToDisplay())
      }

    } catch (error) {
   
    }
    return this.state.value
  }

  /**
   * 
   * @param {string} controlValue 
   * @param {object[]} dependentListValues // Dependent PickList Values
   * @param {string[]} controllingValues // Controller PickList Values
   */
  getValidPicklistValues(controlValue, dependentListValues, controllingValues) {
    try {
      // Figure out the index of the controlValue
      var index = controllingValues.indexOf(controllingValues.find(
        function (item) { return controlValue === item.value; }
      ));

      // Return a list of matching options given the control value
      return dependentListValues.filter(function (item) {
        // atob is base64-decoding.
        return !!(Base64.atob(item.validFor).charCodeAt(index >> 3) & (128 >> (index % 8)));

      });
    } catch (err) {

      // Sentry.captureException(new Error("Not able to get valid picklist values."), {
      // Sentry.Native.captureException(new Error("Not able to get valid picklist values."), {
      //   logger: 'FPickList (Dependent)',
      //   context: 'getValidPicklistValues'
      // });
      return []
    }

  }

  capitalize(label) {
    return label.charAt(0).toUpperCase() + label.slice(1);
  }

  labelToDisplay() {
    const { label, valueToDisplay } = this.state
    if (valueToDisplay === "" || valueToDisplay === null) {
      return this.capitalize(label)
    }
    return valueToDisplay
  }

  modalTrigger() {

    let value = this.getValueToDisplay()
    const { label, placeholder } = this.props
    return (
      <TouchableOpacity onPress={this.toggleModal}>
        <View style={styles.placeholderContainer}>
          <Text style={((label === value) || _.isEmpty(value)) ? styles.placeholder : styles.text}>
            {_.isEmpty(value) ? placeholder : value}
          </Text>
        </View>
      </TouchableOpacity>

    )
  }

  renderPlaceholder() {
    const { placeholder } = this.props
    const { label, value } = this.state

    return (
      <View style={styles.placeholderContainer}>
        <Text style={_.isEmpty(value) ? styles.placeholder : styles.text}>
          {_.isEmpty(value) ? placeholder : value}
        </Text>
      </View>
    )
  }

   renderReadonlyValue() {
    
    let value = this.getValueToDisplay()
    
    return (
      <View style={styles.placeholderContainer}>
        <Text style={_.isEmpty(value) ? styles.placeholder : styles.text}>
          {value}
        </Text>
      </View>
    )
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  render() {
    const { loginStore } = this.props.rootStore
    const { label, isModalVisible, value } = this.state
    let filteredPickListValues = this.getPickListValues()

    // This part covers 2 scenarios
    // First of all when the picklist values are controlled and the filter doesn't return anything
    // and ... If the custom value when no metadata is present
    if (value && value !== "") {
      if (_.isEmpty(filteredPickListValues)) {
        this.handleChange("")
      } else {
        if (filteredPickListValues.hasOwnProperty("value")) {
          let found = !_.isEmpty(filteredPickListValues.filter((item) => item.value == value))
          if (!found) {
            this.handleChange("")
          }
        }

      }
    }



    return (
      <Item stackedLabel style={styles.picklist}>
        <Label style={styles.picklistLabel}>{loginStore.getLabel(label)}</Label>
        <Item style={styles.FModal}>

          {!_.isEmpty(filteredPickListValues) && !this.props.readonly &&
            <FModal

              triggerItem={this.modalTrigger()}
              onBack={this.handleBack}
              label={label}
              isModalVisible={isModalVisible && !this.props.readonly}
              value={this.getValueToDisplay()}
              header={this.capitalize(label)}>
              <FList
                onSelectOption={this.handleChange}
                list={this.state.list}
                customList={filteredPickListValues}
                searchBar={false}
              />
            </FModal>
          }
          {
            !_.isEmpty(filteredPickListValues) && this.props.readonly && 
            this.renderReadonlyValue()
          }
          {
            _.isEmpty(filteredPickListValues) &&
            this.renderPlaceholder()
          }
        </Item>
      </Item>
    )
  }
}

FPickList.defaultProps = {
  setReadOnly: () => { },
  onChange: () => { },
  onSubmit: () => { },
  toggleIsEditing: () => { },
  value: "",
  label: "$Label.PickListLabelDefault",
  picklistvalues: [],
  isEditing: false,
  name: "",
  table: "",
  controllerName: "",
  readonly: false,
  placeholder: ""
};

export default FPickList;
