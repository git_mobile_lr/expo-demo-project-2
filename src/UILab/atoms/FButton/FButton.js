import React, { Component } from 'react';
import {inject, observer} from "mobx-react/native"
import { Icon, Button, Text, View } from 'native-base'
import { styles } from './FButtonStyles'

@inject('rootStore')
@observer
class FButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text : this.getLabel(this.getText()),
            inactive : this.getInactive(),
            color : this.getColor()
        };
    }

    getInactive(){
        const {config} = this.props
        return config.hasOwnProperty("inactive")
            ? config.inactive
            : false
    }

    getColor(){
        const {config} = this.props
        return config.hasOwnProperty("color")
            ? config.color
            : "#00AAFF"
    }

    getText(){
        const {config} = this.props
        return config.hasOwnProperty("text")
            ? config.text
            : ""
    }

    getLabel(text){
        return text.includes("Label") 
            ? this.props.rootStore.loginStore.getLabel(text)
            : text
    }

    render() {
        const { text, inactive,color } = this.state
        return (
            <View style={inactive ? styles.buttonTextInactive : styles.buttonText} >
                <Text style={{color:color,alignSelf:"center"}} >{text}</Text>
            </View>
        );
    }
}

FButton.defaultProps = {
    config: {}
}

export default FButton;