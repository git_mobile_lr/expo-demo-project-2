import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
    buttonText:{
      shadowColor: '#00AAFF',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.2,
      shadowRadius: 2,
      elevation: 1,
      paddingLeft:10,
      paddingRight:10,
      paddingTop:3,
      paddingBottom:3,
      backgroundColor: '#00AAFF',
      borderRadius:20,
      color:'white',
      borderWidth:0.5,
      borderColor:'#00AAFF',
      fontFamily: "Ionicons",
      letterSpacing: 1.8
    },
    buttonTextInactive:{
      shadowColor: '#00AAFF',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.2,
      shadowRadius: 2,
      elevation: 1,
      paddingLeft:10,
      paddingRight:10,
      paddingTop:3,
      paddingBottom:3,
      backgroundColor: 'white',
      borderRadius:20,
      color:'#00AAFF',
      borderWidth:0.5,
      borderColor:'#00AAFF',
      fontFamily: "Ionicons",
      letterSpacing: 1.8,
      height:25
    }
})