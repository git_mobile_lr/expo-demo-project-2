import { StyleSheet, Dimensions, Platform } from 'react-native'
import { COLORS } from '../../constants'

export const styles = StyleSheet.create({
    saveButton:{
        color : COLORS.PRIMARY,
        lineHeight: 24,
        alignSelf:'flex-end'
    },
    tabletSaveButton:{
        color : COLORS.PRIMARY,
        alignSelf:'flex-end'
    },
    disabled:{
        color : COLORS.GRAY
    },
    saveIcon: {
        color : COLORS.PRIMARY,
        lineHeight: 24,
        alignSelf:'flex-end'
    },
    disabledIcon:{
        color : COLORS.GRAY,
        lineHeight: 24,
        alignSelf:'flex-end'
    },
    buttonContainer:{

    },
    spinnerContainer:{
    },
    handleOnSaveStyle: {
        width: Dimensions.get('screen').width / 3,
        flex: 1,
        textAlignVertical: 'center',
        justifyContent: "flex-start",
        paddingTop: Platform.OS === "ios"
            ? 15
            : 15,
        alignItems: Platform.OS === "ios"
            ? "center"
            : "center"
    },
    handleOnSaveButtonStyle: {
        width: Dimensions.get('screen').width / 3,
        flex: 1,
        justifyContent: "flex-end",
        paddingTop: Platform.OS === "ios"
            ? 15
            : 0,
        alignItems: Platform.OS === "ios"
            ? "center"
            : "flex-start"
    }
})