import React, { Component } from 'react';
import { styles } from './Styles'
import { View, Text, Button } from 'native-base'
import { FSpinner } from '../../index'
import _ from 'lodash'

import { TouchableOpacity, TapGestureHandler } from 'react-native-gesture-handler';
import { Dimensions, Platform, Alert, TouchableOpacity as TouchableOpacityAndroid } from 'react-native'
import { FontAwesome } from "@expo/vector-icons"
import { inject, observer } from "mobx-react/native"
type ValidationRuleResult = {
    success: boolean,
    errorMessage?: Array<string>


}

type Props = {
    buttonText: string
    onPress: () => void,
    onValidateRules?: () => ValidationRuleResult,    
    busy: boolean
    disabled: boolean
}

type State = {
    buttonText: string
    busy: boolean
    disabled: boolean
}
@inject('rootStore')
@observer
class FSaveButton extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            buttonText: props.buttonText,
            busy: props.busy,
            disabled: props.disabled
        };
    }

    componentDidUpdate(prevProps: Props) {        
        if (!_.isEqual(prevProps.disabled, this.props.disabled)) {
          // LISTEN TO PROPS CHANGES
          this.setState({ disabled: this.props.disabled })
        }
      }

    handleOnPress = () => {
        
        if (this.props.onValidateRules) {
            const rs = this.props.onValidateRules()
            
            if (rs.success) {
                this.setState({ busy: true }, this.props.onPress)
            } else {

                if (!_.isEmpty(rs.errorMessage)) {
                    alert(rs.errorMessage.join("\n"))

                } else if (!_.isEmpty(rs.warningMessage)) {
                    
                    let warnings = "warning"

                    try {
                        warnings = Platform.OS === 'ios' ? (rs.warningMessage || []).join(" ") : (rs.warningMessage || []).join(".")    
                    } catch (error) {
                        //pass
                        // alert("err " + error)
                    }
                    
                    
                    this.props.rootStore.loginStore.FAlert({
                        dialogs: {
                            "dialog": "WARNING",
                            errorMessage: warnings
                        },
                        onCancel: async () => {

                        },
                        onConfirm: async () => {
                            this.setState({ busy: true }, this.props.onPress)
                        }
                    });
                }

            }
        } else {
            this.setState({ busy: true }, this.props.onPress)
        }
    }

    render() {
        const { buttonText, busy, disabled } = this.state

        const saveButton = this._renderSaveButton()

        return busy
            ? <FSpinner />
            : saveButton

    }

    _renderSaveButton() {
        const { buttonText, busy, disabled } = this.state



        return (
            <TouchableOpacity style={styles.handleOnSaveStyle} onPress={!disabled ? this.handleOnPress : () => { }} accessibilityLabel="Save Button" testID="Save Button iOS">
                <FontAwesome
                    name={'check'}
                    size={25}
                    style={disabled ? styles.disabledIcon : styles.saveIcon}
                />
            </TouchableOpacity>
        )



        //Backup gesture for Android
        return (
            <TouchableOpacityAndroid style={styles.handleOnSaveStyle} onPress={!disabled ? this.handleOnPress : () => { }} accessibilityLabel="Save Button" testID="Save Button iOS">
                <FontAwesome
                    name={'check'}
                    size={25}
                    style={disabled ? styles.disabledIcon : styles.saveIcon}
                />
            </TouchableOpacityAndroid>
        )


        return (<Button style={styles.handleOnSaveButtonStyle}
            transparent
            onPress={!disabled ? this.handleOnPress : () => { }} accessibilityLabel="Save Button" testID="Save Button iOS">
            <Text style={disabled ? styles.disabled : styles.saveButton}>{buttonText}</Text>
        </Button>)
    }
}

//@ts-ignore
FSaveButton.defaultProps = {
    onPress: () => { },
    onValidateRules: null,
    buttonText: "Save",
    busy: false,
    disabled: false
}

export default FSaveButton;