import React, { Component } from 'react';
import { View, Text, Button } from 'native-base'
import styles from './CardBodyStyles'
import FRenderer from '../../../screens/widgets/FRenderer'
import _ from 'lodash'
import DataProvider from '../DataProvider/DataProvider'
import { inject, observer } from "mobx-react/native"
import FSpinner from '../FSpinner/FSpinner'
import { ScrollView } from 'react-native'
import {
    widthPercentageToDP as wp
  } from "react-native-responsive-screen";
import { DEFAULT_CONFIG } from './CardBodyConfig'

@inject('rootStore')
@observer
class CardBody extends Component {
    constructor(props) {
        super(props);
        this.state = {
            config: {...DEFAULT_CONFIG,...props.config},
            data: props.data,
            isModalVisible : false,
            item : []
        }
    }

    componentDidMount(){
        this.setState({
            item: this.getItem()
        })

    }

    componentDidUpdate(prevProps) {
        if (!_.isEqual(prevProps, this.props)) {
            this.setState({ 
                data: this.props.data
            })
        }
    }

    getItem() {
        const { config } = this.state
        return config.hasOwnProperty("item")
            ? config.item
            : []
    }

    render() {
        const { item } = this.state
        if (!_.isEmpty(item)){
            return (
                <ScrollView
                    pagingEnabled
                    horizontal={true}
                    style={styles.cardBody}
                    keyboardShouldPersistTaps={'handled'}
                >
                    <View style={{flex:1,width: wp("95%"),}}>
                             {
                                this.props.UID!="" && !_.isEmpty(this.props.UID) &&
                                <DataProvider 
                                    UID = {this.props.UID}
                                    config={_.first(item).hasOwnProperty("config") ? _.first(item).config : null}
                                    render = { data => {
                                        return data.loading 
                                            ? <FSpinner />
                                            : <FRenderer data={{UID:this.props.UID}} config={_.first(item)}/>
                                    }}
                                />
                            }
                    </View>
                    <View style={{flex:1,height:400,width: wp("95%"),}}>
                        {
                                this.props.UID!="" && !_.isEmpty(this.props.UID) &&
                                <DataProvider 
                                    UID = {this.props.UID}
                                    config={_.last(item).hasOwnProperty("config") ? _.last(item).config : null}
                                    render = { data => {
                                        if (!data.loading){
                       
                                        }
                                        return data.loading 
                                            ? <FSpinner />
                                            : <FRenderer data={data.payload} config={_.last(item)}/>
                                    }}
                                />
                            }
                                
    
                        </View>
                </ScrollView>
            );
        }else{
            return <View />
        }
        
    }
}


CardBody.defaultProps = {
    UID: "INITIAL",
    data: {},
    config: {}
}

export default CardBody;