import React, { Component } from 'react';
import { View } from 'native-base'
import styles from './RoundedCardStyles'

class RoundedCard extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        return (
            <View style={styles.card}>
                {this.props.children}
            </View>
        );
    }
}

export default RoundedCard;