import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    cardHeader : {
        
        borderTopLeftRadius:15,
        borderTopRightRadius:15,
        padding:5,
        flex:1,
        flexDirection: 'row',
        backgroundColor:'#00AAFF',
        justifyContent: 'space-between'
    }
})

export default styles