import React, { Component } from 'react';
import { SafeAreaView } from 'react-native'
import RoundedCard from './RoundedCard'
import FRenderer from '../../../screens/widgets/FRenderer'
import _ from "lodash"
import {inject, observer} from "mobx-react/native"
import { DEFAULT_CONFIG } from './FRoundedCardConfig'
@inject('rootStore')
@observer
class FRoundedCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            config : {...DEFAULT_CONFIG,...props.config}
        };
    }

    componentDidMount(){
        this.setState({
            header : this.getHeader(),
            body: this.getBody(),
            footer: this.getFooter()
        })
    }

    getHeader(){
        const {config} = this.state
        return config.hasOwnProperty("header")
            ? config.header
            : {}
    }

    getBody(){
        const {config} = this.state
        return config.hasOwnProperty("body")
            ? config.body
            : {}
    }

    getFooter(){
        const {config} = this.state
        return config.hasOwnProperty("footer")
            ? config.footer
            : {}
    }


    render() {
        const {header,body,footer} = this.state
        const {UID} = this.props
        return (
            <SafeAreaView>
                    <RoundedCard>
                        <FRenderer data={{"UID":UID}} config={header} /> 
                        <FRenderer data={{"UID":UID}} config={body} /> 
                        <FRenderer data={{"UID":UID}} config={footer} /> 
                    </RoundedCard>
            </SafeAreaView>
        )
    }
}

FRoundedCard.defaultProps = {
    UID: "",
    config : {},
    data : {}
}

export default FRoundedCard;