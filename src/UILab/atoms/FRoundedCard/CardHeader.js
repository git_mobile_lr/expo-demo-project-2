
import React,{Component} from 'react';
import styles from './CardHeaderStyles'
import { Text, View } from 'native-base'
import { TouchableOpacity } from 'react-native'
import  FRenderer  from '../../../screens/widgets/FRenderer'
import _ from 'lodash'
import DEFAULT_CONFIG from './CardHeaderConfig'

class CardHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            config : {...DEFAULT_CONFIG,...props.config},
            data : props.data
        };
    }

    componentDidMount(){
        this.setState({
            item : this.getItem()
        })
    }

    componentDidUpdate(prevProps){
        if (!_.isEqual(prevProps.data,this.props.data)){
          this.setState({data:this.props.data})
        }
      }

    getItem(){
        const {config} = this.state
        return config.hasOwnProperty("item")
            ? config.item
            : []
    }
    
    render() {
        const {item,data} = this.state
        const { UID } = this.props
        return (
                <View style={styles.cardHeader}>
                    <FRenderer data={{ "UID": UID }} config={_.first(item)} />
                </View>
        );
    }
}

CardHeader.defaultProps = {
    UID:"",
    data : {},
    config : {}
}

export default CardHeader;