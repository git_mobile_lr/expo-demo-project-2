import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    cardBody:{
        borderColor:'#EEEEEE',
        paddingBottom:10
    },
    FModal:{
        height:10,
        borderColor: 'transparent',
        alignItems: "center",
        justifyContent: "center"
    },
    modalButton:{
        width:"100%",
        color:"white",
        fontFamily: 'Roboto',
        fontSize: 24,
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft:0,
        textTransform: 'capitalize'
    }
})

export default styles