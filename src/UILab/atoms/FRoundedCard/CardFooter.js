import React,{Component} from 'react';
import { View } from 'native-base'
import styles from './CardFooterStyles'
import _ from "lodash"
import FRenderer from '../../../screens/widgets/FRenderer'
import {inject, observer} from "mobx-react/native"
import FModal from '../../molecules/FModal/FModal'
import DataProvider from '../DataProvider/DataProvider'
import FSpinner from '../FSpinner/FSpinner'
import { DEFAULT_CONFIG } from './CardFooterConfig'
@inject('rootStore')
@observer
class CardFooter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            config : {...DEFAULT_CONFIG,...props.config},
            isModalVisible: false,
            popUpItem :{},
            item:{}
        };
    }

    componentDidMount(){
        this.setState({
            popUpItem : this.getPopUpItem(),
            item : this.getItem(),
        })
    }

    getItem(){
        const {config} = this.state
        return config.hasOwnProperty("item")
            ? config.item
            : []
    }


    getPopUpItem(){
        const {config} = this.state
        return config.hasOwnProperty("popUpItem")
            ? config.popUpItem
            : []
    }

    handleBack = () =>{}

    toggleModal = () => { this.setState({isModalVisible:!this.state.isModalVisible}) }
 
    render() {
        const { loginStore } = this.props.rootStore
        const {item,popUpItem,isModalVisible} = this.state
        const {hide,UID} = this.props
        if (hide) return <View />
        return (
            <View style={styles.cardFooter}>
                <View style={styles.buttonRow}>
                    <FModal
                        onBack={this.handleBack}
                        isModalVisible={isModalVisible}
                        buttonStyle={styles.modalButton}
                        triggerItem={<FRenderer data={{UID:UID}} config={_.first(item)}/>}
                        header={loginStore.getLabel("${Label.Items}")}>
                            <DataProvider 
                                UID = {this.props.UID}
                                config={popUpItem.hasOwnProperty("config") ? popUpItem.config : {}}
                                render = { data => {
                                    if (!data.loading){
                   
                                    }
                                    return data.loading 
                                        ? <FSpinner />
                                        : <FRenderer data={data.payload} config={popUpItem}/>
                                }}
                            />     
                    </FModal>   
                    
                </View>
            </View>     
        );
    } 
}

CardFooter.defaultProps = {
    UID:"",
    hide : false
}

export default CardFooter;