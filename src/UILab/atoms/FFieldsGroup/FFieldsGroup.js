import React, { Component } from 'react'
import {inject, observer} from "mobx-react/native"
import FField from '../../molecules/FField/FField'
import styles from './FFieldsStyles'
import { View } from 'native-base'
import DataProvider from '../DataProvider/DataProvider'
import FSpinner from '../FSpinner/FSpinner'
import { DEFAULT_CONFIG } from './FFieldsGroupConfig'

@inject('rootStore')
@observer
class FFieldsGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            config : {...DEFAULT_CONFIG,...props.config}
            
        };
    }

    componentDidMount(){
        this.setState({
            fields : this.getFields()
        })
    }

    getFields(){
        const {config} = this.state
        return (config.hasOwnProperty("dataText")&&config.hasOwnProperty("dataBinding"))
            ? Object.values(config.dataText).map(_var_=>config.dataBinding.find(item=>item.var===_var_))
            : []
    }

    render() {
        const {fields} = this.state
        const { UID, config } = this.props
        return (
            <View style={styles.woliRow}>
                <DataProvider 
                    UID = {UID}
                    config = {config}
                    render = { data => {
                        return data.loading 
                               ? <FSpinner />
                               : fields.map(item => {
                                        return (
                                            <FField
                                                data={data.payload}
                                                table={item.table}
                                                field={item.field}
                                            />
                                        )
                                    })
                    }}
                />
                
            </View>
        );
    }
}

FFieldsGroup.defaultProps = {
    UID: {},
    fields: [],
    data: {},
    config : {}
}

export default FFieldsGroup;