import React, { Component } from 'react';
import _ from 'lodash'
import FSpinner from '../FSpinner/FSpinner'
import { FilterCondition } from '../../../types'

type Props = {
  initialData: Array<object>
  conditions: Array<FilterCondition>
  paging?: boolean
  pageSize?: number
  currentPage?: number
  onNoConditionsRenderEmpty: boolean
  render: (state: State) => JSX.Element
  getLinkedObjectMediaById: (objName: String, sliceArray: Array<object>) => Array<object>
}

type State = {
  initialData: Array<object>
  conditions: Array<FilterCondition>
  loading: boolean
  payload: Array<object>
}

class FFilter extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      initialData: props.initialData,
      conditions: props.conditions,
      loading: true,
      payload: []
    };
  }

  componentDidUpdate(prevProps: Props) {
    if (!_.isEqual(prevProps.conditions, this.props.conditions) || !_.isEqual(this.props.initialData, this.state.initialData)) {
      this.setState({
        initialData: this.props.initialData,
        conditions: this.props.conditions,
        loading: true,
        payload: []
      }, () => {
        this.applyFilters()
      })
    }
  }


  componentDidMount() {
    this.applyFilters()
  }

  //@ts-ignore obj = self -> What does it mean ??
  resolvePath(path: string | Array<string>, obj = self, separator = '.') {
    var properties = Array.isArray(path)
      ? path
      : path.split(separator)
    return properties.reduce((prev, curr) => prev && prev[curr], obj)
  }

  applyFilters() {
    this.setState({ loading: false, payload: [] })


    this.setState({
      loading: true
    }, async () => {
      try {
        const { initialData, conditions } = this.state
        const { onNoConditionsRenderEmpty } = this.props
        
        // IF NO DATA
        if (_.isEmpty(initialData)) {
          this.setState({ loading: false, payload: [] })
        }

        // IF NO CONDITIONS => RETURNS DATA
        if (_.isEmpty(conditions)) {
          this.setState({ loading: false, payload: initialData })
        }

        // onNoConditionsRenderEmpty
        let AreAllConditionsInactive = !conditions.reduce((acc, condition) => {
          try {


            return condition.active || acc
          } catch (error) {
            console.warn("AreAll")
            return true
          }
        }, false)

        if (AreAllConditionsInactive && onNoConditionsRenderEmpty) {
          this.setState({ loading: false, payload: [] })
        } else {
          if (!_.isEmpty(initialData) && (!_.isEmpty(conditions))) {
            
            
            let filteredResults = conditions.reduce((acc, condition) => {
              return acc.filter( (item) => {
                if (item.hasOwnProperty('IsAttachment')) {
                  
                  if (condition.active) {
                    

                    if (condition.value === "ALL") {
                      
                      return 'ALL' === condition.value
                    }
                  
                    if (item.hasOwnProperty("LinkedObjects")) {
                      
                      let _linkedObjects = item.LinkedObjects.map(x => x.toLowerCase())
                      let _value = (condition.value || "").toLowerCase();

                      if(_.includes(_linkedObjects, _value)){
                        
                        return true
                      }
                      
                      if (item.LinkedObjects.hasOwnProperty(condition.value)) {

                        return true
                      }
                      
                      return false
                    }
                    
                    return false
                    // const mediaData = await this.props.getLinkedObjectMediaById(condition.value, item.LinkedObjects[condition.value])
                  }
                  
                  return false
                } else {
                  const VALUE = this.resolvePath(condition.field, item)
                  
                  if (condition.active) {
                    try {
                      switch (condition.operator) {
                        case "=":
                          return VALUE === condition.value
                        case "!=":
                          return VALUE !== condition.value
                        case ">":
                          return VALUE > condition.value
                        case ">=":
                          return VALUE >= condition.value
                        case "<":
                          return VALUE < condition.value
                        case "<=":
                          return VALUE <= condition.value
                        case "$exists":
                          return !!VALUE
                        case "contains":
  
                          return VALUE.toLowerCase().includes(condition.value.toLowerCase())
                        default:
                          return true
                      }
                    } catch (err) {
                      return true
                    }
                  }

                  return true
                }
              })
            }, initialData)
            //  this.setState({ payload: filteredResults}, ()=>{
            //   this.setState({loading: false})
            //  })
            
            // this.setState({ loading: false })
            this.setState({ payload: filteredResults, loading: false })
          }
        }

      } catch (err) {
        //pass
      }
    })
  }

  render() {
    return this
      .props
      .render(this.state)
  }
}

//@ts-ignore defaults
FFilter.defaultProps = {
  onNoConditionsRenderEmpty: false,
  initialData: [],
  conditions: [],
  paging: true,
  currentPage: 0,
  pageSize: 10,
  render: () => {
    return <FSpinner />
  }
}

export default FFilter;