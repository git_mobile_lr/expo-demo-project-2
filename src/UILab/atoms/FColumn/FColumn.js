import React, { Component } from 'react';
import {styles} from './FColumnStyle'
import FRenderer from '../../../screens/widgets/FRenderer'

class FColumn extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            data : props.data,
            item : this.getItems()
         };
    }

    componentDidUpdate(prevProps){
        if (!_.isEqual(prevProps.data,this.props.data)){
          this.setState({data:this.props.data})
        }
      }

    getItems(){
        const {config} = this.props
        return config.hasOwnProperty("item")
            ? config.item
            : []
    }

    render() {
        const {item,data} = this.state
        return ( item.map(item=>{
            return <FRenderer data={data} config={item} />
        })
        )
    }
}

export default FColumn;