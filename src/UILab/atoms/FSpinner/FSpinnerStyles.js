import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
    errorMessage: {
        alignSelf:'center',
        color:'gray'
    }
})