import React from 'react';
import { Spinner, View, Text, Button } from 'native-base'
import FHeading from '../FHeading/FHeading'
import { inject, observer } from "mobx-react/native"
import { styles } from './FSpinnerStyles'
import { ActivityIndicator } from 'react-native';

//@inject('loginStore')
@inject('rootStore')
@observer
class FSpinner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            timeOut: false,
            showTimeOut: props.showTimeOut,
            timer: setTimeout(() => {
                this.timeOut = true
            }, props.showTimeOut)
        };
    }

    componentWillUnmount() {
        clearTimeout(this.state.timer)
    }

    render() {
        const { timeOut, showTimeOut } = this.state
        const { loginStore } = this.props.rootStore
        const { white } = this.props
        return (
            <View >
                {showTimeOut === 0 &&
                    white
                    ? <Spinner color={"white"} />
                    : <Spinner color='#1EB1F8' />
                }
                {timeOut && showTimeOut > 0 &&
                    <View>
                        <FHeading size={4} value={loginStore.getLabel(this.props.label)} />
                        <Button style={{ alignSelf: 'center' }} rounded info onPress={() => { loginStore.navigate("ReAuth", {}, true) }}><Text>{loginStore.getLabel(this.props.buttonLabel)}</Text></Button>
                    </View>}
            </View>

        )
    }
}

FSpinner.defaultProps = {
    white: false,
    showTimeOut: 0,
    buttonLabel: "${Label.Login}",
    label: "${Label.OnlineResultsError}"
}

export default FSpinner;