import React from 'react'
import { styles } from './FMapStyles'
import MapView from 'react-native-maps';
import { Marker, PROVIDER_GOOGLE } from 'react-native-maps'

class FMap extends React.Component {
    state = {
        value : {
            lat:123,
            long:321
        }
    }

    // To complete:

    // ask permissions

    
    render() {
        return (
            <MapView
                style={{
                    alignSelf: 'stretch',
                    height: 300,

                }}
                showsUserLocation
                followsUserLocation
  
            />
        );
    }
}

export default FMap;