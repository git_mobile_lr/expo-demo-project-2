import React from 'react';
import { inject, observer } from "mobx-react/native"
import { styles } from './FImageStyles'

import {  Image, Text } from 'react-native'
import _ from 'lodash';
import * as FileSystem from 'expo-file-system'

@inject('rootStore')
@observer
class FImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            assetsAreLoaded: false,
            dontReload: false
        };
    }

    componentDidMount(){       
        this._base64()
    }

    async _base64() {
        try {
            console.warn(this.props.mediaObject.FileName + " " + this.props.mediaObject.IsCached)
            
            if(!this.props.mediaObject.IsCached && !this.props.mediaObject.IsLocal && !this.props.mediaObject.IsUploaded ){
                return;
            }
            

            const { loginStore } = this.props.rootStore;
            const { mediaLibraryLastUpdateSmall } = loginStore
            let srcLink = this.props.mediaObject.AssetSmallLink ?
                this.props.mediaObject.AssetSmallLink :
                this.props.mediaObject.AssetLink
            const raw = await FileSystem.readAsStringAsync(srcLink, { encoding: FileSystem.EncodingType.Base64 })

            this.setState({
                assetsAreLoaded: true,
                content: this._getUri(raw),
                mediaLibraryLastUpdateSmall: mediaLibraryLastUpdateSmall
            })
        } catch (error) {
            //pass  
        }
    }

    componentDidUpdate(prevProps) {
        if (this.state.dontReload) {

            return;
        }
        const { loginStore } = this.props.rootStore;

        const { mediaLibraryLastUpdateSmall } = loginStore
        if (mediaLibraryLastUpdateSmall !== this.state.mediaLibraryLastUpdateSmall) {
            //    console.log(this.props.mediaObject.Id, ".")
            const { documentStore } = this.props.rootStore
            let ifFileAnImage = documentStore.ifFileAnImage(this.props.mediaObject)
            if (ifFileAnImage) {
                this._base64();
            }
        }
    }

   
    async _onLoadingError() {

        
       // console.log(this.props.mediaObject.FileName )
        this.setState({ assetsAreLoaded: false }, async () => {

            const { loginStore, documentStore } = this.props.rootStore
            let ifFileAnImage = documentStore.ifFileAnImage(this.props.mediaObject)

            if (ifFileAnImage) {
                let _newMedia = Object.assign({}, { ...this.props.mediaObject })
                if (_newMedia.hasOwnProperty("IsCached")) {
                    _.unset(_newMedia, "IsCached")
                }
                if (_newMedia.hasOwnProperty("AssetLink")) {
                    _.unset(_newMedia, "AssetLink")
                }

                // console.log(JSON.stringify(_newMedia));

                await loginStore.updateMediaObjectById(this.props.mediaObject.Id, _newMedia)

                // setTimeout(() => {
                //     console.warn("my fault" + _newMedia.FileExtension)
                //      loginStore._addAssetLinkToMedia(_newMedia)
                // }, 100);
            }else{
                console.warn("my fault" + _newMedia.FileExtension)
            }

        })
    }

    async _onLoadingEnd() {
        //pass
        this.setState({
            dontReload: true
        })
        
    }

    _getUri(content) {
        try {
          return `data:image/jpeg;base64,${content}`;
        } catch (error) {
          return null
        }
        
      }

    

    render() {
        const { loginStore } = this.props.rootStore;

        const { mediaLibraryLastUpdateSmall } = loginStore

        if (this.state.assetsAreLoaded) {
            const { loginStore } = this.props.rootStore
            

            const IMAGE_SIZE = loginStore.isTablet ? 70 : 50
            let _s = { height: IMAGE_SIZE, width: IMAGE_SIZE, borderRadius: 10, position: 'absolute', marginLeft: 10 }

            

            return (<Image
                resizeMode="cover"
                style={_s}
                source={{
                    uri: this.state.content
                }}
                onError={this._onLoadingError.bind(this)}
                onLoadEnd={this._onLoadingEnd.bind(this)}

            />)

        }

        return null
    }
}

FImage.defaultProps = {
    mediaObject: null
}

export default FImage;