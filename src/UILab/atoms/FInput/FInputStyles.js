import { StyleSheet, Platform } from 'react-native'
import { IS_TABLET, ANDROID } from '../../../screens/widgets/styles'
import { COLORS } from '../../constants'

export const styles = StyleSheet.create({
    label: {
        fontFamily: 'Roboto',
        fontSize: 11,
        color: '#5384A6',
        letterSpacing: 0,
        lineHeight: 16,
    },
    input: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#0D3754',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0,
        paddingLeft: 24,
        paddingVertical: 5

    },
    textError: {
        paddingLeft: 24,
        fontFamily: 'Roboto',
        fontSize: 16,
        letterSpacing: 0,
        lineHeight: 24,
        borderColor: 'transparent',
        color: 'red',
        paddingVertical: 5
    },
    FInput: {

        height: 65,
        width: IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent'
    },
    FInputMultiLine: {

        minHeight: 65,
        height: "auto",
        width: IS_TABLET ? '96%' : '92%',
        borderColor: 'transparent'
    },
    FModal: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        minHeight: 10,
        height: "auto",
        width: '100%',
        borderColor: 'transparent'
    },
    placeholder: {
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#B8C4CC',
        letterSpacing: 0,
        lineHeight: 24,
        paddingLeft: 0,
        paddingVertical:5
    },
    helpText: {
        alignSelf: "flex-start",
        fontFamily: 'Roboto',
        color: COLORS.DARKGRAY,
        fontSize: 12,
        letterSpacing: 0,
        lineHeight: 16,
    }
})
