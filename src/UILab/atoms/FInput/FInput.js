import React from 'react';
import { styles } from './FInputStyles'
import { Text, Item, Input, Label, Icon, View } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { inject, observer } from "mobx-react/native"
import PropTypes from 'prop-types'
import FModal from '../../molecules/FModal/FModal'
import _ from 'lodash'

@inject('rootStore')
@observer
class FInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
            label: props.label,
            name: props.name,
            showValidationErrors: true,
            validationError: false,
            isModalVisible: props.isEditing,
        };
    }

    componentDidUpdate(prevProps) {

        if (!_.isEqual(prevProps.isEditing, this.props.isEditing)) {
            // LISTEN TO PROPS CHANGES

            this.setState({ isModalVisible: this.props.isEditing })
        }

        if (!_.includes(["InstalledProductCreation", "InstalledProductRelation"], this.props.rootStore.loginStore.activeScreen)) {
            // console.warn(this.props.rootStore.appsStore.getActiveScreen() + " " + this.props.rootStore.loginStore.activeScreen)
            if (!this.state.isModalVisible && !this.props.isEditing) {
                if (this.state.value !== this.props.value) {
                    this.setState({ value: this.props.value })
                }
            }
        }
    }

    componentDidMount() {
        const { validationStore } = this.props.rootStore
        
        this.setState({
            validationError: validationStore.validateInput(this.value, this.props.type)
        })
    }

    handleChange = (value) => {
        const { validationStore } = this.props.rootStore
        this.setState({ value: value })
        this.props.onChange(value)
        const validationResult = validationStore.validateInput(value, this.props.type)
        if (_.isEmpty(validationResult)) {
            this.setState({ validationError: false })
        } else {
            this.setState({
                validationError: true,
                errorMessage: validationResult[this.props.type.toLowerCase()]
            })
        }
    }

    onSave = () => {
        const { validationStore } = this.props.rootStore
        let validationResult = validationStore.validateInput(this.state.value, this.props.type)
        if (_.isEmpty(validationResult)) {
            const { type } = this.props
            if (type == "EMAIL") {
                this.props.onSubmit(_.toLower(this.state.value))
                this.props.toggleIsEditing()
            } else {
                this.props.onSubmit(this.state.value)
                this.props.toggleIsEditing()
            }

            return true
        }
        return false
    }

    handleBack = () => {
        this.setState({ isModalVisible: false })
        this.props.toggleIsEditing()
        const { value } = this.state
        const prevValue = this.props.value
        if (prevValue !== value) {
            this.setState({ value: prevValue },
            this.props.onChange(prevValue))
        }
    }

    render() {
        const { maxLength, keyboardType, type, placeholder, displayTextArea, readonly, helpText } = this.props
        const { loginStore } = this.props.rootStore
        const { label, value, isModalVisible, validationError } = this.state
        
        return (
            <Item stackedLabel style={[displayTextArea && (value || placeholder || "").length > 0 ? styles.FInputMultiLine : styles.FInput, {width: this.props.rootStore.loginStore.S_W} ]}>
                <View style={{ alignSelf: 'flex-start' }}>
                    <Label style={styles.label}>{loginStore.getLabelorDefault(label, label)}</Label>
                    {!_.isEmpty(helpText) && <Text style={styles.helpText}>{helpText}</Text>}
                </View>
                <Item style={styles.FModal}>
                    <FModal
                        validationError={validationError}
                        maxLength={maxLength}
                        
                        saveButton={!readonly}
                        displayTextArea={displayTextArea}
                        disabled={this.props.readonly}
                        label={placeholder}
                        type={type}
                        isModalVisible={isModalVisible}
                        value={value}
                        multiSelection={false}
                        header={label}
                        onBack={this.handleBack}
                        onSave={this.onSave}
                        inputInline={true}
                    >
                        <View>
                            <Item style={{ borderColor: 'transparent', backgroundColor: readonly ? '#f0f0f0' : '#fff'}}>
                                <KeyboardAwareScrollView>
                                    <Input
                                        disabled={readonly}
                                        keyboardType={keyboardType}
                                        placeholder={placeholder}
                                        placeholderTextColor='#B8C4CC'
                                        placeholderStyle={styles.placeholder}
                                        name={label}
                                        onChangeText={this.handleChange}
                                        maxLength={maxLength}
                                        style={validationError ? styles.textError : styles.input}
                                        value={value}
                                        multiline={true}
                                        autoFocus={true}
                                    />
                                </KeyboardAwareScrollView>
                                {validationError && <Icon name='close-circle' style={{ color: 'red' }} />}
                            </Item>
                            <Item style={{ borderColor: 'transparent' }}>
                                {validationError && <Text style={styles.textError}>{loginStore.getLabelorDefault(_.first(this.state.errorMessage), "Invalid format")}</Text>}
                            </Item>
                        </View>
                    </FModal>
                </Item>
            </Item>
        )
    }

}

FInput.propTypes = {
    /** This function should be called to change the value of the parent component */
    //onChange: PropTypes.function,

    /** This function should be called to submit the final the value. This function is usually defined in the HOC and triggers syncs. */
    //onSubmit: PropTypes.function,

    /** Label to display above the input */
    label: PropTypes.string.isRequired,

    /** max length of the input */
    maxLength: PropTypes.number,

    /** If it's set to true then the user won't be able to modify its content */
    locked: PropTypes.bool,


    /** The value of the input */
    value: PropTypes.string,

    /** Set to true to force the user to fill this field */
    required: PropTypes.bool,

    /** The name of the object in Sales Force. It's used as a indentifier to send the correct value */
    name: PropTypes.string,

    /** Set the keyboard that is going to be displayed */
    keyboardType: PropTypes.string,

    /** If this option is true the input will be displayed inside of a modal. By default is true*/
    inModal: PropTypes.bool,

    readonly: PropTypes.bool,

    /** Placeholder to display above the input */
    placeholder: PropTypes.string,

    displayTextArea: PropTypes.bool
}


FInput.defaultProps = {
    onChange: () => { },
    onSubmit: () => { },
    label: "$Label.InputLabelDefault",
    maxLength: 80,
    value: "",
    locked: false,
    required: false,
    name: "defaultName",
    keyboardType: "default",
    showValidationErrors: true,
    inModal: true,
    isEditing: false,
    readonly: false,
    placeholder: "",
    displayTextArea: false
};


export default FInput;