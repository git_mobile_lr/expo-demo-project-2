module.exports = {
  // Load setup-tests.js before test execution
  setupFilesAfterEnv: ['./setup-test.js'],
  preset: "jest-expo",
  moduleFileExtensions: ['ts', 'tsx', 'jsx','js'],
  transformIgnorePatterns: [
    "node_modules/(?!(jest-)?react-native|react-clone-referenced-element|@react-native-community|expo(nent)?|@expo(nent)?/.*|react-navigation|@react-navigation/.*|@unimodules/.*|unimodules|sentry-expo|native-base)"
    
  ],
  testPathIgnorePatterns:["node_modules/"],
  modulePathIgnorePatterns: ["<rootDir>/node_modules/"]
};