import React, { useState, useEffect } from 'react';

import AppCurrent from './rings/current/App';
import AppSlow from './rings/slow/App';


import { Text, View } from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
type Props = { 
}

type State = {
  activeRing: string,
  ready: boolean,
  ring: string
}

// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING`
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING-RC`
// const ACTIVE_RING = `@FIELDBUDDY-ACTIVERING-LIVE`
const rings = require('./rings/rings.json')

const ACTIVE_RING = rings.activeRing



const storeActiveRing = async (value : string) => {
  try {
    await AsyncStorage.setItem(ACTIVE_RING, value);
  } catch (e) {
    // saving error
  }
};

const getActiveRing = async () => {
  try {
    // console.warn("ar" + ACTIVE_RING)
    const value = await AsyncStorage.getItem(ACTIVE_RING);
    if (value !== null) {
      return value
      // value previously stored
    }
  } catch (e) {
    // error reading value
  }

  return null
};

const deleteActiveRing = async () => {
  try {
    await AsyncStorage.removeItem(ACTIVE_RING);
    
  } catch (e) {
    // error reading value
  }

  
};
export default class App extends React.Component<Props, State> {


  constructor(props: Props) {
    super(props)
    if (__DEV__) {
      this.state = {
        ready: false,
        activeRing: "current",
        ring: rings.current
      }
    } else {
      this.state = {
        ready: false,
        activeRing: "slow",
        ring: rings.slow
      }
    }
  }

  componentDidMount() {
    
    this._loadAsync()

  }

  async _loadAsync(){
    // await deleteActiveRing()
    await this._readActiveRing()

    
    this.setState({
      ready: true
    })
  }

  async _readActiveRing(){
    let ring = await getActiveRing()
    // console.warn("ring " + ring)
    if(ring !== null ){
      this.setState({
        activeRing: ring,
        ring: ring === "current" ? rings.current : rings.slow
      })
    }
    
  }

  onUseCurrent() {
    this._readActiveRing()
    // if (this.state.activeRing === "current") {
    //   this.setState({
    //     activeRing: "slow",
    //     ring: rings.slow
    //   });
    // } else {
    //   this.setState({
    //     activeRing: "current",
    //     ring: rings.current
    //   });
    // }
  }

  renderButtons() {
    const {activeRing, ring} = this.state
    return (
      <View style={{ height: 50, marginTop: 0, backgroundColor: 'orange' }}>
        <TouchableOpacity onPress={() => { this.onUseCurrent(); }}>
          <Text>Active ring: {activeRing.toUpperCase()}, Swift version: {ring}, Expo SDK: {rings.ExpoSDK}</Text>
        </TouchableOpacity>
      </View>)
  }

  render() {
    const { ready, activeRing } = this.state
    if(!ready) return null

    // const buttons = this.renderButtons()
    const activeApp = (activeRing === 'current') ? (<AppCurrent/>) : (<AppSlow/>)

    return activeApp;
    
    return (
      <>
      
      {activeApp}
      
      </>
    )
  }
}
